/**
 * Form plugins demo
 */
(function($) {
  'use strict';

  
  /******** Dateranger picker ********/
  if ($('.drp').length) {
      $('.drp').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'MM/DD/YYYY h:mm A'
        }
      });
  };
  

   if ($('.select2').length) {
      $('.select2').select2();
   };

  /******** Select 2 plugin ********/
  

})(jQuery);
