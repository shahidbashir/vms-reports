-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2017 at 09:20 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `msmswwwauxiliov_aux`
--

-- --------------------------------------------------------

--
-- Table structure for table `vms_cronejob_report`
--

CREATE TABLE IF NOT EXISTS `vms_cronejob_report` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `client_id` int(100) NOT NULL,
  `main_client` int(100) NOT NULL,
  `report_id` int(100) NOT NULL,
  `report_group` int(100) NOT NULL,
  `run_frequency` varchar(200) NOT NULL,
  `run_day` varchar(200) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `format` varchar(200) NOT NULL,
  `reciver` varchar(200) NOT NULL,
  `date_created` date NOT NULL,
  `cronejob_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
