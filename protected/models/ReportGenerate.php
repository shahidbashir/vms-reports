<?php

/**
 * This is the model class for table "vms_report_generate".
 *
 * The followings are the available columns in table 'vms_report_generate':
 * @property integer $id
 * @property integer $client_id
 * @property integer $main_client
 * @property integer $job_category
 * @property integer $location_id
 * @property integer $department_id
 * @property integer $supplier_id
 * @property string $report_name
 * @property string $report_group
 * @property string $access_group
 * @property string $formate
 * @property string $date_created
 */
class ReportGenerate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_report_generate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, main_client, job_category, location, department, supplier, report_name, report_group, access_group, formate, date_created', 'save'),
			array('client_id, main_client', 'numerical', 'integerOnly'=>true),
			array('report_name, report_group, access_group, formate', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, main_client, job_category, location, department, supplier, report_name, report_group, access_group, formate, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'main_client' => 'Main Client',
			'job_category' => 'Job Category',
			'location' => 'Location',
			'department' => 'Department',
			'supplier' => 'Supplier',
			'report_name' => 'Report Name',
			'report_group' => 'Report Group',
			'access_group' => 'Access Group',
			'formate' => 'Formate',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('main_client',$this->main_client);
		$criteria->compare('job_category',$this->job_category);
		$criteria->compare('location',$this->location);
		$criteria->compare('department',$this->department);
		$criteria->compare('supplier',$this->supplier);
		$criteria->compare('report_name',$this->report_name,true);
		$criteria->compare('report_group',$this->report_group,true);
		$criteria->compare('access_group',$this->access_group,true);
		$criteria->compare('formate',$this->formate,true);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReportGenerate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
