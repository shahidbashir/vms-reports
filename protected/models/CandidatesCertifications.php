<?php

/**
 * This is the model class for table "vms_candidates_certifications".
 *
 * The followings are the available columns in table 'vms_candidates_certifications':
 * @property integer $id
 * @property integer $candidate_id
 * @property integer $profile_id
 * @property string $certification_name
 * @property string $certification_authority
 * @property string $license_number
 * @property string $certification_URL
 * @property string $dates
 * @property string $date_created
 * @property string $date_updated
 */
class CandidatesCertifications extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_candidates_certifications';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('candidate_id, profile_id, certification_name, certification_authority, license_number, certification_URL, dates, date_created, date_updated', 'safe'),
			array('candidate_id, profile_id', 'numerical', 'integerOnly'=>true),
			array('certification_name, certification_URL', 'length', 'max'=>255),
			array('certification_authority', 'length', 'max'=>50),
			array('license_number', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, candidate_id, profile_id, certification_name, certification_authority, license_number, certification_URL, dates, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'candidate_id' => 'Candidate',
			'profile_id' => 'Profile',
			'certification_name' => 'Certification Name',
			'certification_authority' => 'Certification Authority',
			'license_number' => 'License Number',
			'certification_URL' => 'Certification Url',
			'dates' => 'Dates',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('certification_name',$this->certification_name,true);
		$criteria->compare('certification_authority',$this->certification_authority,true);
		$criteria->compare('license_number',$this->license_number,true);
		$criteria->compare('certification_URL',$this->certification_URL,true);
		$criteria->compare('dates',$this->dates,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CandidatesCertifications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
