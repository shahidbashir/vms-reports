<?php

class CpTimesheetDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cp_timesheet_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('timesheet_id, contract_id, candidate_id, client_config_type, project_code_id, creation_week_number, creation_year, creation_day, hours_type, no_of_hours, total_payrate,, total_billrate, date_created, date_updated', 'required'),
			array('timesheet_id, contract_id, candidate_id, client_config_type, project_code_id, creation_week_number, creation_year, no_of_hours', 'numerical', 'integerOnly'=>true),
			array('creation_day', 'length', 'max'=>11),
			array('hours_type', 'length', 'max'=>12),
			array('total_payrate, total_billrate', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, timesheet_id, contract_id, candidate_id, client_config_type, project_code_id, creation_week_number, creation_year, creation_day, hours_type, no_of_hours, total_payrate, total_billrate, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'timesheet_id' => 'Timesheet',
			'contract_id' => 'Contract',
			'candidate_id' => 'Candidate',
			'client_config_type' => 'Client Config Type',
			'project_code_id' => 'Project Code',
			'creation_week_number' => 'Creation Week Number',
			'creation_year' => 'Creation Year',
			'creation_day' => 'Creation Day',
			'hours_type' => 'Hours Type',
			'no_of_hours' => 'No Of Hours',
			'total_payrate' => 'Total Payrate',
			'total_billrate' => 'Total Billrate',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('timesheet_id',$this->timesheet_id);
		$criteria->compare('contract_id',$this->contract_id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('client_config_type',$this->client_config_type);
		$criteria->compare('project_code_id',$this->project_code_id);
		$criteria->compare('creation_week_number',$this->creation_week_number);
		$criteria->compare('creation_year',$this->creation_year);
		$criteria->compare('creation_day',$this->creation_day,true);
		$criteria->compare('hours_type',$this->hours_type,true);
		$criteria->compare('no_of_hours',$this->no_of_hours);
		$criteria->compare('total_payrate',$this->total_payrate,true);
		$criteria->compare('total_billrate',$this->total_billrate,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CpTimesheetDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
