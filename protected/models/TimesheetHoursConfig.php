<?php

/**
 * This is the model class for table "vms_timesheet_hours_config".
 *
 * The followings are the available columns in table 'vms_timesheet_hours_config':
 * @property integer $id
 * @property integer $client_id
 * @property integer $location_id
 * @property integer $location_name
 * @property integer $hours_type
 * @property integer $total_hours
 * @property integer $mon
 * @property integer $tue
 * @property integer $wed
 * @property integer $thu
 * @property integer $fri
 * @property integer $sat
 * @property integer $sun
 * @property integer $doubletime_sat
 * @property integer $doubletime_sun
 * @property integer $overtime
 * @property integer $date_created
 */
class TimesheetHoursConfig extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_timesheet_hours_config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, created_by_id, location_id, location_name, hours_type, total_hours, mon, tue, wed, thu, fri, sat, sun, doubletime_sat, doubletime_sun, overtime, date_created', 'required'),
			array('client_id, location_id, location_name, hours_type, total_hours, mon, tue, wed, thu, fri, sat, sun, doubletime_sat, doubletime_sun, overtime, date_created', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, location_id, location_name, hours_type, total_hours, mon, tue, wed, thu, fri, sat, sun, doubletime_sat, doubletime_sun, overtime, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'location_id' => 'Location',
			'location_name' => 'Location Name',
			'hours_type' => 'Hours Type',
			'total_hours' => 'Total Hours',
			'mon' => 'Mon',
			'tue' => 'Tue',
			'wed' => 'Wed',
			'thu' => 'Thu',
			'fri' => 'Fri',
			'sat' => 'Sat',
			'sun' => 'Sun',
			'doubletime_sat' => 'Doubletime Sat',
			'doubletime_sun' => 'Doubletime Sun',
			'overtime' => 'Overtime',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('location_name',$this->location_name);
		$criteria->compare('hours_type',$this->hours_type);
		$criteria->compare('total_hours',$this->total_hours);
		$criteria->compare('mon',$this->mon);
		$criteria->compare('tue',$this->tue);
		$criteria->compare('wed',$this->wed);
		$criteria->compare('thu',$this->thu);
		$criteria->compare('fri',$this->fri);
		$criteria->compare('sat',$this->sat);
		$criteria->compare('sun',$this->sun);
		$criteria->compare('doubletime_sat',$this->doubletime_sat);
		$criteria->compare('doubletime_sun',$this->doubletime_sun);
		$criteria->compare('overtime',$this->overtime);
		$criteria->compare('date_created',$this->date_created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TimesheetHoursConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
