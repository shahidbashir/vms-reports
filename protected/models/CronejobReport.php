<?php

/**
 * This is the model class for table "vms_cronejob_report".
 *
 * The followings are the available columns in table 'vms_cronejob_report':
 * @property integer $id
 * @property integer $client_id
 * @property integer $main_client
 * @property integer $report_id
 * @property integer $report_group
 * @property string $run_frequency
 * @property string $run_day
 * @property string $start_date
 * @property string $end_date
 * @property string $format
 * @property string $reciver
 * @property string $date_created
 */
class CronejobReport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_cronejob_report';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, main_client, report_id, report_group, run_frequency, run_day, start_date, end_date, format, reciver, date_created', 'required'),
			array('client_id, main_client, report_id, report_group', 'numerical', 'integerOnly'=>true),
			array('cronejob_date','safe'),
			array('run_frequency, run_day, format, reciver', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, main_client, report_id, report_group, run_frequency, run_day, start_date, end_date, format, reciver, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'main_client' => 'Main Client',
			'report_id' => 'Report',
			'report_group' => 'Report Group',
			'run_frequency' => 'Run Frequency',
			'run_day' => 'Run Day',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'format' => 'Format',
			'reciver' => 'Reciver',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('main_client',$this->main_client);
		$criteria->compare('report_id',$this->report_id);
		$criteria->compare('report_group',$this->report_group);
		$criteria->compare('run_frequency',$this->run_frequency,true);
		$criteria->compare('run_day',$this->run_day,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('format',$this->format,true);
		$criteria->compare('reciver',$this->reciver,true);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CronejobReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
