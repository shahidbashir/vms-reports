<?php

/**
 * This is the model class for table "vms_contract".
 *
 * The followings are the available columns in table 'vms_contract':
 * @property integer $id
 * @property integer $workorder_id
 * @property integer $offer_id
 * @property integer $submission_id
 * @property integer $job_id
 * @property integer $client_id
 * @property integer $vendor_id
 * @property integer $candidate_id
 * @property string $start_date
 * @property string $end_date
 * @property string $date_created
 */
class Contract extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_contract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('workorder_id,created_by_id, offer_id, submission_id, job_id, client_id, vendor_id, candidate_id, start_date, end_date, date_created,termination_status,reason_of_termination,termination_notes,termination_can_feedback,termination_date,term_submited_datetime,term_by_id,term_by_type
,term_app_rej_by_id,term_app_rej_by_type,term_app_rej_datetime,ext_status,ext_vendor_approval,contract_work_flow,candidate_dept,job_title,supervisor_name,supervisor_email,supervisor_phone ,hellosign_api_req_id ,hellosign_signer1_id,hellosign_signer2_id,hellosign_signer1_status,hellosign_signer2_status,hellosign_signer1_date,hellosign_signer2_date,hellosign_api_sign_status,hellosign_req_date , doc_signed_date,offer_digital_doc', 'safe'),
			array('workorder_id, offer_id, submission_id, job_id, client_id, vendor_id, candidate_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, workorder_id, offer_id, submission_id, job_id, client_id, vendor_id, candidate_id, start_date, end_date, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'workorder_id' => 'Workorder',
			'offer_id' => 'Offer',
			'submission_id' => 'Submission',
			'job_id' => 'Job',
			'client_id' => 'Client',
			'vendor_id' => 'Vendor',
			'candidate_id' => 'Candidate',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('workorder_id',$this->workorder_id);
		$criteria->compare('offer_id',$this->offer_id);
		$criteria->compare('submission_id',$this->submission_id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
