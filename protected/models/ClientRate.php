<?php

/**
 * This is the model class for table "vms_client_rate".
 *
 * The followings are the available columns in table 'vms_client_rate':
 * @property integer $id
 * @property integer $client_id
 * @property string $type
 * @property string $pricing
 * @property string $over_time
 * @property string $double_time
 * @property integer $created_by
 * @property string $date_created
 */
class ClientRate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_client_rate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, type, pricing, over_time, double_time, created_by, date_created', 'required'),
			array('client_id, created_by', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>15),
			array('pricing', 'length', 'max'=>20),
			array('over_time, double_time', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, type, pricing, over_time, double_time, created_by, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'type' => 'Type',
			'pricing' => 'Pricing',
			'over_time' => 'Over Time',
			'double_time' => 'Double Time',
			'created_by' => 'Created By',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('pricing',$this->pricing,true);
		$criteria->compare('over_time',$this->over_time,true);
		$criteria->compare('double_time',$this->double_time,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClientRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
