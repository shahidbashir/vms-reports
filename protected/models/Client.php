<?php

class Client extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_client';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('super_client_id, first_name, last_name, organization, email, business_name, website_url, business_type, total_staff, revenue_range, describe_business, linkedin_profile, twitter_profile, profile_status, profile_approve, type, password, date_created, date_updated,', 'safe'),
			array('first_name, last_name, organization, email', 'required', 'on'=>'signup1'),
			
			//array('profile_image', 'file', 'types'=>'jpg, png', 'safe','on'=>'buss_steps'),
			
			array('client_job_status,organization,review_email,skip_createlocation,skip_teammembers,password,google_access_token , outlook_access_token','safe'),
			array('first_name,last_name','CRegularExpressionValidator','on'=>'signup1', 'pattern'=>'/^[a-zA-z]{2,}$/','message'=>"{attribute} should contain only letters."),
			array('email', 'checkEmailAddress', 'on'=>'signup1'),
			array('email', 'email', 'on'=>'signup1'),
			array('email,password', 'required', 'on'=>'signin'),
			array('password', 'required', 'on'=>'create_password'),
			array('first_name, last_name, business_name, total_staff', 'required', 'on'=>'singup2'),
			array('first_name,last_name','CRegularExpressionValidator','on'=>'singup2', 'pattern'=>'/^[a-zA-z]{2,}$/','message'=>"{attribute} should contain only character."),
			//array('linkedin_profile,twitter_profile', 'url', 'on'=>'singup2'),
			array('linkedin_profile,twitter_profile,currency', 'safe', 'on'=>'singup2'),
			array('website_url','url', 'validSchemes'=>array('http','https')),
			array('department,member_type,first_name, last_name, email', 'safe', 'on'=>'teammember'),
			array( 'first_name, last_name','CRegularExpressionValidator','pattern'=>'/^[a-zA-z ]{2,}$/','message'=>"{attribute} should contain only letters.",'on'=>'teammember'),
			array('email', 'checkEmailAddress', 'on'=>'teammember'),
			array('email', 'email','message'=>"In-valid {attribute}", 'on'=>'teammember'),
			array('profile_status_by,mobile,phone','safe'),
			//array('profile_status_by', 'checkEnabled', 'on'=>'signin'),
			
			array('profile_image','file','types'=>'jpeg,jpg,png','maxSize'=>1024*1024*2, 'tooLarge'=>'File has to be smaller than 2MB', 'on'=>'create_profile'),
/*
		
			array('super_client_id, profile_status, profile_approve', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, email, business_name, revenue_range', 'length', 'max'=>50),
			array('organization, website_url, linkedin_profile, twitter_profile, password', 'length', 'max'=>255),
			array('business_type', 'length', 'max'=>30),
			array('type', 'length', 'max'=>13), */
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, super_client_id, first_name, last_name, organization, email, business_name, website_url, business_type, total_staff, revenue_range, describe_business, linkedin_profile, twitter_profile, profile_status, profile_approve, type, password, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}
	public function checkEmailAddress($attribute,$params)
	{
	    
	    if(!Yii::app()->user->isGuest) {
	    	$client = Client::model()->findAll(array('condition'=>'(t.super_client_id='.Yii::app()->user->id.' or t.id='.Yii::app()->user->id.') and t.email="'.$this->$attribute.'"'));
	    	if( $client ){
	    	 $this->addError($attribute, 'Email already taken');
	    	}
	     }else {
	     	$client = Client::model()->findAll(array('condition'=>'t.email="'.$this->$attribute.'"'));
	     	if( $client ){
	    	 $this->addError($attribute, 'Email already taken');
	    	}
	     }  
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'super_client_id' => 'Super Client',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'organization' => 'Organization',
			'email' => 'Email',
			'business_name' => 'Business Name',
			'website_url' => 'Website Url',
			'business_type' => 'Business Type',
			'total_staff' => 'Total Staff',
			'revenue_range' => 'Revenue Range',
			'describe_business' => 'Describe Business',
			'linkedin_profile' => 'Linkedin Profile',
			'twitter_profile' => 'Twitter Profile',
			'profile_status' => 'Profile Status',
			'profile_approve' => 'Profile Approve',
			'type' => 'Type',
			'password' => 'Password',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('super_client_id',$this->super_client_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('organization',$this->organization,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('business_name',$this->business_name,true);
		$criteria->compare('website_url',$this->website_url,true);
		$criteria->compare('business_type',$this->business_type,true);
		$criteria->compare('total_staff',$this->total_staff);
		$criteria->compare('revenue_range',$this->revenue_range,true);
		$criteria->compare('describe_business',$this->describe_business,true);
		$criteria->compare('linkedin_profile',$this->linkedin_profile,true);
		$criteria->compare('twitter_profile',$this->twitter_profile,true);
		$criteria->compare('profile_status',$this->profile_status);
		$criteria->compare('profile_approve',$this->profile_approve);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	 
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
