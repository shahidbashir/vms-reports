<?php

class Vendor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_vendor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('super_vendor_id, first_name, last_name, organization, email, person_number, facebook, group, live_account, status, business_name, website_url, business_type, total_staff, revenue_range, describe_business, department, member_type, linkedin_profile, twitter_profile, profile_status, profile_approve,profile_approved_by,profile_approved_type,profile_approved_date,created_by,created_by_type, password, date_created, date_updated', 'safe'),

			array('zip_code, state, city, country, address2, address1, phone, description, contact_person_email,geo_locations', 'safe'),


			array('super_vendor_id, profile_status', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, email, group, business_name, revenue_range, department', 'length', 'max'=>50),
			array('organization, facebook, website_url, linkedin_profile, twitter_profile, password', 'length', 'max'=>255),
			array('person_number, total_staff', 'length', 'max'=>100),
			array('live_account, profile_approve', 'length', 'max'=>3),
			array('status', 'length', 'max'=>17),
			array('business_type, member_type', 'length', 'max'=>30),
			
			array('email','email'),
			array('email','unique'),
			array('first_name,last_name','CRegularExpressionValidator','pattern'=>'/^[a-zA-z]{2,}$/','message'=>"{attribute} should contain only letters and should have atleast 2 of them."),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, super_vendor_id, first_name, last_name, organization, email, person_number, facebook, group, live_account, status, business_name, website_url, business_type, total_staff, revenue_range, describe_business, department, member_type, linkedin_profile, twitter_profile, profile_status, profile_approve, password, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vendorsClient'=>array(self::HAS_MANY, 'AssignedVendorClient', 'vendor_id'),

			'vendorsClientMarkup'=>array(self::HAS_MANY, 'VendorClientMarkup', 'vendor_id'),
			'vendorJobs'=>array(self::HAS_ONE, 'VendorJobs', 'vendor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'super_vendor_id' => 'Super Vendor',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'organization' => 'Organization',
			'email' => 'Email',
			'person_number' => 'Person Number',
			'facebook' => 'Facebook',
			'group' => 'Group',
			'live_account' => 'Live Account',
			'status' => 'Status',
			'business_name' => 'Business Name',
			'website_url' => 'Website Url',
			'business_type' => 'Business Type',
			'total_staff' => 'Total Staff',
			'revenue_range' => 'Revenue Range',
			'describe_business' => 'Describe Business',
			'department' => 'Department',
			'member_type' => 'Member Type',
			'linkedin_profile' => 'Linkedin Profile',
			'twitter_profile' => 'Twitter Profile',
			'profile_status' => 'Profile Status',
			'profile_approve' => 'Profile Approve',
			'password' => 'Password',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('super_vendor_id',$this->super_vendor_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('organization',$this->organization,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('person_number',$this->person_number,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('group',$this->group,true);
		$criteria->compare('live_account',$this->live_account,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('business_name',$this->business_name,true);
		$criteria->compare('website_url',$this->website_url,true);
		$criteria->compare('business_type',$this->business_type,true);
		$criteria->compare('total_staff',$this->total_staff,true);
		$criteria->compare('revenue_range',$this->revenue_range,true);
		$criteria->compare('describe_business',$this->describe_business,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('member_type',$this->member_type,true);
		$criteria->compare('linkedin_profile',$this->linkedin_profile,true);
		$criteria->compare('twitter_profile',$this->twitter_profile,true);
		$criteria->compare('profile_status',$this->profile_status);
		$criteria->compare('profile_approve',$this->profile_approve,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vendor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
