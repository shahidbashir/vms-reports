<?php

/**
 * This is the model class for table "vms_contract_history".
 *
 * The followings are the available columns in table 'vms_contract_history':
 * @property integer $workorder_id
 * @property integer $offer_id
 * @property integer $submission_id
 * @property integer $job_id
 * @property integer $client_id
 * @property integer $vendor_id
 * @property integer $candidate_id
 * @property string $contract_start_date
 * @property string $contract_end_date
 * @property integer $contract_status
 * @property string $wo_start_date
 * @property string $wo_end_date
 * @property integer $onboard_changed_start_date
 * @property integer $onboard_changed_end_date
 * @property string $wo_bill_rate
 * @property string $wo_pay_rate
 * @property string $wo_over_time
 * @property string $wo_double_time
 * @property string $wo_client_over_time
 * @property string $wo_client_double_time
 * @property string $date_created
 */
class ContractHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_contract_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('workorder_id, offer_id, submission_id, job_id, client_id, vendor_id, candidate_id, contract_start_date, contract_end_date, contract_status, wo_start_date, wo_end_date, onboard_changed_start_date, onboard_changed_end_date, wo_bill_rate, wo_pay_rate, wo_over_time, wo_double_time, wo_client_over_time, wo_client_double_time, date_created', 'required'),
			array('workorder_id, offer_id, submission_id, job_id, client_id, vendor_id, candidate_id, contract_status, onboard_changed_start_date, onboard_changed_end_date', 'numerical', 'integerOnly'=>true),
			array('wo_bill_rate, wo_pay_rate, wo_over_time, wo_double_time, wo_client_over_time, wo_client_double_time', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('workorder_id, offer_id, submission_id, job_id, client_id, vendor_id, candidate_id, contract_start_date, contract_end_date, contract_status, wo_start_date, wo_end_date, onboard_changed_start_date, onboard_changed_end_date, wo_bill_rate, wo_pay_rate, wo_over_time, wo_double_time, wo_client_over_time, wo_client_double_time, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'workorder_id' => 'Workorder',
			'offer_id' => 'Offer',
			'submission_id' => 'Submission',
			'job_id' => 'Job',
			'client_id' => 'Client',
			'vendor_id' => 'Vendor',
			'candidate_id' => 'Candidate',
			'contract_start_date' => 'Contract Start Date',
			'contract_end_date' => 'Contract End Date',
			'contract_status' => 'Contract Status',
			'wo_start_date' => 'Wo Start Date',
			'wo_end_date' => 'Wo End Date',
			'onboard_changed_start_date' => 'Onboard Changed Start Date',
			'onboard_changed_end_date' => 'Onboard Changed End Date',
			'wo_bill_rate' => 'Wo Bill Rate',
			'wo_pay_rate' => 'Wo Pay Rate',
			'wo_over_time' => 'Wo Over Time',
			'wo_double_time' => 'Wo Double Time',
			'wo_client_over_time' => 'Wo Client Over Time',
			'wo_client_double_time' => 'Wo Client Double Time',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('workorder_id',$this->workorder_id);
		$criteria->compare('offer_id',$this->offer_id);
		$criteria->compare('submission_id',$this->submission_id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('contract_start_date',$this->contract_start_date,true);
		$criteria->compare('contract_end_date',$this->contract_end_date,true);
		$criteria->compare('contract_status',$this->contract_status);
		$criteria->compare('wo_start_date',$this->wo_start_date,true);
		$criteria->compare('wo_end_date',$this->wo_end_date,true);
		$criteria->compare('onboard_changed_start_date',$this->onboard_changed_start_date);
		$criteria->compare('onboard_changed_end_date',$this->onboard_changed_end_date);
		$criteria->compare('wo_bill_rate',$this->wo_bill_rate,true);
		$criteria->compare('wo_pay_rate',$this->wo_pay_rate,true);
		$criteria->compare('wo_over_time',$this->wo_over_time,true);
		$criteria->compare('wo_double_time',$this->wo_double_time,true);
		$criteria->compare('wo_client_over_time',$this->wo_client_over_time,true);
		$criteria->compare('wo_client_double_time',$this->wo_client_double_time,true);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContractHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
