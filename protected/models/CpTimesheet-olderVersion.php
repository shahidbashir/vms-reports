<?php

/**
 * This is the model class for table "cp_timesheet".
 *
 * The followings are the available columns in table 'cp_timesheet':
 * @property integer $id
 * @property integer $candidate_id
 * @property integer $offer_id
 * @property integer $project_id
 * @property string $month_year
 * @property integer $week_number
 * @property string $type
 * @property string $mon
 * @property string $tue
 * @property string $wed
 * @property string $thu
 * @property string $fri
 * @property string $sat
 * @property string $sun
 * @property string $pay_rate
 * @property string $total
 * @property string $notes
 * @property integer $status
 * @property string $date_created
 */
class CpTimesheet extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cp_timesheet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('timesheet_log_id,candidate_id, offer_id, project_id, month_year, week_number, type, mon, tue, wed, thu, fri, sat, sun, pay_rate, total, notes, status, date_created', 'required'),
			array('timesheet_log_id,candidate_id, offer_id, project_id, week_number, type, mon, tue, wed, thu, fri, sat, sun, pay_rate, total, notes, date_created', 'safe'),
			array('candidate_id, offer_id, project_id, week_number', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>12),
			array('mon, tue, wed, thu, fri, sat, sun, pay_rate, total', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, candidate_id, offer_id, project_id, week_number, type, mon, tue, wed, thu, fri, sat, sun, pay_rate, total, notes, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'candidate_id' => 'Candidate',
			'offer_id' => 'Offer',
			'project_id' => 'Job',
			'week_number' => 'Week Number',
			'type' => 'Type',
			'mon' => 'Mon',
			'tue' => 'Tue',
			'wed' => 'Wed',
			'thu' => 'Thu',
			'fri' => 'Fri',
			'sat' => 'Sat',
			'sun' => 'Sun',
			'pay_rate' => 'Pay Rate',
			'total' => 'Total',
			'notes' => 'Notes',
			'status' => 'Status',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('offer_id',$this->offer_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('week_number',$this->week_number);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('mon',$this->mon,true);
		$criteria->compare('tue',$this->tue,true);
		$criteria->compare('wed',$this->wed,true);
		$criteria->compare('thu',$this->thu,true);
		$criteria->compare('fri',$this->fri,true);
		$criteria->compare('sat',$this->sat,true);
		$criteria->compare('sun',$this->sun,true);
		$criteria->compare('pay_rate',$this->pay_rate,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CpTimesheet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
