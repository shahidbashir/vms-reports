<?php

class Calendar extends CFormModel
{
    public $events = array(
        array('event_id' => '1', 'subject' => 'Subject of event 1', 'content' => 'Content of event 1', 'start' => '2016-11-17T18:00:00', 'end' => '2016-11-17T19:00:00'),
        array('event_id' => '2', 'subject' => 'Subject of event 2', 'content' => 'Content of event 2', 'start' => '2016-11-18T18:00:00', 'end' => '2016-11-18T19:00:00'),
        array('event_id' => '3', 'subject' => 'Subject of event 3', 'content' => 'Content of event 3', 'start' => '2016-11-19T18:00:00', 'end' => '2016-11-19T19:00:00'),
        array('event_id' => '4', 'subject' => 'Subject of event 4', 'content' => 'Content of event 4', 'start' => '2016-11-20T18:00:00', 'end' => '2016-11-20T19:00:00'),
        array('event_id' => '5', 'subject' => 'Subject of event 5', 'content' => 'Content of event 5', 'start' => '2016-11-21T18:00:00', 'end' => '2016-11-21T19:00:00'),
    );

    public function addEventToCalendar($event_id) {
        $payload = "{
          'Subject': '".$this->events[$event_id]['subject']."',
          'Body': {
            'ContentType': 'HTML',
            'Content': '".$this->events[$event_id]['content']."'
          },
          'Start': {
            'DateTime': '".$this->events[$event_id]['start']."',
            'TimeZone': 'Pacific Standard Time'
          },
          'End': {
            'DateTime': '".$this->events[$event_id]['end']."',
            'TimeZone': 'Pacific Standard Time'
          }
        }";
        
        return OutlookService::postEvents(Yii::app()->session['access_token'], Yii::app()->session['user_email'], $payload);
    }
}