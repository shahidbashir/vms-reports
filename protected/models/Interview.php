<?php
/**
 * This is the model class for table "vms_interview".
 *
 * The followings are the available columns in table 'vms_interview':
 * @property integer $id
 * @property integer $submission_id
 * @property string $rejected_by_type
 * @property string $rejected_by_id
 * @property string $rejection_reason
 */
class Interview extends CActiveRecord
{	
	public $name;
	public $startDate;
	public $endDate;
	public $startTime;
	public $endTime;
	public $descriptions;
	public $secret;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_interview';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('submission_id, rejected_by_type, rejected_by_id, rejection_reason', 'required'),
			array('submission_id', 'required'),
			array('submission_id', 'numerical', 'integerOnly'=>true),
			array('submission_id', 'numerical', 'integerOnly'=>true),
			array('rejected_by_type, rejected_by_id', 'length', 'max'=>111),
			array('first_duration,second_duration,third_duration,fourth_duration,interview_type,created_by_id,created_by_type,date_created,interview_creation_date,job_id,interviewer,hiring_score,interview_event_name,interview_start_date,interview_start_date_time,interview_end_date,interview_end_date_time,start_date_status,interview_where,interview_description,interview_attachment,interview_added_guests,interview_alt1_start_date,interview_alt1_start_date_time,interview_alt1_end_date,interview_alt1_end_date_time,alt1_date_status,interview_alt2_start_date,interview_alt2_start_date_time,interview_alt2_end_date,interview_alt2_end_date_time,alt2_date_status,interview_alt3_start_date,interview_alt3_start_date_time,interview_alt3_end_date,interview_alt3_end_date_time,alt3_date_status,follow_up_vendor,follow_up_msp,invite_team_member', 'safe'),
			
			//new fields added for feedback of interview
   			array('reason_for_reschedual,reschedual_note,edu_background, edu_comments, prior_w_exp, prior_w_exp_comments, tech_work_exp, tech_work_exp_comments, verbal_communication, verbal_communication_comments, can_enthusiasm, can_enthusiasm_comments, knowledge_company, knowledge_company_notes, inter_personal_skills, inter_personal_skills_comments, initiative, initiative_comments, time_management,  time_management_comments, customer_service, customer_service_comments, overall_impression, overall_impression_comments, assessment_documents, rating_notes, feedback_status','safe'),
	
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, submission_id, rejected_by_type, rejected_by_id, rejection_reason', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'submission_id' => 'Submission',
			'rejected_by_type' => 'Rejected By Type',
			'rejected_by_id' => 'Rejected By',
			'rejection_reason' => 'Rejection Reason',
		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('submission_id',$this->submission_id);
		$criteria->compare('rejected_by_type',$this->rejected_by_type,true);
		$criteria->compare('rejected_by_id',$this->rejected_by_id,true);
		$criteria->compare('rejection_reason',$this->rejection_reason,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function afterSave()
    {
		//session_destroy();
        parent::afterSave();
		
		$interviewData = Interview::model()->findByPk($this->id);
		$submissionData = VendorJobSubmission::model()->findByPk($interviewData->submission_id);
		$candidateData = Candidates::model()->findByPk($submissionData->candidate_Id);
		$job = Job::model()->findByPk($submissionData->job_id);
        $vendor = Vendor::model()->findByPk($submissionData->vendor_id);
		if($interviewData->start_date_status == 1){
			$intDate = $interviewData->interview_start_date;
			}elseif($interviewData->alt1_date_status == 1){
				$intDate = $interviewData->interview_alt1_start_date;
				}elseif($interviewData->alt2_date_status == 1){
					$intDate = $interviewData->interview_alt2_start_date;
					}elseif($interviewData->alt3_date_status == 1){
						$intDate = $interviewData->interview_alt3_start_date;
						}else{
							 $intDate = $interviewData->interview_start_date;
							 }
			$this->startDate = $intDate;
			$this->endDate = $intDate;
			$this->startTime='';
			$this->endTime='';
        	$this->name = "Client: ".$candidateData->first_name.' '.$candidateData->last_name.' ('.$interviewData->interview_event_name.' )';
			$this->descriptions = '
				Job ID: '.$submissionData->job_id.'
				';
				$this->descriptions .= '
				Job Name: '.$job->title.'
				';
				$this->descriptions .= '
				Candidate Name: '.$candidateData->first_name.' '.$candidateData->last_name.'
				';
				$this->descriptions .= '
				Vendor Name: '.$vendor->first_name.' '.$vendor->last_name.'
				';
				$this->descriptions .= '
				Description from Interview: '.$interviewData->interview_description.'
				';
	
        error_reporting(0);
        if($interviewData->status == 2)
        {
        	require_once Yii::getPathOfAlias('ext').'/google-api-php-client/vendor/autoload.php';
            // Status changed.
           
            // START: Get msp users, which success passed Google Auth.
            $followUpMsp = explode(",",$interviewData->follow_up_msp);
            $exp ='';
            foreach($followUpMsp as  $value) {
            	$exp .= '"'.$value.'",';
            }
            $exp = rtrim($exp,',');
			$users = Admin::model()->findAll('google_access_token IS NOT NULL and id in('.$exp.')');
            if(count($users)>0){
            	$this->secret = Yii::app()->params['msp_secret_path'];
                foreach($users as $item){
                	$this->syncWithGoogleCalendar($item);
                }
            }
             // START: Get vendor users, which success passed Google Auth.
            $followUpVendor = explode(",",$interviewData->follow_up_vendor);
            $exp ='';
            foreach($followUpVendor as  $value) {
            	$exp .= '"'.$value.'",';
            }
            $exp = rtrim($exp,',');
			$vendor = Vendor::model()->findAll('google_access_token IS NOT NULL and id in('.$exp.')');
            if(count($vendor)>0){
            	$this->secret = Yii::app()->params['vendor_secret_path'];
                foreach($vendor as $item){
                	$this->syncWithGoogleCalendar($item);
                }
            }
             // START: Get Client users, which success passed Google Auth.
            $followUpClient = explode(",",$interviewData->invite_team_member);
            $exp ='';
            foreach($followUpClient as  $value) {
            	$exp .= '"'.$value.'",';
            }
            $exp = rtrim($exp,',');
			$client = Client::model()->findAll(array('condition'=>'google_access_token !="" and id in('.$exp.')'));
            if(count($client)>0){
            	$this->secret = Yii::app()->params['client_secret_path'];
                foreach($client as $item){
                	$this->syncWithGoogleCalendar($item);
                }
            }
			
			//main client id
			$mainClient = Client::model()->findByPk(UtilityManager::superClient(Yii::app()->user->id));
			$this->secret = Yii::app()->params['client_secret_path'];
			$this->syncWithGoogleCalendar($mainClient);
			//$this->syncWithOutlookCalendar($mainClient);
        }
    }
	
    public function  syncWithGoogleCalendar($item){
		$eventUniqId = 'idetifierofevent'.$this->id.'user'.$item->id;
		$calendarId = 'primary'; // It's main calendar.
		$client = new Google_Client();
			        $client->setAuthConfigFile($this->secret);
			        $client->setRedirectUri(Yii::app()->controller->createAbsoluteUrl('/site/oAuth2Callback'));
			        $client->addScope(Google_Service_Calendar::CALENDAR);
			        $client->setAccessType('offline');
			        $client->revokeToken();
			        if($model->google_access_token)
			        {
			        	 //echo "<pre>"; var_dump($client->isAccessTokenExpired());exit;
			        if ($client->isAccessTokenExpired()) {
                            $dbToken = json_decode($item->google_access_token);
                            //echo "<pre> access_token";var_dump($dbToken->access_token);exit;
							$client->setAccessToken($dbToken->access_token);
							$client->refreshToken($dbToken->refresh_token);
							$access_token = $client->getAccessToken();
							$tokens_decoded = json_decode($access_token);
							 /*echo "<pre> ddd ";
							 var_dump($tokens_decoded);
							//$refreshToken = $tokens_decoded->refresh_token;
							echo "ccess_token";
							var_dump($access_token);
							echo "<pre> refreshToken";var_dump($refreshToken);
							echo "<pre> tokens_decoded";var_dump($tokens_decoded);
                            exit;
                            $model->google_access_token = json_encode($access_token);
			        		//$refreshTokenSaved = $client->getAccessToken();
			        		/*$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());*/
			        		//$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			        		//$model->google_access_token = json_encode($client->getAccessToken()); 
			            	/*if($model->save(false)){
			            	}*/
			  			} } 
                   
		$client->setAuthConfigFile($this->secret);
		$client->addScope(Google_Service_Calendar::CALENDAR);
		$client->setAccessToken(json_decode($item->google_access_token,true));
		$service = new Google_Service_Calendar($client);
		try{
		$event = new Google_Service_Calendar_Event(array(
		    'id'=>$eventUniqId.time(),
		    'summary' =>$this->name,
		    'description'=>$this->descriptions,
		    'start' => array(
		        'dateTime' => date("Y-m-d\TH:i:s",strtotime($this->startDate)),
		        'timeZone' => "America/New_York",
		    ),
		    'end' => array(
		        'dateTime' =>  date("Y-m-d\TH:i:s",strtotime($this->endDate)),
		        'timeZone' => "America/New_York",
		    ),
		));
		$event = $service->events->insert($calendarId, $event);
		}
		catch(Exception $notExistException){
		}
    }
	
	public function  syncWithOutlookCalendar($item){
		
		try{
			if($item->outlook_access_token)
		{ 
			$interview['subject'] = $this->name;
			$interview['description'] = $this->descriptions;
			$interview['start'] = date("Y-m-d\TH:i:s",strtotime($this->startDate));
			$interview['end'] = date("Y-m-d\TH:i:s",strtotime($this->endDate));
			
			$outlookmodel = new OfflineAccess();					 
			$event = $outlookmodel->addEventToCalendar($interview , $item);

		} 
		}
		catch(Exception $notExistException){
		}
		
		
	}
 	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Interview the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
