<?php

/**
 * This is the model class for table "cp_timesheet_projectdetails".
 *
 * The followings are the available columns in table 'cp_timesheet_projectdetails':
 * @property integer $id
 * @property integer $timehseet_id
 * @property integer $project_code_id
 * @property integer $contract_id
 * @property integer $candidate_id
 * @property integer $client_id
 * @property integer $total_regular_hours
 * @property integer $total_overtime_hours
 * @property integer $total_doubletime_hours
 * @property string $total_payrate
 * @property string $total_billrate
 * @property string $date_created
 * @property string $date_updated
 */
class CpTimesheetProjectdetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cp_timesheet_projectdetails';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('timehseet_id, project_code_id, contract_id, candidate_id, client_id, total_regular_hours, total_overtime_hours, total_doubletime_hours, total_payrate, total_billrate, date_created, date_updated', 'required'),
			array('timehseet_id, project_code_id, contract_id, candidate_id, client_id, total_regular_hours, total_overtime_hours, total_doubletime_hours', 'numerical', 'integerOnly'=>true),
			array('total_payrate, total_billrate', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, timehseet_id, project_code_id, contract_id, candidate_id, client_id, total_regular_hours, total_overtime_hours, total_doubletime_hours, total_payrate, total_billrate, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'timehseet_id' => 'Timehseet',
			'project_code_id' => 'Project Code',
			'contract_id' => 'Contract',
			'candidate_id' => 'Candidate',
			'client_id' => 'Client',
			'total_regular_hours' => 'Total Regular Hours',
			'total_overtime_hours' => 'Total Overtime Hours',
			'total_doubletime_hours' => 'Total Doubletime Hours',
			'total_payrate' => 'Total Payrate',
			'total_billrate' => 'Total Billrate',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('timehseet_id',$this->timehseet_id);
		$criteria->compare('project_code_id',$this->project_code_id);
		$criteria->compare('contract_id',$this->contract_id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('total_regular_hours',$this->total_regular_hours);
		$criteria->compare('total_overtime_hours',$this->total_overtime_hours);
		$criteria->compare('total_doubletime_hours',$this->total_doubletime_hours);
		$criteria->compare('total_payrate',$this->total_payrate,true);
		$criteria->compare('total_billrate',$this->total_billrate,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CpTimesheetProjectdetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
