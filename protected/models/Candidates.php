<?php

/**
 * This is the model class for table "vms_candidates".
 *
 * The followings are the available columns in table 'vms_candidates':
 * @property integer $id
 * @property integer $source
 * @property integer $vendor_id
 * @property integer $created_by_type
 * @property string $candidate_ID
 * @property string $source_type
 * @property string $vendor_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $security_ID
 * @property string $confirm_security_ID
 * @property string $worker_pay_type
 * @property string $W2_directly
 * @property string $US_permanent_resident
 * @property string $former_contractor
 * @property string $reason_last_leaving
 * @property string $submitted_other_posting
 * @property string $email
 * @property string $email_ID_1
 * @property string $additional_email_ID_2
 * @property string $phone
 * @property string $mobile
 * @property string $birthday
 * @property string $marital_status
 * @property string $fax
 * @property string $address
 * @property integer $zip_code
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $work_status
 * @property string $ethnicity
 * @property string $gender
 * @property string $certifications
 * @property string $resume
 * @property string $notes
 * @property string $start_date
 * @property string $current_location
 * @property string $willing_relocate
 * @property string $experience
 * @property string $emp_start_date
 * @property integer $emp_msp_account_mngr
 * @property integer $emp_client_account_mngr
 * @property integer $emp_time_sheet_aprover
 * @property string $emp_user_name
 * @property string $emp_password
 * @property string $emp_location
 * @property string $emp_official_email
 * @property integer $profile_status
 * @property string $date_created
 */
class Candidates extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_candidates';

	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('source, vendor_id, created_by_type, candidate_ID, source_type, vendor_name, first_name, middle_name, last_name, security_ID, confirm_security_ID, worker_pay_type, W2_directly, US_permanent_resident, former_contractor,right_to_represent, reason_last_leaving, submitted_other_posting, email, email_ID_1, additional_email_ID_2, phone, mobile, birthday, marital_status, fax, address, zip_code, city, state, country, work_status, ethnicity, gender, certifications, resume, notes, start_date, current_location, willing_relocate, experience, emp_start_date, emp_msp_account_mngr, emp_client_account_mngr, emp_time_sheet_aprover, emp_user_name, emp_password, emp_location, emp_official_email, profile_status, date_created,security_ID_digits,confirm_security_ID_digits,profile_mg,skill,primary_skill,secondary_skill,other_skill', 'safe'),
			array('confirm_security_ID', 'compare', 'compareAttribute'=>'security_ID'),
			array('source, vendor_id, created_by_type, zip_code, emp_msp_account_mngr, emp_client_account_mngr, emp_time_sheet_aprover, profile_status', 'numerical', 'integerOnly'=>true),
			array('candidate_ID', 'length', 'max'=>30),
			array('source_type, mobile, emp_user_name, emp_location', 'length', 'max'=>111),
			array('vendor_name, phone', 'length', 'max'=>100),
			array('first_name, last_name, fax, city, state, country, work_status', 'length', 'max'=>55),
			array('middle_name, email_ID_1, additional_email_ID_2', 'length', 'max'=>50),
			array('security_ID, confirm_security_ID, email, ethnicity, resume, emp_password, emp_official_email', 'length', 'max'=>255),
			array('worker_pay_type, current_location, experience', 'length', 'max'=>20),
			array('W2_directly, US_permanent_resident, former_contractor, submitted_other_posting', 'length', 'max'=>10),
			array('marital_status, willing_relocate', 'length', 'max'=>5),
			array('address', 'length', 'max'=>155),
			array('gender', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, source, vendor_id, created_by_type, candidate_ID, source_type, vendor_name, first_name, middle_name, last_name, security_ID, confirm_security_ID, worker_pay_type, W2_directly, US_permanent_resident, former_contractor, reason_last_leaving, submitted_other_posting, email, email_ID_1, additional_email_ID_2, phone, mobile, birthday, marital_status, fax, address, zip_code, city, state, country, work_status, ethnicity, gender, certifications, resume, notes, start_date, current_location, willing_relocate, experience, emp_start_date, emp_msp_account_mngr, emp_client_account_mngr, emp_time_sheet_aprover, emp_user_name, emp_password, emp_location, emp_official_email, profile_status, date_created', 'safe', 'on'=>'search'),
		);
	}

	/* public function beforeSave()
    {
        // in this case, we will use the old hashed password.
        if(empty($this->password) && empty($this->repeat_password) && !empty($this->initialPassword))
            $this->password=$this->repeat_password=$this->initialPassword;
 
        return parent::beforeSave();
    }*/
 

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'source' => 'Source',
			'vendor_id' => 'Vendor',
			'created_by_type' => 'Created By Type',
			'candidate_ID' => 'Candidate',
			'source_type' => 'Source Type',
			'vendor_name' => 'Vendor Name',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'last_name' => 'Last Name',
			'security_ID' => 'Security',
			'confirm_security_ID' => 'Confirm Security',
			'worker_pay_type' => 'Worker Pay Type',
			'W2_directly' => 'W2 Directly',
			'US_permanent_resident' => 'Us Permanent Resident',
			'former_contractor' => 'Former Contractor',
			'reason_last_leaving' => 'Reason Last Leaving',
			'submitted_other_posting' => 'Submitted Other Posting',
			'email' => 'Email',
			'email_ID_1' => 'Email Id 1',
			'additional_email_ID_2' => 'Additional Email Id 2',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'birthday' => 'Birthday',
			'marital_status' => 'Marital Status',
			'fax' => 'Fax',
			'address' => 'Address',
			'zip_code' => 'Zip Code',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'work_status' => 'Work Status',
			'ethnicity' => 'Ethnicity',
			'gender' => 'Gender',
			'certifications' => 'Certifications',
			'resume' => 'Resume',
			'notes' => 'Notes',
			'start_date' => 'Start Date',
			'current_location' => 'Current Location',
			'willing_relocate' => 'Willing Relocate',
			'experience' => 'Experience',
			'emp_start_date' => 'Emp Start Date',
			'emp_msp_account_mngr' => 'Emp Msp Account Mngr',
			'emp_client_account_mngr' => 'Emp Client Account Mngr',
			'emp_time_sheet_aprover' => 'Emp Time Sheet Aprover',
			'emp_user_name' => 'Emp User Name',
			'emp_password' => 'Emp Password',
			'emp_location' => 'Emp Location',
			'emp_official_email' => 'Emp Official Email',
			'profile_status' => 'Profile Status',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('source',$this->source);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('created_by_type',$this->created_by_type);
		$criteria->compare('candidate_ID',$this->candidate_ID,true);
		$criteria->compare('source_type',$this->source_type,true);
		$criteria->compare('vendor_name',$this->vendor_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('security_ID',$this->security_ID,true);
		$criteria->compare('confirm_security_ID',$this->confirm_security_ID,true);
		$criteria->compare('worker_pay_type',$this->worker_pay_type,true);
		$criteria->compare('W2_directly',$this->W2_directly,true);
		$criteria->compare('US_permanent_resident',$this->US_permanent_resident,true);
		$criteria->compare('former_contractor',$this->former_contractor,true);
		$criteria->compare('reason_last_leaving',$this->reason_last_leaving,true);
		$criteria->compare('submitted_other_posting',$this->submitted_other_posting,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('email_ID_1',$this->email_ID_1,true);
		$criteria->compare('additional_email_ID_2',$this->additional_email_ID_2,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('marital_status',$this->marital_status,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('zip_code',$this->zip_code);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('work_status',$this->work_status,true);
		$criteria->compare('ethnicity',$this->ethnicity,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('certifications',$this->certifications,true);
		$criteria->compare('resume',$this->resume,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('current_location',$this->current_location,true);
		$criteria->compare('willing_relocate',$this->willing_relocate,true);
		$criteria->compare('experience',$this->experience,true);
		$criteria->compare('emp_start_date',$this->emp_start_date,true);
		$criteria->compare('emp_msp_account_mngr',$this->emp_msp_account_mngr);
		$criteria->compare('emp_client_account_mngr',$this->emp_client_account_mngr);
		$criteria->compare('emp_time_sheet_aprover',$this->emp_time_sheet_aprover);
		$criteria->compare('emp_user_name',$this->emp_user_name,true);
		$criteria->compare('emp_password',$this->emp_password,true);
		$criteria->compare('emp_location',$this->emp_location,true);
		$criteria->compare('emp_official_email',$this->emp_official_email,true);
		$criteria->compare('profile_status',$this->profile_status);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Candidates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
