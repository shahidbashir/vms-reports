<?php

/**
 * This is the model class for table "vms_generated_invoice".
 *
 * The followings are the available columns in table 'vms_generated_invoice':
 * @property string $id
 * @property string $client_id
 * @property string $client_month
 * @property string $client_location
 * @property string $timsheet_id
 * @property string $job_id
 * @property string $candidate_id
 * @property string $vendor_id
 * @property string $invoice_start_date
 * @property string $invoice_end_date
 * @property string $total_regular_hours
 * @property string $total_overtime_hours
 * @property string $total_doubletime_hours
 * @property string $total_hours
 * @property string $total_billrate
 * @property integer $status
 * @property string $date_created
 */
class GeneratedInvoice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_generated_invoice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, client_month, client_location, timsheet_id, job_id, candidate_id, vendor_id, invoice_start_date, invoice_end_date, total_regular_hours, total_overtime_hours, total_doubletime_hours, total_hours, total_billrate, status, date_created,total_bilrate_with_tax', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('client_id, client_location, timsheet_id, job_id, candidate_id, vendor_id', 'length', 'max'=>20),
			array('client_month', 'length', 'max'=>30),
			array('total_regular_hours, total_overtime_hours, total_doubletime_hours, total_hours, total_billrate', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, client_month, client_location, timsheet_id, job_id, candidate_id, vendor_id, invoice_start_date, invoice_end_date, total_regular_hours, total_overtime_hours, total_doubletime_hours, total_hours, total_billrate, status, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'client_month' => 'Client Month',
			'client_location' => 'Client Location',
			'timsheet_id' => 'Timsheet',
			'job_id' => 'Job',
			'candidate_id' => 'Candidate',
			'vendor_id' => 'Vendor',
			'invoice_start_date' => 'Invoice Start Date',
			'invoice_end_date' => 'Invoice End Date',
			'total_regular_hours' => 'Total Regular Hours',
			'total_overtime_hours' => 'Total Overtime Hours',
			'total_doubletime_hours' => 'Total Doubletime Hours',
			'total_hours' => 'Total Hours',
			'total_billrate' => 'Total Billrate',
			'status' => 'Status',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('client_month',$this->client_month,true);
		$criteria->compare('client_location',$this->client_location,true);
		$criteria->compare('timsheet_id',$this->timsheet_id,true);
		$criteria->compare('job_id',$this->job_id,true);
		$criteria->compare('candidate_id',$this->candidate_id,true);
		$criteria->compare('vendor_id',$this->vendor_id,true);
		$criteria->compare('invoice_start_date',$this->invoice_start_date,true);
		$criteria->compare('invoice_end_date',$this->invoice_end_date,true);
		$criteria->compare('total_regular_hours',$this->total_regular_hours,true);
		$criteria->compare('total_overtime_hours',$this->total_overtime_hours,true);
		$criteria->compare('total_doubletime_hours',$this->total_doubletime_hours,true);
		$criteria->compare('total_hours',$this->total_hours,true);
		$criteria->compare('total_billrate',$this->total_billrate,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GeneratedInvoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
