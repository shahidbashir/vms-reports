<?php
class Job extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_job';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('title', 'required'),
			array('parent_id,cat_id,job_po_duration_endDate,jobstep2_complete, user_type, user_id, skills, location, num_openings, experience, type, description, payment_type, invite_team_member, jobStatus, number_submission, labour_cat, request_dept, job_po_duration, desired_start_date, shift, hours_per_week, background_verification,status_posting_access,skills_notes,skills1,skills2,billing_code,markup,pay_rate_candidate,double_time,over_time,job_level,markup_per_unit_hour,pay_rate_candidate_value,bill_rate,pay_rate,markup,qualification,you_will,add_info,pre_name,pre_candidate,pre_supplier_name,pre_current_rate,pre_payment_type,pre_reference_code,pre_total_estimate_code,work_flow,date_created,client_name,bell_notification','safe'),
			
			//array('job_po_duration_endDate','compare','compareAttribute'=>'job_po_duration','operator'=>'>', 'allowEmpty'=>false , 'message'=>'{attribute} must be greater than "{compareValue}".'),
			
			//validating date fields
			array('job_po_duration_endDate', 'validateTags'),
			
			array('number_submission', 'numerical', 'integerOnly'=>true),
			array('title, location', 'length', 'max'=>255),
			array('cat_id, user_type, user_id, job_po_duration', 'length', 'max'=>55),
			array('experience, type, payment_type, jobStatus, labour_cat, request_dept, shift', 'length', 'max'=>111),
			array('hours_per_week, background_verification', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, cat_id, user_type, user_id, skills, location, num_openings, experience, type, description, payment_type, invite_team_member, jobStatus, number_submission, labour_cat, request_dept, job_po_duration, desired_start_date, shift, hours_per_week, background_verification', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	public function validateTags($attribute,$params)
        {
			if(strtotime($this->job_po_duration_endDate) < strtotime($this->job_po_duration)){
				$this->addError($attribute,'End Date should be greater then '.$this->job_po_duration,'');
			}
        }
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'cat_id' => 'Cat',
			'user_type' => 'User Type',
			'user_id' => 'User',
			'skills' => 'Skills',
			'location' => 'Location',
			'num_openings' => 'Num Openings',
			'experience' => 'Experience',
			'type' => 'Type',
			'description' => 'Description',
			'payment_type' => 'Payment Type',
			'invite_team_member' => 'Invite Team Member',
			'jobStatus' => 'Job Status',
			'number_submission' => 'Number Submission',
			'labour_cat' => 'Labout Cat',
			'request_dept' => 'Request Dept',
			'job_po_duration' => 'Job Start Date',
			'job_po_duration_endDate' => 'Job End Date',
			'desired_start_date' => 'Desired Start Date',
			'shift' => 'Shift',
			'hours_per_week' => 'Hours Per Week',
			'background_verification' => 'Background Verification',
		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('cat_id',$this->cat_id,true);
		$criteria->compare('user_type',$this->user_type,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('skills',$this->skills,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('num_openings',$this->num_openings);
		$criteria->compare('experience',$this->experience,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('invite_team_member',$this->invite_team_member,true);
		$criteria->compare('jobStatus',$this->jobStatus,true);
		$criteria->compare('number_submission',$this->number_submission);
		$criteria->compare('labour_cat',$this->labour_cat,true);
		$criteria->compare('request_dept',$this->request_dept,true);
		$criteria->compare('job_po_duration',$this->job_po_duration,true);
		$criteria->compare('desired_start_date',$this->desired_start_date,true);
		$criteria->compare('shift',$this->shift,true);
		$criteria->compare('hours_per_week',$this->hours_per_week,true);
		$criteria->compare('background_verification',$this->background_verification,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/*public function beforeSave(){
		if($this->job_po_duration_endDate >= $this->job_po_duration_endDate){
			
			}
	}*/
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Job the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
