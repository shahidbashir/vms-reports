<?php
/**
 * This is the model class for table "vms_interview".
 *
 * The followings are the available columns in table 'vms_interview':
 * @property integer $id
 * @property integer $submission_id
 * @property string $rejected_by_type
 * @property string $rejected_by_id
 * @property string $rejection_reason
 */
class Interview extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_interview';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('submission_id, rejected_by_type, rejected_by_id, rejection_reason', 'required'),
			array('submission_id', 'required'),
			//array('status,reason_rejection,offer,rating,notes,date_created,created_by_type,created_by_id','safe'),
			
			array('status,reason_rejection,offer,rating,notes,date_created,created_by_type,created_by_id,rejected_by,rejected_type,interviewer,vendor_note
   ,MSP_note,client_note,start_date,rate,vendor_estimate_cost,estimate_start_date,pay_rate,worked_with_client,job_type,job_notes,type,interview_type,
   hiring_score,interview_event_name,interview_start_date,interview_start_date_time,interview_end_date,interview_end_date_time,start_date_status,interview_where,
   interview_description,interview_attachment,interview_added_guests,interview_alt1_start_date,interview_alt1_start_date_time,interview_alt1_end_date,interview_alt1_end_date_time,
   alt1_date_status,interview_alt2_start_date,interview_alt2_start_date_time,interview_alt2_end_date,interview_alt2_end_date_time,alt2_date_status,interview_alt3_start_date,
   interview_alt3_start_date_time,interview_alt3_end_date,interview_alt3_end_date_time,alt3_date_status,','safe'),
   
   
   //new fields added for feedback of interview
   array('edu_background, edu_comments, prior_w_exp, prior_w_exp_comments, tech_work_exp, tech_work_exp_comments, verbal_communication, verbal_communication_comments, can_enthusiasm, can_enthusiasm_comments, knowledge_company, knowledge_company_notes, inter_personal_skills, inter_personal_skills_comments, initiative, initiative_comments, time_management,  time_management_comments, customer_service, customer_service_comments, overall_impression, overall_impression_comments, assessment_documents, rating_notes, feedback_status','safe'),
			
			
			array('submission_id', 'numerical', 'integerOnly'=>true),
			array('submission_id', 'numerical', 'integerOnly'=>true),
			array('rejected_by_type, rejected_by_id', 'length', 'max'=>111),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, submission_id, rejected_by_type, rejected_by_id, rejection_reason', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'submission_id' => 'Submission',
			'rejected_by_type' => 'Rejected By Type',
			'rejected_by_id' => 'Rejected By',
			'rejection_reason' => 'Rejection Reason',
		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('submission_id',$this->submission_id);
		$criteria->compare('rejected_by_type',$this->rejected_by_type,true);
		$criteria->compare('rejected_by_id',$this->rejected_by_id,true);
		$criteria->compare('rejection_reason',$this->rejection_reason,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Interview the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
