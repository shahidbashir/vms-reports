<?php

/**
 * This is the model class for table "vms_contract_extension_req".
 *
 * The followings are the available columns in table 'vms_contract_extension_req':
 * @property integer $id
 * @property integer $contract_id
 * @property integer $reason_of_extension
 * @property string $note_of_extension
 * @property string $new_contract_end_date
 * @property string $pay_rate
 * @property string $bill_rate
 * @property string $overtime_payrate
 * @property string $doubletime_payrate
 * @property string $new_estimate_cost
 * @property string $date_created
 */
class ContractExtensionReq extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_contract_extension_req';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contract_id, reason_of_extension, note_of_extension, new_contract_end_date, pay_rate, bill_rate, overtime_payrate, doubletime_payrate, new_estimate_cost,contract_work_flow, date_created', 'required'),
			array('contract_id, reason_of_extension', 'numerical', 'integerOnly'=>true),
			array('pay_rate, bill_rate, overtime_payrate, doubletime_payrate, new_estimate_cost', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, contract_id, reason_of_extension, note_of_extension, new_contract_end_date, pay_rate, bill_rate, overtime_payrate, doubletime_payrate, new_estimate_cost, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contract_id' => 'Contract',
			'reason_of_extension' => 'Reason Of Rejection',
			'note_of_extension' => 'Note Of Rejection',
			'new_contract_end_date' => 'New Contract End Date',
			'pay_rate' => 'Pay Rate',
			'bill_rate' => 'Bill Rate',
			'overtime_payrate' => 'Overtime Payrate',
			'doubletime_payrate' => 'Doubletime Payrate',
			'new_estimate_cost' => 'New Estimate Cost',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contract_id',$this->contract_id);
		$criteria->compare('reason_of_extension',$this->reason_of_extension);
		$criteria->compare('note_of_extension',$this->note_of_extension,true);
		$criteria->compare('new_contract_end_date',$this->new_contract_end_date,true);
		$criteria->compare('pay_rate',$this->pay_rate,true);
		$criteria->compare('bill_rate',$this->bill_rate,true);
		$criteria->compare('overtime_payrate',$this->overtime_payrate,true);
		$criteria->compare('doubletime_payrate',$this->doubletime_payrate,true);
		$criteria->compare('new_estimate_cost',$this->new_estimate_cost,true);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContractExtensionReq the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
