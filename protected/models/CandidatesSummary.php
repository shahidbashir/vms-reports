<?php

/**
 * This is the model class for table "vms_candidates_summary".
 *
 * The followings are the available columns in table 'vms_candidates_summary':
 * @property integer $id
 * @property integer $candidate_id
 * @property integer $profile_id
 * @property string $summary
 * @property string $experience_Level
 * @property string $willing_to_relocate
 * @property string $current_location
 * @property string $reason_for_change
 * @property string $date_created
 * @property string $date_updated
 */
class CandidatesSummary extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_candidates_summary';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('candidate_id, profile_id, summary, experience_Level, willing_to_relocate, current_location, reason_for_change, date_created, date_updated', 'safe'),
			array('candidate_id, profile_id', 'numerical', 'integerOnly'=>true),
			array('experience_Level', 'length', 'max'=>20),
			array('willing_to_relocate', 'length', 'max'=>5),
			array('current_location', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, candidate_id, profile_id, summary, experience_Level, willing_to_relocate, current_location, reason_for_change, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'candidate_id' => 'Candidate',
			'profile_id' => 'Profile',
			'summary' => 'Summary',
			'experience_Level' => 'Experience Level',
			'willing_to_relocate' => 'Willing To Relocate',
			'current_location' => 'Current Location',
			'reason_for_change' => 'Reason For Change',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('experience_Level',$this->experience_Level,true);
		$criteria->compare('willing_to_relocate',$this->willing_to_relocate,true);
		$criteria->compare('current_location',$this->current_location,true);
		$criteria->compare('reason_for_change',$this->reason_for_change,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CandidatesSummary the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
