<?php

/**
 * This is the model class for table "vms_job_comments".
 *
 * The followings are the available columns in table 'vms_job_comments':
 * @property integer $id
 * @property integer $job_id
 * @property integer $user_id
 * @property string $user_type
 * @property string $user_name
 * @property string $comments
 * @property string $attachment
 * @property integer $show_msp
 * @property integer $date_created
 */
class JobComments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_job_comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('job_id, user_id, user_type, user_name, comments, attachment, date_created', 'required'),
			array('show_msp,show_vendor','safe'),
			array('job_id, user_id, show_msp, date_created', 'numerical', 'integerOnly'=>true),
			array('user_type, user_name, attachment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, job_id, user_id, user_type, user_name, comments, attachment, show_msp, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'job_id' => 'Job',
			'user_id' => 'User',
			'user_type' => 'User Type',
			'user_name' => 'User Name',
			'comments' => 'Comments',
			'attachment' => 'Attachment',
			'show_msp' => 'Show Msp',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_type',$this->user_type,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('attachment',$this->attachment,true);
		$criteria->compare('show_msp',$this->show_msp);
		$criteria->compare('date_created',$this->date_created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JobComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
