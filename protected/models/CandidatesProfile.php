<?php

/**
 * This is the model class for table "vms_candidates_profile".
 *
 * The followings are the available columns in table 'vms_candidates_profile':
 * @property integer $id
 * @property integer $candidate_id
 * @property string $title
 * @property integer $created_by_id
 * @property string $created_by_type
 * @property string $rate
 * @property string $crrency
 * @property string $rate_type
 * @property string $resume
 * @property string $date_created
 * @property string $date_updated
 */
class CandidatesProfile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_candidates_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			

			array('candidate_id, title, created_by_id, created_by_type, rate, crrency, rate_type, resume, date_created, date_updated', 'safe'),

			array('candidate_id, created_by_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('created_by_type, rate, crrency, rate_type', 'length', 'max'=>10),
			array('resume', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, candidate_id, title, created_by_id, created_by_type, rate, crrency, rate_type, resume, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'candidate_id' => 'Candidate',
			'title' => 'Title',
			'created_by_id' => 'Created By',
			'created_by_type' => 'Created By Type',
			'rate' => 'Rate',
			'crrency' => 'Crrency',
			'rate_type' => 'Rate Type',
			'resume' => 'Resume',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('created_by_id',$this->created_by_id);
		$criteria->compare('created_by_type',$this->created_by_type,true);
		$criteria->compare('rate',$this->rate,true);
		$criteria->compare('crrency',$this->crrency,true);
		$criteria->compare('rate_type',$this->rate_type,true);
		$criteria->compare('resume',$this->resume,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CandidatesProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
