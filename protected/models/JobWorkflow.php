<?php

/**
 * This is the model class for table "vms_job_workflow".
 *
 * The followings are the available columns in table 'vms_job_workflow':
 * @property integer $id
 * @property integer $job_id
 * @property integer $workflow_id
 * @property integer $client_id
 * @property integer $number_of_approval
 * @property string $job_status
 */
class JobWorkflow extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_job_workflow';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('job_id, workflow_id, client_id, number_of_approval, job_status', 'required'),
			array('email_sent,rejection_reason,rejection_dropdown,status_time','safe'),
			array('job_id, workflow_id, client_id, number_of_approval', 'numerical', 'integerOnly'=>true),
			array('job_status', 'length', 'max'=>55),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, job_id, workflow_id, client_id, number_of_approval, job_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'job_id' => 'Job',
			'workflow_id' => 'Workflow',
			'client_id' => 'Client',
			'number_of_approval' => 'Number Of Approval',
			'job_status' => 'Job Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('workflow_id',$this->workflow_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('number_of_approval',$this->number_of_approval);
		$criteria->compare('job_status',$this->job_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JobWorkflow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
