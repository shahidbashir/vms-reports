<?php

/**
 * This is the model class for table "vms_interview_comments".
 *
 * The followings are the available columns in table 'vms_interview_comments':
 * @property integer $id
 * @property integer $user_id
 * @property integer $interview_id
 * @property integer $submission_id
 * @property integer $job_id
 * @property string $user_type
 * @property string $user_name
 * @property string $comments
 * @property string $file_name
 * @property string $posted_date
 */
class InterviewComments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_interview_comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, interview_id, submission_id, job_id, user_type, user_name, comments, file_name, posted_date', 'required'),
			array('user_id, interview_id, submission_id, job_id', 'numerical', 'integerOnly'=>true),
			array('user_type, user_name, file_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, interview_id, submission_id, job_id, user_type, user_name, comments, file_name, posted_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'interview_id' => 'Interview',
			'submission_id' => 'Submission',
			'job_id' => 'Job',
			'user_type' => 'User Type',
			'user_name' => 'User Name',
			'comments' => 'Comments',
			'file_name' => 'File Name',
			'posted_date' => 'Posted Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('interview_id',$this->interview_id);
		$criteria->compare('submission_id',$this->submission_id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('user_type',$this->user_type,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('posted_date',$this->posted_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InterviewComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
