<?php

class CpTimesheet extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cp_timesheet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contract_id, workorder_id, offer_id, candidate_id, client_id, approval_manager, client_config_type,invoice_end_date,invoice_start_date,invoice_duration, creation_week_number, creation_year, total_regular_hours, total_overtime_hours, total_doubletime_hours, total_hours, regular_payrate, total_regular_payrate, overtime_payrate, total_overtime_payrate, doubletime_payrate, total_doubletime_payrate, total_payrate, regular_billrate, total_regular_billrate, overtime_billrate, total_overtime_billrate, doubletime_billrate, total_doubletime_billrate, total_billrate, reason_of_rejection, rejection_notes, rejection_date_time, approve_date_time, timesheet_status, timesheet_sub_status, date_created, date_updated', 'required'),
			array('contract_id, workorder_id, offer_id, candidate_id, client_id, client_config_type, creation_week_number, creation_year, total_regular_hours, total_overtime_hours, total_doubletime_hours, total_hours, timesheet_status, timesheet_sub_status', 'numerical', 'integerOnly'=>true),
			array('total_regular_payrate, total_overtime_payrate, total_doubletime_payrate, total_payrate, total_regular_billrate, total_overtime_billrate, total_doubletime_billrate, total_billrate', 'length', 'max'=>10),
			array('reason_of_rejection', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, contract_id, workorder_id, offer_id, candidate_id, client_id, client_config_type, creation_week_number, creation_year, total_regular_hours, total_overtime_hours, total_doubletime_hours, total_hours, total_regular_payrate, total_overtime_payrate, total_doubletime_payrate, total_payrate, total_regular_billrate, total_overtime_billrate, total_doubletime_billrate, total_billrate, reason_of_rejection, rejection_notes, rejection_date_time, approve_date_time, timesheet_status, timesheet_sub_status, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contract_id' => 'Contract',
			'workorder_id' => 'Workorder',
			'offer_id' => 'Offer',
			'candidate_id' => 'Candidate',
			'client_id' => 'Client',
			'client_config_type' => 'Client Config Type',
			'creation_week_number' => 'Creation Week Number',
			'creation_year' => 'Creation Year',
			'total_regular_hours' => 'Total Regular Hours',
			'total_overtime_hours' => 'Total Overtime Hours',
			'total_doubletime_hours' => 'Total Doubletime Hours',
			'total_hours' => 'Total Hours',
			'total_regular_payrate' => 'Total Regular Payrate',
			'total_overtime_payrate' => 'Total Overtime Payrate',
			'total_doubletime_payrate' => 'Total Doubltime Payrate',
			'total_payrate' => 'Total Payrate',
			'total_regular_billrate' => 'Total Regular Billrate',
			'total_overtime_billrate' => 'Total Overtime Billrate',
			'total_doubletime_billrate' => 'Total Doubletime Billrate',
			'total_billrate' => 'Total Billrate',
			'reason_of_rejection' => 'Reason Of Rejection',
			'rejection_notes' => 'Rejection Notes',
			'rejection_date_time' => 'Rejection Date Time',
			'approve_date_time' => 'Approve Date Time',
			'timesheet_status' => 'Timesheet Status',
			'timesheet_sub_status' => 'Timesheet Sub Status',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contract_id',$this->contract_id);
		$criteria->compare('workorder_id',$this->workorder_id);
		$criteria->compare('offer_id',$this->offer_id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('client_config_type',$this->client_config_type);
		$criteria->compare('creation_week_number',$this->creation_week_number);
		$criteria->compare('creation_year',$this->creation_year);
		$criteria->compare('total_regular_hours',$this->total_regular_hours);
		$criteria->compare('total_overtime_hours',$this->total_overtime_hours);
		$criteria->compare('total_doubletime_hours',$this->total_doubletime_hours);
		$criteria->compare('total_hours',$this->total_hours);
		$criteria->compare('total_regular_payrate',$this->total_regular_payrate,true);
		$criteria->compare('total_overtime_payrate',$this->total_overtime_payrate,true);
		$criteria->compare('total_doubletime_payrate',$this->total_doubletime_payrate,true);
		$criteria->compare('total_payrate',$this->total_payrate,true);
		$criteria->compare('total_regular_billrate',$this->total_regular_billrate,true);
		$criteria->compare('total_overtime_billrate',$this->total_overtime_billrate,true);
		$criteria->compare('total_doubletime_billrate',$this->total_doubletime_billrate,true);
		$criteria->compare('total_billrate',$this->total_billrate,true);
		$criteria->compare('reason_of_rejection',$this->reason_of_rejection,true);
		$criteria->compare('rejection_notes',$this->rejection_notes,true);
		$criteria->compare('rejection_date_time',$this->rejection_date_time,true);
		$criteria->compare('approve_date_time',$this->approve_date_time,true);
		$criteria->compare('timesheet_status',$this->timesheet_status);
		$criteria->compare('timesheet_sub_status',$this->timesheet_sub_status);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CpTimesheet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
