<?php

/**
 * This is the model class for table "vms_contract_extension_approval".
 *
 * The followings are the available columns in table 'vms_contract_extension_approval':
 * @property integer $id
 * @property integer $extension_id
 * @property integer $workflow_id
 * @property integer $client_id
 * @property integer $approval_order
 * @property integer $status
 * @property string $apprej_date
 * @property integer $rejection_dropdpwn
 * @property string $rejection_reason
 */
class ContractExtensionApproval extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_contract_extension_approval';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, extension_id, workflow_id, client_id,contract_id, approval_order, status, apprej_date, rejection_dropdpwn, rejection_reason', 'required'),
			array('id, extension_id, workflow_id, client_id, approval_order, status, rejection_dropdpwn', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, extension_id, workflow_id, client_id, approval_order, status, apprej_date, rejection_dropdpwn, rejection_reason', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'extension_id' => 'Extension',
			'workflow_id' => 'Workflow',
			'client_id' => 'Client',
			'approval_order' => 'Approval Order',
			'status' => 'Status',
			'apprej_date' => 'Apprej Date',
			'rejection_dropdpwn' => 'Rejection Dropdpwn',
			'rejection_reason' => 'Rejection Reason',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('extension_id',$this->extension_id);
		$criteria->compare('workflow_id',$this->workflow_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('approval_order',$this->approval_order);
		$criteria->compare('status',$this->status);
		$criteria->compare('apprej_date',$this->apprej_date,true);
		$criteria->compare('rejection_dropdpwn',$this->rejection_dropdpwn);
		$criteria->compare('rejection_reason',$this->rejection_reason,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContractExtensionApproval the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
