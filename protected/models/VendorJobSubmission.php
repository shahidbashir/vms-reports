<?php

/**
 * This is the model class for table "vms_vendor_job_submission".
 *
 * The followings are the available columns in table 'vms_vendor_job_submission':
 * @property integer $id
 * @property integer $vendor_id
 * @property string $client_id
 * @property integer $candidate_Id
 * @property integer $job_id
 * @property string $recommended_pay_rate
 * @property string $candidate_pay_rate
 * @property string $over_time_rate
 * @property string $double_time_rate
 * @property string $resume_status
 * @property integer $rejected_by
 * @property string $rejected_type
 * @property string $notes
 * @property string $estimate_start_date
 */
class VendorJobSubmission extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_vendor_job_submission';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vendor_id, vendor_bill_rate, client_id, markup, candidate_Id, job_id, recommended_pay_rate, candidate_pay_rate, over_time_rate, double_time_rate, resume_status, rejected_by, rejected_type, notes, estimate_start_date,date_created', 'safe'),
			array('vendor_id, candidate_Id, job_id, rejected_by', 'numerical', 'integerOnly'=>true),
			array('client_id', 'length', 'max'=>20),
			array('recommended_pay_rate, candidate_pay_rate, over_time_rate, double_time_rate', 'length', 'max'=>10),
			array('resume_status', 'length', 'max'=>1),
			array('rejected_type', 'length', 'max'=>111),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, vendor_id, client_id, candidate_Id, job_id, recommended_pay_rate, candidate_pay_rate, over_time_rate, double_time_rate, resume_status, rejected_by, rejected_type, notes, estimate_start_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vendor_id' => 'Vendor',
			'client_id' => 'Client',
			'candidate_Id' => 'Candidate',
			'job_id' => 'Job',
			'recommended_pay_rate' => 'Recommended Pay Rate',
			'candidate_pay_rate' => 'Candidate Pay Rate',
			'over_time_rate' => 'Over Time Rate',
			'double_time_rate' => 'Double Time Rate',
			'resume_status' => 'Resume Status',
			'rejected_by' => 'Rejected By',
			'rejected_type' => 'Rejected Type',
			'notes' => 'Notes',
			'estimate_start_date' => 'Estimate Start Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('candidate_Id',$this->candidate_Id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('recommended_pay_rate',$this->recommended_pay_rate,true);
		$criteria->compare('candidate_pay_rate',$this->candidate_pay_rate,true);
		$criteria->compare('over_time_rate',$this->over_time_rate,true);
		$criteria->compare('double_time_rate',$this->double_time_rate,true);
		$criteria->compare('resume_status',$this->resume_status,true);
		$criteria->compare('rejected_by',$this->rejected_by);
		$criteria->compare('rejected_type',$this->rejected_type,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('estimate_start_date',$this->estimate_start_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VendorJobSubmission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
