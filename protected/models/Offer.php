<?php

/**
 * This is the model class for table "vms_offer".
 **/
class Offer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_offer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
 		return array(
			array('submission_id, job_id, offer_digital_doc,client_id, vendor_id, candidate_id, created_by_id, created_by_type, modified_by_id, modified_by_type, status, approver_manager_location, location_manager, offer_accept_date, on_board_status, background_status, exhibit_status, offer_pay_rate, offer_bill_rate, payment_type, shift, hours_per_week, over_time, double_time, client_over_time, client_double_time, estimate_cost, job_start_date, job_end_date, internal_notes, for_admin_notes, ref_notes_vendor, issued_offer_date, valid_till_date, ip, reason_for_rejection, notes, release_to_vendor, date_modified,verification_status,exhibit_status,hellosign_api_email,hellosign_api_req_id,hellosign_api_sign_status,hellosign_req_date, offer_digital_doc,hellosign_signature_id', 'safe'),
			array('submission_id, job_id, candidate_id, created_by_id, modified_by_id, status, approver_manager_location, on_board_status, background_status, exhibit_status, release_to_vendor', 'numerical', 'integerOnly'=>true),
			array('client_id, vendor_id', 'length', 'max'=>20),
			array('created_by_type, modified_by_type, offer_pay_rate', 'length', 'max'=>25),
			array('location_manager, payment_type, shift, hours_per_week', 'length', 'max'=>111),
			array('offer_bill_rate, over_time, double_time, client_over_time, client_double_time', 'length', 'max'=>10),
			array('estimate_cost', 'length', 'max'=>11),
			array('ip, reason_for_rejection', 'length', 'max'=>30),
			array('date_created,bell_notification', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, submission_id, job_id, client_id, vendor_id, candidate_id, created_by_id, created_by_type, modified_by_id, modified_by_type, status, approver_manager_location, location_manager, offer_accept_date, on_board_status, background_status, exhibit_status, offer_pay_rate, offer_bill_rate, payment_type, shift, hours_per_week, over_time, double_time, client_over_time, client_double_time, estimate_cost, job_start_date, job_end_date, internal_notes, for_admin_notes, ref_notes_vendor, issued_offer_date, valid_till_date, ip, reason_for_rejection, notes, release_to_vendor, date_created, date_modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'submission_id' => 'Submission',
			'job_id' => 'Job',
			'client_id' => 'Client',
			'vendor_id' => 'Vendor',
			'candidate_id' => 'Candidate',
			'created_by_id' => 'Created By',
			'created_by_type' => 'Created By Type',
			'modified_by_id' => 'Modified By',
			'modified_by_type' => 'Modified By Type',
			'status' => 'Status',
			'approver_manager_location' => 'Approver Manager Location',
			'location_manager' => 'Location Manager',
			'offer_accept_date' => 'Offer Accept Date',
			'on_board_status' => 'On Board Status',
			'background_status' => 'Background Status',
			'exhibit_status' => 'Exhibit Status',
			'offer_pay_rate' => 'Offer Pay Rate',
			'offer_bill_rate' => 'Offer Bill Rate',
			'payment_type' => 'Payment Type',
			'shift' => 'Shift',
			'hours_per_week' => 'Hours Per Week',
			'over_time' => 'Over Time',
			'double_time' => 'Double Time',
			'client_over_time' => 'Client Over Time',
			'client_double_time' => 'Client Double Time',
			'estimate_cost' => 'Estimate Cost',
			'job_start_date' => 'Job Start Date',
			'job_end_date' => 'Job End Date',
			'internal_notes' => 'Internal Notes',
			'for_admin_notes' => 'For Admin Notes',
			'ref_notes_vendor' => 'Ref Notes Vendor',
			'issued_offer_date' => 'Issued Offer Date',
			'valid_till_date' => 'Valid Till Date',
			'ip' => 'Ip',
			'reason_for_rejection' => 'Reason For Rejection',
			'notes' => 'Notes',
			'release_to_vendor' => 'Release To Vendor',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'offer_digital_doc' => 'Digital Doc'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('submission_id',$this->submission_id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('vendor_id',$this->vendor_id,true);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('created_by_id',$this->created_by_id);
		$criteria->compare('created_by_type',$this->created_by_type,true);
		$criteria->compare('modified_by_id',$this->modified_by_id);
		$criteria->compare('modified_by_type',$this->modified_by_type,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('approver_manager_location',$this->approver_manager_location);
		$criteria->compare('location_manager',$this->location_manager,true);
		$criteria->compare('offer_accept_date',$this->offer_accept_date,true);
		$criteria->compare('on_board_status',$this->on_board_status);
		$criteria->compare('background_status',$this->background_status);
		$criteria->compare('exhibit_status',$this->exhibit_status);
		$criteria->compare('offer_pay_rate',$this->offer_pay_rate,true);
		$criteria->compare('offer_bill_rate',$this->offer_bill_rate,true);
		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('shift',$this->shift,true);
		$criteria->compare('hours_per_week',$this->hours_per_week,true);
		$criteria->compare('over_time',$this->over_time,true);
		$criteria->compare('double_time',$this->double_time,true);
		$criteria->compare('client_over_time',$this->client_over_time,true);
		$criteria->compare('client_double_time',$this->client_double_time,true);
		$criteria->compare('estimate_cost',$this->estimate_cost,true);
		$criteria->compare('job_start_date',$this->job_start_date,true);
		$criteria->compare('job_end_date',$this->job_end_date,true);
		$criteria->compare('internal_notes',$this->internal_notes,true);
		$criteria->compare('for_admin_notes',$this->for_admin_notes,true);
		$criteria->compare('ref_notes_vendor',$this->ref_notes_vendor,true);
		$criteria->compare('issued_offer_date',$this->issued_offer_date,true);
		$criteria->compare('valid_till_date',$this->valid_till_date,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('reason_for_rejection',$this->reason_for_rejection,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('release_to_vendor',$this->release_to_vendor);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Offer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
