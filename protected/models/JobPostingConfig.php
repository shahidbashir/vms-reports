<?php

/**
 * This is the model class for table "vms_job_posting_config".
 *
 * The followings are the available columns in table 'vms_job_posting_config':
 * @property integer $id
 * @property integer $super_client_id
 * @property integer $created_by_id
 * @property integer $teammember_id
 * @property integer $cat_id
 * @property string $date_created
 */
class JobPostingConfig extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_job_posting_config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('super_client_id, created_by_id, teammember_id, cat_id, date_created', 'required'),
			array('super_client_id, created_by_id, teammember_id, cat_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, super_client_id, created_by_id, teammember_id, cat_id, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'super_client_id' => 'Super Client',
			'created_by_id' => 'Created By',
			'teammember_id' => 'Teammember',
			'cat_id' => 'Cat',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('super_client_id',$this->super_client_id);
		$criteria->compare('created_by_id',$this->created_by_id);
		$criteria->compare('teammember_id',$this->teammember_id);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JobPostingConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
