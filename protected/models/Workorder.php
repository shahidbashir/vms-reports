<?php

/**
 * This is the model class for table "vms_workorder".
 *
 * The followings are the available columns in table 'vms_workorder':
 * @property integer $id
 * @property integer $offer_id
 * @property integer $submission_id
 * @property integer $job_id
 * @property integer $client_id
 * @property integer $vendor_id
 * @property integer $candidate_id
 * @property string $wo_start_date
 * @property string $wo_end_date
 * @property string $wo_cost_center
 * @property string $wo_timesheet_code
 * @property string $wo_project
 * @property string $wo_bill_rate
 * @property string $wo_pay_rate
 * @property string $wo_over_time
 * @property string $wo_double_time
 * @property string $wo_client_over_time
 * @property string $wo_client_double_time
 * @property integer $approval_manager
 * @property string $wo_hiring_manager
 * @property string $wo_internal_notes
 * @property string $wo_msp_notes
 * @property string $wo_vendor_notes
 * @property integer $workorder_status
 * @property string $reason_rejection_workorder
 * @property string $release_to_vendor
 * @property integer $wo_form_submitted
 * @property string $date_created
 */
class Workorder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_workorder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('offer_id,created_by_id, submission_id, job_id, client_id, vendor_id, candidate_id, wo_start_date, onboard_changed_start_date, onboard_changed_end_date, wo_end_date, wo_cost_center, wo_timesheet_code, wo_project, wo_bill_rate, wo_pay_rate, wo_over_time, wo_double_time, wo_client_over_time, wo_client_double_time, approval_manager, wo_hiring_manager, wo_internal_notes, wo_msp_notes, wo_vendor_notes, workorder_status, reason_rejection_workorder,rejection_notes, release_to_vendor, wo_form_submitted, on_board_status, date_created,accept_date,rejection_date,modified_by_id,modified_by_type,bell_notification', 'safe'),
			array('offer_id, submission_id, job_id, client_id, vendor_id, candidate_id, approval_manager, workorder_status, wo_form_submitted', 'numerical', 'integerOnly'=>true),
			array('wo_cost_center, wo_timesheet_code, wo_project, reason_rejection_workorder', 'length', 'max'=>255),
			array('wo_bill_rate, wo_pay_rate, wo_over_time, wo_double_time, wo_client_over_time, wo_client_double_time', 'length', 'max'=>10),
			array('wo_hiring_manager', 'length', 'max'=>111),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, offer_id, submission_id, job_id, client_id, vendor_id, candidate_id, wo_start_date, wo_end_date, wo_cost_center, wo_timesheet_code, wo_project, wo_bill_rate, wo_pay_rate, wo_over_time, wo_double_time, wo_client_over_time, wo_client_double_time, approval_manager, wo_hiring_manager, wo_internal_notes, wo_msp_notes, wo_vendor_notes, workorder_status, reason_rejection_workorder, release_to_vendor, wo_form_submitted, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'offer_id' => 'Offer',
			'submission_id' => 'Submission',
			'job_id' => 'Job',
			'client_id' => 'Client',
			'vendor_id' => 'Vendor',
			'candidate_id' => 'Candidate',
			'wo_start_date' => 'Wo Start Date',
			'wo_end_date' => 'Wo End Date',
			'wo_cost_center' => 'Wo Cost Center',
			'wo_timesheet_code' => 'Wo Timesheet Code',
			'wo_project' => 'Wo Project',
			'wo_bill_rate' => 'Wo Bill Rate',
			'wo_pay_rate' => 'Wo Pay Rate',
			'wo_over_time' => 'Wo Over Time',
			'wo_double_time' => 'Wo Double Time',
			'wo_client_over_time' => 'Wo Client Over Time',
			'wo_client_double_time' => 'Wo Client Double Time',
			'approval_manager' => 'Approval Manager',
			'wo_hiring_manager' => 'Wo Hiring Manager',
			'wo_internal_notes' => 'Wo Internal Notes',
			'wo_msp_notes' => 'Wo Msp Notes',
			'wo_vendor_notes' => 'Wo Vendor Notes',
			'workorder_status' => 'Workorder Status',
			'reason_rejection_workorder' => 'Reason Rejection Workorder',
			'release_to_vendor' => 'Release To Vendor',
			'wo_form_submitted' => 'Wo Form Submitted',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('offer_id',$this->offer_id);
		$criteria->compare('submission_id',$this->submission_id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('wo_start_date',$this->wo_start_date,true);
		$criteria->compare('wo_end_date',$this->wo_end_date,true);
		$criteria->compare('wo_cost_center',$this->wo_cost_center,true);
		$criteria->compare('wo_timesheet_code',$this->wo_timesheet_code,true);
		$criteria->compare('wo_project',$this->wo_project,true);
		$criteria->compare('wo_bill_rate',$this->wo_bill_rate,true);
		$criteria->compare('wo_pay_rate',$this->wo_pay_rate,true);
		$criteria->compare('wo_over_time',$this->wo_over_time,true);
		$criteria->compare('wo_double_time',$this->wo_double_time,true);
		$criteria->compare('wo_client_over_time',$this->wo_client_over_time,true);
		$criteria->compare('wo_client_double_time',$this->wo_client_double_time,true);
		$criteria->compare('approval_manager',$this->approval_manager);
		$criteria->compare('wo_hiring_manager',$this->wo_hiring_manager,true);
		$criteria->compare('wo_internal_notes',$this->wo_internal_notes,true);
		$criteria->compare('wo_msp_notes',$this->wo_msp_notes,true);
		$criteria->compare('wo_vendor_notes',$this->wo_vendor_notes,true);
		$criteria->compare('workorder_status',$this->workorder_status);
		$criteria->compare('reason_rejection_workorder',$this->reason_rejection_workorder,true);
		$criteria->compare('release_to_vendor',$this->release_to_vendor,true);
		$criteria->compare('wo_form_submitted',$this->wo_form_submitted);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Workorder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
