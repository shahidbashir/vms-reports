<?php

/**
 * This is the model class for table "vms_candidates_test_scores".
 *
 * The followings are the available columns in table 'vms_candidates_test_scores':
 * @property integer $id
 * @property integer $candidate_id
 * @property integer $profile_id
 * @property string $date_created
 * @property string $date_updated
 * @property string $name
 * @property string $occupation
 * @property string $score
 * @property string $date
 * @property string $description
 */
class CandidatesTestScores extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_candidates_test_scores';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('candidate_id, profile_id, date_created, date_updated, name, occupation, score, date, description', 'safe'),
			array('candidate_id, profile_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('occupation', 'length', 'max'=>50),
			array('score', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, candidate_id, profile_id, date_created, date_updated, name, occupation, score, date, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'candidate_id' => 'Candidate',
			'profile_id' => 'Profile',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
			'name' => 'Name',
			'occupation' => 'Occupation',
			'score' => 'Score',
			'date' => 'Date',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('occupation',$this->occupation,true);
		$criteria->compare('score',$this->score,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CandidatesTestScores the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
