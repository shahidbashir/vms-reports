<?php

/**
 * This is the model class for table "vms_email_template".
 *
 * The followings are the available columns in table 'vms_email_template':
 * @property integer $id
 * @property string $from_email_ID
 * @property string $from_email
 * @property string $cc_email_ID
 * @property string $bcc_email_ID
 * @property string $subject_line
 * @property string $content
 * @property string $type
 */
class EmailTemplate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_email_template';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('from_email, subject_line, content', 'required'),
			array('from_email, cc_email_ID, bcc_email_ID', 'email'),
			array('from_email_ID, cc_email_ID, bcc_email_ID, type','safe'),
			array('from_email_ID, from_email, cc_email_ID, bcc_email_ID, type', 'length', 'max'=>100),
			array('subject_line', 'length', 'max'=>510),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, from_email_ID, from_email, cc_email_ID, bcc_email_ID, subject_line, content, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from_email_ID' => 'From Email',
			'from_email' => 'From Email',
			'cc_email_ID' => 'Cc Email',
			'bcc_email_ID' => 'Bcc Email',
			'subject_line' => 'Subject Line',
			'content' => 'Content',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('from_email_ID',$this->from_email_ID,true);
		$criteria->compare('from_email',$this->from_email,true);
		$criteria->compare('cc_email_ID',$this->cc_email_ID,true);
		$criteria->compare('bcc_email_ID',$this->bcc_email_ID,true);
		$criteria->compare('subject_line',$this->subject_line,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailTemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
