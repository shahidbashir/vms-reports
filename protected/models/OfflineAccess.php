<?php
class OfflineAccess extends CFormModel
{
     public function addEventToCalendar($event, $user) {
       /* $files = CFileHelper::findFiles(Yii::getPathOfAlias('webroot').'/protected/data/user-tokens');
        $results = array();

        if(!empty($files)) {
            foreach($files as $file) {*/
                //$token = oAuthService::getAccessTokenFromFile($file, $authorizeUrl);
                
                $payload = "{
                  'Subject': '".$event['subject']."',
                  'Body': {
                    'ContentType': 'HTML',
                    'Content': '".$event['description']."'
                  },
                  'Start': {
                    'DateTime': '".$event['start']."',
                    'TimeZone': 'Pacific Standard Time'
                  },
                  'End': {
                    'DateTime': '".$event['end']."',
                    'TimeZone': 'Pacific Standard Time'
                  }
                }";
                
          $decodedData = json_decode($user->outlook_access_token,true);
          $tokenArray = oAuthService::getTokenFromRefreshToken($decodedData['refresh_token']);
          $result = OutlookService::postEvents($tokenArray['access_token'],$user->outlook_email_address, $payload);
          
          return $result;
     }
}