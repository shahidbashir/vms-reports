<?php

/**
 * This is the model class for table "vms_invoice_client_vend".
 *
 * The followings are the available columns in table 'vms_invoice_client_vend':
 * @property string $id
 * @property string $client_id
 * @property string $created_by
 * @property string $created_by_type
 * @property string $client_percentage
 * @property string $vendor_percentage
 * @property string $discount_percentage
 * @property integer $payrol_cycle
 * @property string $timesheet_start_day
 * @property string $timesheet_end_day
 * @property string $sales_tax
 * @property string $county_tax
 * @property string $date_created
 * @property string $date_updated
 */
class InvoiceClientVend extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_invoice_client_vend';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, created_by, created_by_type, client_percentage, vendor_percentage, discount_percentage, payrol_cycle, timesheet_start_day, timesheet_end_day, sales_tax, county_tax, date_created, date_updated', 'required'),
			array('payrol_cycle', 'numerical', 'integerOnly'=>true),
			array('client_id, created_by', 'length', 'max'=>20),
			array('created_by_type', 'length', 'max'=>9),
			array('client_percentage, vendor_percentage, discount_percentage', 'length', 'max'=>10),
			array('timesheet_start_day, timesheet_end_day', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, created_by, created_by_type, client_percentage, vendor_percentage, discount_percentage, payrol_cycle, timesheet_start_day, timesheet_end_day, sales_tax, county_tax, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'created_by' => 'Created By',
			'created_by_type' => 'Created By Type',
			'client_percentage' => 'Client Percentage',
			'vendor_percentage' => 'Vendor Percentage',
			'discount_percentage' => 'Discount Percentage',
			'payrol_cycle' => 'Payrol Cycle',
			'timesheet_start_day' => 'Timesheet Start Day',
			'timesheet_end_day' => 'Timesheet End Day',
			'sales_tax' => 'Sales Tax',
			'county_tax' => 'County Tax',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_by_type',$this->created_by_type,true);
		$criteria->compare('client_percentage',$this->client_percentage,true);
		$criteria->compare('vendor_percentage',$this->vendor_percentage,true);
		$criteria->compare('discount_percentage',$this->discount_percentage,true);
		$criteria->compare('payrol_cycle',$this->payrol_cycle);
		$criteria->compare('timesheet_start_day',$this->timesheet_start_day,true);
		$criteria->compare('timesheet_end_day',$this->timesheet_end_day,true);
		$criteria->compare('sales_tax',$this->sales_tax,true);
		$criteria->compare('county_tax',$this->county_tax,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InvoiceClientVend the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
