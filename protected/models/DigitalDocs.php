<?php

/**
 * This is the model class for table "vms_digital_docs".
 *
 * The followings are the available columns in table 'vms_digital_docs':
 * @property integer $id
 * @property integer $client_id
 * @property integer $created_by_id
 * @property integer $doc_type
 * @property string $doc_name
 * @property string $file
 * @property string $date_created
 */
class DigitalDocs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_digital_docs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, status, created_by_id, doc_type, doc_name, file, date_created , digital_email_subject , digital_email_message ,duration_alert ', 'required'),
			array('file', 'file', 'allowEmpty'=>false, 'types'=>'pdf', 'message'=>'pdf files only'),
			array('client_id, created_by_id, doc_type', 'numerical', 'integerOnly'=>true),
			array('doc_name, file', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, created_by_id, doc_type, doc_name, file, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'created_by_id' => 'Created By',
			'doc_type' => 'Doc Type',
			'doc_name' => 'Doc Name',
			'file' => 'File',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('created_by_id',$this->created_by_id);
		$criteria->compare('doc_type',$this->doc_type);
		$criteria->compare('doc_name',$this->doc_name,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DigitalDocs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
