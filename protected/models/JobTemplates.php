<?php

/**
 * This is the model class for table "vms_job_templates".
 *
 * The followings are the available columns in table 'vms_job_templates':
 * @property integer $id
 * @property integer $client_id
 * @property string $job_title
 * @property integer $cat_id
 * @property string $experience
 * @property string $job_level
 * @property string $primary_skills
 * @property string $secondary_skills
 * @property string $good_to_have
 * @property string $notes_for_skills
 * @property string $job_description
 * @property string $you_will
 * @property string $qualification
 * @property string $add_info
 * @property string $date_created
 * @property string $date_updated
 */
class JobTemplates extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vms_job_templates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, job_title, cat_id, experience,temp_max_billrate,temp_min_billrate,temp_experience, job_level, primary_skills, secondary_skills, good_to_have, notes_for_skills, job_description, you_will, qualification, add_info, date_created, date_updated', 'required'),
			array('client_id, cat_id', 'numerical', 'integerOnly'=>true),
			array('job_title, job_level, primary_skills, secondary_skills, good_to_have', 'length', 'max'=>255),
			array('experience', 'length', 'max'=>111),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, job_title, cat_id, experience, job_level, primary_skills, secondary_skills, good_to_have, notes_for_skills, job_description, you_will, qualification, add_info, date_created, date_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'job_title' => 'Job Title',
			'cat_id' => 'Cat',
			'experience' => 'Experience',
			'job_level' => 'Job Level',
			'primary_skills' => 'Primary Skills',
			'secondary_skills' => 'Secondary Skills',
			'good_to_have' => 'Good To Have',
			'notes_for_skills' => 'Notes For Skills',
			'job_description' => 'Job Description',
			'you_will' => 'You Will',
			'qualification' => 'Qualification',
			'add_info' => 'Add Info',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('job_title',$this->job_title,true);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('experience',$this->experience,true);
		$criteria->compare('job_level',$this->job_level,true);
		$criteria->compare('primary_skills',$this->primary_skills,true);
		$criteria->compare('secondary_skills',$this->secondary_skills,true);
		$criteria->compare('good_to_have',$this->good_to_have,true);
		$criteria->compare('notes_for_skills',$this->notes_for_skills,true);
		$criteria->compare('job_description',$this->job_description,true);
		$criteria->compare('you_will',$this->you_will,true);
		$criteria->compare('qualification',$this->qualification,true);
		$criteria->compare('add_info',$this->add_info,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JobTemplates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
