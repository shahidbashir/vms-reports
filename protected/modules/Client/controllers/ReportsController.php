<?php

class ReportsController extends Controller
{
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('spendReportData','spendReportcurrent','allXcel','timeSheetxcel','xcel','index','headReoprt','jobReports','jobFirstReport','headCountReoprt','spendReport','timesheetReport','submissionReports','monthlysubReports','interviewReports','monthlyintReports','saveReaports','filterReport','saveSchedule'),
				'users'=>array('@'),
			),
		);
	}

	public function actionSpendReportData(){

		/* Build SQL Query WHERE clause */
		$whereClause = '';
		if(isset($_POST['category']) && $_POST['category'] !==''){
			$whereClause = " job_category = '" . $_POST['category']. "'";
		}

		if(isset($_POST['department']) && $_POST['department'] !==''){
			if($whereClause == ''){
				$whereClause = " hiring_mgr_department = '" . $_POST['department']. "'";
			}
			else{
				$whereClause = $whereClause . " AND hiring_mgr_department = '" . $_POST['department']. "'";
			}
		}

		if(isset($_POST['status']) && $_POST['status'] !==''){
			if($whereClause == ''){
				$whereClause = " supplier_name = '" . $_POST['status']. "'";
			}
			else{
				$whereClause = $whereClause . " AND supplier_name = '" . $_POST['status']. "'";
			}
		}

		if(isset($_POST['type']) && $_POST['type'] !==''){
			if($whereClause == ''){
				$whereClause = " location = '" . $_POST['type']. "'";
			}
			else{
				$whereClause = $whereClause . " AND location = '" . $_POST['type']. "'";
			}
		}

		if($whereClause==''){
			$sql = 'SELECT * FROM rpt_spend_current ' . $whereClause;
		}else{
		$sql = 'SELECT * FROM rpt_spend_current where' . $whereClause;
		}
		$reportData= new ReportData();

		/* Data for Table */
		$reportData-> tableData = Yii::app()->db->createCommand($sql)->query()->readAll();


		/**************************************************************************/
		/*  Data for Graphs for category Yearly start here    */
		/**************************************************************************/
		if(!empty($whereClause)){
			$whereClause .= " And ";
		}
		$sql = 'SELECT job_category, SUM(total_bilrate_with_tax) AS billrate,Year(invoice_start_date) as yeardisplay FROM rpt_spend_current where ' . $whereClause . '  
		 Year(invoice_start_date) = "'.date('Y').'" GROUP BY job_category,yeardisplay';
		$reportData->jobCountByCategoryYTD = Yii::app()->db->createCommand($sql)->query()->readAll();
		/**************************************************************************/
		/*  Data for Graphs for category Yearly End here    */
		/**************************************************************************/




		/**************************************************************************/
		/*   Data for Graphs for category Quaterly start here    */
		/**************************************************************************/
		if(date('m')==1 || date('m')==2 || date('m')==3){
			$exp = 'Month(invoice_start_date) in ("'.date('01').'","'.date('02').'","'.date('03').'")';

		}
		if(date('m')==4 || date('m')==5 || date('m')==6){
			$exp = 'Month(invoice_start_date) in ("'.date('04').'","'.date('05').'","'.date('06').'")';

		}
		if(date('m')==7 || date('m')==8 || date('m')==9){
			$exp = 'Month(invoice_start_date) in ("'.date('07').'","'.date('08').'","'.date('09').'")';
		}
		if(date('m')==10 || date('m')==11 || date('m')==12){
			$exp = 'Month(invoice_start_date) in ("'.date('10').'","'.date('11').'","'.date('12').'")';

		}

			$sql = 'SELECT job_category, SUM(total_bilrate_with_tax) AS billrate,Month(invoice_start_date) as monthdisplay FROM rpt_spend_current where ' . $whereClause . '  
		'.$exp.' AND Year(invoice_start_date) = "'.date('Y').'" GROUP BY job_category';
		$reportData->jobCountByCategoryQTD = Yii::app()->db->createCommand($sql)->query()->readall();

		/**************************************************************************/
		/*   Data for Graphs for category Quaterly End here    */
		/**************************************************************************/



		/**************************************************************************/
		/* Data for Graphs for category Monthly start here */
		/**************************************************************************/
		$sql = 'SELECT job_category, SUM(total_bilrate_with_tax) AS billrate,Month(invoice_start_date) as monthdisplay FROM rpt_spend_current where ' . $whereClause . '  
		Month(invoice_start_date) = "'.date('m').'" AND Year(invoice_start_date) = "'.date('Y').'" GROUP BY job_category';
		$reportData->jobCountByCategoryMTD = Yii::app()->db->createCommand($sql)->query()->readall();
		/**************************************************************************/
		/* Data for Graphs for category Monthly End here */
		/**************************************************************************/


		/**************************************************************************/
		/*  Data for Graphs for Department Yearly start here    */
		/**************************************************************************/
		$sql = 'SELECT hiring_mgr_department, SUM(total_bilrate_with_tax) AS billrate,Year(invoice_start_date) as yeardisplay FROM rpt_spend_current where ' . $whereClause . '  
		 Year(invoice_start_date) = "'.date('Y').'" GROUP BY hiring_mgr_department,yeardisplay';
		$reportData->spendCountByDepartmentYTD = Yii::app()->db->createCommand($sql)->query()->readAll();
		/**************************************************************************/
		/*  Data for Graphs for Department Yearly End here    */
		/**************************************************************************/



		/**************************************************************************/
		/*   Data for Graphs for department Quaterly start here    */
		/**************************************************************************/
		if(date('m')==1 || date('m')==2 || date('m')==3){
			$exp = 'Month(invoice_start_date) in ("'.date('01').'","'.date('02').'","'.date('03').'")';

		}
		if(date('m')==4 || date('m')==5 || date('m')==6){
			$exp = 'Month(invoice_start_date) in ("'.date('04').'","'.date('05').'","'.date('06').'")';

		}
		if(date('m')==7 || date('m')==8 || date('m')==9){
			$exp = 'Month(invoice_start_date) in ("'.date('07').'","'.date('08').'","'.date('09').'")';
		}
		if(date('m')==10 || date('m')==11 || date('m')==12){
			$exp = 'Month(invoice_start_date) in ("'.date('10').'","'.date('11').'","'.date('12').'")';

		}

		$sql = 'SELECT hiring_mgr_department, SUM(total_bilrate_with_tax) AS billrate,Month(invoice_start_date) as monthdisplay FROM rpt_spend_current where ' . $whereClause . '  
		'.$exp.' AND Year(invoice_start_date) = "'.date('Y').'" GROUP BY hiring_mgr_department';
		$reportData->spendCountBydeparmentQTD = Yii::app()->db->createCommand($sql)->query()->readall();

		/**************************************************************************/
		/*   Data for Graphs for department Quaterly End here    */
		/**************************************************************************/



		/**************************************************************************/
		/* Data for Graphs for department Monthly start here */
		/**************************************************************************/
		$sql = 'SELECT hiring_mgr_department, SUM(total_bilrate_with_tax) AS billrate,Month(invoice_start_date) as monthdisplay FROM rpt_spend_current where ' . $whereClause . '  
		Month(invoice_start_date) = "'.date('m').'" AND Year(invoice_start_date) = "'.date('Y').'" GROUP BY hiring_mgr_department';
		$reportData->spendCountBydepartmentMTD = Yii::app()->db->createCommand($sql)->query()->readall();
		/**************************************************************************/
		/* Data for Graphs for department Monthly End here */
		/**************************************************************************/



		/**************************************************************************/
		/*  Data for Graphs for Supplier Yearly start here    */
		/**************************************************************************/
		$sql = 'SELECT supplier_name, SUM(total_bilrate_with_tax) AS billrate,Year(invoice_start_date) as yeardisplay FROM rpt_spend_current where ' . $whereClause . '  
		 Year(invoice_start_date) = "'.date('Y').'" GROUP BY supplier_name,yeardisplay';
		$reportData->spendCountBySupplierYTD = Yii::app()->db->createCommand($sql)->query()->readAll();
		/**************************************************************************/
		/*  Data for Graphs for Supplier Yearly End here    */
		/**************************************************************************/



		/**************************************************************************/
		/*   Data for Graphs for supplier Quaterly start here    */
		/**************************************************************************/
		if(date('m')==1 || date('m')==2 || date('m')==3){
			$exp = 'Month(invoice_start_date) in ("'.date('01').'","'.date('02').'","'.date('03').'")';

		}
		if(date('m')==4 || date('m')==5 || date('m')==6){
			$exp = 'Month(invoice_start_date) in ("'.date('04').'","'.date('05').'","'.date('06').'")';

		}
		if(date('m')==7 || date('m')==8 || date('m')==9){
			$exp = 'Month(invoice_start_date) in ("'.date('07').'","'.date('08').'","'.date('09').'")';
		}
		if(date('m')==10 || date('m')==11 || date('m')==12){
			$exp = 'Month(invoice_start_date) in ("'.date('10').'","'.date('11').'","'.date('12').'")';

		}

		$sql = 'SELECT supplier_name, SUM(total_bilrate_with_tax) AS billrate,Month(invoice_start_date) as monthdisplay FROM rpt_spend_current where ' . $whereClause . '  
		'.$exp.' AND Year(invoice_start_date) = "'.date('Y').'" GROUP BY supplier_name';
		$reportData->spendCountBysupplierQTD = Yii::app()->db->createCommand($sql)->query()->readall();

		/**************************************************************************/
		/*   Data for Graphs for supplier Quaterly End here    */
		/**************************************************************************/


		/**************************************************************************/
		/* Data for Graphs for supplier Monthly start here */
		/**************************************************************************/
		$sql = 'SELECT supplier_name, SUM(total_bilrate_with_tax) AS billrate,Month(invoice_start_date) as monthdisplay FROM rpt_spend_current where ' . $whereClause . '  
		Month(invoice_start_date) = "'.date('m').'" AND Year(invoice_start_date) = "'.date('Y').'" GROUP BY supplier_name';
		$reportData->spendCountBysupplierMTD = Yii::app()->db->createCommand($sql)->query()->readall();
		/**************************************************************************/
		/* Data for Graphs for supplier Monthly End here */
		/**************************************************************************/


		/**************************************************************************/
		/*  Data for Graphs for Location Yearly start here    */
		/**************************************************************************/
		$sql = 'SELECT location, SUM(total_bilrate_with_tax) AS billrate,Year(invoice_start_date) as yeardisplay FROM rpt_spend_current where ' . $whereClause . '  
		 Year(invoice_start_date) = "'.date('Y').'" GROUP BY location,yeardisplay';
		$reportData->spendCountByLocationYTD = Yii::app()->db->createCommand($sql)->query()->readAll();
		/**************************************************************************/
		/*  Data for Graphs for Location Yearly End here    */
		/**************************************************************************/


		/**************************************************************************/
		/*   Data for Graphs for location Quaterly start here    */
		/**************************************************************************/
		if(date('m')==1 || date('m')==2 || date('m')==3){
			$exp = 'Month(invoice_start_date) in ("'.date('01').'","'.date('02').'","'.date('03').'")';

		}
		if(date('m')==4 || date('m')==5 || date('m')==6){
			$exp = 'Month(invoice_start_date) in ("'.date('04').'","'.date('05').'","'.date('06').'")';

		}
		if(date('m')==7 || date('m')==8 || date('m')==9){
			$exp = 'Month(invoice_start_date) in ("'.date('07').'","'.date('08').'","'.date('09').'")';
		}
		if(date('m')==10 || date('m')==11 || date('m')==12){
			$exp = 'Month(invoice_start_date) in ("'.date('10').'","'.date('11').'","'.date('12').'")';

		}

		$sql = 'SELECT location, SUM(total_bilrate_with_tax) AS billrate,Month(invoice_start_date) as monthdisplay FROM rpt_spend_current where ' . $whereClause . '  
		'.$exp.' AND Year(invoice_start_date) = "'.date('Y').'" GROUP BY location';
		$reportData->spendCountBylocationQTD = Yii::app()->db->createCommand($sql)->query()->readall();

		/**************************************************************************/
		/*   Data for Graphs for location Quaterly End here    */
		/**************************************************************************/


		/**************************************************************************/
		/* Data for Graphs for location Monthly start here */
		/**************************************************************************/
		$sql = 'SELECT location, SUM(total_bilrate_with_tax) AS billrate,Month(invoice_start_date) as monthdisplay FROM rpt_spend_current where ' . $whereClause . '  
		Month(invoice_start_date) = "'.date('m').'" AND Year(invoice_start_date) = "'.date('Y').'" GROUP BY location';
		$reportData->spendCountBylocationMTD = Yii::app()->db->createCommand($sql)->query()->readall();
		/**************************************************************************/
		/* Data for Graphs for location Monthly End here */
		/**************************************************************************/


		$this->layout=false;
		header('Content-type: application/json');
		echo CJSON::encode($reportData);
		Yii::app()->end(); // equal to die() or exit() function
	}
	public function actionSpendReportCurrent(){

		/* Data for Filters */
		$sql = 'SELECT DISTINCT job_category FROM rpt_spend_current';
		$categoryList = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = 'SELECT DISTINCT hiring_mgr_department FROM rpt_spend_current';
		$departmentList = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = 'SELECT DISTINCT supplier_name FROM rpt_spend_current';
		$supplier = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = 'SELECT DISTINCT location FROM rpt_spend_current';
		$typeList = Yii::app()->db->createCommand($sql)->query()->readAll();

		$SavedReport  = ReportGenerate::model()->findAllByAttributes(array('main_client'=>UtilityManager::superClient(Yii::app()->user->id)));

		$this->render('spendReportcurrent',array('categoryList' => $categoryList, 'departmentList' => $departmentList,'typeList'=>$typeList, 'supplier' => $supplier,
			'SavedReport'=>$SavedReport));

	}
	public function actioninterviewReports(){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
		}
		//Monthly Job Report Data 3rd graph
		$i = 30;
		$todayDate = date('Y-m-d');
		$backDate = date('Y-m-d',strtotime(date('Y-m-d') . " -".$i." days"));
		//echo $this->DateRangeStatusCount($todayDate,$backDate,3);
		$monthlyintReport = '';

		$monthlyintReport .= '{name:"Phone Interview", value:'.$this->InterviewRangeStatusCount($rejection='',$jobsid='',$todayDate,$backDate,'Phone Interview').'},';
		$monthlyintReport .= '{name:"Online Interview", value:'.$this->InterviewRangeStatusCount($rejection='',$jobsid='',$todayDate,$backDate,'Online Interview').'},';
		$monthlyintReport .= '{name:"Face to Face Interview", value:'.$this->InterviewRangeStatusCount($rejection='',$jobsid='',$todayDate,$backDate,'Face to Face Interview').'},';
		$monthlyintReport .= '{name:"Assignment Interview", value:'.$this->InterviewRangeStatusCount($rejection='',$jobsid='',$todayDate,$backDate,'Assignment Interview').'},';

		// for 2nd rejection graph
		$criteria = new CDbCriteria();
		$criteria->select="*";
		$criteria->join = 'INNER JOIN vms_job ON vms_job.id = t.job_id';
		$criteria->condition = "vms_job.user_id =".$loginUser;
		$criteria->group='reason_rejection';
		$criteria->addCondition("status = 3");
		$Interview = Interview::model()->findAll($criteria);
		$rejectionReport = '';
		if($Interview){
			foreach ($Interview as $value){
				$rejectionReport .= "['.$value->reason_rejection.', ".$this->InterviewRangeStatusCount($value->reason_rejection,$jobsid='',$todayDate,$backDate,$status='')."],";
			}
		}
		$this->render('interviewReports',array('monthlyintReport'=>$monthlyintReport,'rejectionReport'=>$rejectionReport));
	}

	public function InterviewRangeStatusCount($rejection,$jobsid,$todayDate,$backDate,$status){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
		}
		$criteria = new CDbCriteria();
		$criteria->select="*";
		$criteria->join = 'INNER JOIN vms_job ON vms_job.id = t.job_id';
		$criteria->condition = "vms_job.user_id =".$loginUser;
		if(!empty($jobsid)){
		$criteria->addInCondition('job_id', $jobsid);
		}
		if(!empty($rejection)){
			$criteria->addCondition("reason_rejection = '$rejection'");
		}
		if(!empty($status)) {
			$criteria->addCondition("interview_type = '$status'");
		}
		$criteria->addBetweenCondition("interview_creation_date",$backDate,$todayDate,'AND');
		$sub = Interview::model()->count($criteria);
		return $sub;
	}

	public function actionMonthlyintReports(){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if ($client->super_client_id != 0) {
			$loginUser = $client->super_client_id;
		}
		//Monthly Job Report Data 3rd graph
		$jobs = Job::model()->findAllByAttributes(array('cat_id'=>$_POST['cat']));

		$jobsid = array();
		foreach ($jobs as $joid){
			$jobsid[] = $joid->id;

		}
		$i = 30;
		$todayDate = date('Y-m-d');
		$backDate = date('Y-m-d', strtotime(date('Y-m-d') . " -" . $i . " days"));
		$monthlyintReport = '';

		$monthlyintReport .= '{name:"Phone Interview", value:'.$this->InterviewRangeStatusCount($rejection='',$jobsid,$todayDate,$backDate,'Phone Interview').'},';
		$monthlyintReport .= '{name:"Online Interview", value:'.$this->InterviewRangeStatusCount($rejection='',$jobsid,$todayDate,$backDate,'Online Interview').'},';
		$monthlyintReport .= '{name:"Face to Face Interview", value:'.$this->InterviewRangeStatusCount($rejection='',$jobsid,$todayDate,$backDate,'Face to Face Interview').'},';
		$monthlyintReport .= '{name:"Assignment Interview", value:'.$this->InterviewRangeStatusCount($rejection='',$jobsid,$todayDate,$backDate,'Assignment Interview').'},';

		// for 2nd rejection graph
		$criteria = new CDbCriteria();
		$criteria->select="*";
		$criteria->join = 'INNER JOIN vms_job ON vms_job.id = t.job_id';
		$criteria->condition = "vms_job.user_id =".$loginUser;
		$criteria->group='reason_rejection';
		$criteria->addCondition("status = 3");
		$criteria->addInCondition('job_id', $jobsid);
		$Interview = Interview::model()->findAll($criteria);
		$rejectionReport = '';
		if($Interview){
			foreach ($Interview as $value){
				$rejectionReport .= "['.$value->reason_rejection.', ".$this->InterviewRangeStatusCount($value->reason_rejection,$jobsid,$todayDate,$backDate,$status='')."],";
			}
		}?>
		<script>
			var headCountData =
				[
					<?php echo $monthlyintReport; ?>
				];

			var headCountChart = anychart.funnel( headCountData );
			headCountChart.title("Overall Interview Status");
			// pass the container id, chart will be displayed there
			// set chart legend settings
			var legend = headCountChart.legend();
			legend.enabled(true);
			legend.position("center");
			legend.itemsLayout("horizontal");
			legend.align("top center");
			headCountChart.saveAsPdf();
			// set chart base width settings
			headCountChart.baseWidth("70%");
			// set the neck height
			headCountChart.neckHeight("0%");

			headCountChart.container("interview-rejrection-report");
			headCountChart.background('white');
			// call the chart draw() method to initiate chart display
			headCountChart.draw();
		</script>@
		<script>
			var hiringHeadCountData = [

				<?php echo $rejectionReport; ?>
			];

			var hiringHeadCountChart = anychart.pie( hiringHeadCountData );
			hiringHeadCountChart.title("Interview Rejection Status");
			// pass the container id, chart will be displayed there
			hiringHeadCountChart.container("interview-report");
			// call the chart draw() method to initiate chart display
			hiringHeadCountChart.draw();

		</script>
		<?php
	}


	public function actionMonthlysubReports()
	{
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if ($client->super_client_id != 0) {
			$loginUser = $client->super_client_id;
		}
		//Monthly Job Report Data 3rd graph
		$jobs = Job::model()->findAllByAttributes(array('cat_id'=>$_POST['cat']));

		$jobsid = array();
		foreach ($jobs as $joid){
			$jobsid[] = $joid->id;

		}
		$i = 30;
		$todayDate = date('Y-m-d');
		$backDate = date('Y-m-d', strtotime(date('Y-m-d') . " -" . $i . " days"));
		//echo $this->DateRangeStatusCount($todayDate,$backDate,3);
		$monthlysubReport = '';

		$monthlysubReport .= '{name:"Submitted", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 1) . '},';
		$monthlysubReport .= '{name:"MSP Shortlisted", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 3) . '},';
		$monthlysubReport .= '{name:"Rejected", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 6) . '},';
		$monthlysubReport .= '{name:"Interview Process", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 5) . '},';
		$monthlysubReport .= '{name:"Client Review", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 4) . '},';
		$monthlysubReport .= '{name:"Hired", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 8) . '},';
		$monthlysubReport .= '{name:"Offer", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 7) . '},';
		$monthlysubReport .= '{name:"MSP Review", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 2) . '},';
		$monthlysubReport .= '{name:"Work Order Release", value:' . $this->SubRangeStatusCount($rejction='',$jobsid,$todayDate, $backDate, 9) . '},';

		// for 2nd rejection graph
		$vendorjobsubmission = VendorJobSubmission::model()->findAllByAttributes(array('resume_status' => 6, 'client_id' => $loginUser, 'rejected_type' => 'Client','job_id'=>$jobsid));
		$rejectionReport = '';
		if ($vendorjobsubmission) {
			foreach ($vendorjobsubmission as $value) {
				$rejection = Setting::model()->findByPk($value->reason_for_rejection);
				$rejectionReport .= "['.$rejection->title.', " . $this->SubRangeStatusCount($value->reason_for_rejection,$jobsid,$todayDate, $backDate, 6) . "],";
			}
		}?>
		<script>
				var headCountData =
					[
						<?php echo $monthlysubReport; ?>
					];
				var headCountChart = anychart.funnel(headCountData);
				headCountChart.title("Overall Submission Status");
				var legend = headCountChart.legend();
				legend.enabled(true);
				legend.position("center");
				legend.itemsLayout("horizontal");
				legend.align("top center");
				headCountChart.saveAsPdf();
				headCountChart.baseWidth("70%");
				headCountChart.neckHeight("0%");
				headCountChart.container("submission-rejrection-report");
				headCountChart.background('white');
				headCountChart.draw();
		</script>@
		<script>
				var hiringHeadCountData = [
					<?php echo $rejectionReport; ?>
				];
				var hiringHeadCountChart = anychart.pie(hiringHeadCountData);
				hiringHeadCountChart.title("Submission Rejection Status");
				hiringHeadCountChart.container("job-cat-report");
				hiringHeadCountChart.draw();

		</script>
		<?php
	}
	public function SubRangeStatusCount($rejction,$jobsid,$todayDate,$backDate,$status){

		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition("resume_status = ".$status);
		$criteria->addCondition("client_id = ".$loginUser);
		$criteria->addCondition('rejected_type != :rejected_type');
		if(!empty($rejction)){
			$criteria->addCondition("reason_for_rejection = ".$rejction);
		}
		$criteria->params[ ':rejected_type' ] = 'msp';
		$criteria->addBetweenCondition("date_created",$backDate,$todayDate,'AND');
		$criteria->addInCondition('job_id', $jobsid);
		//$criteria->condition = 'job_id IN('.$jobsid.')';
		//$criteria->condition = "rejected_type" != "msp";
		//$criteria->addCondition("rejected_type" != "msp");
		$sub = VendorJobSubmission::model()->count($criteria);
		return $sub;
	}
	public function actionSubmissionReports(){

		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
		}
		//Monthly Job Report Data 3rd graph
		$i = 30;
		$todayDate = date('Y-m-d');
		$backDate = date('Y-m-d',strtotime(date('Y-m-d') . " -".$i." days"));
		//echo $this->DateRangeStatusCount($todayDate,$backDate,3);
		$monthlysubReport = '';

		$monthlysubReport .= '{name:"Submitted", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,1).'},';
		$monthlysubReport .= '{name:"MSP Shortlisted", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,3).'},';
		$monthlysubReport .= '{name:"Rejected", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,6).'},';
		$monthlysubReport .= '{name:"Interview Process", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,5).'},';
		$monthlysubReport .= '{name:"Client Review", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,4).'},';
		$monthlysubReport .= '{name:"Hired", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,8).'},';
		$monthlysubReport .= '{name:"Offer", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,7).'},';
		$monthlysubReport .= '{name:"MSP Review", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,2).'},';
		$monthlysubReport .= '{name:"Work Order Release", value:'.$this->DateRangeStatusCount($rejction='',$todayDate,$backDate,9).'},';

		// for 2nd rejection graph
		$vendorjobsubmission = VendorJobSubmission::model()->findAllByAttributes(array('resume_status'=>6,'client_id'=>$loginUser,'rejected_type'=>'Client'));
		$rejectionReport = '';
		if($vendorjobsubmission){
			foreach ($vendorjobsubmission as $value){
				$rejection = Setting::model()->findByPk($value->reason_for_rejection);
				$rejectionReport .= "['.$rejection->title.', ".$this->DateRangeStatusCount($value->reason_for_rejection,$todayDate,$backDate,6)."],";
			}
		}

		$this->render('submissionReports',array('monthlysubReport'=>$monthlysubReport,'rejectionReport'=>$rejectionReport));
	}
	public function DateRangeStatusCount($rejction,$todayDate,$backDate,$status){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition("resume_status = ".$status);
		$criteria->addCondition("client_id = ".$loginUser);
		if(!empty($rejction)){
		$criteria->addCondition("reason_for_rejection = ".$rejction);
		}
		$criteria->addCondition('rejected_type != :rejected_type');
		$criteria->params[ ':rejected_type' ] = 'msp';
		//$criteria->condition = "rejected_type" != "msp";
		//$criteria->addCondition("rejected_type" != "msp");
		$criteria->addBetweenCondition("date_created",$backDate,$todayDate,'AND');
		$sub = VendorJobSubmission::model()->count($criteria);
		return $sub;
	}
	public function actionspendReport(){
		$loginClientData = Client::model()->findByPk(Yii::app()->user->id);
			if($loginClientData->member_type==NULL){
				$loginUser = Yii::app()->user->id;
				}else{
					 $loginUser = $loginClientData->super_client_id;
					 }		
		//yearly report for headcount
		$year = array();
		for($i=0;$i<12;$i++){
			$year[] = date('Y-m-d',strtotime(date('Y-1-d').' + '.$i.' months'));
		}
		$hiredTotal = ''; $totalBill = ''; $tableData = array(); $tableData2 = array();
		foreach($year as $exactDate){
				$dates = date("M",strtotime($exactDate));
				//overall billrate query
				$query2 = "SELECT sum(total_bilrate_with_tax) as totalBill FROM vms_generated_invoice WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$totalBillQ = Yii::app()->db->createCommand( $query2 )->query()->read();
				$totalBill .= "['".$dates."',".$totalBillQ['totalBill']."],";
				
				
				
				//table data query
				//total regular hours query
				
				$query = "SELECT id FROM vms_generated_invoice WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."') group by candidate_id";
				$tableData['no_of_emp'][] = Yii::app()->db->createCommand( $query )->query()->count();
				
				$query1 = "SELECT id FROM vms_generated_invoice WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$tableData['total_invoices'][] = Yii::app()->db->createCommand( $query1 )->query()->count();
				
				
				$query3 = "SELECT sum(total_regular_hours) as total_regular_hours, sum(total_overtime_hours) as total_overtime_hours, sum(total_doubletime_hours) as total_doubletime_hours, sum(total_bilrate_with_tax) as total_bilrate_with_tax FROM vms_generated_invoice WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$record = Yii::app()->db->createCommand( $query3 )->query()->read();
			$tableData['month'][] = $dates;
			$tableData['total_regular_hours'][] = $record['total_regular_hours'];
			$tableData['total_overtime_hours'][] = $record['total_overtime_hours'];
			$tableData['total_doubletime_hours'][] = $record['total_doubletime_hours'];
			$tableData['total_bilrate_with_tax'][] = $record['total_bilrate_with_tax'];


		}
		
		$query4 = "SELECT  vms_generated_invoice.invoice_start_date as dated , sum(vms_generated_invoice.total_bilrate_with_tax) as total_bilrate_with_tax,vms_job.cat_id as category FROM vms_generated_invoice INNER JOIN vms_job ON vms_generated_invoice.job_id = vms_job.id WHERE client_id = $loginUser  Group By vms_job.cat_id ORDER BY dated";

		$tableData2 = Yii::app()->db->createCommand( $query4 )->query()->readAll();
	/*echo '<pre>'; print_r($record3); exit;*/
			//total regular hours query
				/*$query4 = "SELECT sum(total_overtime_hours) as total_overtime_hours FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$total_overtime_hours = Yii::app()->db->createCommand( $query4 )->query()->read();
				
				//total regular hours query
				$query5 = "SELECT sum(total_doubletime_hours) as total_doubletime_hours FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$total_doubletime_hours = Yii::app()->db->createCommand( $query5 )->query()->read();*/
				
				/*$query = "SELECT id FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."') group by candidate_id";
				$tableData['no_of_emp'][] = Yii::app()->db->createCommand( $query )->query()->count();
				*/

				/*$tableData2['month'][] = $dates;
				$tableData2['total_bilrate_with_tax'][] = $record3['total_bilrate_with_tax'];
				$tableData2['category'][] = $record3['category'];*/

		/*foreach ($record3 as $value2){
			$tableData2['month'][$value2['category']]['dates'][] = date('m',strtotime($value2['dated']));

			$tableData2['month'][$value2['category']]['total_bilrate_with_tax'][] = $value2['total_bilrate_with_tax'];
		}*/
				


			/*echo '<pre>';
			print_r($tableData2);
			exit;*/
			
		
			
		$this->render('spendReport',array('totalBill'=>$totalBill,'tableData'=>$tableData,'tableData2'=>$tableData2));
	}
	
	public function actionTimesheetReport(){
		$loginClientData = Client::model()->findByPk(Yii::app()->user->id);
			if($loginClientData->member_type==NULL){
				$loginUser = Yii::app()->user->id;
				}else{
					 $loginUser = $loginClientData->super_client_id;
					 }		
		//yearly report for headcount
		$year = array();
		for($i=0;$i<12;$i++){
			$year[] = date('Y-m-d',strtotime(date('Y-1-d').' + '.$i.' months'));
		}
		$hiredTotal = ''; $totalBill = ''; $tableData = array(); $monthYear = array();
		foreach($year as $exactDate){
				$dates = date("M",strtotime($exactDate));
				$date = date("F Y",strtotime($exactDate));
				//overall billrate query
				$query2 = "SELECT sum(total_hours) as totalBill FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$totalBillQ = Yii::app()->db->createCommand( $query2 )->query()->read();
				$totalBill .= "['".$dates."',".$totalBillQ['totalBill']."],";
				
				//table data query
				//total regular hours query
				$query3 = "SELECT sum(total_regular_hours) as total_regular_hours FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$total_regular_hours = Yii::app()->db->createCommand( $query3 )->query()->read();
				
				//total regular hours query
				$query4 = "SELECT sum(total_overtime_hours) as total_overtime_hours FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$total_overtime_hours = Yii::app()->db->createCommand( $query4 )->query()->read();
				
				//total regular hours query
				$query5 = "SELECT sum(total_doubletime_hours) as total_doubletime_hours FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
				$total_doubletime_hours = Yii::app()->db->createCommand( $query5 )->query()->read();
				
				$query = "SELECT id FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."') group by candidate_id";
				$tableData['no_of_emp'][] = Yii::app()->db->createCommand( $query )->query()->count();
				
				$tableData['month'][] = $dates;
				$monthYear[] = $date;
				$tableData['total_regular_hours'][] = $total_regular_hours['total_regular_hours'];
				$tableData['total_overtime_hours'][] = $total_overtime_hours['total_overtime_hours'];
				$tableData['total_doubletime_hours'][] = $total_doubletime_hours['total_doubletime_hours'];
				$tableData['total_spent'][] = $totalBillQ['totalBill'];
				
			}
			
		$this->render('timesheetReport',array('totalBill'=>$totalBill,'tableData'=>$tableData,'monthYear'=>$monthYear));
	}

	public function actionTimeSheetxcel(){
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$month = $_GET['month'];
		/*echo $month;
		exit;*/
		if(isset($_GET['type']) && $_GET['type']=='approved'){
			$Cptimesheet = CpTimesheet::model()->findAllByAttributes(array('client_id'=>$userId,'timesheet_status'=>1,'timesheet_sub_status'=>1));
		}
		if(isset($_GET['type']) && $_GET['type']=='pending'){
			$Cptimesheet = CpTimesheet::model()->findAllByAttributes(array('client_id'=>$userId,'timesheet_status'=>0,'timesheet_sub_status'=>1));
		}
		if(isset($_GET['type']) && $_GET['type']=='all'){
			$Cptimesheet = CpTimesheet::model()->findAllByAttributes(array('client_id'=>$userId,'timesheet_sub_status'=>1));
		}
		header( "Content-Type: application/vnd.ms-excel" );
		header( "Content-disposition: attachment; filename=TimeSheet Report.xls" );
		// print your data here. note the following:
		// - cells/columns are separated by tabs ("\t")
		// - rows are separated by newlines ("\n")

		// for example:
		echo 'S.No' . "\t"  . 'Time Sheet ID' . "\t"  . 'From Date' . "\t". 'To Date' . "\t" . 'First Name' . "\t" . 'Last Name' . "\t" . 'Work Location' . "\t" . 'Regular Hour' . "\t" . 'Over Time' . "\t" . 'Double Time' . "\t" . 'Bill Rate' . "\t" . 'Double Time Rate' . "\t" . 'Over Time Rate' . "\t" . 'Total Cost' . "\t" .'Approved By' . "\t" .'Date Of Approval'. "\t" .'Status' . "\n";
		$i=0;
		foreach ($Cptimesheet as $value){
			$dateFormate = date('M', strtotime($value['invoice_start_date']));
			if ($dateFormate == $month) {
				$i++;
				if($value->timesheet_status==1){
					$status= 'Approved';
				}elseif ($value->timesheet_status==0){
					$status= 'Pending';
				}elseif ($value->timesheet_status==2){
					$status= 'Rejected';
				}else{
					$status= 'Draft';
				}
				$candidate = Candidates::model()->findByPk($value->candidate_id);
				$Offer = Offer::model()->findByPk($value->offer_id);
				$location = Location::model()->findByPk($Offer->approver_manager_location);
				$client = Client::model()->findByPk($value->approval_manager);


				echo $i . "\t" . $value->id . "\t" . $value->invoice_start_date . "\t" . $value->invoice_end_date . "\t" . $candidate->first_name . "\t" . $candidate->last_name . "\t" . $location->name . "\t" . $value->total_regular_hours . "\t" . $value->total_overtime_hours . "\t" . $value->total_doubletime_hours . "\t" .'$'. $value->regular_billrate . "\t" .'$'. $value->doubletime_billrate . "\t" .'$'. $value->total_overtime_payrate . "\t" .'$'. $value->total_billrate . "\t" . $client->first_name . ' ' . $client->last_name . "\t" . $value->approve_date_time. "\t" . $status . "\n";
			}
		}

	}
	public function actionAllXcel(){
		$userId = UtilityManager::superClient(Yii::app()->user->id);
			$contract = Contract::model()->findAllByAttributes(array('client_id'=>$userId));
			header( "Content-Type: application/vnd.ms-excel" );
			header( "Content-disposition: attachment; filename=Detail Report.xls" );
			// print your data here. note the following:
			// - cells/columns are separated by tabs ("\t")
			// - rows are separated by newlines ("\n")

			// for example:
			echo 'S.No' . "\t"  . 'First Name' . "\t"  . 'Last Name' . "\t". 'Candidate Id' . "\t" . 'Date of Joining' . "\t" . 'Contract Duration ( Start Date )' . "\t" . 'Contract Duration ( End Date )' . "\t" . 'Bill Rate' . "\t" . 'Over Time' . "\t" . 'Double Time' . "\t" . 'Location' . "\t" . 'Vendor Name' . "\t" . 'Job ID' . "\t" . 'Hiring Manager' . "\t" .'Contract Id' . "\t" .'Workorder Id' . "\t" .'Offer Id' . "\t" .'Status' . "\n";

			$i = 0;
			foreach ($contract as $value) {
				$i++;
				if($value['termination_status'] == 2 && strtotime($value['termination_date']) <= time()){
					$status = 'Closed';
				}elseif($value['ext_status'] == 2 ){
					$status = 'Approved ( Renewed )';
				}else {
					$status = 'Approved';
				}
				$candidate = Candidates::model()->findByPk($value['candidate_id']);
				$workorder = Workorder::model()->findByPk($value['workorder_id']);
				$vendor = Vendor::model()->findByPk($value['vendor_id']);
				$offer = Offer::model()->findByPk($value['offer_id']);
				$location = Location::model()->findByPk($offer->approver_manager_location);
				$client = Client::model()->findByPk($workorder->wo_hiring_manager);

					echo $i  . "\t" . $candidate->first_name  . "\t" .  $candidate->last_name. "\t" .$candidate->id . "\t" . $workorder->onboard_changed_start_date .  "\t" . $value['start_date'] .  "\t" . $value['end_date'] .  "\t" .'$'. $workorder->wo_bill_rate .  "\t" .'$'. $workorder->wo_over_time .  "\t" .'$'. $workorder->wo_double_time .  "\t" . $location->name .  "\t" . $vendor->organization .  "\t" . $value['job_id'] .  "\t" . $client->first_name.' '.$client->last_name . "\t" .$value['id'] . "\t" .$workorder->id . "\t" .$offer->id  . "\t" .$status . "\n";
			}

	}
	
	public function actionHeadCountExcel(){
		header( "Content-Type: application/vnd.ms-excel" );
		header( "Content-disposition: attachment; filename=Head Count Report.xls" );

		// for example:
		echo 'S.No' 
				. "\t"  . 'Job Id' 
				. "\t"  . 'Candidate Id'
				. "\t"  . 'Candidate Name'
				. "\t"  . 'Canddiate Email'
				. "\t"  . 'Start Date'
				. "\t"  . 'End Date'
				. "\t"  . 'Bill Rate'
				. "\t"  . 'Pay Rate'
				. "\t"  . 'Hiring Mgr Name'
				. "\t"  . 'Hiring Mg Department'
				. "\t"  . 'Supplier Name'
				. "\t"  . 'Location'
				. "\t"  . 'Termination Date'
				. "\t"  . 'Status'
				. "\t"  . 'Job Title'
				. "\t"  . 'Job Category'
				. "\n";
		
		$sql = 'SELECT * FROM rpt_headcount_current';
		$headcountData = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$i = 0;
		foreach ($headcountData as $value) {
			$i++;
			echo $i  
				. "\t" . $value['job_id'] 
				. "\t" . $value['candidate_id']
				. "\t" . $value['candidate_name']
				. "\t" . $value['candidate_email']
				. "\t" . $value['start_date']
				. "\t" . $value['end_date']
				. "\t" . $value['bill_rate']
				. "\t" . $value['pay_rate']
				. "\t" . $value['hiring_mgr_name']
				. "\t" . $value['hiring_mgr_department']
				. "\t" . $value['supplier_name']
				. "\t" . $value['location']
				. "\t" . $value['termination_date']
				. "\t" . $value['Status']
				. "\t" . $value['job_title']
				. "\t" . $value['job_category']
				. "\n";
		}
		
	}

	public function actionHeadCountTrendExcel(){
		header( "Content-Type: application/vnd.ms-excel" );
		header( "Content-disposition: attachment; filename=Head Count Report.xls" );
		
		// for example:
		echo 'S.No'
		. "\t"  . 'Year Month'
						. "\t"  . 'Job Id'
					. "\t"  . 'Candidate Id'
					. "\t"  . 'Candidate Name'
					. "\t"  . 'Canddiate Email'
				. "\t"  . 'Start Date'
				. "\t"  . 'End Date'
				. "\t"  . 'Bill Rate'
			. "\t"  . 'Pay Rate'
			. "\t"  . 'Hiring Mgr Name'
				. "\t"  . 'Hiring Mg Department'
				. "\t"  . 'Supplier Name'
					. "\t"  . 'Location'
					. "\t"  . 'Termination Date'
						. "\t"  . 'Status'
					 		. "\t"  . 'Job Title'
					 		. "\t"  . 'Job Category'
					 		. "\n";
					 		
					 		$sql = 'SELECT * FROM rpt_headcount_yearmonth';
					 		$headcountData = Yii::app()->db->createCommand($sql)->query()->readAll();
					 		
					 		$i = 0;
					 		foreach ($headcountData as $value) {
					 			$i++;
					 			echo $i
					 			. "\t" . $value['year_month_id']
					 			. "\t" . $value['job_id']
					 			. "\t" . $value['candidate_id']
					 			. "\t" . $value['candidate_name']
					 			. "\t" . $value['candidate_email']
					 			. "\t" . $value['start_date']
					 			. "\t" . $value['end_date']
					 			. "\t" . $value['bill_rate']
					 			. "\t" . $value['pay_rate']
					 			. "\t" . $value['hiring_mgr_name']
					 			. "\t" . $value['hiring_mgr_department']
					 			. "\t" . $value['supplier_name']
					 			. "\t" . $value['location']
					 			. "\t" . $value['termination_date']
					 			. "\t" . $value['Status']
					 			. "\t" . $value['job_title']
					 			. "\t" . $value['job_category']
					 			. "\n";
					 		}
					 		
	}
	public function actionXcel(){
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		if(isset($_GET['month']) && $_GET['type']=='single') {
			$number = $_GET['number'];
			$month = $_GET['month'];
			header( "Content-Type: application/vnd.ms-excel" );
			header( "Content-disposition: attachment; filename=Reports.xls" );

			// print your data here. note the following:
			// - cells/columns are separated by tabs ("\t")
			// - rows are separated by newlines ("\n")

			// for example:
			echo 'S.No' . "\t" . 'Month' . "\t" . 'Hires' . "\n";
			echo '1' . "\t" . $month . "\t" . $number . "\n";

		}else{

			$month = $_GET['month'];
			$contract = Contract::model()->findAllByAttributes(array('client_id'=>$userId));
			header( "Content-Type: application/vnd.ms-excel" );
			header( "Content-disposition: attachment; filename=Monthly Detail Report.xls" );
			// print your data here. note the following:
			// - cells/columns are separated by tabs ("\t")
			// - rows are separated by newlines ("\n")

			// for example:
			echo 'S.No' . "\t"  . 'First Name' . "\t"  . 'Last Name' . "\t". 'Candidate Id' . "\t" . 'Date of Joining' . "\t" . 'Contract Duration ( Start Date )' . "\t" . 'Contract Duration ( End Date )' . "\t" . 'Bill Rate' . "\t" . 'Over Time' . "\t" . 'Double Time' . "\t" . 'Location' . "\t" . 'Vendor Name' . "\t" . 'Job ID' . "\t" . 'Hiring Manager' . "\t" .'Contract Id' . "\t" .'Workorder Id' . "\t" .'Offer Id'. "\t" .'Status' . "\n";

			$i = 0;
			foreach ($contract as $value) {
				$candidate = Candidates::model()->findByPk($value['candidate_id']);
				$workorder = Workorder::model()->findByPk($value['workorder_id']);
				$vendor = Vendor::model()->findByPk($value['vendor_id']);
				$offer = Offer::model()->findByPk($value['offer_id']);
				$location = Location::model()->findByPk($offer->approver_manager_location);
				$client = Client::model()->findByPk($workorder->wo_hiring_manager);
				$dateFormate = date('M', strtotime($value['date_created']));
				if ($dateFormate == $month) {
					$i++;
					if($value['termination_status'] == 2 && strtotime($value['termination_date']) <= time()){
						$status = 'Closed';
					}elseif($value['ext_status'] == 2 ){
						$status = 'Approved ( Renewed )';
					}else {
						$status = 'Approved';
					}
					echo $i  . "\t" . $candidate->first_name  . "\t" .  $candidate->last_name. "\t" .$candidate->id . "\t" . $workorder->onboard_changed_start_date .  "\t" . $value['start_date'] .  "\t" . $value['end_date'] .  "\t" .'$'. $workorder->wo_bill_rate .  "\t" .'$'. $workorder->wo_over_time .  "\t" .'$'. $workorder->wo_double_time .  "\t" . $location->name .  "\t" . $vendor->organization .  "\t" . $value['job_id'] .  "\t" . $client->first_name.' '.$client->last_name . "\t" .$value['id'] . "\t" .$workorder->id . "\t" .$offer->id  . "\t" .$status. "\n";
				}
			}
		}
	}
	
	public function actionHeadCountReoprt(){

		/** query for hiring against category count report **/
		$loginClientData = Client::model()->findByPk(Yii::app()->user->id);
			if($loginClientData->member_type==NULL){
				$loginUser = Yii::app()->user->id;
				}else{
					 $loginUser = $loginClientData->super_client_id;
					 }		
		//yearly report for headcount
		$year = array();
		for($i=0;$i<12;$i++){
			$year[] = date('Y-m-d',strtotime(date('Y-1-d').' + '.$i.' months'));
		}
		$hiredTotal = ''; $monthsT = array(); $monthHiredT = array(); $monthYear = array();
		foreach($year as $exactDate){
				$dates = date("M",strtotime($exactDate));
				$monthname = date("F Y",strtotime($exactDate));
				$query2 = "SELECT id FROM vms_contract WHERE client_id=$loginUser and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."') ORDER BY id DESC";
				$hiredCount = Yii::app()->db->createCommand( $query2 )->query()->count();
				$hiredTotal .= "['".$dates."',".$hiredCount."],";
				$monthsT[] = $dates;
				$monthYear[] = $monthname;
				$monthHiredT[] = $hiredCount;
			}

		$jobgategory = Setting::model()->findAllByAttributes(array('category_id'=>'9'),array('order'=>'id asc'));
		$number = '';
		$string = '';
		if($jobgategory){
			foreach ($jobgategory as $catvalue){
		  $sql = 'SELECT j.* FROM vms_job as j INNER JOIN vms_contract as c on j.id = c.job_id and j.cat_id="'.$catvalue->title.'" and c.client_id='.$loginUser;

				$categoryreports = Yii::app()->db->createCommand($sql)->query()->readAll();
				$number = count($categoryreports);
				if($number >0) {
					$string .= '[' . '"' . $catvalue->title . '"' . ',' . $number . '],';
				}
			} }

		/** query for hiring manager count report **/
		$hiringManager = Client::model()->findAllByAttributes(array('member_type'=>'Hiring Manager'));
		$number1 = '';
		$string1 = '';
		if($hiringManager){
			foreach ($hiringManager as $managerValue){
				$sql = 'SELECT w.* FROM vms_workorder as w INNER JOIN vms_contract as c on w.id = c.workorder_id and w.wo_hiring_manager="'.$managerValue->id.'"';

				$hiringrrecords = Yii::app()->db->createCommand($sql)->query()->readAll();
				$number1 = count($hiringrrecords);
				if($number1 >0) {
					$string1 .= '[' . '"' . $managerValue->first_name.' '.$managerValue->last_name . '"' . ',' . $number1 . '],';
				}
			}
		}

		/** query for hiring count report month wise **/
		if(isset($_POST['datesearch'])){
			$begin = new DateTime(date('Y-m-d',strtotime($_POST['first_date'])));
			$end = new DateTime(date('Y-m-d',strtotime($_POST['last_date'])));

			$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
			$monthlyReport = '';
			foreach($daterange as $date){
				$monthlyReport .= "['".$date->format("m-d-Y")."', ".$this->dailyCountHiring($date->format("Y-m-d"))."],";
			}

		}else{
			$monthlyReport = '';
			for($i=0;$i<30;$i++){
				$monthlyReport .= "['".date('m-d-Y',strtotime(date('Y-m-d') . ' -'.$i.' days'))."', ".$this->dailyCountHiring(date('Y-m-d',strtotime(date('Y-m-d') . ' -'.$i.' days')))."],";
			}
		}
		/* query for headcount location */
		$Location = Location::model()->findAll();
		$this->render('headCountReoprt',array('monthYear'=>$monthYear,'string'=>$string,'string1'=>$string1,'monthlyReport'=>$monthlyReport,'hiredTotal'=>$hiredTotal,'monthsT'=>$monthsT,'monthHiredT'=>$monthHiredT,'Location'=>$Location));
	}

	public function DailyCountHiring($date){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
		}
		$query = "SELECT id FROM vms_contract WHERE DATE_FORMAT(date_created, '%Y-%m-%d')='".$date."' and client_id=".$loginUser;
		$hiring = Yii::app()->db->createCommand( $query )->query()->count();
		return $hiring;
	}




	public function actionIndex()
	{
		$jobgategory = Setting::model()->findAllByAttributes(array('category_id'=>'9'),array('order'=>'id asc'));
		$firstcategory = Setting::model()->findAllByAttributes(array('category_id'=>'9'),array('order'=>'id asc','limit'=>1));	
		$this->render('index',array('jobgategory'=>$jobgategory,'firstcategory'=>$firstcategory));
	}
	
	public function actionHeadReoprt(){
		
		$jobgategory = Setting::model()->findAllByAttributes(array('category_id'=>'9'),array('order'=>'id asc'));
		$firstcategory = Setting::model()->findByAttributes(array('category_id'=>'9'),array('order'=>'id asc','limit'=>1));
		
		$this->render('headReoprt',array('jobgategory'=>$jobgategory,'firstcategory'=>$firstcategory));
	}
	
	public function actionJobReports(){	
			$this->render('jobReports');
		}
	//first report ajax call
	public function actionJobFirstReport(){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
			}
			echo $this->FirstChartScript($_POST['job_category']);
		}
	
	public function JobCountingOnStatus($status){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
			}
		$jobs = Job::model()->countByAttributes(array('user_id'=>$loginUser,'jobStatus'=>$status));
		return $jobs;
		}
		
	public function JobCountingOnCategory($category,$status){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
			}
		$jobs = Job::model()->countByAttributes(array('user_id'=>$loginUser,'cat_id'=>$category,'jobStatus'=>$status));
		return $jobs;
		}
		
	public function actionScorecard(){
		$this->render('scorecard');
	}
	
	public function actionheadcountCurrent(){
		
		$whereClause = '';
		$filterCategory = '';
		$filterLocation = '';
						
		if(isset($_GET['category']) && $_GET['category'] !==''){
			$whereClause = " WHERE job_category = '" . $_GET['category']. "' ";
			$filterCategory = $_GET['category'];
		}

		if(isset($_GET['location']) && $_GET['location'] !==''){
			if($whereClause == ''){
				$whereClause = " WHERE location = '" . $_GET['location']. "' ";
			}
			else{
				$whereClause = $whereClause . " AND location = '" . $_GET['location']. "' ";
			}
			$filterLocation = $_GET['location'];
		}
		
		/* Data for data table */
		$sql = 'SELECT * FROM rpt_headcount_current' . $whereClause;
		$headcountData = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		
		/* Data for graphs */
		$sql = 'SELECT job_category, COUNT(1) AS headcount FROM rpt_headcount_current ' . $whereClause . 'GROUP BY job_category';
		$headcountByCategory = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = 'SELECT hiring_mgr_department as department, COUNT(1) AS headcount FROM rpt_headcount_current ' . $whereClause . 'GROUP BY hiring_mgr_department';
		$headcountByDepartment= Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = 'SELECT supplier_name, COUNT(1) AS headcount FROM rpt_headcount_current ' . $whereClause . 'GROUP BY supplier_name';
		$headcountBySupplier = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$sql = 'SELECT location, COUNT(1) AS headcount FROM rpt_headcount_current ' . $whereClause . 'GROUP BY location';
		$headcountByLocation = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		/* Data for Filters */ 
		$sql = 'SELECT DISTINCT job_category FROM rpt_headcount_current';
		$jobCategoryList = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = 'SELECT DISTINCT location FROM rpt_headcount_current';
		$locationList = Yii::app()->db->createCommand($sql)->query()->readAll();
		$SavedReport  = ReportGenerate::model()->findAllByAttributes(array('main_client'=>UtilityManager::superClient(Yii::app()->user->id)));
		
		$this->render('headcountCurrent',array('headcountData'=>$headcountData, 'headcountByCategory' => $headcountByCategory, 'headcountByDepartment' => $headcountByDepartment, 'headcountBySupplier' => $headcountBySupplier, 'headcountByLocation' => $headcountByLocation, 'jobCategoryList' => $jobCategoryList, 'locationList' => $locationList, 'filterCategory' => $filterCategory, 'filterLocation' => $filterLocation,
			'SavedReport'=>$SavedReport));
	}

	public function actionSaveReaports(){
		$reportGroup = UtilityManager::reportGroup();
		$accessGroup = UtilityManager::accessGroup();
		$model = new ReportGenerate;
		$model->client_id = Yii::app()->user->id;
		$model->main_client = UtilityManager::superClient(Yii::app()->user->id);
		$model->job_category = $_POST['Category'] ? $_POST['Category'] : '';
		$model->location = $_POST['location'] ? $_POST['location'] : '';
		$model->department = $_POST['department'] ? $_POST['department'] : '';
		$model->supplier = $_POST['supplier'] ? $_POST['supplier'] : '';
		$model->report_name = $_POST['reportNamt'] ? $_POST['reportNamt'] : '';
		$model->report_group = $_POST['rpt_group'] ? $_POST['rpt_group'] : '';
		$model->access_group = $_POST['rpt_access_group'] ? $_POST['rpt_access_group'] : '';
		$model->formate = $_POST['rpt_format'] ? $_POST['rpt_format'] : '';
		$model->date_created = date('Y-m-d');
		$model->save(false);
		$SavedReport  = ReportGenerate::model()->findAllByAttributes(array('main_client'=>UtilityManager::superClient(Yii::app()->user->id)));
		?>
		<thead>
		<tr>
			<td>
				Report Name
			</td>
			<td>
				Report Group
			</td>
			<td>
				Access Group
			</td>
			<td>
				Format
			</td>
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<?php if($SavedReport){
			foreach ($SavedReport as $value){
				$dataUrl = array();
				if($value->job_category){
					$dataUrl['category']= $value->job_category;
				}
				if($value->location){
					$dataUrl['location']= $value->location;
				}
				if($value->department){
					$dataUrl['department']= $value->department;
				}
				if($value->supplier){
					$dataUrl['supplier']= $value->supplier;
				}
				if($value->access_group){
					$dataUrl['access_group']= $value->access_group;
				}
				if($value->access_group){
					$dataUrl['report_group']= $value->report_group;
				}
				if($value->formate){
					$dataUrl['formate']= $value->formate;
				}
				?>
				<tr>
					<td>
						<?php echo $value->report_name ?>
					</td>
					<td>
						<?php echo $reportGroup[$value->report_group]; ?>
					</td>
					<td>
						<?php echo $accessGroup[$value->access_group]; ?>
					</td>
					<td>
						<?php echo $value->formate ?>
					</td>
					<td>
							<span class="glyphicon glyphicon-download-alt"></span>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/filterReport',$dataUrl) ?>" data-toggle="tooltip" data-placement="top" title="Download Data Sheet" >
								Download
							</a>
					</td>
				</tr>
			<?php } } ?>
		</tbody>
		<?php
	}

	public function actionFilterReport(){
		
		if($_GET['formate'] == 'Excel'){
			header( "Content-Type: application/vnd.ms-excel" );
			header( "Content-disposition: attachment; filename=Report.xls" );
			// for example:
			echo 'S.No'
				. "\t"  . 'Job Id'
				. "\t"  . 'Candidate Id'
				. "\t"  . 'Candidate Name'
				. "\t"  . 'Canddiate Email'
				. "\t"  . 'Start Date'
				. "\t"  . 'End Date'
				. "\t"  . 'Bill Rate'
				. "\t"  . 'Pay Rate'
				. "\t"  . 'Hiring Mgr Name'
				. "\t"  . 'Hiring Mg Department'
				. "\t"  . 'Supplier Name'
				. "\t"  . 'Location'
				. "\t"  . 'Termination Date'
				. "\t"  . 'Status'
				. "\t"  . 'Job Title'
				. "\t"  . 'Job Category'
				. "\n";

			$whereClause = '';
			if(isset($_GET['category']) && $_GET['category'] !==''){
				$whereClause .= " WHERE job_category = '". $_GET['category']. "'";
			}

			if(isset($_GET['location']) && $_GET['location'] !==''){
				$location = Location::model()->findByPk($_GET['location']);
				$whereClause .= " and location = '". $location->name. "'";
			}
			if(isset($_GET['department']) && $_GET['department'] !==''){
				$whereClause .= " and hiring_mgr_department = '". $_GET['department']."'";
			}
			if(isset($_GET['supplier']) && $_GET['supplier'] !==''){
				$whereClause .= " and supplier_name = '". $_GET['supplier']."'";
			}

			 $sql = 'SELECT * FROM rpt_headcount_current '.$whereClause.'';
			$headcountData = Yii::app()->db->createCommand($sql)->query()->readAll();
			$i = 0;
			foreach ($headcountData as $value) {
				$i++;
				echo $i
					. "\t" . $value['job_id']
					. "\t" . $value['candidate_id']
					. "\t" . $value['candidate_name']
					. "\t" . $value['candidate_email']
					. "\t" . $value['start_date']
					. "\t" . $value['end_date']
					. "\t" . $value['bill_rate']
					. "\t" . $value['pay_rate']
					. "\t" . $value['hiring_mgr_name']
					. "\t" . $value['hiring_mgr_department']
					. "\t" . $value['supplier_name']
					. "\t" . $value['location']
					. "\t" . $value['termination_date']
					. "\t" . $value['Status']
					. "\t" . $value['job_title']
					. "\t" . $value['job_category']
					. "\n";
			}
		}
		if($_GET['formate'] == 'CSV'){
			$separator = ';';

			$tmp =  "Job Id; Candidate Id; Candidate Name; Canddiate Email; Start Date; End Date; Bill Rate; Pay Rate; Hiring Mgr Name; Hiring Mg Department;
				Supplier Name; Location; Termination Date; Status; Job Title; Job Category\r\n";

			$whereClause = '';
			if(isset($_GET['category']) && $_GET['category'] !==''){
				$whereClause .= " WHERE job_category = '". $_GET['category']. "'";
			}

			if(isset($_GET['location']) && $_GET['location'] !==''){
				$whereClause .= " and location = '". $_GET['location']. "'";
			}
			if(isset($_GET['department']) && $_GET['department'] !==''){
				$whereClause .= " and hiring_mgr_department = '". $_GET['department']."'";
			}
			if(isset($_GET['supplier']) && $_GET['supplier'] !==''){
				$whereClause .= " and supplier_name = '". $_GET['supplier']."'";
			}

			$sql = 'SELECT * FROM rpt_headcount_current '.$whereClause.'';
			$headcountData = Yii::app()->db->createCommand($sql)->query()->readAll();

			$i = 0;
			foreach ($headcountData as $value) {
				$i++;
				$tmp .=
					 $value['job_id'].
					 ';' . $value['candidate_id'].
					 ';' . $value['candidate_name'].
					 ';' . $value['candidate_email'].
					 ';' . $value['start_date'].
					 ';' . $value['end_date'].
					 ';' . $value['bill_rate'].
					 ';' . $value['pay_rate'].
					 ';' . $value['hiring_mgr_name'].
					 ';' . $value['hiring_mgr_department'].
					 ';' . $value['supplier_name'].
					 ';' . $value['location'].
					 ';' . $value['termination_date'].
					 ';' . $value['Status'].
					 ';' . $value['job_title'].
					 ';' . $value['job_category'].
					 "\r\n";
			}
			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename="export_' . date('d.m.Y') . '.csv"');
			echo iconv('utf-8', 'windows-1251', $tmp);
			Yii::app()->end();
		}
		if($_GET['formate'] == 'PDF'){
			header( "Content-Type: application/vnd.ms-excel" );
			header( "Content-disposition: attachment; filename=Head Count Report.xls" );
		}




	}

	public function actionSaveSchedule(){
		$existrecord = CronejobReport::model()->findByAttributes(array('reciver'=>$_POST['rpt_receivers']));
		if($existrecord){
			echo 'Error';
			exit;
		}
		$model = new CronejobReport;
		$model->client_id = Yii::app()->user->id;
		$model->main_client = UtilityManager::superClient(Yii::app()->user->id);
		$model->report_id = $_POST['report_id'];
		$model->report_group = $_POST['rptr_group'];
		$model->run_frequency = $_POST['rpt_run_frequency'];
		$model->run_day = $_POST['rpt_run_day'];
		$model->start_date = $_POST['startDate'];
		$model->end_date = $_POST['endDate'];
		$model->format = $_POST['rpt_formate'];
		$model->reciver = $_POST['rpt_receivers'];
		$model->date_created = date('Y-m-d');
		if($model->save()){
			echo 'Success';
		}else{
			echo 'Error';
		}
	}
	public function actionheadcountTrend(){
		
		$whereClause = '';
		$filterCategory = '';
		$filterLocation = '';
		
		if(isset($_GET['category']) && $_GET['category'] !==''){
			$whereClause = " WHERE job_category = '" . $_GET['category']. "' ";
			$filterCategory = $_GET['category'];
		}
		
		if(isset($_GET['location']) && $_GET['location'] !==''){
			if($whereClause == ''){
				$whereClause = " WHERE location = '" . $_GET['location']. "' ";
			}
			else{
				$whereClause = $whereClause . " AND location = '" . $_GET['location']. "' ";
			}
			$filterLocation = $_GET['location'];
		}
		
		$sql = 'SELECT * FROM rpt_headcount_yearmonth ' . $whereClause . ' ORDER BY 1';
		$headcountData = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$sql = 'SELECT  DS.YEAR_MONTH_ID, document_solutions, professional, network_security
			FROM (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS document_solutions
			  FROM rpt_headcount_yearmonth 
			  WHERE job_category = \'Document Solutions\'
			  GROUP BY YEAR_MONTH_ID
			) AS DS
			LEFT JOIN (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS professional 
			  FROM rpt_headcount_yearmonth 
			  WHERE job_category = \'Professional\'
			  GROUP BY YEAR_MONTH_ID
			) AS P
			  ON DS.YEAR_MONTH_ID = P.YEAR_MONTH_ID
			LEFT JOIN (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS network_security 
			  FROM rpt_headcount_yearmonth 
			  WHERE job_category = \'Network Security\'
			  GROUP BY YEAR_MONTH_ID
			) AS NS
			  ON DS.YEAR_MONTH_ID = NS.YEAR_MONTH_ID';
		$headcountByCategory = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$sql = '
			SELECT DS.YEAR_MONTH_ID, Auxilio, Other
			FROM (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS Auxilio
			  FROM rpt_headcount_yearmonth 
			  WHERE hiring_mgr_department = \'Auxilio\'
			  GROUP BY YEAR_MONTH_ID
			) AS DS
			LEFT JOIN (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS Other 
			  FROM rpt_headcount_yearmonth 
			  WHERE hiring_mgr_department = \'\'
			  GROUP BY YEAR_MONTH_ID
			) AS P
			  ON DS.YEAR_MONTH_ID = P.YEAR_MONTH_ID';
		$headcountByDepartment= Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$sql = 'SELECT DS.YEAR_MONTH_ID, CGS
			FROM (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS CGS
			  FROM rpt_headcount_yearmonth 
			  WHERE supplier_name = \'CGS Business Solutions\'
			  GROUP BY YEAR_MONTH_ID
			) AS DS';
		$headcountBySupplier = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$sql = '
			SELECT  DS.YEAR_MONTH_ID, BSH10009, BSH10003, BSH10007
			FROM (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS BSH10009
			  FROM rpt_headcount_yearmonth 
			  WHERE location = \'Bon Secours  Goshen Medical Associates( BSH1-0009 )\'
			  GROUP BY YEAR_MONTH_ID
			) AS DS
			LEFT JOIN (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS BSH10003 
			  FROM rpt_headcount_yearmonth 
			  WHERE location = \'BS Schervier Nursing Care Center( BSH1-0003 )\'
			  GROUP BY YEAR_MONTH_ID
			) AS P
			  ON DS.YEAR_MONTH_ID = P.YEAR_MONTH_ID
			LEFT JOIN (
			  SELECT YEAR_MONTH_ID, COUNT(1) AS BSH10007 
			  FROM rpt_headcount_yearmonth 
			  WHERE location = \'Bon Secours Health System, Inc.( BSH1-0007 )\'
			  GROUP BY YEAR_MONTH_ID
			) AS NS
			  ON DS.YEAR_MONTH_ID = NS.YEAR_MONTH_ID';
		$headcountByLocation = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$sql = 'SELECT DISTINCT job_category FROM rpt_headcount_current';
		$jobCategoryList = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = 'SELECT DISTINCT location FROM rpt_headcount_current';
		$locationList = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = 'SELECT DISTINCT hiring_mgr_department as department FROM rpt_headcount_current';
		$departmentList = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$sql = 'SELECT DISTINCT supplier_name FROM rpt_headcount_current';
		$supplierList = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$this->render('headcountTrend',array('headcountData'=>$headcountData, 'headcountByCategory' => $headcountByCategory, 'headcountByDepartment' => $headcountByDepartment, 'headcountBySupplier' => $headcountBySupplier, 'headcountByLocation' => $headcountByLocation, 'jobCategoryList' => $jobCategoryList, 'locationList' => $locationList, 'filterCategory' => $filterCategory, 'filterLocation' => $filterLocation, 'departmentList' => $departmentList, 'supplierList' => $supplierList));
	}
	
	public function FirstChartScript($category){ ?>
		<script>
        	//first chart
			 var browserDataFirst = [{
				label: 'Pending Approval',
				data: <?php echo $this->JobCountingOnCategory($category,1); ?>,
				color: '#ff5a5f'
			  }, {
				label: 'Open',
				data: <?php echo $this->JobCountingOnCategory($category,3); ?>,
				color: '#ffb400'
			  }, {
				label: 'Re-open',
				data: <?php echo $this->JobCountingOnCategory($category,6); ?>,
				color: '#007a87'
			  }, {
				label: 'Rejected',
				data: <?php echo $this->JobCountingOnCategory($category,5); ?>,
				color: '#6c6441'
			  },{
				label: 'Filled ',
				data: <?php echo $this->JobCountingOnCategory($category,4); ?>,
				color: '#b4a76c'
			  },
			  {
				label: 'Hold',
				data: <?php echo $this->JobCountingOnCategory($category,10); ?>,
				color: '#7b0051'
			  },
			  {
				label: 'New Request',
				data: <?php echo $this->JobCountingOnCategory($category,11); ?>,
				color: '#8ce071'
			  },
			   {
				label: 'Draft',
				data: <?php echo $this->JobCountingOnCategory($category,2); ?>,
				color: '#9F00FF'
			  }];
			  
			  $.plot($('.pie'), browserDataFirst, {
				series: {
				  pie: {
					show: true,
					innerRadius: 0.5,
					stroke: {
					  width: 0
					},
					label: {
					  show: true
					}
				  }
				},
				legend: {
				  show: true
				}
			  });
        </script>
		<?php }

	
}