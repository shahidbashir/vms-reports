<?php
class CalendarController extends Controller
{
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $model = new Calendar();
        $dataProvider = new CArrayDataProvider($model->events, array(
            'keyField'=>'event_id',
            'sort'=>array(
                'attributes'=>array(
                    'event_id'
                ),
            ),
            'pagination'=>array(
                'pageSize'=>10,
            ),
        ));

        $this->render('index', array('dataProvider' => $dataProvider));
    }

    function getLink($event)
    {
        $loggedIn = !is_null(Yii::app()->session['access_token']);

        if($loggedIn)
            echo CHtml::link('Add to Calendar', array('calendar/add', 'event_id' => $event['event_id']));
        else
            echo  CHtml::link('Login', oAuthService::getLoginUrl($this->createAbsoluteUrl('calendar/authorize')));
    }

    public function actionAuthorize()
    {
        $auth_code = $_GET['code'];
        $redirectUri = $this->createAbsoluteUrl('calendar/authorize');

        $tokens = oAuthService::getTokenFromAuthCode($auth_code, $redirectUri);

        if ($tokens['access_token']) {
            Yii::app()->session['access_token'] = $tokens['access_token'];
            Yii::app()->session['refresh_token'] = $tokens['refresh_token'];

            // expires_in is in seconds
            // Get current timestamp (seconds since Unix Epoch) and
            // add expires_in to get expiration time
            // Subtract 5 minutes to allow for clock differences
            $expiration = time() + $tokens['expires_in'] - 300;
            Yii::app()->session['token_expires'] = $expiration;

            // Get the user's email
            $user = OutlookService::getUser($tokens['access_token']);
            Yii::app()->session['user_email'] = $user['EmailAddress'];

            // Redirect back to home page
            Yii::app()->user->setFlash('success', "You are now logged in. Click on Add to Calendar");
        }
        else
        {
            Yii::app()->user->setFlash('error', $tokens['error']);
        }

        $this->redirect($this->createAbsoluteUrl('calendar/index'));
    }

    public function actionAdd($event_id){
        $model = new Calendar();
        $result = $model->addEventToCalendar($event_id-1);
        
        if(isset($result['errorNumber']))
            Yii::app()->user->setFlash('error', "Unable to add the event. Please try again");
        else
            Yii::app()->user->setFlash('success', "Event added to the calendar successfully");

        $this->redirect($this->createAbsoluteUrl('calendar/index'));

    }
}