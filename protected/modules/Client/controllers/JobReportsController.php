<?php

class JobReportsController extends Controller
{
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('categoryXcel','xcel','index','monthlyReports','categoryjobreport'),
				'users'=>array('@'),
			),
		);
	}
	public function actionCategoryXcel(){
		$category = '';
		if(isset($_GET['cat'])){
			$category = $_GET['cat'];
		}
		if(isset($_GET['type']) && $_GET['type']=='all'){
			$jobs = Job::model()->findAllByAttributes(array('jobstep2_complete'=>1,'cat_id'=>$category));

		}
		if(isset($_GET['type']) && $_GET['type']=='approved'){
			$jobs = Job::model()->findAllByAttributes(array('jobStatus'=>11,'jobstep2_complete'=>1,'cat_id'=>$category));
		}
		if(isset($_GET['type']) && $_GET['type']=='pending'){
			$jobs = Job::model()->findAllByAttributes(array('jobStatus'=>1,'jobstep2_complete'=>1,'cat_id'=>$category));
		}
		header( "Content-Type: application/vnd.ms-excel" );
		header( "Content-disposition: attachment; filename=CategoryWise Job Report.xls" );

		echo 'S.No' . "\t" . 'Date ( Active from )' . "\t" . 'Job ID'. "\t" . 'Job Title'. "\t" . 'Job Category'. "\t" . 'Bill Rate'. "\t" . 'Opening'. "\t" . 'Location'. "\t" . 'Bill Code'. "\t" . 'Job Request Code'. "\t" . 'Client'. "\t" . 'Type of Job'. "\t" . 'Duration ( Start Date )'. "\t" . 'Duration ( End Date )'. "\t" . 'Total Days'. "\t" . 'Hours'. "\t" . 'Estimated Cost'. "\t" . 'Job Approval'. "\t" . 'Status'. "\t" . 'Created By ' . "\n";
		$i =0;
		foreach ($jobs as $jobvalue){
			$postingstatus = UtilityManager::jobStatus();
			switch ($postingstatus[$jobvalue->jobStatus]) {
				case "Pending Approval":
					$color = 'label-pending-aproval';
					break;
				case "Open":
					$color = 'label-open';
					break;
				case "Filled":
					$color = 'label-filled';
					break;
				case "Rejected":
					$color = 'label-rejected';
					break;
				case "Re-open":
					$color = 'label-re-open';
					break;
				case "Hold":
					$color = 'label-hold';
					break;
				case "New Request":
					$color = 'label-new-request';
					break;
				case "Draft":
					$color = 'tag label-draft';
					break;

				default:
					$color = 'label-new-request';
			}

			if($jobvalue->user_subclient_id==0){
				$clientId = $jobvalue->user_id;
			}else {
				$clientId = $jobvalue->user_subclient_id;
			}
			$client = Client::model()->findByPk($clientId);
			$clientreocrd = $client->first_name .' '.$client->last_name;
			$workflowName = '';
			$jobapproval = JobWorkflow::model()->findAllByAttributes(array('job_id'=>$jobvalue->id));
			if($jobapproval){
				foreach ($jobapproval as $approvaldata) {
					$client = Client::model()->findByPk($approvaldata->client_id);
					$workflowName .= $client->first_name . ' ,';
				}
			}
			$Jobclient = ProClientNames::model()->findByPk($jobvalue->client_name);
			$clientName='';
			if($Jobclient){
				$clientName = $Jobclient->name;
			}
			$locationID = unserialize($jobvalue->location);
			$locationName = '';
			foreach ($locationID as $lId){
				$location = Location::model()->findByPk($lId);
				$locationName.= $location->name.' ,';
			}
			$i++;
			echo $i . "\t" . $jobvalue->release_date . "\t" . $jobvalue->id . "\t" . $jobvalue->title. "\t" . $jobvalue->cat_id. "\t" . $jobvalue->bill_rate. "\t" . $jobvalue->num_openings. "\t" . $locationName. "\t" . $jobvalue->billing_code. "\t" . $jobvalue->request_dept. "\t" . $clientName . "\t" . $jobvalue->type. "\t" . $jobvalue->job_po_duration. "\t" . $jobvalue->job_po_duration_endDate. "\t" . 'Today Days'. "\t" . 'Hours'. "\t" . $jobvalue->pre_total_estimate_code. "\t" . $workflowName. "\t" . $postingstatus[$jobvalue->jobStatus]. "\t" . $clientreocrd . "\n";
		}

	}
	public function actionXcel(){

		if(isset($_GET['type']) && $_GET['type']=='all'){
			$jobs = Job::model()->findAllByAttributes(array('jobstep2_complete'=>1));

		}
		if(isset($_GET['type']) && $_GET['type']=='approved'){
			$jobs = Job::model()->findAllByAttributes(array('jobStatus'=>11,'jobstep2_complete'=>1));
		}
		if(isset($_GET['type']) && $_GET['type']=='pending'){
			$jobs = Job::model()->findAllByAttributes(array('jobStatus'=>1,'jobstep2_complete'=>1));
		}
		header( "Content-Type: application/vnd.ms-excel" );
		header( "Content-disposition: attachment; filename= Job Report.xls" );

		echo 'S.No' . "\t" . 'Date ( Active from )' . "\t" . 'Job ID'. "\t" . 'Job Title'. "\t" . 'Job Category'. "\t" . 'Bill Rate'. "\t" . 'Opening'. "\t" . 'Location'. "\t" . 'Bill Code'. "\t" . 'Job Request Code'. "\t" . 'Client'. "\t" . 'Type of Job'. "\t" . 'Duration ( Start Date )'. "\t" . 'Duration ( End Date )'. "\t" . 'Total Days'. "\t" . 'Hours'. "\t" . 'Estimated Cost'. "\t" . 'Job Approval'. "\t" . 'Status'. "\t" . 'Created By ' . "\n";
		$i =0;
		foreach ($jobs as $jobvalue){
			$date1 = DateTime::createFromFormat('m/d/Y',$jobvalue->job_po_duration);
			$date2 = DateTime::createFromFormat('m/d/Y',$jobvalue->job_po_duration_endDate);
			$numberOfdays = $date2->diff($date1)->format("%a");
			$t1 = StrToTime ($jobvalue->job_po_duration_endDate);
			$t2 = StrToTime ($jobvalue->job_po_duration);
			$diff = $t1 - $t2;
			$hours = $diff / ( 60 * 60 );
			$postingstatus = UtilityManager::jobStatus();
			switch ($postingstatus[$jobvalue->jobStatus]) {
				case "Pending Approval":
					$color = 'label-pending-aproval';
					break;
				case "Open":
					$color = 'label-open';
					break;
				case "Filled":
					$color = 'label-filled';
					break;
				case "Rejected":
					$color = 'label-rejected';
					break;
				case "Re-open":
					$color = 'label-re-open';
					break;
				case "Hold":
					$color = 'label-hold';
					break;
				case "New Request":
					$color = 'label-new-request';
					break;
				case "Draft":
					$color = 'tag label-draft';
					break;

				default:
					$color = 'label-new-request';
			}

			if($jobvalue->user_subclient_id==0){
				$clientId = $jobvalue->user_id;
			}else {
				$clientId = $jobvalue->user_subclient_id;
			}
			$client = Client::model()->findByPk($clientId);
			$clientreocrd = $client->first_name .' '.$client->last_name;
			$workflowName = '';
			$jobapproval = JobWorkflow::model()->findAllByAttributes(array('job_id'=>$jobvalue->id));
			if($jobapproval){
				foreach ($jobapproval as $approvaldata) {
					$client = Client::model()->findByPk($approvaldata->client_id);
					$workflowName .= $client->first_name . ' ,';
				}
			}
			$Jobclient = ProClientNames::model()->findByPk($jobvalue->client_name);
			$clientName='';
			if($Jobclient){
				$clientName = $Jobclient->name;
			}
			$locationID = unserialize($jobvalue->location);
			$locationName = '';
			foreach ($locationID as $lId){
				$location = Location::model()->findByPk($lId);
				$locationName.= $location->name.' ,';
			}
			$i++;
		echo $i . "\t" . $jobvalue->release_date . "\t" . $jobvalue->id . "\t" . $jobvalue->title. "\t" . $jobvalue->cat_id. "\t" . $jobvalue->bill_rate. "\t" . $jobvalue->num_openings. "\t" . $locationName. "\t" . $jobvalue->billing_code. "\t" . $jobvalue->request_dept. "\t" . $clientName . "\t" . $jobvalue->type. "\t" . $jobvalue->job_po_duration. "\t" . $jobvalue->job_po_duration_endDate. "\t" . $numberOfdays . "\t" . $hours . "\t" . $jobvalue->pre_total_estimate_code. "\t" . $workflowName. "\t" . $postingstatus[$jobvalue->jobStatus]. "\t" . $clientreocrd . "\n";
		}

	}
	public  function actionCategoryjobreport(){
		if(isset($_POST['category'])){

			$category = $_POST['category']; 
			
			
			?>
			<script> var jobsbycatData = [

					["Pending Approval", <?php echo $this->JobCountingOnCategory($category,1); ?>],
					["Open", <?php echo $this->JobCountingOnCategory($category,3); ?>],
					["Re-open", <?php echo $this->JobCountingOnCategory($category,6); ?>],
					["Rejected", <?php echo $this->JobCountingOnCategory($category,5); ?>],
					["Filled", <?php echo $this->JobCountingOnCategory($category,4); ?>],
					["Hold", <?php echo $this->JobCountingOnCategory($category,10); ?>],
					["New Request", <?php echo $this->JobCountingOnCategory($category,11); ?>],
					["Draft", <?php echo $this->JobCountingOnCategory($category,2); ?>]
				];

				var jobsbycatChart = anychart.pie( jobsbycatData );
				jobsbycatChart.title("Job Category Request Report");
				// pass the container id, chart will be displayed there
				jobsbycatChart.container("job-by-cat");
				// call the chart draw() method to initiate chart display
				jobsbycatChart.draw();
			</script>
			<?php
		}


	}
	public function actionIndex()
	{
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
			}
		//2nd graph
		//job category report
		$job_cat_reportData = '';
		$jobCategory = Setting::model()->findAll(array('condition'=>'category_id=9'));
		foreach($jobCategory as $cat){
			$jobs = Job::model()->countByAttributes(array('user_id'=>$loginUser,'cat_id'=>$cat->title));
			$job_cat_reportData .= "['".$cat->title."', $jobs],";
			}
		
		//Monthly Job Report Data 3rd graph
		$i = 30;
		$todayDate = date('Y-m-d');
		$backDate = date('Y-m-d',strtotime(date('Y-m-d') . " -".$i." days"));
		//echo $this->DateRangeStatusCount($todayDate,$backDate,3);
		$monthlyJobReport = '';
		
		$monthlyJobReport .= "['Pending Approval', ".$this->DateRangeStatusCount($todayDate,$backDate,1)."],";
		$monthlyJobReport .= "['Open', ".$this->DateRangeStatusCount($todayDate,$backDate,3)."],";
		$monthlyJobReport .= "['Re-open', ".$this->DateRangeStatusCount($todayDate,$backDate,6)."],";
		$monthlyJobReport .= "['Rejected', ".$this->DateRangeStatusCount($todayDate,$backDate,5)."],";
		$monthlyJobReport .= "['Filled', ".$this->DateRangeStatusCount($todayDate,$backDate,4)."],";
		$monthlyJobReport .= "['Hold', ".$this->DateRangeStatusCount($todayDate,$backDate,10)."],";
		$monthlyJobReport .= "['New Request', ".$this->DateRangeStatusCount($todayDate,$backDate,11)."],";
		$monthlyJobReport .= "['Draft', ".$this->DateRangeStatusCount($todayDate,$backDate,2)."],";
		
		
		$this->render('index',array('job_cat_reportData'=>$job_cat_reportData,'monthlyJobReport'=>$monthlyJobReport));
	}
	public function actionMonthlyReports(){

		//$i = 30;
		$todayDate = date('Y-m-d',strtotime($_POST['first_date']));
		$backDate = date('Y-m-d',strtotime($_POST['end_date']));
		//$todayDate = $_POST['first_date'];
		//$backDate = $_POST['end_date'];
		//echo $this->DateRangeStatusCount($todayDate,$backDate,3);
		$monthlyJobReport = '';

		$monthlyJobReport .= "['Pending Approval', ".$this->DateRangeStatusCount($todayDate,$backDate,1)."],";
		$monthlyJobReport .= "['Open', ".$this->DateRangeStatusCount($todayDate,$backDate,3)."],";
		$monthlyJobReport .= "['Re-open', ".$this->DateRangeStatusCount($todayDate,$backDate,6)."],";
		$monthlyJobReport .= "['Rejected', ".$this->DateRangeStatusCount($todayDate,$backDate,5)."],";
		$monthlyJobReport .= "['Filled', ".$this->DateRangeStatusCount($todayDate,$backDate,4)."],";
		$monthlyJobReport .= "['Hold', ".$this->DateRangeStatusCount($todayDate,$backDate,10)."],";
		$monthlyJobReport .= "['New Request', ".$this->DateRangeStatusCount($todayDate,$backDate,11)."],";
		$monthlyJobReport .= "['Draft', ".$this->DateRangeStatusCount($todayDate,$backDate,2)."],";
		?>
		<script>
			var data = anychart.data.set([
				<?php echo $monthlyJobReport; ?>
			]);

			// map data for the each series
			var Sales2003 = data.mapAs({x: [0], value: [1]});

			// chart type
			chart = anychart.column();

			// set title
			chart.title("Monthly Job Report");

			// set series data
			chart.column(Sales2003);

			// set axes titles
			var xAxis = chart.xAxis();
			xAxis.title("Status");
			var yAxis = chart.yAxis();
			yAxis.title("Jobs");

			// draw
			chart.container("monthly-job-report");
			chart.draw();
		</script>

		<?php
	}
	
	public function DateRangeStatusCount($todayDate,$backDate,$status){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
			}
		$criteria = new CDbCriteria();
		$criteria->addCondition("jobStatus = ".$status);
		
		$criteria->addBetweenCondition("date_created",$backDate,$todayDate,'AND');
		$jobs = Job::model()->count($criteria);
		return $jobs;
		}
	
	public function JobCountingOnStatus($status){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
			}
		$jobs = Job::model()->countByAttributes(array('user_id'=>$loginUser,'jobStatus'=>$status));
		return $jobs;
		}
		
	public function JobCountingOnCategory($category,$status){
		$loginUser = Yii::app()->user->id;
		$client = Client::model()->findByPk($loginUser);
		if($client->super_client_id != 0){
			$loginUser = $client->super_client_id;
			}
		$jobs = Job::model()->countByAttributes(array('user_id'=>$loginUser,'cat_id'=>$category,'jobStatus'=>$status));
		return $jobs;
		}
}