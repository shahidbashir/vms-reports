<?php
class SettinggeneralController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('clientbaseOn','changeClientstatus','teaminfo','importRecord','deletejobpostingCat','jobpostingConfiguration','workHourconfig','deleteCWorkflow','expenses','deleteExpense','conWorkFlowMembers','contractWorkFlows','deleteCWFmember','contractWorkFlow','deleteCName','addCName','workflowType','index','changPassword','workflowConfiguration','deleteWorkflowConfiguration','timeSheetcode','categoryConfiguration','deletecategoryconfig','deleteTimesheetcode','addWorkFlow','workFlows','members','createteammember','createdepartment','createlocation','createbillingcode','createjobrequest','deleteteammember','deletelocation','deletedepartment','deletebillingcode','deletejobrequest','deleteWFmember','editProfile','workFlowMembers','settingExhibit','deletefileExhibit','backgroundverification','deleteWorkflow','extension','timesheetConfig','timesheetConfig1' ,'addLocation' ,'deleteCLocation' ,'notificationAlert' ,'deleteNotificationAlert'),
				'users'=>array('@'),
			),
			array('allow', //createteammember allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionTeaminfo(){
		if(isset($_POST['teammemberId'])) {
			$id = $_POST['teammemberId'];
			$ClientInfo = Client::model()->findByPk($id);
			$modeldata = JobPostingConfig::model()->findAllByAttributes(array('teammember_id'=>$id));
			?>
			<table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">

				<tbody>
				<tr>
					<td>Name</td>
					<td><?php echo $ClientInfo->first_name.' '.$ClientInfo->last_name; ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><?php echo $ClientInfo->email; ?></td>
				</tr>
				<tr>
					<td>Department</td>
					<td><?php echo $ClientInfo->department; ?></td>
				</tr>
				<tr>
					<td>Member Type</td>
					<td><?php echo $ClientInfo->member_type; ?></td>
				</tr>
				<?php if($modeldata) { ?>
				<tr><th>Job Posting Category</th></tr>
					<?php
					$i = 0;
					foreach($modeldata as $value ) {
						$i++;
					$category = Setting::model()->findByPk($value->cat_id);?>
				<tr>
					<td>
					<?php echo $category->title; ?>
					</td>
				</tr>
					<?php } ?>
				<?php } ?>
				</tbody>
			</table>
			<?php
		}
	}
	
	public function actionImportRecord(){

		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		if(isset($_POST['ImportData'])){
			$contractModel = Contract::model()->findByPk($_POST['contract_id']);
			$workorderModel = Workorder::model()->findByPk($contractModel->workorder_id);
			$workorderModel->wo_bill_rate = $_POST['bill_rate'];
			$workorderModel->wo_pay_rate = $_POST['pay_rate'];
			$workorderModel->wo_over_time = $_POST['over_time'];
			$workorderModel->wo_double_time = $_POST['double_time'];
			$workorderModel->wo_client_over_time = $_POST['client_over_time'];
			$workorderModel->wo_client_double_time = $_POST['client_double_time'];
			$workorderModel->approval_manager = $_POST['approval_manager'];
			if(isset($_POST['start_date'])){
				$workorderModel->wo_start_date = date('Y/m/d',strtotime($_POST['start_date']));
				$workorderModel->onboard_changed_start_date = date('Y/m/d',strtotime($_POST['start_date']));
			}
			if(isset($_POST['end_date'])) {
				$workorderModel->wo_end_date = date('Y/m/d',strtotime($_POST['end_date']));
				$workorderModel->onboard_changed_end_date = date('Y/m/d',strtotime($_POST['end_date']));
			}
			if($workorderModel->save(false)){
				if(isset($_POST['start_date'])) {
					$contractModel->start_date = date('Y/m/d',strtotime($_POST['start_date']));
					}
				if(isset($_POST['end_date'])) {
					$contractModel->end_date = date('Y/m/d',strtotime($_POST['end_date']));
					}
				$contractModel->save(false);
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong>  Data saved successfully. </div>');
			}
		}
		
		$criteria=new CDbCriteria();
		$criteria->condition = 'client_id ='.$loginUserId;
		$criteria->condition = 'import_record = 1';
		$criteria->order = 'id DESC';
		$count=Contract::model()->count($criteria);
		$pages=new CPagination($count);

		// results per page
		$pages->pageSize=10;
		$pages->applyLimit($criteria);
		$models=Contract::model()->findAll($criteria);

		$this->render('importRecord', array(
			'models' => $models,
			'pages' => $pages
		));
	}
    public function actionDeletejobpostingCat(){
        if(isset($_GET['Config_id'])){
            $configID = $_GET['Config_id'];
            $model = JobPostingConfig::model()->findByPk($configID);
            if($model->delete()) {
                Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong>  Deleted successfully. </div>');
            }
        }

            $this->redirect(array('jobpostingConfiguration','id'=>$_GET['id']));

    }
    public function actionJobpostingConfiguration(){

        $model = new JobPostingConfig;
        $clientID = Yii::app()->user->id;
        $mainclient = Yii::app()->user->id;
        $client = Client::model()->findByPk($mainclient);
        if($client->member_type!=NULL){
            $client = Client::model()->findByPk($client->super_client_id);
            $mainclient = $client->id;
        }
        if(isset($_POST['JobPostingConfig']))
        {
            $model->attributes=$_POST['JobPostingConfig'];
            $model->created_by_id = $clientID;
            $model->super_client_id = $mainclient;
            $model->date_created=date('Y-m-d');
            if($model->save(false)) {
                Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Added successfully. </div>');
            }else{
                Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue. </div>');
            }
            $this->redirect(array('jobpostingConfiguration','id'=>$_GET['id']));

        }
        $modeldata = JobPostingConfig::model()->findAllByAttributes(array('teammember_id'=>$_GET['id']));

        $this->render('jobpostingConfiguration',array('model'=>$model,'modeldata'=>$modeldata));
    }
	public function actionWorkHourconfig(){

		$clientID = Yii::app()->user->id;
		$mainclient = Yii::app()->user->id;
		$client = Client::model()->findByPk($mainclient);
		if($client->member_type!=NULL){
			$client = Client::model()->findByPk($client->super_client_id);
			$mainclient = $client->id;
		}

		$model = new WorkHourConfiguration;
		if(isset($_POST['WorkHourConfiguration']))
		{
			$model->attributes=$_POST['WorkHourConfiguration'];
			$model->client_id = $clientID;
			$model->main_client = $mainclient;
			$model->date_created=date('Y-m-d');
			if($model->save(false)) {
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Added successfully. </div>');
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue. </div>');
			}
			$this->redirect(array('workHourconfig'));

		}
			$WorkHourConfiguration = WorkHourConfiguration::model()->findByAttributes(array('main_client'=>$clientID));
		$this->render('workHourconfig',array('model'=>$model,'WorkHourConfiguration'=>$WorkHourConfiguration));
	}
	public function actionClientbaseOn(){

		$clientID  = UtilityManager::superClient(Yii::app()->user->id);
		$timesheetConfig = TimesheetConfig::model()->findByAttributes(array('client_id'=>$clientID));

		if($timesheetConfig){
			$model = $timesheetConfig;
		}else{
			$model = new TimesheetConfig;
		}

		if(isset($_POST['typeForm'])){
			$model->client_id = $clientID;
			$model->type = $_POST['type'];
			$model->status = 1;
			$model->date_created = date('Y-m-d');
			if($model->save(false)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong>Success!</strong> Time Sheet Configurtion Saved Successfully. </div>');
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <strong>Error!</strong> Having some issue while Saving Time Sheet Configurtion. </div>');
			}
		}

		$this->render('clientbaseOn',array('model'=>$model));
	}
	public function actionTimesheetConfig(){
		
		if(isset($_POST['first_form'])){
			Yii::app()->session['first_form'] = $_POST;
				$this->redirect(array('timesheetConfig1'));
			}
		$clientID = Yii::app()->user->id;
		$mainclient = Yii::app()->user->id;
		$client = Client::model()->findByPk(Yii::app()->user->id);
		if($client->member_type!=NULL){
			$client = Client::model()->findByPk($client->super_client_id);
			$mainclient = $client->id;
		}
		$model = TimesheetHoursConfig::model()->findAllByAttributes(array('client_id'=>array($clientID,$mainclient)));
		$this->render('timesheetConfig',array('model'=>$model));
		}
		
	public function actionTimeSheetConfig1(){
			
			$userID = Yii::app()->user->id;
			$firstForm = Yii::app()->session['first_form'];
			$location = Location::model()->findByPk($firstForm['location']);
			
			$clientID = Yii::app()->user->id;
			$mainclient = Yii::app()->user->id;
			$client = Client::model()->findByPk(Yii::app()->user->id);
			if($client->member_type!=NULL){
				$client = Client::model()->findByPk($client->super_client_id);
				$mainclient = $client->id;
			}
			
			$model = TimesheetHoursConfig::model()->findByAttributes(array('client_id'=>array($clientID,$mainclient),'location_id'=>$firstForm['location']));
			
			
			if(empty($model)){
				$model = new TimesheetHoursConfig;
				}
				
			/*echo '<pre>';
			print_r($model);
			exit;*/
			
			if(isset($_POST['daily_form'])){
					
					$model->attributes = $_POST['TimesheetHoursConfig'];
					$model->client_id = $mainclient;
					$model->created_by_id = $userID;
					$model->date_created = date('Y-m-d');
					$model->location_id = $firstForm['location'];
					$model->location_name = $location->name;
					$model->hours_type = $firstForm['type'];
					if($model->save(false)){
						Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Timesheet configuration added successfully. </div>');
			   		$this->redirect(array('timeSheetConfig'));
						}
				}
			
			if(isset($_POST['weekly_form'])){
					
					$model->attributes = $_POST['TimesheetHoursConfig'];
					$model->client_id = $userID;
					$model->date_created = date('Y-m-d');
					$model->location_id = $firstForm['location'];
					$model->location_name = $location->name;
					$model->hours_type = $firstForm['type'];
					if($model->save(false)){
						Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Timesheet configuration added successfully. </div>');
			   		$this->redirect(array('timeSheetConfig'));
						}
				}
			
			$this->render('timesheetConfig1',array('model'=>$model));
		}
	
	public function actionDeleteExpense($id){
		$model = Expenses::model()->findByAttributes(array('client_id'=>Yii::app()->user->id,'id'=>$id));
		if(empty($model)){
			$this->redirect(array('expenses'));
		}
		if($model->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Expenses code deleted successfully. </div>');
		}else{
			Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue in deletion. </div>');
		}
		$this->redirect(array('expenses'));
	}
	public function actionExpenses(){
		$model = new Expenses;
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		if(isset($_POST['Expenses']))
		{

			$model->attributes=$_POST['Expenses'];
			$model->client_id=$userId;
			$model->date_created=date('Y-m-d');
			if($model->save(false)) {
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Added successfully. </div>');
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue. </div>');
			}
			$this->redirect(array('expenses'));

		}
		$expenseRecord = Expenses::model()->findAllByAttributes(array('client_id'=>$userId));
		$this->render('expense',array('model'=>$model,'expenseRecord'=>$expenseRecord));
	}
	public function actionAddCName(){
				
		$clientID = Yii::app()->user->id;
		$mainclient = Yii::app()->user->id;
		$client = Client::model()->findByPk($mainclient);
		if($client->member_type!=NULL){
			$client = Client::model()->findByPk($client->super_client_id);
			$mainclient = $client->id;
		}

		error_reporting(0);
		$model=new ProClientNames;
		
		if(isset($_POST['submit'])){
			$model->name = $_POST['name'];
			$model->client_id = $clientID;
			$model->main_client = $mainclient;
			$model->save(false);
			}
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$clientInfo = Client::model()->findByPk($userId);
		$record = ProClientNames::model()->findAllByAttributes(array('main_client'=>$mainclient));
		$this->render('addCName',array('model'=>$model,'record'=>$record,'clientInfo'=>$clientInfo));
		
	}
	
	public function actionDeleteCName($id){
		 ProClientNames::model()->deleteByPk($id);
		 Yii::app()->user->setFlash('success', '<div class="alert alert-success">
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <strong>Success!</strong> Data deleted successfully. </div>');
		 $this->redirect(array('addCName'));
	}
	public function actionDeleteCLocation($id){
		 ClientLocations::model()->deleteByPk($id);
		 Yii::app()->user->setFlash('success', '<div class="alert alert-success">
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <strong>Success!</strong> Location deleted successfully. </div>');
		 $this->redirect(array('addLocation'));
    }
	public function actionDeleteNotificationAlert($id){
		 GuestNotifications::model()->deleteByPk($id);
		 Yii::app()->user->setFlash('success', '<div class="alert alert-success">
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <strong>Success!</strong> Data deleted successfully. </div>');
		 $this->redirect(array('notificationAlert'));
	}
	public function actionChangeClientstatus(){
		if(isset($_POST['status'])){
			$status =  $_POST['status'];
			$userId = UtilityManager::superClient(Yii::app()->user->id);
			$clientInfo = Client::model()->findByPk($userId);
			$clientInfo->client_job_status = $status;
			if($clientInfo->save(false)){
				echo 'Success';
			}

		}
	}
	
	public function actionWorkflowType(){
		$clientID = UtilityManager::superClient(Yii::app()->user->id);
		$model = WorkflowType::model()->findByAttributes(array('client_id'=>$clientID));
		if(empty($model)){
			$model = new WorkflowType;
			}
		if(isset($_POST['workflow_process'])){
			$model->client_id = $clientID;
			$model->workflow_process = $_POST['workflow_process'];
			if($model->save(false)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Setting saved successfully. </div>');
				}
			}
		$this->render('workflowType',array('model'=>$model));
		}

	public function actionWorkflowConfiguration(){
		//error_reporting(0);
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$teamMembers = Client::model()->findAll();
		$model = new WorkflowConfiguration;
		if(isset($_POST['WorkflowConfiguration'])){
			$model->attributes=$_POST['WorkflowConfiguration'];
			$model->client_id = $userId;
			$model->date_created = date('Y-m-d');
			if($model->save()){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Data saved successfully. </div>');
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
					 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					 <strong>Error!</strong> Having some issue while Saving Data. </div>');
			}
			$this->redirect(array('workflowConfiguration'));
		}
		$WorkflowConfiguration = WorkflowConfiguration::model()->findAllByAttributes(array('client_id'=>$userId));

		$this->render('workflowConfiguration',array('teamMembers'=>$teamMembers,'model'=>$model,'WorkflowConfiguration'=>$WorkflowConfiguration));
	}

	public function actionDeleteWorkflowConfiguration(){

		if(isset($_GET['id'])){

			WorkflowConfiguration::model()->deleteByPk($_GET['id']);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Data deleted successfully. </div>');
			$this->redirect(array('workflowConfiguration'));
		}
	}

	public function actionCategoryConfiguration(){
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$model = new CategoryConfiguration;

		if(isset($_POST['CategoryConfiguration'])){
			$model->attributes=$_POST['CategoryConfiguration'];
			$costCenter = $_POST['cost_center'];
			$costcenter = '';
			$category = $_POST['CategoryConfiguration']['category_id'];
			foreach($costCenter as $value){
				$q = new CDbCriteria();
				$q->addCondition('category_id=:category_id');
				$q->params=array(':category_id'=>$category);
				$q->addSearchCondition('cost_center', $value);
				$categoryConfiguration = CategoryConfiguration::model()->findAll( $q );
				if(!empty($categoryConfiguration)){

				}else{
					$costcenter.= $value.',';
				}
			}
			$model->cost_center = $costcenter;
			$model->client_id = $userId;
			$model->date_created = date('Y-m-d');
			if($model->save()){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Data saved successfully. </div>');
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
					 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					 <strong>Error!</strong> Having some issue while Saving Data. </div>');
			}

			$this->redirect(array('categoryConfiguration'));

		}
		$categoryConfiguration = CategoryConfiguration::model()->findAllByAttributes(array('client_id'=>$userId));
		$this->render('categoryConfiguration',array('model'=>$model,'categoryConfiguration'=>$categoryConfiguration));
	}

	public function actionDeletecategoryconfig(){
		if(isset($_GET['id'])){

			CategoryConfiguration::model()->deleteByPk($_GET['id']);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Data deleted successfully. </div>');
			$this->redirect(array('categoryConfiguration'));
		}
	}

	public function actiondeleteTimesheetcode(){
		if(isset($_GET['id'])){

			TimeSheetCode::model()->deleteByPk($_GET['id']);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Data deleted successfully. </div>');
			$this->redirect(array('timeSheetcode'));

		}

	}

	public function actionTimeSheetcode(){
			$model = new TimeSheetCode;
			$userId = UtilityManager::superClient(Yii::app()->user->id);

		if(isset($_POST['TimeSheetCode'])){
			$timeSheet = TimeSheetCode::model()->findByAttributes(array('time_code'=>$_POST['TimeSheetCode']['time_code'],'project_id'=>$_POST['TimeSheetCode']['project_id']));
			if(!empty($timeSheet)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
					 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					 <strong>Error!</strong> Having some issue while Saving Data. </div>');

				$this->redirect(array('timeSheetcode'));
			}
			$model->attributes=$_POST['TimeSheetCode'];
			$model->client_id = $userId;
			$model->date_created = date('Y-m-d');
			if($model->save()){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Data saved successfully. </div>');
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
					 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					 <strong>Error!</strong> Having some issue while Saving Data. </div>');
			}

			$this->redirect(array('timeSheetcode'));
		}

		$modelTimesheet = TimeSheetCode::model()->findAllByAttributes(array('client_id'=>$userId));
		$this->render('timeSheetcode',array('model'=>$model,'modelTimesheet'=>$modelTimesheet));
	}

	public function actionExtension(){
		$this->render('extensions');
		}
	
	public function actionDeleteWorkflow(){
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$workflowmember = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$id));
			if($workflowmember){
			foreach($workflowmember as $workflowdata){
				$workmember = WorklflowMember::model()->findByPk($workflowdata->id);
				$workmember->delete();
				}
			}
				$workflow = Worklflow::model()->findByPk($id);
				
				if($workflow->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Deleted successfully. </div>');
		}else{
			 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue in deletion. </div>');
			 }
			 $this->redirect(array('workflows'));
			}
		}
		
	public function actionDeleteCWorkflow(){
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$workflowmember = ContractWorklflowMember::model()->findAllByAttributes(array('contract_flow_id'=>$id));
			if($workflowmember){
			foreach($workflowmember as $workflowdata){
				$workmember = ContractWorklflowMember::model()->findByPk($workflowdata->id);
				$workmember->delete();
				}
			}
				$workflow = ContractWorkflow::model()->findByPk($id);
				
				if($workflow->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Deleted successfully. </div>');
		}else{
			 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue in deletion. </div>');
			 }
			 $this->redirect(array('contractWorkFlows'));
			}
		}
	
	public function actionBackgroundverification(){
		$userId = UtilityManager::superClient(Yii::app()->user->id);
   $loginUserId = $userId;
   $model = Client::model()->findByPk($loginUserId);
  if(isset($_POST['Backgroundverification'])){
   if(!empty($_POST['check']))
   {
   $verification = '';
   $Checkdata = $_POST['check'];
   foreach($Checkdata as $value){
     $verification .= $value.',';
   }
   $model->backgroundverification = $verification;
   if($model->save(false)){
   Yii::app()->user->setFlash('success', '<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           <strong>Success!</strong> Verification type Successfully saved.</div>');
   }else{
    Yii::app()->user->setFlash('error', '<div class="alert alert-danger" >
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong> Something went wrong.</div>');
   }

  }
   else{
    Yii::app()->user->setFlash('error', '<div class="alert alert-danger" >
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong> Something went wrong.</div>');
   }
  }



  $this->render('backgroundverification');
 }
 
	public function actionSettingExhibit(){
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$model = new ClientExhibit;
		if(isset($_POST['Save'])){
			$loginUserId = Yii::app()->user->id;
			$model->client_id = $userId;
			if(preg_match('/[\'^�$%&*()}{@#~?><>,|=_+�-]/',$_POST['file_name'])){
				Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Document name cannot be Special characters it should be a valid name.</div>');
				$this->redirect(array('settingExhibit'));
			}

			$model->name = $_POST['file_name'];
			$rnd = rand(0,9999);
			if(!empty($_FILES['ClientExhibit']['name']['file'])){
				$imageUploadFile = CUploadedFile::getInstance($model, 'file');
				if($imageUploadFile != ""){
					$fileName = "{$rnd}-{$imageUploadFile}";  // random number + file name
					$model->file = $fileName;
				}
				if (preg_match('/[\'^�$%&*()}{@#~?><>,|=_+�-]/', $imageUploadFile)){

					Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> file name cannot be Special characters  it should be a valid name.</div>');
					$this->redirect(array('settingExhibit'));
				}
			}
			if($model->save())
			{
				if(!empty($_FILES['ClientExhibit']['name']['file'])) {
					if ($imageUploadFile !== "") { // validate to save file
						$imageUploadFile->saveAs(Yii::app()->basePath . '/../images/exhibitfiles/' . $fileName);
					}
				}
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your file Successfully saved.</div>');
			}else{
				Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Something went wrong.</div>');
			}
			$this->redirect(array('settingExhibit'));
		}
		$this->render('exhibit',array('model'=>$model));
	}
	public function actionDeletefileExhibit(){
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$model = ClientExhibit::model()->findByPk($id);
			if($model->delete()){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your file Successfully Deleted.</div>');
			}else{
				Yii::app()->user->setFlash('error', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Something went wrong.</div>');
			}
			$this->redirect(array('settingExhibit'));
		}
	}
	
	public function actionEditProfile()
	{
		$model = Client::model()->findByPk(Yii::app()->user->id);
		
		if(isset($_POST['Client']))
		{
			if (preg_match('/[\'^�$%&*()}{@#~?><>,|=_+�-]/', $_POST['Client']['first_name'])){

				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> First name cannot be Special characters  it should be a Letter/character/Alphabet.</div>');
				$this->redirect(array('editProfile'));
			}
			if (preg_match('/[\'^�$%&*()}{@#~?><>,|=_+�-]/', $_POST['Client']['last_name'])){

				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Last name cannot be Special characters  it should be a Letter/character/Alphabet.</div>');
				$this->redirect(array('editProfile'));
			}
			$model->first_name = $_POST['Client']['first_name'];
			$model->last_name = $_POST['Client']['last_name'];
			$model->mobile = $_POST['Client']['mobile'];
			$model->phone = $_POST['Client']['phone'];
			$imageUploadFile = CUploadedFile::getInstance($model, 'profile_image');
			$rnd = rand(0,9999);
			if($imageUploadFile != ""){
				$fileName = "{$rnd}-{$imageUploadFile}";  // random number + file name
				$model->profile_image = $fileName;
			}
			if($model->save()){
				if ($imageUploadFile != "") { // validate to save file
						$imageUploadFile->saveAs(Yii::app()->basePath . '/../images/profile_img/' . $fileName);
					}
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your Profile Successfully Updated.</div>');
				}else{
					 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> There is error in submitting form.</div>');
					 }
			$this->redirect(array('editProfile'));
		}
		
		$this->render('editProfile',array('model'=>$model));
	}
	public function actionIndex()
	{
		$clientModel = Client::model()->findByPk(Yii::app()->user->id);
		$clientModel->scenario='singup2';
		if(isset($_POST['Client']))
		{ 
			$clientModel->attributes=$_POST['Client'];
			$clientModel->business_type=$_POST['Client']['business_type'];
			$clientModel->total_staff=$_POST['Client']['total_staff'];
			$clientModel->website_url=$_POST['Client']['website_url'];
			$clientModel->revenue_range=$_POST['Client']['revenue_range'];
			$clientModel->describe_business=$_POST['Client']['describe_business'];
			$imageUploadFile = CUploadedFile::getInstance($clientModel, 'profile_image');
			$rnd = rand(0,9999);
			if($imageUploadFile != ""){
				$fileName = "{$rnd}-{$imageUploadFile}";  // random number + file name
				$clientModel->profile_image = $fileName;
			}
			if($clientModel->save()) {

					if ($imageUploadFile != "") { // validate to save file
						$imageUploadFile->saveAs(Yii::app()->basePath . '/../images/profile_img/' . $fileName);
					}

				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your setting saved successfully.</div>');
				$this->redirect(array('index'));
			}
		}
		
		 $this->render('profile_create',array('model'=>$clientModel));
	}
	public function actionChangPassword(){
		$id = Yii::app()->user->getId();
		$user_type = Yii::app()->user->getState('type');
		/**if(isset($_POST['updatepassword']) && $user_type == 'super_admin')**/
		if(isset($_POST['updatepassword']))
		{
			$model = Client::model()->findByAttributes(array('id'=>$id));
			if($model->password == md5($_POST['old_password']))
			{
				if($_POST['new_password']== $_POST['repeat_password'])
					{
						$model->password = md5($_POST['new_password']);
						if ($model->save(false))
							{
								Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Password changed successfully. </div>');
								$this->redirect(array('changpassword'));
							}
					}
					else
					{
						Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Your Confirm Password did not matched. </div>');
						$this->redirect(array('changpassword'));
					}
			}
			else
			{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Your current password did not matched. </div>');
				$this->redirect(array('changpassword'));
			}
		}
		$this->render('changepassword');
	}
	
	public function actionAddWorkFlow()
	{
		if(isset($_GET['id'])){
			 $model=Worklflow::model()->findByPk($_GET['id']);
			}else{
				 $model=new Worklflow;
				 }
		
		$model1=new WorklflowMember;
		
		if(isset($_POST['Worklflow']) && isset($_GET['id'])){
			
			$OrderWorkflowMembers = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_GET['id'],'order_approve'=>$_POST['WorklflowMember']['order_approve']));
			
			if($OrderWorkflowMembers){
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> This approval order number is already used. So please use different order number. </div>');
				 $this->redirect(array('addWorkFlow','id'=>$_GET['id']));
				 }
			
			$oldMember = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_GET['id'],'client_id'=>$_POST['WorklflowMember']['client_id']));
			 
			 if($oldMember){
				 Yii::app()->user->setFlash('error', '<div class="card m-b-20 error-message">
				<p>You are trying to add duplicate entry.</p>
			</div> ');
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> You are trying to add duplicate entry. Please try again. </div>');
				 $this->redirect(array('addWorkFlow','id'=>$_GET['id']));
				 }else{
					 $model1->workflow_id = $_GET['id'];
					 $model1->attributes = $_POST['WorklflowMember'];
					 if($model1->save(false)){
						 Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your workflow member added successfull.</div>');
						 $this->redirect(array('addWorkFlow','id'=>$_GET['id']));
					 }
				 }
			}else{
				if(isset($_POST['Worklflow']))
					{
						$model->attributes = $_POST['Worklflow'];
						$model->client_id = Yii::app()->user->id;
						$model->date_created = date('Y-m-d');
						if($model->save(false)){
							 $model1->workflow_id = $model->id;
							 $model1->attributes = $_POST['WorklflowMember'];
							 if($model1->save(false)){
								 Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your workflow created successfull. Now you can add more members.</div>');
								 $this->redirect(array('addWorkFlow','id'=>$model->id));
							 }else{
								 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Having some issue please try again. </div>');
								  }
						}
					}
			}
		
		
		$this->render('addworkflow',array('model'=>$model,'model1'=>$model1));
	}
	
	
	public function actionContractWorkFlow()
	{
		if(isset($_GET['id'])){
			 $model=ContractWorkflow::model()->findByPk($_GET['id']);
			}else{
				 $model=new ContractWorkflow;
				 }
		
		$model1=new ContractWorklflowMember;
		
		if(isset($_POST['ContractWorkflow']) && isset($_GET['id'])){
			
			$OrderWorkflowMembers = ContractWorklflowMember::model()->findAllByAttributes(array('contract_flow_id'=>$_GET['id'],'order_approve'=>$_POST['ContractWorklflowMember']['order_approve']));
			
			if($OrderWorkflowMembers){
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> This approval order number is already used. So please use different order number. </div>');
				 $this->redirect(array('addWorkFlow','id'=>$_GET['id']));
				 }
			
			$oldMember = ContractWorklflowMember::model()->findAllByAttributes(array('contract_flow_id'=>$_GET['id'],'client_id'=>$_POST['ContractWorklflowMember']['client_id']));
			 
			 if($oldMember){
				 Yii::app()->user->setFlash('error', '<div class="card m-b-20 error-message">
				<p>You are trying to add duplicate entry.</p>
			</div> ');
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> You are trying to add duplicate entry. Please try again. </div>');
				 $this->redirect(array('contractWorkFlow','id'=>$_GET['id']));
				 }else{
					 $model1->contract_flow_id = $_GET['id'];
					 $model1->attributes = $_POST['ContractWorklflowMember'];
					 if($model1->save(false)){
						 Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your workflow member added successfull.</div>');
						 $this->redirect(array('contractWorkFlow','id'=>$_GET['id']));
					 }
				 }
			}else{
				if(isset($_POST['ContractWorkflow']))
					{
						$model->attributes = $_POST['ContractWorkflow'];
						$model->client_id = Yii::app()->user->id;
						$model->date_created = date('Y-m-d');
						if($model->save(false)){
							 $model1->contract_flow_id = $model->id;
							 $model1->attributes = $_POST['ContractWorklflowMember'];
							 if($model1->save(false)){
								 Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your workflow created successfull. Now you can add more members.</div>');
								 $this->redirect(array('contractWorkFlow','id'=>$model->id));
							 }else{
								 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Having some issue please try again. </div>');
								  }
						}
					}
			}
		
		
		$this->render('contractWorkflow',array('model'=>$model,'model1'=>$model1));
	}
	
	
	public function actionWorkFlows()
	{
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$model = Worklflow::model()->findAllByAttributes(array('client_id'=>$userId));
		$this->render('workflows',array('model'=>$model));
	}
	
	public function actionContractWorkFlows()
	{
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$model = ContractWorkflow::model()->findAllByAttributes(array('client_id'=>$userId));
		$this->render('contractworkflows',array('model'=>$model));
	}
	
	public function actionWorkFlowMembers()
	{
		$model=Worklflow::model()->findByPk($_GET['id']);
		$this->render('workflowmembers',array('model'=>$model));
	}
	
	public function actionConWorkFlowMembers()
	{
		$model=ContractWorkflow::model()->findByPk($_GET['id']);
		$this->render('conworkflowmembers',array('model'=>$model));
	}
	
	function actionMembers()
	{
		$data = Client::model()->findAllByAttributes(array('department'=>$_POST['dept_id'],'super_client_id'=>Yii::app()->user->id));
		//$data = Client::model()->findAllByAttributes(array('department'=>$_POST['dept_id']));
		echo '<select name="WorklflowMember[client_id]" class="form-control" >';
		foreach($data as $data){ ?>
        	<option value="<?php echo $data->id; ?>"><?php echo $data->first_name.' '.$data->last_name; ?></option>
        <?php
		}
		echo '</select>';
	}
	
	
	
	
	public function actionCreateteammember()
	{
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$model=new Client;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model->scenario='teammember';

		if(isset($_POST['Client']))
		{

			$model->attributes=$_POST['Client'];
			$model->super_client_id = $userId;
			$model->type='Sub Client';
			$model->profile_status = 1;
			$model->profile_approve = 'Yes';
			$model->date_created=date('Y-m-d');
			$model->date_updated=date('Y-m-d');
			if($model->validate() && $model->save()) {
				UtilityManager::emailByTemplate($model->id,'Client','client-creation');
				//$this->clientEmail($model->id);
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Team member added successfully. </div>');
			   
				$this->redirect(array('createteammember'));
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Oops Some thing Went Wrong. </div>');
				}
				
				//$this->redirect(array('view','id'=>$model->id));
		}
        $allTeam = Client::model()->findAll(array('condition'=>'super_client_id='.$userId));
       
		$this->render('addteammember',array(
			'model'=>$model,
			'allTeam'=>$allTeam,
		));
	} 
	 public function clientEmail($id) {
		 $info = Client::model()->findByPk($id);
          $customerEmail = $info->email;
          //$applicationUrl = $info->application_url;
          $htmlbody='<div align="center">
        <table border="0" cellspacing="0" cellpadding="0" width="650" style="background:white;border:solid #e0e0e0 1.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt">
          <tbody>
            <tr>
              <br valign="top"><h1>Hi,<u></u><u></u></h1>
              <p>Thanks for trying US VMS!</p></br>
                <p>To get started, please confirm your account by clicking the following link:: <form method="post" action='.Yii::app()->createAbsoluteUrl('client/accountactiavtion').'>
					<input type="hidden" name="client_id" value="'.$id.'" />
					<input type="submit" value="Click here" />
					</form> <br />
                <p>Please do not reply to this email. If you are receiving this email in error, you can safely ignore it.</p></br>
                <p>The US Tec Solution team</p></br>
                <p></p>
                </td>
            </tr>
          </tbody>
        </table>
      </div> ';
            $message = Yii::app()->sendgrid->createEmail();
      //shortcut to $message=new YiiSendGridMail($viewsPath);
            $message->setHtml($htmlbody)
                ->setSubject('US VMS - Account Verification')
                ->addTo($customerEmail)
                ->setFrom('account@simplifyvms.net');
            Yii::app()->sendgrid->send($message);
   }
   
	public function actionCreatelocation()
	{
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$model=new Location;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Location']))
		{
			if(strpos($_POST['Location']['name'],'.') !== FALSE){

				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Location name & Code is not valid.</div>');
				$this->redirect(array('createlocation'));
			}
			$location = Location::model()->findByAttributes(array('name'=>$_POST['Location']['name'],'reference_id'=>$userId));
			if(!empty($location)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Location name is duplicated.</div>');
				$this->redirect(array('createlocation'));
			}
			$model->attributes=$_POST['Location'];
			$model->reference_id=$userId;
			$model->user_type='Client';
			if($model->validate() && $model->save()) {
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Location added successfully. </div>');
			}else{
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
				 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 <strong>Error!</strong> Having some issue while adding Location. </div>');
				 }
			$this->redirect(array('createlocation'));
		}
        $allLocation= Location::model()->findAll(array('condition'=>'reference_id='.$userId));
       
		$this->render('createlocation',array(
			'model'=>$model,
			'allLocation'=>$allLocation,
		));
	}
	
	
	public function actionCreatedepartment()
	{
	
         $userId = UtilityManager::superClient(Yii::app()->user->id);                  
		if($_POST)
		{
			$checkUnique = Setting::model()->countByAttributes(array('title'=>trim($_POST['department']),
				'client_id'=>$userId));
			if( !$checkUnique ) {
				$model              = new Setting;
				$model->client_id   = $userId;
				$model->title       = $_POST['department'];
				$model->category_id = 4;
				$model->type        = 'Client';  
	            if($model->save(false)) {
					Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Your Department successfully Saved. </div>');
				}else{
					 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Opps! Having some issue while adding department. </div>');
					 }
		    }else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Opps! There is an Error. Already existed. </div>');
		    }
		}
		$modelSetting = Setting::model()->findAll(array('condition'=>'category_id=4','order'=>'id ASC'));
       
		$this->render('department',array(
			'modelSetting'=>$modelSetting,
		));
	}
	public function actionCreatebillingcode()
	{
		$model = new BillingcodeDepartment;
		$userId = UtilityManager::superClient(Yii::app()->user->id);
                           
		if(isset($_POST['BillingcodeDepartment']))
		{
			
			$model->attributes=$_POST['BillingcodeDepartment'];
			$model->client_id=$userId;
			$model->date_created=date('Y-m-d');
			if($model->save(false)) {
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Added successfully. </div>');
			}else{
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue. </div>');
				 }
			
			$this->redirect(array('createbillingcode'));
           
		}
		$modelBillingcodeDepartment = BillingcodeDepartment::model()->findAll(array('condition'=>'client_id='.$userId,'order'=>'id DESC'));
       
		$this->render('billingcode',array(
			'modelBillingcodeDepartment'=>$modelBillingcodeDepartment,
			'model'=>$model,
		));
	}
	public function actionCreatejobrequest()
	{
		$model = new JobrequestDepartment;
		$userId = UtilityManager::superClient(Yii::app()->user->id);
                           
		if(isset($_POST['JobrequestDepartment']))
		{
			
			$model->attributes=$_POST['JobrequestDepartment'];
			$model->client_id=$userId;
			$model->date_created=date('Y-m-d');
			if($model->save(false)) {
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Added successfully. </div>');
				$this->redirect(array('createjobrequest'));
			}else{
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue. </div>');
				 }

           
		}
		$modelJobrequestDepartment = JobrequestDepartment::model()->findAll(array('condition'=>'client_id='.$userId,'order'=>'id DESC'));

		$this->render('jobrequest',array(
			'modelJobrequestDepartment'=>$modelJobrequestDepartment,
			'model'=>$model,
		));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDeleteteammember($id)
	{
		$model = $this->loadModel($id);
		if($model->super_client_id==Yii::app()->user->id) {
			$model->delete();
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Team member deleted successfully. </div>');
		 }
		$this->redirect(array('createteammember'));
	}
	public function actionDeletelocation($id)
	{
		$model = Location::model()->findByPk($id);
		if($model->reference_id==Yii::app()->user->id) {
			if(empty($model)){
			$this->redirect(array('createlocation'));
			}
			if($model->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Location deleted successfully. </div>');
			}else{
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue in deletion. </div>');
				 }
		}
		$this->redirect(array('createlocation'));
	}
	public function actionDeletedepartment($id)
	{
		$model = Setting::model()->findByAttributes(array('client_id'=>Yii::app()->user->id,'id'=>$id));
		if(empty($model)){
			$this->redirect(array('createdepartment'));
			}
		if($model->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Deleted successfully. </div>');
		}else{
			 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue in deletion. </div>');
			 }
		$this->redirect(array('createdepartment'));
	}
	
	public function actionDeleteWFmember($id)
	{
		$model = WorklflowMember::model()->findByPk($id);
		if(empty($model)){
			$this->redirect(array('addWorkFlow','id'=>$_GET['workflow_id']));
			}
		$model->delete();
		Yii::app()->user->setFlash('error', '<div class="card m-b-20 error-message">
				<p>Member deleted successfully .</p>
			</div> ');
		$this->redirect(array('addWorkFlow','id'=>$_GET['workflow_id']));
	}
	
	public function actionDeleteCWFmember($id)
	{
		$model = ContractWorklflowMember::model()->findByPk($id);
		if(empty($model)){
			$this->redirect(array('contractWorkFlow','id'=>$_GET['workflow_id']));
			}
		$model->delete();
		Yii::app()->user->setFlash('error', '<div class="card m-b-20 error-message">
				<p>Member deleted successfully .</p>
			</div> ');
		$this->redirect(array('contractWorkFlow','id'=>$_GET['workflow_id']));
	}
	
	
	public function actionDeletebillingcode($id)
	{
		$model = BillingcodeDepartment::model()->findByAttributes(array('client_id'=>Yii::app()->user->id,'id'=>$id));
		if(empty($model)){
			$this->redirect(array('createbillingcode'));
			}
		if($model->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Billing code deleted successfully. </div>');
			}else{
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue in deletion. </div>');
				 }
		$this->redirect(array('createbillingcode'));
	}
	public function actionDeletejobrequest($id)
	{
		$model = JobrequestDepartment::model()->findByAttributes(array('client_id'=>Yii::app()->user->id,'id'=>$id));
		if(empty($model)){
			$this->redirect(array('createjobrequest'));
			}
		if($model->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Deleted successfully. </div>');
		}else{
			 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Error!</strong> Having some issue in deletion. </div>');
			 }
		$this->redirect(array('createjobrequest'));
	}
	
	public function actionAddLocation(){
				
		$clientID = Yii::app()->user->id;
		$mainclient = Yii::app()->user->id;
		$client = Client::model()->findByPk($mainclient);
		if($client->member_type!=NULL){
			$client = Client::model()->findByPk($client->super_client_id);
			$mainclient = $client->id;
		}

		error_reporting(0);
		$model=new ClientLocations;
		
		if(isset($_POST['add_location'])){
			$ifExists = ClientLocations::model()->findByAttributes(array('client_id'=>$_POST['client_id'] , 'location_id'=>$_POST['location_id']));
			if($ifExists){
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
				 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 <strong>Error!</strong> This Location is already added. </div>');
				 $this->redirect(array('addLocation'));
			} 
 			$locationInfo = Location::model()->findAll(array('condition'=>'id='.$_POST['location_id']));
			$clientInfo = ProClientNames::model()->findAllByAttributes(array('main_client'=>$_POST['client_id']));
  			$model->client_id = $_POST['client_id'];
			$model->location_id = $_POST['location_id']; 
			$model->location_name = $locationInfo[0]->name; 
			$model->client_name = $clientInfo[0]->name;  
		    $model->date_created=date('Y-m-d');
			$model->save(false);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 <strong>Success!</strong> Location added successfully. </div>');
			 $this->redirect(array('addLocation'));
		}
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$clientInfo = Client::model()->findByPk($userId);
		$record = ProClientNames::model()->findAllByAttributes(array('main_client'=>$mainclient));
		$client_locations = ClientLocations::model()->findAllByAttributes(array('client_id'=>$mainclient));
		$allLocation= Location::model()->findAll(array('condition'=>'reference_id='.$mainclient));
 		$this->render('addLocation',array('model'=>$model,'record'=>$record,'clientInfo'=>$clientInfo , 'locations'=>$allLocation ,'client_locations'=>$client_locations));
		
	}
	
	public function actionNotificationAlert(){
		$clientID = Yii::app()->user->id;
		if(isset($_POST['add_alert'])){
			$ifExists = GuestNotifications::model()->findByAttributes(array('client_id'=>$clientID , 'type'=>$_POST['type'] ,'email'=>$_POST['email_addres']));
			if($ifExists){
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
				 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 <strong>Error!</strong> User for this type is already added. </div>');
				 $this->redirect(array('notificationAlert'));
			} 
 			$model = new GuestNotifications;
  			$model->client_id = $clientID;
			$model->type = $_POST['type']; 
			$model->email = $_POST['email_addres']; 
			$model->name = $_POST['user_name']; 
 			$model->save(false);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 <strong>Success!</strong> Location added successfully. </div>');
			 $this->redirect(array('notificationAlert'));
		}
		$notifications = GuestNotifications::model()->findAllByAttributes(array('client_id'=>$clientID));
		///echo '<pre>';print_r($notifications);exit;
		$this->render('notificationAlert' , array('notifications'=>$notifications));//,array('model'=>$model)
	}
	
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Client the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Client::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}