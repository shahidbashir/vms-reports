<?php 
class OfferController extends Controller 
{ 
	 
	public function filters() 
	{ 
		return array( 
			'accessControl', // perform access control for CRUD operations 
		); 
	} 
	/** 
	 * Specifies the access control rules. 
	 * This method is used by the 'accessControl' filter. 
	 * @return array access control rules 
	 */ 
	public function accessRules() 
	{ 
		return array( 
		 	array('allow',  // allow all users to perform 'index' and 'view' actions 
			  'actions'=>array('offerHelloSignResponse' ,'onBoardHelloSignResponse'), 
			  'users'=>array('*'), 
			 ),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions 
				'actions'=>array('workorderView','workorderList','workOrderCandidates','workOrderStep1','workOrderStep2','locationManager','offerCandidates','step1','step2','create','view','offerView','rejected','approved','deleteBackground','deleteExhibit','downloadBackground','downloadExhibit','jobWorkers','interviewCandidates','exhibit','background','offBoarding','onBoarding','other','notes','addProject','deleteProject','onBoardingList','ofBoardingList','timesheetList','approvedTimesheet','pendingTimesheet','timesheetDetail','getProject','pendingboarding','todayboarding','createoffer','createworkOrder'),
				'users'=>array('@'), 
			), 
			array('deny',  // deny all users 
				'users'=>array('*'), 
			), 
		); 
	}
	
	public function actionWorkorderView(){

		error_reporting(0);
       
        $workOrderID = $_GET['id'];
	    $workOrderModel =  Workorder::model()->findByPk($workOrderID);
		$offerData = Offer::model()->findByPk($workOrderModel->offer_id);
		$client = Client::model()->findByPk($workOrderModel->client_id);
	    $backgroundModel = new OfferBackground;
	    $exhibitModel = new OfferExhibit;
				
		//these quries will be used for onboarding of candidate
		$submissionModel =  VendorJobSubmission::model()->findByPk($offerData->submission_id);
	    //$vendorModel =  Vendor::model()->findByPk($submissionModel->vendor_id);
	    $candidateModel = Candidates::model()->findByPk($submissionModel->candidate_Id);
	    $candidateModel->candidate_ID = "CID-".$candidateModel->id.$submissionModel->id.$offerModel->id;
	    $candidateModel->emp_password = "CID-".$candidateModel->id.$submissionModel->id.$offerModel->id;
		
		
		//workOrder Projects list
		$projectIDs = array();
		if($workOrderModel->wo_project){
			$projectIDs = explode(',',$workOrderModel->wo_project);
			}
		$projects = Project::model()->findAllByAttributes(array('id'=>$projectIDs));
		
		//workOrder Cost Center Cosdes list
		$costCenterIDs = array();
		if($workOrderModel->wo_cost_center){
			$costCenterIDs = explode(',',$workOrderModel->wo_cost_center);
			}
		$costCenters = CostCenter::model()->findAllByAttributes(array('id'=>$costCenterIDs));
		
		//workOrder Time Sheet Codes list
		$timeSheetIDs = array();
		if($workOrderModel->wo_timesheet_code){
			$timeSheetIDs = explode(',',$workOrderModel->wo_timesheet_code);
			}
		$timeSheetCodes = TimeSheetCode::model()->findAllByAttributes(array('id'=>$timeSheetIDs));


	      $sql ="select sub.id,sub.job_id,
			can.first_name,can.last_name,can.id as c_id,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,workorder.approval_manager

			FROM vms_vendor_job_submission sub,vms_candidates as can,vms_workorder as workorder,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and sub.id = workorder.submission_id and  workorder.id=".$workOrderModel->id;  


		$workOrders = Yii::app()->db->createCommand($sql)->queryRow();
				
		//code for updation of status of offer from dropdown submitted value
		if(isset($_POST['Workorder'])){
			$workOrderModel->workorder_status = $_POST['Workorder']['workorder_status'];
			
			//if workorder is rejected then offer will be automatically rejected.
			//now you will create a new offer for the submission.
			/*if($_POST['Workorder']['workorder_status']==2){
				$workOrderModel->status = 2;
				//when workorder is rejected then change the submission status to offer.
				$submissionData = VendorJobSubmission::model()->findByPk($workOrderModel->submission_id);
				$submissionData->resume_status = 7;
				$submissionData->save(false);
				}*/
			$workOrderModel->save(false);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Status!</strong> Changed successfully.</div>');
	  	}
		
		if(isset($_POST['accepted'])){
			$workOrderModel->workorder_status = 1;
			$workOrderModel->accept_date = date("Y-m-d H:i:s");
			$workOrderModel->save(false);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <strong>Status!</strong> WorkOrder Approved successfully.</div>');
		}
		
		if(isset($_POST['rejected'])){
			$workOrderModel->workorder_status = 1;
			$workOrderModel->save(false);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Status!</strong> WorkOrder Approved successfully.</div>');
		}
		
		if(isset($_POST['rejected'])){
			$workOrderModel->workorder_status = 2;
			$workOrderModel->rejection_date = date("Y-m-d H:i:s");;
			$workOrderModel->reason_rejection_workorder = $_POST['reason_for_rejection'];
			$workOrderModel->notes_workorder = $_POST['notes'];
			$workOrderModel->save(false);
			Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <strong>Status!</strong> WorkOrder Rejected successfully.</div>');
		}
		//below code was for approval, rejection of offer so for now we will do this through dropdown
		
		
		
	   //commenting this code for now		
       /* if(isset($_POST['accepted'])){
	    	$offerModel->status = 1;
	    	if($offerModel->save(false)){
	    		Yii::app()->user->setFlash('success', '<strong>Success!</strong> Offer status accepted successfully.');
	    		$subject = "Offer Approved";
				$this->emailNotification($offerModel,$subject);
	    	}	
	    }else if(isset($_POST['rejected'])){
			 $offerModel->attributes = array("status" => 2,
           "reason_for_rejection" => $_POST['reason_for_rejection'],
           "notes" => $_POST['notes']);
      if($offerModel->save(false)){
       Yii::app()->user->setFlash('success', '<strong>Success!</strong> Offer Rejected Successfully.');
       $subject = "Offer Rejected";
    //$this->emailNotification($offerModel,$subject);
      }
			}else if(!empty($_GET['workorder-status'])){
	    	$offerModel->workorder_status = $_GET['workorder-status'];
	    	if($offerModel->save(false)){
	    		$label = $offerModel->workorder_status ==2?"Rejected":"Accepted";
	    		Yii::app()->user->setFlash('success', '<strong>Success!</strong> Work order status <strong>'.$label.'</strong> successfully.');

	    		$subject = $offerModel->workorder_status ==2?"Offer Rejected":"Offer Accepted";
				$this->emailNotification($offerModel,$subject);
	    	}	
	    }*/

		$this->render('workorderView',array('client'=>$client,'workOrderModel'=>$workOrderModel,'workOrders'=>$workOrders,'offerData'=>$offerData,'projects'=>$projects,'costCenters'=>$costCenters,'timeSheetCodes'=>$timeSheetCodes,'candidateModel'=>$candidateModel,'submissionModel'=>$submissionModel));


	} 
	
	//new code for seprate workorder listing
	public function actionWorkorderList(){
	  error_reporting(0);
	  //$loginUserId = Yii::app()->user->id;
	  $loginUserId = UtilityManager::superClient(Yii::app()->user->id);
	  if(!empty($_GET['id'])){
	   $id = $_GET['id'];
	   $jobmodel = Job::model()->findByPk($id);
	   $jobID = 'and sub.job_id="'.$jobmodel->id.'"';
	   $veiwName = "/job/workorderlistJob";
	   
	  }else{
	   $jobID = "";
	   $veiwName = "workorderList";
	  }
		$condition = '';
		
		if(isset($_POST['jobId'])){
			$jobid = $_POST['job_id'];
				$condition = 'and workorder.job_id='.$jobid;
		}
	
	  $sql ="select sub.id,sub.vendor_id,sub.job_id,workorder.wo_start_date,wo_bill_rate,offer_id,workorder.id as workorder_id,
	
	   can.first_name,can.last_name,can.id as c_id,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,workorder.workorder_status
	
	   FROM vms_vendor_job_submission sub,vms_candidates as can,vms_workorder as workorder,vms_vendor as vendor
	
	   WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and workorder.client_id=".$loginUserId." and sub.id = workorder.submission_id ".$jobID." ".$condition." AND sub.id !=".Yii::app()->user->getState('submission_id')." and workorder.wo_form_submitted=1 order by workorder.id desc";
	
	  $count_query = "select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can,vms_workorder as workorder,vms_vendor as vendor
	
	   WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id AND sub.id !=".Yii::app()->user->getState('submission_id')." and workorder.client_id=".$loginUserId." and sub.id = workorder.submission_id ".$jobID." ".$condition." and workorder.wo_form_submitted=1";
	
	  $item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
	
	  $dataProvider=new CSqlDataProvider($sql, array(
	   'keyField'=>'id',
	   'totalItemCount'=>$item_count,
	   'pagination'=>array(
		'pageSize'=>25,
	   ),
	  ));
	
	  $workorders = $dataProvider->getData();
	
	  $this->render($veiwName,array('workorders'=>$workorders,'model'=>$jobmodel,'pages'=>$dataProvider->pagination));
	
	 }
	

	

	public function actionGetProject(){

	if(isset($_POST['cost_id'])){

		$project = Project::model()->findAllByAttributes(array('cost_center'=>$_POST['cost_id']));
		if($project){
			foreach($project as $projectdata){ ?>
				<option value="<?php echo $projectdata['id'] ?>"><?php echo $projectdata['project_name']; ?></option>
			<?php }
		}
	}
	}
	
	//timesheet detail view
	/*public function actionTimesheetDetail(){
		
		$logRecord = CpTimesheetLog::model()->findByPk($_GET['logID']);
			
		$recordArray = array();
		$recordArray['year'] = $logRecord->year;
		$recordArray['week_number'] = $logRecord->week_number;
		Yii::app()->session['selectTimesheet'] = $recordArray;
		
		if(isset($_POST['approve'])){
			$logRecord->attributes=$_POST['CpTimesheetLog'];
			$logRecord->status = 1;
			$logRecord->rejection_date = date('Y-m-d');
			$logRecord->save(false);
			
		}
		
		if(isset($_POST['reject'])){
			$logRecord->attributes=$_POST['CpTimesheetLog'];
			$logRecord->status = 2;
			$logRecord->rejection_date = date('Y-m-d');
			$logRecord->save(false);
			
		}
		
		$history = CpTimesheetLog::model()->findAllByAttributes(array('candidate_id'=>$logRecord->candidate_id,'week_number'=>$logRecord->week_number,'status'=>2),array('order'=>'id desc'));
		
		$regularHour = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$_GET['logID'],'type'=>'Regular Hour'));
		$overTime = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$_GET['logID'],'type'=>'Over Time'));
		$doubleTime = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$_GET['logID'],'type'=>'Double Time'));
		
		$this->render('timesheetDetail',array('model'=>$regularHour,'model1'=>$logRecord,'overTime'=>$overTime,'doubleTime'=>$doubleTime,'history'=>$history));
		
		}*/
		
	public function actionTimesheetDetail($id){ error_reporting(0);
		$model = CpTimesheet::model()->findByPk($id);
		
		$client = Client::model()->findByPk($model->client_id);
		if($client->member_type!=NULL){
			$client = Client::model()->findByPk($client->super_client_id);
			}
			//echo $client->id;
		$timesheetConfig = TimesheetConfig::model()->findByAttributes(array('client_id'=>$client->id));
		 
		$clientType = $timesheetConfig->type;
		
		if(isset($_POST['approve'])){
			$model->timesheet_status = 1;
			$model->approve_date_time = date('Y-m-d H:i:s');
			if($model->save(false)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <strong>Success!</strong> TimeSheet Aproved Successfully.</div>');
				}
			
		}
		
		if(isset($_POST['reject'])){
			$model->reason_of_rejection = $_POST['reason_for_rejection'];
			$model->rejection_notes = $_POST['rejection_notes'];
			$model->timesheet_status = 2;
			$model->rejection_date_time = date('Y-m-d H:i:s');
			if($model->save(false)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <strong>Success!</strong> TimeSheet Rejected Successfully.</div>');
				}
			
		}
		
		$this->render('timesheetDetail',array('model'=>$model,'clientType'=>$clientType));
		}
	
	public function actionTimesheetList(){
			//28 userid have records
			$userID = Yii::app()->user->id;
			$client = Client::model()->findByPk($userID);
			//adding this condition for now to show timesheet list to the main client.
			// .' or '.$client->super_client_id.'=0
		if($client->super_client_id==0 || $client->member_type=='Administrator') {
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
			$sql = 'SELECT t.* FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and o.client_id='.$loginUserId.' order by id desc ';

			$count_query = 'select count(*) FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and o.client_id='.$loginUserId;

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		}else{
					
			$sql = 'SELECT t.* FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and o.approval_manager = '.$userID.' or '.$client->super_client_id.'=0 order by id desc ';
					
			$count_query = 'select count(*) FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and o.approval_manager = '.$userID.' or '.$client->super_client_id.'=0';

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		}

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$record = $dataProvider->getData();
			
			$this->render('timesheetList',array('record'=>$record,'pages'=>$dataProvider->pagination));
		}
		
	public function actionApprovedTimesheet(){
			
			$userID = Yii::app()->user->id;		
			$sql = 'SELECT tl.* FROM `cp_timesheet_log` as tl
					INNER JOIN vms_offer as o
					ON tl.offer_id=o.id
					WHERE tl.status = 1 and o.approval_manager = '.$userID;
		$count_query = "select count(*) FROM `cp_timesheet_log` as tl
					INNER JOIN vms_offer as o
					ON tl.offer_id=o.id
					WHERE tl.status = 1 and o.approval_manager = ".$userID;
		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$record = $dataProvider->getData();
					
			//$record = Yii::app()->db->createCommand($sql)->queryAll();
			
			$this->render('timesheetList',array('record'=>$record,'pages'=>$dataProvider->pagination));
		}
		
	public function actionPendingTimesheet(){
			
			$userID = Yii::app()->user->id;		
			$sql = 'SELECT tl.* FROM `cp_timesheet_log` as tl
					INNER JOIN vms_offer as o
					ON tl.offer_id=o.id
					WHERE tl.status = 0 and o.approval_manager = 28';
		$count_query = "select count(*) FROM `cp_timesheet_log` as tl
					INNER JOIN vms_offer as o
					ON tl.offer_id=o.id
					WHERE tl.status = 0 and o.approval_manager = 28";
		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$record = $dataProvider->getData();
					
			//$record = Yii::app()->db->createCommand($sql)->queryAll();
			
			$this->render('timesheetList',array('record'=>$record,'pages'=>$dataProvider->pagination));
		}

	public function actionOfBoardingList(){

		$loginUserId = Yii::app()->user->id;
		$jobmodel =  new Job;
		if(!empty($_GET['id'])){
			$id = $_GET['id'];
			$jobmodel = Job::model()->findByPk($id);
			$jobID = 'and sub.job_id="'.$jobmodel->id.'"';
			$veiwName = "of_boarding_list";
		}else{
			$jobID = "";
			$veiwName = "/offer/of_boarding_list";
		}


		$sql ="select sub.id,sub.vendor_id,sub.job_id,offer.id as offer_id,offer.valid_till_date as expiry_date,

			can.first_name,can.last_name,can.id as c_id,offer.rates_regular,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,offer.workorder_status,offer.status as offer_status,offer.payment_type

			FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and sub.id = offer.submission_id ".$jobID." and sub.client_id = ".$loginUserId." and offer.workorder_status= 1 and offer.on_board_status=1 and offer.status=5" ;

		$count_query = "select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and sub.id = offer.submission_id ".$jobID." and sub.client_id = ".$loginUserId." and offer.workorder_status= 1 and offer.on_board_status=1 and offer.status=5";

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$offers = $dataProvider->getData();
		//$offers = Yii::app()->db->createCommand($sql)->query()->readAll();

		$this->render($veiwName,array('model'=>$jobmodel,'offers'=>$offers,'pages'=>$dataProvider->pagination));
	}

	public function actionOnBoardingList(){

		//$loginUserId = Yii::app()->user->id;
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$date = date('Y-m-d'); 
		$sql ='select * From vms_workorder where workorder_status=1  AND submission_id !='.Yii::app()->user->getState('submission_id').' and client_id ='.$loginUserId;
		
		$count_query = 'select count(*) FROM vms_workorder where client_id ='.$loginUserId.' AND submission_id !='.Yii::app()->user->getState('submission_id') ;

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
		$models = $dataProvider->getData();

		$this->render('on_boardinglist', array('models' => $models,'pages' => $dataProvider->pagination));
	}
	
	public function actionPendingboarding(){

		//$loginUserId = Yii::app()->user->id;
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$date = date('Y-m-d'); 
		
		//removing this query for now b/c what rohit said now is that if a candidate is onboarded then he 
		//not be listed in joiners list so thats why removing the dates conditions b/c these apply after the 
		//candidate onboarded.
		
		/*$sql ='select * From vms_workorder where IF(onboard_changed_start_date!="0000-00-00",if(onboard_changed_start_date<>"'.$date.'",true,false),if(wo_start_date<"'.$date.'",true,false)) and client_id ='.$loginUserId;
		$count_query = 'select count(*) FROM vms_workorder where IF(onboard_changed_start_date!="0000-00-00",if(onboard_changed_start_date<>"'.$date.'",true,false),if(wo_start_date<"'.$date.'",true,false))and client_id ='.$loginUserId;*/
		
		
		$sql ='select * From vms_workorder where workorder_status IN(0,1) and on_board_status=0 and wo_start_date<"'.$date.'" and client_id ='.$loginUserId;
		$count_query = 'select count(*) From vms_workorder where workorder_status IN(0,1) and on_board_status=0 and wo_start_date<"'.$date.'" and client_id ='.$loginUserId;
		
		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
		$models = $dataProvider->getData();

		$this->render('pendingboarding', array('models' => $models,'pages' => $dataProvider->pagination));
	}
	public function actionTodayboarding(){
		//$loginUserId = Yii::app()->user->id;
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$date = date('Y-m-d'); 
		
		//removing this query for now b/c what rohit said now is that if a candidate is onboarded then he 
		//not be listed in joiners list so thats why removing the dates conditions b/c these apply after the 
		//candidate onboarded.
		
		/*$sql ='select * From vms_workorder where IF(onboard_changed_start_date!="0000-00-00",if(onboard_changed_start_date="'.$date.'",true,false),if(wo_start_date="'.$date.'",true,false)) and client_id ='.$loginUserId;
		$count_query = 'select count(*) FROM vms_workorder where IF(onboard_changed_start_date!="0000-00-00",if(onboard_changed_start_date="'.$date.'",true,false),if(wo_start_date="'.$date.'",true,false))and client_id ='.$loginUserId;*/
		
		$sql ='select * From vms_workorder where workorder_status IN(0,1) and on_board_status=0 and wo_start_date="'.$date.'" and client_id ='.$loginUserId;
		$count_query = 'select count(*) From vms_workorder where workorder_status IN(0,1) and on_board_status=0 and wo_start_date="'.$date.'" and client_id ='.$loginUserId;
		
		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
		$models = $dataProvider->getData();

		$this->render('todayboarding', array('models' => $models,'pages' => $dataProvider->pagination));
	}
	 
	public function actionJobWorkers(){ 
		$offerID = $_GET['offerID']; 
		$offerData = Offer::model()->findByPk($offerID); 
		$submissionData = VendorJobSubmission::model()->findByPk($offerData->submission_id); 
		$candidateData = Candidates::model()->findByPk($submissionData->candidate_Id); 
		
		if(isset($_POST['setup_account'])){ 
			$candidateData->emp_start_date = date("Y-m-d", strtotime($_POST['emp_start_date'])); 
			$candidateData->emp_msp_account_mngr = $_POST['emp_msp_account_mngr']; 
			$candidateData->emp_client_account_mngr = $_POST['emp_client_account_mngr']; 
			$candidateData->emp_time_sheet_aprover = $_POST['emp_time_sheet_aprover']; 
			$candidateData->emp_official_email = $_POST['emp_official_email']; 
			$candidateData->emp_user_name = $_POST['emp_user_name']; 
			$candidateData->emp_location = $_POST['emp_location']; 
			$candidateData->emp_note = $_POST['emp_note']; 
			$candidateData->emp_pay_rate = $_POST['emp_pay_rate']; 
			$candidateData->emp_over_time = $_POST['emp_over_time']; 
			$candidateData->emp_double_time = $_POST['emp_double_time']; 
			if($candidateData->save(false)){ 
				$canPersonalMail = CandidatesEmail::model()->findByAttributes(array('candidate_id'=>$submissionData->candidate_Id,'type'=>'Personal')); 
				if($canPersonalMail){ 
					$canPersonalMail->email = $_POST['personal_email']; 
					$canPersonalMail->save(false); 
					}else{ 
						$canPersonalMail = new CandidatesEmail; 
						$canPersonalMail->candidate_id = $submissionData->candidate_Id; 
						$canPersonalMail->email = $_POST['personal_email']; 
						$canPersonalMail->type = 'Personal'; 
						$canPersonalMail->save(false);
						} 
				 
				$candidateID= $candidateData->id; 
				$htmlbody='<div align="center"> 
				<table border="0" cellspacing="0" cellpadding="0" width="650" style="background:white;border:solid #e0e0e0 1.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt"> 
				  <tbody> 
					<tr> 
					  <br valign="top"><h1>Hi,<u></u><u></u></h1> 
					  <p>Thanks!</p></br> 
						<p>You have been invited to this job:: <a href="simplifydemo.net/candidate/index.php?r=site/accountactiavtion/candidate-id/"'.$candidateID.'">candidate.demobetademo.net//index.php?r=site/accountactiavtion/candidate-id/"'.$candidateID.'"</a><br /> 
						<p>Please do not reply to this email. If you are receiving this email in error, you can safely ignore it.</p></br> 
						<p>The US Tec Solution team</p></br> 
						<p></p> 
						</td> 
					</tr> 
				  </tbody> 
				</table> 
			  </div> '; 
		 
		 
		 
			$message = Yii::app()->sendgrid->createEmail(); 
			 //shortcut to $message=new YiiSendGridMail($viewsPath); 
			$message->setHtml($htmlbody) 
			->setSubject('Simplify VMS - Candidate Username')
			->addTo($canPersonalMail->email) 
			->setFrom('account@simplifyvms.net');
				 
			Yii::app()->sendgrid->send($message); 
			 
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
				<p>Set Up Account completed successfully</p> 
			</div> ');		 
					 
				} 
			} 
		$this->render('jobWorkers'); 
		} 
	 
	public function actionOfferCandidates(){ 
		/*$sql = "SELECT s . *  
		FROM vms_vendor_job_submission s 
		LEFT JOIN (select * from vms_offer group by submission_id order by id DESC)    
		o ON s.id = o.submission_id 
		WHERE s.job_id =  ".$_POST['job_id']." and  if(s.id =o.submission_id, o.status = 2 and s.resume_status = 7,s.resume_status =  7) "; */
		
		//now with all these status candidates will be listed in dropdown (3,4,5,7)
		$sql = "SELECT s . *  
		FROM vms_vendor_job_submission s 
		LEFT JOIN (select * from vms_offer group by submission_id order by id DESC)    
		o ON s.id = o.submission_id 
		WHERE s.job_id =  ".$_POST['job_id']." and  if(s.id =o.submission_id, o.status = 2 and s.resume_status IN(3,4,5,7),s.resume_status IN(3,4,5,7)) "; 
		
		$submissionData = Yii::app()->db->createCommand($sql)->queryAll(); 
		 
		foreach($submissionData as $key=>$value){ 
			$candidateData = Candidates::model()->findByPk($value['candidate_Id']); 
			echo '<option value="'.$value['id'].'">'.$candidateData->first_name.' '.$candidateData->last_name.'</option>'; 
			} 
	} 
	
	public function actionWorkOrderCandidates(){ 
		$sql = "SELECT s . *  
		FROM vms_vendor_job_submission s 
		LEFT JOIN (select * from vms_offer group by submission_id order by id DESC)    
		o ON s.id = o.submission_id 
		WHERE s.job_id =  ".$_POST['job_id']." and  if(s.id =o.submission_id, o.status = 1 and s.resume_status = 9,s.resume_status =  9) "; 
		 
		$submissionData = Yii::app()->db->createCommand($sql)->queryAll();
				
		foreach($submissionData as $key=>$value){
			$workorderData = Workorder::model()->findByAttributes(array('submission_id'=>$value['id']),array('order'=>'id desc'));
			//if there is no workorder for this candidate or workorder status is rejected then you can create new workorder for that candidate.
			$flag = false;
			
			if(empty($workorderData)){
				$flag = true;
				}else if($workorderData->workorder_status==2){
					 $flag = true;
					 }else{
					 $flag = false;
					 }
					 
			$candidateData = Candidates::model()->findByPk($value['candidate_Id']);
			if($flag == true){ 
				echo '<option value="'.$value['id'].'">'.$candidateData->first_name.' '.$candidateData->last_name.'</option>'; 
			}
			} 
	} 
	 
	public function actionInterviewCandidates(){ 
		
		$VendorJobSubmission = VendorJobSubmission::model()->findAllByAttributes(array('job_id'=>$_POST['job_id'],'resume_status'=>'5'));
		
		echo '<option value=""></option>';
		foreach($VendorJobSubmission as $key=>$value){ 
			//if candidate interview is already in pending state then show him in list
			$interviewCount = Interview::model()->countByAttributes(array('submission_id'=>$value->id,'status'=>array(1,2)));
			if(empty($interviewCount)){
				$candidateData = Candidates::model()->findByPk($value['candidate_Id']); 
				echo '<option value="'.$value['id'].'">'.$candidateData->first_name.' '.$candidateData->last_name.'</option>'; 
			}
		} 
	}
	
	
		
	public function actionWorkOrderStep1(){	 
		$loginUserId = Yii::app()->user->id;
		$superClient = UtilityManager::superClient(Yii::app()->user->id); 
		$jobData = Job::model()->findAllByAttributes(array('user_id'=>$superClient,'jobStatus'=>array(3,6,11))); 	
		if(isset($_POST['first_form'])){
				Yii::app()->session['wo_first_form'] = $_POST; 
				$this->redirect(array('workOrderStep2','sub_id'=>$_POST['can_submission_id'])); 
			}  
		$this->render('wostep1',array('jobData'=>$jobData)); 
	} 


	public function actionWorkOrderStep2(){ 
		
		$model = new Workorder;
		
		$loginUserId = Yii::app()->user->id; 
		$submissionData = VendorJobSubmission::model()->findByPk($_GET['sub_id']); 
		
		$workorderData = Workorder::model()->findByAttributes(array('submission_id'=>$submissionData->id),array('order'=>'id desc'));
		$candidateData = Candidates::model()->findByPk($submissionData->candidate_Id); 
		$jobData = Job::model()->findByPk($submissionData->job_id);
		if($jobData->jobStatus==12){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Alert !</strong> Sorry the Job Id # : '.$jobData->id.' has been closed.</div>');
			$this->redirect(array('job/joblisting'));
		}
		//$allTeamMembers = Client::model()->findAllByAttributes(array('super_client_id'=>$jobData->user_id));
		$allTeamMembers = Yii::app()->db->createCommand('SELECT * FROM `vms_client` Where super_client_id='.$jobData->user_id.' or (id='.$loginUserId.' and super_client_id=0)')->query()->readAll(); 
		
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$submissionData->job_id)->query()->readAll(); 
		 
		$sql = "SELECT admin.*,vms_teammember_job.id as member_id FROM `admin` inner join vms_teammember_job on admin.id =vms_teammember_job.staff_id where vms_teammember_job.job_id =".$submissionData->job_id; 
	    $mspInvitedTeam = Yii::app()->db->createCommand($sql)->queryAll();
		
		 
		if(isset($_POST['second_form'])){
			
			$olderForm = Yii::app()->session['wo_first_form'];
			$offerData = Offer::model()->findByAttributes(array('submission_id'=>$submissionData->id),array('order'=>'id desc'));
			
			$model->submission_id = $olderForm['can_submission_id']; 
			$model->job_id = $olderForm['job_id'];
			$model->offer_id = $offerData->id;
			//saving client id and vendor id to the workorder table table
			$model->client_id = $submissionData->client_id;
		   $model->created_by_id = $loginUserId;
		   $model->vendor_id = $submissionData->vendor_id;
		   $model->candidate_id = $submissionData->candidate_Id;
			 
			
			$model->wo_start_date = date("Y-m-d", strtotime($_POST['job_start_date'])); 
			$model->wo_end_date = date("Y-m-d", strtotime($_POST['job_end_date']));
			$model->approval_manager = $_POST['approval_manager'];
			if(!empty($_POST['cost_center'])) {
				$model->wo_cost_center = implode(',', $_POST['cost_center']);
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> Cost center Field must be required.</div>');
				$this->redirect(array('workOrderStep2','sub_id'=>$_GET['sub_id']));
			}
			if(!empty($_POST['project'])){
			$model->wo_project = implode(',', $_POST['project']);
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> Project Field must be required.</div>');
				$this->redirect(array('workOrderStep2','sub_id'=>$_GET['sub_id']));
			}
			if(!empty($_POST['timesheet_code'])) {
				$model->wo_timesheet_code = implode(',', $_POST['timesheet_code']);
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> Timesheet code Field must be required.</div>');
				$this->redirect(array('workOrderStep2','sub_id'=>$_GET['sub_id']));
			}
			$model->wo_bill_rate = $_POST['bill_rate'];  
			$model->wo_pay_rate = $_POST['pay_rate'];  
			$model->wo_over_time = $_POST['over_time']; 
			$model->wo_double_time = $_POST['double_time'];
			$model->wo_client_over_time = $_POST['client_over_time']; 
			$model->wo_client_double_time = $_POST['client_double_time'];  
			$model->wo_hiring_manager = $_POST['hiring_manager'];
			if(!empty($_POST['ref_notes'])) {
				$model->wo_internal_notes = $_POST['ref_notes'];
			}
			if(!empty($_POST['ref_notes_msp'])) {
			$model->wo_msp_notes = $_POST['ref_notes_msp'];
			}
			if(!empty($_POST['ref_notes_vendor'])) {
				$model->wo_vendor_notes = $_POST['ref_notes_vendor'];
			}
			$model->wo_form_submitted = 1; 
			
			
			 
			$model->save(false);
			
		/*$vendorJobSubmission = VendorJobSubmission::model()->findAll(array('condition'=>'job_id='.$olderForm['job_id']));
		   if($vendorJobSubmission){
				foreach($vendorJobSubmission as $value){
				 $vendor = Vendor::model()->findByPk($value['vendor_id']);
				 UtilityManager::emailWorkorder($vendor->email,$vendor->first_name,$vendor->last_name,$model->id);
				}
		   }
		   $mspadmin = Admin::model()->findByPk(47);
		   UtilityManager::emailWorkorder($mspadmin->email,$mspadmin->first_name,$mspadmin->last_name,$model->id);*/
			
			//$this->redirect(array('workorderList')); 
			$this->redirect(array('workorderView','id'=>$model->id)); 
			} 
		 
		$this->render('wostep2',array('jobData'=>$jobData,'submissionData'=>$submissionData,'candidateData'=>$candidateData,'invitedTeamMembers'=>$invitedTeamMembers,'mspInvitedTeam'=>$mspInvitedTeam,'allTeamMembers'=>$allTeamMembers)); 
		}
	public function actionCreateworkOrder(){
		$subData = VendorJobSubmission::model()->findByPk($_GET['submission-id']);
		$workorder_Send = array();
		$workorder_Send['job_id'] = $subData->job_id;
		$workorder_Send['can_submission_id'] = $_GET['submission-id'];
		$workordermodel = Workorder::model()->findAllByAttributes(
			array(
				'submission_id' => $_GET['submission-id'],
			),
			array(
				'order' => 'id desc',
				'limit' => 1,
			));
		if(!empty($workordermodel) && ($workordermodel[0]->workorder_status == 0 || $workordermodel[0]->workorder_status == 1)){
			$this->redirect(array('workorderView', 'id' => $workordermodel[0]->id));
		}else {
			Yii::app()->session['wo_first_form'] = $workorder_Send;
			$this->redirect(array('workOrderStep2', 'sub_id' => $_GET['submission-id']));
		}
	}
	public function actionCreateoffer(){
		$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']);
		$offer_step1 = array();
		$offer_step1['job_id'] = $submissionData->job_id;
		$offer_step1['can_submission_id'] = $_GET['submission-id'];
		$offermodel = Offer::model()->findAllByAttributes(
			array(
				'submission_id' => $_GET['submission-id'],
			),
			array(
				'order' => 'id desc',
				'limit' => 1,
			));
		if(!empty($offermodel) && ($offermodel[0]->status == 4 || $offermodel[0]->status == 1)){
			$this->redirect(array('view','id'=>$offermodel[0]->id));
		}else {
			Yii::app()->session['wo_first_form'] = $offer_step1;
			$this->redirect(array('Step2', 'sub_id' => $_GET['submission-id']));
		}
	}
	public function actionStep1(){ 
		 
		//$loginUserId = Yii::app()->user->id;
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id); 
		$jobData = Job::model()->findAllByAttributes(array('user_id'=>$loginUserId,'jobStatus'=>array(3,6,11))); 
		 
		if(isset($_POST['first_form'])){
			Yii::app()->session['wo_first_form'] = $_POST;
			
			$this->redirect(array('Step2','sub_id'=>$_POST['can_submission_id'])); 
			} 
		 
		$this->render('step1',array('jobData'=>$jobData)); 
		}



	public function actionStep2(){ 
		error_reporting(0);
		$model = new Offer; 
		
		ob_start();
		 
		$loginUserId = Yii::app()->user->id; 
		$submissionData = VendorJobSubmission::model()->findByPk($_GET['sub_id']); 
		$candidateData = Candidates::model()->findByPk($submissionData->candidate_Id); 
		$jobData = Job::model()->findByPk($submissionData->job_id);
		if($jobData->jobStatus==12){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Alert !</strong> Sorry the Job Id # : '.$jobData->id.' has been closed.</div>');
			$this->redirect(array('job/joblisting'));
		}
		$allTeamMembers = Client::model()->findAllByAttributes(array('super_client_id'=>$jobData->user_id)); 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$submissionData->job_id)->query()->readAll(); 
		 
		$sql = "SELECT admin.*,vms_teammember_job.id as member_id FROM `admin` inner join vms_teammember_job on admin.id =vms_teammember_job.staff_id where vms_teammember_job.job_id =".$submissionData->job_id; 
	    $mspInvitedTeam = Yii::app()->db->createCommand($sql)->queryAll();
		
		 
		if(isset($_POST['second_form'])){ 
		
			$olderForm = Yii::app()->session['wo_first_form'];			 
			 
			$model->ip = $_SERVER['REMOTE_ADDR']; 
			$model->created_by_id = Yii::app()->user->id; 
			$model->created_by_type = 'Client'; 
			$model->status = 4;
			$model->date_created = date('Y-m-d H:i:s'); 
			 
			$model->submission_id = $olderForm['can_submission_id']; 
			$model->job_id = $olderForm['job_id']; 
			
			$model->offer_pay_rate = $_POST['pay_rate'];
			$model->offer_bill_rate = $_POST['bill_rate'];  
			$model->payment_type = $_POST['payment_type']; 
			$model->shift = $_POST['shift']; 
			$model->hours_per_week = $_POST['hours_per_week']; 
			$model->over_time = $_POST['over_time']; 
			$model->double_time = $_POST['double_time'];
			$model->client_over_time = $_POST['client_over_time']; 
			$model->client_double_time = $_POST['client_double_time']; 
			//$model->location = $_POST['location']; 
			$model->job_start_date = date("Y-m-d", strtotime($_POST['job_start_date'])); 
			$model->job_end_date = date("Y-m-d", strtotime($_POST['job_end_date']));
			//$model->approval_manager = $_POST['approval_manager'];
			if(!empty($_POST['ref_notes'])){
			$model->internal_notes = $_POST['ref_notes'];
			}
			if(!empty($_POST['ref_notes_msp'])){
			$model->for_admin_notes = $_POST['ref_notes_msp'];
			}
			if(!empty($_POST['ref_notes_vendor'])) {
				$model->ref_notes_vendor = $_POST['ref_notes_vendor'];
			}
			$model->estimate_cost = $_POST['estimate_cost']; 
			
			$model->approver_manager_location = $_POST['approver_manager_location']; 
			$model->location_manager = $_POST['location_manager'];
			$model->valid_till_date = date("Y-m-d", strtotime($_POST['valid_till_date'])); 
			
			//saving client id and vendor id to the offer table
			$model->client_id = $submissionData->client_id;
			$model->vendor_id = $submissionData->vendor_id;
			$model->candidate_id = $submissionData->candidate_Id;
			 
			$model->save(false); 
			
			$submissionData->resume_status = 7;
			$submissionData->save(false);
			
			$vendorJobSubmission = VendorJobSubmission::model()->findAllByAttributes(array('job_id'=>$olderForm['job_id']),array('group'=>'vendor_id'));

		   if($vendorJobSubmission){
				foreach($vendorJobSubmission as $value){
				 $vendor = Vendor::model()->findByPk($value['vendor_id']);
					//echo $vendor->email;
				 UtilityManager::emailWorkorder($vendor->email,$vendor->first_name,$vendor->last_name,$model->id);
				}
		   }
		   $mspadmin = Admin::model()->findByPk(47);
		   UtilityManager::emailWorkorder($mspadmin->email,$mspadmin->first_name,$mspadmin->last_name,$model->id);
			
			//$this->redirect(array('job/worker','id'=>$submissionData->job_id,'type'=>'worker'));
			$this->redirect(array('offer/view','id'=>$model->id));  
			} 
		 
		$this->render('step2',array('jobData'=>$jobData,'submissionData'=>$submissionData,'candidateData'=>$candidateData,'invitedTeamMembers'=>$invitedTeamMembers,'mspInvitedTeam'=>$mspInvitedTeam,'allTeamMembers'=>$allTeamMembers)); 
		}
		
	public function actionLocationManager(){
			$location = Location::model()->findByPk($_POST['location_id']);
			echo '<option value="'.$location->location_account_mananger.'">'.$location->location_account_mananger.'</option>';
		} 
	 
	public function actionCreate() 
	{  
		$this->layout = 'admin'; 
		$id = 	$_GET['id']; 
		$model = new Offer; 
		$jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id)); 
		 
		if(isset($_GET['offerId'])){ 
			if($_GET['offerId']!='Null'){ 
				$model = Offer::model()->findByPk($_GET['offerId']); 
				} 
			} 
		 
		 
		if(isset($_POST['Offer'])){ 
			$model->attributes = $_POST['Offer']; 
			$model->submission_id = $_GET['submission-id']; 
			$model->ip = $_SERVER['REMOTE_ADDR']; 
			$model->created_by_id = Yii::app()->user->id; 
			$model->created_by_type = 'Client'; 
			 
			$model->issued_offer_date = date("Y-m-d", strtotime($_POST['Offer']['issued_offer_date'])); 
			$model->valid_till_date = date("Y-m-d", strtotime($_POST['Offer']['valid_till_date'])); 
			$model->date_created = date("Y-m-d"); 
			if($model->save(false)){ 
				 
				$this->emailNotification($model); 
				 
				$this->redirect(array('create','id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'offerId'=>$model->id)); 
				} 
			} 
		 
		 
		 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['id'])->query()->readAll(); 
		if(!$jobmodel) 
			$this->redirect(array('job/joblisting')); 
			 
		$this->render('create',array('jobmodel'=>$jobmodel,'invitedTeamMembers'=>$invitedTeamMembers,'model'=>$model)); 
	} 
	 
	 
	public function emailNotification($offerModel,$subject){ 
		
	  $offer = Offer::model()->findByPk($_GET['id']);
	  
	  //$vendorJobSubmission = VendorJobSubmission::model()->findByPk($_GET['submission-id']); 
	  $vendorJobSubmission = VendorJobSubmission::model()->findByPk($offer->submission_id); 
	 
	 
	  $jobmodel = Job::model()->findByPk($vendorJobSubmission->job_id); 
	   
	  $candidate = Candidates::model()->findByPk($vendorJobSubmission->candidate_Id); 
	 
	   $sql ="select vms_client.* FROM  vms_client,vms_teammember_job  WHERE vms_client.`id` =vms_teammember_job.teammember_id and vms_teammember_job.job_id=".$jobmodel->id; 
		 
		$allMemeber = Yii::app()->db->createCommand($sql)->query()->readAll(); 
	 
		$message ='0ffer ID: '.$offerModel->id; 
		$message .='<br>Job ID: '.$jobmodel->id; 
		$message .='<br>Job Title: '.$jobmodel->title; 
		$message .='<br>Candidates Name: '.$candidate->first_name.' '.$candidate->last_name; 
		$message .='<br>Email Address: '.$candidate->email;
		$message .='<br>Pay Rate ( Candidate ): '.$offerModel->offer_pay_rate;
		$message .='<br>Over Time ( Candidate ): '.$offerModel->over_time;
		$message .='<br>Double Time ( Candidate ): '.$offerModel->double_time;
		$message .='<br>Bill Rate ( For Client ): '.$offerModel->offer_bill_rate;
		$message .='<br>Over Time  ( For CLient ): '.$offerModel->client_over_time;
		$message .='<br>Double Time ( For Client ): '.$offerModel->client_double_time;
		// echo '<br>Offer Created By: '.$client->first_name.' '.$Client->last_name; 
		//$message .='<br>Date of issue of Offer: '.$offerModel->issued_offer_date;
		$message .='<br>Validate of the Offer: '.$offerModel->valid_till_date; 
		$message .='<br>Date of Time of Offer: '.$offerModel->date_created; 
		$message .='<br>Ip address: '.$offerModel->ip; 
		 
		$client = Client::model()->findByPk($jobmodel->user_id); 
 		UtilityManager::offerNotification($client->email,$jobmodel->title,$message); 
		 
		if($allMemeber) { 
		 foreach($allMemeber as $value){ 
			UtilityManager::offerNotification($value['email'],$jobmodel->title,$message); 
		 } 
		} 
		 
	  $vendorJobSubmission = VendorJobSubmission::model()->findAll(array('condition'=>'job_id='.$jobmodel->id)); 
		if($vendorJobSubmission){ 
		 foreach($vendorJobSubmission as $value){ 
		  $vendor = Vendor::model()->findByPk($value->vendor_id); 
		  
		  UtilityManager::offerNotification($vendor->email,$jobmodel->title,$message); 
		 } 
		} 
	 
		$sql ="select admin.* FROM  admin,vms_teammember_job  WHERE admin.`id` =vms_teammember_job.staff_id and vms_teammember_job.job_id=".$jobmodel->id; 
	   $allMemebers = Yii::app()->db->createCommand($sql)->query()->readAll();
	 
	   if($allMemebers) {
		foreach($allMemebers as $value){
		  UtilityManager::offerNotification($value['email'],$jobmodel->title,$message); 
		} 
	   } 
	 } 
	  
	  
	public function actionOfferView(){ 
		error_reporting(0); 
        
        $offerID = $_GET['id']; 
	    $offerModel =  Offer::model()->findByPk($offerID); 
	    $backgroundModel = new OfferBackground; 
	    $exhibitModel = new OfferExhibit; 
	      $sql ="select sub.id,sub.job_id, 
			can.first_name,can.last_name,can.id as c_id,vendor.organization as v_organization,sub.estimate_start_date,
			sub.candidate_pay_rate,offer.status as offer_status,can.current_location,offer.approval_manager 
			FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor 
			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and sub.id = offer.submission_id and  offer.id=".$offerModel->id;   
		$offers = Yii::app()->db->createCommand($sql)->queryRow(); 
		 
        if(isset($_POST['Offer']['workorder_status'])){ 
            foreach($_POST['Offer']['workorder_status'] as $key=>$value){ 
            	$offerModel->workorder_status =$key; 
            } 
        	 
	    	if($offerModel->save(false)){ 
	    		Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Offer Work order status saved successfully.</div>'); 
	    	}	 
        }else if(!empty($_POST['Offer']['status'])){ 
	    	$offerModel->status = $_POST['Offer']['status']; 
	    	if($offerModel->save(false)){ 
	    		Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Offer status saved successfully.</div>'); 
	    	}	 
	    } 
	     
	    if(isset($_POST['OfferBackground']) && !isset($_POST['OfferBackground']['status'])){ 
	    	 
	    	$backgroundModel->attributes = $_POST['OfferBackground']; 
	    	$backgroundModel->offer_id = $offerModel->id; 
	    	$backgroundModel->date_created = date('Y-m-d'); 
	    	 
	    	$backgroundModel->file=CUploadedFile::getInstance($backgroundModel,'file'); 
	    	$name = rand(1000,9999) . time(); // rand(1000,9999) optional 
	    	$name = md5($name).$backgroundModel->file; //optional 
	    	if($backgroundModel->save(false)){ 
	    		$path = Yii::app()->basePath.'/../background_verify/'; 
	    		if (!file_exists($path)) { 
	    			mkdir($path, 0777, true); 
	    		} 
	    		$backgroundModel->file->saveAs($path.'/'.$name); 
	    		$backgroundModel->file = $name; 
  				$backgroundModel->save(false); 
	    		Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Offer background verification saved successfully.</div>'); 
	    	} 
	    	 
	    }else  if(!empty($_POST['OfferBackground']['status'])){ 
	    	$backgroundModelList = OfferBackground::model()->findAll(array('condition'=>'offer_id='.$offerModel->id)); 
	    	foreach($backgroundModelList as $value){ 
	    		 
	    		$backgroundUpdated = OfferBackground::model()->findByPk($value->id); 
		    	$backgroundUpdated->status = $_POST['OfferBackground']['status']; 
		    	 
		    	if($backgroundUpdated->save(false)){ 
		    		Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> background verification status saved successfully.</div>'); 
		    	} 
	    	} 
	    	 
	    } 
	    if(isset($_POST['OfferExhibit']) && !isset($_POST['OfferExhibit']['status'])){ 
	    	 
	    	$exhibitModel->attributes = $_POST['OfferExhibit']; 
	    	$exhibitModel->offer_id = $offerModel->id; 
	    	$exhibitModel->date_created = date('Y-m-d'); 
	    	 
	    	$exhibitModel->file=CUploadedFile::getInstance($exhibitModel,'file'); 
	    	$name = rand(1000,9999) . time(); // rand(1000,9999) optional 
	    	$name = md5($name).$exhibitModel->file; //optional 
	    	if($exhibitModel->save(false)){ 
	    		$path = Yii::app()->basePath.'/../exhibit/'; 
	    		if (!file_exists($path)) { 
	    			mkdir($path, 0777, true); 
	    		} 
	    		$exhibitModel->file->saveAs($path.'/'.$name); 
	    		$exhibitModel->file = $name; 
  				$exhibitModel->save(false); 
	    		Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Offer exhibit saved successfully.</div>'); 
	    	} 
	    	 
	    }else  if(!empty($_POST['OfferExhibit']['status'])){ 
	    	$exhibitModelList = OfferExhibit::model()->findAll(array('condition'=>'offer_id='.$offerModel->id)); 
	    	foreach($exhibitModelList as $value){ 
	    		 
	    		$exhibitUpdated = OfferExhibit::model()->findByPk($value->id); 
		    	$exhibitUpdated->status = $_POST['OfferExhibit']['status']; 
		    	 
		    	if($exhibitUpdated->save(false)){ 
		    		Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> exhibit status saved successfully.</div>'); 
		    	} 
	    	} 
	    	 
	    } 
	    
	   $backgroundModelList = OfferBackground::model()->findAll(array('condition'=>'offer_id='.$offerModel->id)); 
	    $exhibitModelList = OfferExhibit::model()->findAll(array('condition'=>'offer_id='.$offerModel->id)); 
		$this->render('overview',array('offerModel'=>$offerModel,'backgroundModel'=>$backgroundModel,'exhibitModel'=>$exhibitModel,'backgroundModelList'=>$backgroundModelList,'exhibitModelList'=>$exhibitModelList,'offers'=>$offers,'')); 
	}  
    public function actionDownloadBackground($id){ 
   		$offerBackground = OfferBackground::model()->findByPk($id); 
	 	//$path = Yii::app()->basePath.'/../background_verify/'; 
	 	
		$path = Yii::app()->basePath.'/../background_verify/';
		$file = $path.$offerBackground->file; 
	  
  
		if (file_exists($file)) { 
		    header('Content-Description: File Transfer'); 
		    header('Content-Type: application/octet-stream'); 
		    header('Content-Disposition: attachment; filename="'.basename($file).'"'); 
		    header('Expires: 0'); 
		    header('Cache-Control: must-revalidate'); 
		    header('Pragma: public'); 
		    header('Content-Length: ' . filesize($file)); 
		    readfile($file); 
		    exit; 
		} 
 	} 
 	public function actionDownloadExhibit($id){ 
   		$offerExhibit = OfferExhibit::model()->findByPk($id); 
	 	//$path = Yii::app()->basePath.'/../exhibit/';
		$path = Yii::app()->basePath.'/../exhibit/';
	 	$file = $path.$offerExhibit->file; 
	  
		if (file_exists($file)) { 
		    header('Content-Description: File Transfer'); 
		    header('Content-Type: application/octet-stream'); 
		    header('Content-Disposition: attachment; filename="'.basename($file).'"'); 
		    header('Expires: 0'); 
		    header('Cache-Control: must-revalidate'); 
		    header('Pragma: public'); 
		    header('Content-Length: ' . filesize($file)); 
		    readfile($file); 
		    exit; 
		} 
 	} 
	public function actionDeleteBackground(){ 
		$id = $_GET['id']; 
 		$model = OfferBackground::model()->findByPk($id); 
 		$offerID = $model->offer_id; 
		if($model->delete()){ 
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Background verification deleted successfully.</div>'); 
			 
		}else{ 
				Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failed!</strong> Background verification not deleted, try again.</div>'); 
		} 
		$this->redirect(array('offer/view','id'=>$offerID)); 
	} 
	public function actionDeleteExhibit(){ 
		$id = $_GET['id']; 
 		$model = OfferExhibit::model()->findByPk($id); 
 		$offerID = $model->offer_id; 
		if($model->delete()){ 
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Exhibit deleted successfully.</div>'); 
			 
		}else{ 
				Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failed!</strong> Exhibit not deleted, try again.</div>'); 
		} 
		$this->redirect(array('offer/view','id'=>$offerID)); 
	} 
	  

	public function actionView(){

		//error_reporting(0);
		$offerID = $_GET['id'];
		$offerModel =  Offer::model()->findByPk($offerID);
		$backgroundModel = new OfferBackground;
		$exhibitModel = new OfferExhibit;
		
		$superClient = UtilityManager::superClient(Yii::app()->user->id);
		$DigitalDocs = DigitalDocs::model()->findByAttributes(array('client_id'=>$superClient,'doc_type'=>1));
 

		$sql ="select sub.id,sub.job_id,
	   can.first_name,can.last_name,can.id as c_id,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,offer.status as offer_status,offer.approver_manager_location

	   FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

	   WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and sub.id = offer.submission_id and  offer.id=".$offerModel->id;


		$offers = Yii::app()->db->createCommand($sql)->queryRow();
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$offerModel->job_id)->query()->readAll();
		$sql = "SELECT admin.*,vms_teammember_job.id as member_id FROM `admin` inner join vms_teammember_job on admin.id =vms_teammember_job.staff_id where vms_teammember_job.job_id =".$offerModel->job_id;
		$mspInvitedTeam = Yii::app()->db->createCommand($sql)->queryAll();

		//code for updation of status of offer from dropdown submitted value


		if(isset($_POST['Offer'])){
			$offerModel->status = $_POST['Offer']['status'];
			$offerModel->offer_accept_date = date("Y-m-d H:i:s");
			$offerModel->save(false);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Status!</strong> Changed successfully.</div>');
		}



		//below code was for approval, rejection of offer so for now we will do this through dropdown
		if(isset($_GET['type']) && $_GET['type']=='accepted'){
			
			$offerModel->status = 1;
			$offerModel->modified_by_type = 'Client';
			$offerModel->modified_by_id = Yii::app()->user->id;
			$offerModel->offer_accept_date = date('Y-m-d H:i:s');
			if($offerModel->save(false)){
			
			//digital document work
			
		if(!empty($DigitalDocs)){
				require_once Yii::getPathOfAlias('webroot').'/pdfExt/hellosign/vendor/autoload.php'; 
				$client = new HelloSign\Client('f8d49c7ffaed7522cee96aef781b71b577cc9e345449de2e9453b79b07428649');
				$request = new HelloSign\SignatureRequest;
			
			$candidate = Candidates::model()->findByPk($offerModel->candidate_id);
			$vendor = Vendor::model()->findByPk($offerModel->vendor_id);
			
			require_once Yii::getPathOfAlias('webroot').'/pdfExt/fpdf/fpdf.php';
			require_once Yii::getPathOfAlias('webroot').'/pdfExt/fpdi/fpdi.php';
			
			// initiate FPDI
			$pdf = new FPDI();
			
			// set the source file
			$pageCount = $pdf->setSourceFile(Yii::app()->basePath.'/../digitalDocs/'.$DigitalDocs->file);
			
			for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
			
			$tplIdx = $pdf->importPage($pageNo);
			
			// add a page
			$pdf->AddPage();
			$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
			
			// font and color selection
			$pdf->SetFont('Helvetica');
			
			// now write some text above the imported page
			 
			if($pageNo == $pageCount) {
				$pdf->SetXY(25, 116);
				$pdf->Write(8, 'Vendor Name :  '.$vendor->first_name.' '.$vendor->last_name.'                          Date : '.date('m-d-Y'));
				$pdf->SetXY(25, 130);
				$pdf->Write(8, 'Printed Name :  '.$candidate->first_name.' '.$candidate->last_name.'                       Date : '.date('m-d-Y'));
			}
			}
			$offerFileName = time().$DigitalDocs->file;
 			$pdf->Output(Yii::app()->basePath.'/../unsignedDocs/'.$offerFileName, "F");
			
			//Hello sign api Code by zoobia
			
			$request->enableTestMode();
			$request->setTitle($DigitalDocs->doc_name);
			$request->setSubject($DigitalDocs->digital_email_subject);
			$request->setMessage($DigitalDocs->digital_email_message);
			$request->setSigningRedirectUrl('http://'.$_SERVER['HTTP_HOST'].'/index.php/Client/offer/offerHelloSignResponse');
			
			$request->addSigner($vendor->email, $vendor->first_name.' '.$vendor->last_name);
			//$request->addSigner("zoobia_humayun@live.com", "Zoobia");
			 
			$request->addFile(Yii::app()->basePath.'/../unsignedDocs/'.$offerFileName); 
			$request->setFormFieldsPerDocument(
				array( //everything
					array( //document 1
						array( //component 1
							"api_id" => 'signature_0656deb6',
							"name" => "Signature",
							"type"=> "signature",
							"x" => 80,
							"y" => 405,
							"width" => 170,
							"height" => 22,
							"required" => true,
							"signer" => 0,
							"page"=> $pageCount
						),
						array( //component 2
							"api_id" => 'date_77da448c',
							"name" => "Date",
							"type" => "date_signed",
							"x" => 320,
							"y" => 405,
							"width" => 110,
							"height" => 22,
							"required" => true,
							"signer" => 0,
							"page"=> $pageCount
						) 
					)
				)
			);
			
			$response = $client->sendSignatureRequest($request);
			  if(!$response->hasError()){
				  $signature_lists = $request->getSignatures($response);
						foreach($signature_lists as $list){
							$signature_id  = $list->getId();
 							$offerModel->hellosign_signature_id = $signature_id;
  				  }
				$offerModel->hellosign_api_email = 1;
				$offerModel->hellosign_api_req_id = $request->getId();
				$offerModel->hellosign_api_sign_status = 0;
				$offerModel->hellosign_req_date = date('Y-m-d');
				$offerModel->offer_digital_doc = $offerFileName;
				$offerModel->save(false);
			  }
 		}
				$subData = VendorJobSubmission::model()->findByPk($offerModel->submission_id);
				
				//creating session and redirecting to the workorder step2
				$workorder_Send = array();
				$workorder_Send['job_id'] = $subData->job_id;
				$workorder_Send['can_submission_id'] = $offerModel->submission_id;
				
				Yii::app()->session['wo_first_form'] = $workorder_Send;
				$subData->resume_status = 9;
				if($subData->save(false)){ 
				Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Offer status accepted successfully.</div>');
				$subject = "Offer Approved";
				//commenting offer email notification now
				//$this->emailNotification($offerModel,$subject);
				$this->redirect(array('workOrderStep2','sub_id'=>$offerModel->submission_id));
				}
			}
		}else if(isset($_GET['type']) && $_GET['type']=='workorder'){
			$subData = VendorJobSubmission::model()->findByPk($offerModel->submission_id);
				
			//creating session and redirecting to the workorder step2
			$workorder_Send = array();
			$workorder_Send['job_id'] = $subData->job_id;
			$workorder_Send['can_submission_id'] = $offerModel->submission_id;
			
			Yii::app()->session['wo_first_form'] = $workorder_Send;
			
			$this->redirect(array('workOrderStep2','sub_id'=>$offerModel->submission_id));
			
			}else if(isset($_POST['rejected'])){
			$offerModel->status = 2;
			$offerModel->reason_for_rejection = $_POST['reason_for_rejection'];
			$offerModel->notes = $_POST['notes'];
			if($offerModel->save(false)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Offer Rejected Successfully.</div>');
				$subject = "Offer Rejected";
				$this->emailNotification($offerModel,$subject);
			}
		}else if(!empty($_GET['workorder-status'])){

			$offerModel->workorder_status = $_GET['workorder-status'];
			if($offerModel->save(false)){
				$label = $offerModel->workorder_status ==2?"Rejected":"Accepted";
				Yii::app()->user->setFlash('success', '<strong>Success!</strong> Work order status <strong>'.$label.'</strong> successfully.');

				$subject = $offerModel->workorder_status ==2?"Offer Rejected":"Offer Accepted";
				$this->emailNotification($offerModel,$subject);
			}
		}


		//echo "<pre>";print_r($_POST);exit;
		
		$digitalDocStatus = 0;
		$filePath = '';
		//if digital signature is done then show the file<br />
		if(!empty($DigitalDocs)){
			if(!empty($offerModel->hellosign_api_req_id)){
				if($offerModel->hellosign_api_sign_status !=0 ){ 
					 $digitalDocStatus = 1;
				 }
				$filePath = '/signedDocs/'.$offerModel->offer_digital_doc;
			}
		}
		$this->render('overview',array('filePath'=>$filePath,'digitalDocStatus'=>$digitalDocStatus,'offerModel'=>$offerModel,'offers'=>$offers,'invitedTeamMembers'=>$invitedTeamMembers,'mspInvitedTeam'=>$mspInvitedTeam));


	}  

	public function actionBackground(){
		
        $offerID = $_GET['id'];
	    $offerModel =  Offer::model()->findByPk($offerID);
	    $backgroundModel = new OfferBackground;

	     if(isset($_POST['OfferBackground']) && !isset($_POST['completed'])){
	    	
	    	$backgroundModel->attributes = $_POST['OfferBackground'];
	    	$backgroundModel->offer_id = $offerModel->id;
	    	$backgroundModel->date_created = date('Y-m-d');
	    	
	    	$backgroundModel->file=CUploadedFile::getInstance($backgroundModel,'file');

	    	$name = rand(1000,9999) . time(); // rand(1000,9999) optional
	    	$name = md5($name).$backgroundModel->file; //optional

	    	if($backgroundModel->save(false)){

	    		$path = Yii::app()->basePath.'/../background_verify/';

	    		if (!file_exists($path)) {
	    			mkdir($path, 0777, true);
	    		}
	    		$backgroundModel->file->saveAs($path.'/'.$name);

	    		$backgroundModel->file = $name;
	    		$backgroundModel->status = 1;
  				$backgroundModel->save(false);

	    		Yii::app()->user->setFlash('success', '<strong>Success!</strong> Offer background verification saved successfully.');
	    	}
	    	
	    }else if(isset($_POST['completed'])){
			
			$offerModel->background_status = 1;
			$offerModel->save(false);
			
	    	$backgroundModelList = OfferBackground::model()->findAll(array('condition'=>'offer_id='.$offerModel->id));
	    	foreach($backgroundModelList as $value){
	    		$backgroundUpdated = OfferBackground::model()->findByPk($value->id);
		    	$backgroundUpdated->status = 2;
		    	if($backgroundUpdated->save(false)){
		    		Yii::app()->user->setFlash('success', '<strong>Success!</strong> background verification status saved successfully.');
		    		$this->redirect(array('background','id'=>$offerModel->id));
		    	}
	    	}
	    	
	    }

	     $backgroundModelList = OfferBackground::model()->findAll(array('condition'=>'offer_id='.$offerModel->id));
	   

	    $this->render('background',array('offerModel'=>$offerModel,'backgroundModel'=>$backgroundModel,'backgroundModelList'=>$backgroundModelList));
	 }

	 public function actionExhibit(){

        $offerID = $_GET['id'];
	    $offerModel =  Offer::model()->findByPk($offerID);
	    $exhibitModel = new OfferExhibit;

	      if(isset($_POST['OfferExhibit']) && !isset($_POST['completed'])){
	    	
	    	$exhibitModel->attributes = $_POST['OfferExhibit'];
	    	$exhibitModel->offer_id = $offerModel->id;
	    	$exhibitModel->date_created = date('Y-m-d');
	    	
	    	$exhibitModel->file=CUploadedFile::getInstance($exhibitModel,'file');

	    	$name = rand(1000,9999) . time(); // rand(1000,9999) optional
	    	$name = md5($name).$exhibitModel->file; //optional

	    	if($exhibitModel->save(false)){

	    		$path = Yii::app()->basePath.'/../exhibit/';

	    		if (!file_exists($path)) {
	    			mkdir($path, 0777, true);
	    		}
	    		$exhibitModel->file->saveAs($path.'/'.$name);

	    		$exhibitModel->file = $name;
  				$exhibitModel->save(false);

	    		Yii::app()->user->setFlash('success', '<strong>Success!</strong> Offer exhibit saved successfully.');

	    	}
	    	
	    }else  if(isset($_POST['completed'])){
			
			$offerModel->exhibit_status = 1;
			$offerModel->save(false);
	    	$exhibitModelList = OfferExhibit::model()->findAll(array('condition'=>'offer_id='.$offerModel->id));

	    	foreach($exhibitModelList as $value){
	    		
	    		$exhibitUpdated = OfferExhibit::model()->findByPk($value->id);
	    		$exhibitUpdated->status = 2;
		    	if($exhibitUpdated->save(false)){


		    		Yii::app()->user->setFlash('success', '<strong>Success!</strong> exhibit status saved successfully.');
		    		$this->redirect(array('background','id'=>$offerModel->id));
		    	}
	    	}
	    	
	    }



	    $exhibitModelList = OfferExhibit::model()->findAll(array('condition'=>'offer_id='.$offerModel->id));

	    $this->render('exhibit',array('offerModel'=>$offerModel,'exhibitModel'=>$exhibitModel,'exhibitModelList'=>$exhibitModelList));
	 }

	public function actionNotes(){

        $offerID = $_GET['id'];
	    $offerModel =  Offer::model()->findByPk($offerID);

	    $this->render('notes',array('offerModel'=>$offerModel));
	}

	public function actionOther(){

        $offerID = $_GET['id'];
	    $offerModel =  Offer::model()->findByPk($offerID);

	    $this->render('other',array('offerModel'=>$offerModel));
	}

	public function actionOnBoarding(){
		
		//pdf and digital doc work
		$superClient = UtilityManager::superClient(Yii::app()->user->id);
		$DigitalDocs = DigitalDocs::model()->findByAttributes(array('client_id'=>$superClient,'doc_type'=>2));
		 
		$workOrderID = $_GET['id'];
		$workOrderModel = Workorder::model()->findByPk($workOrderID);
        $offerID = $workOrderModel->offer_id;
		
	    $offerModel =  Offer::model()->findByPk($offerID);
	    $submissionModel =  VendorJobSubmission::model()->findByPk($offerModel->submission_id);

	    $vendorModel =  Vendor::model()->findByPk($submissionModel->vendor_id);
	    
	    
	    $candidateModel = Candidates::model()->findByPk($submissionModel->candidate_Id);
	    $candidateModel->candidate_ID = "CID-".$candidateModel->id.$submissionModel->id.$offerModel->id;
	    $candidateModel->emp_password = "CID-".$candidateModel->id.$submissionModel->id.$offerModel->id;
		
		

	    if(isset($_POST['Candidates'])){
			
			$location = Location::model()->findByPk($offerModel->approver_manager_location);
		
			//////////////**************///////// 
			
	     	$candidateModel->attributes = $_POST['Candidates'];
	     	//$candidateModel->emp_password = md5($_POST['Candidates']['emp_password']);
			
			//file uploading will be now only from vendor side
	     	/*$candidateModel->profile_mg=CUploadedFile::getInstance($candidateModel,'profile_mg');

	    	$name = rand(1000,9999) . time(); // rand(1000,9999) optional
	    	$name = md5($name).$candidateModel->profile_mg; //optional
	    	$path = Yii::app()->basePath.'/../profile_img/';
	    	if (!file_exists($path)) {
	    			mkdir($path, 0777, true);
	    	}
			
			if($uploadedimage!=null){
				$candidateModel->profile_mg->saveAs($path.'/'.$name);
				$candidateModel->profile_mg = $name;
			}*/
			
			$candidateModel->save(false);
			
			//creating contract for this workorder
			$contractModel = new Contract;
			$contractModel->workorder_id = $workOrderModel->id;
			$contractModel->offer_id = $workOrderModel->offer_id;
			$contractModel->created_by_id = Yii::app()->user->id;
			$contractModel->submission_id = $workOrderModel->submission_id;
			$contractModel->job_id = $workOrderModel->job_id;
			$contractModel->client_id = $workOrderModel->client_id;
			$contractModel->vendor_id = $workOrderModel->vendor_id;
			$contractModel->candidate_id = $workOrderModel->candidate_id;
			//submitting the same which was selected while onboarding of candidate.
			$contractModel->start_date = date('Y-m-d',strtotime($_POST['onboard_changed_start_date']));
			$contractModel->end_date = date('Y-m-d',strtotime($_POST['onboard_changed_end_date']));
			$contractModel->job_title = $_POST['job_title'];
			$contractModel->candidate_dept = $_POST['candidate_dept'];
			$contractModel->supervisor_name = $_POST['supervisor_name'];
			$contractModel->supervisor_email = $_POST['supervisor_email'];
			$contractModel->supervisor_phone = $_POST['supervisor_phone'];
			$contractModel->save(false);
			//end of contract creation
			
			$workOrderModel->onboard_changed_start_date = date('Y-m-d',strtotime($_POST['onboard_changed_start_date']));
			$workOrderModel->onboard_changed_end_date = date('Y-m-d',strtotime($_POST['onboard_changed_end_date']));
			$workOrderModel->on_board_status = 1;
			if($workOrderModel->save(false)){
				$submissionModel->resume_status = 8;
				$submissionModel->save(false);
				//email to suprevisor
				UtilityManager::supervisoremailOnboard($contractModel->id);
				//email for candidate login
				$this->emailOnboard($_POST['Candidates']['emp_password'],$_POST['Candidates']['emp_official_email'],$_POST['Candidates']['email']);
				}
			
			// do this code if there is a document. Else do the normal things	
			if(!empty($DigitalDocs) && $DigitalDocs->status=='Active')
			{
				
				require_once Yii::getPathOfAlias('webroot').'/pdfExt/hellosign/vendor/autoload.php'; 
				$client = new HelloSign\Client('f8d49c7ffaed7522cee96aef781b71b577cc9e345449de2e9453b79b07428649');
				$request = new HelloSign\SignatureRequest;
				
				//////*************************//////////
				
				require_once Yii::getPathOfAlias('webroot').'/pdfExt/fpdf/fpdf.php';
				require_once Yii::getPathOfAlias('webroot').'/pdfExt/fpdi/fpdi.php';
				
				// initiate FPDI
				$pdf = new FPDI();
				
				// set the source file
				$pageCount = $pdf->setSourceFile(Yii::app()->basePath.'/../digitalDocs/'.$DigitalDocs->file);
				
				for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
				
				$tplIdx = $pdf->importPage($pageNo);
				
				// add a page
				$pdf->AddPage();
				$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				
				// font and color selection
				$pdf->SetFont('Helvetica', '', 10, '', true);
				
				// now write some text above the imported page
				 
				if($pageNo == 1) {
					$pdf->SetXY(60, 65.5);
					$pdf->Write(8, $candidateModel->first_name.' '.$candidateModel->last_name);
					$pdf->SetXY(60, 70);
					$pdf->Write(8, $_POST['job_title']);
					$pdf->SetXY(60, 74.5);
					$pdf->Write(8, $_POST['supervisor_name']);
					$pdf->SetXY(60, 79);
					$pdf->Write(8, $location->name);
				}
				
				if($pageNo == $pageCount) {
					$pdf->SetXY(12.5, 93.5);
					$pdf->Write(8, $candidateModel->first_name);
					$pdf->SetXY(110, 93.5);
					$pdf->Write(8, $_POST['supervisor_name']);
				}
				
				}
				$offerFileName = time().$DigitalDocs->file;
				$pdf->Output(Yii::app()->basePath.'/../unsignedDocs/'.$offerFileName, "F");
				
				
				//Hello sign api Code by zoobia
				
				$request->enableTestMode();
				$request->setTitle($DigitalDocs->doc_name);
				$request->setSubject($DigitalDocs->digital_email_subject);
				$request->setMessage($DigitalDocs->digital_email_message);
				$request->setSigningRedirectUrl('http://'.$_SERVER['HTTP_HOST'].'/index.php/Client/offer/onBoardHelloSignResponse');				
				/*$request->addSigner($candidateModel->email, $candidateModel->first_name.' '.$candidateModel->last_name);
				$request->addSigner($candidateModel->email, $_POST['supervisor_name']);*/
				//$request->addSigner('zoobia_humayun@live.com', 'Zoobia Humayun' , 0);
				 
				$request->addSigner($candidateModel->emp_official_email, $candidateModel->first_name.' '.$candidateModel->last_name , 0);
				$request->addSigner(new HelloSign\Signer(array(
					'name'          => $_POST['supervisor_name'] ,
					'email_address' => $_POST['supervisor_email'],
					"order" => 1
					)));
					
				/*$request->addSigner('zoobia_humayun@live.com', 'Zoobia Humayun', 0);
				$request->addSigner(new HelloSign\Signer(array(
					'name'          => "Malik Fayyaz" ,
					'email_address' => "zoobia.humayun@gmail.com",
					"order" => 1
					)));*/
				//$request->addSigner("mr.malikfayyaz@gmail.com", "Malik Fayyaz");
				
				$random_prefix = 'tests' . rand(1, 10000);
							  
				$request->addFile(Yii::app()->basePath.'/../unsignedDocs/'.$offerFileName); 
				$request->setFormFieldsPerDocument(
					array( //everything
						array( //document 1
							array( //component 1
								"api_id" => $random_prefix . "_1",
								"name" => "Employee",
								"type"=> "signature",
								"x" => 50,
								"y" => 237,
								"width" => 170,
								"height" => 22,
								"required" => true,
								"signer" => 0,
								"page"=> $pageCount
							),
							array( //component 2
								"api_id" => $random_prefix . "_2",
								"name" => "Date",
								"type" => "date_signed",
								"x" => 50,
								"y" => 295,
								"width" => 110,
								"height" => 22,
								"required" => true,
								"signer" => 0,
								"page"=> $pageCount
							) ,
							array( //component 1
								"api_id" => $random_prefix . "_3",
								"name" => "Supervisor",
								"type"=> "signature",
								"x" => 320,
								"y" => 237,
								"width" => 170,
								"height" => 22,
								"required" => true,
								"signer" => 1,
								"page"=> $pageCount
							),
							array( //component 2
								"api_id" => $random_prefix . "_4",
								"name" => "Date",
								"type" => "date_signed",
								"x" => 320,
								"y" => 295,
								"width" => 110,
								"height" => 22,
								"required" => true,
								"signer" => 1,
								"page"=> $pageCount
							) 
						)
					)
				);
				
				$response = $client->sendSignatureRequest($request);
				if(!$response->hasError()){
						$signature_lists = $request->getSignatures($response);
						foreach($signature_lists as $list){
							$signature_id  = $list->getId();
							$order = $list->getOrder() + 1;
							$name = 'hellosign_signer'.$order.'_id';
							$contractModel->$name = $signature_id;
							 
						}
						
					  $contractModel->hellosign_api_email = 1;
					  $contractModel->hellosign_api_req_id = $request->getId();
					  $contractModel->hellosign_api_sign_status = 0;
					  $contractModel->hellosign_req_date = date('Y-m-d');
					  $contractModel->offer_digital_doc = $offerFileName;
					  $contractModel->save(false);
				}  
			}
			
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Status!</strong> The Candidate has been successfully on-boarded. Candidate Login information has been emailed.</div>'); 
			$this->redirect(array('workorderView','id'=>$workOrderModel->id));
	     }
		 
		 
		 //for now commenting the code
	     /*if(isset($_POST['Offer'])){
	     	$offerModel->attributes = $_POST['Offer'];
			$offerModel->job_start_date= date('Y-m-d',strtotime($_POST['Offer']['job_start_date']));
			$offerModel->on_board_status = 1;
	     	$offerModel->save(false);
			
			$this->emailOnboard($_POST['Candidates']['emp_password'],$_POST['Candidates']['emp_official_email'],$_POST['Candidates']['email']);
			
	     	Yii::app()->user->setFlash('success', '<strong>Success!</strong> Ob worker created successfully.');
		    		$this->redirect(array('onBoarding','id'=>$offerModel->id));
	     }*/

	    //$this->render('on_boarding',array('offerModel'=>$offerModel,'candidateModel'=>$candidateModel,'vendorModel'=>$vendorModel,'workOrderModel'=>$workOrderModel));
	}
	
	
	public function emailOnboard($password,$officialemail,$personalemail){
  
   //echo $password;echo $email;exit;
		$baseUrl = Yii::app()->getBaseUrl(true);
		$link = CHtml::link($baseUrl.'/candidate', $baseUrl.'/candidate');
  	$workOrderID = $_GET['id'];
	$workOrderModel =  Workorder::model()->findByPk($workOrderID);
	$offer = Offer::model()->findByPk($workOrderModel->offer_id);
   $vendorJobSubmission = VendorJobSubmission::model()->findByPk($offer->submission_id);
   $jobmodel = Job::model()->findByPk($vendorJobSubmission->job_id); 
   $candidate = Candidates::model()->findByPk($vendorJobSubmission->candidate_Id); 
   
   $candidateID= $candidate->id;
   $homeUrl = str_replace("http://","",Yii::app()->getBaseUrl()).'/../';
   
  $htmlbody = '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px; "Proxima Nova","Helvetica Neue",Arial,sans-serif;padding:2% 7%">
            <div>

            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$candidate->first_name.' '.$candidate->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">Please find below link for the Candidate portal <br/>
                    1. Monthly Time Sheet<br/>
     				2. Pay Slip<br/>
     				3. Update Your Profile </p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4">Please login using following link, </span><a href="'.$baseUrl.'/candidate" style="color:#069;font-size:13px;line-height:1.4;text-decoration:none" target="_blank">'.$baseUrl.'/candidate</a>
                    </p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                    <span style="color:#333;font-size:13px;line-height:1.4">Your Account Information</span><br>
                    UserName: '.$candidate->candidate_ID.'<br />
      				Password: '.$password.'<br />
                    </p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
   Regard <br> Webmaster - SimplifyVMS
   </p>
  </div>
                </div>
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';

   
   $message = Yii::app()->sendgrid->createEmail(); 
    //shortcut to $message=new YiiSendGridMail($viewsPath); 
   $message->setHtml($htmlbody) 
   ->setSubject('Your Account Information')
   ->addTo($officialemail)
   ->addTo($personalemail) 
   ->setFrom('account@simplifyvms.net');
     
   Yii::app()->sendgrid->send($message);
  }

	public function actionOffBoarding(){

        $offerID = $_GET['id'];
	    $offerModel =  Offer::model()->findByPk($offerID);

	    $this->render('off_boarding',array('offerModel'=>$offerModel));
	}

	public function actionaddProject(){
		
		$workOrderID = $_GET['id'];
		$workOrderModel = Workorder::model()->findByPk($workOrderID);
        $offerID = $workOrderModel->offer_id;
	    $offerModel =  Offer::model()->findByPk($offerID);
		
		//workOrder Projects list
		$projectIDs = array();
		if($workOrderModel->wo_project){
			$projectIDs = explode(',',$workOrderModel->wo_project);
			}
		$projects = Project::model()->findAllByAttributes(array('id'=>$projectIDs));
		
		//workOrder Cost Center Cosdes list
		$costCenterIDs = array();
		if($workOrderModel->wo_cost_center){
			$costCenterIDs = explode(',',$workOrderModel->wo_cost_center);
			}
		$costCenters = CostCenter::model()->findAllByAttributes(array('id'=>$costCenterIDs));
		
		//workOrder Time Sheet Codes list
		$timeSheetIDs = array();
		if($workOrderModel->wo_timesheet_code){
			$timeSheetIDs = explode(',',$workOrderModel->wo_timesheet_code);
			}
		$timeSheetCodes = TimeSheetCode::model()->findAllByAttributes(array('id'=>$timeSheetIDs));
		
	    $this->render('addProject',array('offerModel'=>$offerModel,'projects'=>$projects,'costCenters'=>$costCenters,'timeSheetCodes'=>$timeSheetCodes,'workOrderModel'=>$workOrderModel));
		}
		
	public function actionDeleteProject(){
		$model = CpProjects::model()->findByPk($_GET['pro_id']);
		if($model->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
			  <strong>Success!</strong> Deleted successfully.</div>'); 
			}
		$this->redirect(array('addProject','id'=>$_GET['id']));
		} 

	public function actionOfferHelloSignResponse(){
		$this->layout = 'simple';
		 if(isset($_GET['signature_id'])) { 
			  $signature_id = $_GET['signature_id'];
			  require_once Yii::getPathOfAlias('webroot').'/pdfExt/hellosign/vendor/autoload.php'; 
			  $client = new HelloSign\Client('f8d49c7ffaed7522cee96aef781b71b577cc9e345449de2e9453b79b07428649');
			  $offerModel = Offer::model()->findByAttributes(array('hellosign_signature_id'=>$signature_id));
			   $offerModel->hellosign_api_sign_status = 1;
			   $offerModel->doc_signed_date = date("Y-m-d H:i:s");
			   $offerModel->save(false);
			   //print_r($offerModel);exit;
			   sleep(10);
			   $client->getFiles($offerModel->hellosign_api_req_id, Yii::app()->basePath.'/../signedDocs/'.$offerModel->offer_digital_doc, HelloSign\SignatureRequest::FILE_TYPE_PDF);
			   $this->render('hellosign_message');
		  }
 	}

	public function actionOnBoardHelloSignResponse(){
		$this->layout = 'simple';
		
		if(isset($_GET['signature_id'])) { 
		  $signature_id = $_GET['signature_id'];
		  require_once Yii::getPathOfAlias('webroot').'/pdfExt/hellosign/vendor/autoload.php'; 
		  $client = new HelloSign\Client('f8d49c7ffaed7522cee96aef781b71b577cc9e345449de2e9453b79b07428649');
		   $query = "select * from vms_contract where hellosign_signer1_id = '".$signature_id."' OR hellosign_signer2_id = '".$signature_id."'";
 		   $contractModel = (object) Yii::app()->db->createCommand( $query )->query()->read();
		   $saveModel = Contract :: model()->findByPk($contractModel->id);
		   
 			if($contractModel->hellosign_signer1_id == $signature_id){
 				$saveModel->hellosign_signer1_status = 1;
				$saveModel->hellosign_signer1_date = date("Y-m-d H:i:s");
				
			}
			if($contractModel->hellosign_signer2_id == $signature_id){
 				$saveModel->hellosign_signer2_status = 1;
				$saveModel->hellosign_signer2_date = date("Y-m-d H:i:s");
			} 
			
			$signature_request = $client->getSignatureRequest($contractModel->hellosign_api_req_id);
				   if($signature_request->isComplete() == 1){
					   $saveModel->hellosign_api_sign_status = 1;
					   $saveModel->doc_signed_date = date("Y-m-d H:i:s");
 				   }
			
			$saveModel->save(false);
			sleep(10);
			$client->getFiles($contractModel->hellosign_api_req_id, Yii::app()->basePath.'/../signedDocs/'.$contractModel->offer_digital_doc, HelloSign\SignatureRequest::FILE_TYPE_PDF);
			$this->render('hellosign_message');
			
	  }
	}
}