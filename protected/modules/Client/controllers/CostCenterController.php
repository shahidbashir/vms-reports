<?php

class CostCenterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('terminationSearch','interviewSearch','offerSearch','boardingSearch','workorderSearch','timesheetSearch','contractSearch','skills','json1','json','create','update','delete','testToken','jobSearch','submissionSearch','interviewNotification','subNotification'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionTerminationSearch(){

		if(isset($_POST['subValue'])){
			$contractValue = $_POST['subValue'];
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);

			$sql ="select  v.first_name as vendorfirstname , v.last_name as vendorlastname ,v.organization ,c.id as contractID ,c.ext_status,c.termination_status,c.termination_date ,w.id as workorderId,can.first_name,can.last_name,j.title,j.id as jobId ,o.approver_manager_location as location ,c.start_date as startDate, l.name, o.id as offerid from vms_contract c, vms_job j, vms_candidates can,vms_workorder w,vms_offer o,vms_location l,vms_vendor v where c.candidate_id=can.id and c.job_id=j.id and c.offer_id=o.id and c.workorder_id=w.id 
			and c.vendor_id=v.id and  o.approver_manager_location= l.id and c.termination_status IN('1','2','3') and (can.first_name like '%".$contractValue."%' OR can.last_name like '%".$contractValue."%' OR v.last_name like '%".$contractValue."%' OR v.first_name like '%".$contractValue."%' OR v.organization like '%".$contractValue."%' OR j.title like '%".$contractValue."%' OR j.id = '".$contractValue."' OR c.id = '".$contractValue."' OR c.termination_date like '%".date('Y-m-d',strtotime($contractValue))."%' OR l.name like '%".$contractValue."%') and c.client_id ='".$loginUserId."' ORDER BY c.id DESC";

			$count_query = "select count(*) FROM vms_contract c, vms_job j, vms_candidates can, vms_workorder w, vms_offer o, vms_location l,vms_vendor v where c.vendor_id=v.id and c.candidate_id=can.id and c.job_id=j.id and c.offer_id=o.id and c.workorder_id=w.id 
			and o.approver_manager_location= l.id and c.termination_status IN('1','2','3') and (can.first_name like '%".$contractValue."%' OR can.last_name like '%".$contractValue."%' OR j.title like '%".$contractValue."%' OR v.last_name like '%".$contractValue."%' OR v.first_name like '%".$contractValue."%' OR v.organization like '%".$contractValue."%' OR j.id = '".$contractValue."' OR c.id = '".$contractValue."' OR c.termination_date like '%".date('Y-m-d',strtotime($contractValue))."%' OR l.name like '%".$contractValue."%' ) and c.client_id =".$loginUserId;

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

			$dataProvider=new CSqlDataProvider($sql, array(
				'keyField'=>'id',
				'totalItemCount'=>$item_count,
				'pagination'=>array(
					'pageSize'=>25,
				),
			));

			$models = $dataProvider->getData();
			if($models){
				$i = 1;
				foreach($models as $model){
					$workorderStatus = UtilityManager::terminationStatus();
					if($model['termination_status'] == 1){
						$className ="label-re-open";
					}else if($model['termination_status'] == 2){
						$className ="label-pending-aproval";
					}elseif($model['termination_status'] == 3){
						$className ="label-rejected";
					}else{
						$className ="label-new-request";
					}
					?>
					<tr>
						<td><span class="tag <?php echo $className;?>"><?php echo  $workorderStatus[$model['termination_status']];?></span></td>
						<td><?php echo $i; ?></td>
						<td><?php echo $model['contractID']; ?></td>
						<td><a href=""><?php echo $model['first_name'].' '.$model['last_name']; ?></a></td>
						<td> <?php echo $model['title'].' ('.$model['jobId'].')'; ?> </td>
						<td><?php echo $model['termination_date']; ?></td>
						<td><?php echo $model['name']; ?></td>
						<td><a href=""><?php echo $model['organization']. ' ( '.$model['vendorfirstname'].' '.$model['vendorlastname'].' )'; ?></a></td>
						<td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/terminationView',array('id'=>$model['contractID'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
					</tr>
					<?php $i++; } }else{ ?>
				<tr>
					<td colspan="6"> Sorry No Record Found.. </td>
				</tr>
			<?php }
		}
	}

	public function actionInterviewSearch(){
		if(isset($_POST['subValue'])) {
		$interviewsearch = $_POST['subValue'];
			$interviewstatus =  $_POST['status'];
			$status ='';
			if($interviewstatus == 'today'){
				$date = date('Y-m-d');
				$status = "and w.status = 2 and ((w.interview_start_date ='".$date."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$date."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$date."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$date."' and w.alt3_date_status =1))";
			}elseif ($interviewstatus == 'upcomming'){
				$tomorrow = date("Y-m-d", strtotime("+1 day"));
				$status = "and w.status = 2 and ((w.interview_start_date ='".$tomorrow."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$tomorrow."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$tomorrow."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$tomorrow."' and w.alt3_date_status =1))";
			}elseif ($interviewstatus == 'waiting'){
				$status = 'and w.status = 1';
			}elseif ($interviewstatus == 'cencelled'){
				$status = 'and w.status = 3';
			}elseif ($interviewstatus == 'Completed') {
				$status = 'and w.status = 5';
			}
			$mainclient = UtilityManager::superClient(Yii::app()->user->id);

			//condition for hiring manager
			$loginUserId = Yii::app()->user->id;
			$modelClient = Client::model()->findByPk($loginUserId);
			$hiringManagerCond = '';
			$allJobIDs = array();
			if($modelClient->member_type == 'Hiring Manager'){
				$mainCientJobs = Job::model()->findAllByAttributes(array('user_subclient_id'=>$loginUserId));
				foreach($mainCientJobs as $mainCientJobsKey=>$mainCientJobsValue){
					$allJobIDs[$mainCientJobsValue->id] = $mainCientJobsValue->id;
				}
				$hiringManagerCond = " and w.job_id IN ('".implode("','" , $allJobIDs)."')";
			}
			$condition = 'and (can.first_name like "%'.$interviewsearch.'%" OR can.last_name like "%'.$interviewsearch.'%" OR j.title like "%'.$interviewsearch.'%" OR j.id = "'.$interviewsearch.'" OR w.interview_type like "%'.$interviewsearch.'%" OR w.id = "'.$interviewsearch.'")';

			 $sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,can.first_name,can.last_name,j.title From vms_interview w ,vms_vendor_job_submission s,vms_candidates can,vms_job j 
			where w.submission_id=s.id and s.candidate_Id=can.id and w.job_id=j.id and s.client_id='".$mainclient."' AND s.id !=".Yii::app()->user->getState('submission_id')." $status $condition $hiringManagerCond order by w.id desc";

			$count_query = 'select count(*) FROM vms_interview w ,vms_vendor_job_submission s,vms_candidates can,vms_job j 
			where w.submission_id=s.id and s.candidate_Id=can.id and w.job_id=j.id and s.client_id='.$mainclient.' AND s.id !='.Yii::app()->user->getState('submission_id').' '.$status.' '.$condition.' '.$hiringManagerCond;

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

			$interview=new CSqlDataProvider($sql, array(
				'keyField'=>'id',
				'totalItemCount'=>$item_count,
				'pagination'=>array(
					'pageSize'=>100,
				),
			));
			$model = $interview->getData();
			if($model){
				foreach($model as $allData){

					$location = Location::model()->findByPk($allData['interview_where']);
					$postingstatus = UtilityManager::interviewStatus();
					$candidateData = Candidates::model()->findByPk($allData['candidate_Id']);

					switch ($postingstatus[$allData['status']]) {
						case "Approved":
							$color = 'label-open';
							break;
						case "Waiting for Approval":
							$color = 'label-interview-pending';
							break;
						case "Cancelled":
							$color = 'label-interview-cancelled';
							break;
						case "Reschedule":
							$color = 'label-interview-reschedule';
							break;
						case "Interview Completed":
							$color = 'label-interview-completed';
							break;
						default:
							$color = 'label label-primary';
					}


					if($allData['start_date_status']){
						$date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
					}

					else if($allData['alt1_date_status']){
						$date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
					}
					else if($allData['alt2_date_status']){
						$date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
					}
					else if($allData['alt3_date_status']){
						$date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
					}else{
						$date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
					}

					$newTime = explode(' ',$date);

					$jobData = Job::model()->findByPk($allData['job_id']);
					?>
					<tr>
						<td><span class="tag <?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span></td>
						<td><?php echo $allData['interview_type'] ?></td>
						<td><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>"><?php echo $allData['id'] ?></a></td>
						<td><a href=""><?php echo $allData['first_name'].' '.$allData['last_name']; ?></a></td>
						<td><a href="">
								<?php echo $jobData->title.'('. $jobData->id.')'; ?>
							</a></td>
						<td><?php echo date('m-d-Y',strtotime($newTime[0])); ?></td>
						<td><?php echo $newTime[1] ?></td>
						<td><?php echo $newTime[3]; ?></td>
						<!--<td ><?php /*echo $location->name; */?></td>-->
						<td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
					</tr>
				<?php }
			}else{ ?>
				<tr>
					<td colspan="10">Sorry No Record Found</td>
				</tr>
			<?php }
		}

	}
	public function actionOfferSearch(){

		if(isset($_POST['subValue'])) {
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
			$offersearch = $_POST['subValue'];
			$status ='';
			if($_POST['status']=='Pending'){
				$status = 'and o.status=4';
			}
			if($_POST['status']=='Approved'){
				$status = 'and o.status=1';
			}
			$condition = "and (can.first_name like '%".$offersearch."%' OR can.last_name like '%".$offersearch."%' OR o.id = '".$offersearch."'  OR j.id = '".$offersearch."' OR l.name like '%".$offersearch."%' OR o.offer_pay_rate like '%".$offersearch."%' OR o.offer_bill_rate like '%".$offersearch."%' OR o.job_start_date = '".date('Y-m-d',strtotime($offersearch))."')";
			$sql = "select o.status as offer_status,o.submission_id,o.id as offer_id,o.offer_bill_rate,o.offer_pay_rate,o.job_start_date,j.id as Jobid,can.first_name,can.last_name,o.approver_manager_location,l.name
			  from vms_offer o,vms_location l,vms_job j,vms_candidates can where o.candidate_id = can.id and o.approver_manager_location = l.id $condition and j.id=o.job_id $status and o.client_id=" . $loginUserId . " AND o.submission_id !=" . Yii::app()->user->getState('submission_id') . " group by o.id order by o.id desc";

			$count_query = "select count(*) FROM vms_offer o,vms_location l,vms_job j,vms_candidates can where o.candidate_id = can.id and o.approver_manager_location = l.id $condition  and j.id=o.job_id $status and o.client_id=" . $loginUserId . " AND o.submission_id !=".Yii::app()->user->getState('submission_id');

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

			$dataProvider = new CSqlDataProvider($sql, array(
				'keyField' => 'id',
				'totalItemCount' => $item_count,
				'pagination' => array(
					'pageSize' => 100,
				),
			));
			$offers = $dataProvider->getData();

			 if($offers){ foreach($offers as $value) {
				$offerStatus = UtilityManager::offerStatus();
				if($value['offer_status'] == 1){
					$className ="label-open";
				}else if($value['offer_status'] == 2)
				{
					$className ="label-rejected";
				}elseif($value['offer_status'] == 3){
					$className ="info";
				}else if($value['offer_status'] == 4){
					$className ="label-hold";
				}else {
					$className ="label-re-open";
				}
				?>
				<tr>
					<td><span class="tag <?php echo $className;?>"><?php echo  $offerStatus[$value['offer_status']];?></span></td>
					<td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('id'=>$value['offer_id'])); ?>"><?php echo $value['offer_id'];?></a></td>
					<td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$value['Jobid'],array('type'=>'info')); ?>"><?php echo $value['Jobid'];?></a></td>
					<td><?php echo $value['first_name'].' '.$value['last_name'];?></td>
					<td><?php echo date('F d,Y',strtotime($value['job_start_date']));?></td>
					<td><?php echo '$'.$value['offer_bill_rate'];?></td>
					<td><?php echo '$'.$value['offer_pay_rate'];?></td>
					<td><?php echo $value['name']; ?></td>
					<td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('id'=>$value['offer_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
				</tr>
			<?php } }else{ ?>
				<tr>
					<td colspan="9">There is no offer created</td>
				</tr>
			<?php }
		}
	}
	public function actionBoardingSearch(){

		if(isset($_POST['subValue'])) {
			$boardingsearch = $_POST['subValue'];
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
			if($_POST['action']=='pendingboarding'){
				$date = date('Y-m-d');
				$datecondition = "and w.wo_start_date<'".$date."'";

			}else{
				$date = date('Y-m-d');
				$datecondition = "and w.wo_start_date = '".$date."'";
			}


			//removing this query for now b/c what rohit said now is that if a candidate is onboarded then he
			//not be listed in joiners list so thats why removing the dates conditions b/c these apply after the
			//candidate onboarded.

			/*$sql ='select * From vms_workorder where IF(onboard_changed_start_date!="0000-00-00",if(onboard_changed_start_date<>"'.$date.'",true,false),if(wo_start_date<"'.$date.'",true,false)) and client_id ='.$loginUserId;
            $count_query = 'select count(*) FROM vms_workorder where IF(onboard_changed_start_date!="0000-00-00",if(onboard_changed_start_date<>"'.$date.'",true,false),if(wo_start_date<"'.$date.'",true,false))and client_id ='.$loginUserId;*/


			$sql = "select w.* , can.first_name , can.last_name , j.id as jobId, j.title,l.name From vms_workorder w , vms_location as l , vms_candidates as can ,vms_job as j ,vms_offer as o where w.candidate_id = can.id and w.job_id = j.id and w.offer_id = o.id and o.approver_manager_location = l.id 
			and (can.first_name like '%".$boardingsearch."%' OR can.last_name like '%".$boardingsearch."%' OR w.id = '".$boardingsearch."'  OR j.id = '".$boardingsearch."' OR l.name like '%".$boardingsearch."%' OR j.title like '%".$boardingsearch."%') and w.workorder_status IN(0,1) and w.on_board_status=0 $datecondition and w.client_id ='".$loginUserId."' group by w.id order by w.id desc";

			$count_query = "select count(*) From vms_workorder w , vms_location as l , vms_candidates as can ,vms_job as j ,vms_offer as o where w.candidate_id = can.id and w.job_id=j.id and w.offer_id = o.id and o.approver_manager_location = l.id and (can.first_name like '%".$boardingsearch."%' OR can.last_name like '%".$boardingsearch."%' OR w.id ='".$boardingsearch."'  OR j.id ='".$boardingsearch."' OR l.name like '%".$boardingsearch."%' OR j.title like '%".$boardingsearch."%') and w.workorder_status IN(0,1) and w.on_board_status=0 $datecondition and w.client_id =".$loginUserId;

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
			$dataProvider = new CSqlDataProvider($sql, array(
				'keyField' => 'id',
				'totalItemCount' => $item_count,
				'pagination' => array(
					'pageSize' => 100,
				),
			));
			$models = $dataProvider->getData();
			$i = 0;
			if($models){
				foreach($models as $model){

				$i++; ?>
				<tr>

					<td>
						<?php echo $i; ?>
					</td>

					<td>
						<a href="<?php echo $this->createAbsoluteUrl('offer/workorderView',array('id'=>$model['id'])); ?>"><?php echo $model['first_name'].' '.$model['last_name']; ?></a>
					</td>
					<td>
						<?php echo $model['name']; ?>
					</td>
					<td>
						<a href=""><?php echo $model['title'].'('.$model['jobId'].')'; ?></a>
					</td>
					<td>
						<?php echo $model['id']; ?>
					</td>

					<td style="text-align: center">
						<a href="<?php echo $this->createAbsoluteUrl('offer/workorderView',array('id'=>$model['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
					</td>
				</tr>
				<?php
				}
			}else
			{ ?>
				<tr><td colspan="7">Sorry No Record Found</td></tr>
			<?php }
		}
	}
	public function actionWorkorderSearch(){

		if(isset($_POST['subValue'])){
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
			$workordersearch = $_POST['subValue'];
			$sql ="select l.name as location ,sub.id,workorder.wo_start_date,wo_bill_rate,offer_id,workorder.id as workorder_id,
	
	   can.first_name,can.last_name,can.id as c_id,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,workorder.workorder_status,j.id as jobId
	
	   FROM vms_vendor_job_submission sub,vms_candidates as can,vms_workorder as workorder,vms_vendor as vendor,vms_job as j ,vms_location as l , vms_offer as o
	
	   WHERE j.id=workorder.job_id and workorder.offer_id = o.id and o.approver_manager_location = l.id and workorder.candidate_id =can.id and workorder.vendor_id=vendor.id and workorder.client_id=".$loginUserId." and sub.id = workorder.submission_id  AND sub.id !=".Yii::app()->user->getState('submission_id')." and workorder.wo_form_submitted=1
	    and (can.first_name like '%".$workordersearch."%' OR can.last_name like '%".$workordersearch."%' OR l.name like '%".$workordersearch."%' OR vendor.organization like '%".$workordersearch."%' OR workorder.id = '".$workordersearch."' OR workorder.wo_bill_rate = '".$workordersearch."' OR j.id = '".$workordersearch."' OR l.name= '".$workordersearch."' OR workorder.wo_start_date = '".date('Y-m-d',strtotime($workordersearch))."') group by workorder.id order by workorder.id desc";

			$count_query = "select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can,vms_workorder as workorder,vms_vendor as vendor,vms_job as j ,vms_location as l , vms_offer as o
	   WHERE j.id=workorder.job_id and workorder.offer_id = o.id and o.approver_manager_location = l.id and workorder.candidate_id =can.id and workorder.vendor_id=vendor.id and workorder.client_id=".$loginUserId." and sub.id = workorder.submission_id  AND sub.id !=".Yii::app()->user->getState('submission_id')." and workorder.wo_form_submitted=1
	    and (can.first_name like '%".$workordersearch."%' OR can.last_name like '%".$workordersearch."%' OR l.name like '%".$workordersearch."%' OR vendor.organization like '%".$workordersearch."%' OR workorder.id = '".$workordersearch."' OR workorder.wo_bill_rate = '".$workordersearch."' OR j.id = '".$workordersearch."' OR l.name= '".$workordersearch."' OR workorder.wo_start_date = '".date('Y-m-d',strtotime($workordersearch))."')";

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

			$dataProvider=new CSqlDataProvider($sql, array(
				'keyField'=>'id',
				'totalItemCount'=>$item_count,
				'pagination'=>array(
					'pageSize'=>100,
				),
			));
			$workorders = $dataProvider->getData();
			 if($workorders){
				$i = 0;
				foreach($workorders as $value) {
					//removing this b/c rohit said
					/*if(date('m-d-Y',strtotime($value['onboard_changed_start_date']))=='01-01-1970'){
                        $onboardStartDate = date('F d,Y',strtotime($value['wo_start_date']));
                    }else{
                        $onboardStartDate = date('F d,Y',strtotime($value['onboard_changed_start_date']));
                    }*/
					$onboardStartDate = date('F d,Y',strtotime($value['wo_start_date']));
					$i++;
					$offerStatus = UtilityManager::workorderStatus();

					if($value['workorder_status'] == 1){
						$className ="label-hold";
					}else if($value['workorder_status'] == 2){
						$className ="label-pending-aproval";
					}elseif($value['workorder_status'] == 0){
						$className ="label-new-request";
					}else{
						$className ="info";
					}
					?>
					<tr>
						<td><span class="tag <?php echo $className;?>"><?php echo  $offerStatus[$value['workorder_status']];?></span></td>
						<td><a href=""><?php echo $value['workorder_id'];?></a></td>
						<td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$value['jobId'],array('type'=>'info')); ?>"><?php echo $value['jobId'];?></a></td>
						<td><a href=""><?php echo $value['first_name'].' '.$value['last_name'];?></a></td>
						<td><?php echo $value['v_organization']; ?></td>
						<td><?php echo $onboardStartDate; ?></td>
						<td><?php echo '$ '.$value['wo_bill_rate']; ?></td>
						<td><?php echo $value['location']; ?></td>
						<td style="text-align: center" class="actions"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/workorderView',array('id'=>$value['workorder_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
							<!--<a href="#" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg"></i></a>--></td>
					</tr>
				<?php } }else{ ?>
				<tr>
					<td colspan="7">Sorry no record found...</td>
				</tr>
			<?php }
		}


	}
	public function actionTimesheetSearch(){

		if(isset($_POST['subValue'])){
			$status = '';
				if($_POST['action']=='rejectedTimeSheets'){
					$status = 'and t.timesheet_status=2';
				}
			if($_POST['action']=='approvedTimeSheets'){
					$status = ' and t.timesheet_status=1';
				}
			if($_POST['action']=='pendingTimeSheets'){
				$status = 'and t.timesheet_status=0';
			}

			$timesheetValue = $_POST['subValue'];
			$userID = Yii::app()->user->id;
			$client = Client::model()->findByPk($userID);
			//adding this condition for now to show timesheet list to the main client.
			// .' or '.$client->super_client_id.'=0
			if($client->super_client_id==0 || $client->member_type=='Administrator') {
				$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
				  $sql ="select  t.* ,can.first_name,can.last_name,l.name,v.organization,o.approver_manager_location,w.id as workorderId from cp_timesheet t,vms_location l,
				 vms_candidates can,vms_vendor v,vms_offer o,vms_workorder w where t.workorder_id=w.id and t.timesheet_sub_status!=0 and w.client_id=$loginUserId $status
				 and v.id =w.vendor_id and t.candidate_id=can.id and t.offer_id=o.id and l.id=o.approver_manager_location and (can.first_name like '%".$timesheetValue."%' OR can.last_name like '%".$timesheetValue."%' OR l.name like '%".$timesheetValue."%' OR v.organization like '%".$timesheetValue."%' OR t.id = '".$timesheetValue."' OR t.total_hours = '".$timesheetValue."' OR t.total_payrate = '".$timesheetValue."' OR t.total_billrate = '".$timesheetValue."' OR t.invoice_start_date = '".date('Y-m-d',strtotime($timesheetValue))."' OR t.invoice_end_date = '".date('Y-m-d',strtotime($timesheetValue))."')  Group By t.id ORDER BY t.id DESC";

				$count_query = "select count(*) FROM cp_timesheet t,vms_location l,
				 vms_candidates can,vms_vendor v,vms_offer o,vms_workorder w where t.workorder_id=w.id and t.timesheet_sub_status!=0 and w.client_id='.$loginUserId.' $status 
				 and t.candidate_id=can.id and t.offer_id=o.id and l.id=o.approver_manager_location and(can.first_name like '%".$timesheetValue."%' OR can.last_name like '%".$timesheetValue."%' OR l.name like '%".$timesheetValue."%' OR v.organization like '%".$timesheetValue."%' OR t.id = '".$timesheetValue."' OR t.total_hours = '".$timesheetValue."' OR t.total_payrate = '".$timesheetValue."' OR t.total_billrate = '".$timesheetValue."' OR t.invoice_start_date = '".date('Y-m-d',strtotime($timesheetValue))."' OR t.invoice_end_date = '".date('Y-m-d',strtotime($timesheetValue))."')";

				$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

				$dataProvider=new CSqlDataProvider($sql, array(
					'keyField'=>'id',
					'totalItemCount'=>$item_count,
					'pagination'=>array(
						'pageSize'=>25,
					),
				));

				$models = $dataProvider->getData();

			}else{

				$sql ="select  t.* ,can.first_name,can.last_name,l.name,v.organization,o.approver_manager_location,w.id as workorderId from cp_timesheet t,vms_location l,
				 vms_candidates can,vms_vendor v,vms_offer o,vms_workorder w where t.workorder_id=w.id and t.timesheet_sub_status!=0 and w.approval_manager = $userID or $client->super_client_id =0 $status
				 and t.candidate_id=can.id and t.offer_id=o.id and l.id=o.approver_manager_location and (can.first_name like '%".$timesheetValue."%' OR can.last_name like '%".$timesheetValue."%' OR l.name like '%".$timesheetValue."%' OR v.organization like '%".$timesheetValue."%' OR t.id = '".$timesheetValue."' OR t.total_hours = '".$timesheetValue."' OR t.total_payrate = '".$timesheetValue."' OR t.total_billrate = '".$timesheetValue."' OR t.invoice_start_date = '".date('Y-m-d',strtotime($timesheetValue))."' OR t.invoice_end_date = '".date('Y-m-d',strtotime($timesheetValue))."')  Group By t.id ORDER BY t.id DESC";

				$count_query = "select count(*) FROM cp_timesheet t,vms_location l,
				 vms_candidates can,vms_vendor v,vms_offer o,vms_workorder w where t.workorder_id=w.id and t.timesheet_sub_status!=0 and w.approval_manager = $userID or $client->super_client_id =0 $status
				 and t.candidate_id=can.id and t.offer_id=o.id and l.id=o.approver_manager_location and(can.first_name like '%".$timesheetValue."%' OR can.last_name like '%".$timesheetValue."%' OR l.name like '%".$timesheetValue."%' OR v.organization like '%".$timesheetValue."%' OR t.id = '".$timesheetValue."' OR t.total_hours = '".$timesheetValue."' OR t.total_payrate = '".$timesheetValue."' OR t.total_billrate = '".$timesheetValue."' OR t.invoice_start_date = '".date('Y-m-d',strtotime($timesheetValue))."' OR t.invoice_end_date = '".date('Y-m-d',strtotime($timesheetValue))."')";

				$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

				$dataProvider=new CSqlDataProvider($sql, array(
					'keyField'=>'id',
					'totalItemCount'=>$item_count,
					'pagination'=>array(
						'pageSize'=>25,
					),
				));

				$models = $dataProvider->getData();
			}
			 if($models){
				foreach($models as $value){
					$postingstatus = UtilityManager::timeSheetStatus();
					switch ($postingstatus[$value['timesheet_status']]) {
						case "Approved":
							$color = 'tag label-new-request';
							break;
						case "Pending":
							$color = 'tag label-hold';
							break;
						case "Rejected":
							$color = 'tag label-pending-aproval';
							break;
						default:
							$color = 'tag label-re-open';
					}
					?>
					<tr>
						<td><span class="<?php echo $color; ?>"><?php echo $postingstatus[$value['timesheet_status']]; ?></span></td>
						<td><a href=""><?php echo $value['id']; ?></a></td>
						<td><a href="">
								<?php  echo $value['first_name'].' '.$value['last_name']; ?>
							</a></td>
						<td><a href="">
								<?php echo $value['organization']; ?>
							</a></td>
						<td><?php echo $value['total_hours']; ?> <i class="fa fa-question"
																	data-toggle="tooltip" data-placement="right" title="Regular Hr: <?php echo $value['total_regular_hours']; ?>, Over Time: <?php echo $value['total_overtime_hours']; ?>, Double Time: <?php echo $value['total_doubletime_hours']; ?>"
							></i></td>
						<td>$<?php echo $value['total_payrate']; ?></td>
						<td>$<?php echo $value['total_billrate']; ?></td>
						<td><?php echo date("m-d-Y", strtotime($value['invoice_start_date'])).' To '.date("m-d-Y", strtotime($value['invoice_end_date'])); ?></td>
						<td><?php echo $value['name']?></td>
						<td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('offer/timesheetDetail',array('id'=>$value['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
					</tr>
				<?php }
	}

		}


	}
	public function actionContractSearch()
	{

		if(isset($_POST['subValue'])){
			$contractValue = $_POST['subValue'];
			//$condition = ' ';
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);

			$sql ="select  c.id as contractID ,c.ext_status,c.termination_status,c.termination_date ,w.id as workorderId,can.first_name,can.last_name,j.title,j.id as jobId ,o.approver_manager_location as location ,c.start_date as startDate, l.name, o.id as offerid from vms_contract c, vms_job j, vms_candidates can,vms_workorder w,vms_offer o,vms_location l where c.candidate_id=can.id and c.job_id=j.id and c.offer_id=o.id and c.workorder_id=w.id 
			and o.approver_manager_location= l.id and (can.first_name like '%".$contractValue."%' OR can.last_name like '%".$contractValue."%' OR j.title like '%".$contractValue."%' OR j.id = '".$contractValue."' OR w.id = '".$contractValue."' OR c.start_date like '%".date('Y-m-d',strtotime($contractValue))."%' OR l.name like '%".$contractValue."%') and c.client_id ='".$loginUserId."' ORDER BY c.id DESC";

			$count_query = "select count(*) FROM vms_contract c, vms_job j, vms_candidates can, vms_workorder w, vms_offer o, vms_location l where c.candidate_id=can.id and c.job_id=j.id and c.offer_id=o.id and c.workorder_id=w.id 
			and o.approver_manager_location= l.id and (can.first_name like '%".$contractValue."%' OR can.last_name like '%".$contractValue."%' OR j.title like '%".$contractValue."%' OR j.id = '".$contractValue."' OR w.id = '".$contractValue."' OR c.start_date like '%".date('Y-m-d',strtotime($contractValue))."%' OR l.name like '%".$contractValue."%' ) and c.client_id =".$loginUserId;

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

			$dataProvider=new CSqlDataProvider($sql, array(
				'keyField'=>'id',
				'totalItemCount'=>$item_count,
				'pagination'=>array(
					'pageSize'=>25,
				),
			));

			$models = $dataProvider->getData();
		}
		if($models){
			foreach($models as $model){
				//print_r($model);
				$workorder = Workorder::model()->findByPk($model['workorderId']);
				$jobs = Job::model()->findByPk($model['jobId']);
				$offer = Offer::model()->findByPk($model['offerid']);
				$location = Location::model()->findByPk($model['location']);
				$workorderStatus = UtilityManager::workorderStatus();
				if($workorder->workorder_status == 1){
					$className ="label-re-open";
				}else if($workorder->workorder_status == 2){
					$className ="label-pending-aproval";
				}elseif($workorder->workorder_status == 0){
					$className ="label-rejected";
				}elseif($workorder->workorder_status == 3){
					$className ="label-re-open";
				}else{
					$className ="label-new-request";
				}
				?>
				<tr>
					<td>
						<?php if($model['termination_status'] == 2 && strtotime($model['termination_date']) <= time()){ ?>
							<span class="tag label-rejected">Closed</span>
						<?php }elseif($model['ext_status'] == 2 ){?>
							<span class="tag label-re-open">Open</span>
							<span class="tag label-rejected">Renewed</span>
						<?php }else {?>
							<span class="tag <?php echo $className;?>"><?php echo  $workorderStatus[$workorder->workorder_status];?></span>
						<?php }?>
					</td>

					<td><a href=""><?php echo $workorder->id; ?></a></td>
					<td><?php echo $model['first_name'].' '.$model['last_name']; ?></td>
					<td><?php echo date('F d,Y',strtotime($model['startDate'])); ?></td>
					<td><?php echo $location->name; ?></td>
					<td><a href=""><?php echo $jobs->title.' ('.$jobs->id.')'; ?></a></td>
					<td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('contract/contractView', array('id' => $model['contractID'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
				</tr>
			<?php } }else{ ?>
			<tr>
				<td colspan="6"> Sorry No Record Found.. </td>
			</tr>
		<?php }

	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSubNotification(){
		if(isset($_POST['submissionId'])){
			$id = $_POST['submissionId'];
			$model = VendorJobSubmission::model()->findByPk($id);
			$model->bell_notification = 1;
			if($model->save(false)){
				echo 'success';
			}
		}
	}
	public function actionInterviewNotification(){
		if(isset($_POST['interviewId'])){
			$id = $_POST['interviewId'];
			$model = Interview::model()->findByPk($id);
			$model->bill_notification = 1;
			if($model->save(false)){
				echo 'success';
			}
		}

	}
	 public function actionJobSearch(){
		 $loginUserId = Yii::app()->user->id;
		 if(isset($_POST['searchvalue'])){
			 $searchvalue = $_POST['searchvalue'];
			 $Criteria = new CDbCriteria();
			 $Criteria->select="*";
			 $Criteria->condition = "user_id=".$loginUserId." and jobstep2_complete != 0";
			 $Criteria->order = "id desc";
			 $Criteria->condition = "user_id=".$loginUserId." and (t.title like '%".$searchvalue."%' || t.id = '".$searchvalue."' || t.job_po_duration = '".$searchvalue."' || t.bill_rate = '".$searchvalue."%' || t.jobStatus = '".$searchvalue."' || t.num_openings = '".$searchvalue."' || t.jobStatus = '".$searchvalue."' || t.job_po_duration like '%".date('Y-m-d',strtotime($searchvalue))."%' || t.job_po_duration_endDate like '%".date('Y-m-d',strtotime($searchvalue))."%')  and jobstep2_complete != 0";
			 $count=Job::model()->count($Criteria);
			 $pages=new CPagination($count);

			 // results per page
			 $pages->pageSize=100;
			 $pages->applyLimit($Criteria);
			 $jobs=Job::model()->findAll($Criteria);
 if($jobs){

	foreach($jobs as $jobKey=>$jobValue){


		//calculating number of hired and availity of candidates which are on workorder state

		$countworkorders = Workorder::model()->countByAttributes(array('job_id'=>$jobValue->id,'workorder_status'=>1));
		$countHired = Workorder::model()->countByAttributes(array('job_id'=>$jobValue->id,'workorder_status'=>1,'on_board_status'=>1));

		//if($jobSecondDate[1]==date('m/d/Y')){
		$postingstatus = UtilityManager::jobStatus();

		switch ($postingstatus[$jobValue->jobStatus]) {
			case "Pending Approval":
				$color = 'label-pending-aproval';
				break;
			case "Open":
				$color = 'label-open';
				break;
			case "Filled":
				$color = 'label-filled';
				break;
			case "Rejected":
				$color = 'label-rejected';
				break;
			case "Re-open":
				$color = 'label-re-open';
				break;
			case "Hold":
				$color = 'label-hold';
				break;
			case "New Request":
				$color = 'label-new-request';
				break;
			case "Draft":
				$color = 'tag label-draft';
				break;

			default:
				$color = 'label-new-request';
		}


		?>
		<tr>

			<!--<td><input type="checkbox" value=""></td>-->
			<td><span class="tag <?php echo $color; ?>"><?php echo $postingstatus[$jobValue->jobStatus]; ?></span></td>
			<td class="user-avatar"><a class="underline" href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobValue->id,array('type'=>'info')); ?>"><?php echo $jobValue->id; ?></a></td>
			<td>

				<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobValue->id,array('type'=>'info')); ?>" class="underline"><?php
					$limit = 60;
					if (strlen($jobValue->title) > $limit)
						$jobValue->title = substr($jobValue->title, 0, strrpos(substr($jobValue->title, 0, $limit), ' ')) . '...';
					echo $jobValue->title;
					?></a>


				<?php $closingdate = date('Y-m-d', strtotime('-4 day', strtotime($jobValue->desired_start_date)));
				if($closingdate== date('Y-m-d')){
					?>
					<i class="icon svg-fire"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/fire@512px-red.svg"></i>
				<?php } ?>
			</td>
			<td><?php echo $jobValue->job_po_duration.' - '.$jobValue->job_po_duration_endDate; ?></td>
			<td><?php echo $jobValue->num_openings ?></td>
			<td><?php echo $countHired; ?></td>
			<td><?php echo $countworkorders - $countHired;?></td>



			<td class="actions">
				<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobValue->id,array('type'=>'info')); ?>" data-toggle="tooltip" data-placement="top" title="View" class="icon">
					<i class="icon svg-icon">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg">
					</i>
				</a>
				<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/deletejob/id/'.$jobValue->id); ?>" data-toggle="tooltip" data-placement="top" title="Delete" class="icon">
					<i class="icon svg-icon">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg">
					</i>
				</a>
			</td>

		</tr>
	<?php } }

		 }

	 }

	public function actionSubmissionSearch()
	{

		if(isset($_POST['subValue'])){
			$submissionValue = $_POST['subValue'];
			$condition = ' ';
			if(is_numeric($submissionValue)){
				$condition ="OR sub.candidate_pay_rate='".$submissionValue."'";
			}
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);

		 $sql ="select  can.* , sub.job_id,sub.recommended_pay_rate,sub.candidate_Id,sub.makrup as vendorMarkup ,j.title,sub.candidate_pay_rate,sub.estimate_start_date,sub.resume_status,sub.id as sub_id FROM vms_vendor_job_submission sub,vms_job as j,vms_candidates as can

			WHERE sub.candidate_Id =can.id and sub.job_id=j.id AND sub.id !=".Yii::app()->user->getState('submission_id')." and (can.current_location like '%".$submissionValue."%' OR can.first_name like '%".$submissionValue."%'  OR j.title like '%".$submissionValue."%' OR sub.id='".$submissionValue."' $condition OR sub.job_id='".$submissionValue."' OR sub.estimate_start_date like '%".date('Y-m-d',strtotime($submissionValue))."%')  and (sub.resume_status = 3 OR resume_status = 4 OR resume_status = 5 OR resume_status = 7 OR resume_status = 8 OR resume_status = 9 OR resume_status = 6 ) and sub.client_id='".$loginUserId."' ORDER BY id DESC";

		$count_query = "select count(*) FROM vms_vendor_job_submission sub,vms_job as j ,vms_candidates as can WHERE sub.candidate_Id =can.id and sub.job_id=j.id and (can.current_location like '%".$submissionValue."%' OR can.first_name like '%".$submissionValue."%' OR j.title like '%".$submissionValue."%' $condition OR sub.id='".$submissionValue."' OR sub.job_id='".$submissionValue."' OR sub.estimate_start_date like '%".date('Y-m-d',strtotime($submissionValue))."%')  and (sub.resume_status = 3 OR resume_status = 4 OR resume_status = 5 OR resume_status = 7 OR resume_status = 8 OR resume_status = 9 OR resume_status = 6) and sub.client_id=".$loginUserId;

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>100,
			),
		));

		$model = $dataProvider->getData();
		}
		if($model){
			foreach($model as $value) {

				//$submissionModel = VendorJobSubmission::model()->findByAttributes(array('candidate_Id'=>$value->id,'client_id'=>Yii::app()->user->id,'resume_status'=>array('3','4','5','7')),array('order' =>'id DESC' , ));
				//$Candidates = Candidates::model()->findByPk($value['candidate_Id']);
				//$jobModel = Job::model()->findByPk($value['job_id']);
				$billrate = $value['candidate_pay_rate'] + $value['candidate_pay_rate'] * ($value['vendorMarkup'])/100;
				$status = UtilityManager::resumeStatus();
				switch ($status[$value['resume_status']]) {

					case "Submitted":
						$color = 'label-hold';
						break;

					case "MSP Review":
						$color = 'label-filled';
						break;

					case "MSP Shortlisted":
						$color = 'label-new-request';
						break;

					case "Client Review":
						$color = 'label-filled';
						break;

					case "Interview Process":
						$color = 'label-open';
						break;

					case "Rejected":
						$color = 'label-rejected';
						break;

					case "Offer":
						$color = 'label-pending-aproval';
						break;

					case "Approved":
						$color = 'label-new-request';
						break;

					case "Work Order Release":
						$color = 'label-re-open';
						break;

					default:
						$color = 'label-new-request';

				}
				?>
				<tr>
					<td><span class="tag <?php echo $color; ?>"><?php echo $status[$value['resume_status']]; ?></span></td>
					<td><a href=""><?php echo $value['sub_id'];?></a></td>
					<td><a href=""><?php echo $value['first_name'].' '.$value['last_name'];?></a></td>
					<td><a href=""><?php echo $value['title'].'('.$value['job_id'].')';?></a></td>
					<td><?php echo date('F d,Y',strtotime($value['estimate_start_date']));?></td>
					<td>$<?php echo $value['candidate_pay_rate'];?></td>
					<td>
						<?php echo $billrate;?>
					</td>
					<td><?php echo $value['current_location'];?></td>
					<td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$value['sub_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
				</tr>
			<?php } }else{ ?>
			<tr>
				<td colspan="9">Currently there is no submission for following job</td>
			</tr>
		<?php }
	}
	public function actionSkills(){
		
		header('content-type: text/json');

		$search = preg_quote(isset($_REQUEST['search']) ? $_REQUEST['search'] : '');
		$start = (isset($_REQUEST['start']) ? $_REQUEST['start'] : 1);
		
		// in this getting json data and showing in dropdown
		//$obj = json_decode(file_get_contents('http://billrate.simplifyvms.biz/names.json'), true);
		$obj = json_decode(file_get_contents(Yii::app()->basePath.'/names.json'), true);
		
		/*$jsonArray = array();
		$skills = Skills::model()->findAll();
		foreach($skills as $key=>$value){
			$jsonArray[$key] = array('text'=>$value->name,'value'=>$value->name[0].$value->name[1]);
			}
		$obj = $jsonArray;*/
		
		$ret = array();
		
		foreach($obj as $item)
		{
			if(preg_match('/' . ($start ? '^' : '') . $search . '/i', $item['text']))
			{
				$ret[] = array('value' => $item['text'], 'text' => $item['text']);
			}
		}
		
		echo json_encode($ret);
		
		}
	 
	public function actionJson(){
		echo '<pre>';
		$skills = Skills::model()->findAll();
		$jobTitles = Job::model()->findAll();
		/*echo '<pre>';
		print_r($jobTitles);
		exit;*/
		$jsonArray = array();
		foreach($jobTitles as $key=>$value){
			$jsonArray[$key] = array('text'=>$value->title,'value'=>$value->title);
			}
		print_r(json_encode($jsonArray));
		}
		
	public function actionJson1(){
		$a = array(array('name'=>'afghanistan','value'=>'af'),array('name'=>'pakistan','value'=>'pk'));
		echo '<pre>';
		print_r($a);
		print_r(json_encode($a));
		}
	 
	public function actionTestToken(){
		$this->layout = 'token1';
		$model = new Job;
		$this->render('token',array('model'=>$model));
	}
		
	public function actionCreate()
	{
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$model=new CostCenter;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CostCenter']))
		{
			$model->attributes=$_POST['CostCenter'];
			$model->client_id = $userId;
			$model->date_created = date('Y-m-d');
			if($model->save()){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Cost Center code saved successfully. </div>');
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
					 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					 <strong>Error!</strong> Having some issue while Saving Cost. </div>');
			}

			$this->redirect(array('create'));
		}
		$modelCost = CostCenter::model()->findAllByAttributes(array('client_id'=>$userId));

		$this->render('create',array(
			'model'=>$model,'modelCost'=>$modelCost,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Cost Center code deleted successfully. </div>');
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('create'));
	}

	/**
	 * Lists all models.
	 */

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CostCenter the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CostCenter::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CostCenter $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cost-center-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
