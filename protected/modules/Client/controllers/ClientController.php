<?php
class ClientController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','businessinfo','update','createteammember','deleteteammember','createlocation','deletelocation','skipMembers','skipLocation','admin','submission','rejectSub','saveNote','downloadResume','logReport'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionLogReport(){
		if(isset($_POST['Logreport'])){

			$daterange = explode(' ',$_POST['daterange']);
			$startdate = date("Y-m-d", strtotime($daterange[0]));
			$enddate = date("Y-m-d", strtotime($daterange[4]));
			$criteria=new CDbCriteria;
			$criteria->addBetweenCondition("date_created",$startdate,$enddate,'AND');
			$LogReport = ClientLogreport::model()-> findAll($criteria);
		}else{

		$LogReport = ClientLogreport::model()->findAll();

		}
		$this->render('logReport',array('LogReport'=>$LogReport));
	}
	public function actionView()
	{
		$this->render('view',array());
	}
	
	public function actionSkipLocation()
	{
		$model=Client::model()->findByPk(Yii::app()->user->id);
		$model->skip_createlocation = 1;
		$model->save(false);
		$this->redirect(array('default/index'));
	}
	
	public function actionSkipMembers()
	{
		$model=Client::model()->findByPk(Yii::app()->user->id);
		$model->skip_teammembers = 1;
		$model->save(false);
		$this->redirect(array('createlocation'));
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreateteammember()
	{
		$model=new Client;
		$this->layout = 'buss_steps';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model->scenario='teammember';
		if(isset($_POST['Client']))
		{
			$model->attributes=$_POST['Client'];
			$model->super_client_id=Yii::app()->user->id;
			$model->type='Sub Client';
			$model->date_created=date('Y-m-d');
			$model->date_updated=date('Y-m-d');
			
			if($model->validate() && $model->save()) {
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Team Member added successfully</div>');
				UtilityManager::emailByTemplate($model->id,'Client','client-creation');
				//$this->clientEmail($model->id);
				$this->redirect(array('createteammember'));
			}else{
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Having some issue please try again. </div>');
				 }
				
				//$this->redirect(array('view','id'=>$model->id));
		}
        $allTeam = Client::model()->findAll(array('condition'=>'super_client_id='.Yii::app()->user->id));
       
		$this->render('create',array(
			'model'=>$model,
			'allTeam'=>$allTeam,
		));
	}
	 public function clientEmail($id) {
		 $info = Client::model()->findByPk($id);
          $customerEmail = $info->email;
          //$applicationUrl = $info->application_url;
          $htmlbody='<div align="center">
        <table border="0" cellspacing="0" cellpadding="0" width="650" style="background:white;border:solid #e0e0e0 1.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt">
          <tbody>
            <tr>
              <br valign="top"><h1>Hi,<u></u><u></u></h1>
              <p>Thanks for trying US VMS!</p></br>
                <p>Hi Full Name please confirm your account by clicking the following link::
                '.CHtml::link('Click Here',$this->createAbsoluteUrl('client/accountactiavtion',array('client_id'=>$id))).'
                 <br />
                <p>Please do not reply to this email. If you are receiving this email in error, you can safely ignore it.</p></br>
                <p>The US Tec Solution team</p></br>
                <p></p>
                </td>
            </tr>
          </tbody>
        </table>
      </div> ';
            $message = Yii::app()->sendgrid->createEmail();
      //shortcut to $message=new YiiSendGridMail($viewsPath);
            $message->setHtml($htmlbody)
                ->setSubject('Simplify VMS - Account Verification')
                ->addTo($customerEmail)
                ->setFrom('account@simplifyvms.net');
            Yii::app()->sendgrid->send($message);
   }
	public function actionCreatelocation()
	{
		$model=new Location;
		$this->layout = 'buss_steps';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Location']))
		{
			$model->attributes=$_POST['Location'];
			$model->reference_id=Yii::app()->user->id;
			$model->user_type='Client';
			if($model->validate() && $model->save()) {
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Location added successfully. </div>');
				$LocationCount = Location::model()->countByAttributes(array('reference_id'=>Yii::app()->user->id));
				if($LocationCount ==1 ) {
					
				}
				$this->redirect(array('createlocation'));
			}else{
				 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
				 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 <strong>Error!</strong> Having some issue while adding Location. </div>');
				 }
				
		}
        $allLocation= Location::model()->findAll(array('condition'=>'reference_id='.Yii::app()->user->id));
       
		$this->render('createlocation',array(
			'model'=>$model,
			'allLocation'=>$allLocation,
		));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDeleteteammember($id)
	{
		$model = $this->loadModel($id);
		if($model->super_client_id==Yii::app()->user->id){ 
			if($model->delete()){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Team member deleted successfully. </div>');
				}else{
					 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
					 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					 <strong>Error!</strong> Having some issue while deleting team member. </div>');
					 }
		}
		$this->redirect(array('createteammember'));
	}
	public function actionDeletelocation($id)
	{
		$model = Location::model()->findByPk($id);
		if($model->reference_id==Yii::app()->user->id){
			if($model->delete()){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Location deleted successfully. </div>');
				}else{
					 Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
					 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					 <strong>Error!</strong> Having some issue while deleting Location. </div>');
					 }
		}
		$this->redirect(array('createlocation'));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Client the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Client::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	/**
	 * Performs the AJAX validation.
	 * @param Client $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='client-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionAdmin()
	 {
		 $loginUserId = Yii::app()->user->id;
		 $modelClient = Client::model()->findByPk($loginUserId);
		 
		 
		$hiringManagerCond = '';
		 $allJobIDs = array();
		 if($modelClient->member_type == 'Hiring Manager'){
				$mainCientJobs = Job::model()->findAllByAttributes(array('user_subclient_id'=>$loginUserId));
				foreach($mainCientJobs as $mainCientJobsKey=>$mainCientJobsValue){ 
					$allJobIDs[$mainCientJobsValue->id] = $mainCientJobsValue->id; 
				}
				
				$hiringManagerCond = " and sub.job_id IN ('".implode("','" , $allJobIDs)."')";
			}
			
			
		$mainclient = UtilityManager::superClient(Yii::app()->user->id);
		
		  
		 $condition ='';
	  if(isset($_POST['s'])){
	   /*if(empty($_POST['type']) || $_POST['type'] == 'Name'){
	   //or CONCAT_WS("",can.first_name, can.last_name)='.$_POST['s']
		   $condition = 'and can.first_name="'.$_POST['s'].'" or can.last_name="'.$_POST['s'].'" or CONCAT(can.first_name, " ", can.last_name)="'.$_POST['s'].'"';
	   }else{
		   $condition = 'and can.phone="'.$_POST['s'].'"';
	   }*/
		  $condition = 'and sub.job_id='.$_POST['s'];
	  }
		 
		$sql ="select  can.* , sub.job_id,sub.recommended_pay_rate,sub.candidate_Id,sub.makrup as vendorMarkup ,sub.candidate_pay_rate,sub.estimate_start_date,sub.resume_status,sub.id as sub_id FROM vms_vendor_job_submission sub,vms_candidates as can

   WHERE sub.candidate_Id =can.id  $condition  and ( sub.resume_status IN(3,4,5,7,8,9) OR (sub.resume_status = 6 and sub.rejected_type != 'msp' )) and sub.client_id='".$mainclient."' ".$hiringManagerCond." AND sub.id !=".Yii::app()->user->getState('submission_id')."  ORDER BY id DESC";
 
		 $count_query = "select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can WHERE sub.candidate_Id =can.id $condition  and (sub.resume_status IN(3,4,5,7,8,9) OR (resume_status = 6 and rejected_type != 'msp' )) AND sub.id !=".Yii::app()->user->getState('submission_id')." and sub.client_id=".$mainclient;
 
		 $item_count = Yii::app()->db->createCommand($count_query )->queryScalar();

		 $dataProvider=new CSqlDataProvider($sql, array(
			 'keyField'=>'id',
			 'totalItemCount'=>$item_count,
			 'pagination'=>array(
				 'pageSize'=>25,
			 ),
		 ));

		 $model = $dataProvider->getData(); 
		 //$model = Candidates::model()->findAll(array('condition'=>$condition,'order'=>'id DESC'));
	
	  $this->render('admin',array(
	   'model'=>$model,'pages'=>$dataProvider->pagination
	  ));
	 }
	 
	
	public function actionSubmission(){
	  $id = $_GET['submission-id'];
	  $vendorSubmission = VendorJobSubmission::model()->findByPk($id);
	  $candidateModel = Candidates::model()->findByPk($vendorSubmission->candidate_Id);
	  
		$vendor = Vendor::model()->findByAttributes(array('id'=>$vendorSubmission->vendor_id));
		$vendorTeam = Vendor::model()->findByAttributes(array('id'=>$candidateModel->source));
		$jobModel=Job::model()->findByPk($vendorSubmission->job_id);
		$candidatesProfile = CandidatesProfile::model()->findByAttributes(array('candidate_id'=>$candidateModel->id));

		if($vendorSubmission->resume_status == 1 || $vendorSubmission->resume_status == 2 || $vendorSubmission->resume_status == 3){
			$vendorSubmission->resume_status = 4;
			$vendorSubmission->save(false);
		}
	
	   if(isset($_POST['VendorJobSubmission'])){
		$vendorSubmission->resume_status = $_POST['VendorJobSubmission']['resume_status'];
		$vendorSubmission->save(false);
		
		
		//timeLine code by Mike
		$Clientdata = Client::model()->findByPk($vendorSubmission->client_id);
		$Candidatedata = Candidates::model()->findByPk($vendorSubmission->candidate_Id);
		$Jobdata = Job::model()->findByPk($vendorSubmission->job_id);
		$history = new JobHistory;
		$history->client_id = $vendorSubmission->client_id;
		$history->job_id = $vendorSubmission->job_id;
		//$history->workflow_status = 'Approved';
		   $resumeStatus= UtilityManager::resumeStatus();
		$history->operation = $resumeStatus[$_POST['VendorJobSubmission']['resume_status']];
		$history->description = $Clientdata->first_name.' '.$resumeStatus[$_POST['VendorJobSubmission']['resume_status']].' '.$Candidatedata->first_name.' '.$Candidatedata->last_name;
		$history->save(false);
	
		//Yii::app()->user->setFlash('success', '<strong>Status!</strong> '.$vendorSubmission->resume_status);
		Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Status!</strong> Changed successfully.</div>');
	   }
	
	  $this->render('submission',array('vendorSubmission'=>$vendorSubmission,'vendor'=> $vendor,'vendorTeam'=>$vendorTeam,'candidateModel'=>$candidateModel,'jobModel'=>$jobModel,'candidatesProfile'=>$candidatesProfile));
	 }
	
	 public function actionRejectSub(){
   
	   $id = $_GET['submission-id'];
	   $vendorSubmission = VendorJobSubmission::model()->findByPk($id);
	   $vendorSubmission->resume_status = 6;
	   $vendorSubmission->rejected_by = Yii::app()->user->id;
	   $vendorSubmission->rejected_type ='Client';
	   $vendorSubmission->reason_for_rejection =$_POST['rejection_type'];
	   $vendorSubmission->notes =$_POST['notes'];
	   $vendorSubmission->save(false);
	   
		$Clientdata = Client::model()->findByPk($vendorSubmission->client_id);
		 $Candidatedata = Candidates::model()->findByPk($vendorSubmission->candidate_Id);
		$Jobdata = Job::model()->findByPk($vendorSubmission->job_id);
		$history = new JobHistory;
		$history->client_id = $vendorSubmission->client_id;
		$history->job_id = $vendorSubmission->job_id;
		//$history->workflow_status = 'Approved';
		$history->operation = 'Rejected';
		$history->description = $Clientdata->first_name.' Rejected '.$Candidatedata->first_name.' '.$Candidatedata->last_name;
		$history->save(false);
	   
	   Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Status!</strong> Changed.</div>');
	
	   $this->redirect(array('submission','submission-id'=>$id));
	  }
	
	 public function actionSaveNote(){
	  
	  $id = $_POST['submissionID'];
	  $vendorSubmission = VendorJobSubmission::model()->findByPk($id);
	  $vendorSubmission->notes = $_POST['notes'];
	  $vendorSubmission->save(false);
	  echo 'Saved';exit;
	
	  
	 }
	 
	 
	 public function actionDownloadResume($id){

	  $model = Candidates::model()->findByPk($id);
	
	   $baseUrl = Yii::app()->getBaseUrl(true);
			if (strstr($baseUrl, 'local')) {
				//$path = Yii::app()->basePath.'/../msp/exhibit/';
			   $resume = Yii::app()->basePath.'/../candidate_resumes/';
			}else {
	   //$resume = '/home/simplifydemo.net/public_html/msp/candidate_resumes/';
				$resume = Yii::app()->basePath.'/../candidate_resumes/';
			}
	
	  /*if(!$model->source)
	   $resume = Yii::app()->basePath.'/../candidate_resumes/';
	  else
	
	  $resume = '/home/wwwinfivmsnity/public_html/vendor/candidate_resumes/';*/
	  
	  
	  $file = $resume.$model->resume;
	
	if ($model) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		readfile($file);
		exit;
	}
	 }
	 
	 
	 // for sorting of submissions
	public function actionSort()
	 {
		 $loginUserId = Yii::app()->user->id;
		 $modelClient = Client::model()->findByPk($loginUserId);
 		 
		$hiringManagerCond = '';
		 $allJobIDs = array();
		 if($modelClient->member_type == 'Hiring Manager'){
				$mainCientJobs = Job::model()->findAllByAttributes(array('user_subclient_id'=>$loginUserId));
				foreach($mainCientJobs as $mainCientJobsKey=>$mainCientJobsValue){ 
					$allJobIDs[$mainCientJobsValue->id] = $mainCientJobsValue->id; 
				}
				
				$hiringManagerCond = " and sub.job_id IN ('".implode("','" , $allJobIDs)."')";
			}
			
			
		$mainclient = UtilityManager::superClient(Yii::app()->user->id);
		
		  
		 $condition ='';
	  if(isset($_POST['s'])){
	   /*if(empty($_POST['type']) || $_POST['type'] == 'Name'){
	   //or CONCAT_WS("",can.first_name, can.last_name)='.$_POST['s']
		   $condition = 'and can.first_name="'.$_POST['s'].'" or can.last_name="'.$_POST['s'].'" or CONCAT(can.first_name, " ", can.last_name)="'.$_POST['s'].'"';
	   }else{
		   $condition = 'and can.phone="'.$_POST['s'].'"';
	   }*/
		  $condition = 'and sub.job_id='.$_POST['s'];
	  }
		 
	  $sql ="select  can.* , sub.job_id,sub.recommended_pay_rate,sub.candidate_Id,sub.makrup as vendorMarkup ,sub.candidate_pay_rate,sub.estimate_start_date,sub.resume_status,sub.id as sub_id FROM vms_vendor_job_submission sub,vms_candidates as can

			WHERE sub.candidate_Id =can.id  $condition  and (sub.resume_status = 3 OR resume_status = 4 OR resume_status = 5 OR resume_status = 7 OR resume_status = 8 OR resume_status = 9 OR (resume_status = 6 and rejected_type != 'msp' )) and sub.client_id='".$mainclient."' ".$hiringManagerCond." ORDER BY id DESC";

		 $count_query = "select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can WHERE sub.candidate_Id =can.id $condition  and (sub.resume_status = 3 OR resume_status = 4 OR resume_status = 5 OR resume_status = 7 OR resume_status = 8 OR resume_status = 9 OR (resume_status = 6 and rejected_type != 'msp' )) and sub.client_id=".$mainclient;

		 $item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		 $dataProvider=new CSqlDataProvider($sql, array(
			 'keyField'=>'id',
			 'totalItemCount'=>$item_count,
			 'pagination'=>array(
				 'pageSize'=>10,
			 ),
		 ));

		 $model = $dataProvider->getData();
		 
		 //$model = Candidates::model()->findAll(array('condition'=>$condition,'order'=>'id DESC'));
	
	  $this->render('admin',array(
	   'model'=>$model,'pages'=>$dataProvider->pagination
	  ));
	 }
	 
	 
}
