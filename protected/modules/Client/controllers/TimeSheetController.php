<?php

class TimeSheetController extends Controller
{
	public function filters() 
	{ 
		return array( 
			'accessControl', // perform access control for CRUD operations 
		); 
	} 
	/** 
	 * Specifies the access control rules. 
	 * This method is used by the 'accessControl' filter. 
	 * @return array access control rules 
	 */ 
	public function accessRules() 
	{ 
		return array( 
		 
			array('allow', // allow admin user to perform 'admin' and 'delete' actions 
				'actions'=>array('pendingTimeSheets','approvedTimeSheets','rejectedTimeSheets'),
				'users'=>array('@'), 
			), 
			array('deny',  // deny all users 
				'users'=>array('*'), 
			), 
		); 
	}
	
	public function actionIndex()
	{			
		$this->render('index');
	}
	
	public function actionPendingTimeSheets(){
			//28 userid have records
			$userID = Yii::app()->user->id;
			$client = Client::model()->findByPk($userID);
			//adding this condition for now to show timesheet list to the main client.
			// .' or '.$client->super_client_id.'=0
			if($client->super_client_id==0 || $client->member_type=='Administrator'){
				$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
				$sql = 'SELECT t.* FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status = 0 and o.client_id='.$loginUserId.' order by id desc ';

				$count_query = 'select count(*) FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status = 0 and o.client_id='.$loginUserId;

				$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
			}else{
					
			$sql = 'SELECT t.* FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status = 0 and ( o.approval_manager = '.$userID.' or '.$client->super_client_id.'=0 ) order by id desc ';
					
			$count_query = 'select count(*) FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status = 0 and ( o.approval_manager = '.$userID.' or '.$client->super_client_id.'=0 )';

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
			}
		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$record = $dataProvider->getData();
			
			$this->render('/offer/timesheetList',array('record'=>$record,'pages'=>$dataProvider->pagination));
		}
		
	public function actionApprovedTimeSheets(){
			$userID = Yii::app()->user->id;
			$client = Client::model()->findByPk($userID);
		if($client->super_client_id==0 || $client->member_type=='Administrator'){
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
			$sql = 'SELECT t.* FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status=1 and o.client_id='.$loginUserId.' order by id desc ';

			$count_query = 'select count(*) FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status=1 and o.client_id='.$loginUserId;

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		}else{
			$sql = 'SELECT t.* FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status=1 and (o.approval_manager = '.$userID.' or '.$client->super_client_id.'=0 ) order by id desc ';
					
			$count_query = 'select count(*) FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status=1 and (o.approval_manager = '.$userID.' or '.$client->super_client_id.'=0 )';

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		}
		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$record = $dataProvider->getData();
			
		$this->render('/offer/timesheetList',array('record'=>$record,'pages'=>$dataProvider->pagination));
	}
		
	public function actionRejectedTimeSheets(){
			$userID = Yii::app()->user->id;
			$client = Client::model()->findByPk($userID);
		if($client->super_client_id==0 || $client->member_type=='Administrator') {
			$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
			$sql = 'SELECT t.* FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status=2 and o.client_id='.$loginUserId.' order by id desc ';

			$count_query = 'select count(*) FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status=2 and o.client_id='.$loginUserId;

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		}else{
			$sql = 'SELECT t.* FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status=2 and ( o.approval_manager = '.$userID.' or '.$client->super_client_id.'=0 ) order by id desc ';
					
			$count_query = 'select count(*) FROM `cp_timesheet` as t
					INNER JOIN vms_workorder as o
					ON t.workorder_id=o.id
					WHERE t.timesheet_sub_status!=0 and t.timesheet_status=2 and ( o.approval_manager = '.$userID.' or '.$client->super_client_id.'=0 )';

			$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
		}
			$dataProvider=new CSqlDataProvider($sql, array(
				'keyField'=>'id',
				'totalItemCount'=>$item_count,
				'pagination'=>array(
					'pageSize'=>25,
				),
			));

			$record = $dataProvider->getData();
			
			$this->render('/offer/timesheetList',array('record'=>$record,'pages'=>$dataProvider->pagination));
	}
}