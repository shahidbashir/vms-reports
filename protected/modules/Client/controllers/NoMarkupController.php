<?php

class NoMarkupController extends Controller
{
	public function filters() 
	{ 
		return array( 
			'accessControl', // perform access control for CRUD operations 
			'postOnly + delete', // we only allow deletion via POST request 
		); 
	} 
	/** 
	 * Specifies the access control rules. 
	 * This method is used by the 'accessControl' filter. 
	 * @return array access control rules 
	 */ 
	public function accessRules() 
	{ 
		error_reporting(0);
		return array( 
			array('allow',  // allow all users to perform 'index' and 'view' actions 
				'actions'=>array(''), 
				'users'=>array('*'), 
			), 
			array('allow', // allow authenticated user to perform 'create' and 'update' actions 
				'actions'=>array('jobStep1','jobstep2','addjob','selectetemplate','clientLocations'),
				'users'=>array('@'), 
			), 
			array('allow', // allow admin user to perform 'admin' and 'delete' actions 
				'actions'=>array(''), 
				'users'=>array('admin'), 
			), 
			array('deny',  // deny all users 
				'users'=>array('*'), 
			), 
		); 
	}

	public function actionAddjob()
	{
		if(isset($_POST['new_job'])){

			//for now removing the validation of special chracters from job title;
			/* if (preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $_POST['new_job_title']))
             {
              Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Error!</strong> Your Job Title must be letter</div>');
              $this->redirect(array('job/addjob'));
             }*/
			Yii::app()->session['job_title'] = $_POST['new_job_title'];

			$this->redirect(array('noMarkup/jobStep1'));
		}

		if(isset(Yii::app()->session['job_title'])){
			unset(Yii::app()->session['job_title']);
		}


		if(isset(Yii::app()->session['job_id'])){
			unset(Yii::app()->session['job_id']);
		}
		$this->render('addjob');
	}

	public function actionJobStep1(){
		$clientID = Yii::app()->user->id;
		$mainclient = Yii::app()->user->id;
		$client = Client::model()->findByPk($mainclient);
		if($client->member_type!=NULL){
			$client = Client::model()->findByPk($client->super_client_id);
			$mainclient = $client->id;
		}

		if($client->member_type == 'Team Member'){
			$this->redirect(array('job/jobListing'));
		}

		if(isset($_GET['id'])){
			$model = Job::model()->findByPk($_GET['id']);
		}else{
			$model=new Job;
		}

		$this->layout = 'admin';
		if(isset($_POST['Job']))
		{
			/*$startdate = strtotime($_POST['job_po_duration']);
			$enddate = strtotime($_POST['job_po_duration_endDate']);

			if($startdate > $enddate){
				Yii::app()->user->setFlash('error123', "<p class='required-field'>Job start date can't be greater then end date</p>");
				$this->redirect(Yii::app()->user->returnUrl);
			}*/

			$model->attributes=$_POST['Job'];

			$model->user_id = $mainclient;
			$model->user_subclient_id = $clientID;
			$model->user_type = 'Client';
			$model->job_po_duration = $_POST['job_po_duration'];
			$model->job_po_duration_endDate = $_POST['job_po_duration_endDate'];
			$model->desired_start_date = $_POST['desired_start_date'];
			$model->markup = $_POST['Job']['vendor_client_markup'];
			$model->cost_center_code = $_POST['Job']['cost_center_code'];;
			$model->date_created = date('Y-m-d');

			//if template is selected then storing its id
			if(isset($_POST['template_id'])){
				$model->template_id = $_POST['template_id'];
			}

			//old skills
			$skill = Skills::model()->findAll();
			$OldSkills = array();
			foreach($skill as $skill){
				$OldSkills[] = strtolower($skill->name);
			}

			if($_POST['Job']['skills']) {
				$SkillsAddition = $_POST['Job']['skills'];
				$model->skills = implode(',',$_POST['Job']['skills']);
			}else {
				$model->skills = '';
			}

			if($_POST['Job']['skills1']) {
				$SkillsAddition1 = $_POST['Job']['skills1'];
				$model->skills1 = implode(',',$_POST['Job']['skills1']);
			}else {
				$model->skills1 = '';
			}

			if($_POST['Job']['skills2']) {
				$SkillsAddition2 = $_POST['Job']['skills2'];
				$model->skills2 = implode(',',$_POST['Job']['skills2']);
			}else {
				$model->skills2 = '';
			}

			if(isset($_POST['Job']['location'])){
				$model->location = serialize($_POST['Job']['location']);
			}
			//doing this becuase teamMembers will be multiple.
			if(isset($_POST['Job']['invite_team_member'])) {
				$model->invite_team_member = serialize($_POST['Job']['invite_team_member']);
			}
			//$model->request_dept = $_POST['Job']['request_dept'];

			if($model->save()){

				$history = new JobHistory;
				$history->client_id = $client->id;
				$history->job_id = $model->id;
				$history->operation = 'added Job';
				$history->job_status = 'Pending';
				$history->description = $client->first_name . '  added Job  Title '.$model->title;
				$history->save(false);

				// Invite teammeber to this job
				if(isset($_POST['Job']['invite_team_member'])) {
					if(!empty($_POST['Job']['invite_team_member'][0])){
						$inviteteammember = $_POST['Job']['invite_team_member'];
						foreach($inviteteammember as $value) {
							if(Yii::app()->db->createCommand('insert into vms_teammember_job set teammember_id='.$value.' , job_id='.$model->primaryKey.', date_created="'.date('Y-m-d').'"')->execute())
							{
								//$this->clientEmail(Yii::app()->db->getLastInsertID());
								//Yii::app()->user->setFlash('success', "Team member invited successfully");
							}
						}
					}
				}

				//this code is to remove duplication of skills.
				if($_POST['Job']['skills']) {
					foreach($SkillsAddition as $skillVal){
						$skillModel = new Skills;
						if (!in_array(str_replace(' ', '', strtolower($skillVal)), $OldSkills)) {
							$skillModel->name =  $skillVal;
							$skillModel->save(false);
						}

					}
				}

				if($_POST['Job']['skills1']) {
					foreach($SkillsAddition1 as $skillVal1){
						$skillModel = new Skills;
						if (!in_array(str_replace(' ', '', strtolower($skillVal1)), $OldSkills)) {
							$skillModel->name =  $skillVal1;
							$skillModel->save(false);
						}

					}
				}
				if($_POST['Job']['skills2']) {
					foreach($SkillsAddition2 as $skillVal2){
						$skillModel = new Skills;
						if (!in_array(str_replace(' ', '', strtolower($skillVal2)), $OldSkills)) {
							$skillModel->name =  $skillVal2;
							$skillModel->save(false);
						}

					}
				}


				// workflow approval template...
				$loc = unserialize($model->location);
				$locationsV = '';
				if($loc){
					foreach($loc as $val){
						$location = Location::model()->findByPk($val);
						$locationsV .= $location->name.', ';
					}
				}
				/*Yii::app()->user->setFlash('success', '<div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <strong>Success!</strong> Your Job Posting was successfully submitted. After review it would be published</div>'); */

				$this->redirect(array('jobstep2','id'=>$model->id));
				//$this->redirect(array($_SERVER['HTTP_REFERER']));
			}else{
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger"> 
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Error!</strong> Your Job Posting was not successfully submitted. </div>');
			}
		}
		$this->render('create',array (
			'model'=>$model,
		));

	}

	public function actionJobstep2(){

		error_reporting(0);
		$userId = UtilityManager::superClient(Yii::app()->user->id);
		$client = Client::model()->findByPk(Yii::app()->user->id);

		Yii::app()->session['job_id'] = $_GET['id'];
		$model = Job::model()->findByPk($_GET['id']);

		$clientWType = WorkflowType::model()->findByAttributes(array('client_id'=>$model->user_id));

		//if(isset($_POST['Post_secondForm'])){
		if(isset($_POST['Post_secondForm']) || isset($_POST['save_draft']) || isset($_POST['publish_template'])){

			$model->attributes=$_POST['Job'];
			//$model->markup = $_POST['Job']['vendor_client_markup'];
			$model->jobstep2_complete = 1;
			if($model->save(false)){

				//if client is with the setting of workflow approval yes then this code will run otherwise else
				if( empty($clientWType->workflow_process) || $clientWType->workflow_process=='Yes'){
				}else{
					$model->jobStatus = 3;
					$model->save(false);
				}

				if(isset($_POST['save_draft'])){
					$model->jobStatus = 2;
					$model->save(false);
				}
				//creating new template
				if(isset($_POST['publish_template'])){
					$this->JobCreationTemplate($model);
				}

				unset(Yii::app()->session['job_id']);
			}
			$WorklflowMember = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_POST['Job']['work_flow']),
				array(
					'order' => 'order_approve asc'
				));

			$WorklflowMemberLimit = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_POST['Job']['work_flow']),
				array(
					'order' => 'order_approve asc',
					'limit' => 1,
				));

			$workflowID =  $_POST['Job']['work_flow'];

			foreach($WorklflowMember as $keyFlow=>$valueFlow){
				$JobWorkflow = new JobWorkflow;
				$JobWorkflow->job_id = $model->id;
				$JobWorkflow->workflow_id = $_POST['Job']['work_flow'];
				$JobWorkflow->client_id = $valueFlow->client_id;
				$JobWorkflow->number_of_approval = $valueFlow->order_approve;
				//if client is with the setting of workflow approval yes then this code will run otherwise else
				if( empty($clientWType->workflow_process) || $clientWType->workflow_process=='Yes'){
					$JobWorkflow->job_status = 'Pending';
					$JobWorkflow->email_sent = 0;
				}else{
					$JobWorkflow->job_status = 'Approved';
					$JobWorkflow->email_sent = 1;
					$JobWorkflow->status_time = date('Y-m-d H:i:s');
				}

				$JobWorkflow->save(false);
			}

			//timeLine code by Mike
			$workFlowdata = Worklflow::model()->findByPk($_POST['Job']['work_flow']);
			$history = new JobHistory;
			$history->client_id = $client->id;
			$history->job_id = $model->id;
			$history->workflow_status = 'Pending';
			$history->operation = 'Assigning Work flow';
			$history->description = $client->first_name . '  Assigning Work flow  '.$workFlowdata->flow_name.' to The Job Title '.$model->title;
			$history->save(false);

			$loc = unserialize($model->location);
			$locationsV = '';
			if($loc){
				foreach($loc as $val){
					$location = Location::model()->findByPk($val);
					$locationsV .= $location->name.', ';
				}
			}

			//if client is with the setting of workflow approval yes then this code will run otherwise else
			if( empty($clientWType->workflow_process) || $clientWType->workflow_process=='Yes'){

				$clientData = Client::model()->findByPk($WorklflowMemberLimit[0]['client_id']);
				$JobWorkflowEmail = JobWorkflow::model()->findAllByAttributes(array('number_of_approval'=>1,'workflow_id'=>$_POST['Job']['work_flow'],'job_id'=>$model->id));


				// workflow approval template...

				$workFlow = Worklflow::model()->findByPk($model->work_flow);

				$htmlbody =  '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px,Arial,sans-serif;padding:2% 7%">
            <div>

            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$clientData->first_name.' '.$clientData->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">We are waiting for your approval.</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4"><strong>Job ID </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->id.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Name </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->title.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Description </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->description.'</span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Pay Rate </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->pay_rate.' </span><br /><br />


						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Location </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$locationsV.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Hour for the Job </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->hours_per_week.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Shift Time. </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->shift.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Project Reference Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->pre_reference_code.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Bill Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->billing_code.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Request Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->request_dept.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Work Flow Approval : </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$workFlow->flow_name.' </span><br /><br />


						To Approve the Job ,
						<a href="'.Yii::app()->createAbsoluteUrl('Client/job/directApprove',
						array('workflowid'=>$workflowID,'jobid'=>$model->id,'clientid'=>$WorklflowMemberLimit[0]['client_id'])).'" style="color:#069;font-size:13px;line-height:1.4;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://fwebinc.freshsales.io/confirmations/xbootIN_zLafNA1D3eYAoA&amp;source=gmail&amp;ust=1468345965323000&amp;usg=AFQjCNHpRcdBxBCT16Bscb76mCXx0b9dDw">click here</a>
                    </p><br />

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4">If clicking the link does not work, copy and paste this URL in your browser\'s address bar to approve job</span><br>
                        '.Yii::app()->createAbsoluteUrl('Client/job/directApprove',
						array('workflowid'=>$workflowID,'jobid'=>$model->id,'clientid'=>$WorklflowMemberLimit[0]['client_id'])).'
                    </p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
                        Thanks,<br> Webmaster - Simplifyvms
                    </p>
                </div>
            </div>


            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';


				$message = Yii::app()->sendgrid->createEmail();
				//shortcut to $message=new YiiSendGridMail($viewsPath);
				$message->setHtml($htmlbody)
					->setSubject('Simplyfy VMS - Job Approval Invitation')
					->addTo($clientData->email)
					->setFrom('alert@simplifyvms.net');

				//till here workflow approval

				//because of some issue email is not getting sent to thats commented portion of code is not working
				//so now moving it out from the mail condition will fix this later

				Yii::app()->sendgrid->send($message);

				/*if(Yii::app()->sendgrid->send($message)){
                    if($JobWorkflowEmail[0]) {
                        $JobWorkflowEmail[0]->email_sent = 1;
                        $JobWorkflowEmail[0]->save(false);
                    }
                }*/
				if($JobWorkflowEmail[0]) {
					$JobWorkflowEmail[0]->email_sent = 1;
					$JobWorkflowEmail[0]->save(false);
				}
			}else{
				foreach($WorklflowMember as $keyFlow=>$valueFlow){
					$clientData = Client::model()->findByPk($valueFlow->client_id);
					$workFlow = Worklflow::model()->findByPk($model->work_flow);
					/*$JobWorkflow = JobWorkflow::model()->findByPk($valueFlow->id);
					//$JobWorkflow->job_id = $model->id;
					//$JobWorkflow->workflow_id = $_POST['Job']['work_flow'];
					//$JobWorkflow->client_id = $valueFlow->client_id;
					//$JobWorkflow->number_of_approval = $valueFlow->order_approve;
					//$JobWorkflow->job_status = 'Approved';
					$JobWorkflow->email_sent = 1;
					if($JobWorkflow->save(false)){*/
					UtilityManager::directJobApprovalNotify($clientData,$model,$locationsV,$workFlow);
					//}
				}
			}
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your Job Posting was successfully submitted. After review it would be published</div>');
			//exit;
			$this->redirect(array('addjob'));
			//$this->redirect(array($_SERVER['HTTP_REFERER']));
		}

		$this->render('jobstep2',array('model'=>$model));


	}

	public function actionSelectetemplate(){

		if(isset($_POST['templet_id'])){
			if(isset(Yii::app()->session['template_id'])){
				unset(Yii::app()->session['template_id']);
			}
			Yii::app()->session['template_id'] = $_POST['templet_id'];
		}

		$model = new Job;
		$jobTempelatemodel = JobTemplates::model()->findByPk(Yii::app()->session['template_id']);
		$this->render('selecttempelate',array('model'=>$model,'jobTempelatemodel'=>$jobTempelatemodel));
	}
	
	public function actionClientLocations(){
		   $pro_client_id = $_POST['pro_client_id'];
		   $Client = ProClientNames::model()->findByPk($pro_client_id);
		   $client_locations = ClientLocations::model()->findAllByAttributes(array('client_id'=>$Client->client_id));
			foreach ($client_locations as $location){
		    ?><option value="<?php echo $location->location_id; ?>"><?php echo $location->location_name; ?></option>
			<?php 
			}
 	}
	
	
	
}