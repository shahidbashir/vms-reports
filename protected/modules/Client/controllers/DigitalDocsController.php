<?php

class DigitalDocsController extends Controller
{
	public function filters() 
	{ 
		return array( 
			'accessControl', // perform access control for CRUD operations 
			'postOnly + delete', // we only allow deletion via POST request 
		); 
	} 
	/** 
	 * Specifies the access control rules. 
	 * This method is used by the 'accessControl' filter. 
	 * @return array access control rules 
	 */ 
	public function accessRules() 
	{ 
		error_reporting(0);
		return array( 
			array('allow',  // allow all users to perform 'index' and 'view' actions 
				'actions'=>array(''), 
				'users'=>array('*'), 
			), 
			array('allow', // allow authenticated user to perform 'create' and 'update' actions 
				'actions'=>array('downloadContractDigitalDoc','index','deleteDigitalDocs','downloadDigitalDoc','downloadOfferDigitalDoc'),
				'users'=>array('@'), 
			), 
			array('allow', // allow admin user to perform 'admin' and 'delete' actions 
				'actions'=>array(''), 
				'users'=>array('admin'), 
			), 
			array('deny',  // deny all users 
				'users'=>array('*'), 
			), 
		); 
	}
	public function actionDownloadContractDigitalDoc($id){
		$model = Contract::model()->findByPk($id);

		$baseUrl = Yii::app()->getBaseUrl(true);
		if (strstr($baseUrl, 'local')) {
			$resume = Yii::app()->basePath.'/../signedDocs/';
		}else {
			$resume = Yii::app()->basePath.'/../signedDocs/';
		}

		$file = $resume.$model->offer_digital_doc;

		//file_put_contents($file, "zoobia fayyaz", FILE_APPEND);


		if ($model) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}
	public function actionDownloadOfferDigitalDoc($id){

		$model = Offer::model()->findByPk($id);

		$baseUrl = Yii::app()->getBaseUrl(true);
		if (strstr($baseUrl, 'local')) {
			$resume = Yii::app()->basePath.'/../signedDocs/';
		}else {
			$resume = Yii::app()->basePath.'/../signedDocs/';
		}

		$file = $resume.$model->offer_digital_doc;

		//file_put_contents($file, "zoobia fayyaz", FILE_APPEND);


		if ($model) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}

	public function actionIndex()
	{
		$userId = Yii::app()->user->id;
		$superClient = UtilityManager::superClient($userId);
		$model = new DigitalDocs;
		
			
		if(isset($_POST['update'])){
				$data1 = DigitalDocs::model()->findByPk($_POST['id']);
				$data1->attributes=$_POST['DigitalDocs'];
				if($data1->save(false)){
					Yii::app()->user->setFlash('msg', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Updated successfully.</div>');
					}else{ 
					  Yii::app()->user->setFlash('msg', '<div class="alert alert-danger">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <strong>Error!</strong> Not Updated.</div>');
					 }
			}
		
		if(isset($_POST['DigitalDocs']) && (!isset($_POST['update']))){
			$model->attributes=$_POST['DigitalDocs'];
			$model->client_id = $userId;
			$model->created_by_id = $superClient;
			$model->date_created = date('Y-m-d H:i:s');
			$rnd = rand(0,9999);
			if(!empty($_FILES['DigitalDocs']['name']['file'])){
			$imageUploadFile = CUploadedFile::getInstance($model, 'file');
			   if($imageUploadFile != ""){
				 $fileName = "{$rnd}-{$imageUploadFile}";  // random number + file name
			     $model->file = $fileName;
				}
			}
			
			if($model->save()){
				if(!empty($_FILES['DigitalDocs']['name']['file'])){
					if($imageUploadFile !==""){ // validate to save file
							$imageUploadFile->saveAs(Yii::app()->basePath.'/../digitalDocs/'.$fileName);
					}
				}
				Yii::app()->user->setFlash('msg', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Document uploaded successfully.</div>');
			}else{ 
				  Yii::app()->user->setFlash('msg', '<div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Document not uploaded.</div>');
				 }
			$this->redirect('index');
			
			}	
		
		$data = DigitalDocs::model()->findAllByAttributes(array('client_id'=>$superClient));
		$this->render('index',array('model'=>$model,'data'=>$data));
	}
	
	public function actionDeleteDigitalDocs(){
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$model = DigitalDocs::model()->findByPk($id);
			if($model->delete()){
				Yii::app()->user->setFlash('msg', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your file Successfully Deleted.</div>');
			}else{
				Yii::app()->user->setFlash('msg', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Error!</strong> Something went wrong.</div>');
			}
			$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadDigitalDoc($id){

		$model = DigitalDocs::model()->findByPk($id);

		$baseUrl = Yii::app()->getBaseUrl(true);
		if (strstr($baseUrl, 'local')) {
			$resume = Yii::app()->basePath.'/../digitalDocs/';
		}else {
			$resume = Yii::app()->basePath.'/../digitalDocs/';
		}
		
		$file = $resume.$model->file;
		
		
		//file_put_contents($file, "zoobia fayyaz", FILE_APPEND);
		

		if ($model) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}
}