<?php

class JobEditController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules(){
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('getWorkFlowID','editDetails','editSkills','editInternalDetails','editOtherInfo','editRates','vendorMarkup'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(''),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	public function actionEditDetails()
	{
		$jobID = $_GET['id'];
		$model = Job::model()->findByPk($jobID);
		if(isset($_POST['editdetails']) || isset($_POST['publish'])){
			$model->attributes =$_POST['Job'];
			/*$model->job_po_duration = $_POST['job_po_duration'];
			$model->job_po_duration_endDate = $_POST['job_po_duration_endDate'];
			$model->desired_start_date = $_POST['desired_start_date'];*/
			$model->description = $_POST['job_description'];
			$model->you_will = $_POST['you_will'];
			$model->qualification = $_POST['qualifications'];
			$model->add_info = $_POST['additional_information'];
			if(isset($_POST['Job']['location'])){
				$model->location = serialize($_POST['Job']['location']);
			}
			//doing this becuase teamMembers will be multiple.
			if(isset($_POST['Job']['invite_team_member'])) {
				$model->invite_team_member = serialize($_POST['Job']['invite_team_member']);
			}
			
			//$model->pre_candidate = $_POST['Job']['pre_candidate'];
			if(isset($_POST['publish'])){
				$model->jobStatus = 1;
				}
		   if($model->save(false)){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <strong>Success!</strong> Job is Updated</div>');
			$this->redirect(array('editDetails','id'=>$_GET['id']));
			}
		}
		$this->render('editDetails',array('model'=>$model));
	}
	public function actionEditSkills()
	{
		$jobID = $_GET['id'];
		$model = Job::model()->findByPk($jobID);
		if(isset($_POST['editSkills']) || isset($_POST['publish'])){
			$model->attributes =$_POST['Job'];
			if($_POST['Job']['skills']) {
				$SkillsAddition = $_POST['Job']['skills'];
				$model->skills = implode(',',$_POST['Job']['skills']);
			}else {
				$model->skills = '';
			}

			if($_POST['Job']['skills1']) {
				$SkillsAddition1 = $_POST['Job']['skills1'];
				$model->skills1 = implode(',',$_POST['Job']['skills1']);
			}else {
				$model->skills1 = '';
			}

			if($_POST['Job']['skills2']) {
				$SkillsAddition2 = $_POST['Job']['skills2'];
				$model->skills2 = implode(',',$_POST['Job']['skills2']);
			}else {
				$model->skills2 = '';
			}
			if(isset($_POST['publish'])){
				$model->jobStatus = 1;
				}
				
		   if($model->save(false)){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <strong>Success!</strong> Job is Updated</div>');
			$this->redirect(array('editSkills','id'=>$_GET['id']));
			}
		}
		$this->render('editSkills',array('model'=>$model));
	}
	
	public function actionEditInternalDetails()
	{
		$jobID = $_GET['id'];
		$loginUserId = Yii::app()->user->id;
		$model = Job::model()->findByPk($jobID);
		$workflow = Worklflow::model() ->findByPk($model->work_flow);
		$JobrequestDepartment = JobrequestDepartment::model()->findAllByAttributes(array('client_id'=>$loginUserId));
		$BillingcodeDepartment = BillingcodeDepartment::model()->findAllByAttributes(array('client_id'=>$loginUserId));
		$workflowmember = WorklflowMember::model()->findAllByAttributes(array('workflow_id' => $workflow->id),array('order'=>'order_approve asc'));
		if(isset($_POST['internalDetails']) || isset($_POST['publish'])){
			$model->attributes =$_POST['Job'];
			if(isset($_POST['publish'])){
				$model->jobStatus = 1;
				}
		   if($model->save(false)){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <strong>Success!</strong> Job is Updated</div>');
			$this->redirect(array('editInternalDetails','id'=>$_GET['id']));
			}
		}
		$this->render('editInternalDetails',array('model'=>$model,'workflow'=>$workflow,'workflowmember'=>$workflowmember,'JobrequestDepartment'=>$JobrequestDepartment,'BillingcodeDepartment'=>$BillingcodeDepartment));
	}
	
	public function actionEditOtherInfo()
	{
		$jobID = $_GET['id'];
		$model = Job::model()->findByPk($jobID);
		if(isset($_POST['editOtherInfo']) || isset($_POST['publish'])){
			$model->attributes =$_POST['Job'];
			if(isset($_POST['publish'])){
				$model->jobStatus = 1;
				}
		   if($model->save(false)){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <strong>Success!</strong> Job is Updated</div>');
			$this->redirect(array('editOtherInfo','id'=>$_GET['id']));
			}
		}
		$this->render('editOtherInfo',array('model'=>$model));
	}
	
	public function actionEditRates()
	{
		$jobID = $_GET['id'];
		$model = Job::model()->findByPk($jobID);
		if(isset($_POST['editRates'])){
			$model->attributes =$_POST['Job'];
		   if($model->save(false)){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <strong>Success!</strong> Job is Updated</div>');
			$this->redirect(array('editRates','id'=>$_GET['id']));
			}
		}
		$this->render('editRates',array('model'=>$model));
	}
	
	public function actionGetWorkFlowID(){

		if(isset($_POST['estimate_cost']) && isset($_POST['cat_id'])) {

			$sql = 'select * from vms_workflow_configuration where category="' . $_POST['cat_id'] . '" and client_id="' . Yii::app()->user->id . '"  and start_rang <="' . $_POST['estimate_cost'] . '"  and end_rang >="' . $_POST['estimate_cost'] . '"';

			$result = Yii::app()->db->createCommand($sql)->queryRow();
			if ($result) {
				echo $result['workflow_id'];
				 }else{ 
				 		echo ''; 
					  }
		}
	}
		
	public function actionVendorMarkup(){
		
		$categoryID = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$_POST['cat_value'])); 
		
		//Administrative/Clerical Jobs category id 71
		//client id 41
		$rateData = VendorClientMarkup::model()->findAllByAttributes(array('client_id'=>Yii::app()->user->id,'category_id'=>$categoryID->id),
			array(
				'order' => 'mark_up desc', 
				'limit' => '1' 
			));
		if($rateData){
			$mark_up = $rateData[0]->mark_up;
		}else{
			$mark_up = 0;
		}
		echo $mark_up;
	}
}