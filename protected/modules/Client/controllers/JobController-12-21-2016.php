<?php 
class JobController extends Controller 
{ 
	/** 
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning 
	 * using two-column layout. See 'protected/views/layouts/column2.php'. 
	 */ 
	//public $layout='//layouts/job'; 
	/** 
	 * @return array action filters 
	 */ 
	public function filters() 
	{ 
		return array( 
			'accessControl', // perform access control for CRUD operations 
			'postOnly + delete', // we only allow deletion via POST request 
		); 
	} 
	/** 
	 * Specifies the access control rules. 
	 * This method is used by the 'accessControl' filter. 
	 * @return array access control rules 
	 */ 
	public function accessRules() 
	{ 
		return array( 
			array('allow',  // allow all users to perform 'index' and 'view' actions 
				'actions'=>array('directApprove','directView','directApproveTime','uploadFeeds'), 
				'users'=>array('*'), 
			), 
			array('allow', // allow authenticated user to perform 'create' and 'update' actions 
				'actions'=>array('jobStep1','jobstep2','getworkflowApproval','workflowConfiguration','timeLine','jobNotes','index','view','create','update','admin','deletejob','jobListing','email','jobview','mobJobListing','mobJobview','workflowStatus','clone','jobsubmission','submissionView','viewResume','scheduleInterview','joblisting1','overview','waiting','aprrovedFirsttime','aprrovedfourthtime','aprrovedSecondtime','aprrovedThirdtime','scheduleStatus','downloadResume','downloadAttachment','archivedJobs','closedJobs','locations','payrates','worker','interviewCompleted','offers','addjob','jobtemplates','addTemplate','deleteTempelt','selectetemplate','interviewStep1','offer','pendingWorkorder','rejectedWorkorder','approvedworkers','editjob','emaileditjob','downloadResum','billMatchDesc','billMatchAsc','updateTemplate','templateView'),
				'users'=>array('@'), 
			), 
			array('allow', // allow admin user to perform 'admin' and 'delete' actions 
				'actions'=>array('admin','delete'), 
				'users'=>array('admin'), 
			), 
			array('deny',  // deny all users 
				'users'=>array('*'), 
			), 
		); 
	}

	public function actionBillMatchAsc(){
		//error_reporting(0);
		$id =  $_GET['id'];
		$jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id));

		if(!$jobmodel){
			$this->redirect(array('job/joblisting'));
		}

		$query = 'SELECT * from vms_vendor_job_submission where job_id='.$id.' and (resume_status != "1" and resume_status != "2" and( resume_status != "1" and rejected_type != "msp")) order by candidate_pay_rate asc';

		// order by candidate_pay_rate desc

		$submissionData = Yii::app()->db->createCommand( $query )->query()->readAll();
		$this->render('billMatch',array('jobmodel'=>$jobmodel,'submissionData'=>$submissionData));
	}

	public function actionBillMatchDesc(){

		//error_reporting(0);
		$id =  $_GET['id'];
		$jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id));

		if(!$jobmodel){
			$this->redirect(array('job/joblisting'));
		}

		$query = 'SELECT * from vms_vendor_job_submission where job_id='.$id.' and (resume_status != "1" and resume_status != "2" and( resume_status != "1" and rejected_type != "msp")) order by candidate_pay_rate desc';

		// order by candidate_pay_rate desc

		$submissionData = Yii::app()->db->createCommand( $query )->query()->readAll();
		$this->render('billMatch',array('jobmodel'=>$jobmodel,'submissionData'=>$submissionData));
	}

	public function actionTimeLine(){
	  $id =  $_GET['id'];
	  $jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id));
	
	  $JobHistory = JobHistory::model()->findAllByAttributes(array('job_id'=>$id));
	  $this->render('timeLine',array('jobmodel'=>$jobmodel,'JobHistory'=>$JobHistory));
	 }
		
	public function actionJobNotes(){

	   $id =  $_GET['id'];
	
	  $LoginUserId = Yii::app()->user->id;
	  $jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id));
	  if(isset($_POST['JobNotes'])) {
	   $invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id=' . $id)->queryRow();
	
	   if ($invitedTeamMembers['id'] == $LoginUserId || $invitedTeamMembers['super_client_id'] == $LoginUserId) {
	
		$model = new JobComments;
		$model->job_id = $id;
		$model->user_id = $LoginUserId;
		$model->user_type = $invitedTeamMembers['type'];
		$model->user_name = $invitedTeamMembers['first_name'];
		$model->comments = $_POST['JobComments']['comments'];
		if(isset($_POST['JobComments']['show_msp'])) { $showmsp = $_POST['JobComments']['show_msp']; }else{ $showmsp = 0; }
		$model->show_msp = $showmsp;
		$model->date_created = date('Y-m-d');
		$rnd = rand(0, 9999);
		$uploadedFile = CUploadedFile::getInstance($model, 'attachment');
		if ($uploadedFile != "") {
		 $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
		 $model->attachment = $fileName;
		}
		if ($model->save(false)) {
		 if ($uploadedFile != "") {
		  $uploadedFile->saveAs(Yii::app()->basePath . '/../submissionattachment/' . $model->attachment);
		 }
		 Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <strong>Success!</strong> Comment is successfully saved.</div>');
		 $this->redirect(array('jobNotes','id'=>$_GET['id'],'type'=>'jobnotes'));
		}
		Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <strong>Rejected!</strong> Your are not to authorize to comments.</div>');
	   }
	  }
	  $this->render('jobNotes',array('jobmodel'=>$jobmodel));
	 }
		
	public function actionEditJob(){
  $model = Job::model()->findByPk($_GET['id']);
  if(isset($_POST['savjob'])){

   $model->attributes =$_POST['Job'];
   if($model->save(false)){
    Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Job is Updated</div>');
    $this->redirect(array('editJob','id'=>$_GET['id']));
   }

  }
  if(isset($_POST['saveandEmai'])){
   $model->attributes =$_POST['Job'];
   if($model->save(false)){

     $vendorJobSubmission = VendorJobSubmission::model()->findAll(array('condition'=>'job_id='.$_GET['id']));
    if($vendorJobSubmission){
		 foreach($vendorJobSubmission as $value){
		  $vendor = Vendor::model()->findByPk($value->vendor_id);
		  $this->emaileditJob($vendor->email,$vendor->first_name,$vendor->last_name,$_GET['id']);
		 }
    }
    $mspadmin = Admin::model()->findByPk(47);
    $this->emaileditJob($mspadmin->email,$mspadmin->first_name,$mspadmin->last_name,$_GET['id']);
	
	
	
	
    Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Job is Updated and email is created</div>');

    $this->redirect(array('editJob','id'=>$_GET['id']));
   }
  }
  $this->render('editJob',array('model'=>$model));
 }
 	
	public function emaileditJob($email,$firstname,$lastname,$Jobid){

  $jobmodel = Job::model()->findByPk($Jobid);




  $htmlbody = '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px; "Proxima Nova","Helvetica Neue",Arial,sans-serif;padding:2% 7%">
            <div>

            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$firstname.' '.$lastname.',</h1>

      Job ID : '.$jobmodel->id.'<br/>
      JobName : '.$jobmodel->title.'<br/>
      Job Description : '.$jobmodel->description.'<br/>
      Job Pay Rate : '.$jobmodel->pay_rate.'<br/>
                     Number of Submission : '.$jobmodel->number_submission.'<br/>
      Hour Per week : '.$jobmodel->hours_per_week.'<br/>
      Shift Time. : '.$jobmodel->shift.'<br/>
                     Project Reference Code : '.$jobmodel->pre_reference_code.'<br/>
                     Bill Code : '.$jobmodel->billing_code.'<br/>
                     Job Request Code : '.$jobmodel->id.'<br/>

  </div>
                </div>
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';

  $message = Yii::app()->sendgrid->createEmail();
  //shortcut to $message=new YiiSendGridMail($viewsPath);
  $message->setHtml($htmlbody)
   ->setSubject($jobmodel->id.'/'.$jobmodel->title.'- Update on Job')
   ->addTo($email)
   ->setFrom('account@simplifyvms.net');

  Yii::app()->sendgrid->send($message);
 }
 	
	public function actionOffer(){
		error_reporting(0);
		$loginUserId = Yii::app()->user->id;
		if(!empty($_GET['id'])){
			$id = $_GET['id'];
			$jobmodel = Job::model()->findByPk($id);
			$jobID = 'and sub.job_id="'.$jobmodel->id.'"';
			$veiwName = "offer";
		}else{
			$jobID = "";
			$veiwName = "/offer/offer";
		}

		$sql ="select sub.id,sub.vendor_id,sub.job_id,offer.offer_bill_rate,offer.rates_regular,offer.job_start_date,offer.id as offer_id,offer.valid_till_date as expiry_date,

			can.first_name,can.last_name,can.id as c_id,offer.rates_regular,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,offer.workorder_status,offer.status as offer_status,offer.payment_type

			FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and offer.client_id=".$loginUserId." and sub.id = offer.submission_id ".$jobID." order by offer.id desc";

		$count_query = "select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and offer.client_id=".$loginUserId." and sub.id = offer.submission_id ".$jobID;

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$offers = $dataProvider->getData();
		//$offers = Yii::app()->db->createCommand($sql)->query()->readAll();

		$this->render($veiwName,array('model'=>$jobmodel,'offers'=>$offers,'pages'=>$dataProvider->pagination));
	}

	public function actionPendingWorkorder(){
		error_reporting(0);
		$loginUserId = Yii::app()->user->id;
		if(!empty($_GET['id'])){
			$id = $_GET['id'];
			$jobmodel = Job::model()->findByPk($id);
			$jobID = 'and sub.job_id="'.$jobmodel->id.'"';
			$veiwName = "offer";
		}else{
			$jobID = "";
			$veiwName = "/offer/offer";
		}

		$sql ="select sub.id,sub.vendor_id,sub.job_id,offer.id as offer_id,offer.valid_till_date as expiry_date,

			can.first_name,can.last_name,can.id as c_id,offer.rates_regular,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,offer.workorder_status,offer.status as offer_status,offer.payment_type

			FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and offer.workorder_status=0 and offer.client_id=".$loginUserId." and sub.id = offer.submission_id ".$jobID;

		$count_query = 'select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and offer.workorder_status=0 and offer.client_id=".$loginUserId." and sub.id = offer.submission_id '.$jobID;

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$offers = $dataProvider->getData();
		//$offers = Yii::app()->db->createCommand($sql)->query()->readAll();

		$this->render($veiwName,array('model'=>$jobmodel,'offers'=>$offers,'pages'=>$dataProvider->pagination));
	}
	
	public function actionRejectedWorkorder(){
		error_reporting(0);
		$loginUserId = Yii::app()->user->id;
		if(!empty($_GET['id'])){
			$id = $_GET['id'];
			$jobmodel = Job::model()->findByPk($id);
			$jobID = 'and sub.job_id="'.$jobmodel->id.'"';
			$veiwName = "offer";
		}else{
			$jobID = "";
			$veiwName = "/offer/offer";
		}

		$sql ="select sub.id,sub.vendor_id,sub.job_id,offer.id as offer_id,offer.valid_till_date as expiry_date,

			can.first_name,can.last_name,can.id as c_id,offer.rates_regular,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,offer.workorder_status,offer.status as offer_status,offer.payment_type

			FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and offer.workorder_status=2 and offer.client_id=".$loginUserId." and sub.id = offer.submission_id ".$jobID;

		$count_query = 'select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and offer.workorder_status=2 and offer.client_id=".$loginUserId." and sub.id = offer.submission_id '.$jobID;

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$offers = $dataProvider->getData();
		//$offers = Yii::app()->db->createCommand($sql)->query()->readAll();

		$this->render($veiwName,array('model'=>$jobmodel,'offers'=>$offers,'pages'=>$dataProvider->pagination));
	}
	
	public function actionApprovedworkers(){
		error_reporting(0);
		$loginUserId = Yii::app()->user->id;
		if(!empty($_GET['id'])){
			$id = $_GET['id'];
			$jobmodel = Job::model()->findByPk($id);
			$jobID = 'and sub.job_id="'.$jobmodel->id.'"';
			$veiwName = "offer";
		}else{
			$jobID = "";
			$veiwName = "/offer/offer";
		}

		$sql ="select sub.id,sub.vendor_id,sub.job_id,offer.id as offer_id,offer.valid_till_date as expiry_date,

			can.first_name,can.last_name,can.id as c_id,offer.rates_regular,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,offer.workorder_status,offer.status as offer_status,offer.payment_type

			FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and offer.workorder_status=1 and offer.client_id=".$loginUserId." and sub.id = offer.submission_id ".$jobID;

		$count_query = "select count(*) FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor

			WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and offer.workorder_status=1 and offer.client_id=".$loginUserId." and sub.id = offer.submission_id ".$jobID;

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$offers = $dataProvider->getData();
		//$offers = Yii::app()->db->createCommand($sql)->query()->readAll();

		$this->render($veiwName,array('model'=>$jobmodel,'offers'=>$offers,'pages'=>$dataProvider->pagination));
	}
	
	public function actionSelectetemplate(){
		
		if(isset($_POST['templet_id'])){
			 if(isset(Yii::app()->session['template_id'])){
				unset(Yii::app()->session['template_id']);
			 }
			 Yii::app()->session['template_id'] = $_POST['templet_id'];
			 }
		 
	 $model = new Job;
	 $jobTempelatemodel = JobTemplates::model()->findByPk(Yii::app()->session['template_id']);
	 $this->render('selecttempelate',array('model'=>$model,'jobTempelatemodel'=>$jobTempelatemodel));
	}
	 
	 
	public function actionInterviewCompleted() 
	{ 
		$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']); 
		$submissionData->form_submitted = 0; 
		$submissionData->save(false); 
				 
		$interview = Interview::model()->findByPk($_GET['interviewId']); 
		$interview->status = 5; 
		if($interview->save(false)); 
		 
		$this->redirect(array('interview/allinterview','jobid'=>$_GET['id'],'type'=>'interview','st'=>'all')); 
		} 
	 
	public function actionOffers(){ 
		error_reporting(0); 
		$id =  $_GET['id']; 
		$jobmodel = Job::model()->findByPk($id); 
		 
		$sql ="select sub.id,sub.job_id,offer.id as offer_id,offer.valid_till_date as expiry_date, 
		 
		can.first_name,can.last_name,can.id as c_id,offer.rates_regular,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate,offer.status as offer_status,offer.workorder_status 
		 
		FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor 
		 
		WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and sub.id = offer.submission_id and offer.workorder_status=1 and  sub.job_id=".$jobmodel->id;   
		 
		 
		$offers = Yii::app()->db->createCommand($sql)->query()->readAll(); 
		 
		$this->render('offers',array('model'=>$jobmodel,'offers'=>$offers)); 
	} 
	 
	public function actionWorker(){ 
		error_reporting(0); 
		$id =  $_GET['id']; 
		$jobmodel = Job::model()->findByPk($id); 
		 
		 //removing workorder status from this query
		/*$sql ="select sub.id,sub.job_id,offer.id as offer_id,offer.valid_till_date as expiry_date, 
		 
		can.first_name,can.last_name,can.id as c_id,offer.rates_regular,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate as before_rate, offer.rates_regular as after_rate,offer.status as offer_status,offer.workorder_status 
		 
		FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor 
		 
		WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and sub.id = offer.submission_id and offer.workorder_status=1 and  sub.job_id=".$jobmodel->id; */ 
		
		$sql ="select sub.id,sub.job_id,offer.id as offer_id,offer.valid_till_date as expiry_date, 
		can.first_name,can.last_name,can.id as c_id,offer.rates_regular,vendor.organization as v_organization,sub.estimate_start_date,sub.candidate_pay_rate as before_rate, offer.rates_regular as after_rate,offer.status as offer_status,offer.workorder_status 
		 
		FROM vms_vendor_job_submission sub,vms_candidates as can,vms_offer as offer,vms_vendor as vendor 
		 
		WHERE sub.candidate_Id =can.id and sub.vendor_id=vendor.id and sub.id = offer.submission_id and sub.job_id=".$jobmodel->id; 
		 
		 
		$offers = Yii::app()->db->createCommand($sql)->query()->readAll(); 
		 
		$this->render('worker',array('model'=>$jobmodel,'offers'=>$offers)); 
	} 
	 
	public function actionPayrates(){
		
		$categoryID = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$_POST['cat_value'])); 
		
		//Administrative/Clerical Jobs category id 71
		//client id 41
		$rateData = VendorClientMarkup::model()->findAllByAttributes(array('client_id'=>Yii::app()->user->id,'category_id'=>$categoryID->id),
			array(
				'order' => 'mark_up desc', 
				'limit' => '1' 
			));
		if($rateData){
			$mark_up = $rateData[0]->mark_up;
		}else{
			$mark_up = 0;
		}
		//echo $mark_up;



		$categoryConfiguration = CategoryConfiguration::model()->findByAttributes(array('client_id'=>Yii::app()->user->id,'category_id'=>$_POST['cat_value']));


		$costCenter = '';
		if($categoryConfiguration){
		$costCode = explode(',',$categoryConfiguration->cost_center);
		foreach($costCode as $value){
			if($value) {
				$costCenter .= '<option value="' . $value . '">' . $value . '</option>';
			}
		}
		}else{
			$costCenter .= '<option value=""> </option>';
		}
		$response = array();

		$response['markup'] = $mark_up;
		$response['costcenter'] = $costCenter;

		echo json_encode($response);
	}

	public function actionWorkflowConfiguration(){

		if(isset($_POST['estimate_cost']) && isset($_POST['cat_id'])) {

			$sql = 'select * from vms_workflow_configuration where category="' . $_POST['cat_id'] . '" and client_id="' . Yii::app()->user->id . '"  and start_rang <="' . $_POST['estimate_cost'] . '"  and end_rang >="' . $_POST['estimate_cost'] . '"';

			$result = Yii::app()->db->createCommand($sql)->queryRow();
			if ($result) {
				$workflowmember = WorklflowMember::model()->findByAttributes(array('workflow_id' => $result['workflow_id']));
				$workflow = Worklflow::model()->findByPk($result['workflow_id']);
				$clientdata = Client::model()->findByPk($workflowmember->client_id);

				?>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<div class="form-group">
						<label for="">Job Work flow approval.</label>
						<select name="Job[work_flow]" class="form-control" readonly="readonly">
							<option value='<?php echo $workflow->id; ?>'><?php echo $workflow->flow_name; ?></option>
						</select>
					</div>

					<p class="m-t-10 m-b-10">Following are the Member for Job Approval</p>

					<table class="table table-striped table-hover table-white">
						<thead>
						<tr>
							<th>Approval</th>
							<th>Name</th>
							<th>Department</th>
							<th>Work Flow Order</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td><?php echo $workflow->flow_name; ?></td>
							<td><?php echo $clientdata->first_name . ' ' . $clientdata->last_name; ?></td>
							<td><?php echo $workflowmember->department_id; ?></td>
							<td><?php echo $workflowmember->order_approve; ?></td>
						</tr>
						</tbody>
					</table>
				</div>

			<?php }else{ ?>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="form-group" >
            <div class="well well-yellow">
				<p>Sorry there is no workflow according to this estimated cost</p>
            </div>
            </div>
            </div>
			<?php }
		}
	}
	
	 
	public function actionLocations($id){ 
		$this->layout = 'admin'; 
		$model = $this->loadModel($id); 
		$this->render('location',array('model'=>$model)); 
	} 
	 
	public function actionWaiting(){ 
		  
		$this->layout = 'admin'; 
		$loginUserId = Yii::app()->user->id; 
		//$TeammemberJob = TeammemberJob::model()->findAllByAttributes(array('teammember_id'=>$loginUserId)); 
		$JobWorkflow = JobWorkflow::model()->findAllByAttributes(array('client_id'=>$loginUserId,'job_status'=>'Pending')); 
		 
		$jobIds = array(); 
		foreach($JobWorkflow as $key=>$value){ 
			$jobIds[] = $value->job_id; 
			} 
		 
		$criteria = new CDbCriteria(); 
		//$criteria->addInCondition("id", $jobIds); 
		$criteria->addInCondition("id", $jobIds); 
		$jobs = Job::model()->findAll($criteria); 
		 
		/*echo '<pre>'; 
		print_r($jobIds); 
		exit;*/ 
		 
		$this->render('joblisting',array('loginUserId'=>$loginUserId ,'jobs'=>$jobs)); 
		} 
	 
	 
	public function actionScheduleStatus() 
	{ 
		$this->layout = 'admin'; 
		$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']); 
		$interViewModel = Interview::model()->findByPk($_GET['interviewId']); 
		$jobmodel = Job::model()->findByPk($_GET['id']); 
		 
		$submissionData->form_submitted = 0; 
		$submissionData->save(false); 
		 
		if(isset($_POST['saveInterviewStatus'])){ 
			/*echo '<pre>'; 
			print_r($_POST); 
			exit;*/ 
			$interViewModel->attributes = $_POST['Interview']; 
			$interViewModel->status = 3; 
			$interViewModel->submission_id = $_GET['submission-id']; 
			$interViewModel->created_by_id = Yii::app()->user->id; 
			$interViewModel->created_by_type = 'Client'; 
			if($interViewModel->save(false)){				 
				$sql ="select vms_client.* FROM  vms_client,vms_teammember_job  WHERE vms_client.`id` =vms_teammember_job.teammember_id and vms_teammember_job.job_id=".$_GET['id']; 
			 
			$allMemeber = Yii::app()->db->createCommand($sql)->query()->readAll(); 
			 
			if($allMemeber) { 
				foreach($allMemeber as $value){ 
				$ut = new UtilityManager; 
					//$ut->jobNotification($value['email'],$jobmodel->title); 
				} 
			} 
			 
			$vendorJobSubmission = VendorJobSubmission::model()->findAll(array('condition'=>'id='.$_GET['submission-id'])); 
			if($vendorJobSubmission){ 
				foreach($vendorJobSubmission as $value){ 
					$vendor = Vendor::model()->findByPk($value->vendor_id); 
					//UtilityManager::jobNotification($vendor->email,$jobmodel->title); 
				} 
			} 
			$this->redirect(array('interview/allinterview','jobid'=>$jobmodel->id,'type'=>'interview','st'=>'all')); 
			} 
			 
		} 
		//error_reporting(0); 
		$id = 	$_GET['id']; 
		 
		$jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id)); 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['id'])->query()->readAll(); 
		 
		$this->render('scheduleStatus',array('interViewModel'=>$interViewModel,'submissionData'=>$submissionData,'jobmodel'=>$jobmodel,'invitedTeamMembers'=>$invitedTeamMembers)); 
	}
	 
	public function actionSubmissionView(){ 
		$id = $_GET['id']; 
		$vendorSubmission = VendorJobSubmission::model()->findByPk($id); 
		$candidateModel = Candidates::model()->findByPk($vendorSubmission->candidate_Id); 
		$vendor = Vendor::model()->findByAttributes(array('id'=>$vendorSubmission->vendor_id)); 
		
		$vendorTeam = Vendor::model()->findByAttributes(array('id'=>$candidateModel->source)); 
		 
		 
		$jobModel=Job::model()->findByPk($vendorSubmission->job_id); 
		$candidatesProfile = CandidatesProfile::model()->findByAttributes(array('candidate_id'=>$candidateModel->id)); 
		if($vendorSubmission->resume_status == 1){ 
			$vendorSubmission->resume_status = 2; 
			$vendorSubmission->save(false); 
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Status!</strong> MSP Review.</div>'); 
		} 
		if(isset($_POST['VendorJobSubmission'])){ 
			$vendorSubmission->resume_status = $_POST['VendorJobSubmission']['resume_status']; 
			$vendorSubmission->save(false); 
			$status = UtilityManager::resumeStatus(); 
			 
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Status!</strong> '.$status[$vendorSubmission->resume_status].'.</div>'); 
		} 
		$this->render('submission_view',array('vendorSubmission'=>$vendorSubmission,'vendor'=> $vendor,'vendorTeam'=>$vendorTeam,'candidateModel'=>$candidateModel,'jobModel'=>$jobModel,'candidatesProfile'=>$candidatesProfile)); 
	} 
	//having some issue of bracket in this clone action (need testing) 
	//Job cloning 
	public function actionClone($id) 
	{ 
		$model=$this->loadModel($id); 
		$modelNew = new Job; 
		 
		//$this->layout = 'job'; 
		$this->layout = 'admin'; 
		 
		// Uncomment the following line if AJAX validation is needed 
		// $this->performAjaxValidation($model); 
		if(isset($_POST['Job'])) 
		{ 
			$modelNew->attributes=$_POST['Job']; 
			$modelNew->parent_id = $_GET['id']; 
			 
			$modelNew->user_id = Yii::app()->user->id; 
			$modelNew->user_type = 'Client'; 
			 
			$modelNew->job_po_duration = $_POST['job_po_duration']; 
			$modelNew->desired_start_date = $_POST['desired_start_date']; 
			 
			//old skills 
			$skill = Skills::model()->findAll(); 
			$OldSkills = array(); 
			foreach($skill as $skill){ 
				$OldSkills[] = $skill->name; 
				} 
			 
			if($_POST['Job']['skills']) { 
				$SkillsAddition = explode(",",$_POST['Job']['skills']); 
				$model->skills = str_replace(' ', '', $_POST['Job']['skills']); 
		    }else { 
		    	$model->skills = ''; 
		    } 
			 
			 
			$modelNew->location = serialize($_POST['Job']['location']); 
			 
			$modelNew->invite_team_member = ''; 
			//$modelNew->request_dept = serialize($_POST['Job']['request_dept']);
			 
			 
			 
			 
			if($modelNew->save(false)){ 
				 
				// Invite teammeber to this job 
				if(isset($_POST['Job']['invite_team_member'])) { 
					if(!empty($_POST['Job']['invite_team_member'][0])){ 
						$inviteteammember = $_POST['Job']['invite_team_member']; 
						foreach($inviteteammember as $value) { 
							if(Yii::app()->db->createCommand('insert into vms_teammember_job set teammember_id='.$value.' , job_id='.$modelNew->primaryKey.', date_created="'.date('Y-m-d').'"')->execute()) 
							{ 
								//$this->clientEmail(Yii::app()->db->getLastInsertID()); 
								//Yii::app()->user->setFlash('success', "Team member invited successfully"); 
							} 
						} 
					} 
				} 
				 
				if($_POST['Job']['skills']) { 
					foreach($SkillsAddition as $skillVal){ 
					$skillModel = new Skills; 
						if (!in_array(str_replace(' ', '', $skillVal), $OldSkills)) { 
							$skillModel->name =  $skillVal; 
							$skillModel->save(false); 
						} 
						 
					} 
			   } 
				 
			 
			 $WorklflowMember = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_POST['Job']['work_flow']), 
					array( 
						'order' => 'order_approve asc' 
					)); 
					 
				$WorklflowMemberLimit = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_POST['Job']['work_flow']), 
						array( 
							'order' => 'order_approve asc', 
							'limit' => 1, 
						)); 
				 
				$workflowID =  $_POST['Job']['work_flow']; 
				 
				foreach($WorklflowMember as $keyFlow=>$valueFlow){ 
					$JobWorkflow = new JobWorkflow; 
					$JobWorkflow->job_id = $model->id; 
					$JobWorkflow->workflow_id = $_POST['Job']['work_flow']; 
					$JobWorkflow->client_id = $valueFlow->client_id; 
					$JobWorkflow->number_of_approval = $valueFlow->order_approve; 
					$JobWorkflow->job_status = 'Pending'; 
					$JobWorkflow->email_sent = 0; 
					 
					$JobWorkflow->save(false); 
					} 
					 
				$clientData = Client::model()->findByPk($WorklflowMemberLimit[0]['client_id']); 
				 
				$JobWorkflowEmail = JobWorkflow::model()->findAllByAttributes(array('number_of_approval'=>1,'workflow_id'=>$_POST['Job']['work_flow'])); 
				 
				 
				 
				 
				 
				 
				$htmlbody='<div align="center"> 
				<table border="0" cellspacing="0" cellpadding="0" width="650" style="background:white;border:solid #e0e0e0 1.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt"> 
				  <tbody> 
					<tr> 
					  <br valign="top"><h1>Hi,<u></u><u></u></h1> 
					  <p>Thanks!</p></br> 
						<p>You have been invited to this job:: '.Yii::app()->createAbsoluteUrl('Client/job/workflowStatus', 
						array('workflowid'=>$workflowID,'jobid'=>$modelNew->id,'clientid'=>$WorklflowMemberLimit[0]['client_id'])).'<br /> 
						<p>Please do not reply to this email. If you are receiving this email in error, you can safely ignore it.</p></br> 
						<p>The US Tec Solution team</p></br> 
						<p></p> 
						</td> 
					</tr> 
				  </tbody> 
				</table> 
			  </div> '; 
		 
		 
		 
			$message = Yii::app()->sendgrid->createEmail(); 
			 //shortcut to $message=new YiiSendGridMail($viewsPath); 
			$message->setHtml($htmlbody) 
			->setSubject('Simplifyvms - Job Approval Invitation') 
			->addTo($clientData->email) 
			->setFrom('alert@simplifyvms.net'); 
				 
				 
				 
				if(Yii::app()->sendgrid->send($message)){ 
					$JobWorkflowEmail[0]->email_sent = 1; 
					$JobWorkflowEmail[0]->save(false); 
					} 
					 
			    
			    
			    
			    
			    
			   Yii::app()->user->setFlash('success', '<div class="card m-b-20 success-message"> 
				<p>Your Job Posting was successfully submitted. After review it would be published</p> 
			</div> '); 
				//$this->redirect(array('joblisting')); 
				$this->redirect(array('joblisting')); 
			}else{ 
				 Yii::app()->user->setFlash('success', '<div class="card m-b-20 error-message"> 
				<p>Your Job Posting was not successfully submitted.</p> 
			</div> '); 
				} 
				 
				 
				 
				/*$this->redirect(array('view','id'=>$modelNew ->id));*/ 
			} 
			 
			/*echo '<pre>'; 
			print_r($model); 
			exit;*/ 
		$this->render('update',array( 
			'model'=>$model, 
		)); 
	} 
	/** 
	 * Displays a particular model. 
	 * @param integer $id the ID of the model to be displayed 
	 */ 
	 
	public function actionView($id) 
	{ 
		$model = $this->loadModel($id); 
		 
		error_reporting(0); 
		// Invite teammeber to this job 
		if(!empty($_POST['inviteteammember'][0])) { 
			 
			 
			$inviteteammember = $_POST['inviteteammember']; 
			foreach($inviteteammember as $value) { 
				if(Yii::app()->db->createCommand('insert into vms_teammember_job set teammember_id='.$value.' , job_id='.$id.', date_created="'.date('Y-m-d').'"')->execute()) 
				{ 
					$this->clientEmail(Yii::app()->db->getLastInsertID()); 
					Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Success!</strong> Team member invited successfully.</div>'); 
				} 
		    } 
		} 
        // 
       $invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$id)->query()->readAll(); 
        // 
		$client = Client::model()->findByPk(Yii::app()->user->id); 
		if( $client->member_type == 'Account Approver'){ 
			$allTeamMember = Client::model()->findAll(array('condition'=>'super_client_id='.$client->super_client_id.' and profile_approve ="Yes" and 	profile_status=1 ')); 
		}else if(empty($client->member_type )){ 
			$allTeamMember = Client::model()->findAll(array('condition'=>'super_client_id='.$client->id.' and profile_approve ="Yes" and 	profile_status=1 ')); 
		} 
		//checking weather this job is allowed for user or not 
		$OldMembers = ''; 
		$loginUserId = Yii::app()->user->id; 
		 
		$alreadyInvitedMembers = TeammemberJob::model()->findAllByAttributes(array('job_id'=>$_GET['id'])); 
		foreach($alreadyInvitedMembers as $alreadyInvitedMembers){ 
			$OldMembers[] = $alreadyInvitedMembers['teammember_id']; 
			} 
			 
		if($model->user_id == $loginUserId){ 
		$this->render('view',array('model'=>$model,'allTeamMember'=>$allTeamMember,'invitedTeamMembers'=>$invitedTeamMembers)); 
		}else if(in_array($loginUserId,$OldMembers)){ 
			$this->render('view',array('model'=>$model,'allTeamMember'=>$allTeamMember,'invitedTeamMembers'=>$invitedTeamMembers)); 
			}else{ 
			 $this->redirect(array('job/joblisting')); 
			 } 
		 
	} 
	
	public function clientEmail($id) { 
		 $info = Client::model()->findByPk($id); 
          $customerEmail = $info->email; 
          //$applicationUrl = $info->application_url; 
          $htmlbody='<div align="center"> 
        <table border="0" cellspacing="0" cellpadding="0" width="650" style="background:white;border:solid #e0e0e0 1.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt"> 
          <tbody> 
            <tr> 
              <br valign="top"><h1>Hi,<u></u><u></u></h1> 
              <p>Thanks!</p></br> 
                <p>You have been invited to this job:: '.Yii::app()->createAbsoluteUrl('Client/default/login').'<br /> 
                <p>Please do not reply to this email. If you are receiving this email in error, you can safely ignore it.</p></br> 
                <p>The US Tec Solution team</p></br> 
                <p></p> 
                </td> 
            </tr> 
          </tbody> 
        </table> 
      </div> '; 
            $message = Yii::app()->sendgrid->createEmail(); 
      //shortcut to $message=new YiiSendGridMail($viewsPath); 
            $message->setHtml($htmlbody) 
                ->setSubject('Simplifyvms - Job Invitation') 
                ->addTo($customerEmail) 
                ->setFrom('alert@simplifyvms.net'); 
            Yii::app()->sendgrid->send($message); 
   } 
	 
	public function actionJobListing() 
	{
		error_reporting(0);
		$this->layout = 'admin'; 
		$loginUserId = Yii::app()->user->id; 
		$modelClient=Client::model()->findByPk($loginUserId); 
		/*Account Approver*/ 
		//if($modelClient->member_type == 'Team Member' || $modelClient->member_type == 'Account Approver'){
		if($modelClient->member_type != NULL){
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id'; 
			//have a check if the second step is complete
			//$Criteria->condition = "vms_teammember_job.teammember_id =".$loginUserId;
			
			$Criteria->condition = "vms_teammember_job.teammember_id =".$loginUserId." and job.jobstep2_complete != 0";

			//$Criteria->order = "order by t.id desc"; 
			if(isset($_POST['s'])) { 
				$t = $_POST['t']; 
				$Criteria->condition = "t.title like '%".$t."%'"; 
			}

			$count=Job::model()->count($Criteria);
			$pages=new CPagination($count);

			// results per page
			$pages->pageSize=25;
			$pages->applyLimit($Criteria);
			$jobsInvitedFor=Job::model()->findAll($Criteria);

			//$jobsInvitedFor = Job::model()->findAll($Criteria);
			 
			$allJobIDs = array(); 
			foreach($jobsInvitedFor as $inviteKey=>$inviteValue){ 
				$allJobIDs[$inviteValue->id] = $inviteValue->id; 
			} 
			$worklflowJobs = WorklflowMember::model()->findAllByAttributes(array('client_id'=>$loginUserId)); 
			 
			foreach($worklflowJobs as $key=>$value){ 
				$JobIDs = Job::model()->findAllByAttributes(array('work_flow'=>$value->workflow_id)); 
				foreach($JobIDs as $JobIDs){ 
					$allJobIDs[$JobIDs->id] = $JobIDs->id; 
					} 
				} 
			 
			 
			$criteria = new CDbCriteria(); 
			$criteria->addInCondition("id", $allJobIDs); 
			$criteria->order = "id desc";
			$count=Job::model()->count($criteria);
			$pages=new CPagination($count);

			// results per page
			$pages->pageSize=25;
			$pages->applyLimit($criteria);
			$jobs=Job::model()->findAll($criteria);
			//$jobs = Job::model()->findAll($criteria);
			 
			 
		}else{ 
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			$Criteria->condition = "user_id=".$loginUserId." and jobstep2_complete != 0"; 
			$Criteria->order = "id desc"; 
			 
			if(isset($_POST['s'])) { 
				$t = $_POST['s']; 
				$Criteria->condition = "user_id=".$loginUserId." and t.title like '%".$t."%'"; 
			}
			$count=Job::model()->count($Criteria);
			$pages=new CPagination($count);

			// results per page
			$pages->pageSize=25;
			$pages->applyLimit($Criteria);
			$jobs=Job::model()->findAll($Criteria);

			//$jobs = Job::model()->findAll($Criteria);
					 
		 } 
		  
		 
		$this->render('joblisting',array('loginUserId'=>$loginUserId ,'jobs'=>$jobs,'pages'=>$pages));
	} 
	 
	public function actionJobListing1() 
	{ 
		$this->layout = 'admin'; 
		$loginUserId = Yii::app()->user->id; 
		$modelClient = Client::model()->findByPk($loginUserId); 
		/*Account Approver*/ 
		if($modelClient->member_type == 'Team Member' || $modelClient->member_type == 'Account Approver'){ 
			//changing the query for pending job  
			 
			/*$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id'; 
			$Criteria->condition = "vms_teammember_job.teammember_id =".$loginUserId." and t.status_posting_access= 'Waiting for Approval'";*/ 
			 
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			$Criteria->join = 'INNER JOIN vms_job_workflow ON vms_job_workflow.job_id = t.id'; 
			$Criteria->condition = "vms_job_workflow.client_id =".$loginUserId." and vms_job_workflow.job_status= 'Pending'";
			$Criteria->order = "id desc";
			$count=Job::model()->count($Criteria);
			$pages=new CPagination($count);

			// results per page
			$pages->pageSize=10;
			$pages->applyLimit($Criteria);
			if(isset($_POST['s'])) { 
				$t = $_POST['t']; 
				//making some urgent changes in below query
				//$Criteria->condition = "t.status_posting_access= '2' and t.title like '%".$t."%'";
				$Criteria->condition = "t.jobStatus= '2' and t.title like '%".$t."%'"; 
			} 
			$jobs = Job::model()->findAll($Criteria); 
			 
		}else{ 
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			//$Criteria->condition = "status_posting_access= 'Waiting for Approval'"; 
			$Criteria->condition = "jobStatus= '2' and user_id=".$loginUserId; 
			if(isset($_POST['s'])) { 
				$t = $_POST['t']; 
				$Criteria->condition = "jobStatus= '2' and user_id=".$loginUserId." and t.title like '%".$t."%'"; 
			}
			$Criteria->order = "id desc";
			$count=Job::model()->count($Criteria);
			$pages=new CPagination($count);

			// results per page
			$pages->pageSize=10;
			$pages->applyLimit($Criteria);
			$jobs = Job::model()->findAll($Criteria); 
					 
		 } 
		$this->render('joblisting',array('loginUserId'=>$loginUserId ,'jobs'=>$jobs,'pages'=>$pages));
	} 
	 
	public function actionArchivedJobs() 
	{ 
		$this->layout = 'admin'; 
		$loginUserId = Yii::app()->user->id; 
		$modelClient=Client::model()->findByPk($loginUserId); 
		/*Account Approver*/ 
		if($modelClient->member_type == 'Team Member' || $modelClient->member_type == 'Account Approver'){ 
			 
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id'; 
			//$Criteria->condition = "vms_teammember_job.teammember_id =".$loginUserId; 
			//condition by malik for approved jobs 
			$Criteria->condition = "vms_teammember_job.teammember_id =".$loginUserId." and t.jobStatus= '11'"; 
			if(isset($_POST['s'])) { 
				$t = $_POST['t']; 
				//$Criteria->condition = "t.title like '%".$s."%'"; 
				$Criteria->condition = "t.jobStatus= '11' and t.title like '%".$t."%'"; 
			} 
			$jobs = Job::model()->findAll($Criteria); 
			 
		}else{ 
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			//$Criteria->condition = "status_posting_access= 'Waiting for Approval'"; 
			$Criteria->condition = "jobStatus= '11' and user_id=".$loginUserId; 
			if(isset($_POST['s'])) { 
				$t = $_POST['t']; 
				$Criteria->condition = "jobStatus= '11' and user_id=".$loginUserId." and t.title like '%".$t."%'"; 
			} 
			$jobs = Job::model()->findAll($Criteria); 
					 
		 } 
		$this->render('joblisting',array('loginUserId'=>$loginUserId ,'jobs'=>$jobs)); 
	} 
	 
	public function actionClosedJobs() 
	{ 
		$this->layout = 'admin'; 
		$loginUserId = Yii::app()->user->id; 
		$modelClient = Client::model()->findByPk($loginUserId); 
		/*Account Approver*/ 
		if($modelClient->member_type == 'Team Member' || $modelClient->member_type == 'Account Approver'){ 
			 
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id'; 
			//$Criteria->condition = "vms_teammember_job.teammember_id =".$loginUserId; 
			//condition by malik for approved jobs 
			$Criteria->condition = "vms_teammember_job.teammember_id =".$loginUserId." and t.jobStatus= '7'"; 
			if(isset($_POST['s'])) { 
				$t = $_POST['t']; 
				//$Criteria->condition = "t.title like '%".$s."%'"; 
				$Criteria->condition = "t.jobStatus= '7' and t.title like '%".$t."%'"; 
			} 
			$jobs = Job::model()->findAll($Criteria); 
			 
		}else{ 
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			//$Criteria->condition = "status_posting_access= 'Waiting for Approval'"; 
			$Criteria->condition = "jobStatus= '7' and user_id=".$loginUserId; 
			if(isset($_POST['s'])) { 
				$t = $_POST['t']; 
				$Criteria->condition = "jobStatus= '7' and user_id=".$loginUserId." and t.title like '%".$t."%'"; 
			} 
			$jobs = Job::model()->findAll($Criteria); 
					 
		 } 
		$this->render('joblisting',array('loginUserId'=>$loginUserId ,'jobs'=>$jobs)); 
	} 
	 
	//THIS function is not purified so need to work on it Doing this work on urgent basis.  
	public function actionOverview($id) 
	{ 
		error_reporting(0); 
		$this->layout = 'admin'; 
		$OldMembers = ''; 
		$workFlowMembers = ''; 
		$loginUserId = Yii::app()->user->id; 
		$jobData = Job::model()->findByPk($_GET['id']);		 
			 
		$alreadyInvitedMembers = TeammemberJob::model()->findAllByAttributes(array('job_id'=>$_GET['id'])); 
		foreach($alreadyInvitedMembers as $alreadyInvitedMembers){ 
			$OldMembers[] = $alreadyInvitedMembers['teammember_id']; 
			} 
		 
		$workflowMembers = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$jobData->work_flow)); 
		foreach($workflowMembers as $workflowMembers1){ 
			$workFlowMembers[] = $workflowMembers1['client_id']; 
			}
		 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['id'])->query()->readAll(); 
		 
	if($jobData->user_id == $loginUserId){ 
	$this->render('jobview',array('loginUserId'=>$loginUserId ,'jobData'=>$jobData,'invitedTeamMembers'=>$invitedTeamMembers)); 
		}else if(in_array($loginUserId,$OldMembers) || in_array($loginUserId,$workFlowMembers)){ 
			$this->render('jobview',array('loginUserId'=>$loginUserId ,'jobData'=>$jobData,'invitedTeamMembers'=>$invitedTeamMembers)); 
			}else{ 
			 $this->redirect(array('job/joblisting')); 
			 }
	} 
	 
	public function actionMobJobListing() 
	{ 
		//$loginUserId = $_GET['client_id']; 
		$loginUserId = $_POST['client_id']; 
		//$loginUserId = Yii::app()->user->id; 
		$response = array(); 
		$json_data =  array(); 
		 
		$modelClient = Client::model()->findByPk($loginUserId); 
		if($modelClient->member_type == 'Team Member'){ 
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id'; 
			$Criteria->condition = "vms_teammember_job.teammember_id =".$loginUserId; 
			if(!empty($_POST['s'])) { 
				$s = $_POST['s']; 
				$Criteria->condition = "t.title like '%".$s."%'"; 
			} 
			$jobs = Job::model()->findAll($Criteria); 
		}else{  
			$Criteria = new CDbCriteria(); 
			$Criteria->select="*"; 
			$Criteria->condition = "user_id=".$loginUserId; 
			if(!empty($_POST['s'])) { 
				$s = $_POST['s']; 
				$Criteria->condition = "t.title like '%".$s."%'"; 
			} 
			 
			$jobs = Job::model()->findAll($Criteria);
			 
			foreach($jobs as $jobKey=>$jobsVal){ 
				$json_data[$jobKey]['id'] = $jobsVal->id; 
				$json_data[$jobKey]['title'] = $jobsVal->title; 
				$json_data[$jobKey]['user_id'] = $jobsVal->user_id; 
				$json_data[$jobKey]['cat_id'] = $jobsVal->cat_id; 
				$json_data[$jobKey]['skills'] = $jobsVal->skills; 
				$json_data[$jobKey]['description'] = $jobsVal->description; 
				$json_data[$jobKey]['rate'] = $jobsVal->rate; 
				$json_data[$jobKey]['currency'] = $jobsVal->currency; 
				$json_data[$jobKey]['payment_type'] = $jobsVal->payment_type; 
				} 		 
		 } 
		 
		$response["success"] = 1; 
		$response["jobs"] = $json_data; 
		echo json_encode($response); 
		exit; 
		  
		 
		//$this->render('joblisting',array('loginUserId'=>$loginUserId ,'jobs'=>$jobs)); 
	}
	 
	public function actionJobView() 
	{ 
		//$this->layout = 'job'; 
		$this->layout = 'admin'; 
		 
		$OldMembers = ''; 
		$loginUserId = Yii::app()->user->id; 
		$jobData = Job::model()->findByPk($_GET['id']); 
		 
		 
		if(isset($_POST['add_new_skills'])) { 
			$jobData->skills = $jobData->skills.','.$_POST['skill']; 
			if($jobData->save(false)){ 
				$this->redirect(array('job/jobview','id'=>$_GET['id'])); 
				} 
			} 
			 
		 
		 
		 
		$alreadyInvitedMembers = TeammemberJob::model()->findAllByAttributes(array('job_id'=>$_GET['id'])); 
		foreach($alreadyInvitedMembers as $alreadyInvitedMembers){ 
			$OldMembers[] = $alreadyInvitedMembers['teammember_id']; 
			} 
			 
		 
		 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['id'])->query()->readAll(); 
		 
	if($jobData->user_id == $loginUserId){ 
	$this->render('jobview',array('loginUserId'=>$loginUserId ,'jobData'=>$jobData,'invitedTeamMembers'=>$invitedTeamMembers)); 
		}else if(in_array($loginUserId,$OldMembers)){ 
			$this->render('jobview',array('loginUserId'=>$loginUserId ,'jobData'=>$jobData,'invitedTeamMembers'=>$invitedTeamMembers)); 
			}else{ 
			 $this->redirect(array('job/joblisting')); 
			 } 
	} 
		
	public function actionJobsubmission(){ 
		error_reporting(0); 
		//$this->layout = 'job'; 
		$this->layout = 'admin'; 
		$id = 	$_GET['id']; 
		 
		$jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id)); 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['id'])->query()->readAll(); 
		if(!$jobmodel) 
			$this->redirect(array('job/joblisting')); 
		 
			 
		$sql ="SELECT vms_vendor_job_submission.id as s_id ,vms_vendor_job_submission.resume_status as j_status,vms_vendor_job_submission.estimate_start_date as start_date,vms_vendor_job_submission.date_created,
		vms_vendor_job_submission.candidate_pay_rate as job_pay,vms_vendor.first_name as v_f_name, vms_vendor.last_name as v_l_name,vms_candidates.* FROM  vms_vendor,vms_candidates,vms_vendor_job_submission WHERE  
			vms_vendor_job_submission.resume_status IN('2','3','4','5','6','7') and 
		     vms_vendor_job_submission.`job_id` =".$id." and vms_vendor_job_submission.vendor_id = vms_vendor.id and vms_vendor_job_submission.candidate_Id =vms_candidates.id";
			 
			 
		$submittedUsers = Yii::app()->db->createCommand($sql)->query()->readAll(); 
		 
		$query = 'SELECT * from vms_vendor_job_submission where job_id='.$id.' and (resume_status != "1" and resume_status != "2" and( resume_status != "1" and rejected_type != "msp"))'; 
		$submissionData = Yii::app()->db->createCommand( $query )->query()->readAll(); 
		 
		$this->render('jobsubmission',array('jobmodel'=>$jobmodel,'submittedUsers'=>$submittedUsers,'invitedTeamMembers'=>$invitedTeamMembers,'submissionData'=>$submissionData)); 
	}
		
	public function actionViewResume(){ 
		//$this->layout = 'job'; 
		$this->layout = 'admin'; 
		error_reporting(0); 
		$id = 	$_GET['id']; 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['id'])->query()->readAll(); 
		$jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id)); 
		 
		 
		if(!$jobmodel){ 
			$this->redirect('job/joblisting'); 
		} 
		 
		if(isset($_POST['saveInterviewStatus'])){ 
			 $submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']); 
			 $submissionData->resume_status = $_POST['Interview']['status']; 
			  
			 $submissionData->save(false); 
			} 
		if(isset($_POST['resume_status'])){ 
		 $sql ="update vms_vendor_job_submission set resume_status='".$_POST['resume_status']."' where id =".$_GET['submission-id']; 
		$update = Yii::app()->db->createCommand($sql)->execute(); 
		if($update) { 
			$sql ="admin.* FROM  admin,vms_teammember_job  WHERE admin.`id` =staff_id and vms_teammember_job.job_id=".$id; 
			$allMemeber = Yii::app()->db->createCommand($sql)->query()->readAll(); 
			if($allMemeber) { 
				foreach($allMemeber as $value){ 
					UtilityManager::jobNotification($value['email'],$jobmodel->title); 
				} 
			} 
			 
			Yii::app()->user->setFlash('success', "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, labore."); 
				$this->redirect(array('job/submission','id'=>$id)); 
		} 
		} 
		 
		 
		$sql ="SELECT vms_vendor_job_submission.candidate_Id as c_id ,vms_vendor_job_submission.resume_status as j_status,vms_vendor_job_submission.start_date as v_start_date,vms_vendor_job_submission.rate as job_pay,vms_vendor.first_name as v_f_name, vms_vendor.last_name as v_l_name,vms_candidates.* FROM  vms_vendor,vms_candidates,vms_vendor_job_submission WHERE  
			vms_vendor_job_submission.resume_status ='3' and 
			vms_vendor_job_submission.`job_id` =".$id." and vms_vendor_job_submission.`id` =".$_GET['submission-id']." and vms_vendor_job_submission.vendor_id = vms_vendor.id and vms_vendor_job_submission.candidate_Id = vms_candidates.id"; 
			 
		$submittedUsers = Yii::app()->db->createCommand($sql)->query()->read(); 
		$this->render('viewresume',array('jobmodel'=>$jobmodel, 
			'submittedUsers'=>$submittedUsers,'invitedTeamMembers'=>$invitedTeamMembers)); 
	}
		
	public function actionInterviewStep1(){ 
		 
		$loginUserId = Yii::app()->user->id; 
		//$jobData = Job::model()->findAllByAttributes(array('user_id'=>$loginUserId,'jobStatus'=>'3'),array('order'=>' id desc'));
		$jobData = Job::model()->findAllByAttributes(array('user_id'=>$loginUserId,'jobStatus'=>array(3,6,11)),array('order'=>' id desc'));
		
		if(isset($_POST['first-step'])){
			$this->redirect(array('job/scheduleInterview','id'=>$_POST['job_id'],'submission-id'=>$_POST['submission_id']));
			}
		
		$this->render('interviewStep1',array('jobData'=>$jobData));
	}
		
	/*public function actionUploadFeeds(){
		
		$targetPath = Yii::app()->basePath.'/../interviewFeedback/'; 
		
		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'];   
			//$targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //4
			$targetFile =  $targetPath. $_FILES['file']['name'];  //5
		
			move_uploaded_file($tempFile,$targetFile); //6
		
		}
	}*/
		 
	public function actionScheduleInterview(){ 
		 
		$this->layout = 'admin';
		$interViewModel= new Interview; 
		error_reporting(0); 
		 
		$id = 	$_GET['id']; 
		$submisionID = $_GET['submission-id']; 
		 
		$jobmodel = Job::model()->findByAttributes(array('id'=>$id,'user_id'=>Yii::app()->user->id)); 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['id'])->query()->readAll(); 
		if(!$jobmodel){ 
			$this->redirect(array('job/joblisting')); 
		} 
		$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']); 
		if(isset($_POST['resume_status'])){
						
			$submissionData->form_submitted = 1; 
			$submissionData->resume_status = 5; 
		 	 
		 
		if($submissionData->save(false)) {
			 
			$interViewModel->attributes = $_POST['interViewModel']; 
			$interViewModel->interview_type = $_POST['interViewModel']['interview_type'];
			//all invited teammembers
			$interViewModel->invite_team_member = implode(',', $_POST['interViewModel']['invite_team_member']); 
			 
			$interViewModel->interview_added_guests = $_POST['interViewModel']['interview_added_guests']; 
			$interViewModel->interview_start_date = date("Y-m-d", strtotime($_POST['interViewModel']['interview_start_date'])); 
			$interViewModel->interview_alt1_start_date = date("Y-m-d", strtotime($_POST['interViewModel']['interview_alt1_start_date'])); 
			$interViewModel->interview_alt2_start_date = date("Y-m-d", strtotime($_POST['interViewModel']['interview_alt2_start_date'])); 
			$interViewModel->interview_alt3_start_date = date("Y-m-d", strtotime($_POST['interViewModel']['interview_alt3_start_date'])); 
			 
			 
			$interViewModel->submission_id = $submissionData->id; 
			$interViewModel->status = $_POST['interViewModel']['interview_status']; 
			 
			$rnd = rand(0,9999); 
   			$uploadedFile=CUploadedFile::getInstance($interViewModel,'interview_attachment'); 
			 
   			if($uploadedFile != "") { 
				$fileName = "{$rnd}-{$uploadedFile}";  // random number + file name 
				$interViewModel->interview_attachment = $fileName; 
				} 
			 
			if($interViewModel->save(false)){ 
				if($uploadedFile != ""){ 
				 $uploadedFile->saveAs(Yii::app()->basePath.'/../submissionattachment/'.$interViewModel->interview_attachment); 
   				 } 
				} 
			
			
			//work done on email templates 
			
			$vendorJobSubmission = VendorJobSubmission::model()->findAll(array('condition'=>'id='.$_GET['submission-id'])); 
			if($vendorJobSubmission){ 
				foreach($vendorJobSubmission as $value){ 
					$vendor = Vendor::model()->findByPk($value->vendor_id); 
					//UtilityManager::jobNotification($vendor->email,$jobmodel->title); 
					UtilityManager::interviewNotification($vendor->email,$submisionID,$id,$interViewModel->id);
				} 
			} 
			
			//mail to the all members
			//$sql ="admin.* FROM  admin,vms_teammember_job  WHERE admin.`id` = staff_id and vms_teammember_job.job_id=".$id; 	
			$sql ="select vms_client.* FROM  vms_client,vms_teammember_job  WHERE vms_client.`id` =vms_teammember_job.teammember_id and vms_teammember_job.job_id=".$id; 
			$allMemeber = Yii::app()->db->createCommand($sql)->query()->readAll(); 
			
			/*echo '<pre>';
			print_r($allMemeber);
			exit;*/
			
			if($allMemeber) { 
				foreach($allMemeber as $value){ 
					UtilityManager::interviewNotification($value['email'],$submisionID,$id,$interViewModel->id);
				} 
			} 
			
			if($interViewModel->interview_added_guests){
				$guests = explode(",",$interViewModel->interview_added_guests);
					foreach($guests as $k=>$val){
						UtilityManager::interviewGuestNotification($val,$submisionID,$id,$interViewModel->id);
					}
				}
			
			 
			 
		  Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Success!</strong> Your Interview Posting was successfully submitted.</div>'); 
		     
			$this->redirect(array('job/scheduleInterview','id'=>$id,'submission-id'=>$submisionID)); 
			//$this->redirect(array('job/scheduleInterview','id'=>$id)); 
		} 
		} 
		
		if(isset($_POST['feedback'])){
			
			/*echo '<pre>';
			print_r($_FILES);
			//print_r($_POST);
			exit;*/
			
			$interviewSingle = Interview::model()->findByPk($_GET['interviewId']);
			$interviewSingle->attributes = $_POST['Interview'];
			$interviewSingle->feedback_status = 1;
			$interviewSingle->save(false);
			/*echo '<pre>';
			print_r($_POST);
			exit;*/
			}
			
		if(isset($_POST['Notes'])){

		   $LoginUserId = Yii::app()->user->id;
		   //$vendorJobSubmission = VendorJobSubmission::model()->findAll(array('condition'=>'job_id='.$_GET['id']));
		   $invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and team_jobs.job_id='.$_GET['id'])->queryRow();

		   if($invitedTeamMembers['id']==$LoginUserId || $invitedTeamMembers['super_client_id']==$LoginUserId) {
			$model = new InterviewComments;
			$model->interview_id = $_GET['interviewId'];
			$model->submission_id = $_GET['submission-id'];
			$model->job_id = $_GET['id'];
			$model->user_id = $LoginUserId;
			$model->user_type = $invitedTeamMembers['type'];
			$model->user_name = $invitedTeamMembers['first_name'];
			$model->comments = $_POST['InterviewComments']['comments'];
			$model->posted_date = date('Y-m-d');
			$rnd = rand(0, 9999);
			$uploadedFile = CUploadedFile::getInstance($model, 'file_name');
			if ($uploadedFile != "") {
			 $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
			 $model->file_name = $fileName;
			}
			if ($model->save(false)) {

			 if ($uploadedFile != "") {
			  $uploadedFile->saveAs(Yii::app()->basePath . '/../submissionattachment/' . $model->file_name);
			 }
			}
		   }
		
		
		
		
		  }
		 
		$interviewData = Interview::model()->findByAttributes(array('submission_id'=>$_GET['submission-id']), 
		array('order'=>'id DESC')); 
		
		$this->render('scheduleInterview',array('interViewModel'=>$interViewModel,'jobmodel'=>$jobmodel, 
			'invitedTeamMembers'=>$invitedTeamMembers,'submissionData'=>$submissionData,'interviewData'=>$interviewData)); 
	} 
	
	public function actionDownloadResum($id){

		  $model = InterviewComments::model()->findByPk($id);
		
		  $baseUrl = Yii::app()->getBaseUrl(true);
		  if (strstr($baseUrl, 'local')) {
		   $resume = Yii::app()->basePath.'/../submissionattachment/';
		  }else {
		   $resume = '/home/biwwwsimplifyvms/public_html/billrate/submissionattachment/';
		  }
		
		  /*if(!$model->source)
				 $resume = Yii::app()->basePath.'/../candidate_resumes/';
				else
		
				$resume = '/home/wwwinfivmsnity/public_html/vendor/candidate_resumes/';*/
		
		
		  $file = $resume.$model->file_name;
		
		  if ($model) {
		   header('Content-Description: File Transfer');
		   header('Content-Type: application/octet-stream');
		   header('Content-Disposition: attachment; filename="'.basename($file).'"');
		   header('Expires: 0');
		   header('Cache-Control: must-revalidate');
		   header('Pragma: public');
		   header('Content-Length: ' . filesize($file));
		   readfile($file);
		   exit;
		  }
		 }
		
	public function actionMobJobView() 
	{ 
		//$this->layout = 'job'; 
		$this->layout = 'admin'; 
		 
		$OldMembers = ''; 
		$response = array(); 
		$loginUserId = $_POST['client_id']; 
		//$loginUserId = Yii::app()->user->id; 
		$jobData = Job::model()->findByPk($_POST['id']); 
		 
		 
		/*if(isset($_POST['add_new_skills'])) { 
			$jobData->skills = $jobData->skills.','.$_POST['skill']; 
			if($jobData->save(false)){ 
				$this->redirect(array('job/jobview','id'=>$_POST['id'])); 
				} 
			}*/ 
		 
		 
		 
		$alreadyInvitedMembers = TeammemberJob::model()->findAllByAttributes(array('job_id'=>$_POST['id'])); 
		foreach($alreadyInvitedMembers as $alreadyInvitedMembers){ 
			$OldMembers[] = $alreadyInvitedMembers['teammember_id']; 
			} 
			 
		 
		 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_POST['id'])->query()->readAll(); 
		 
		 
		if($jobData){ 
				$json_data['id'] = $jobData->id; 
				$json_data['title'] = $jobData->title; 
				$json_data['user_id'] = $jobData->user_id; 
				$json_data['cat_id'] = $jobData->cat_id; 
				$json_data['skills'] = $jobData->skills; 
				$json_data['description'] = $jobData->description; 
				$json_data['rate'] = $jobData->rate; 
				$json_data['currency'] = $jobData->currency; 
				$json_data['payment_type'] = $jobData->payment_type; 
				} 
		 
		 
	if($jobData->user_id == $loginUserId){ 
		 
		//job details api 
		$response["success"] = 1; 
		$response["jobs"] = $json_data; 
		echo json_encode($response); 
		exit;  
		}else if(in_array($loginUserId,$OldMembers)){ 
			//job details api 
			$response["success"] = 1; 
			$response["jobs"] = $json_data; 
			echo json_encode($response); 
			exit; 
			 
			}else{ 
			 $this->redirect(array('job/joblisting')); 
			 } 
	} 
	/** 
	 * Creates a new model. 
	 * If creation is successful, the browser will be redirected to the 'view' page. 
	 */ 
	  
	public function actionAddjob() 
	 { 
	  if(isset($_POST['new_job'])){
	   if (preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $_POST['new_job_title']))
	   {
		Yii::app()->user->setFlash('success', '<div class="alert alert-danger">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <strong>Error!</strong> Your Job Title must be letter</div>');
		$this->redirect(array('job/addjob'));
	   }
	   Yii::app()->session['job_title'] = $_POST['new_job_title'];
	   //$this->redirect(array('job/create'));
	   $this->redirect(array('job/jobStep1'));
	   }
	  if(isset(Yii::app()->session['job_title'])){
	  unset(Yii::app()->session['job_title']);
	  }
	  
	  
	  if(isset(Yii::app()->session['job_id'])){
		  unset(Yii::app()->session['job_id']);
	  }
	  $this->render('addjob');
	 }
	 
	
	///////////////////working on new job design //////////////////
	 
	public function actionJobStep1(){
		 
		$client = Client::model()->findByPk(Yii::app()->user->id); 
		if($client->member_type == 'Team Member'){ 
			$this->redirect(array('job/jobListing')); 
		}
		if(isset(Yii::app()->session['job_id'])){
			$model = Job::model()->findByPk(Yii::app()->session['job_id']);
		}else{
		$model=new Job;
		}
		$this->layout = 'admin'; 
		if(isset($_POST['Job'])) 
		{
			$model->attributes=$_POST['Job']; 
			 
			$model->user_id = Yii::app()->user->id; 
			$model->user_type = 'Client';
			$model->job_po_duration = $_POST['job_po_duration'];
			$model->job_po_duration_endDate = $_POST['job_po_duration_endDate']; 
			$model->desired_start_date = $_POST['desired_start_date'];
			$model->markup = $_POST['Job']['vendor_client_markup'];
			$model->date_created = date('Y-m-d');
			
			//if template is selected then storing its id
			if(isset($_POST['template_id'])){
				$model->template_id = $_POST['template_id'];
			}
			 
			//old skills 
			$skill = Skills::model()->findAll(); 
			$OldSkills = array(); 
			foreach($skill as $skill){ 
				$OldSkills[] = strtolower($skill->name); 
				} 
			 
			if($_POST['Job']['skills']) {
				$SkillsAddition = $_POST['Job']['skills'];  
				$model->skills = implode(',',$_POST['Job']['skills']);
		    }else { 
		    	$model->skills = ''; 
		    } 
			 
			if($_POST['Job']['skills1']) { 
				$SkillsAddition1 = $_POST['Job']['skills1'];
				$model->skills1 = implode(',',$_POST['Job']['skills1']);
		    }else { 
		    	$model->skills1 = ''; 
		    } 
			 
			if($_POST['Job']['skills2']) {  
				$SkillsAddition2 = $_POST['Job']['skills2']; 
				$model->skills2 = implode(',',$_POST['Job']['skills2']);
		    }else { 
		    	$model->skills2 = ''; 
		    } 
			 
			 if(isset($_POST['Job']['location'])){
			$model->location = serialize($_POST['Job']['location']);
			 }
			//doing this becuase teamMembers will be multiple.
			if(isset($_POST['Job']['invite_team_member'])) {
				$model->invite_team_member = serialize($_POST['Job']['invite_team_member']);
			}
			//$model->request_dept = $_POST['Job']['request_dept'];
			 
			if($model->save(false)){ 
			
				$history = new JobHistory;
				$history->client_id = $client->id;
				$history->job_id = $model->id;
				$history->operation = 'added Job';
				$history->job_status = 'Pending';
				$history->description = $client->first_name . '  added Job  Title '.$model->title;
				$history->save(false);
			
				// Invite teammeber to this job 
				if(isset($_POST['Job']['invite_team_member'])) { 
					if(!empty($_POST['Job']['invite_team_member'][0])){ 
						$inviteteammember = $_POST['Job']['invite_team_member']; 
						foreach($inviteteammember as $value) { 
							if(Yii::app()->db->createCommand('insert into vms_teammember_job set teammember_id='.$value.' , job_id='.$model->primaryKey.', date_created="'.date('Y-m-d').'"')->execute()) 
							{ 
							//$this->clientEmail(Yii::app()->db->getLastInsertID()); 
								//Yii::app()->user->setFlash('success', "Team member invited successfully"); 
							} 
						} 
					} 
				} 
				 
				//this code is to remove duplication of skills. 
				if($_POST['Job']['skills']) { 
					foreach($SkillsAddition as $skillVal){ 
					$skillModel = new Skills; 
						if (!in_array(str_replace(' ', '', strtolower($skillVal)), $OldSkills)) { 
							$skillModel->name =  $skillVal; 
							$skillModel->save(false); 
						} 
						 
					} 
			   } 
			    
			   if($_POST['Job']['skills1']) { 
					foreach($SkillsAddition1 as $skillVal1){ 
					$skillModel = new Skills; 
						if (!in_array(str_replace(' ', '', strtolower($skillVal1)), $OldSkills)) { 
							$skillModel->name =  $skillVal1; 
							$skillModel->save(false); 
						} 
						 
					} 
			   } 
			   if($_POST['Job']['skills2']) { 
					foreach($SkillsAddition2 as $skillVal2){ 
					$skillModel = new Skills; 
						if (!in_array(str_replace(' ', '', strtolower($skillVal2)), $OldSkills)) { 
							$skillModel->name =  $skillVal2; 
							$skillModel->save(false); 
						} 
						 
					} 
			   } 

				 
	// workflow approval template...
	$loc = unserialize($model->location);
	$locationsV = '';
				if($loc){
	foreach($loc as $val){
		$location = Location::model()->findByPk($val);
		$locationsV .= $location->name.', ';
		}
				}
		Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Success!</strong> Your Job Posting was successfully submitted. After review it would be published</div>'); 
				
				$this->redirect(array('jobstep2','id'=>$model->id));
				//$this->redirect(array($_SERVER['HTTP_REFERER'])); 
			}else{ 
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger"> 
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Error!</strong> Your Job Posting was not successfully submitted. </div>'); 
				} 
		} 
		$this->render('create',array ( 
			'model'=>$model, 
		)); 
	
		}

	public function actionJobstep2(){
		$client = Client::model()->findByPk(Yii::app()->user->id);
		Yii::app()->session['job_id'] = $_GET['id'];
		$model = Job::model()->findByPk($_GET['id']);

		//if(isset($_POST['Post_secondForm'])){
		if(isset($_POST['Post_secondForm']) || isset($_POST['save_draft']) || isset($_POST['publish_template'])){
			
			$model->attributes=$_POST['Job'];
			$model->markup = $_POST['Job']['vendor_client_markup'];
			$model->jobstep2_complete = 1;
			if($model->save(false)){
				
				/*if(isset($_POST['Post_secondForm'])){
					echo 'save job as pending approval';
					}*/
					
				if(isset($_POST['save_draft'])){
					$model->jobStatus = 2;
					$model->save(false);
					}
				//creating new template	
				if(isset($_POST['publish_template'])){
					$this->JobCreationTemplate($model);
					}
				
				unset(Yii::app()->session['job_id']);
			}
			$WorklflowMember = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_POST['Job']['work_flow']),
				array(
					'order' => 'order_approve asc'
				));

			$WorklflowMemberLimit = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_POST['Job']['work_flow']),
				array(
					'order' => 'order_approve asc',
					'limit' => 1,
				));

			$workflowID =  $_POST['Job']['work_flow'];

			foreach($WorklflowMember as $keyFlow=>$valueFlow){
				$JobWorkflow = new JobWorkflow;
				$JobWorkflow->job_id = $model->id;
				$JobWorkflow->workflow_id = $_POST['Job']['work_flow'];
				$JobWorkflow->client_id = $valueFlow->client_id;
				$JobWorkflow->number_of_approval = $valueFlow->order_approve;
				$JobWorkflow->job_status = 'Pending';
				$JobWorkflow->email_sent = 0;

				$JobWorkflow->save(false);
			}

			//timeLine code by Mike
			$workFlowdata = Worklflow::model()->findByPk($_POST['Job']['work_flow']);
			$history = new JobHistory;
			$history->client_id = $client->id;
			$history->job_id = $model->id;
			$history->workflow_status = 'Pending';
			$history->operation = 'Assigning Work flow';
			$history->description = $client->first_name . '  Assigning Work flow  '.$workFlowdata->flow_name.' to The Job Title '.$model->title;
			$history->save(false);

			$clientData = Client::model()->findByPk($WorklflowMemberLimit[0]['client_id']);

			$JobWorkflowEmail = JobWorkflow::model()->findAllByAttributes(array('number_of_approval'=>1,'workflow_id'=>$_POST['Job']['work_flow'],'job_id'=>$model->id));


			// workflow approval template...
			$loc = unserialize($model->location);
			$locationsV = '';
			if($loc){
				foreach($loc as $val){
					$location = Location::model()->findByPk($val);
					$locationsV .= $location->name.', ';
				}
			}
			$workFlow = Worklflow::model()->findByPk($model->work_flow);

			$htmlbody =  '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px,Arial,sans-serif;padding:2% 7%">
            <div>

            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$clientData->first_name.' '.$clientData->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">We are waiting for your approval.</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4"><strong>Job ID </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->id.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Name </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->title.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Description </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->description.'</span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Pay Rate </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->pay_rate.' </span><br /><br />


						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Location </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$locationsV.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Hour for the Job </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->hours_per_week.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Shift Time. </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->shift.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Project Reference Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->pre_reference_code.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Bill Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->billing_code.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Request Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->request_dept.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Work Flow Approval : </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$workFlow->flow_name.' </span><br /><br />


						To Approve the Job ,
						<a href="'.Yii::app()->createAbsoluteUrl('Client/job/workflowStatus',
					array('workflowid'=>$workflowID,'jobid'=>$model->id,'clientid'=>$WorklflowMemberLimit[0]['client_id'])).'" style="color:#069;font-size:13px;line-height:1.4;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://fwebinc.freshsales.io/confirmations/xbootIN_zLafNA1D3eYAoA&amp;source=gmail&amp;ust=1468345965323000&amp;usg=AFQjCNHpRcdBxBCT16Bscb76mCXx0b9dDw">click here</a>
                    </p><br />

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4">If clicking the link does not work, copy and paste this URL in your browser\'s address bar to approve job</span><br>
                        '.Yii::app()->createAbsoluteUrl('Client/job/workflowStatus',
					array('workflowid'=>$workflowID,'jobid'=>$model->id,'clientid'=>$WorklflowMemberLimit[0]['client_id'])).'
                    </p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
                        Thanks,<br> Webmaster - Simplifyvms
                    </p>
                </div>
            </div>


            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';


			$message = Yii::app()->sendgrid->createEmail();
			//shortcut to $message=new YiiSendGridMail($viewsPath);
			$message->setHtml($htmlbody)
				->setSubject('Simplyfy VMS - Job Approval Invitation')
				->addTo($clientData->email)
				->setFrom('alert@simplifyvms.net');

			//till here workflow approval



			//because of some issue email is not getting sent to thats commented portion of code is not working
			//so now moving it out from the mail condition will fix this later

			if(Yii::app()->sendgrid->send($message)){
				/*if($JobWorkflowEmail[0]) {
                    $JobWorkflowEmail[0]->email_sent = 1;
                    $JobWorkflowEmail[0]->save(false);
                } */
			}
			if($JobWorkflowEmail[0]) {
				$JobWorkflowEmail[0]->email_sent = 1;
				$JobWorkflowEmail[0]->save(false);
			}
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <strong>Success!</strong> Your Job Posting was successfully submitted. After review it would be published</div>');

			$this->redirect(array('addjob'));
			//$this->redirect(array($_SERVER['HTTP_REFERER']));
		}

$this->render('jobstep2',array('model'=>$model));

	}

	public function actionGetworkflowApproval(){

		if(isset($_POST['workflowmember_id'])){
			$workflow = Worklflow::model()->findByPk($_POST['workflowmember_id']);
			$workflowmember = WorklflowMember::model()->findByAttributes(array('workflow_id'=>$_POST['workflowmember_id']));
			$clientdate = Client::model()->findByPk($workflowmember->client_id); ?>
			<tr>
				<td><?php echo $workflow->flow_name; ?></td>
				<td><?php echo $clientdate->first_name. ' '.$clientdate->last_name; ?></td>
				<td><?php echo $workflowmember->department_id; ?></td>
				<td><?php echo 'NULL'; ?></td>
			</tr>
		<?php }


	}
	
	///////////////////working on new job design //////////////////
	
	
	public function actionCreate() 
	{ 
		$client = Client::model()->findByPk(Yii::app()->user->id); 
		if($client->member_type == 'Team Member'){ 
			$this->redirect(array('job/jobListing')); 
		} 
		$model=new Job; 
		$this->layout = 'admin'; 
		if(isset($_POST['Job'])) 
		{
			$model->attributes=$_POST['Job']; 
			 
			$model->user_id = Yii::app()->user->id; 
			$model->user_type = 'Client'; 
			 
			$model->job_po_duration = $_POST['job_po_duration']; 
			$model->desired_start_date = $_POST['desired_start_date'];
			$model->markup = $_POST['Job']['vendor_client_markup'];
			$model->date_created = date('Y-m-d');
			 
			//old skills 
			$skill = Skills::model()->findAll(); 
			$OldSkills = array(); 
			foreach($skill as $skill){ 
				$OldSkills[] = strtolower($skill->name); 
				} 
			 
			if($_POST['Job']['skills']) {
				//$SkillsAddition = explode(",",$_POST['Job']['skills']); 
				//$model->skills = str_replace(' ', '', $_POST['Job']['skills']);
				$SkillsAddition = $_POST['Job']['skills'];  
				$model->skills = implode(',',$_POST['Job']['skills']);
		    }else { 
		    	$model->skills = ''; 
		    } 
			 
			if($_POST['Job']['skills1']) { 
				//$SkillsAddition1 = explode(",",$_POST['Job']['skills1']); 
				//$model->skills1 = str_replace(' ', '', $_POST['Job']['skills1']);
				$SkillsAddition1 = $_POST['Job']['skills1'];
				$model->skills1 = implode(',',$_POST['Job']['skills1']);
		    }else { 
		    	$model->skills1 = ''; 
		    } 
			 
			if($_POST['Job']['skills2']) { 
				//$SkillsAddition2 = explode(",",$_POST['Job']['skills2']); 
				//$model->skills2 = str_replace(' ', '', $_POST['Job']['skills2']); 
				$SkillsAddition2 = $_POST['Job']['skills2']; 
				$model->skills2 = implode(',',$_POST['Job']['skills2']);
		    }else { 
		    	$model->skills2 = ''; 
		    } 
			 
			 if(isset($_POST['Job']['location'])){
			$model->location = serialize($_POST['Job']['location']);
			 }
			//doing this becuase teamMembers will be multiple.
			if(isset($_POST['Job']['invite_team_member'])) {
				$model->invite_team_member = serialize($_POST['Job']['invite_team_member']);
			}
			//$model->request_dept = $_POST['Job']['request_dept'];
			 
			if($model->save(false)){ 
			
				$history = new JobHistory;
				$history->client_id = $client->id;
				$history->job_id = $model->id;
				$history->operation = 'added Job';
				$history->job_status = 'Pending';
				$history->description = $client->first_name . '  added Job  Title '.$model->title;
				$history->save(false);
			
				// Invite teammeber to this job 
				if(isset($_POST['Job']['invite_team_member'])) { 
					if(!empty($_POST['Job']['invite_team_member'][0])){ 
						$inviteteammember = $_POST['Job']['invite_team_member']; 
						foreach($inviteteammember as $value) { 
							if(Yii::app()->db->createCommand('insert into vms_teammember_job set teammember_id='.$value.' , job_id='.$model->primaryKey.', date_created="'.date('Y-m-d').'"')->execute()) 
							{ 
							//$this->clientEmail(Yii::app()->db->getLastInsertID()); 
								//Yii::app()->user->setFlash('success', "Team member invited successfully"); 
							} 
						} 
					} 
				} 
				 
				//this code is to remove duplication of skills. 
				if($_POST['Job']['skills']) { 
					foreach($SkillsAddition as $skillVal){ 
					$skillModel = new Skills; 
						if (!in_array(str_replace(' ', '', strtolower($skillVal)), $OldSkills)) { 
							$skillModel->name =  $skillVal; 
							$skillModel->save(false); 
						} 
						 
					} 
			   } 
			    
			   if($_POST['Job']['skills1']) { 
					foreach($SkillsAddition1 as $skillVal1){ 
					$skillModel = new Skills; 
						if (!in_array(str_replace(' ', '', strtolower($skillVal1)), $OldSkills)) { 
							$skillModel->name =  $skillVal1; 
							$skillModel->save(false); 
						} 
						 
					} 
			   } 
			   if($_POST['Job']['skills2']) { 
					foreach($SkillsAddition2 as $skillVal2){ 
					$skillModel = new Skills; 
						if (!in_array(str_replace(' ', '', strtolower($skillVal2)), $OldSkills)) { 
							$skillModel->name =  $skillVal2; 
							$skillModel->save(false); 
						} 
						 
					} 
			   } 
			    
			    
			    
			   $WorklflowMember = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_POST['Job']['work_flow']), 
					array( 
						'order' => 'order_approve asc' 
					)); 
					 
				$WorklflowMemberLimit = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_POST['Job']['work_flow']), 
						array( 
							'order' => 'order_approve asc', 
							'limit' => 1, 
						)); 
				 
				$workflowID =  $_POST['Job']['work_flow']; 
				 
				foreach($WorklflowMember as $keyFlow=>$valueFlow){ 
					$JobWorkflow = new JobWorkflow; 
					$JobWorkflow->job_id = $model->id; 
					$JobWorkflow->workflow_id = $_POST['Job']['work_flow']; 
					$JobWorkflow->client_id = $valueFlow->client_id; 
					$JobWorkflow->number_of_approval = $valueFlow->order_approve; 
					$JobWorkflow->job_status = 'Pending'; 
					$JobWorkflow->email_sent = 0; 
					 
					$JobWorkflow->save(false); 
					} 
				
				//timeLine code by Mike
				$workFlowdata = Worklflow::model()->findByPk($_POST['Job']['work_flow']);
				$history = new JobHistory;
				$history->client_id = $client->id;
				$history->job_id = $model->id;
				$history->workflow_status = 'Pending';
				$history->operation = 'Assigning Work flow';
				$history->description = $client->first_name . '  Assigning Work flow  '.$workFlowdata->flow_name.' to The Job Title '.$model->title;
				$history->save(false);
					 
				$clientData = Client::model()->findByPk($WorklflowMemberLimit[0]['client_id']); 
				 
				$JobWorkflowEmail = JobWorkflow::model()->findAllByAttributes(array('number_of_approval'=>1,'workflow_id'=>$_POST['Job']['work_flow'],'job_id'=>$model->id)); 
				 
				 
				 
				 
				 
	// workflow approval template...
	$loc = unserialize($model->location);
	$locationsV = '';
				if($loc){
	foreach($loc as $val){
		$location = Location::model()->findByPk($val);
		$locationsV .= $location->name.', ';
		}
				}
	$workFlow = Worklflow::model()->findByPk($model->work_flow);
			  
	$htmlbody =  '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px,Arial,sans-serif;padding:2% 7%">
            <div>
                
            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$clientData->first_name.' '.$clientData->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">We are waiting for your approval.</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4"><strong>Job ID </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->id.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Name </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->title.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Description </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->description.'</span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Pay Rate </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->pay_rate.' </span><br /><br />
						
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Location </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$locationsV.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Hour for the Job </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->hours_per_week.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Shift Time. </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->shift.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Project Reference Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->pre_reference_code.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Bill Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->billing_code.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Request Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->request_dept.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Work Flow Approval : </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$workFlow->flow_name.' </span><br /><br />
						
						
						To Approve the Job ,
						<a href="'.Yii::app()->createAbsoluteUrl('Client/job/workflowStatus', 
						array('workflowid'=>$workflowID,'jobid'=>$model->id,'clientid'=>$WorklflowMemberLimit[0]['client_id'])).'" style="color:#069;font-size:13px;line-height:1.4;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://fwebinc.freshsales.io/confirmations/xbootIN_zLafNA1D3eYAoA&amp;source=gmail&amp;ust=1468345965323000&amp;usg=AFQjCNHpRcdBxBCT16Bscb76mCXx0b9dDw">click here</a>
                    </p><br />

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4">If clicking the link does not work, copy and paste this URL in your browser\'s address bar to approve job</span><br>
                        '.Yii::app()->createAbsoluteUrl('Client/job/workflowStatus', 
						array('workflowid'=>$workflowID,'jobid'=>$model->id,'clientid'=>$WorklflowMemberLimit[0]['client_id'])).'
                    </p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
                        Thanks,<br> Webmaster - Simplifyvms
                    </p>
                </div>
            </div>

            
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
		 
		 
		 
			$message = Yii::app()->sendgrid->createEmail(); 
			 //shortcut to $message=new YiiSendGridMail($viewsPath); 
			$message->setHtml($htmlbody) 
			->setSubject('Simplyfy VMS - Job Approval Invitation') 
			->addTo($clientData->email) 
			->setFrom('alert@simplifyvms.net'); 
			
			//till here workflow approval	 
				 
			
				
				//because of some issue email is not getting sent to thats commented portion of code is not working
				//so now moving it out from the mail condition will fix this later
				
				if(Yii::app()->sendgrid->send($message)){ 
					/*if($JobWorkflowEmail[0]) { 
						$JobWorkflowEmail[0]->email_sent = 1; 
						$JobWorkflowEmail[0]->save(false); 
					} */
					}
			   if($JobWorkflowEmail[0]) { 
					$JobWorkflowEmail[0]->email_sent = 1; 
					$JobWorkflowEmail[0]->save(false); 
				} 
				Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Success!</strong> Your Job Posting was successfully submitted. After review it would be published</div>'); 
				
				$this->redirect(array('addjob'));
				//$this->redirect(array($_SERVER['HTTP_REFERER'])); 
			}else{ 
				Yii::app()->user->setFlash('success', '<div class="alert alert-danger"> 
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
          <strong>Error!</strong> Your Job Posting was not successfully submitted. </div>'); 
				} 
		} 
		$this->render('create',array ( 
			'model'=>$model, 
		)); 
	} 
	 
	 
	public function actionJobTemplates(){
		//error_reporting(0);
		$client_id = Yii::app()->user->id;

		$criteria=new CDbCriteria();
		$criteria->condition = 'client_id ='.$client_id;
		//$criteria->addInCondition("client_id", $client_id);
		$criteria->order = "id desc";
		$count=JobTemplates::model()->count($criteria);
		$pages=new CPagination($count);

		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		$model=JobTemplates::model()->findAll($criteria);

		//$model = JobTemplates::model()->findAllByAttributes(array('client_id'=>$client_id),array('order'=>'id desc'));

		 $this->render('jobtemplates',array('model'=>$model,'pages' => $pages));
		}
		
	public function actionAddTemplate(){
		error_reporting(0);
		$model = new JobTemplates;
		if(isset($_POST['JobTemplates'])){
			$model->attributes=$_POST['JobTemplates'];
			
			if($_POST['JobTemplates']['primary_skills']) {
				$model->primary_skills = implode(',',$_POST['JobTemplates']['primary_skills']);
		    }else { 
		    	$model->primary_skills = ''; 
		    } 
			 
			if($_POST['JobTemplates']['secondary_skills']) { 
				$model->secondary_skills = implode(',',$_POST['JobTemplates']['secondary_skills']);
		    }else { 
		    	$model->secondary_skills = ''; 
		    } 
			 
			if($_POST['JobTemplates']['good_to_have']) { 
				$model->good_to_have = implode(',',$_POST['JobTemplates']['good_to_have']);
		    }else { 
		    	$model->good_to_have = ''; 
		    }
			$temp_experience = '';
			foreach($_POST['temp_experience'] as $value){
				$temp_experience .= $value.',';
			}
			$model->temp_experience = $temp_experience;
			$temp_min_billrate = '';
			foreach($_POST['temp_min_billrate'] as $value){
				$temp_min_billrate .= $value.',';
			}
			$model->temp_min_billrate = $temp_min_billrate;
			$temp_max_billrate = '';
			foreach($_POST['temp_max_billrate'] as $value){
				$temp_max_billrate .= $value.',';
			}
			$model->temp_max_billrate = $temp_max_billrate;
			$model->client_id = Yii::app()->user->id;
			$model->date_created = date("Y-m-d");;
			if($model->save(false)){
			$this->redirect(array('jobtemplates'));
			}
		}
		 $this->render('addtemplate',array('model'=>$model));
		}
		
	public function actionUpdateTemplate(){
		//error_reporting(0);
		$templateId = $_GET['template_id'];
		$model = JobTemplates::model()->findByPk($templateId);
		if(isset($_POST['JobTemplates'])){
			$model->attributes=$_POST['JobTemplates'];
			$temp_experience = '';
			foreach($_POST['temp_experience'] as $value){
				$temp_experience .= $value.',';
			}
			$model->temp_experience = $temp_experience;
			$temp_min_billrate = '';
			foreach($_POST['temp_min_billrate'] as $value){
				$temp_min_billrate .= $value.',';
			}
			$model->temp_min_billrate = $temp_min_billrate;
			$temp_max_billrate = '';
			foreach($_POST['temp_max_billrate'] as $value){
				$temp_max_billrate .= $value.',';
			}
			$model->temp_max_billrate = $temp_max_billrate;
			$model->client_id = Yii::app()->user->id;
			$model->date_created = date("Y-m-d");;
			if($model->save(false)){
				$this->redirect(array('jobtemplates'));
			}
		}
		$this->render('updatetemplate',array('model'=>$model));
	}

	public function actionTemplateView(){
		if(isset($_GET['template_id'])){
			$templateID = $_GET['template_id'];
			$model = JobTemplates::model()->findByPk($templateID);
			$this->render('templateView',array('model'=>$model));
		}

	}
	
	public function actionDeleteTempelt(){

		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$model = JobTemplates::model()->findByPk($id);
			$model->delete();
			$this->redirect(array('jobtemplates'));

		}

	}
	 
	 
	public function actionDirectApprove(){
		$this->layout = 'simple';
		$JobWorkflowEmail = JobWorkflow::model()->findAllByAttributes(array('client_id'=>$_GET['clientid'],'workflow_id'=>$_GET['workflowid'],'job_id'=>$_GET['jobid'])); 
		 
		$Job = Job::model()->findByPk($_GET['jobid']); 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['jobid'])->query()->readAll(); 
		$client = Client::model()->findByPk($_GET['clientid']); 
		
		
		 
		$EmailSend = JobWorkflow::model()->findAllByAttributes(array('email_sent'=>0,'workflow_id'=>$_GET['workflowid']), 
					array( 
						'order' => 'number_of_approval asc', 
						'limit' => 1, 
					)); 
		 
		 
		if($EmailSend){ 
			 
		$clientData = Client::model()->findByPk($EmailSend[0]['client_id']); 
			  
			  
	//email body for approval of workflow		  
			 
	$loc = unserialize($Job->location);
	$locationsV = '';
			if($loc){
	foreach($loc as $val){
		$location = Location::model()->findByPk($val);
		$locationsV .= $location->name.', ';
		}
			}
	$workFlow = Worklflow::model()->findByPk($Job->work_flow);
			  
	$htmlbody =  '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px,Arial,sans-serif;padding:2% 7%">
            <div>
                
            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$clientData->first_name.' '.$clientData->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">We are waiting for your approval.</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4"><strong>Job ID </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->id.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Name </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->title.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Description </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->description.'</span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Pay Rate </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->pay_rate.' </span><br /><br />
						
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Location </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$locationsV.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Hour for the Job </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->hours_per_week.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Shift Time. </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->shift.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Project Reference Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->pre_reference_code.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Bill Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->billing_code.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Request Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->request_dept.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Work Flow Approval : </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$workFlow->flow_name.' </span><br /><br />
						
						
						To Approve the Job ,
						<a href="'.Yii::app()->createAbsoluteUrl('Client/job/directApprove', 
						array('workflowid'=>$_GET['workflowid'],'jobid'=>$_GET['jobid'],'clientid'=>$EmailSend[0]['client_id'])).'" style="color:#069;font-size:13px;line-height:1.4;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://fwebinc.freshsales.io/confirmations/xbootIN_zLafNA1D3eYAoA&amp;source=gmail&amp;ust=1468345965323000&amp;usg=AFQjCNHpRcdBxBCT16Bscb76mCXx0b9dDw">click here</a>
                    </p><br />

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4">If clicking the link does not work, copy and paste this URL in your browser\'s address bar to approve job</span><br>
                        '.Yii::app()->createAbsoluteUrl('Client/job/workflowStatus', 
						array('workflowid'=>$_GET['workflowid'],'jobid'=>$_GET['jobid'],'clientid'=>$EmailSend[0]['client_id'])).'
                    </p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
                        Thanks,<br> Simplifyvms Team
                    </p>
                </div>
            </div>

            
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>'; 
			  
			  
			  
		 
			$message = Yii::app()->sendgrid->createEmail(); 
			 //shortcut to $message=new YiiSendGridMail($viewsPath); 
			$message->setHtml($htmlbody) 
			->setSubject('Simplifyvms - Job Approval Invitation') 
			->addTo($clientData->email) 
			->setFrom('alert@simplifyvms.net'); 
			 
			/*********************** Mail content **********************/ 
			 
			 
			$JobWorkflowEmail[0]->job_status = 'Approved'; 
			//date and time of rejection for history 
			$JobWorkflowEmail[0]->status_time = date("Y-m-d H:i:s"); 
			 
				if($JobWorkflowEmail[0]->save(false)){ 
					if(Yii::app()->sendgrid->send($message)){ 
						 
						$EmailSend[0]->email_sent = 1; 
						$EmailSend[0]->save(false); 
					} 
				} 
				 
			}else{ 
				  $job = Job::model()->findByPk($_GET['jobid']); 
				  $job->jobStatus = '3'; 
				  $job->save(false); 
				  $JobWorkflowEmail[0]->job_status = 'Approved'; 
				  $JobWorkflowEmail[0]->status_time = date("Y-m-d H:i:s"); 
				  $JobWorkflowEmail[0]->save(false); 
				 } 
			$this->render('directApprove',array('model'=>$Job));	 
			//$this->redirect(array('directView','jobid'=>$_GET['jobid']));
		 
		
		}
		
	/*public function actionDirectView(){
		$this->layout = 'simple';
		$this->render('directApprove');
		}*/
	 
	 
	public function actionWorkflowStatus() 
	{ 
		$this->layout = 'admin'; 
		 
		$JobWorkflowEmail = JobWorkflow::model()->findAllByAttributes(array('client_id'=>$_GET['clientid'],'workflow_id'=>$_GET['workflowid'],'job_id'=>$_GET['jobid'])); 
		 
		 
		 
		$Job = Job::model()->findByPk($_GET['jobid']); 
		$invitedTeamMembers = Yii::app()->db->createCommand('SELECT vms_client.* FROM `vms_client` inner join vms_teammember_job as team_jobs on vms_client.id = team_jobs.teammember_id and job_id='.$_GET['jobid'])->query()->readAll(); 
		$client = Client::model()->findByPk($_GET['clientid']); 
		 
		/*echo date("Y-d-m:h-i-s"); 
		exit;*/
		$workFlowvalue = WorklflowMember::model()->findByAttributes(array('workflow_id'=>$_GET['workflowid']));
		if($client->super_client_id==0){
			$Clientdata = Client::model()->findByPk($_GET['clientid']);
		}else{
			$Clientdata = Client::model()->findByPk($workFlowvalue->client_id);
		}
		$Jobdata = Job::model()->findByPk($_GET['jobid']);
		$history = new JobHistory;
		$history->client_id = $workFlowvalue->client_id;
		$history->job_id = $_GET['jobid'];
		$history->workflow_status = 'Approved';
		$history->operation = 'Approval';
		$history->description = $Clientdata->first_name.' Approved  Job Title '.$Jobdata->title;
		$history->save(false);
		 
		if(isset($_POST['Reason'])){ 
			$JobWorkflowEmail[0]->job_status = 'Rejected'; 
			$JobWorkflowEmail[0]->rejection_dropdown = $_POST['rejection_dropdown']; 
			//date and time of rejection for history 
			$JobWorkflowEmail[0]->status_time = date("Y-m-d H:i:s"); 
			$JobWorkflowEmail[0]->rejection_reason = $_POST['reason_details']; 
			$JobWorkflowEmail[0]->save(false);
			$Job->jobStatus = 5;
			$Job->save(false);
			Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
			  <strong>Success!</strong> Your rejection submitted successfully.</div>');

			$workFlowvalue = WorklflowMember::model()->findByAttributes(array('workflow_id'=>$_GET['workflowid']));
			if($client->super_client_id==0){
				$Clientdata = Client::model()->findByPk($_GET['clientid']);
			}else{
				$Clientdata = Client::model()->findByPk($workFlowvalue->client_id);
			}
			$Jobdata = Job::model()->findByPk($_GET['jobid']);
			$history = new JobHistory;
			$history->client_id = $workFlowvalue->client_id;
			$history->job_id = $_GET['jobid'];
			$history->workflow_status = 'Rejected';
			$history->operation = 'Rejection';
			$history->description = $Clientdata->first_name.' Rejected  Job Title '.$Jobdata->title;
			$history->save(false);
			}

		if(isset($_POST['approve'])){

			$EmailSend = JobWorkflow::model()->findAllByAttributes(array('email_sent'=>0,'workflow_id'=>$_GET['workflowid'],'job_id'=>$_GET['jobid']), 
						array( 
							'order' => 'number_of_approval asc', 
							'limit' => 1, 
						));

			$workFlowvalue = WorklflowMember::model()->findByAttributes(array('workflow_id'=>$_GET['workflowid']));

			if($client->super_client_id==0){
				$Clientdata = Client::model()->findByPk($_GET['clientid']);
			}else{
				$Clientdata = Client::model()->findByPk($workFlowvalue->client_id);
			}
			$Jobdata = Job::model()->findByPk($_GET['jobid']);
			$history = new JobHistory;
			$history->client_id = $workFlowvalue->client_id;
			$history->job_id = $_GET['jobid'];
			$history->workflow_status = 'Approved';
			$history->operation = 'Approval';
			$history->description = $Clientdata->first_name.' Approved  Job Title '.$Jobdata->title;
			$history->save(false);

			if($EmailSend){
				


				 
			$clientData = Client::model()->findByPk($EmailSend[0]['client_id']); 
						  
	//email body for approval of workflow		  
			 
	$loc = unserialize($Job->location);
	$locationsV = '';
				if($loc){
	foreach($loc as $val){
		$location = Location::model()->findByPk($val);
		$locationsV .= $location->name.', ';
		}
				}
	$workFlow = Worklflow::model()->findByPk($Job->work_flow);
			  
	$htmlbody =  '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px,Arial,sans-serif;padding:2% 7%">
            <div>
                
            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$clientData->first_name.' '.$clientData->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">We are waiting for your approval.</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4"><strong>Job ID </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->id.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Name </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->title.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Description </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->description.'</span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Pay Rate </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->pay_rate.' </span><br /><br />
						
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Location </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$locationsV.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Hour for the Job </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->hours_per_week.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Shift Time. </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->shift.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Project Reference Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->pre_reference_code.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Bill Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->billing_code.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Request Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->request_dept.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Work Flow Approval : </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$workFlow->flow_name.' </span><br /><br />
						
						
						To Approve the Job ,
						<a href="'.Yii::app()->createAbsoluteUrl('Client/job/directApprove', 
						array('workflowid'=>$_GET['workflowid'],'jobid'=>$_GET['jobid'],'clientid'=>$EmailSend[0]['client_id'])).'" style="color:#069;font-size:13px;line-height:1.4;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://fwebinc.freshsales.io/confirmations/xbootIN_zLafNA1D3eYAoA&amp;source=gmail&amp;ust=1468345965323000&amp;usg=AFQjCNHpRcdBxBCT16Bscb76mCXx0b9dDw">click here</a>
                    </p><br />

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4">If clicking the link does not work, copy and paste this URL in your browser\'s address bar to approve job</span><br>
                        '.Yii::app()->createAbsoluteUrl('Client/job/workflowStatus', 
						array('workflowid'=>$_GET['workflowid'],'jobid'=>$_GET['jobid'],'clientid'=>$EmailSend[0]['client_id'])).'
                    </p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
                        Thanks,<br> Simplifyvms Team
                    </p>
                </div>
            </div>

            
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>'; 
			  
			  
			  
		 
			$message = Yii::app()->sendgrid->createEmail(); 
			 //shortcut to $message=new YiiSendGridMail($viewsPath); 
			$message->setHtml($htmlbody) 
			->setSubject('Simplifyvms - Job Approval Invitation') 
			->addTo($clientData->email) 
			->setFrom('alert@simplifyvms.net'); 
			 
			/*********************** Mail content **********************/ 
			 
			 
			$JobWorkflowEmail[0]->job_status = 'Approved'; 
			//date and time of rejection for history 
			$JobWorkflowEmail[0]->status_time = date("Y-m-d H:i:s"); 
			 
				if($JobWorkflowEmail[0]->save(false)){ 
					if(Yii::app()->sendgrid->send($message)){ 
						 //just moving this code out b/c email function is not working
						 /*$EmailSend[0]->email_sent = 1; 
						 $EmailSend[0]->save(false);*/   
					} 
					$EmailSend[0]->email_sent = 1; 
					$EmailSend[0]->save(false); 
				}
			}else{ 
				  
				  $job = Job::model()->findByPk($_GET['jobid']); 
				  
				 
				  
				  $job->jobStatus = '3'; 
				  $job->save(false);  
				  
				  $JobWorkflowEmail[0]->job_status = 'Approved'; 
				  $JobWorkflowEmail[0]->status_time = date("Y-m-d H:i:s"); 
				  $JobWorkflowEmail[0]->save(false); 
				 } 
				  
				 Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
				  <strong>Success!</strong> You have approved job successfully.</div>'); 
		 } 
			 
		/**** Currently online Client verification check *****/ 
		 
		 
		if(Yii::app()->user->id == $_GET['clientid']){ 
			$flag = true; 
			}else{ 
				 $flag = false; 
				 } 
		 
		$jobMainClient = Job::model()->findByPk($_GET['jobid']); 
		 
		if($jobMainClient->user_type == 'Client' && $jobMainClient->user_id == Yii::app()->user->id ){ 
			$flag1 = true; 
			}else{ 
				 $flag1 = false; 
				 } 
		 
		/**** uncomment the check when you wants to validate ****/ 
		 
		if(/*$JobWorkflowEmail && */$flag == true || $flag1 == true){ 
			$this->render('approvejob',array('JobWorkflowEmail'=>$JobWorkflowEmail,'Job'=>$Job,'Client'=>$client,'invitedTeamMembers'=>$invitedTeamMembers,'flag1'=>$flag1)); 
			}else{ 
				 $this->redirect(array('default/index')); 
				 } 
		 
		 
	} 
	/** 
	 * Updates a particular model. 
	 * If update is successful, the browser will be redirected to the 'view' page. 
	 * @param integer $id the ID of the model to be updated 
	 */ 
	public function actionUpdate($id) 
	{ 
		$model=$this->loadModel($id); 
		//$this->layout = 'job'; 
		$this->layout = 'admin'; 
		// Uncomment the following line if AJAX validation is needed 
		// $this->performAjaxValidation($model); 
		if(isset($_POST['Job'])) 
		{ 
			$model->attributes=$_POST['Job']; 
			if($model->save()) 
				$this->redirect(array('view','id'=>$model->id)); 
		} 
		$this->render('update',array( 
			'model'=>$model, 
		)); 
	} 
	/** 
	 * Deletes a particular model. 
	 * If deletion is successful, the browser will be redirected to the 'admin' page. 
	 * @param integer $id the ID of the model to be deleted 
	 */ 
	public function actionDeletejob($id) 
	{ 
		if($this->loadModel($id)->delete()){
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> Data deleted successfully. </div>');
			$this->redirect(array('joblisting')); 
		} 
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser 
		if(!isset($_GET['ajax'])) 
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); 
	} 
	/** 
	 * Lists all models. 
	 */ 
	public function actionIndex() 
	{ 
		$dataProvider=new CActiveDataProvider('Job'); 
		$this->render('index',array( 
			'dataProvider'=>$dataProvider, 
		)); 
	} 
	/** 
	 * Manages all models. 
	 */ 
	public function actionAdmin() 
	{ 
		$model=new Job('search'); 
		$model->unsetAttributes();  // clear any default values 
		if(isset($_GET['Job'])) 
			$model->attributes=$_GET['Job']; 
		$this->render('admin',array( 
			'model'=>$model, 
		)); 
	} 
	/** 
	 * Returns the data model based on the primary key given in the GET variable. 
	 * If the data model is not found, an HTTP exception will be raised. 
	 * @param integer $id the ID of the model to be loaded 
	 * @return Job the loaded model 
	 * @throws CHttpException 
	 */ 
	public function loadModel($id) 
	{ 
		$model=Job::model()->findByPk($id); 
		if($model===null) 
			throw new CHttpException(404,'The requested page does not exist.'); 
		return $model; 
	} 
	/** 
	 * Performs the AJAX validation. 
	 * @param Job $model the model to be validated 
	 */ 
	protected function performAjaxValidation($model) 
	{ 
		if(isset($_POST['ajax']) && $_POST['ajax']==='job-form') 
		{ 
			echo CActiveForm::validate($model); 
			Yii::app()->end(); 
		} 
	} 
	 
	
	public function actionDirectApproveTime(){ 
	  if(isset($_GET['submissonId'])){ 
		$submissonId = $_GET['submissonId']; 
		$JobId = $_GET['id'];   
		$interview = Interview::model()->findByPk($_GET['interviewId']); 
		$interview->status = 2; 
		$interview->start_date_status = 1;	   
		 
			if($interview->save(false)){ 
				$this->render('directApprove',array('model'=>$interview)); 
			} 
		} 
	 }
	
	public function actionAprrovedFirsttime(){ 
	  if(isset($_GET['submissonId'])){ 
		$submissonId = $_GET['submissonId']; 
		$JobId = $_GET['id'];   
		$interview = Interview::model()->findByPk($_GET['interviewId']); 
		$interview->status = 2; 
		$interview->start_date_status = 1;	   
		 
			if($interview->save(false)){ 
				$this->redirect(array('interview/allinterview','jobid'=>$JobId,'type'=>'interview','st'=>'all')); 
			} 
		} 
	 }
	  
	public function actionAprrovedSecondtime(){ 
		if(isset($_GET['submissonId'])){ 
			$submissonId = $_GET['submissonId']; 
			$JobId = $_GET['id'];   
			$interview = Interview::model()->findByPk($_GET['interviewId']); 
			$interview->status = 2; 
			$interview->alt1_date_status = 1; 
				if($interview->save(false)){ 
					$this->redirect(array('interview/allinterview','jobid'=>$JobId,'type'=>'interview','st'=>'all')); 
				} 
		} 
	 
	} 
	  
	  
	 public function actionAprrovedThirdtime(){ 
		 if(isset($_GET['submissonId'])){ 
				$submissonId = $_GET['submissonId']; 
				$JobId = $_GET['id'];   
				$interview = Interview::model()->findByPk($_GET['interviewId']); 
				$interview->status = 2; 
				$interview->alt2_date_status = 1; 
					if($interview->save(false)){ 
						$this->redirect(array('interview/allinterview','jobid'=>$JobId,'type'=>'interview','st'=>'all')); 
					} 
			} 
		 } 
		 
	 public function actionAprrovedfourthtime(){ 
		 if(isset($_GET['submissonId'])){ 
				$submissonId = $_GET['submissonId']; 
				$JobId = $_GET['id'];   
				$interview = Interview::model()->findByPk($_GET['interviewId']); 
				$interview->status = 2; 
				$interview->alt3_date_status = 1; 
					if($interview->save(false)){ 
						$this->redirect(array('interview/allinterview','jobid'=>$JobId,'type'=>'interview','st'=>'all')); 
					} 
			} 
		 } 
	  
	 public function actionDownloadResume(){ 
		if(!empty($_GET['type']) && $_GET['type']=="profile"){ 
		 $candidate =  CandidatesProfile::model()->findByPk($_GET['id']); 
		 $path = '/home/biwwwsimplifyvms/public_html/billrate/msp/candidate_resumes/';
		 
	   $file = $path.$candidate->resume; 
		}else{ 
	   $candidate = Candidates::model()->findByPk($_GET['id']); 
	 
	 
	   if($candidate->source_type =='vendor'){ 
	 
		$path = '/home/biwwwsimplifyvms/public_html/billrate/vendor/candidate_resumes/';
		$file = $path.$candidate->resume; 
	   }else { 
		$path = '/home/biwwwsimplifyvms/public_html/billrate/msp/candidate_resumes/';
		$file = $path.$candidate->resume; 
	   } 
	 
	   } 
	  
	 if (file_exists($file)) { 
		 header('Content-Description: File Transfer'); 
		 header('Content-Type: application/octet-stream'); 
		 header('Content-Disposition: attachment; filename="'.basename($file).'"'); 
		 header('Expires: 0'); 
		 header('Cache-Control: must-revalidate'); 
		 header('Pragma: public'); 
		 header('Content-Length: ' . filesize($file)); 
		 readfile($file); 
		 exit; 
	 } 
	 
	 }
	 
	 //on job creation saving data as a template too.
	 public static function JobCreationTemplate($model){
			$templateEntry = new JobTemplates;
			$categoryID = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$model->cat_id));					
			$templateEntry->client_id = $model->user_id;
			$templateEntry->cat_id = $categoryID->id;					
			$templateEntry->job_title = $model->title;
			$templateEntry->experience = $model->experience;
			$templateEntry->job_level = $model->job_level;
			$templateEntry->primary_skills = $model->skills;
			$templateEntry->secondary_skills = $model->skills1;
			$templateEntry->good_to_have = $model->skills2;
			$templateEntry->notes_for_skills = $model->skills_notes;
			$templateEntry->job_description = $model->description;
			$templateEntry->you_will = $model->you_will;
			$templateEntry->qualification = $model->qualification;
			$templateEntry->add_info = $model->add_info;
			$templateEntry->date_created = date('Y-m-d');
			$templateEntry->save(false);
		 }
	  
	 //older resume function 
	 /*public function actionDownloadResume(){ 
		$candidate = Candidates::model()->findByPk($_GET['id']); 
		if($candidate->source_type =='vendor'){ 
		 
		$path = '/home/demobetademo/public_html/vendor/candidate_resumes/'; 
		}else { 
		$path = '/home/demobetademo/public_html/mspadmin/candidate_resumes/'; 
		} 
		 
		$file = $path.$candidate->resume; 
		 
		if (file_exists($file)) { 
		 header('Content-Description: File Transfer'); 
		 header('Content-Type: application/octet-stream'); 
		 header('Content-Disposition: attachment; filename="'.basename($file).'"'); 
		 header('Expires: 0'); 
		 header('Cache-Control: must-revalidate'); 
		 header('Pragma: public'); 
		 header('Content-Length: ' . filesize($file)); 
		 readfile($file); 
		 exit; 
		} 
	 
	 }*/ 
	 
	 
} 
