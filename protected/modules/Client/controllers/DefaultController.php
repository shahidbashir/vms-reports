<?php
class DefaultController extends Controller
{
	//public $layout='/layouts/test';
	
	public $newCustomer;
	public $newDoctor;
	public $newAffiate;
	public $kitNotSentByCustomert;
	public $doctorNotAssigned;
	public $recentTransaction;
	
	private $_config = array();
	
	public function init()
	{
		$this->_config = Yii::app()->controller->module->getModuleConfig();
	}
	
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	 
	public function filters()
    {
		//Yii::app()->user->loginUrl =  array('Client/default/login');
        return array(
            'accessControl',
        );
    }
	
	public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('login','appLogin','captcha','resetpassword','oAuth2Callback','workflowStatus'),
                'users'=>array('*'),
            ),
			array('allow',
                'actions'=>array('pdfDownloadConsolidate','pdfDownload','view','admin','adminStatistic','manage','forgotpassword','keepAlive','search','adminconsolidate','approvedInvoice','consolidateView'),
                'roles'=>array('@'),
            ),
            array('allow',
                'actions'=>array('index', 'manage', 'logout')
            ),
            array('deny',
                'actions'=>array('delete'),
                'users'=>array('*'),
            ),
        );
		
		
    }
	public function actionPdfDownloadConsolidate(){

		$consolidateData='';
		$consolidateModel ='';

		$consolidateModel = ConsolidatedInvoice::model()->findByPk($_GET['id']);
		$clientModel = Client::model()->findByPk($consolidateModel->client_id);
		$invoiceClientModel = InvoiceClientVend::model()->findByAttributes(array('client_id'=>$clientModel->id));





		$consolidatedTaxModel ='';
		if($consolidateModel) {
			$consolidatedTaxModel = ConsolidatedTax::model()->findAll(array('condition'=>'cosolidated_invoice_id='.$consolidateModel->id));
			// isset() removed
		}

		$locationModel = Location::model()->findByPk(array('reference_id'=>$clientModel->id));

		$html='';

		$fileName = 'Invoice#'.$consolidateModel->serial_number.'-'.date("m/d/Y",strtotime($consolidateModel->date_created));
		$currentDate=date("Y-m-d");
		$startDate =date("Y-m-d");



		$generatedIDs = $consolidateModel->generated_invoice_id;
		$invoiceIDs = explode(",", $generatedIDs);
		$total = 0;
		$totalMSPFees = $totalDiscount=$totalHours = $totalIndiPrice = $totalMSPFeesPercent=$totalDiscountPercent=0;

		$sql = "select invoice.*,can.first_name,can.middle_name,can.last_name from vms_generated_invoice invoice,vms_candidates as can where invoice.candidate_id=can.id and invoice.id=".$invoiceIDs[0];
		$invoiceReader = Yii::app()->db->createCommand($sql)->queryRow();

		$vendorModel = Vendor::model()->findByPk($invoiceReader['vendor_id']);


		$timesheetDetail = CpTimesheet::model()->findByAttributes(array('id'=>$invoiceReader['timsheet_id']));

		$clientApprover = Client::model()->findByPk($timesheetDetail->approval_manager);
		$offerModel = Offer::model()->findByPk($timesheetDetail->offer_id);

		if(!$clientApprover){
			$clientApprover = $clientModel;
		}

		$locationModel = Location::model()->findByPk($offerModel->approver_manager_location);
		if(!$locationModel){
			$locationModel = Location::model()->findByPk($clientModel->id);

		}

		$vendorModel = Vendor::model()->findByPk($invoiceReader['vendor_id']);

		$title='<table><tr><td align="left" width="50%"><strong>'.$clientModel->business_name.'</strong><div><small><br />'.$locationModel->name.'<br />'.$locationModel->address1.'<br />'.$clientApprover->email.'</small></div></td><td align="right" width="50%"><strong>INVOICE</strong><div ><small><br />Invoice Date '.date("m/d/Y",strtotime($consolidateModel->date_created)).'<br />Invoice #'.$consolidateModel->serial_number.'</small></div></td></tr></table>';

		if($invoiceIDs)
			$html .='<br /><hr /><br /><div></div><div></div><div></div>
	<table border="0.5" cellpadding="3" align="left" width="100%"><tbody><tr><td  width="4%">ID</td>
	<td width="22%">Candidate Name</td><td width="14%">Location</td><td width="21%">Supplier</td>
	 <th width="17%" align="right">MSP Fees</th>
	 <th style="width: 10%" align="right">Discount</th><td width="12%" align="right">Amount</td></tr>';
		foreach($invoiceIDs as $value) {
			$sql = "select invoice.*,can.current_location,can.first_name,can.middle_name,can.last_name from vms_generated_invoice invoice,vms_candidates as can where invoice.candidate_id=can.id and invoice.id=".$value;
			$invoiceReader = Yii::app()->db->createCommand($sql)->queryRow();

			$vendorModel = Vendor::model()->findByPk($invoiceReader['vendor_id']);

			$html .='<tr><td width="4%">'.$value.'</td>
	<td width="22%">'.$invoiceReader['first_name']." ".$invoiceReader['middle_name']." ".$invoiceReader['last_name'].'</td><td width="14%">'.$invoiceReader['current_location'].'</td><td width="21%">'.$vendorModel->organization.'</td>';

			$toApply = $invoiceReader['total_billrate'];
			$mspFees = 0;
			$discount=0;
			foreach($consolidatedTaxModel as $taxValue) {
				if($taxValue->type=="MSP Fees"){
					$mspFeesCurrent = round($taxValue->value/100*$toApply,2);
					$html .='<td width="17%" align="right">$'.$mspFeesCurrent.'</td>';
					$mspFees = $mspFees+$mspFeesCurrent;
					$totalMSPFeesPercent=$taxValue->value;
				}
			}

			if($mspFees==0){

				$html .='<td  width="17%" align="right">$'.$mspFees.'</td>';
			}
			$totalMSPFees = $totalMSPFees+$mspFees;

			foreach($consolidatedTaxModel as $taxValue) {
				if($taxValue->type=="Discount"){
					$discountCurrent = round($taxValue->value/100*$toApply,2);
					$html .='<td width="10%" align="right">$'.$discountCurrent.'</td>';
					$discount = $discount+$discountCurrent;
					$totalDiscountPercent=$taxValue->value;
				}
			}
			if($discount==0){
				$html .='<td width="10%" align="right">$'.$discount.'</td>';
			}
			$totalDiscount = $totalDiscount+$discount;
			$html .='<td width="12%" align="right">$'.($invoiceReader['total_bilrate_with_tax']+$mspFees-$discount).'</td></tr>';
			//$total = $total+$invoiceReader['total_bilrate_with_tax'];
			$total = $total+$invoiceReader['total_bilrate_with_tax']+$mspFees-$discount;

		}
		$html.='<tr bgcolor="#eee"><td colspan="4" align="left"></td><td align="right"><strong>$'.$totalMSPFees.'('.$totalMSPFeesPercent.'%)</strong></td><td align="right"><strong>$'.$totalDiscount.'('.$totalDiscountPercent.'%)</strong></td><td align="right"><strong>$'.$total.'</strong></td></tr>';

		$sTotal = $total;//echo round($sTotal,2);
		$html.='<tr bgcolor="#eee"><td colspan="6" align="left"><strong>Subtotal</strong></td><td align="right"><strong>$'.$sTotal.'</strong></td></tr>';
		/*$tax = 0;
        $discount = 0;
        $cTax = 0;
        $mspFees = 0;
        $gTax=0;
        foreach($consolidatedTaxModel as $taxValue) {
      if($taxValue->type=="MSP Fees"){
          $mspFeesCurrent = round($taxValue->value/100*$sTotal,2);
          $mspFees = $mspFees+$mspFeesCurrent;
          $html .='<tr bgcolor="#eee"><td colspan="4" align="left"><strong>MSP Fees ('.$taxValue->value.'%)'.'</strong></td><td align="right"><strong>$'.$mspFeesCurrent.'</strong></td></tr>';
       }else if($taxValue->type=="Discount"){
          $discountCurrent = round($taxValue->value/100*$sTotal,2);
          $discount = $discount+$discountCurrent;
          $html .='<tr bgcolor="#eee"><td colspan="4" align="left"><strong>Discount ('.$taxValue->value.'%)'.'</strong></td><td align="right"><strong>$'.$discountCurrent.'</strong></td></tr>';
       }else if($taxValue->type=="Genral Tax"){
          $currentValue = $taxValue->value/100*$sTotal;
          $gTax = $gTax+$currentValue;
          $html .='<tr bgcolor="#eee"><td colspan="4" align="left"><strong>Genral Tax - '.$taxValue->label.'</strong></td><td align="right"><strong>$'.$currentValue.'</strong></td></tr>';
       }else if($taxValue->type=="Customizable Tax"){
          $currentValue = $taxValue->value/100*$sTotal;
          $cTax = $cTax+$currentValue;
          $html .='<tr bgcolor="#eee"><td colspan="4" align="left"><strong>Customizable Tax - '.$taxValue->label.'</strong></td><td align="right"><strong>$'.$currentValue.'</strong></td></tr>';
       } }*/

		$dueAmount = round($total,2);
		$html .='<tr bgcolor="#ccc"><td colspan="6" align="left"><strong>Total</strong></td><td align="right"><strong>$'.$dueAmount.'</strong></td></tr>';
		$html .='<tr><td colspan="6" align="left"><strong>Amount Paid</strong></td><td align="right">$0</td></tr>';
		$html .='<tr><td colspan="6" align="left"><strong>MSP Fees</strong></td><td align="right">$'.$totalMSPFees.'('.$totalMSPFeesPercent.'%)</td></tr>';
		$html .='<tr><td colspan="6" align="left"><strong>Amount Due</strong></td><td ><strong>$'.$dueAmount.'</strong></td></tr>';
		$html.='</tbody></table><br>';

		if(!empty($_GET['summmary'])) {
			error_reporting(0);
			$nextHtml ='';
			$pages=array();
			foreach($invoiceIDs as $value) {

				$generatedModel = GeneratedInvoice::model()->findByPk($value);
				$clientModel = Client::model()->findByPk($generatedModel->client_id);
				$invoiceClientModel = InvoiceClientVend::model()->findByAttributes(array('client_id'=>$clientModel->id));

				$criteria=new CDbCriteria;
				$join = 'INNER JOIN  vms_invoice_tax invoice ON t.invoice_tax_id=invoice.id
                 INNER JOIN  vms_generated_invoice ginvoice ON t.invoice_id=ginvoice.id';
				$criteria->join=$join;

				$criteria->condition="t.invoice_id=".$generatedModel->id;

				$invoiceInvoiceTax = InvoiceInvoiceTax::model()->findAll($criteria);


				$candidateModel = Candidates::model()->findByPk($generatedModel->candidate_id);
				$timesheetDetail = CpTimesheet::model()->findByAttributes(array('id'=>$generatedModel->timsheet_id));
				$vendorModel = Vendor::model()->findByPk($generatedModel->vendor_id);
				$clientApprover = Client::model()->findByPk($timesheetDetail->approval_manager);
				$offerModel = Offer::model()->findByPk($timesheetDetail->offer_id);
				$jobModel = Job::model()->findByPk($generatedModel->job_id);
				$contractModel = Contract::model()->findByPk($timesheetDetail->contract_id);
				$workorderModel = Workorder::model()->findByPk($timesheetDetail->workorder_id);


				if(!$clientApprover){
					$clientApprover = $clientModel;
				}
				$offerModel = Offer::model()->findByPk($timesheetDetail->offer_id);
				$nextHtml = '<br /><br /><br /><table border="0.5" style="border: 1px solid red" cellpadding="3" align="left">
        <tr><td>Invoice Number</td><td>'.$generatedModel->serial_number.'</td></tr>
        <tr><td>Invoice Created Date</td><td>'.date("m/d/Y",strtotime($generatedModel->date_created)).'</td></tr>
        			<tr><td>Candidate Name</td><td>'.$candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name." (".$candidateModel->id.")".'</td></tr>
        			<tr><td>Supplier Organization name</td><td>'.$vendorModel->organization.' ('.$vendorModel->first_name.' '.$vendorModel->last_name.')</td></tr>
        			<tr><td>Job ID</td><td>'.$jobModel->id.'</td></tr>
			<tr><td>Job Name</td><td>'.$jobModel->title.')</td></tr>
			<tr><td>Contract ID</td><td>'.$timesheetDetail->contract_id.'</td></tr>
			<tr><td>Offer ID </td><td>'.$timesheetDetail->offer_id.'</td></tr>';

				$nextHtml .= '<tr><td>Bill Rate</td><td>$'.$timesheetDetail->regular_billrate.'</td></tr>
			<tr><td>Over Time Rate</td><td>$'.$timesheetDetail->overtime_billrate.'</td></tr>
			<tr><td>Double Time Rate</td><td>$'.$timesheetDetail->doubletime_billrate.'</td></tr>';

				$nextHtml .= '<tr><td>Time Sheet From</td><td>'.date("m/d/Y",strtotime($timesheetDetail->invoice_start_date)).'</td></tr>
			<tr><td>Time Sheet To</td><td>'.date("m/d/Y",strtotime($timesheetDetail->invoice_end_date)).'</td></tr>
        			<tr><td>Time Sheet Approval </td><td>'.$clientApprover->first_name.' '.$clientApprover->last_name.'</td></tr>
        			<tr><td>Time Sheet Approved on</td><td>'.date("m/d/Y H:i:s",strtotime($timesheetDetail->approve_date_time)).'</td></tr></table>';


				$startDate = strtotime($generatedModel->invoice_start_date);
				$endDate = strtotime($generatedModel->invoice_end_date);
				$datediff = abs($startDate - $endDate);

				//$days = $datediff / (60 * 60 * 24));

				$begin = new DateTime($generatedModel->invoice_start_date);
				$end = new DateTime($generatedModel->invoice_end_date);
				$end = $end->modify( '+1 day' );

				$interval = DateInterval::createFromDateString('1 day');
				$period = new DatePeriod($begin, $interval, $end);



				$nextHtml .= '<br />Time Sheet Code: '.$timesheetDetail->id.'<br /><table border="0.5" style="border: 1px solid red" cellpadding="3" align="left">
						<tr><td width="40%"></td><td width="20%">Regular Hours</td><td width="20%">Over Time</td><td width="20%">Double Time</td></tr>';
				$rT=$oT=$dT=0;
				foreach ( $period as $dt ) {
					$detailForRegular = CpTimesheetDetails::model()->findAll(array('condition'=>'timesheet_id='.$timesheetDetail->id.' and hours_type="Regular Hour" and creation_day="'.$dt->format("Y-m-d").'"'));
					$detailForOverTime = CpTimesheetDetails::model()->findAll(array('condition'=>'timesheet_id='.$timesheetDetail->id.' and hours_type="Over Time" and creation_day="'.$dt->format("Y-m-d").'"'));
					$detailForDoubleTime = CpTimesheetDetails::model()->findAll(array('condition'=>'timesheet_id='.$timesheetDetail->id.' and hours_type="Double Time" and creation_day="'.$dt->format("Y-m-d").'"'));
					$rHours = 0;
					foreach($detailForRegular as $rValue){
						$rHours = $rHours+$rValue->no_of_hours;
					}
					$oHours = 0;
					foreach($detailForOverTime as $oValue){
						$oHours = $oHours+$oValue->no_of_hours;
					}
					$dHours = 0;
					foreach($detailForDoubleTime as $dValue){
						$dHours = $dHours+$dValue->no_of_hours;
					}
					$nextHtml .= '<tr><td width="40%">'.$dt->format( "l").'</td><td width="20%">'.$rHours.'</td><td width="20%">'.$oHours.'</td><td width="20%">'.$dHours.'</td></tr>';

					$rT = $rT+$rHours;
					$oT = $oT+$oHours;
					$dT = $dT+$dHours;
				}

				$nextHtml .= '<tr><td width="40%">Total Hours</td><td width="20%">'.$rT.'</td><td width="20%">'.$oT.'</td><td width="20%">'.$dT.'</td></tr>
			<tr><td width="40%">Total Cost</td><td width="20%">$'.$timesheetDetail->total_regular_billrate.'</td><td width="20%">$'.$timesheetDetail->total_overtime_billrate.'</td><td width="20%">$'.$timesheetDetail->total_doubletime_billrate.'</td></tr>';

				$total = round($generatedModel->total_billrate,2);
				$sTotal = round($total,2);

				//$nextHtml.='<tr bgcolor="#eee"><td colspan="3" align="left"><strong>Subtotal</strong></td><td align="right"><strong>$'.$sTotal.'</strong></td></tr>';
				$nextHtml .= '
			<tr><td colspan="2">Tax</td><td width="20%">Percentage</td><td align="right" width="20%">Amount</td></tr>';



				$tax = 0;
				foreach($invoiceInvoiceTax as $taxValue) {
					$invoice= InvoiceTax::model()->findByPk($taxValue->invoice_tax_id);
					$taxCurrent = $invoice->value/100*$sTotal;
					$tax = $tax+$taxCurrent;
					$nextHtml .='<tr><td colspan="2">'.$invoice->label.'</td><td width="20%">'.$invoice->value.'%</td><td  align="right" width="20%">$'.$taxCurrent.'</td></tr>';
				}
				$dueAmount = $total+$tax;
				$nextHtml .='<tr><td colspan="3">Total Amount  (Including Taxes)</td><td align="right" width="20%">$'.$dueAmount.'</td></tr></table>';

				$pages[] = $nextHtml;
			}
		}else{
			$nextHtml='';
		}

		//$html .='</div>';
		$footer = "<small class='p-t-2'><strong>PAYMENT TERMS AND POLICIES</strong> All accounts are to be paid within 7 days from receipt of invoice. To be paid by cheque or credit card or direct payment online. If account is not paid within 7 days the credits details supplied as confirmation of work undertaken will be charged the agreed quoted fee noted above. If the Invoice remails unpaid. our dept recovery agency, Urban, may charge you a fee of 25% of the unpaid portion of the invoice amount and other legal and collection costs not covered by the fee.</small>";

		//include( Yii::getPathOfAlias('ext.tcpdf.tcpdf').'.php' );
		// create new PDF document



		$pdf = new CusTcpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);
		$pdf->pdfHeaderText=$title;
		$pdf->pdfFooterText=$footer;
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('sdfsdf');
		$pdf->SetTitle('TCPDF  065');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, ''.$title, '');

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->SetFont('helvetica' ,'BI', 10,'', false);



		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		$pdf->SetFont('helvetica', '', 11, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();
		//$pdf->setPageMark();
// Set some content to print
		$htmlFirstPage = <<<EOD
         $html
EOD;
		if(!empty($_GET['summmary'])) {
			$htmlArr = array();
			foreach($pages as $key=>$value){
				$htmlArr[] = <<<EOD
         $value
         
EOD;
			}}


		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $htmlFirstPage, 0, 1, 0, true, '', true);

		if(!empty($_GET['summmary'])) {
			foreach($htmlArr as $key=>$value){
				$pdf->AddPage();
				$pdf->writeHTMLCell(0, 0, '', '', $value, 0, 1, 0, true, '', true);
			}
		}



		$pdf->Output($fileName.'.pdf','D');


	}
	public function actionPdfDownload($id){
		error_reporting(0);
		$generatedModel = GeneratedInvoice::model()->findByPk($id);
		$clientModel = Client::model()->findByPk($generatedModel->client_id);
		$invoiceClientModel = InvoiceClientVend::model()->findByAttributes(array('client_id'=>$clientModel->id));

		$criteria=new CDbCriteria;
		$join = 'INNER JOIN  vms_invoice_tax invoice ON t.invoice_tax_id=invoice.id
                 INNER JOIN  vms_generated_invoice ginvoice ON t.invoice_id=ginvoice.id';
		$criteria->join=$join;

		$criteria->condition="t.invoice_id=".$generatedModel->id;

		$invoiceInvoiceTax = InvoiceInvoiceTax::model()->findAll($criteria);


		$candidateModel = Candidates::model()->findByPk($generatedModel->candidate_id);
		$timesheetDetail = CpTimesheet::model()->findByAttributes(array('id'=>$generatedModel->timsheet_id));

		$vendorModel = Vendor::model()->findByPk($generatedModel->vendor_id);
		$clientApprover = Client::model()->findByPk($timesheetDetail->approval_manager);
		$offerModel = Offer::model()->findByPk($timesheetDetail->offer_id);
		$jobModel = Job::model()->findByPk($generatedModel->job_id);
		$contractModel = Contract::model()->findByPk($timesheetDetail->contract_id);
		$workorderModel = Workorder::model()->findByPk($timesheetDetail->workorder_id);


		if(!$clientApprover){
			$clientApprover = $clientModel;
		}
		$offerModel = Offer::model()->findByPk($timesheetDetail->offer_id);

		$locationModel = Location::model()->findByPk($offerModel->approver_manager_location);
		if(!$locationModel){
			$locationModel = Location::model()->findByPk($clientModel->id);

		}



		$html='';
		// $title='Simplify VMS- Invoice #'.$generatedModel->serial_number.', Issued on '.date("m/d/Y",strtotime($generatedModel->date_created));
		//$title="<table><tr><td align='left' width='50%'><strong>".$clientModel->business_name."</strong><div><small><br />".$locationModel->name."<br />".$locationModel->address1."<br />".$clientApprover->email."</small></div></td><td align='right' width='50%'><strong>INVOICE</strong><div ><small><br />".date("m/d/Y",strtotime($generatedModel->date_created))."<br />Invoice #".$generatedModel->serial_number."<br />WO ".$timesheetDetail->workorder_id."</small></div></td></tr></table><br /><hr />";
		$fileName = 'Invoice#'.$generatedModel->serial_number.'-'.date("m/d/Y",strtotime($generatedModel->date_created));
		$currentDate=date("Y-m-d");
		$startDate =date("Y-m-d");



		$total = 0;

		$title='<table><tr><td align="left" width="50%"><strong>'.$clientModel->business_name.'</strong><div><small><br />'.$locationModel->name.'<br />'.$locationModel->address1.'<br />'.$clientApprover->email.'</small></div></td><td align="right" width="50%"><strong>INVOICE</strong><div ><small><br />Invoice Date '.date("m/d/Y",strtotime($generatedModel->date_created)).'<br />Invoice #'.$generatedModel->serial_number.'<br />PO '.$timesheetDetail->workorder_id.'</small></div></td></tr></table>';

		$html .='<br /><hr /><br /><br /><p class="m-b-0"><strong>Candidate</strong><br />'.$candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name." (".$candidateModel->id.")".'</p><p class="m-b-0"><strong>Vendor Name</strong><br />'.$vendorModel->organization.' ('.$vendorModel->first_name.' '.$vendorModel->last_name.')</p><br /><div class="p-t-2 p-b-2 clearfix"><div class="overflow-hidden">
                                <table border="0.5" style="border: 1px solid red" cellpadding="5" align="left">
                                <tr bgcolor="#ccc"><th>Description</th><th align="right">Hours</th><th align="right">Unit Price</th><th align="right">Amount</th></tr></thead><tbody>
       <tr bgcolor="#eee" style=" border-bottom: none !important;
"><td>Regular Hours</td><td align="right">'.$timesheetDetail->total_regular_hours.'</td><td align="right">$'.$timesheetDetail->regular_billrate.'</td><td align="right">$'.$timesheetDetail->total_regular_billrate.'</td></tr><tr bgcolor="#eee"><td>Over Time</td><td align="right">'.$timesheetDetail->total_overtime_hours.'</td><td align="right">$'.$timesheetDetail->overtime_billrate.'</td><td align="right">$'.$timesheetDetail->total_overtime_billrate.'</td></tr><tr bgcolor="#eee"><td>Double Time</td><td align="right">'.$timesheetDetail->total_doubletime_hours.'</td><td align="right">$'.$timesheetDetail->doubletime_billrate.'</td><td align="right">$'.$timesheetDetail->total_doubletime_billrate.'</td></tr>';$total = round($total+$generatedModel->total_billrate,2);$sTotal = round($total,2);
		$html.='<tr bgcolor="#eee"><td colspan="3" align="left"><strong>Subtotal</strong></td><td align="right"><strong>$'.$sTotal.'</strong></td></tr>';
		$tax = 0;
		foreach($invoiceInvoiceTax as $taxValue) {
			$invoice= InvoiceTax::model()->findByPk($taxValue->invoice_tax_id);
			$taxCurrent = round($invoice->value/100*$sTotal,2);
			$tax = $tax+$taxCurrent;
			$html .='<tr bgcolor="#eee"><td colspan="3" align="left"><strong>'.$invoice->label.' ('.$invoice->value.'%)'.'</strong></td><td align="right"><strong>$'.$taxCurrent.'</strong></td></tr>';
		}
		$dueAmount = $total+$tax;
		$html .='<tr bgcolor="#ccc"><td colspan="3" align="left"><strong>Total</strong></td><td align="right"><strong>$'.$dueAmount.'</strong></td></tr>';
		//$html .='<tr><td style="width:75%">Amount Paid</td><td style="width:25%">$0</td></tr>';
		//$html .='<tr><td style="width:75%"><strong>Amount Due</strong></td><td style="width:25%"><strong>$'.$dueAmount.'</strong></td></tr>';
		$html.='</tbody></table><br>';

		if(!empty($_GET['summmary'])) {
			$nextHtml = '<br /><br /><br /><br /><table border="0.5" style="border: 1px solid red" cellpadding="5" align="left">
        			<tr><td>Candidate Name</td><td>'.$candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name." (".$candidateModel->id.")".'</td></tr>
        			<tr><td>Supplier Organization name</td><td>'.$vendorModel->organization.' ('.$vendorModel->first_name.' '.$vendorModel->last_name.')</td></tr>
        			<tr><td>Time Sheet Duration</td><td>'.$timesheetDetail->invoice_duration.'</td></tr>
        			<tr><td>Time Sheet Approval </td><td>'.$clientApprover->first_name.' '.$clientApprover->last_name.'</td></tr>
        			<tr><td>Time Sheet Approved on</td><td>'.date("m/d/Y H:i:s",strtotime($timesheetDetail->approve_date_time)).'</td></tr>
        			</table>';

			$nextHtml .= '<br /><br /><table border="0.5" style="border: 1px solid red" cellpadding="5" align="left">
			<tr><td>Job ID</td><td>'.$jobModel->id.'</td></tr>
			<tr><td>Job Name</td><td>'.$jobModel->title.')</td></tr>
			<tr><td>Contract ID</td><td>'.$timesheetDetail->contract_id.'</td></tr>
			<tr><td>Offer ID </td><td>'.$timesheetDetail->offer_id.'</td></tr></table>';
			$nextHtml .= '<br /><br /><table border="0.5" style="border: 1px solid red" cellpadding="5" align="left">
			<tr><td>Bill Rate</td><td>$'.$timesheetDetail->regular_billrate.'</td></tr>
			<tr><td>Over Time Rate</td><td>$'.$timesheetDetail->overtime_billrate.'</td></tr>
			<tr><td>Double Time Rate</td><td>$'.$timesheetDetail->doubletime_billrate.'</td></tr></table>';


			$startDate = strtotime($generatedModel->invoice_start_date);
			$endDate = strtotime($generatedModel->invoice_end_date);
			$datediff = abs($startDate - $endDate);

			//$days = $datediff / (60 * 60 * 24));

			$begin = new DateTime($generatedModel->invoice_start_date);
			$end = new DateTime($generatedModel->invoice_end_date);
			$end = $end->modify( '+1 day' );

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);



			$nextHtml .= '<br /><br /><br />Time Sheet Breakup<br />Time Sheet Code: '.$timesheetDetail->id.'<br /><br /><table border="0.5" style="border: 1px solid red" cellpadding="5" align="left">
						<tr><td width="40%"></td><td width="20%">Regular Hours</td><td width="20%">Over Time</td><td width="20%">Double Time</td></tr>';
			$rT=$oT=$dT=0;
			foreach ( $period as $dt ) {
				$detailForRegular = CpTimesheetDetails::model()->findAll(array('condition'=>'timesheet_id='.$timesheetDetail->id.' and hours_type="Regular Hour" and creation_day="'.$dt->format("Y-m-d").'"'));
				$detailForOverTime = CpTimesheetDetails::model()->findAll(array('condition'=>'timesheet_id='.$timesheetDetail->id.' and hours_type="Over Time" and creation_day="'.$dt->format("Y-m-d").'"'));
				$detailForDoubleTime = CpTimesheetDetails::model()->findAll(array('condition'=>'timesheet_id='.$timesheetDetail->id.' and hours_type="Double Time" and creation_day="'.$dt->format("Y-m-d").'"'));
				$rHours = 0;
				foreach($detailForRegular as $rValue){
					$rHours = $rHours+$rValue->no_of_hours;
				}
				$oHours = 0;
				foreach($detailForOverTime as $oValue){
					$oHours = $oHours+$oValue->no_of_hours;
				}
				$dHours = 0;
				foreach($detailForDoubleTime as $dValue){
					$dHours = $dHours+$dValue->no_of_hours;
				}
				$nextHtml .= '<tr><td width="40%">'.$dt->format( "l").'</td><td width="20%">'.$rHours.'</td><td width="20%">'.$oHours.'</td><td width="20%">'.$dHours.'</td></tr>';

				$rT = $rT+$rHours;
				$oT = $oT+$oHours;
				$dT = $dT+$dHours;
			}

			$nextHtml .= '<tr><td>Total Hours</td><td>'.$rT.'</td><td>'.$oT.'</td><td>'.$dT.'</td></tr>
			<tr><td>Total Cost</td><td>$'.$timesheetDetail->total_regular_billrate.'</td><td>'.$timesheetDetail->total_overtime_billrate.'</td><td>'.$timesheetDetail->total_doubletime_billrate.'</td></tr></table>';
		}else{
			$nextHtml='';
		}

		//$html .='</div>';
		$footer = "<small class='p-t-2'><strong>PAYMENT TERMS AND POLICIES</strong> All accounts are to be paid within 7 days from receipt of invoice. To be paid by cheque or credit card or direct payment online. If account is not paid within 7 days the credits details supplied as confirmation of work undertaken will be charged the agreed quoted fee noted above. If the Invoice remails unpaid. our dept recovery agency, Urban, may charge you a fee of 25% of the unpaid portion of the invoice amount and other legal and collection costs not covered by the fee.</small></div></div>";

		//include( Yii::getPathOfAlias('ext.tcpdf.tcpdf').'.php' );
		// create new PDF document



		$pdf = new CusTcpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);
		$pdf->pdfHeaderText=$title;
		$pdf->pdfFooterText=$footer;
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('sdfsdf');
		$pdf->SetTitle('TCPDF  065');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, ''.$title, '');

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		$pdf->SetFont('helvetica', '', 11, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();
		//$pdf->setPageMark();
// Set some content to print
		$html2 = <<<EOD
         $html
         
EOD;
		$html3 = <<<EOD
         $nextHtml
         
EOD;


		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html2, 0, 1, 0, true, '', true);

		if(!empty($_GET['summmary'])) {
			$pdf->AddPage();
			$pdf->writeHTMLCell(0, 0, '', '', $html3, 0, 1, 0, true, '', true);
		}

		$pdf->Output($fileName.'.pdf','D');

		/* <table  cellpadding="3" align="right" width="30%">
   <tr><td>Subtotal</td></td>'.$sTotal = $total.'</td></tr>';


             $tax = 0;
             foreach($invoiceInvoiceTax as $taxValue) {
                 $invoice= InvoiceTax::model()->findByPk($taxValue->invoice_tax_id);
               $html .='<tr><td>'.$invoice->label.'</td><td>$'.
               $tax = $tax+$invoice->value/100*$sTotal.' ('.$invoice->value.'%)';
               $html.='</td></tr>';
           }
           $dueAmount = $total+$tax;
           $html .='<tr><td>Total</td><td>$'.$dueAmount.'</td></tr>';
           $html .='<tr><td>Amount Paid</td><td>$0</td></tr>';
           $html .='<tr><td>Amount Due</td><td>$'.$dueAmount.'</td></tr>';
           $html .='</table>';


            $html .='<small class="p-t-2"><strong>PAYMENT TERMS AND POLICIES</strong>All accounts are to be paid within 7 days from receipt of invoice. To be paid by cheque or credit card or direct payment online. If account is not paid within 7 days the credits details supplied as confirmation of work undertaken will be charged the agreed quoted fee noted above. If the Invoice remails unpaid. our dept recovery agency, Urban, may charge you a fee of 25% of the unpaid portion of the invoice amount and other legal and collection costs not covered by the fee.</small>';
   */

	}
	public function actionView($id)
	{   //error_reporting(0);
		$generatedModel = GeneratedInvoice::model()->findByPk($id);
		$clientModel = Client::model()->findByPk($generatedModel->client_id);
		$invoiceClientModel = InvoiceClientVend::model()->findByAttributes(array('client_id'=>$clientModel->id));

		$criteria=new CDbCriteria;
		$join = 'INNER JOIN  vms_invoice_tax invoice ON t.invoice_tax_id=invoice.id
                 INNER JOIN  vms_generated_invoice ginvoice ON t.invoice_id=ginvoice.id';
		$criteria->join=$join;

		$criteria->condition="t.invoice_id=".$generatedModel->id;

		$invoiceInvoiceTax = InvoiceInvoiceTax::model()->findAll($criteria);

		$this->render('view',array(
			'generatedModel'=>$generatedModel,'clientModel'=>$clientModel,'invoiceClientModel'=>$invoiceClientModel,'invoiceInvoiceTax'=>$invoiceInvoiceTax));
	}
	public function actionAdmin()
	{
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$month = $_GET['month'];

		$criteriaApproved=new CDbCriteria;
		$criteriaApproved->addCondition("client_id=".$loginUserId);
		$join = 'INNER JOIN  vms_candidates can ON t.candidate_id=can.id
                 INNER JOIN  vms_vendor ven ON t.vendor_id=ven.id';
		$criteriaApproved->join=$join;

		$criteriaConsolidated=new CDbCriteria;
		$criteriaConsolidated->addCondition("client_id=".$loginUserId);
		$criteriaConsolidated->join=$join;

		$criteriaPending=new CDbCriteria;
		$criteriaPending->addCondition("client_id=".$loginUserId);
		$criteriaPending->join=$join;

		if(!empty($_POST['s'])) {
			$search = trim($_POST['s']);
			//$criteriaMonth->condition=" client_month ='".$currentMonth."' and (can.first_name like '%".$search."%' or can.middle_name like '%".$search."%' or can.last_name like '%".$search."%' or t.id like '%".$search."%' or ven.organization like '%".$search."%' or t.total_hours like '%".$search."%' or t.total_billrate like '%".$search."%')";
			$criteriaApproved->condition="  (can.first_name like '%".$search."%' or can.middle_name like '%".$search."%' or can.last_name like '%".$search."%' or t.id like '%".$search."%' or ven.organization like '%".$search."%' or t.total_hours like '%".$search."%' or t.total_billrate like '%".$search."%')";

			$criteriaPending->condition="t.status=1 and (can.first_name like '%".$search."%' or can.middle_name like '%".$search."%' or can.last_name like '%".$search."%' or t.id like '%".$search."%' or ven.organization like '%".$search."%' or t.total_hours like '%".$search."%' or t.total_billrate like '%".$search."%')";

			$criteriaConsolidated->condition="(can.first_name like '%".$search."%' or can.middle_name like '%".$search."%' or can.last_name like '%".$search."%' or t.id like '%".$search."%' or ven.organization like '%".$search."%' or t.total_hours like '%".$search."%' or t.total_billrate like '%".$search."%')";
		}else{
			$criteriaApproved->condition="client_month ='".$month."' and t.status=2 and t.consolidate_invoice_generated !=1";
			$criteriaPending->condition="client_month ='".$month."' and t.status in(0,1) and t.consolidate_invoice_generated !=1";
			$criteriaConsolidated->condition="client_month ='".$month."' and t.consolidate_invoice_generated=1";
		}
		//

		$modelApproved=GeneratedInvoice::model()->findAll($criteriaApproved);
		$modelPending=GeneratedInvoice::model()->findAll($criteriaPending);
		$modelConsolidated=GeneratedInvoice::model()->findAll($criteriaConsolidated);

		$this->render('admin',array(
			'modelApproved'=>$modelApproved,'modelPending'=>$modelPending,'modelConsolidated'=>$modelConsolidated,'month'=>$month
		));
	}
	public function actionAdminStatistic()
	{
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$currentMonth = date('F-Y', strtotime(date('Y-m-d')));

		$criteriaMonth=new CDbCriteria;
		$join = 'INNER JOIN  vms_candidates can ON t.candidate_id=can.id
                 INNER JOIN  vms_vendor ven ON t.vendor_id=ven.id';

		$criteriaAll=new CDbCriteria;
		$criteriaAll->addCondition("client_id=".$loginUserId);
		$criteriaAll->order="t.invoice_start_date DESC";
		$modelList=GeneratedInvoice::model()->findAll($criteriaAll);
		error_reporting(0);
		$invoiceArr =  array();
		foreach($modelList as $value){

			$sql = "select count(contract_id) as t_contract,count(id) as t_timesheet from cp_timesheet  where  timesheet_status=1 and id=".$value->timsheet_id;

			$countReader = Yii::app()->db->createCommand($sql)->queryRow();


			$sql = "select count(*) as pending_timesheet from cp_timesheet tsheet where timesheet_status=0 and id=".$value->timsheet_id;

			$countPendingReader = Yii::app()->db->createCommand($sql)->queryRow();


			$previousArr = $invoiceArr[$value->client_month];

			if ($countReader['t_timesheet'] !='0' && array_key_exists($value->client_month, $invoiceArr)) {
				$contract =  $previousArr['tContract']+1;
				$timesheet = $previousArr['tTimesheet']+1;
			}else if (array_key_exists($value->client_month, $invoiceArr)) {
				$contract =  $previousArr['tContract'];
				$timesheet = $previousArr['tTimesheet'];
			}else{
				$contract =  $countReader['t_contract'] !='0'?1:0;;
				$timesheet =  $countReader['t_timesheet'] !='0'?1:0;;
			}

			if ($countPendingReader['pending_timesheet'] !='0' && array_key_exists($value->client_month, $invoiceArr)) {
				$pendingTimesheet = $previousArr['tPendingTimesheet']+1;

			}else if (array_key_exists($value->client_month, $invoiceArr)) {
				$pendingTimesheet = $previousArr['tPendingTimesheet'];
			}else{
				$pendingTimesheet = $countPendingReader['pending_timesheet'] !='0'?1:0;
			}

			$invoiceArr[$value->client_month] = array(
				'tContract'=>$contract,
				'tTimesheet'=>$timesheet,
				'tPendingTimesheet'=>$pendingTimesheet,
				'totalHours'=>$value->total_hours+$previousArr['totalHours'],
				'totalBilrateWithTax'=>$value->total_bilrate_with_tax+$previousArr['totalBilrateWithTax'],
			);


		}

		$this->render('admin_statistic',array(
			'modelList'=>$modelList,'invoiceArr'=>$invoiceArr
		));
	}
	public function actionWorkflowStatus()
	{
		$this->layout = 'login';

		$JobWorkflowEmail = JobWorkflow::model()->findAllByAttributes(array('client_id'=>$_GET['clientid'],'workflow_id'=>$_GET['workflowid'],'job_id'=>$_GET['jobid']));



		$Job = Job::model()->findByPk($_GET['jobid']);

			$EmailSend = JobWorkflow::model()->findAllByAttributes(array('email_sent'=>0,'workflow_id'=>$_GET['workflowid'],'job_id'=>$_GET['jobid']),
				array(
					'order' => 'number_of_approval asc',
					'limit' => 1,
				));

			if($EmailSend){
				$clientData = Client::model()->findByPk($EmailSend[0]['client_id']);

				//email body for approval of workflow

				$loc = unserialize($Job->location);
				$locationsV = '';
				if($loc){
					foreach($loc as $val){
						$location = Location::model()->findByPk($val);
						$locationsV .= $location->name.', ';
					}
				}
				$workFlow = Worklflow::model()->findByPk($Job->work_flow);

				$htmlbody =  '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px,Arial,sans-serif;padding:2% 7%">
            <div>
                
            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$clientData->first_name.' '.$clientData->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">We are waiting for your approval.</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4"><strong>Job ID </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->id.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Name </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->title.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Description </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->description.'</span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Pay Rate </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->pay_rate.' </span><br /><br />
						
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Location </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$locationsV.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Hour for the Job </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->hours_per_week.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Shift Time. </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->shift.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Project Reference Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->pre_reference_code.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Bill Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->billing_code.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Request Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$Job->request_dept.' </span><br /><br />
						
						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Work Flow Approval : </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$workFlow->flow_name.' </span><br /><br />
						

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
                        Thanks,<br> Simplifyvms Team
                    </p>
                </div>
            </div>

            
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';


				$message = Yii::app()->sendgrid->createEmail();
				//shortcut to $message=new YiiSendGridMail($viewsPath);
				$message->setHtml($htmlbody)
					->setSubject('Simplifyvms - Job Approval Invitation')
					->addTo($clientData->email)
					->setFrom('alert@simplifyvms.net');

				/*********************** Mail content **********************/


				$JobWorkflowEmail[0]->job_status = 'Approved';
				//date and time of rejection for history
				$JobWorkflowEmail[0]->status_time = date("Y-m-d H:i:s");

				if($JobWorkflowEmail[0]->save(false)){
					
				}
				$EmailSend[0]->email_sent = 1;
				$EmailSend[0]->save(false);
			}else{
				$job = Job::model()->findByPk($_GET['jobid']);
				$job->jobStatus = '3';
				$job->save(false);

				$JobWorkflowEmail[0]->job_status = 'Approved';
				$JobWorkflowEmail[0]->status_time = date("Y-m-d H:i:s");
				$JobWorkflowEmail[0]->save(false);
			}

			Yii::app()->user->setFlash('success', '<div class="alert alert-success"> 
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
				  <strong>Success!</strong> You have approved job successfully.</div>');
		$this->render('directApprove',array('model'=>$Job));
exit;
	}
	public function actionConsolidateView($id)
	{
		$consolidateModel = ConsolidatedInvoice::model()->findByPk($id);
		$clientModel = Client::model()->findByPk($consolidateModel->client_id);
		$invoiceClientModel = InvoiceClientVend::model()->findByAttributes(array('client_id'=>$clientModel->id));
		$consolidatedTaxModel = ConsolidatedTax::model()->findAll(array('condition'=>'cosolidated_invoice_id='.$consolidateModel->id));

		if(!empty($_POST['reason_for_rejection'])){
			$consolidateModel->attributes = array(
				'status' =>3,
				'reason_for_rejection'=>$_POST['reason_for_rejection'],
				'reject_notes'=>$_POST['reject_notes'],
				'rejection_date'=>date("Y-m-d H:i:s"),

			);
			if($consolidateModel->save(false)){
				Yii::app()->user->setFlash('success', '<strong>Success!</strong> Offer background verification saved successfully.');
			}
		}else if(!empty($_POST['approved_notes'])){
			$consolidateModel->attributes = array(
				'status' =>2,
				'approved_notes'=>$_POST['approved_notes'],
				'approved_date'=>date("Y-m-d H:i:s"),
			);
			if($consolidateModel->save(false)){
				Yii::app()->user->setFlash('success', '<strong>Success!</strong> Offer background verification saved successfully.');
			}
		}

		$this->render('view_consolidate',array(
			'consolidateModel'=>$consolidateModel,'clientModel'=>$clientModel,'invoiceClientModel'=>$invoiceClientModel,'consolidatedTaxModel'=>$consolidatedTaxModel
		));
	}
	public function actionAdminconsolidate()
	{

		$userid = UtilityManager::superClient(Yii::app()->user->id);

		$criteria=new CDbCriteria;
		$criteria->join='INNER JOIN  vms_generated_invoice g ON t.generated_invoice_id=g.id  
                        INNER JOIN  vms_candidates can ON g.candidate_id=can.id
                        INNER JOIN  vms_vendor ven ON g.vendor_id=ven.id';
		$criteria->condition="t.client_id =".$userid;
		$criteria->condition="t.status =1";
		if(!empty($_POST['s'])) {
			$search = trim($_POST['s']);
			$criteria->condition="t.amount_due like '%".$search."%'  or t.serial_number like '%".$search."%' ";
		}

		$consolidateModel = ConsolidatedInvoice::model()->findAll($criteria);

		$this->render('admin_consolidate',array(
			'consolidateModel'=>$consolidateModel,
		));
	}
	public function actionApprovedInvoice(){
		$userid = UtilityManager::superClient(Yii::app()->user->id);
		$criteria=new CDbCriteria;
		$criteria->join='INNER JOIN  vms_generated_invoice g ON t.generated_invoice_id=g.id  
                        INNER JOIN  vms_candidates can ON g.candidate_id=can.id
                        INNER JOIN  vms_vendor ven ON g.vendor_id=ven.id';
		$criteria->condition="t.client_id =".$userid;
		$criteria->condition="t.status =2";
		if(!empty($_POST['s'])) {
			$search = trim($_POST['s']);
			$criteria->condition=" t.amount_due like '%".$search."%'  or t.serial_number like '%".$search."%' ";
		}

		$consolidateModel = ConsolidatedInvoice::model()->findAll($criteria);

		$this->render('admin_consolidate',array(
			'consolidateModel'=>$consolidateModel,
		));
	}
	public function actionSearch(){
		$loginUserId = Yii::app()->user->id;
		
		/** jobs query placed here */
		$jobsearch = '';
		$submissionresult = '';
		$offerresult = '';
		$interviewresult = '';
		$offersearch = '';
		$pages = '';
		if(!empty($_GET['s'])){
			$q = $_GET['s'];
		if (is_numeric($q) && is_int(0+$q)){
			
			 $query = 'select * from vms_job where user_id = "'.$loginUserId.'" and id = '.$q.'';
			$jobsearch = Yii::app()->db->createCommand( $query )->query()->readAll();
			if(!empty($jobsearch)){
			$query = 'select * from vms_vendor_job_submission where client_id = "'.$loginUserId.'" and id = '.$q.'';
			$submissionsearch = Yii::app()->db->createCommand( $query )->query()->readAll();
			}
			if(!empty($submissionsearch)){
			$query = 'select * from vms_offer where client_id = "'.$loginUserId.'" and id = '.$q.'';
			$offersearch = Yii::app()->db->createCommand( $query )->query()->readAll();
			}
			if(!empty($offersearch)){
			$query = 'select * from vms_interview where id = '.$q.'';
			$interviewsearch = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
			// $searchSubmission
			/*echo '<pre>'; print_r($jobsearch); exit;*/
			if(!empty($jobsearch)){
				$query = 'select * from vms_vendor_job_submission where client_id = "'.$loginUserId.'" and job_id ='.$jobsearch[0]['id'];
			$submissionresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				if($jobsearch){
					$query = 'select * from vms_interview where job_id ='.$jobsearch[0]['id'];
					$interviewresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
				if($jobsearch){
				$query = 'select * from vms_offer where client_id = "'.$loginUserId.'" and job_id ='.$jobsearch[0]['id'];
			$offerresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
			}

			/*else if(!empty($submissionsearch)){
				$query = 'select * from vms_vendor_job_submission where client_id = "'.$loginUserId.'" and client_id = and id ='.$submissionsearch[0]['id'];
			$submissionresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				if(!empty($submissionresult)){
				$query = 'select * from vms_job where user_id = "'.$loginUserId.'" and id ='.$submissionsearch[0]['job_id'];
			$jobsearch = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
				if(!empty($jobsearch)){
				$query = 'select * from vms_offer where client_id = "'.$loginUserId.'" and submission_id ='.$submissionsearch[0]['id'];
			$offerresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
				if(!empty($offerresult)){
				$query = 'select * from vms_interview where submission_id ='.$submissionsearch[0]['id'];
			$interviewresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
			}
			else if(!empty($offersearch)){
				
				$query = 'select * from vms_vendor_job_submission where client_id = "'.$loginUserId.'" and id ='.$offersearch[0]['submission_id'];
			$submissionresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				if(!empty($submissionresult)){
				$query = 'select * from vms_job where user_id = "'.$loginUserId.'" and id ='.$offersearch[0]['job_id'];
			$jobsearch = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
				if(!empty($jobsearch)){
				$query = 'select * from vms_offer where client_id = "'.$loginUserId.'" and id ='.$offersearch[0]['id'];
			$offerresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
				if(!empty($offerresult)){
				$query = 'select * from vms_interview where submission_id ='.$offersearch[0]['submission_id'];
			$interviewresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
			}
			else{
				if(!empty($interviewsearch)){
				$query = 'select * from vms_vendor_job_submission where client_id = "'.$loginUserId.'" and id ='.$interviewsearch[0]['submission_id'];
			$submissionresult = Yii::app()->db->createCommand( $query )->query()->readAll();
					}
				if(!empty($submissionresult)){
				$query = 'select * from vms_job where user_id = "'.$loginUserId.'" and id ='.$interviewsearch[0]['job_id'];
			$jobsearch = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
				if(!empty($jobsearch)){
				$query = 'select * from vms_offer where client_id = "'.$loginUserId.'" and submission_id ='.$interviewsearch[0]['submission_id'];
			$offerresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
				if(!empty($offerresult)){
				$query = 'select * from vms_interview where id ='.$interviewsearch[0]['id'];
			$interviewresult = Yii::app()->db->createCommand( $query )->query()->readAll();
				}
			}*/
		}else{
			// here we have to check text returned from textbox like job title.
		} 
			
		}
		/*echo '<pre>';
		print_r($offer); exit;*/
		
		$this->render('search',array('loginUserId'=>$loginUserId ,'jobs'=>$jobsearch,'submission'=>$submissionresult,'offer'=>$offerresult,'interview'=>$interviewresult));
	}
	public function actionKeepAlive()
	{
		echo 'OK';
		Yii::app()->end();
	}
	
	public function actionOAuth2Callback()
    {
		/*echo Yii::app()->basePath;
		exit;*/		
        if(Yii::app()->user->isGuest)
        {
            throw new CHttpException(403,"Only registered users can user OAuth for Google Calendar.");
        }
        
        $model = Client::model()->findByPk(Yii::app()->user->id);
        
        if($model->google_access_token)
        {
            throw new CHttpException(403,"You are already auth in google calendar.");
        }
        
        require_once Yii::getPathOfAlias('ext').'/google-api-php-client/vendor/autoload.php';
        
        $client = new Google_Client();
        
        $client->setAuthConfigFile(Yii::app()->params['client_secret_path']);
        $client->setRedirectUri($this->createAbsoluteUrl('default/oAuth2Callback'));
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $client->setAccessType('offline');
        
        if (! isset($_GET['code'])) {
            $auth_url = $client->createAuthUrl();
            header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
        } else {
            
            $client->authenticate($_GET['code']);
            
            $model->google_access_token = json_encode($client->getAccessToken());
            if($model->save())
            {
                Yii::app()->user->setFlash('success','OAuth2.0 complete.');
                $this->redirect(Yii::app()->homeUrl);
            }
            else
            {
                var_dump($model->errors);
            }
        }
    }
	
	
	public function actionIndex()
	{		
		error_reporting(0);
		$Client = Yii::app()->user->getState('isAdmin');
		$profile_approve = Yii::app()->user->getState('profile_approve');
		$clientModel = Client::model()->findByPk(Yii::app()->user->id);
		//$clientModel->super_client_id != 0 ;
		$searchedData = '';
		$connection = Yii::app()->db;
		if(Yii::app()->user->isGuest || ($Client == '' || $Client == false))
		{
			$this->redirect(array('default/login'));
			Yii::app()->end();
		}
		
		$clientModel->scenario='singup2';
		
		if(isset($_POST['Client']))
		{ 
			$clientModel->scenario='create_profile';
			$clientModel->attributes=$_POST['Client'];
			
			$rnd = rand(0,9999);
			if(!empty($_FILES['Client']['name']['profile_image'])){
			$imageUploadFile = CUploadedFile::getInstance($clientModel, 'profile_image');
			   if($imageUploadFile != ""){
				 $fileName = "{$rnd}-{$imageUploadFile}";  // random number + file name
			     $clientModel->profile_image = $fileName;
				}
			}
			
			$clientModel->linkedin_profile=$_POST['Client']['linkedin_profile'];
			$clientModel->twitter_profile=$_POST['Client']['twitter_profile'];
			$clientModel->business_name=$_POST['Client']['business_name'];
			$clientModel->business_type=$_POST['Client']['business_type'];
			$clientModel->total_staff=$_POST['Client']['total_staff'];
			$clientModel->website_url=$_POST['Client']['website_url'];
			$clientModel->revenue_range=$_POST['Client']['revenue_range'];
			$clientModel->describe_business=$_POST['Client']['describe_business'];
			if($clientModel->save()) {
				if(!empty($_FILES['Client']['name']['profile_image'])){
					if($imageUploadFile !==""){ // validate to save file
							$imageUploadFile->saveAs(Yii::app()->basePath.'/../images/profile_img/'.$fileName);
					}
				}
				$this->redirect(array('client/createteammember'));
			}
		}
		//adding this condition for team member that they have no need to approve their accounts// malik
		$mainclient = UtilityManager::superClient(Yii::app()->user->id);
		//condition for hiring manager
		$loginUserId = Yii::app()->user->id;
		$modelClient = Client::model()->findByPk($loginUserId);
		$hiringManagerCond = '';
		$allJobIDs = array();
		if($modelClient->member_type == 'Hiring Manager'){
			$mainCientJobs = Job::model()->findAllByAttributes(array('user_subclient_id'=>$loginUserId));
			foreach($mainCientJobs as $mainCientJobsKey=>$mainCientJobsValue){
				$allJobIDs[$mainCientJobsValue->id] = $mainCientJobsValue->id;
			}
			$hiringManagerCond = " and w.job_id IN ('".implode("','" , $allJobIDs)."')";
		}
		$date = date('Y-m-d');

		$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 2  $hiringManagerCond AND s.id !='".Yii::app()->user->getState('submission_id')."'
			
			and ((w.interview_start_date ='".$date."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$date."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$date."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$date."' and w.alt3_date_status =1))";

		$count_query = "select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 2

			and ((w.interview_start_date ='".$date."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$date."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$date."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$date."' and w.alt3_date_status =1))";

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$todayInterview=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
		
//now showing all the interviews list of a client all jobs
		$sql1 = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".$mainclient."' AND s.id !='".Yii::app()->user->getState('submission_id')."' and w.status = 2  $hiringManagerCond order by w.id desc";

		$count_query1 = 'select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where  w.status = 2 and s.client_id='.$mainclient;

		$item_count1 = Yii::app()->db->createCommand($count_query1)->queryScalar();

		$completedInterviews=new CSqlDataProvider($sql1, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count1,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		if($clientModel->super_client_id != 0){
			 $this->render('index',array('todayInterview'=>$todayInterview,'completedInterviews'=>$completedInterviews));
		}else{
			if(empty($clientModel->business_name) && (!in_array($clientModel->member_type,array('Account Approver','Team Member' )))) 		         {
				$this->layout = 'buss_steps';
			    $this->render('profile_create',array('model'=>$clientModel));
			 }else if($clientModel->skip_teammembers==0){
				$this->redirect(array('client/createteammember'));
				}else if($clientModel->skip_createlocation==0){
				$this->redirect(array('client/createlocation'));
				}else if($clientModel->profile_approve !='Yes') {
				if($clientModel->review_email==0){
			 		UtilityManager::emailByTemplate($clientModel->id,'Client','client-under-review');
					$clientModel->review_email = 1;
					$clientModel->save(false);
				}
			   	$this->render('profile_approve',array('model'=>$clientModel));
			  }else {

			  	 $this->render('index',array('todayInterview'=>$todayInterview,'completedInterviews'=>$completedInterviews));
			  }
		}
		
	}
	
	
	/*
	* Client LOGIN
	*/
	public function actionLogin()
	{
		error_reporting(0);
		$this->layout = 'login';
		$model = new Client;
		
		if(!(Yii::app()->user->isGuest)){
			$this->redirect('index');
			}
		
		
		if(isset($_POST['Client']))
		{
			$model->scenario='signin';
			$model->attributes=$_POST['Client'];
			
			$identity = new UserLogin($_POST['Client']['email'],$_POST['Client']['password']);
			
			if($model->validate()){
			if($identity->authenticate()){
				Yii::app()->user->login($identity);
				Yii::app()->session->remove('error');
				$Logreport = new ClientLogreport;
				$LoginuserInfo = Client::model()->findByPk(Yii::app()->user->id);
				$Logreport->user_id = $LoginuserInfo->id;
				$Logreport->time_in = date('H:i:s');
				$Logreport->first_name = $LoginuserInfo->first_name;
				$Logreport->last_name = $LoginuserInfo->last_name;
				$Logreport->user_permission = $LoginuserInfo->member_type;
				$ip = $_SERVER['REMOTE_ADDR'];
				$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
				$Logreport->country = $details->country;
				$Logreport->ip_address = $ip;
				$Logreport->date_created = date('Y-m-d');
				$Logreport->save();
				$this->redirect(array('default/index'));
				Yii::app()->end();
			}else
			{
				Yii::app()->session->add('error', 'Invalid login information');
				$this->redirect(array('default/login'));
				Yii::app()->end();
			}
			}
			//$model=Department::model()->findByPk((int)$id);
		}
		
		$this->render('login', array('model' => $model));
	}
	
	public function actionForgotPassword() {
        $model=new Client;
		$model->scenario='create_password';
			if(!empty($_POST['Client']['email'])){
				$client=Client::model()->findByAttributes(array('email'=>$_POST['Client']['email']));
				if($client) {
					UtilityManager::emailByTemplate($client->id,'Client','client-forgot-password');
					Yii::app()->user->setFlash('success', '<div class="alert alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  <strong>Success!</strong> Please check your email for reset password link.</div>');
					$this->redirect(array('login'));
				}else {
					Yii::app()->user->setFlash('success', "Email did not find!");
				}
			}else if(isset($_POST['Client'])){
				Yii::app()->user->setFlash('success', "Email did not find!");
			}
	$this->render('forgotpass', array('model' => $model));
	}
	
	
	public function actionResetpassword()
	{
		$this->layout = 'login';
        if( isset($_GET['client_id'])) {
        	//$id =  UrlEnDeCode::getIdDecoded($_GET['client_id']);
        	$search = array('s', 'l', 'm', 'k', 'z','n','o','q','t','d');
		    $replace  = array(0,1, 2, 3, 4, 5,6,7,8,9);
		
		    $id = str_replace($search, $replace, $_GET['client_id']);
        }
		$model=Client::model()->findByPk($id);
       
			
		$model->scenario='create_password';
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
			if(!empty($_POST['Client']['password']) && $_POST['Client']['password']==$_POST['Client_password_confirm'])
			{
				$model->password=md5($_POST['Client']['password']);
				if($model->validate() && $model->save()) {
					Yii::app()->user->setFlash('success', '<div class="alert alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  <strong>Success!</strong> Password changed successfully.</div>');
					$this->redirect(array('login'));
				}
			}else if(isset($_POST['Client'])){
				Yii::app()->user->setFlash('success', "Password did not match!");
			}
			$this->render('accountactiavtion',array(
				'model'=>$model,
			));
		
	}
	/*
	* Client Moblie App LOGIN
	http://localhost/vms1/index.php?r=Client/default/appLogin&email=mr.malikfayyaz@gmail.com&password=malik
	*/
	
	public function actionAppLogin()
	{
		$this->layout = 'login';
		$model = new Client;
		
		if(isset($_POST['email']))
		{		
			$identity = new UserAppLogin($_POST['email'],$_POST['password']);
			$response = array();
			if($model->validate())
			if($identity->authenticate()){
				Yii::app()->user->login($identity);
				
				$response["success"] = 1;
				$response["message"] = "Login Successful";
				$response["client_id"] = Yii::app()->user->id;
				echo json_encode($response);
				exit;
			}else
			{
				$response["success"] = 0;
				$response["message"] = "Incorrect Username or Password";
				echo json_encode($response);
				exit;
			}
		}
		
		$this->render('login', array('model' => $model));
	}
	
	public function actionLogout()
	{
		$ClientLogreport = ClientLogreport::model()->findByAttributes(array('user_id'=>Yii::app()->user->id));
		if($ClientLogreport){
			$ClientLogreport->time_out = date('H:i:s');
			$ClientLogreport->save();
		}
		Yii::app()->user->logout();
		$this->redirect(array('default/login'));
		Yii::app()->end();
	}
	
	
	/********************
	** PRIVATE METHODS **
	********************/
	private function __getModel()
	{
		$model_name = Yii::app()->request->getParam('m');
		
		if(empty($model_name) || !$this->__verifyMethod($model_name))
		{
			$this->redirect(array('default/index'));
			Yii::app()->end();
		}
		
		// loading model
		$model = new $model_name();
		
		if(!is_object($model))
		{
			$this->redirect(array('default/index'));
			Yii::app()->end();
		}
		
		return array('model' => $model, 'model_name' => $model_name);
	}
	
	private function __verifyMethod($model)
	{
		// verify it in config array
		if(!isset($this->_config[$model]) && !is_array($this->_config[$model]))
		{
			return false;
		}
		
		// verify if model exists
		if(!class_exists($model))
		{
			return false;
		}
		
		return true;
	}
	
	public function __loadModel($model_name, $id)
	{ 	
		eval('$model = '.$model_name.'::model()->findByPk((int)$id);');
		
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	} 
	
}