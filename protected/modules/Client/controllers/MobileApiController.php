<?php

class MobileApiController extends Controller
{
	
	public $layout='admin';
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('pendingApprovalListing','login','dashboard','jobDetail','jobListing','newRequest','pendingApproval','jobFilled','otherJobs'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionLogin()
	{		
		$password = md5($_POST['password']);
		$model = Client::model()->findByAttributes(array('email'=>$_POST['username'],'password'=>$password));
		$response = array();
		
		if($model){
			$response["success"] = 1;
			$response["userid"] = $model->id;
			$response["name"] = $model->first_name.' '.$model->last_name;
			$response["message"] = "Success";
			echo json_encode($response);
			}else{
				 $response["success"] = 0;
				 $response["message"] = "Incorrect Username or Password";
				 echo json_encode($response);
				 }
	}
	
	public function actionDashboard()
	{
		$userID = $_POST['userid'];
		$user = Client::model()->findByPk($userID);
		$response = array();
		
		//jobs count query
		$jobs = Job::model()->countByAttributes(array('user_id'=>$userID,'jobStatus'=>11));
		
		//interview count query
		$interviewSql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status=2 and s.client_id=".$userID;
			
		//$interviews = Yii::app()->db->createCommand($interviewSql)->query()->readAll();
		$interviews = Yii::app()->db->createCommand($interviewSql)->query()->count();
		
		//job approval count query
		$pendingJob = Job::model()->countByAttributes(array('user_id'=>$userID,'jobStatus'=>1));
		
		//timesheet approval count query
		$timesheetSql = "SELECT t.*,o.id FROM cp_timesheet_log t,vms_offer o where o.id =t.offer_id and t.status=0 and o.client_id=".$userID;
		$cpTimesheet = Yii::app()->db->createCommand($timesheetSql)->query()->readAll();
	    $timeSheetApprovel = count($cpTimesheet);
		
		if($user){
			$response["success"] = 1;
			$response["jobs"] = $jobs;
			$response["interviews"] = $interviews;
			$response["pendingjobs"] = $pendingJob;
			$response["timesheetapproval"] = $timeSheetApprovel;
			$response["message"] = "Success";
			echo json_encode($response);
			}else{
				$response["success"] = 0;
				$response["message"] = "Error No Record Found!";
				echo json_encode($response);
			 	}
	}
	
	
	public function actionJobListing()
	{
		//$_POST['client_id'] = 41;
		$modelClient=Client::model()->findByPk($_POST['client_id']);
		/*Account Approver*/
		if($modelClient->member_type != NULL){

			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id';
			$Criteria->condition = "vms_teammember_job.teammember_id =".$_POST['client_id'];
			//$Criteria->order = "order by t.id desc";
			if(isset($_POST['s'])) {
				$t = $_POST['t'];
				$Criteria->condition = "t.title like '%".$t."%'";
			}
			$jobsInvitedFor = Job::model()->findAll($Criteria);

			$allJobIDs = array();
			foreach($jobsInvitedFor as $inviteKey=>$inviteValue){
				$allJobIDs[$inviteValue->id] = $inviteValue->id;
			}
			$worklflowJobs = WorklflowMember::model()->findAllByAttributes(array('client_id'=>$_POST['client_id']));

			foreach($worklflowJobs as $key=>$value){
				$JobIDs = Job::model()->findAllByAttributes(array('work_flow'=>$value->workflow_id));
				foreach($JobIDs as $JobIDs){
					$allJobIDs[$JobIDs->id] = $JobIDs->id;
				}
			}


			$criteria = new CDbCriteria();
			$criteria->addInCondition("id", $allJobIDs);
			$criteria->order = "id desc";
			$jobs = Job::model()->findAll($criteria);


		}else{
			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->condition = "user_id=".$_POST['client_id'];
			$Criteria->order = "id desc";

			if(isset($_POST['s'])) {
				$t = $_POST['s'];
				$Criteria->condition = "user_id='".$_POST['client_id']."' and t.title like '%".$t."%'";
			}
			$jobs = Job::model()->findAll($Criteria);

		}
		$response = array();

		if($jobs){
			$response["success"] = 1;
			$response["AllJobs"] = $jobs;
			$response["message"] = "Success";
			echo json_encode($response);
		}else{
			$response["success"] = 0;
			$response["message"] = "Error No Record Found!";
			echo json_encode($response);
		}


	}
	
	public function actionNewRequest()
	{
		//$_POST['client_id'] = 41;
		
		$clientID = $_POST['client_id'];
		$modelClient=Client::model()->findByPk($clientID);
		/*Account Approver*/
		if($modelClient->member_type != NULL){

			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id';
			$Criteria->condition = "vms_teammember_job.teammember_id =".$clientID .' and t.jobStatus = 11 ';
			//$Criteria->order = "order by t.id desc";
			if(isset($_POST['s'])) {
				$t = $_POST['t'];
				$Criteria->condition = "t.title like '%".$t."%'";
			}
			$jobsInvitedFor = Job::model()->findAll($Criteria);

			$allJobIDs = array();
			foreach($jobsInvitedFor as $inviteKey=>$inviteValue){
				$allJobIDs[$inviteValue->id] = $inviteValue->id;
			}
			$worklflowJobs = WorklflowMember::model()->findAllByAttributes(array('client_id'=>$clientID));

			foreach($worklflowJobs as $key=>$value){
				$JobIDs = Job::model()->findAllByAttributes(array('work_flow'=>$value->workflow_id));
				foreach($JobIDs as $JobIDs){
					$allJobIDs[$JobIDs->id] = $JobIDs->id;
				}
			}


			$criteria = new CDbCriteria();
			$criteria->addInCondition("id", $allJobIDs);
			$criteria->order = "id desc";
			$jobs = Job::model()->findAll($criteria);


		}else{
			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->condition = "user_id=".$clientID.' and jobStatus = 11 ';
			$Criteria->order = "id desc";

			if(isset($_POST['s'])) {
				$t = $_POST['s'];
				$Criteria->condition = "user_id='".$clientID."' and t.title like '%".$t."%'";
			}
			$jobs = Job::model()->findAll($Criteria);

		}
		
		$jobArray = array();
		
		foreach($jobs as $jobsK=>$jobsV){
			
			foreach($jobsV as $key1=>$val){
				
				if($key1=='location'){
					$loc = unserialize($val);
					$locationsV = '';
					if($loc){
					foreach($loc as $val){
						$location = Location::model()->findByPk($val);
						$locationsV .= $location->name.', ';
						}
					}
						$jobArray[$jobsK][$key1] = $locationsV;
					}else if($key1=='id'){
						$jobArray[$jobsK]['id'] =  $val;
						}else if($key1=='title'){
						$jobArray[$jobsK]['title'] =  $val;
						}else if($key1=='bill_rate'){
						$jobArray[$jobsK]['bill_rate'] =  $val;
						}
						//else{
							  //older code for job all keys
							  //$jobArray[$jobsK][$key1] = $val;
							  //$jobArray[$jobsK]['jobid'] = $val->id;
							  //$jobArray[$jobsK]['title'] = $val->title;
						//}
					
				}
				//interview count query
				$interviewSql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status=2 and s.job_id = ".$jobsV->id." and s.client_id=".$clientID;
				$interviews = Yii::app()->db->createCommand($interviewSql)->query()->count();
				
				//submission count query
				$submissions = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobsV->id,'resume_status'=>array(3,4,5,6,7,8)));
				
				$jobArray[$jobsK]['submissions'] = $submissions;
				$jobArray[$jobsK]['interviews'] = $interviews;
			}
		
		$response = array();
		
		/*echo '<pre>';
		print_r($jobArray);
		exit;*/
		

		if($jobs){
			$response["success"] = 1;
			$response["AllJobs"] = $jobArray;
			$response["message"] = "Success";
			echo json_encode($response);
		}else{
			$response["success"] = 0;
			$response["message"] = "Error No Record Found!";
			echo json_encode($response);
		}


	}
	
	public function actionPendingApproval()
	{
		//$_POST['client_id'] = 41;
		$clientID = $_POST['client_id'];
		$modelClient=Client::model()->findByPk($clientID);
		/*Account Approver*/
		if($modelClient->member_type != NULL){

			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id';
			$Criteria->condition = "vms_teammember_job.teammember_id =".$clientID.' and t.jobStatus = 1 ';;
			//$Criteria->order = "order by t.id desc";
			if(isset($_POST['s'])) {
				$t = $_POST['t'];
				$Criteria->condition = "t.title like '%".$t."%'";
			}
			$jobsInvitedFor = Job::model()->findAll($Criteria);

			$allJobIDs = array();
			foreach($jobsInvitedFor as $inviteKey=>$inviteValue){
				$allJobIDs[$inviteValue->id] = $inviteValue->id;
			}
			$worklflowJobs = WorklflowMember::model()->findAllByAttributes(array('client_id'=>$clientID));

			foreach($worklflowJobs as $key=>$value){
				$JobIDs = Job::model()->findAllByAttributes(array('work_flow'=>$value->workflow_id));
				foreach($JobIDs as $JobIDs){
					$allJobIDs[$JobIDs->id] = $JobIDs->id;
				}
			}


			$criteria = new CDbCriteria();
			$criteria->addInCondition("id", $allJobIDs);
			$criteria->order = "id desc";
			$jobs = Job::model()->findAll($criteria);


		}else{
			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->condition = "user_id=".$clientID.' and jobStatus = 1 ';;
			$Criteria->order = "id desc";

			if(isset($_POST['s'])) {
				$t = $_POST['s'];
				$Criteria->condition = "user_id='".$clientID."' and t.title like '%".$t."%'";
			}
			$jobs = Job::model()->findAll($Criteria);

		}
		
		$jobArray = array();
		
		foreach($jobs as $jobsK=>$jobsV){
			foreach($jobsV as $key1=>$val){
					
				if($key1=='location'){
					$loc = unserialize($val);
					$locationsV = '';
					/*if($loc){
					foreach($loc as $val){
						$location = Location::model()->findByPk($val);
						$locationsV .= $location->name.', ';
						}
					}*/
						$jobArray[$jobsK][$key1] = $locationsV;
					}else if($key1=='id'){
						$jobArray[$jobsK]['id'] =  $val;
						}else if($key1=='title'){
						$jobArray[$jobsK]['title'] =  $val;
						}else if($key1=='bill_rate'){
						$jobArray[$jobsK]['bill_rate'] =  $val;
						}
					
					
					
					
					
					
				}
			$workflowName = Worklflow::model()->findByPk($jobsV->work_flow);
			$workflowMembers = WorklflowMember::model()->countByAttributes(array('workflow_id'=>$jobsV->work_flow));
			
			$pendingMember = JobWorkflow::model()->findByAttributes(array('email_sent'=>1,'job_status'=>'Pending','workflow_id'=>$jobsV->work_flow), 
						array( 
							'order' => 'number_of_approval asc', 
							'limit' => 1, 
						));
			
			$client = Client::model()->findByPk($pendingMember->client_id);
					
			
				
			$jobArray[$jobsK]['workflowName'] = $workflowName->flow_name;
			$jobArray[$jobsK]['workflowMembers'] = $workflowMembers;
			$jobArray[$jobsK]['pendingMemberName'] = $client->first_name.' '.$client->last_name;
			}
		
		$response = array();
		
			
		if($jobs){
			$response["success"] = 1;
			$response["AllJobs"] = $jobArray;
			$response["message"] = "Success";
			echo json_encode($response);
		}else{
			$response["success"] = 0;
			$response["message"] = "Error No Record Found!";
			echo json_encode($response);
		}


	}
	
	public function actionJobFilled()
	{
		//$_POST['client_id'] = 41;
		
		
		
		$clientID = $_POST['client_id'];
		
		$modelClient=Client::model()->findByPk($clientID);
		/*Account Approver*/
		if($modelClient->member_type != NULL){

			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id';
			$Criteria->condition = "vms_teammember_job.teammember_id =".$clientID.' and t.jobStatus = 4 ';;
			//$Criteria->order = "order by t.id desc";
			if(isset($_POST['s'])) {
				$t = $_POST['t'];
				$Criteria->condition = "t.title like '%".$t."%'";
			}
			$jobsInvitedFor = Job::model()->findAll($Criteria);

			$allJobIDs = array();
			foreach($jobsInvitedFor as $inviteKey=>$inviteValue){
				$allJobIDs[$inviteValue->id] = $inviteValue->id;
			}
			$worklflowJobs = WorklflowMember::model()->findAllByAttributes(array('client_id'=>$clientID));

			foreach($worklflowJobs as $key=>$value){
				$JobIDs = Job::model()->findAllByAttributes(array('work_flow'=>$value->workflow_id));
				foreach($JobIDs as $JobIDs){
					$allJobIDs[$JobIDs->id] = $JobIDs->id;
				}
			}


			$criteria = new CDbCriteria();
			$criteria->addInCondition("id", $allJobIDs);
			$criteria->order = "id desc";
			$jobs = Job::model()->findAll($criteria);


		}else{
			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->condition = "user_id=".$clientID.' and jobStatus = 4 ';;
			$Criteria->order = "id desc";

			if(isset($_POST['s'])) {
				$t = $_POST['s'];
				$Criteria->condition = "user_id='".$clientID."' and t.title like '%".$t."%'";
			}
			$jobs = Job::model()->findAll($Criteria);

		}
		
		$jobArray = array();
		
		foreach($jobs as $jobsK=>$jobsV){
			foreach($jobsV as $key1=>$val){
				if($key1=='location'){
				$loc = unserialize($val);
				$locationsV = '';
				/*if($loc){
				foreach($loc as $val){
					$location = Location::model()->findByPk($val);
					$locationsV .= $location->name.', ';
					}
				}*/
					$jobArray[$jobsK][$key1] = $locationsV;
				}else if($key1=='id'){
					$jobArray[$jobsK]['id'] =  $val;
					}else if($key1=='title'){
					$jobArray[$jobsK]['title'] =  $val;
					}else if($key1=='bill_rate'){
					$jobArray[$jobsK]['bill_rate'] =  $val;
					}
				
				}
			}
		
		$response = array();

		if($jobs){
			$response["success"] = 1;
			$response["AllJobs"] = $jobArray;
			$response["message"] = "Success";
			echo json_encode($response);
		}else{
			$response["success"] = 0;
			$response["message"] = "Error No Record Found!";
			echo json_encode($response);
		}


	}
	
	public function actionOtherJobs()
	{
		//$_POST['client_id'] = 41;
		$clientID = $_POST['client_id'];
		$modelClient=Client::model()->findByPk($clientID);
		/*Account Approver*/
		if($modelClient->member_type != NULL){

			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->join = 'INNER JOIN vms_teammember_job ON vms_teammember_job.job_id = t.id';
			$Criteria->condition = "vms_teammember_job.teammember_id =".$clientID.' and (t.jobStatus = 3 or t.jobStatus = 5 or t.jobStatus = 6 or t.jobStatus = 10) ';
			//$Criteria->order = "order by t.id desc";
			if(isset($_POST['s'])) {
				$t = $_POST['t'];
				$Criteria->condition = "t.title like '%".$t."%'";
			}
			$jobsInvitedFor = Job::model()->findAll($Criteria);

			$allJobIDs = array();
			foreach($jobsInvitedFor as $inviteKey=>$inviteValue){
				$allJobIDs[$inviteValue->id] = $inviteValue->id;
			}
			$worklflowJobs = WorklflowMember::model()->findAllByAttributes(array('client_id'=>$clientID));

			foreach($worklflowJobs as $key=>$value){
				$JobIDs = Job::model()->findAllByAttributes(array('work_flow'=>$value->workflow_id));
				foreach($JobIDs as $JobIDs){
					$allJobIDs[$JobIDs->id] = $JobIDs->id;
				}
			}


			$criteria = new CDbCriteria();
			$criteria->addInCondition("id", $allJobIDs);
			$criteria->order = "id desc";
			$jobs = Job::model()->findAll($criteria);


		}else{
			$Criteria = new CDbCriteria();
			$Criteria->select="*";
			$Criteria->condition = "user_id=".$clientID.' and (jobStatus = 3 or jobStatus = 5 or jobStatus = 6 or jobStatus = 10) ';
			$Criteria->order = "id desc";

			if(isset($_POST['s'])) {
				$t = $_POST['s'];
				$Criteria->condition = "user_id='".$clientID."' and t.title like '%".$t."%'";
			}
			$jobs = Job::model()->findAll($Criteria);

		}
		
		$jobArray = array();
		
		foreach($jobs as $jobsK=>$jobsV){
			foreach($jobsV as $key1=>$val){
				
				if($key1=='location'){
				$loc = unserialize($val);
				$locationsV = '';
				if($loc){
				foreach($loc as $val){
					$location = Location::model()->findByPk($val);
					$locationsV .= $location->name.', ';
					}
				}
						$jobArray[$jobsK][$key1] = $locationsV;
					}else if($key1=='skills'){
						$jobArray[$jobsK]['primarySkills'] =  explode(",", $val);
						}
					else if($key1=='skills1'){
						$jobArray[$jobsK]['secondarySkills'] =  explode(",", $val);
						}
					else if($key1=='skills2'){
						$jobArray[$jobsK]['goodToHaveSkills'] =  explode(",", $val);
						}else{
					$jobArray[$jobsK][$key1] = $val;
					}
				}
			}
		
		$response = array();

		if($jobs){
			$response["success"] = 1;
			$response["AllJobs"] = $jobArray;
			$response["message"] = "Success";
			echo json_encode($response);
		}else{
			$response["success"] = 0;
			$response["message"] = "Error No Record Found!";
			echo json_encode($response);
		}


	}

	public function actionJobDetail()
	{
		error_reporting(0);
		$jobs = Job::model()->findByPk($_POST['job_id']);
		$workflow = Worklflow::model()->findByPk($jobs->work_flow);
		
		
		//job statuses
		$postingstatus = UtilityManager::jobStatus();

		switch ($postingstatus[$jobs->jobStatus]) {
			case "Pending Approval":
				$statusValue = 'Pending Approval';
				break;
			case "Open":
				$statusValue = 'Open';
				break;
			case "Filled":
				$statusValue = 'Filled';
				break;
			case "Rejected":
				$statusValue = 'Rejected';
				break;
			case "Re-open":
				$statusValue = 'Re-open';
				break;
			case "Hold":
				$statusValue = 'Hold';
				break;
			case "New Request":
				$statusValue = 'New-request';
				break;
		}
		
		
		$response = array();

		if($jobs){
			$response["success"] = 1;
			$response["title"] = $jobs->title;
			$response["bill_rate"] = $jobs->bill_rate.' '.$jobs->payment_type;
			$response["num_openings"] = $jobs->num_openings;
			
			//making changes in location section
			//$response["location"] = $jobs->location;
			
			$locationsV = '';
			
			if($job->location){
				$loc = unserialize($job->location);
				foreach($loc as $val){
					$location = Location::model()->findByPk($val);
					if($location)
					$locationsV .= $location->name.', ';
				}
			}
			$response["location"] = $locationsV;
					
			
			$response["type"] = $jobs->type;
			$response["category"] = $jobs->cat_id;
			$response["experience"] = $jobs->experience;
			$response["job_level"] = $jobs->job_level;
			$response["primery_skills"] = explode(",", $jobs->skills);
			$response["secondary_skills"] = explode(",", $jobs->skills1);
			$response["good_to_have"] = explode(",", $jobs->skills2);
			$response["skills_notes"] = $jobs->skills_notes;
			$response["jobDescription"] = $jobs->description;
			$response["you_will"] = $jobs->you_will;
			$response["qualification"] = $jobs->qualification;
			$response["add_info"] = $jobs->add_info;
			$response["hours_per_week"] = $jobs->hours_per_week;
			$response["shift"] = $jobs->shift;
			$response["labour_cat"] = $jobs->labour_cat;
			$response["number_submission"] = $jobs->number_submission;
			$response["background_verification"] = $jobs->background_verification;
			$response["hours_per_week"] = $jobs->hours_per_week;
			$response["job_po_duration"] = $jobs->job_po_duration;
			$response["job_closing_date"] = $jobs->desired_start_date;
			$response["request_dept"] = $jobs->request_dept;
			$response["billing_code"] = $jobs->billing_code;
			$response["reference_code"] = $jobs->pre_reference_code;
			$response["Approval_work_flow"] = $workflow->flow_name;
			$response["Budget"] = $jobs->pre_total_estimate_code;
			$response["jobStatus"] = $statusValue;			
			
            $response["message"] = "Success";
			echo json_encode($response);
		}else{
			$response["success"] = 0;
			$response["message"] = "Error No Record Found!";
			echo json_encode($response);
		}
	}
	
	public function actionPendingApprovalListing(){
		 error_reporting(0);
		 $clientID = $_POST['client_id'];
		 
		 $approvalJobs = JobWorkflow::model()->findAllByAttributes(array('client_id'=>$clientID,'email_sent'=>1,'job_status'=>'Pending'));
		 
		 $response = array();
		 
		 $jobArray = array();
		 
		 foreach($approvalJobs as $key=>$value){
			 //echo $key.'<br>';
			 $job = Job::model()->findByPk($value->job_id);
			 
			 
			 
			$locationsV = '';
			
			if($job->location){
				$loc = unserialize($job->location);
				foreach($loc as $val){
					$location = Location::model()->findByPk($val);
					$locationsV .= $location->name.', ';
				}
			}
			 $jobArray[$key]['job_id'] = $value->job_id;
			 $jobArray[$key]['job_name'] = $job->title;
			 $jobArray[$key]['locations'] = $locationsV;
			 $jobArray[$key]['bill_rate'] = $job->bill_rate;
			 $jobArray[$key]['job_workflow_approval'] = $value->id;
			 }
		 
		 /*echo '<pre>';
		 print_r($jobArray);
		 exit;*/
		 
		 if($approvalJobs){
			$response["success"] = 1;
			$response["pendingForApproval"] = $jobArray;
			$response["message"] = "Success";
			echo json_encode($response);
		}else{
			$response["success"] = 0;
			$response["message"] = "Error No Record Found!";
			echo json_encode($response);
		}
		 
		 
		}
		
		


	
}