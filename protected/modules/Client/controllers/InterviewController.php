<?php
class InterviewController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin';
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules(){
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','cancel','allinterview','interviewList','interviewCancelled','reschedualInterview','readyforOffer','rejectOfferSub'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(''),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	
	public function actionIndex(){	
		
		$date = "2016-08-29";
		
		$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 2 and s.job_id=".$_GET['jobid']."
		
		and ((w.interview_start_date ='".$date."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$date."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$date."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$date."' and w.alt3_date_status =1))";
		
		$todayInterview = Yii::app()->db->createCommand($sql)->query()->readAll();
		
		$sql1 = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 1 and s.job_id=".$_GET['jobid'];
		$pendingInterview = Yii::app()->db->createCommand($sql1)->query()->readAll();
				
		$Job = Job::model()->findAllByAttributes(array('user_type'=>'Client','user_id'=>Yii::app()->user->id));
		$jobIds = array();
		foreach($Job as $key=>$value){
			$jobIds[] = $value->id;
			}
			
		$criteria = new CDbCriteria();
		$criteria->addInCondition("job_id", $jobIds);
		$clientJobSub = VendorJobSubmission::model()->findAll($criteria);
		
		$this->render('index',array('clientJobSub'=>$clientJobSub,'todayInterview'=>$todayInterview,'pendingInterview'=>$pendingInterview));
	}
		
	public function actionCancel(){
       error_reporting(0);
		/*$sql ="select 
	sub.id,sub.resume_status,sub.interview_start_date as start1_date, sub.interview_alt1_start_date as start2_date,sub.interview_alt2_start_date as start3_date,sub.interview_alt3_start_date as start4_date, sub.start_date_status as start1_status,sub.alt1_date_status as start2_status,sub.alt2_date_status as start3_status,sub.alt3_date_status as start4_status, 
	
		sub.interview_start_date_time as start1_time,
		sub.interview_end_date_time as start1_endtime,
		sub.interview_alt1_start_date_time as start2_time,
		sub.interview_alt1_end_date_time as start2_endtime,
		sub.interview_alt2_start_date_time as start3_time,
		sub.interview_alt2_end_date_time as start3_endtime,
		sub.interview_alt3_start_date_time as start4_time,
		sub.interview_alt3_end_date_time as start4_endtime,
		sub.job_id,
		sub.interview_type,
	
		can.first_name,can.last_name,can.id as c_id  
	FROM vms_vendor_job_submission sub,vms_candidates as can WHERE 
	   sub.candidate_Id =can.id and sub.resume_status ='6' and  sub.start_date_status =0 and sub.alt1_date_status =0 and sub.alt2_date_status =0  and sub.alt3_date_status =0 "; */ 
	   
		//$sql = "SELECT w.id as interviewID,w.status,w.reason_rejection,w.offer,w.rating,w.notes,w.created_by_id,w.created_by_type,w.date_created, s.* FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 3 and s.job_id=".$_GET['jobid'];
		
		$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 3 and s.job_id=".$_GET['jobid'];
	
		$cancelledInterview = Yii::app()->db->createCommand($sql)->query()->readAll();
	
		$this->render('cancel',array('cancelledInterview'=>$cancelledInterview));
		
		}
			
	public function actionAllinterview(){
		   
		    //$sql = "SELECT w.id as interviewID,w.status,w.reason_rejection,w.offer,w.rating,w.notes,w.created_by_id,w.created_by_type,w.date_created, s.* FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status !=5 and s.job_id=".$_GET['jobid'];
			
			//all interviews list against the job
			$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.job_id=".$_GET['jobid'];
			
			
		    $allInterviews = Yii::app()->db->createCommand($sql)->query()->readAll();	
		
			
			$this->render('allinterview',array('interview'=>$allInterviews));
			}
			
	public function actionInterviewList(){
		$condition = '';
		if(isset($_POST['job_id'])){
			$condition = 'and s.job_id='.$_POST['job_id'];
		}
		
		$mainclient = UtilityManager::superClient(Yii::app()->user->id);
		
		//condition for hiring manager
		$loginUserId = Yii::app()->user->id;
		$modelClient = Client::model()->findByPk($loginUserId);
		$hiringManagerCond = '';
		 $allJobIDs = array();
		 if($modelClient->member_type == 'Hiring Manager'){
				$mainCientJobs = Job::model()->findAllByAttributes(array('user_subclient_id'=>$loginUserId));
				foreach($mainCientJobs as $mainCientJobsKey=>$mainCientJobsValue){ 
					$allJobIDs[$mainCientJobsValue->id] = $mainCientJobsValue->id; 
				}
		$hiringManagerCond = " and w.job_id IN ('".implode("','" , $allJobIDs)."')";
		 }
		 
		//now showing all the interviews list of a client all jobs
		$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".$mainclient."' AND s.id !=".Yii::app()->user->getState('submission_id')." $condition $hiringManagerCond order by w.id desc";
		
		$count_query = 'select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='.$mainclient;

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$interview=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
		
		
		//now showing all the interviews list of a client all jobs
		$sql1 = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".$mainclient."' AND s.id !=".Yii::app()->user->getState('submission_id')." and w.status = 2 $condition  $hiringManagerCond order by w.id desc";
		
		$count_query1 = 'select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where  w.status = 2 and s.client_id='.$mainclient;

		$item_count1 = Yii::app()->db->createCommand($count_query1)->queryScalar();

		$completedInterviews=new CSqlDataProvider($sql1, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count1,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$sql1 = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".$mainclient."' and w.status = 5 $condition  $hiringManagerCond order by w.id desc";

		$count_query1 = 'select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where  w.status = 2 and s.client_id='.$mainclient;

		$item_count1 = Yii::app()->db->createCommand($count_query1)->queryScalar();

		$statuscompleted=new CSqlDataProvider($sql1, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count1,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$sql1 = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".$mainclient."' AND s.id !=".Yii::app()->user->getState('submission_id')." and w.status = 1 $condition  $hiringManagerCond order by w.id desc";

		$count_query1 = 'select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where  w.status = 2 and s.client_id='.$mainclient;

		$item_count1 = Yii::app()->db->createCommand($count_query1)->queryScalar();

		$waitingforApproval = new CSqlDataProvider($sql1, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count1,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$sql1 = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".$mainclient."' AND s.id !=".Yii::app()->user->getState('submission_id')." and w.status = 3 $condition  $hiringManagerCond order by w.id desc";

		$count_query1 = 'select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where  w.status = 2 and s.client_id='.$mainclient;

		$item_count1 = Yii::app()->db->createCommand($count_query1)->queryScalar();

		$rejectedInterview = new CSqlDataProvider($sql1, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count1,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
			
			//$date = "2016-10-21";
			$date = date('Y-m-d');
		
			$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 2 $condition  $hiringManagerCond AND s.id !=".Yii::app()->user->getState('submission_id')."
			
			and ((w.interview_start_date ='".$date."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$date."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$date."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$date."' and w.alt3_date_status =1))";

		$count_query = "select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 2

			and ((w.interview_start_date ='".$date."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$date."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$date."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$date."' and w.alt3_date_status =1))";

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$todayInterview=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
		
		$tomorrow = date("Y-m-d", strtotime("+1 day"));
		$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 2 $condition  $hiringManagerCond AND s.id !=".Yii::app()->user->getState('submission_id')."
			
			and ((w.interview_start_date ='".$tomorrow."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$tomorrow."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$tomorrow."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$tomorrow."' and w.alt3_date_status =1)) ORDER BY id ASC";

		$count_query = "select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status = 2

			and ((w.interview_start_date ='".$tomorrow."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$tomorrow."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$tomorrow."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$tomorrow."' and w.alt3_date_status =1))";

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$upcommingInterview=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));
			
			
			$this->render('interviewList',array('interview'=>$interview,
				'todayInterview'=>$todayInterview,
				'completedInterviews'=>$completedInterviews,
				'upcommingInterview'=>$upcommingInterview,
				'statuscompleted'=>$statuscompleted,
				'waitingforApproval'=>$waitingforApproval,
				'rejectedInterview'=>$rejectedInterview));
	}
	
	
	public function actionInterviewCancelled(){

		//now showing all the interviews list of a client all jobs
		$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status=3 and s.client_id=".Yii::app()->user->id;

		$count_query = 'select count(*) FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where w.status=3 and s.client_id='.Yii::app()->user->id;

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>25,
			),
		));

		$interview = $dataProvider->getData();


		//$interview = Yii::app()->db->createCommand($sql)->query()->readAll();
		$this->render('interviewscancelled',array('interview'=>$interview,'pages'=>$dataProvider->pagination));
	}
	
	public function actionReschedualInterview(){
		
		if(isset($_POST['Reschedual'])){
			$interviewId = $_GET['interviewId'];
			$interviewdata  = Interview::model()->findByPk($interviewId);
			$interviewdata->reason_for_reschedual = $_POST['reason_for_reschedual'];
			$interviewdata->reschedual_note = $_POST['reschedual_note'];
			$interviewdata->status = 4;
			if($interviewdata->save(false)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong>Interview Rescheduale successfully. </div>');
				
				$this->redirect(array('job/scheduleInterview','id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$_GET['interviewId']));
			}
			
			
		}
	}
	
	public function actionReadyforOffer(){
			
		if(isset($_GET['submission-id'])){
			$interviewId = $_GET['interviewId'];
			$interviewdata  = Interview::model()->findByPk($interviewId);
			$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']);
			$interviewdata->status = 5;
			
			
			$offer_step1 = array();
			$offer_step1['job_id'] = $submissionData->job_id;
			$offer_step1['can_submission_id'] = $_GET['submission-id'];
			
			Yii::app()->session['wo_first_form'] = $offer_step1;
			
			$interviewdata->save(false);
			$submissionData->resume_status = 7;
			if($submissionData->save(false)){
				
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong>Offer request was raised on '. date('m-d-Y H:i:s') .'</div>');
				
				$this->redirect(array('offer/step2','sub_id'=>$_GET['submission-id']));
				//$this->redirect(array('job/scheduleInterview','id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$_GET['interviewId']));
			}
		}
		
	}
	
	public function actionRejectOfferSub(){
			
		if(isset($_GET['submission-id'])){
			$interviewId = $_GET['interviewId'];
			$interviewdata  = Interview::model()->findByPk($interviewId);
			$interviewdata->status = 3;
			$interviewdata->save(false);
			$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']);
			$submissionData->resume_status = 6;
			if($submissionData->save(false)){
				Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong>Candidate rejected on '. date('m-d-Y H:i:s') .'</div>');
				
				$this->redirect(array('job/scheduleInterview','id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$_GET['interviewId']));
			}
		}
		
	}
}