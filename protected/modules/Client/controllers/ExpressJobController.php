<?php

class ExpressJobController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(''),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function JobTitles(){
		
		$jobTitles = Job::model()->findAll();
		$jsonArray = array();
		foreach($jobTitles as $key=>$value){
			$jsonArray[$key] = array('value'=>$value->title,'data'=>$value->description);
			}
		echo json_encode($jsonArray);
		
		}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ExpressJob;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ExpressJob']))
		{
			$model->attributes=$_POST['ExpressJob'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ExpressJob']))
		{
			$model->attributes=$_POST['ExpressJob'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$supperClient = UtilityManager::superClient(Yii::app()->user->id);
		$loginUser = Yii::app()->user->id;
		
		$criteria=new CDbCriteria();
		$criteria->condition = 'user_id = :id';
        $criteria->order = 'id DESC';
        $criteria->params = array (':id'=>$supperClient);
		$count=ExpressJob::model()->count($criteria);
		$pages=new CPagination($count);
	
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		$expressJobs=ExpressJob::model()->findAll($criteria);
		
		$model = new ExpressJob;
		if(isset($_POST['ExpressJob']))
		{
			$model->attributes=$_POST['ExpressJob'];
			if(isset($_POST['ExpressJob']['location'])){
				$model->location = implode(',',$_POST['ExpressJob']['location']);
			}
			$model->job_po_duration = $_POST['express_job_s_date'];
			//$model->job_po_duration_endDate = $_POST['job_po_duration_endDate'];
			$model->desired_start_date = $_POST['express_job_e_date'];
			$model->date_created = date('Y-m-d');
			$model->status = 1;
			$model->user_id = $supperClient;
			$model->created_by_id = Yii::app()->user->id;
			$model->user_type = "Client";
			if($model->save(false)){
				
				/*$title = 'Title of Notification';
				$message = 'Message of Notification';
				$url = 'https://simplifydemo.pushcrew.com';
			
				$apiToken = '096d430c61ca1e1cba2f3fea9c70a660';
			
				$curlUrl = 'https://pushcrew.com/api/v1/send/all';
				
				//set POST variables
				$fields = array(
				  'title' => $title,
				  'message' => $message,
				  'url' => $url
				);
			
				$httpHeadersArray = Array();
				$httpHeadersArray[] = 'Authorization: key='.$apiToken;
			
				//open connection
				$ch = curl_init();
			
				//set the url, number of POST vars, POST data
				curl_setopt($ch, CURLOPT_URL, $curlUrl);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
				curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeadersArray);
			
				//execute post
				$result = curl_exec($ch);
			
				$resultArray = json_decode($result, true);
			
				if($resultArray['status'] == 'success') {
					//success
					//echo $resultArray['request_id']; //ID of Notification Request
				}
				else if($resultArray['status'] == 'failure') {
					//failure
				}
				else {
					//failure
				}*/
				
				$this->redirect(array('index'));
			}
		}
		$this->render('index',array('model'=>$model,'expressJobs'=>$expressJobs,'pages'=>$pages));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ExpressJob('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ExpressJob']))
			$model->attributes=$_GET['ExpressJob'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ExpressJob the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ExpressJob::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ExpressJob $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='express-job-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
