<?php
class OfflineAccessController extends Controller
{
    public function actionIndex()
    {
        $files = CFileHelper::findFiles(Yii::getPathOfAlias('webroot').'/protected/data/user-tokens', array('absolutePaths' => false));

        $this->render('index', array('users' => $files));
    }
    public function actionAuthorize()
    {
 	    $auth_code = $_GET['code'];
        $redirectUri = $this->createAbsoluteUrl('offlineAccess/authorize');

        $tokens = oAuthService::getTokenFromAuthCode($auth_code, $redirectUri);

        if ($tokens['access_token']) {
              $user = OutlookService::getUser($tokens['access_token']);
              $loginUser = Yii::app()->user->id;
			  $client = Client::model()->findByPk($loginUser);
			  
			  $client->outlook_email_address = $user['EmailAddress'];
			  //$client->outlook_access_token = $tokens['access_token']; 
			  $client->outlook_access_token = json_encode($tokens);
			   $client->save(false);

            //$file = Yii::getPathOfAlias('webroot').'/protected/data/user-tokens/'.$user['EmailAddress'].'.txt';
             // Redirect back to home page
			 //echo 'access token save';
            Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Success!</strong> You have successfully give this app access.</div>');
			$this->redirect($this->createAbsoluteUrl('settinggeneral/extension'));
        }
        else
        {
			echo 'error while saving';
            Yii::app()->user->setFlash('error', $tokens['error']);
        }

        $this->redirect($this->createAbsoluteUrl('offlineAccess/index'));
    }

    public function actionAdd(){
		//error_reporting(0);
        $model = new OfflineAccess();
		
        $results = $model->addEventToCalendar(1, $this->createAbsoluteUrl('offlineAccess/authorize'));
		echo '<pre>';
		print_r( $results);		
		exit;
		
        $error = false;
        
        foreach($results as $result) {
            if(isset($result['errorNumber'])) {
                $error = true;
                break;
            }
        }
        
        if($error)
            Yii::app()->user->setFlash('error', "Unable to add the event. Please try again");
        else
            Yii::app()->user->setFlash('success', "Event added to the calendar successfully");

        $this->redirect($this->createAbsoluteUrl('offlineAccess/index'));

    }
}?>