<?php
class ContractController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules() 
	{ 
		return array( 
		 
			array('allow', // allow admin user to perform 'admin' and 'delete' actions 
				'actions'=>array('onboarddigitalDoc','digitalDoc','downloadExtensionNote','index','ContractExtensionWorkflowList','contractrenewalsList','contractclosedList','contractView','deleteBackgroundverification','contractTermination','terminationStep1','terminationView','terminationDetails','contractExtension','extensionDetails','rateCalc','contractExtensionList','contractWflow','extensionView', 'contractTerminationCronJob','contractWflowById' ,'approveRejectBillRequest', 'approveExtWorkflow'),
				'users'=>array('@'), 
			), 
			array('deny',  // deny all users 
				'users'=>array('*'),
			), 
		); 
	}
	public function actionOnboarddigitalDoc(){

		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$condition = '';
		if(isset($_POST['candidate_name'])){
			$condition = 'and (can.first_name LIKE "%'.$_POST['candidate_name'].'%" || can.last_name LIKE "%'.$_POST['candidate_name'].'%")';
		}
		 $sql ="select sub.id as sub_id ,contract.hellosign_signer1_date,contract.hellosign_signer2_date,contract.id as contractId,contract.hellosign_signer1_status,contract.hellosign_signer2_status,contract.job_id as jobId ,can.first_name,can.last_name,can.id as c_id,vendor.organization as v_organization 
FROM vms_contract as contract LEFT JOIN vms_vendor_job_submission sub ON contract.submission_id=sub.id LEFT JOIN vms_candidates as can ON contract.candidate_id=can.id LEFT JOIN vms_vendor as vendor ON contract.vendor_id=vendor.id
WHERE contract.client_id='".$loginUserId."' $condition and (contract.hellosign_signer1_status=1 || contract.hellosign_signer2_status=1 ) AND sub.id !='".Yii::app()->user->getState('submission_id')."' order by contract.id desc";

		$count_query = "select can.last_name,can.id as c_id,vendor.organization as v_organization 
FROM vms_contract as contract LEFT JOIN vms_vendor_job_submission sub ON contract.submission_id=sub.id LEFT JOIN vms_candidates as can ON contract.candidate_id=can.id LEFT JOIN vms_vendor as vendor ON contract.vendor_id=vendor.id
WHERE contract.client_id='".$loginUserId."' $condition and (contract.hellosign_signer1_status=1 || contract.hellosign_signer2_status=1 ) AND sub.id !=".Yii::app()->user->getState('submission_id');

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();
/*echo $item_count; exit;*/
		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>100,
			),
		));
		$digitalDoc = $dataProvider->getData();
		$this->render('onboarddigitalDoc',array('digitalDoc'=>$digitalDoc,'pages'=>$dataProvider->pagination));
	}
	public function actionDigitalDoc(){
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$condition = '';
		if(isset($_POST['candidate_name'])){
			$condition = 'and (can.first_name LIKE "%'.$_POST['candidate_name'].'%" || can.last_name LIKE "%'.$_POST['candidate_name'].'%")';
		}
		$sql ="select sub.id as sub_id ,offer.doc_signed_date,offer.id as offerId,offer.hellosign_api_sign_status,offer.job_id as jobId ,can.first_name,can.last_name,can.id as c_id,vendor.organization as v_organization 
FROM vms_offer as offer LEFT JOIN vms_vendor_job_submission sub ON offer.submission_id=sub.id LEFT JOIN vms_candidates as can ON offer.candidate_id=can.id LEFT JOIN vms_vendor as vendor ON offer.vendor_id=vendor.id
WHERE offer.client_id='".$loginUserId."' $condition and offer.hellosign_api_sign_status=1 AND sub.id !='".Yii::app()->user->getState('submission_id')."' order by offer.id desc";

		$count_query = "select offer.hellosign_api_sign_status,offer.job_id as jobId ,can.first_name,can.last_name,can.id as c_id,vendor.organization as v_organization 
FROM vms_offer as offer LEFT JOIN vms_vendor_job_submission sub ON offer.submission_id=sub.id LEFT JOIN vms_candidates as can ON offer.candidate_id=can.id LEFT JOIN vms_vendor as vendor ON offer.vendor_id=vendor.id
WHERE offer.client_id='".$loginUserId."' $condition and offer.hellosign_api_sign_status=1 AND sub.id !=".Yii::app()->user->getState('submission_id');

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>100,
			),
		));
		$digitalDoc = $dataProvider->getData();
		$this->render('digitalDoc',array('digitalDoc'=>$digitalDoc,'pages'=>$dataProvider->pagination));
	}

	public function actionContractrenewalsList(){

		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$todaydate = date('Y-m-d');
		$sql = 'SELECT  * FROM vms_contract WHERE if(termination_status = 0,end_date <="'.$todaydate.'",termination_date <= "'.$todaydate.'") and client_id ="'.$loginUserId.'"
		  ORDER BY 
         ABS(DATEDIFF(NOW(), `end_date`)) asc';

		$count_query = 'select count(*) FROM vms_contract WHERE if(termination_status = 0,end_date <="'.$todaydate.'",termination_date <= "'.$todaydate.'") and client_id ="'.$loginUserId.'"
		  ORDER BY 
         ABS(DATEDIFF(NOW(), `end_date`)) asc';

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>100,
			),
		));

		$this->render('contractrenewalsList', array(
			'dataProvider' => $dataProvider));

	}
	public function actionContractclosedList(){

		$loginUserId = Yii::app()->user->id;
		$todaydate = date('Y-m-d');
		$sql = 'SELECT  * FROM vms_contract WHERE if(termination_status = 0,end_date >"'.$todaydate.'",termination_date > "'.$todaydate.'") and client_id ="'.$loginUserId.'"
		  ORDER BY 
         ABS(DATEDIFF(NOW(), `end_date`)) asc';

		$count_query = 'select count(*) FROM vms_contract WHERE if(termination_status = 0,end_date >"'.$todaydate.'",termination_date > "'.$todaydate.'") and client_id ="'.$loginUserId.'"
		  ORDER BY 
         ABS(DATEDIFF(NOW(), `end_date`)) asc';

		$item_count = Yii::app()->db->createCommand($count_query)->queryScalar();

		$dataProvider=new CSqlDataProvider($sql, array(
			'keyField'=>'id',
			'totalItemCount'=>$item_count,
			'pagination'=>array(
				'pageSize'=>100,
			),
		));

		$this->render('contractclosedList', array(
			'dataProvider' => $dataProvider));

	}

	public function actionContractWflow(){
		$clientID = Yii::app()->user->id;
		$estimatedCost = $_POST['estimatedCost'];
		$client = Client::model()->findByPk($clientID);
		if($client->member_type!=NULL){
			$client = Client::model()->findByPk($client->super_client_id);
			$clientID = $client->id;
			}
		$query = "select * from vms_contract_workflow where client_id=$clientID and $estimatedCost BETWEEN  range_from AND range_to";
		$contractWorkFlow = Yii::app()->db->createCommand( $query )->query()->readAll();
		
		if(isset($contractWorkFlow[0])){
			$members = ContractWorklflowMember::model()->findAllByAttributes(array('contract_flow_id'=>$contractWorkFlow[0]['id']));
			
			echo UtilityManager::extensionWFmembersTable($contractWorkFlow[0]['id'],$contractWorkFlow[0]['contract_flow_name'],$members);
		  }
		}
	
	public function actionContractExtension(){ error_reporting(0);
  			if(isset($_GET['id'])){
				 $id = $_GET['id'];
			}else{
				$id = 0;
			}
 		$model = new ContractExtensionReq;
		if(isset($_POST['ContractExtensionReq'])){
			if(isset($_POST['isAjax']) && $_POST['isAjax'] == 1){   
 			   
				//echo '<pre>';print_r($_POST);exit;
				$contract = Contract::model()->findByPk($_POST['contract_id']);
				$model->reason_of_extension =  $_POST['reason_of_extension'];
				$model->note_of_extension =  $_POST['note_of_extension'];
				$model->new_contract_end_date =  $_POST['new_contract_end_date'];
				$model->pay_rate = $_POST['pay_rate'];
				$model->bill_rate =  $_POST['bill_rate'];
				$model->overtime_payrate = $_POST['overtime_payrate'];
				$model->doubletime_payrate =  $_POST['doubletime_payrate'];
				$model->overtime_billrate = $_POST['overtime_billrate'];
				$model->doubletime_billrate =  $_POST['doubletime_billrate'];
				$model->new_estimate_cost =  $_POST['new_estimate_cost'];
				$model->contract_id =  $_POST['contract_id'];
  				$model->new_contract_end_date = date('Y-m-d',strtotime($_POST['new_contract_end_date']));
				$model->contract_work_flow = $_POST['contract_work_flow'];
				if($model->save(false)){
 					$contract->contract_work_flow = $_POST['contract_work_flow'];
					$contract->ext_status = 1;
					$contract->ext_vendor_approval = 1;
					$contract->save(false);
			   }
 			}else{
			  //echo '<pre>';print_r($_POST);exit;
			  $contract = Contract::model()->findByPk($_POST['ContractExtensionReq']['contract_id']);
			  $model->attributes=$_POST['ContractExtensionReq'];
			  $model->new_contract_end_date = date('Y-m-d',strtotime($_POST['ContractExtensionReq']['new_contract_end_date']));
			  if($model->save(false)){
 				  $contract->contract_work_flow = $_POST['ContractExtensionReq']['contract_work_flow'];
				  $contract->ext_status = 1;
				  $contract->ext_vendor_approval = 1;
				  $contract->save(false);
				  }
			}
		}
			 
			if(isset($_POST['isAjax']) && $_POST['isAjax'] == 1){  
				echo '<div class="alert alert-success">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						  <strong>Success!</strong> Extension Request Submitted Successfully.</div>';
						  exit;
			}else{
			  $this->render('contractExtension', array(
				   'model' => $model,
				   'id' => $id
			  ));
			}
		}
	
	public function actionContractTermination(){
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$criteria=new CDbCriteria();
		$criteria->condition = 'termination_status IN("1","2","3") and client_id ='.$loginUserId;
       	$criteria->order = 'id DESC';
		$count=Contract::model()->count($criteria);
		$pages=new CPagination($count);

		// results per page
		$pages->pageSize=10;
		$pages->applyLimit($criteria);
		$models=Contract::model()->findAll($criteria);

		$this->render('contractTermination', array(
			 'models' => $models,
			 'pages' => $pages
		));
	}
		
	/*public function actionContractExtensionList(){
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		
		
		$criteria=new CDbCriteria();
		$criteria->condition = 'ext_status IN("1","2","3") and client_id ='.$loginUserId;
       	$criteria->order = 'id DESC';
		$count=Contract::model()->count($criteria);
		$pages=new CPagination($count);

		// results per page
		$pages->pageSize=10;
		$pages->applyLimit($criteria);
		$models=Contract::model()->findAll($criteria);

		$this->render('contractExtensionList', array(
			 'models' => $models,
			 'pages' => $pages
		));
	}*/
	
	public function actionContractExtensionList(){
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		
		//malik and ali
		
		$criteria=new CDbCriteria();
		$criteria->join = 'INNER JOIN (select req.* from vms_contract_extension_req as req group by req.id order by req.id desc) as ext ON t.id=ext.contract_id';
		$criteria->condition = 't.ext_status IN("1","2","3") and t.client_id ='.$loginUserId.' and ext.contract_id=t.id';
		$criteria->group='ext.contract_id';
		$criteria->order = 'ext.id DESC';
		$models=Contract::model()->findAll($criteria);

		$this->render('contractExtensionList', array(
			 'models' => $models,
			 //'pages' => $pages
		));
	}
	
	public function actionExtensionView($id , $type = ''){
		
		$contractExtReq = ContractExtensionReq::model()->findByPk($id);
		$contract = Contract::model()->findByPk($contractExtReq->contract_id);
		$workOrder = Workorder::model()->findByPk($contract->workorder_id);
		$vendor = Vendor::model()->findByPk($contract->vendor_id);
		$model = new ContractExtensionComments();
		if(isset($_POST['ExtensionComments'])){
			$model->user_id = Yii::app()->user->id;
			$model->extension_id = $id;
			if(isset($_POST['is_public'])){
				$model->status = 1;
			}else{
				$model->status = 0;
			}
			$model->comment = $_POST['comment'];
			$model->date_added = date("Y-m-d H:i:s");
			$rnd = rand(0,9999);
			$uploadedFile=CUploadedFile::getInstance($model,'media');
			if($uploadedFile != ""){
				$fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
				$model->media = $fileName;
			}
			if($model->save(false)){
				if($uploadedFile != ""){
					$uploadedFile->saveAs(Yii::app()->basePath.'/../extension_comments/'.$model->media);// image
				}
				Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Comment posted successfully.</div>');
				$this->redirect(array('extensionView','id'=>$id,'type'=>'comment'));
			}else{
				Yii::app()->user->setFlash('error', '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> Comment not posted.</div>');
				$this->redirect(array('extensionView','id'=>$id,'type'=>'comment'));
			}
			//exit;
		}
		if($type == '')
			$type = 'action';
		$criteria=new CDbCriteria();
		$criteria->condition = 'extension_id ='.$id;
		$criteria->condition = 'status = 1';
		$criteria->order = 'id DESC';
		$comments = ContractExtensionComments::model()->findAll($criteria);
		$this->render('extensionView', array(
			 'contractExtReq' => $contractExtReq,
			 'contract' => $contract,
			'comments'=>$comments,
			'model' => $model,
			 'workOrder' => $workOrder,
			 'vendor' => $vendor,
			'type' => $type
			));
	}
	public function actionDownloadExtensionNote($id){

		$contractExtComment = ContractExtensionComments::model()->findByPk($id);

		$baseUrl = Yii::app()->getBaseUrl(true);
		if (strstr($baseUrl, 'local')) {
			$path = Yii::app()->basePath.'/../extension_comments/';
		}else {
			$path = Yii::app()->basePath.'/../extension_comments/';
		}
		$file = $path.$contractExtComment->media;

		if ($contractExtComment) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}
	public function actionTerminationStep1(){
 		if(isset($_GET['id']))
			{
				$id = $_GET['id'];
			}else{
				$id = 0;
			}
		if(isset($_POST['terminationForm'])){
  				$contractID = $_POST['contract_id'];
 				$contract = Contract::model()->findByPk($contractID);
				$contract->reason_of_termination = $_POST['reason_of_termination'];
				$contract->termination_notes = $_POST['termination_notes'];
				$contract->termination_can_feedback = $_POST['termination_can_feedback'];
				$contract->termination_date = date('Y-m-d',strtotime($_POST['termination_date']));
				$contract->termination_status = 1;
				$contract->term_by_id = Yii::app()->user->id;
				$contract->term_by_type = 'Client';
				$contract->term_submited_datetime = date('Y-m-d H:i:s');
 				if($contract->save(false)){
 				  //print_r($_POST);
				 if(isset($_POST['isAjax']) && $_POST['isAjax'] == 1){  
						 echo '<div class="alert alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  <strong>Success!</strong> Termination Request Submitted Successfully.</div>';
					  exit;
				 }else{
					Yii::app()->user->setFlash('success', '<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Success!</strong> Termination Request Submitted Successfully.</div>');
					$this->redirect(array('terminationView','id'=>$contractID));
				 }
			   }
			}
		 	$this->render('terminationStep1' , array('id'=>$id));
		}
		
	public function actionTerminationView($id){
 		$contract = Contract::model()->findByPk($id);
		$offer = Offer::model()->findByPk($contract->offer_id);
		$workorder = Workorder::model()->findByPk($contract->workorder_id);
		$vendor = Vendor::model()->findByPk($contract->vendor_id);
		$location = Location::model()->findByPk($offer->approver_manager_location);
		
		if(isset($_POST['approved'])){
			$contract->termination_status = 2;
			$contract->term_app_rej_by_id = Yii::app()->user->id;
			$contract->term_app_rej_by_type = 'Client';
			$contract->term_app_rej_datetime = date('Y-m-d H:i:s');
			$contract->save(false);
 			  if(isset($_POST['isAjax']) && $_POST['isAjax'] == 1){  
						 echo '<div class="alert alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  <strong>Success!</strong> Successfully Done.</div>';
					  exit;
			  }else{
				  Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <strong>Success!</strong> Successfully Done.</div>');
			  }
		}
		if(isset($_POST['reject'])){
			$contract->termination_status = 3;
			$contract->term_app_rej_by_id = Yii::app()->user->id;
			$contract->term_app_rej_by_type = 'Client';
			$contract->term_app_rej_datetime = date('Y-m-d H:i:s');
			$contract->save(false);
			if(isset($_POST['isAjax']) && $_POST['isAjax'] == 1){  
						 echo '<div class="alert alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  <strong>Success!</strong> Successfully Done.</div>';
					  exit;
			  }else{
				  Yii::app()->user->setFlash('success', '<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <strong>Success!</strong> Successfully Done.</div>');
			  }
		}
		
		$this->render('terminationView',array('contract'=>$contract,'offer'=>$offer,'workorder'=>$workorder,'vendor'=>$vendor,'location'=>$location));
		}
		
	public function actionIndex()
	{ 
		//$loginUserId = Yii::app()->user->id;
		$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
		$criteria=new CDbCriteria();
		$criteria->condition = 'client_id ='.$loginUserId.' AND submission_id !='.Yii::app()->user->getState('submission_id');
       	$criteria->order = 'id DESC';
		$count=Contract::model()->count($criteria);
		$pages=new CPagination($count);

		// results per page
		$pages->pageSize=10;
		$pages->applyLimit($criteria);
		$models=Contract::model()->findAll($criteria);

		$this->render('index', array(
		'models' => $models,
			 'pages' => $pages
		));
	}
	
	public function actionContractView($id){
		
		if(isset($_POST['ImportData'])){
		   $contractModel = Contract::model()->findByPk($_POST['contract_id']);
		   $workorderModel = Workorder::model()->findByPk($contractModel->workorder_id);
		   $offerModel = Offer::model()->findByPk($contractModel->offer_id);
		   
		   $workorderModel->wo_bill_rate = $_POST['bill_rate'];
		   $workorderModel->wo_pay_rate = $_POST['pay_rate'];
		   $workorderModel->wo_over_time = $_POST['over_time'];
		   $workorderModel->wo_double_time = $_POST['double_time'];
		   $workorderModel->wo_client_over_time = $_POST['client_over_time'];
		   $workorderModel->wo_client_double_time = $_POST['client_double_time'];
		   $workorderModel->approval_manager = $_POST['approval_manager'];
		   if(isset($_POST['start_date'])){
			$workorderModel->wo_start_date = date('Y-m-d',strtotime($_POST['start_date']));
			$workorderModel->onboard_changed_start_date = date('Y-m-d',strtotime($_POST['start_date']));
		   }
		   if(isset($_POST['end_date'])) {
			$workorderModel->wo_end_date = date('Y-m-d',strtotime($_POST['end_date']));
			$workorderModel->onboard_changed_end_date = date('Y-m-d',strtotime($_POST['end_date']));
		   }
		   if($workorderModel->save(false)){
			if(isset($_POST['start_date'])) {
			 $contractModel->start_date = date('Y-m-d',strtotime($_POST['start_date']));
			}
			if(isset($_POST['end_date'])) {
			 $contractModel->end_date = date('Y-m-d',strtotime($_POST['end_date']));
			}
			$contractModel->save(false);
			
			
			$offerModel->location_manager = $_POST['location_manager'];
			$offerModel->approver_manager_location = $_POST['approver_manager_location'];
			$offerModel->save(false);
			
			Yii::app()->user->setFlash('success', '<div class="alert alert-success">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <strong>Success!</strong>  Data saved successfully. </div>');
		   }
		}
		
		$model = Contract::model()->findByPk($id);
		$jobData = Job::model()->findByPk($model->job_id);
		$vendor = Vendor::model()->findByPk($model->vendor_id);
		$offer = Offer::model()->findByPk($model->offer_id);
		$submission = VendorJobSubmission::model()->findByPk($model->submission_id);
		$workorder = Workorder::model()->findByPk($model->workorder_id);
		$client = Client::model()->findByPk($model->client_id);
		$candidate = Candidates::model()->findByPk($model->candidate_id);
		
		//workOrder Projects list
		$projectIDs = array();
		if($workorder->wo_project){
			$projectIDs = explode(',',$workorder->wo_project);
			}
		$projects = Project::model()->findAllByAttributes(array('id'=>$projectIDs));
		
		//workOrder Cost Center Cosdes list
		$costCenterIDs = array();
		if($workorder->wo_cost_center){
			$costCenterIDs = explode(',',$workorder->wo_cost_center);
			}
		$costCenters = CostCenter::model()->findAllByAttributes(array('id'=>$costCenterIDs));
		
		//workOrder Time Sheet Codes list
		$timeSheetIDs = array();
		if($workorder->wo_timesheet_code){
			$timeSheetIDs = explode(',',$workorder->wo_timesheet_code);
			}
		$timeSheetCodes = TimeSheetCode::model()->findAllByAttributes(array('id'=>$timeSheetIDs));
		
		$this->render('contractView', array('model' => $model,'jobData'=>$jobData,'vendor'=>$vendor,'offer'=>$offer,'submission'=>$submission,'workorder'=>$workorder,'client'=>$client,'candidate'=>$candidate,'projects'=>$projects,'costCenters'=>$costCenters,'timeSheetCodes'=>$timeSheetCodes));
		}
		
		public function actionDeleteBackgroundverification(){
			$Id = $_GET['id'];
			$backGround = OfferBackground::model()->findByPk($Id);
			$backGround->delete();
			$this->redirect(array('contractView','id'=>$_GET['contractID']));
			
			}
			
		public function Candidates(){
		$candidates = Candidates::model()->findAll();
		$jsonArray = array();
		foreach($candidates as $key=>$value){
			$contract = Contract::model()->findByAttributes(array('candidate_id'=>$value->id,'termination_status'=>'0'));
			if(!empty($contract)){
			$jsonArray[] = array('value'=>$value->first_name.' '.$value->last_name.' ( Contract ID: '.$contract->id.' )','data'=>$contract->id);
			}
			}
			
		echo json_encode($jsonArray);
		}
		
		public function ExtensionCandidates(){
		  $candidates = Candidates::model()->findAll();
		  $jsonArray = array();
		  foreach($candidates as $key=>$value){
			  $contract = Contract::model()->findByAttributes(array('candidate_id'=>$value->id,'ext_status'=>array(0,2)));
			if(!empty($contract)){
			$jsonArray[] = array('value'=>$value->first_name.' '.$value->last_name.' ( Contract ID: '.$contract->id.' )','data'=>$contract->id);
			}
		  }
 		  echo json_encode($jsonArray);
		}
		
		
		
		public function ActionTerminationDetails(){
			$contractID = $_POST['contractID'];
			$contract = Contract::model()->findByPk($contractID);
			$offer = Offer::model()->findByPk($contract->offer_id);
			$workorder = Workorder::model()->findByPk($contract->workorder_id);
			$vendor = Vendor::model()->findByPk($contract->vendor_id);
			$location = Location::model()->findByPk($offer->approver_manager_location);
			$aprovermanager = Client::model()->findByPk($workorder->approval_manager);
			if($workorder->onboard_changed_start_date=='0000-00-00'){
				$onboardStartDate = date('F d,Y',strtotime($workorder->wo_start_date));
				}else{
					 $onboardStartDate = date('F d,Y',strtotime($workorder->onboard_changed_start_date));
					 }
					 
			if($workorder->onboard_changed_end_date=='0000-00-00'){
				$onboardEndDate = date('F d,Y',strtotime($workorder->wo_end_date));
				}else{
					 $onboardEndDate = date('F d,Y',strtotime($workorder->onboard_changed_end_date));
					 }
			  $response = array();
			  $response['woid'] = $contract->workorder_id;
			  $response['cid'] = $contract->id;
			  $response['brate'] = '$'.$workorder->wo_bill_rate;
			  $response['prate'] = '$'.$workorder->wo_pay_rate;
			  $response['location'] =  $location->name;
			  $response['start_date'] =  $onboardStartDate;
			  $response['end_date'] =  $onboardEndDate;
			  $response['name'] =  $aprovermanager->first_name.' '.$aprovermanager->last_name;
			  $response['jid'] = $contract->job_id;
			  $response['vendor'] = $vendor->first_name.' '.$vendor->last_name.' ('.$vendor->organization.')';
			  $response['termination_status'] = $contract->termination_status; 
			  if($contract->termination_status == 2 || $contract->termination_status == 1 ){
					  $client = Client::model()->findByPk($contract->term_by_id);
					  $setting = Setting::model()->findByPk($contract->reason_of_termination);
					  $response['statusTxt'] = '<div class="alert alert-danger">Contract was terminated by '.$client->first_name.' '.$client->last_name.' on '.date('d-m-Y',strtotime($contract->termination_date)).'</div>';
					  $response['termination_reason'] = $setting->title;
					  $response['termination_notes'] = $contract->termination_notes;
					  $response['termination_date'] = date('d-m-Y',strtotime($contract->termination_date));
					  $response['termination_can_feedback'] = $contract->termination_can_feedback;
				  }
	  
			  echo json_encode($response);
			
			}
			
		public function ActionExtensionDetails(){
			$contractID = $_POST['contractID'];
			$contract = Contract::model()->findByPk($contractID);
			$submission = VendorJobSubmission::model()->findByPk($contract->submission_id);
			$offer = Offer::model()->findByPk($contract->offer_id);
			$workorder = Workorder::model()->findByPk($contract->workorder_id);
			$vendor = Vendor::model()->findByPk($contract->vendor_id);
			$location = Location::model()->findByPk($offer->approver_manager_location);
			$job = Job::model()->findByPk($contract->job_id);
			
			$response = array();
 			$response['woid'] = $contract->workorder_id;
			$response['cid'] = $contract->id;
			$response['brate'] = '$'.$workorder->wo_bill_rate;
			$response['prate'] = '$'.$workorder->wo_pay_rate;
			$response['location'] =  $location->name;
			$response['jid'] = $contract->job_id;
			$response['vendor'] = $vendor->first_name.' '.$vendor->last_name.' ('.$vendor->organization.')';
			$aprovermanager = Client::model()->findByPk($workorder->approval_manager);
			if($workorder->onboard_changed_start_date=='0000-00-00'){
				$onboardStartDate = date('F d,Y',strtotime($workorder->wo_start_date));
				}else{
					 $onboardStartDate = date('F d,Y',strtotime($workorder->onboard_changed_start_date));
					 }
					 
			if($workorder->onboard_changed_end_date=='0000-00-00'){
				$onboardEndDate = date('F d,Y',strtotime($workorder->wo_end_date));
				}else{
					 $onboardEndDate = date('F d,Y',strtotime($workorder->onboard_changed_end_date));
					 }
 
				$response['ext_bill_rate'] =$workorder->wo_bill_rate;
				$response['ext_pay_rate'] =$workorder->wo_pay_rate;
				$response['ext_over_time_prate'] = $workorder->wo_over_time;
				$response['ext_double_time_prate'] = $workorder->wo_double_time;
				$response['overtime_billrate'] = $workorder->wo_client_over_time;
				$response['doubletime_billrate'] = $workorder->wo_client_double_time;
				$response['start_date'] =  $onboardStartDate;
				$response['end_date'] =  $onboardEndDate;
				$response['name'] =  $aprovermanager->first_name.' '.$aprovermanager->last_name;
 				//$response['job_start_date'] = date('m/d/Y',strtotime($contract->start_date));
				//$response['job_end_date'] = date('m/d/Y',strtotime($contract->end_date));
 				//New contract start date will be the end date of last contract
				$response['contract_end_date'] = date('m/d/Y',strtotime($contract->end_date));
				$response['hours_per_week'] = $job->hours_per_week;
				//$response['markup'] = $job->markup;
				$response['markup'] = $submission->makrup;
				$response['ext_status'] = $contract->ext_status;
				if($contract->ext_status == 2 || $contract->ext_status == 1){
					$contractExtReq = ContractExtensionReq::model()->findByAttributes(array('contract_id'=>$contract->id));
					$reason = Setting::model()->findByPk($contractExtReq->reason_of_extension);
					$response['ext_reason'] =  $reason->title;
					$response['new_contract_end_date'] = date('m/d/Y',strtotime($contractExtReq->new_contract_end_date));
					$response['note_of_extension'] = $contractExtReq->note_of_extension; 
					$response['new_estimate_cost'] = $contractExtReq->new_estimate_cost;
 				}
		
				echo json_encode($response);
 			}
		
		public function actionRateCalc(){
			$contract = Contract::model()->findByPk($_POST['cid']);
			$job = Job::model()->findByPk($contract->job_id);
			$clientRateData = ClientRate::model()->findByAttributes(array('client_id'=>$job->user_id));
			$over_time = 0; $double_time = 0;
			if($clientRateData){
				$over_time = $clientRateData->over_time;
				$double_time = $clientRateData->double_time;
			}
			$response = array();
			$response['over_time'] = $over_time;
			$response['double_time'] = $double_time;
			
			echo json_encode($response);
		}
			
 
		// Added by Zoobia Humayun
		public function actionApproveRejectBillRequest($id){
 			$contractExtReq = ContractExtensionReq::model()->findByPk($id);
			$contract = Contract::model()->findByPk($contractExtReq->contract_id); 
 			$workOrder = Workorder::model()->findByPk($contract->workorder_id);
			$loginUserId = Yii::app()->user->id;
			if(isset($_POST['rejectbill'])){
 				$contractExtReq->bill_request_status = 3;
				$contractExtReq->reason_bill_req_rejection = $_POST['reason'];
				$contractExtReq->notes_bill_req_rejection = $_POST['note'];
				$contractExtReq->bill_req_rejection_date = date("Y-m-d H:i:s");
				$contractExtReq->save(false); 
			}else{
				$contractHistory = new ContractHistory();
				 
				 // Save the old information in history table
					$contractHistory->workorder_id = $workOrder['id'];
					$contractHistory->offer_id  = $workOrder['offer_id'];
					$contractHistory->submission_id  = $workOrder['submission_id'];
					$contractHistory->job_id  = $workOrder['job_id'];
					$contractHistory->client_id  = $workOrder['client_id'];
					$contractHistory->vendor_id = $workOrder['vendor_id'];
					$contractHistory->candidate_id = $workOrder['candidate_id'];
					$contractHistory->contract_start_date= $contract['start_date'];
					$contractHistory->contract_end_date = $contract['end_date'];
 					$contractHistory->onboard_changed_end_date= $workOrder['onboard_changed_end_date'];
					$contractHistory->wo_bill_rate= $workOrder['wo_bill_rate'];
					$contractHistory->wo_pay_rate= $workOrder['wo_pay_rate'];
					$contractHistory->wo_over_time= $workOrder['wo_over_time'];
					$contractHistory->wo_double_time= $workOrder['id'];
					$contractHistory->wo_client_over_time= $workOrder['wo_double_time'];
					$contractHistory->wo_client_double_time= $workOrder['wo_client_double_time'];
					$contractHistory->date_created = date("Y-m-d H:i:s");
					
					if($contractHistory->save(false)){ 
							// changes in workorder table
 						  $workOrder->onboard_changed_end_date = $contractExtReq->new_contract_end_date;
						  $workOrder->wo_bill_rate = $contractExtReq->vendor_bill_rate;
						  $workOrder->wo_pay_rate = $contractExtReq->vendor_pay_rate;
						  $workOrder->wo_over_time = $contractExtReq->vendor_overtime_payrate;
						  $workOrder->wo_double_time = $contractExtReq->vendor_doubletime_payrate;
						  $workOrder->wo_client_over_time = $contractExtReq->vendor_overtime_billrate;
						  $workOrder->wo_client_double_time = $contractExtReq->vendor_doubletime_billrate;
					}
					$workOrder->save(false);
				// changes status in contract table 
				 $contractExtReq->bill_req_rejection_date = date("Y-m-d H:i:s");
				$contractExtReq->bill_request_status = 2;
 			}
			$contractExtReq->bill_req_apprej_by = $loginUserId;
			$contractExtReq->save(false); 
		
			$this->redirect(array('extensionView','id'=>$id));	
		}
		
		public function actionContractWflowById(){
		  $clientID = Yii::app()->user->id;
		  $workflow_id = $_POST['id'];
		  $extension_id = $_POST['extension_id'];
		  $contract_id = $_POST['contract_id'];
		  $client = Client::model()->findByPk($clientID);
		  if($client->member_type!=NULL){
			  $client = Client::model()->findByPk($client->super_client_id);
			  $clientID = $client->id;
			  }
		  $query = "select * from vms_contract_workflow where client_id=$clientID AND id = ".$workflow_id; 
		  $contractWorkFlow = Yii::app()->db->createCommand( $query )->query()->readAll();
		  if(isset($contractWorkFlow[0])){
			$members = ContractWorklflowMember::model()->findAllByAttributes(array('contract_flow_id'=>$contractWorkFlow[0]['id']));
			
  			echo UtilityManager::extensionWFAcceptReject($contractWorkFlow[0]['id'],$contractWorkFlow[0]['contract_flow_name'],$members, $clientID, $extension_id , $contract_id);
		  }
		}	
		public function actionApproveExtWorkflow(){
 			if(isset($_POST['rejectbill'])){
				//print_r($_POST);
 				$objId = $_POST['id'];
				$extension_id = $_POST['extension_id'];
				$contract_id = $_POST['contract_id'];
				$extensionAppObj = ContractExtensionApproval::model()->findByPk($objId);
				$extensionAppObj->status = 3;
				$extensionAppObj->rejection_dropdpwn =  $_POST['reason'];
				$extensionAppObj->rejection_reason =  $_POST['note'];
				$extensionAppObj->apprej_date = date('Y-m-d H:i:s');
				//print_r($extensionAppObj);
				$extensionAppObj->save(false);

				 $contractObj = Contract::model()->findByPk($contract_id);
				 $contractObj->ext_status = 3;
				 $contractObj->termination_date = date('Y-m-d H:i:s');
				 $contractObj->term_submited_datetime= date('Y-m-d H:i:s');
				 $contractObj->term_app_rej_datetime= date('Y-m-d H:i:s');
				 $contractObj->save(false);
				//ContractExtensionApproval
			}else{
				$objId = $_GET['id'];
				$extension_id = $_GET['extension_id'];
				$contract_id = $_GET['contract_id'];
				$extensionAppObj = ContractExtensionApproval::model()->findByPk($objId);
				// Approve the request
				$extensionAppObj->status = 2;
				$extensionAppObj->apprej_date = date('Y-m-d H:i:s');
 				$extensionAppObj->save(false);
				// Assign next member to approve the request
				 $next_order = $_GET['order'] +1 ;
				 $query = "select * from vms_contract_extension_approval where workflow_id = ".$_GET['workflow_id']." AND extension_id = ".$_GET['extension_id'];
		 	      $count = Yii::app()->db->createCommand( $query )->query()->count();
				   if($next_order <= $count)
				   {
					  $updateExt = ContractExtensionApproval::model()->findByAttributes(
										  array(
											  'workflow_id' =>$_GET['workflow_id'],
											  'extension_id' =>$_GET['extension_id'],
											  'approval_order' =>$next_order
										  )
									  );
					   $updateExt->status = 1;
					   $updateExt->apprej_date = date('Y-m-d H:i:s');
					   $updateExt->save(false);
				   }else{
					   $contractObj = Contract::model()->findByPk($contract_id);
					   $contractObj->ext_status = 2;
					   $contractObj->termination_date = date('Y-m-d H:i:s');
					   $contractObj->term_submited_datetime= date('Y-m-d H:i:s');
					   $contractObj->term_app_rej_datetime= date('Y-m-d H:i:s');
					   $contractObj->save(false);
				   }
			}
			$this->redirect(array('extensionView','id'=>$extension_id));	
		}
		// 15-03-2017
		public function actionContractExtensionWorkflowList(){ 
		 
		 error_reporting(0);
		 $loginUserId = Yii::app()->user->id;
 		 $memberFlowContractIDs = ContractExtensionApproval::model()->findAllByAttributes(array('client_id'=>$loginUserId));
 		 
		 foreach($memberFlowContractIDs as $idObj){
			 if($idObj->contract_id !=0)
 				$contract_ids[] = $idObj->contract_id;
		  }
		   
		  if(!empty($contract_ids)) {
			 $contract_ids = implode(',' , $contract_ids);
			$criteria=new CDbCriteria();
			//$criteria->condition = 'ext_status IN("1","2","3") and client_id ='.$loginUserId;
			$criteria->condition = 'ext_status IN("1","2","3") AND id IN('.$contract_ids.')';
			$criteria->order = 'id DESC';
			$count=Contract::model()->count($criteria);
			$pages=new CPagination($count);

			// results per page
			$pages->pageSize=10;
			$pages->applyLimit($criteria);
		    $models=Contract::model()->findAll($criteria);
		  }
		//	print_r($models);
		$this->render('contractExtensionList', array(
			 'models' => $models,
			 'pages' => $pages
		));
	}
	// 28-03-2017-ZH
	public function actionContractTerminationCronJob(){ error_reporting(0);
		$query = "select * from vms_contract where termination_status = 2 AND termination_date <> ";
		 $count = Yii::app()->db->createCommand( $query )->query()->count();
 	}
}