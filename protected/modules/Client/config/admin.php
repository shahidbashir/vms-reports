<?php

	$adminConfig = array(
						 'Customers' => array(
											  'NAV' => array(
															 'Add Customers',
															 'Manage Customers'
															 ),
											  
											  'list' => array(
															  'id',
															  'email',
															  //'password',
															  'first_name',
															  'last_name',
															  /*array(
																	'name' => 'Name',
																	'value' => '$data->first_name." ".$data->last_name'
																	),*/
															  //'zipcode',
															  //'address',
															  //'city',
															  //'prefecture',
															  //'telephone_number',
															  //'fax_number',
															  //'title',
															  //'company',
															  //'position',
															  //'signup_date:date',
															  array(
																	'name' => 'signup_date',
																	'value' => 'date("M j, Y", $data->signup_date)'
																	),
															  //'validation_code',
															  'activation_status:boolean',
															 /* array(
																	'name' => 'activation_status',
																	//'value' => '$data->showStatus()'
																	),*/
															  array(
																	'class'=>'CButtonColumn',
																	'viewButtonUrl' => 'Yii::app()->createUrl("Admin/default/view",array("m"=>"Customers","id"=>$data->id))',
																	'updateButtonUrl' => 'Yii::app()->createUrl("Admin/default/update",array("m"=>"Customers","id"=>$data->id))',
																	'deleteButtonUrl' => 'Yii::app()->createUrl("Admin/default/delete",array("m"=>"Customers","id"=>$data->id))'
																	)
															  ),
											  ),
						 
						 'Admin' => array(
											  'NAV' => array(
															 'Add Customers',
															 'Manage Customers'
															 ),
											  
											  'list' => array(
															  'id',
															  'username',
															  'password',
															  'email',
															  'admin_date',
															  'admin_status',
															  ),
											  ),
						 'Agent' => array(
										  'NAV' => array(
															 'Add Customers',
															 'Manage Customers'
															 ),
										  'list' => array(
														  'id',
														  'email',
														  'username',
														  'password',
														  'name',
														  'contact_number',
														  ),
										  )
						 );

?>