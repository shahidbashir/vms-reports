<?php



class ClientModule extends CWebModule

{

	public $adminTitle = 'Admin Panel';

	

	public function init()

	{

		// this method is called when the module is being created

		// you may place code here to customize the module or the application



		// import the module-level models and components

		$this->setImport(array(

			'Client.models.*',

			'Client.components.*',

		));

		Yii::app()->setComponents(array(

			'user'=>array(

				// enable cookie-based authentication

				'allowAutoLogin'=>true,

				'loginUrl' => array

				(

					'0' => 'Client/default/login'

				)

			),

		));



		Yii::app()->user->setStateKeyPrefix('admin');

	}

	

	public function action()

	{

		return array('components'=>array(

					'user'=>array(

						// enable cookie-based authentication

						'class'=>'UserLogin',

						'loginUrl'=>array('Client/default/login'),

					),

			  ));

	}



	public function beforeControllerAction($controller, $action)

	{

		if(parent::beforeControllerAction($controller, $action))

		{

			$admin = Yii::app()->user->getState('isAdmin');

			if(Yii::app()->user->isGuest || ($admin == '' || $admin == false))

			{

				$controller->layout = 'login';

			}

			else

			{

				$controller->layout = 'admin';

			}

			// this method is called before any module controller action is performed

			// you may place customized code here

			return true;

		}

		else

			return false;

	}

	

	public function getModuleConfig()

	{

		require dirname(__FILE__).'/config/admin.php';

		return $adminConfig;

	}

}

