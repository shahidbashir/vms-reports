<?php 
$this->pageTitle = 'Interview Schedule';
/*echo '<pre>';
print_r($todayInterview);
exit;*/
if(isset($_GET['st'])=='pending'){	
	  ?>
    <div class="client-workspace">
      <?php if(Yii::app()->user->hasFlash('success')):
              echo Yii::app()->user->getFlash('success');
        endif; ?>
      <?php
      $jobID = $_GET['jobid']; 
      $jobmodel = Job::model()->findByPk($jobID);
      $this->renderPartial('/job/_menu',array('model'=>$jobmodel));?>
    </div>
    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="interviews">
        <div class="row">
          <div class="col-md-12">
            <h5 class="m-t-10 ">Interview Schedule</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse nam illo nisi placeat temporibus corporis veritatis sequi! Assumenda, aliquam similique.</p>
            <br>
            <div class="client-setting">
              <?php $this->renderPartial('/job/_menu_interview',array('model'=>$jobmodel));?>
              <!-- navbar --> 
              
              <br>
              <h5 class="m-t-10 "><?php echo date('D M Y'); ?></h5>
              <table class="table table-striped dataTable no-footer">
            <thead>
              <tr>
                <th>Interview ID</th>
                <th>Time</th>
                <th>Name of Candidate</th>
                <th>Type of Interview</th>
                <th>Status</th>
                <th style="text-align: center">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($pendingInterview as $pendingData){
				$postingstatus = UtilityManager::interviewStatus();
				$candidateData = Candidates::model()->findByPk($pendingData['candidate_Id']);
				//$interviewData = Interview::model()->findByAttributes(array('submission_id'=>$pendingData['id']));
				
                switch ($postingstatus[$pendingData['status']]) {
                    case "Approved":
                        $color = 'label-success';
                        break;
                    case "Waiting for Approval":
                        $color = 'label-info';
                        break;
                    case "Cancelled":
                        $color = 'label-default';
                        break;
                    case "Reschedule":
                        $color = 'label-primary';
                        break;
                    default:
                        $color = 'label-primary';
                }
                
                 ?>
              <tr>
                <td><a href="">ID-<?php echo $pendingData['id'] ?></a></td>
                <td>
                
                <?php
        
                  if($pendingData['start_date_status']){
                    echo $pendingData['interview_start_date'].' '.$pendingData['interview_start_date_time'];?> to <?php echo $pendingData['interview_end_date_time'];
                  }
        
                  else if($pendingData['alt1_date_status']){
                    echo $pendingData['interview_alt1_start_date'].' '.$pendingData['interview_alt1_start_date_time'];?> to <?php echo $pendingData['interview_alt1_end_date_time'];
                  }
                  else if($pendingData['alt2_date_status']){
                    echo $pendingData['interview_alt2_start_date'].' '.$pendingData['interview_alt2_start_date_time'];?> to <?php echo $pendingData['interview_alt2_end_date_time'];
                  }
                  else if($pendingData['alt3_date_status']){
                    echo $pendingData['interview_alt3_start_date'].' '.$pendingData['interview_alt3_start_date_time'];?> to <?php echo $pendingData['interview_alt3_end_date_time'];
                  }else{
					  echo $pendingData['interview_start_date'].' '.$pendingData['interview_start_date_time'];?> to <?php echo $pendingData['interview_end_date_time'];
					  }
                  ?>
                  
                  </td>
                <td><?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?></td>
                <td><?php echo $pendingData['interview_type'] ?></td>
                <td><span class="label <?php echo $color ?>"><?php echo $postingstatus[$pendingData['status']] ?></span></td>
                <td style="text-align: center"><a href="job-info.html" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Note"><i class="fa  fa-sticky-note-o"></i></a> 
                <a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$pendingData['job_id'],'submission-id'=>$pendingData['submissionID'],'interviewId'=>$pendingData['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="fa fa-eye"></i></a>
                </td>
              </tr>
              
              <?php } ?>
            </tbody>
          </table>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php }else{ ?>
<div class="client-workspace">
  <?php if(Yii::app()->user->hasFlash('success')):
		  echo Yii::app()->user->getFlash('success');
	endif; ?>
  <?php
  $jobID = $_GET['jobid']; 
  $jobmodel = Job::model()->findByPk($jobID);
  $this->renderPartial('/job/_menu',array('model'=>$jobmodel));?>
</div>
<!-- Tab panes -->
<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="interviews">
    <div class="row">
      <div class="col-md-12">
        <h5 class="m-t-10 ">Interview Schedule</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse nam illo nisi placeat temporibus corporis veritatis sequi! Assumenda, aliquam similique.</p>
        <br>
        <div class="client-setting">
          <?php $this->renderPartial('/job/_menu_interview',array('model'=>$jobmodel));?>
          <!-- navbar --> 
          
          <br>
          <h5 class="m-t-10 "><?php echo date('D M Y'); ?></h5>
  		  <table class="table table-striped dataTable no-footer">
        <thead>
          <tr>
            <th>Interview ID</th>
            <th>Time</th>
            <th>Name of Candidate</th>
            <th>Type of Interview</th>
            <th>Status</th>
            <th style="text-align: center">Action</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach($todayInterview as $todayData){
			$postingstatus = UtilityManager::interviewStatus();
			$candidateData = Candidates::model()->findByPk($todayData['candidate_Id']);
			//$interviewData = Interview::model()->findByAttributes(array('submission_id'=>$todayData['id']));
				
                switch ($postingstatus[$todayData['status']]) {
                    case "Approved":
                        $color = 'label-success';
                        break;
                    case "Waiting for Approval":
                        $color = 'label-info';
                        break;
                    case "Cancelled":
                        $color = 'label-default';
                        break;
                    case "Reschedule":
                        $color = 'label-primary';
                        break;
                    default:
                        $color = 'label-primary';
                }
            
             ?>
          <tr>
            <td><a href="">ID-<?php echo $todayData['id'] ?></a></td>
            <td>
            
            <?php
        
			  if($todayData['start_date_status']){
				echo $todayData['interview_start_date'].' '.$todayData['interview_start_date_time'];?> to <?php echo $todayData['interview_end_date_time'];
			  }
	
			  else if($todayData['alt1_date_status']){
				echo $todayData['interview_alt1_start_date'].' '.$todayData['interview_alt1_start_date_time'];?> to <?php echo $todayData['interview_alt1_end_date_time'];
			  }
			  else if($todayData['alt2_date_status']){
				echo $todayData['interview_alt2_start_date'].' '.$todayData['interview_alt2_start_date_time'];?> to <?php echo $todayData['interview_alt2_end_date_time'];
			  }
			  else if($todayData['alt3_date_status']){
				echo $todayData['interview_alt3_start_date'].' '.$todayData['interview_alt3_start_date_time'];?> to <?php echo $todayData['interview_alt3_end_date_time'];
			  }else{
					  echo $todayData['interview_start_date'].' '.$todayData['interview_start_date_time'];?> to <?php echo $todayData['interview_end_date_time'];
					  }
			  ?>
              
              </td>
            <td><?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?></td>
            <td><?php echo $todayData['interview_type'] ?></td>
            <td><span class="label <?php echo $color ?>"><?php echo $postingstatus[$todayData['status']] ?></span></td>
            <td style="text-align: center"><a href="job-info.html" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Note"><i class="fa  fa-sticky-note-o"></i></a> 
            <a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$todayData['job_id'],'submission-id'=>$todayData['submissionID'],'interviewId'=>$todayData['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="fa fa-eye"></i></a>
            </td>
          </tr>
          
          <?php } ?>
        </tbody>
      </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php	} ?>
