<?php $this->pageTitle = 'Interview Schedule'; ?>
<div class="main-content">
    <div class="row">

        <?php if(Yii::app()->user->hasFlash('success')):
            echo Yii::app()->user->getFlash('success');
        endif; ?>
        <!--Condensed Table-->

        <!--Hover table-->
        <div class="col-sm-12">
            <div class="panel panel-default panel-table">
                <div class="panel-heading"> List of Cancelled Interviews </div>
                <div class="panel-body">
                    <div class="clarfix">
                        <div class="col-xs- col-sm- col-md- col-lg-">
                            <div role="tabpanel">
                                <!-- Nav tabs -->


                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="today-interview">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th width="10%">Interview ID</th>
                                                <th width="15%">Date </th>
                                                <th>Time</th>
                                                <th>Candidate</th>
                                                <th>Job</th>
                                                <th>Type of Interview</th>
                                                <th>Status</th>
                                                <th class="actions">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php
                                            foreach($interview as $allData){

                                                $postingstatus = UtilityManager::interviewStatus();
                                                $candidateData = Candidates::model()->findByPk($allData['candidate_Id']);

                                                switch ($postingstatus[$allData['status']]) {
                                                    case "Approved":
                                                        $color = 'label label-success';
                                                        break;
                                                    case "Waiting for Approval":
                                                        $color = 'label label-danger';
                                                        break;
                                                    case "Cancelled":
                                                        $color = 'label label-default';
                                                        break;
                                                    case "Reschedule":
                                                        $color = 'label-primary';
                                                        break;
                                                    default:
                                                        $color = 'label-primary';
                                                }


                                                if($allData['start_date_status']){
                                                    $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
                                                }

                                                else if($allData['alt1_date_status']){
                                                    $date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
                                                }
                                                else if($allData['alt2_date_status']){
                                                    $date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
                                                }
                                                else if($allData['alt3_date_status']){
                                                    $date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
                                                }else{
                                                    $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
                                                }

                                                $newTime = explode(' ',$date,2);

                                                $jobData = Job::model()->findByPk($allData['job_id']);
                                                ?>
                                                <tr>
                                                    <td class="user-avatar">ID-<?php echo $allData['id'] ?></td>
                                                    <td><?php echo $newTime[0]; ?></td>
                                                    <td><?php echo $newTime[1]; ?></td>
                                                    <td><a href="" class="blue"><?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?></a></td>
                                                    <td><a href="" class="blue"><?php echo $jobData->title; ?></a></td>
                                                    <td><?php echo $allData['interview_type'] ?></td>
                                                    <td><span class="<?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span></td>
                                                    <td class="actions"><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>" class="icon"><i class="mdi mdi-eye"></i></a> <a href="#" class="icon"><i class="mdi mdi-delete"></i></a></td>
                                                </tr>

                                            <?php } ?>
                                            </tbody>
                                        </table>
                                        <?php
                                        $this->widget('CLinkPager', array(
                                            'pages' => $pages,
                                            'header' => '',
                                            'nextPageLabel' => 'Next',
                                            'prevPageLabel' => 'Prev',
                                            'selectedPageCssClass' => 'active',
                                            'hiddenPageCssClass' => 'disabled',
                                            'htmlOptions' => array(
                                                'class' => 'pagination',
                                            )
                                        ))
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- col -->
                    </div>
                    <!-- row -->

                </div>
            </div>
        </div>
    </div>
</div>

