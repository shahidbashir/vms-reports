<div class="row table-row-to-remove-margin" style="margin-top: -37px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table m-b-30 m-t-20 without-border">
            <thead class="thead-default">
            <tr>
                <th style="width: 10px;">Status</th>
                <th>Type</th>
                <th style="width: 109px;"><a href="">ID</a></th>
                <th>Name</th>
                <th> Job( Job ID ) </th>
                <th>Date</th>
                <th style="width: 96px;">Start Time</th>
                <th style="width: 96px;">End Time</th>
                <!--<th style="width: 100px;">Location</th>-->
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody id="upcome">
            <?php
            $umodel = $upcommingInterview->getData();
            if($umodel){
                foreach($umodel as $allData){

                    $location = Location::model()->findByPk($allData['interview_where']);
                    $postingstatus = UtilityManager::interviewStatus();
                    $candidateData = Candidates::model()->findByPk($allData['candidate_Id']);

                    switch ($postingstatus[$allData['status']]) {
                        case "Approved":
                            $color = 'label-open';
                            break;
                        case "Waiting for Approval":
                            $color = 'label-interview-pending';
                            break;
                        case "Cancelled":
                            $color = 'label-interview-cancelled';
                            break;
                        case "Reschedule":
                            $color = 'label-interview-reschedule';
                            break;
                        case "Interview Completed":
                            $color = 'label-interview-completed';
                            break;
                        default:
                            $color = 'label label-primary';
                    }


                    if($allData['start_date_status']){
                        $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
                    }

                    else if($allData['alt1_date_status']){
                        $date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
                    }
                    else if($allData['alt2_date_status']){
                        $date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
                    }
                    else if($allData['alt3_date_status']){
                        $date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
                    }else{
                        $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
                    }

                    $newTime = explode(' ',$date);

                    $jobData = Job::model()->findByPk($allData['job_id']);
                    ?>
                    <tr>
                        <td><span class="tag <?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span></td>
                        <td><?php echo $allData['interview_type'] ?></td>
                        <td><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>"><?php echo $allData['id'] ?></a></td>
                        <td><a href=""><?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?></a></td>
                        <td><a href="">
                                <?php echo $jobData->title.'('. $jobData->id.')'; ?>
                            </a></td>
                        <td><?php echo date('m-d-Y',strtotime($newTime[0])); ?></td>
                        <td><?php echo $newTime[1] ?></td>
                        <td><?php echo $newTime[3]; ?></td>
                        <!--<td ><?php /*echo $location->name; */?></td>-->
                        <td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                    </tr>
                <?php }
            }else{ ?>
                <tr>
                    <td colspan="10">Sorry No Record Found</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="row m-b-10" style="padding: 10px 0 10px; ">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $upcommingInterview->pagination,
                    'header' => '',
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class' => 'pagination m-t-0 m-b-0',
                    )
                ))
                ?>
            </div>
            <!--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <p class="text-right"> Showing 10 to 20 of 50 entries </p>
           </div>-->
        </div>
    </div>
</div>