<?php $this->pageTitle = 'Interviews'; error_reporting(0); //echo '<pre>'; print_r($completedInterviews->getData()); exit; ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List of Interviews <!--<a href="" class="pull-right"><i class="fa fa-question"></i></a> <a href="" class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
      <p class="m-b-20">A Completed list of interview schedule for jobs.</p>
      <?php if(Yii::app()->user->hasFlash('success')):
                            echo Yii::app()->user->getFlash('success');
                            endif; ?>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="sdata" placeholder="Search Interviews by Type, ID , Name , Job">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <div class="row m-b-20">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <form name="search" method="post">
            <div class="two-fields-70-30">
              <div class="form-group form-control-2">
                <?php $jobs = Job::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id));?>
                <label for="" style="text-align: left;">Sort by Job Name</label>
                <select name="job_id" id="input" class="form-control  select2" required>
                  <option value="">Select</option>
                  <?php foreach ($jobs as $jobvalue) { ?>
                  <option value="<?php echo $jobvalue->id; ?>"><?php echo $jobvalue->title.'('.$jobvalue->id.')';?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="" >&nbsp; </label>
                <p>
                  <button type="submit" name="search" class="btn btn-success">Show</button>
                </p>
              </div>
            </div>
          </form>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <p class="bold m-t-40"> <a href="" class="pull-right"> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/listing@512px.svg"></i> </a> <a href="" class="pull-right" style="margin-right: 10px; "> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/menu@512px.svg"></i> </a> </p>
        </div>
      </div>
      <div class="simplify-tabs m-b-50">
        <div role="tabpanel"> 
          <!-- Nav tabs -->
          <ul class="nav nav-tabs no-hover" id="interview" role="tablist">
            <li class="nav-item" id="today"> <a class="nav-link "  href="#home" role="tab" data-toggle="tab" data-placement="bottom" title="Today Interview"> Today ( <?php echo date('m-d-Y'); ?> )</a> </li>
              <li class="nav-item" id="upcomming"> <a class="nav-link "  href="#tomorrow" role="tab" data-toggle="tab" data-placement="bottom" title="Upcoming Interview"> Upcoming</a> </li>
              <li class="nav-item" id="waiting"> <a class="nav-link "  href="#Waiting" role="tab" data-toggle="tab" data-placement="bottom" title="Waiting for Approval"> Waiting</a> </li>
              <li class="nav-item" id="cencelled"> <a class="nav-link "  href="#Canceled" role="tab" data-toggle="tab" data-placement="bottom" title="Canceled"> Cancelled</a> </li>
              <li class="nav-item" id="Completed"> <a class="nav-link "  href="#completed" role="tab" data-toggle="tab" data-placement="bottom" title="Completed Interviews"> Completed</a> </li>
              <li class="nav-item" id="all"> <a class="nav-link active"  href="#tab"role="tab" data-toggle="tab" data-placement="bottom" title="All Interview"> All</a> </li>
              <li class="nav-item"> <a class="nav-link "  href="#tab2" role="tab" data-toggle="tab" data-placement="bottom" title="Calendar View"> Calendar</a> </li>
          </ul>
          <div class="tab-content ">
            <div class="tab-pane" id="home" role="tabpanel">
              <div class="row table-row-to-remove-margin" style="margin-top: -37px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <table class="table m-b-30 m-t-20 without-border">
                    <thead class="thead-default">
                      <tr>
                        <th style="width: 10px;">Status</th>
                        <th>Type</th>
                        <th style="width: 109px;"><a href="">ID</a></th>
                        <th>Name</th>
                        <th> Job( Job ID ) </th>
                        <th>Date</th>
                        <th style="width: 96px;">Start Time</th>
                        <th style="width: 96px;">End Time</th>
                        <!--<th style="width: 100px;">Location</th>-->
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody id="now">
                      <?php
                      $model = $todayInterview->getData();
                        if($model){
         foreach($model as $allData){
            
            $location = Location::model()->findByPk($allData['interview_where']);
            $postingstatus = UtilityManager::interviewStatus();
            $candidateData = Candidates::model()->findByPk($allData['candidate_Id']);
            
                switch ($postingstatus[$allData['status']]) {
                    case "Approved":
                        $color = 'label-open';
                        break;
                    case "Waiting for Approval":
                        $color = 'label-interview-pending';
                        break;
                    case "Cancelled":
                        $color = 'label-interview-cancelled';
                        break;
                    case "Reschedule":
                        $color = 'label-interview-reschedule';
                        break;
               case "Interview Completed":
                        $color = 'label-interview-completed';
                        break;
                    default:
                        $color = 'label label-primary';
                }
                
        
                  if($allData['start_date_status']){
                    $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
                  }
        
                  else if($allData['alt1_date_status']){
                    $date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
                  }
                  else if($allData['alt2_date_status']){
                    $date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
                  }
                  else if($allData['alt3_date_status']){
                    $date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
                  }else{
                 $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
                 }

            $newTime = explode(' ',$date);
            
            $jobData = Job::model()->findByPk($allData['job_id']);
                  ?>
                      <tr>
                        <td><span class="tag <?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span></td>
                        <td><?php echo $allData['interview_type'] ?></td>
                        <td><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>"><?php echo $allData['id'] ?></a></td>
                        <td><a href="">
                                <?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?>
                            </a></td>
                        <td><a href="">
                                <?php echo $jobData->title.'('. $jobData->id.')'; ?>
                          </a></td>
                        <td><?php echo date('m-d-Y',strtotime($newTime[0])); ?></td>
                          <td><?php echo $newTime[1] ?></td>
                          <td><?php echo $newTime[3]; ?></td>
                        <!--<td ><?php /*echo $location->name; */?></td>-->
                        <td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                      </tr>
                      <?php }
                                                   }else{ ?>
                      <tr>
                        <td colspan="10">Sorry No Record Found</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <div class="row m-b-10" style="padding: 10px 0 10px; ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <?php
                          $this->widget('CLinkPager', array(
                              'pages' => $todayInterview->pagination,
                              'header' => '',
                              'nextPageLabel' => 'Next',
                              'prevPageLabel' => 'Prev',
                              'selectedPageCssClass' => 'active',
                              'hiddenPageCssClass' => 'disabled',
                              'htmlOptions' => array(
                                  'class' => 'pagination m-t-0 m-b-0',
                              )
                          ))
                          ?>
                    </div>
                      <!--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                 <p class="text-right"> Showing 10 to 20 of 50 entries </p>
             </div>-->
                  </div>
                </div>
              </div>
              <!-- row --> 
            </div>
            <div class="tab-pane active" id="tab" role="tabpanel">
              <div class="row table-row-to-remove-margin" style="margin-top: -37px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <table class="table m-b-30 m-t-20 without-border">
                    <thead class="thead-default">
                      <tr>
                        <th style="width: 10px;">Status</th>
                        <th>Type</th>
                        <th style="width: 109px;"><a href="">ID</a></th>
                        <th>Name</th>
                        <th> Job( Job ID ) </th>
                        <th>Date</th>
                        <th style="width: 96px;">Start Time</th>
                        <th style="width: 96px;">End Time</th>
                        <!--<th style="width: 100px;">Location</th>-->
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody id="subdata">
                      <?php
                      $model1 = $interview->getData();
                      if($model1){
         foreach($model1 as $allData){
$location = Location::model()->findByPk($allData['interview_where']);
            $postingstatus = UtilityManager::interviewStatus();
            $candidateData = Candidates::model()->findByPk($allData['candidate_Id']);
            
                switch ($postingstatus[$allData['status']]) {
                    case "Approved":
                        $color = 'label-open';
                        break;
                    case "Waiting for Approval":
                        $color = 'label-interview-pending';
                        break;
                    case "Cancelled":
                        $color = 'label-interview-cancelled';
                        break;
                    case "Reschedule":
                        $color = 'label-interview-reschedule';
                        break;
               case "Interview Completed":
                        $color = 'label-interview-completed';
                        break;
                    default:
                        $color = 'label label-primary';
                }
                
        
                  if($allData['start_date_status']){
                    $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
                  }
        
                  else if($allData['alt1_date_status']){
                    $date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
                  }
                  else if($allData['alt2_date_status']){
                    $date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
                  }
                  else if($allData['alt3_date_status']){
                    $date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
                  }else{
                 $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
                 }

            $newTime = explode(' ',$date);
            $jobData = Job::model()->findByPk($allData['job_id']);
                  ?>
                      <tr>
                        <td><span class="tag <?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span></td>
                        <td><?php echo $allData['interview_type'] ?></td>
                        <td><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>"><?php echo $allData['id'] ?></a></td>
                        <td><a href="">
                          <?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?>
                          </a></td>
                        <td><a href=""><?php echo $jobData->title.'('.$jobData->id.')'; ?></a></td>
                        <td><?php echo date('m-d-Y',strtotime($newTime[0])); ?></td>
                        <td><?php echo $newTime[1] ?></td>
                        <td><?php echo $newTime[3]; ?></td>
                        <!--<td><?php /*echo $location->name; */?></td>-->
                        <td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                      </tr>
                      <?php } 
                         }else{ ?>
                      <tr>
                        <td colspan="10"> Sorry No Record Found</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <div class="row m-b-10" style="padding: 10px 0 10px; ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <?php
                      $this->widget('CLinkPager', array(
                          'pages' => $interview->pagination,
                          'header' => '',
                          'nextPageLabel' => 'Next',
                          'prevPageLabel' => 'Prev',
                          'selectedPageCssClass' => 'active',
                          'hiddenPageCssClass' => 'disabled',
                          'htmlOptions' => array(
                              'class' => 'pagination m-t-0 m-b-0',
                          )
                      ))
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <div class="tab-pane" id="tomorrow" role="tabpanel">
                  <?php $this->renderPartial('upcoming',array('upcommingInterview'=>$upcommingInterview)); ?>
                  <!-- row -->
              </div>
              <div class="tab-pane" id="completed" role="tabpanel">
                  <?php $this->renderPartial('completed',array('statuscompleted'=>$statuscompleted)); ?>
                  <!-- row -->
              </div>
              <div class="tab-pane" id="Waiting" role="tabpanel">
                  <?php $this->renderPartial('waitinglist',array('waitingforApproval'=>$waitingforApproval)); ?>
                  <!-- row -->
              </div>
              <div class="tab-pane" id="Canceled" role="tabpanel">
                  <?php $this->renderPartial('rejectedlist',array('rejectedInterview'=>$rejectedInterview)); ?>
                  <!-- row -->
              </div>

            <!-- tab-pane -->
            <div class="tab-pane " id="tab2" role="tabpanel">
              <?php $this->renderPartial('calendarView',array('completedInterviews'=>$completedInterviews)); ?>
            </div>
            <!-- tab-pane --> 
          </div>
        </div>
      </div>
    </div>
    <!-- col --> 
  </div>
  <!-- row -->
  <div class="clearfix" style="padding: 10px 20px 30px; ">

  </div>
  <div class="seprater-bottom-100"></div>
</div>


<script>
    $(document).ready(function(){
        $('#sdata').keyup(function(){
            var status = $("#interview").find("a.active").parent('li').attr('id')
            var subValue=$(this).val();
            //alert(status);
            $.ajax({
                'url':'<?php echo $this->createUrl('costCenter/interviewSearch') ?>',
                type: "POST",
                data: { subValue: subValue,status:status},
                'success':function(html){
                    //alert(html);
                    if(status == 'today'){
                        $("#now").html(html)
                    }else if(status =='upcomming'){
                        $("#upcome").html(html)
                    }else if(status =='waiting'){
                        $("#wait").html(html)
                    }else if(status =='cencelled'){
                        $("#cencele").html(html)
                    }else if(status =='Completed'){
                        //alert(status);
                        $("#Complete").html(html)
                    }else {
                        $("#subdata").html(html)
                    }
                }
            });
        });
    });
</script>