<?php //echo '<pre>'; print_r($completedInterviews->getData()); exit; ?>
<?php
		$allData = '';
		$url = '';
		$eventColor = '';
		$interviewData = $completedInterviews->getData();
		
		foreach($interviewData as $key=>$value){
				if($value['start_date_status']){
					$interviewDate = $value['interview_start_date'].'T'.date("H:i:s", strtotime($value['interview_start_date_time']));
				}
				else if($value['alt1_date_status']){
					$interviewDate = $value['interview_alt1_start_date'].'T'.date("H:i:s", strtotime($value['interview_alt1_start_date_time']));
				}
				else if($value['alt2_date_status']){
					$interviewDate = $value['interview_alt2_start_date'].'T'.date("H:i:s", strtotime($value['interview_alt2_start_date_time']));
				}
				else if($value['alt3_date_status']){
					$interviewDate = $value['interview_alt3_start_date'].'T'.date("H:i:s", strtotime($value['interview_alt3_start_date_time']));
				}else{
					$interviewDate = $value['interview_start_date'].'T'.date("H:i:s", strtotime($value['interview_start_date_time']));
				}
				
				
			if($value['interview_type']=='Phone Interview'){
			   $eventColor = '#ff5a5f';
			   }elseif($value['interview_type']=='Online Interview'){
				    $eventColor = '#ffb400';
					}elseif($value['interview_type']=='Face to Face Interview'){
				    $eventColor = '#007a87';
					}else{
						 $eventColor = '#7b0051';
						 }
				
				
				$allData .= "
					{
                     title: '".$value['interview_event_name']."',
                     start: '".$interviewDate."',
					 url: '".$this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$value['job_id'],'submission-id'=>$value['submission_id'],'interviewId'=>$value['id']))."',
					 color: '".$eventColor."',
                  	
					},
				  ";
			}
			//echo $url;
	?>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div id='calendar'></div>
  </div>
</div>
<script type="text/javascript">
   
   $(document).ready(function() {
      var date = new Date();
      $('a[href="#tab2"]').on('shown.bs.tab', function (e) {
            $('#calendar').fullCalendar({
               header: {
                  left: 'prev,next today',
                  center: 'title',
                  right: 'month,agendaWeek,agendaDay,listWeek'
               },
               defaultDate: date,
               navLinks: true, // can click day/week names to navigate views
               editable: true,
               eventLimit: true, // allow "more" link when too many events
               events: [
                  <?php echo $allData; ?>
               ],
			   eventColor: '#378006',
			   eventClick: function(event) {
					if (event.url) {
						window.open(event.url);
						return false;
					}
				}
            });
            
         });
      });

</script>