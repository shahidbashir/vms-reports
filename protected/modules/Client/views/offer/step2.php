<?php $this->pageTitle = 'Offer Creation';
		$clientRateData = ClientRate::model()->findByAttributes(array('client_id'=>$jobData->user_id));
		$over_time = 0;
		$half_time = 0;
	    $double_time = 0;
		if($clientRateData){  
			$over_time = $clientRateData->over_time;
  			$double_time = $clientRateData->double_time;
		}
		$categoryID = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$jobData->cat_id));
		$mark_up = $submissionData->makrup;
		//picking up bill rate value for the No markup job category.
		
		$settingData = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$jobData->cat_id));
		$noMarkupCat = NoMarkupCate::model()->findByAttributes(array('category_id'=>$settingData->id));	
		
		if(!empty($noMarkupCat->bill_rate)){
			$billrate = $noMarkupCat->bill_rate;
			}else{
				 $billrate = $submissionData->candidate_pay_rate + $submissionData->candidate_pay_rate * ($mark_up)/100;
				 }
			
			//$billrate = $submissionData->candidate_pay_rate + $submissionData->candidate_pay_rate*$mark_up/100;
			$jobLocations = Location::model()->findAllByAttributes(array('id'=>unserialize($jobData->location)));
			$vendor = Vendor::model()->findByPk($submissionData->vendor_id);
?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10"><?php echo $jobData->title; ?> </h4>
      <p class="m-b-42">Location : <?php foreach($jobLocations as $jobLocation){
		  echo $jobLocation->name.', ';
		  } ?></p>
      <form action="" method="POST" role="form">
      <div class="row add-job">
      <div class="col-md-12">
      <div class="panel panel-border-color panel-border-color-primary">
      <div class="panel-heading"></div>
      <div class="panel-body">
      <table class="table table-condensed table-50 ">
        <tbody>
          <tr>
            <td>Candidate Name</td>
            <td><?php echo $candidateData->first_name.' '.$candidateData->last_name; ?></td>
          </tr>
          <tr>
            <td>Current Location</td>
            <td><?php echo $candidateData->current_location; ?></td>
          </tr>
          <tr>
            <td>Vendor Name</td>
            <td><?php echo $vendor->organization; ?></td>
          </tr>
        </tbody>
      </table>
      <br>
      <hr>
      <br>
      <h4 class="m-b-20">Job Start Date and Other Information</h4>
      <table class="table table-condensed table-50 ">
        <tbody>
          <tr>
            <td>Start Date<i class="required-field fa fa-asterisk"></i></td>
            <td><input type="text" name="job_start_date" value="<?php echo date('m/d/Y',strtotime($jobData->job_po_duration)); ?>"  onchange="calculate()" class="form-control datetimepickermodified dateField123"></td>
          </tr>
          <tr>
            <td>End Date<i class="required-field fa fa-asterisk"></i></td>
            <td><input type="text" name="job_end_date" value="<?php echo date('m/d/Y',strtotime($jobData->job_po_duration_endDate)); ?>"  onchange="calculate()" class="form-control datetimepickermodified dateField1234"></td>
          </tr>
          <tr>
            <td>Location<i class="required-field fa fa-asterisk"></i></td>
            <td><select name="approver_manager_location" id="approver_manager_location" class="form-control" required>
                <option value=""></option>
                <?php  foreach($jobLocations as $jobLocations){ ?>
                <option value="<?php echo $jobLocations->id; ?>"><?php echo $jobLocations->name; ?></option>
                <?php }  ?>
              </select></td>
          </tr>
          <tr>
            <td>Location Manager</td>
            <td><select name="location_manager" id="location_manager" class="form-control" >
                <option value=""></option>
              </select></td>
          </tr>
        </tbody>
      </table>
      <br>
      <hr>
      <br>
      <h4 class="m-b-20">Rates</h4>
      <table class="table table-condensed table-50 ">
        <input type="hidden" readonly name="payment_type" id="payment_type" class="form-control" value="<?php echo $jobData->payment_type; ?>" required="required">
        <input type="hidden" readonly name="hours_per_week" id="hours_per_week" class="form-control" value="<?php echo $jobData->hours_per_week; ?>" required="required">
        <input type="hidden" readonly name="shift" id="shift" class="form-control" value="<?php echo $jobData->shift; ?>" required="required">
        <!--on ajax call find the vendor highest markup and fill its value in this field-->
        <input type="hidden" name="Job[vendor_client_markup]" id="vendor_client_markup123" class="form-control" value="<?php echo $mark_up; ?>">
        <input type="hidden" id="calculated_value" name="calculated_value" value="" class="form-control"  />
        <tbody>
          <tr>
            <td>Pay Rate<i class="required-field fa fa-asterisk"></i></td>
            <td><input type="text" name="pay_rate" id="pay_rate" class="form-control" value="<?php echo $submissionData->candidate_pay_rate; ?>" required="required" onchange="calculate('payrate')"></td>
          </tr>
          <tr>
            <td>Bill Rate<i class="required-field fa fa-asterisk"></i></td>
            <td><input type="text" name="bill_rate" id="bill_rate" class="form-control" value="<?php echo $billrate; ?>" required="required" onchange='calculate("billrate")'></td>
          </tr>
          <?php if(!empty($noMarkupCat)){ ?>
          <tr>
            <td>Vendor Bill Rate</td>
            <td><input type="text" readonly="readonly" class="form-control" value="<?php echo $submissionData->vendor_bill_rate; ?>" ></td>
          </tr>
          <?php } ?>
          <tr>
            <td>Over Time Rate for Candidate</td>
            <td><input type="text" id="over_time" readonly name="over_time" class="form-control" value="<?php echo $submissionData->candidate_pay_rate + $submissionData->candidate_pay_rate*$over_time/100; ?>" required="required" title=""></td>
          </tr>
          <tr>
            <td>Double Time Rate for Candidate</td>
            <td><input type="text" id="double_time" readonly name="double_time" class="form-control" value="<?php echo $submissionData->candidate_pay_rate + $submissionData->candidate_pay_rate*$double_time/100; ?>" required="required"  title=""></td>
          </tr>
          <tr>
            <td>Over Time Rate for Client</td>
            <td><input type="text" id="client_over_time" readonly name="client_over_time" class="form-control" value="<?php echo $billrate + $billrate*$over_time/100; ?>" required="required" title=""></td>
          </tr>
          <tr>
            <td>Double Time Rate for Client</td>
            <td><input type="text" id="client_double_time" readonly name="client_double_time" class="form-control" value="<?php echo $billrate + $billrate*$double_time/100; ?>" required="required"  title=""></td>
          </tr>
          <tr>
            <td>Offer Expire Date<i class="required-field fa fa-asterisk"></i></td>
            <td><input type="text" name="valid_till_date" value="<?php //echo date('m/d/Y',strtotime($jobDuration[1])); ?>"  class="form-control datetimepickermodified" required="required"></td>
          </tr>
        </tbody>
      </table>
      <br>
      <hr>
      <br>
      <div class="clearfix no-margin">
      <div class="col-md-12">
      <h4>Notes &amp; Comments</h4>
      <div class="simplify-tabs m-b-42">
      <div role="tabpanel">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs no-hover" role="tablist">
        <li class="nav-item active"> <a class="nav-link "  href="#internal" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class="nav-icon"></i> Internal</a> </li>
        <li class="nav-item"> <a class="nav-link" href="#msp" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> 
        <i class="nav-icon"></i> MSP</a> </li>
        <li class="nav-item"> 
        <a class="nav-link "  href="#vendor" role="tab" data-toggle="tab" data-placement="bottom" title="Overview">
        <i class="nav-icon"></i> Vendor</a> 
        </li>
      </ul>
      <div class="tab-content ">
      <div class="tab-pane active" id="internal"  role="tabpanel">
      <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      
        <div class="form-group">
          <label for="">Note</label>
          <textarea name="ref_notes" id="input" class="form-control" rows="5"></textarea>
        </div>
        <br>
        <br>
      
    </div>
  </div>
</div>
<div class="tab-pane" id="msp"  role="tabpanel">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      
        <div class="form-group">
          <label for="">Note</label>
          <textarea name="ref_notes_msp" id="input" class="form-control" rows="5"></textarea>
        </div>
        <br>
      
    </div>
  </div>
</div>
<div class="tab-pane" id="vendor"  role="tabpanel">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      
        <div class="form-group">
          <label for="">Note</label>
          <textarea name="ref_notes_vendor" id="input" class="form-control" rows="5"></textarea>
        </div>
        <br>
      
    </div>
  </div>
</div>
</div>
</div>
<!-- tabpanel -->
</div>
<!-- simplify-tabs -->
<hr>
<br>
          <!--<h4>Teams</h4>
<div class="simplify-tabs m-b-42">
  <div role="tabpanel">
    <ul class="nav nav-tabs no-hover" role="tablist">
      <li class="nav-item active"> <a class="nav-link "  href="#internal2" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> 				<i class=" nav-icon"></i> MSP</a>
      </li>
      <li class="nav-item"> <a class="nav-link "  href="#vendor2" role="tab" data-toggle="tab" data-placement="bottom" title="Overview">
      <i class=" nav-icon"></i> Client</a>
      </li>
    </ul>
    <div class="tab-content ">
      <div class="tab-pane active" id="internal2"  role="tabpanel">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <br>
            <table class="table m-b-20 without-border">
              <thead class="thead-default">
                <tr>
                  <th>S.No </th>
                  <th>Name</th>
                  <th>Role</th>
                  <th>Email Address</th>
                </tr>
              </thead>
              <tbody>
                <?php     $i = 0;
				          foreach($invitedTeamMembers as $TeamMembers){
							  $i++; ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $TeamMembers['first_name'].' '.$TeamMembers['last_name'] ?></td>
                              <td><?php echo $TeamMembers['member_type'] ?></td>
                              <td><?php echo $TeamMembers['email'] ?></td>
                            </tr>
                            <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="vendor2"  role="tabpanel">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <br>
            <table class="table m-b-20 without-border">
              <thead class="thead-default">
                <tr>
                  <th>S.No </th>
                  <th>Name</th>
                  <th>Role</th>
                  <th>Email Address</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 0;
						foreach($mspInvitedTeam as $mspTeam){
						   $i++;  ?>
                          <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $mspTeam['first_name'].' '.$mspTeam['last_name'] ?></td>
                            <td><?php echo $mspTeam['staff_type'] ?></td>
                            <td><?php echo $mspTeam['email'] ?></td>
                          </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div> </div>-->
<!-- simplify-tabs -->
</div>
</div>
<div> <span id="pre_total_estimate_cost" ></span>
  <input type="hidden" name="estimate_cost" id="estimate_cost" />
  <button type="submit" name="second_form" class="btn btn-success">Release Offer</button>
</div>
<br>
<br>
</div>
<!-- panel-body -->
</div>
</div>
</div>
</form>
</div>
<!-- col -->
</div>
<!-- row -->
<div class="seprater-bottom-100"></div>
</div>
<?php if(empty($noMarkupCat)){ ?>
<script>  
//calculating rates of per hour job.
  function calculate(type){
	  var Job_bill_rate = document.getElementById("bill_rate").value;
	  var Job_pay_rate = document.getElementById("pay_rate").value;
	  var Job_payment_type = document.getElementById("payment_type").value;
	  var Job_hours_per_week = document.getElementById("hours_per_week").value;
	  var firstDate = $(".dateField123").val();
	  var secondDate = $(".dateField1234").val();
	  var markup = document.getElementById("vendor_client_markup123").value;
	  //var bill_rate_hidden = document.getElementById("bill_rate_hidden").value;
	  //Usage    
	  var startDate = new Date(firstDate);
	  var endDate = new Date(secondDate);
	  var numOfDates = getBusinessDatesCount(startDate,endDate);
	  //alert(numOfDates);
	  var str = Job_hours_per_week;
	  var numberHours = str.split(" ",1);
	  var hoursPerDay = numberHours/5;
	  var str = Job_hours_per_week;
	  var numberHours = str.split(" ",1);
	  var estimatedSallary = Job_pay_rate * numOfDates * hoursPerDay;
	  if(type == 'billrate'){
		  var pay_rate = Job_bill_rate * 100/( 100 + parseFloat(markup));
		      //rounded value of pay rate  
	      var r_payrate = pay_rate.toFixed(3);
		  document.getElementById("pay_rate").value = r_payrate;
		  <!--client bill rate-->
		  document.getElementById("client_over_time").value = (parseFloat(Job_bill_rate) + parseFloat(Job_bill_rate * <?php echo $over_time ?>/100)).toFixed(3);
		  document.getElementById("client_double_time").value = (parseFloat(Job_bill_rate) + parseFloat(Job_bill_rate * <?php echo $double_time ?>/100)).toFixed(3);
		  document.getElementById("over_time").value = (parseFloat(r_payrate) + parseFloat(r_payrate * <?php echo $over_time ?>/100)).toFixed(3);      	  document.getElementById("double_time").value = (parseFloat(r_payrate) + parseFloat(r_payrate * <?php echo $double_time ?>/100)).toFixed(3);    
	  }else if(type == 'payrate'){
		   var bill_rate = parseFloat(Job_pay_rate) + (parseFloat(Job_pay_rate) * parseFloat(markup)/100);
		   document.getElementById("bill_rate").value = bill_rate.toFixed(3);
		   //document.getElementById("pre_total_estimate_cost").innerHTML = estimatedSallary +' USD for the Period of '+firstDate+' to '+secondDate;      
		   document.getElementById("estimate_cost").value = estimatedSallary;      
		   <!--client bill rate-->
		   document.getElementById("client_over_time").value = (parseFloat(bill_rate) + parseFloat(bill_rate * <?php echo $over_time ?>/100)).toFixed(3);
		   document.getElementById("client_double_time").value = (parseFloat(bill_rate) + parseFloat(bill_rate * <?php echo $double_time ?>/100)).toFixed(3);
		   document.getElementById("over_time").value = (parseFloat(Job_pay_rate) + parseFloat(Job_pay_rate * <?php echo $over_time ?>/100)).toFixed(3);
		   document.getElementById("double_time").value = (parseFloat(Job_pay_rate) + parseFloat(Job_pay_rate * <?php echo $double_time ?>/100)).toFixed(3); 
		   }  
		}
		calculate(); 
		
</script>
<?php }else{ ?>
<script>
function calculate(type){
	  var Job_bill_rate = document.getElementById("bill_rate").value;
	  var Job_pay_rate = document.getElementById("pay_rate").value;
	  var Job_payment_type = document.getElementById("payment_type").value;
	  var Job_hours_per_week = document.getElementById("hours_per_week").value;
	  var firstDate = $(".dateField123").val();
	  var secondDate = $(".dateField1234").val();
	  
	  var startDate = new Date(firstDate);
	  var endDate = new Date(secondDate);
	  var numOfDates = getBusinessDatesCount(startDate,endDate);
	  //alert(numOfDates);
	  var str = Job_hours_per_week;
	  var numberHours = str.split(" ",1);
	  var hoursPerDay = numberHours/5;
	  var str = Job_hours_per_week;
	  var numberHours = str.split(" ",1);
	  var estimatedSallary = Job_pay_rate * numOfDates * hoursPerDay;
	  if(type == 'billrate'){
		  document.getElementById("client_over_time").value = (parseFloat(Job_bill_rate) + parseFloat(Job_bill_rate * <?php echo $over_time ?>/100)).toFixed(3);
		  document.getElementById("client_double_time").value = (parseFloat(Job_bill_rate) + parseFloat(Job_bill_rate * <?php echo $double_time ?>/100)).toFixed(3);   
	  }else if(type == 'payrate'){
		   document.getElementById("over_time").value = (parseFloat(Job_pay_rate) + parseFloat(Job_pay_rate * <?php echo $over_time ?>/100)).toFixed(3);
		   document.getElementById("double_time").value = (parseFloat(Job_pay_rate) + parseFloat(Job_pay_rate * <?php echo $double_time ?>/100)).toFixed(3); 
		   }   
	}
</script>

<?php } ?>
<script>
function getBusinessDatesCount(startDate, endDate) { 
		   var count = 0;
		   var curDate = startDate;
		   while (curDate <= endDate) {
			    var dayOfWeek = curDate.getDay();
				  if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
				    count++;  curDate.setDate(curDate.getDate() + 1);    
			}    
	return count;  
	}
</script>