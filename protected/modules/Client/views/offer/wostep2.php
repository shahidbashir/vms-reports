<?php $this->pageTitle = 'WorkOrder Creation';
$clientRateData = ClientRate::model()->findByAttributes(array('client_id'=>$jobData->user_id));
/*echo '<pre>'; print_r($clientRateData); exit;*/
$over_time = 0; $half_time = 0; $double_time = 0;
if($clientRateData){
    $over_time = $clientRateData->over_time;
    $double_time = $clientRateData->double_time;
}
//$jobDuration = explode("-",$jobData->job_po_duration) ;

//getting highest markup value of vendors for specific category
$categoryID = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$jobData->cat_id));

//Administrative/Clerical Jobs category id 71
//client id 41

//commenting highest markup code now it will be specific vendor submission markup
/*$rateData = VendorClientMarkup::model()->findAllByAttributes(array('client_id'=>Yii::app()->user->id,'category_id'=>$categoryID->id),
    array(
        'order' => 'mark_up desc',
        'limit' => '1'
    ));

if($rateData){
    $mark_up = $rateData[0]->mark_up;
}else{
    $mark_up = 0;
}*/

$mark_up = $submissionData->makrup;




$jobLocations = Location::model()->findAllByAttributes(array('id'=>unserialize($jobData->location)));
$workflow = Worklflow::model()->findByPk($jobData->work_flow);
$oldData = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$workflow->id),array( 'order' => 'order_approve asc'));
$clientdata = Client::model()->findByPk($jobData->user_id);
//changing hiring manager list/ now all teammembers will be listed
//$hairingManager = Client::model()->findAllByAttributes(array('super_client_id'=>$jobData->user_id,'member_type'=>'Hiring Manager'));
$hairingManager = Client::model()->findAllByAttributes(array('super_client_id'=>$jobData->user_id));

$vendor = Vendor::model()->findByAttributes(array('id'=>$submissionData->vendor_id));

$offerdata = Offer::model()->findByAttributes(array('submission_id'=>$submissionData->id),array('order'=>'id desc'));
$location = Location::model()->findByPk($offerdata->approver_manager_location);


//before we were using the candidate pay rate in submission for calculations but now we have seprate offer pay rate
//$offerPayRate = $submissionData->candidate_pay_rate;
$offerPayRate = $offerdata->offer_pay_rate;

//picking up bill rate value for the No markup job category.
		
$settingData = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$jobData->cat_id));
$noMarkupCat = NoMarkupCate::model()->findByAttributes(array('category_id'=>$settingData->id));	


if(!empty($noMarkupCat->bill_rate)){
	$billrate = $noMarkupCat->bill_rate;
	}else{
		 $billrate = $offerPayRate + $offerPayRate*$mark_up/100;
		 }

//$billrate = $offerPayRate + $offerPayRate*$mark_up/100;

/*echo '<pre>';
print_r($offerdata);
exit;*/

?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
      <?php if(Yii::app()->user->hasFlash('success')):?>
          <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
  <form action="" method="POST" role="form">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Work Order Creation</h4>
      
      <p class="m-b-40">Assign Manager & other details to workorder</p>
      
      <table class="table table-condensed table-50 ">
        <tbody>
          <tr>
            <td>Candidate Name</td>
            <td><?php echo $candidateData->first_name.' '.$candidateData->last_name; ?></td>
          </tr>
          <tr>
            <td>Current Location</td>
            <td><?php echo $candidateData->current_location; ?></td>
          </tr>
          <tr>
            <td>Vendor Name</td>
            <td><?php echo $vendor->organization; ?></td>
          </tr>
        </tbody>
      </table>
      <br>
      <hr>
      <br>
      
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h4 class="m-b-20">Job Start Date</h4>
          <table class="table table-condensed table-50 ">
            <tbody>
              <tr>
                <td>Start Date<i class="required-field fa fa-asterisk"></i></td>
                <td><input type="text" name="job_start_date" value="<?php echo date('m/d/Y',strtotime($offerdata->job_start_date)); ?>"  onchange="calculate()" class="form-control datetimepickermodified dateField123"></td>
              </tr>
              <tr>
                <td>End Date<i class="required-field fa fa-asterisk"></i></td>
                <td><input type="text" name="job_end_date" value="<?php echo date('m/d/Y',strtotime($offerdata->job_end_date)); //echo date('m/d/Y',strtotime($jobData->job_po_duration_endDate)); ?>"  onchange="calculate()" class="form-control datetimepickermodified dateField123"></td>
              </tr>
              <tr>
                <td>Time Sheet Approving Manager<i class="required-field fa fa-asterisk"></i></td>
                <td><select name="approval_manager" id="input" class="form-control select2" required="required">
                <option value=""> Select Time Sheet Approving Manager</option>
                          <?php foreach($allTeamMembers as $allMembers){
                              if($allMembers['member_type']== NULL){
                                  $membertype = 'Super admin';
                              }else{
                                  $membertype = $allMembers['member_type'];
                              }
                              ?>
                          <option value="<?php echo $allMembers['id']; ?>"><?php echo $allMembers['first_name'].' '.$allMembers['last_name'].'('.$membertype.')'; ?></option>
                          <?php } ?>
                        </select></td>
              </tr>
              <?php 
				  $costcenter = CostCenter::model()->findAllByAttributes(array('client_id'=>$jobData->user_id));
				  $projects = Project::model()->findAllByAttributes(array('client_id'=>$jobData->user_id));
				  $timeSheetCodes = TimeSheetCode::model()->findAllByAttributes(array('client_id'=>$jobData->user_id));
			  ?>
              <?php /*?>
				  filtering of project base on this selected cost center is done but 
				  now just commenting the code on the demand of client
				  so for putting it back in working codition just uncomment ajax call from admin layout
				  <?php */?> 
              <tr>
                <td>Cost Center<i class="required-field fa fa-asterisk"></i></td>
                <td><select  name="cost_center[]" id="cost_center" multiple="" class="select2">
                          <?php if($costcenter){
                                foreach($costcenter as $costdata){
                                ?>
                          <option value="<?php echo $costdata['id']; ?>"><?php echo $costdata['cost_code']; ?></option>
                          <?php } } ?>
                        </select></td>
              </tr>
              <tr>
                <td>Timesheet Codes<i class="required-field fa fa-asterisk"></i></td>
                <td><select name="timesheet_code[]" id="timesheet_code" multiple="" class="select2">
                        <?php if($timeSheetCodes){
                                foreach($timeSheetCodes as $tsValue){
                                ?>
                          <option value="<?php echo $tsValue['id']; ?>"><?php echo $tsValue['time_code']; ?></option>
                          <?php } } ?>
                          </select></td>
              </tr>
              <tr>
                <td>Project<i class="required-field fa fa-asterisk"></i></td>
                <td><select name="project[]" id="project" multiple="" class="select2">
                        <?php if($projects){
                                foreach($projects as $projectsdata){
                                ?>
                          <option value="<?php echo $projectsdata['id']; ?>"><?php echo $projectsdata['project_name']; ?></option>
                          <?php } } ?>
                          </select></td>
              </tr>
              <tr>
                <td>Location</td>
                <td><input type="text" readonly="readonly" value="<?php echo $location->name ?>" class="form-control"  /></td>
              </tr>
            </tbody>
          </table>
          <br>
          <hr>
          <br>
          <h4 class="m-b-20">Rates</h4>
          <table class="table table-condensed table-50 ">
                        
            <input type="hidden" readonly="readonly" name="payment_type" id="payment_type" class="form-control" value="<?php echo $jobData->payment_type; ?>" required="required">
            <input type="hidden" readonly="readonly" name="hours_per_week" id="hours_per_week" class="form-control" value="<?php echo $jobData->hours_per_week; ?>" required="required">
            <input type="hidden" readonly="readonly" name="shift" id="shift" class="form-control" value="<?php echo $jobData->shift; ?>" required="required">
            <!--on ajax call find the vendor highest markup and fill its value in this field-->
            <input type="hidden" name="Job[vendor_client_markup]" id="vendor_client_markup123" class="form-control" value="<?php echo $mark_up; ?>">
            <input type="hidden" id="calculated_value" name="calculated_value" value="" class="form-control"  />
            
            <tbody>
                    <tr>
                      <td>Bill Rate<i class="required-field fa fa-asterisk"></i></td>
                      <td><input type="text" name="bill_rate" id="bill_rate" class="form-control" value="<?php echo $billrate; ?>" required="required" onchange='calculate("billrate")'></td>
                    </tr>
                    
                    <tr>
                      <td>Pay Rate<i class="required-field fa fa-asterisk"></i></td>
                      <td><input type="text" name="pay_rate" id="pay_rate" class="form-control" value="<?php echo $offerPayRate; ?>" required="required" onchange="calculate('payrate')"></td>
                    </tr>
                    
                    <?php if(!empty($noMarkupCat)){ ?>
                   <tr>
                     <td>Vendor Bill Rate</td>
                     <td><input type="text" readonly="readonly" class="form-control" value="<?php echo $submissionData->vendor_bill_rate; ?>" ></td>
                   </tr>
                   <?php } ?>
                    
                    <tr>
                        <td>Over time Rate for Client</td>
                        <td><input type="text" id="client_over_time" readonly="readonly" name="client_over_time" class="form-control" value="<?php echo $billrate + $billrate*$over_time/100; ?>" required="required"></td>
                    </tr>
                    <tr>
                        <td>Double Time Rate for Client</td>
                        <td><input type="text" id="client_double_time" readonly="readonly" name="client_double_time" class="form-control" value="<?php echo $billrate + $billrate*$double_time/100; ?>" required="required"></td>
                    </tr>
                    
                    <tr>
                        <td>Over Time Rate for Candidate</td>
                        <td><input type="text" id="over_time" readonly="readonly" name="over_time" class="form-control" value="<?php echo $offerPayRate + $offerPayRate*$over_time/100; ?>" required="required"></td>
                    </tr>
                    <tr>
                        <td>Double Time Rate for Candidate</td>
                        <td><input type="text" id="double_time" readonly="readonly" name="double_time" class="form-control" value="<?php echo $offerPayRate + $offerPayRate*$double_time/100; ?>" required="required"></td>
                    </tr>
                    <?php 
					if($vendor){
						$vendorName = $vendor->organization;
					}else{
						  $vendorName = '';
						 }?>
                    <tr>
                        <td>Vendor Markup ( <?php echo $vendorName;  ?> )</td>
                        <td><input type="text" readonly="readonly" value="<?php echo $mark_up ?>%" class="form-control"  /></td>
                    </tr>
                    </tbody>
            
          </table>
          <br>
          <hr>
          <br>
          <h4 class="m-b-20">Work Flow Approval</h4>
          <table class="table table-condensed table-50 ">
            <tbody>
              <tr>
                <td colspan="2">Job Created By : <?php echo $clientdata->first_name.' '.$clientdata->last_name.' on '.$jobData->date_created ?><!--10th May 2016--></td>
              </tr>
              <tr>
                <td>Hiring Manager<i class="required-field fa fa-asterisk"></i></td>
                <td><select name="hiring_manager" id="input" class="form-control select2" required="required">
                          <option value=""> Select Hiring Manager</option>
                          <?php /*?>
						  //this code was running for hiring manager query now all teammember could be hiring manager
						  <?php if($hairingManager)
                            foreach($hairingManager as $managerData){ ?>
                          <option value="<?php echo $managerData['id']; ?>"><?php echo $managerData['first_name'].' '.$managerData['last_name']; ?></option>
                          <?php } ?><?php */?>
                          <?php if($allTeamMembers)
                            foreach($allTeamMembers as $managerData){ ?>
                          <option value="<?php echo $managerData['id']; ?>"><?php echo $managerData['first_name'].' '.$managerData['last_name']; ?></option>
                          <?php } ?>
                        </select></td>
              </tr>
              <tr>
                <td>Work Flow Name</td>
                <td><?php echo $workflow->flow_name; ?></td>
              </tr>
            </tbody>
          </table>
          <br>
          <hr>
          <br>
          <table class="table m-b-20 without-border">
            <thead class="thead-default">
              <tr>
                <th>Person Name</th>
                <th>Department</th>
                <th>Approval Date</th>
              </tr>
            </thead>
            <tbody>
            <?php
				if($oldData){ foreach($oldData as $oldData){
				if($oldData['client_id'] != 0){
				$clientDatav = Client::model()->findByPk($oldData['client_id']);
				$jobWorkflow = JobWorkflow::model()->findByAttributes(array('job_id'=>$jobData->id,'workflow_id'=>$workflow->id));
			?>
              <tr>
                <td><?php echo $clientDatav->first_name.' '.$clientDatav->last_name; ?></td>
                <td><?php echo $oldData['department_id']; ?></td>
                <td><?php echo date('jS M Y',strtotime($jobWorkflow->status_time)); ?></td>
              </tr>
            <?php } } }?>
            </tbody>
          </table>
          <br>
          <hr>
          <br>
          <div class="row no-margin">
            <div class="col-md-12">
              <h4>Notes &amp; Comments</h4>
              <div class="simplify-tabs m-b-42">
                <div role="tabpanel"> 
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs no-hover" role="tablist">
                    <li class="nav-item active"> <a class="nav-link " href="#internal" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"></i> Internal</a> </li>
                    <li class="nav-item"> <a class="nav-link " href="#msp" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"></i> MSP</a> </li>
                    <li class="nav-item"> <a class="nav-link " href="#vendor" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"></i> Vendor</a> </li>
                  </ul>
                  <div class="tab-content ">
                    <div class="tab-pane active" id="internal" role="tabpanel">
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                              <label for="">Note</label>
                              <textarea name="ref_notes" id="input" class="form-control" rows="5" ></textarea>
                            </div>
                            <br>
                            <br>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="msp" role="tabpanel">
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                              <label for="">Note</label>
                              <textarea name="ref_notes_msp" id="input" class="form-control" rows="5" ></textarea>
                            </div>
                            <br>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="vendor" role="tabpanel">
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                              <label for="">Note</label>
                              <textarea name="ref_notes_vendor" id="input" class="form-control" rows="5" ></textarea>
                            </div>
                            <br>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- tabpanel --> 
              </div>
              <!-- simplify-tabs -->
              
              <hr>
              <br>
              <!--<h4>Teams</h4>
              <div class="simplify-tabs m-b-42">
                <div role="tabpanel"> 

                  <ul class="nav nav-tabs no-hover" role="tablist">
                    <li class="nav-item active"> <a class="nav-link " href="#internal2" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"></i> Client</a> </li>
                    <li class="nav-item"> <a class="nav-link " href="#vendor2" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"></i> Vendor</a> </li>
                  </ul>
                  <div class="tab-content ">
                    <div class="tab-pane active" id="internal2" role="tabpanel">
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <br>
                          <table class="table m-b-20 without-border">
                            <thead class="thead-default">
                              <tr>
                                <th>S.No </th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Email Address</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
								$i = 0;
								foreach($invitedTeamMembers as $TeamMembers){
									$i++;
									?>
								<tr>
								  <td><?php echo $i; ?></td>
								  <td><?php echo $TeamMembers['first_name'].' '.$TeamMembers['last_name'] ?></td>
								  <td><?php echo $TeamMembers['member_type'] ?></td>
								  <td><?php echo $TeamMembers['email'] ?></td>
								</tr>
								<?php } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="vendor2" role="tabpanel">
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <br>
                          <table class="table m-b-20 without-border">
                            <thead class="thead-default">
                              <tr>
                                <th>S.No </th>
                                <th>Name</th>
                                <th>Email Address</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                  <td>1</td>
                                  <td><?php echo $vendor->organization.'('.$vendor->first_name.' '.$vendor->last_name.')';?></td>
                                  <td><?php echo $vendor->email ?></td>
                                </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
               
              </div>-->
              <!-- simplify-tabs --> 
            </div>
          </div>
          <!-- row -->
          
          <p>
            <button type="submit" name="second_form" class="btn btn-success">Create Work Order</button>
          </p>
        </div>
      </div>
      <!-- row -->
      
      <div class="seprater-bottom-100"></div>
      <div class="seprater-bottom-100"></div>
    </div>
    <!-- col -->
  </form> 
    
  </div>
  <!-- row --> 
  
</div>
<script>
    //calculating rates of per hour job.
    function calculate(type){

        var Job_bill_rate = document.getElementById("bill_rate").value;
        var Job_pay_rate = document.getElementById("pay_rate").value;
        var Job_payment_type = document.getElementById("payment_type").value;
        var Job_hours_per_week = document.getElementById("hours_per_week").value;
        var firstDate = $(".dateField123").val();
        var secondDate = $(".dateField1234").val();
        var markup = document.getElementById("vendor_client_markup123").value;
    
        //var bill_rate_hidden = document.getElementById("bill_rate_hidden").value;

        //Usage
        var startDate = new Date(firstDate);
        var endDate = new Date(secondDate);
        var numOfDates = getBusinessDatesCount(startDate,endDate);

        //alert(numOfDates);

        var str = Job_hours_per_week;
        var numberHours = str.split(" ",1);

        var hoursPerDay = numberHours/5;

        var str = Job_hours_per_week;
        var numberHours = str.split(" ",1);
        var estimatedSallary = Job_pay_rate * numOfDates * hoursPerDay;

        if(type == 'billrate'){
		<!--client bill rate-->
		document.getElementById("client_over_time").value = (parseFloat(Job_bill_rate) + parseFloat(Job_bill_rate * <?php echo $over_time ?>/100)).toFixed(3);
		document.getElementById("client_double_time").value = (parseFloat(Job_bill_rate) + parseFloat(Job_bill_rate * <?php echo $double_time ?>/100)).toFixed(3);
		
		<?php if(empty($noMarkupCat)){ ?>
		var pay_rate = Job_bill_rate * 100/( 100 + parseFloat(markup));
		//rounded value of pay rate
		var r_payrate = pay_rate.toFixed(3);
		document.getElementById("pay_rate").value = parseFloat(r_payrate);
		document.getElementById("over_time").value = (parseFloat(r_payrate) + parseFloat(r_payrate * <?php echo $over_time ?>/100)).toFixed(3);
		document.getElementById("double_time").value = (parseFloat(r_payrate) + parseFloat(r_payrate * <?php echo $double_time ?>/100)).toFixed(3);
		<?php } ?>
			  
    }else if(type == 'payrate'){
			
			<?php if(empty($noMarkupCat)){ ?>
            var bill_rate = parseFloat(Job_pay_rate) + (parseFloat(Job_pay_rate) * parseFloat(markup)/100);
            document.getElementById("bill_rate").value = bill_rate.toFixed(3);
      
      		<!--client bill rate-->
      		document.getElementById("client_over_time").value = (parseFloat(bill_rate) + parseFloat(bill_rate * <?php echo $over_time ?>/100)).toFixed(3);
            document.getElementById("client_double_time").value = (parseFloat(bill_rate) + parseFloat(bill_rate * <?php echo $double_time ?>/100)).toFixed(3);
			<?php } ?>
			
            document.getElementById("over_time").value = (parseFloat(Job_pay_rate) + parseFloat(Job_pay_rate * <?php echo $over_time ?>/100)).toFixed(3);
            document.getElementById("double_time").value = (parseFloat(Job_pay_rate) + parseFloat(Job_pay_rate * <?php echo $double_time ?>/100)).toFixed(3);

        }


    }

    calculate();
</script>
<script>
function getBusinessDatesCount(startDate, endDate) {
        var count = 0;
        var curDate = startDate;
        while (curDate <= endDate) {
            var dayOfWeek = curDate.getDay();
            if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
                count++;
            curDate.setDate(curDate.getDate() + 1);
        }
        return count;
    }
</script>