<?php $this->renderPartial('overview_menu',array('offerModel'=>$workOrderModel));?>
  <div class="tab-content">
       <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'job-form',
            'enableAjaxValidation'=>false,
             'htmlOptions' => array('enctype' => 'multipart/form-data'),
          ));

     ?>
          <?php if(Yii::app()->user->hasFlash('success')):
			  echo Yii::app()->user->getFlash('success');
		endif; ?>
         
           <div class="row">
             <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="form-group">
                 <label for="">Start Date</label>
                 <?php echo $form->textField($offerModel,'job_start_date',array('class'=>'form-control datetimepicker')); ?> 			
				 <?php echo $form->error($offerModel,'job_start_date'); ?>
               </div>
             </div> <!-- col -->

             <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="form-group">
                 <label for="">Offical Email Address</label>
                  <?php echo $form->textField($candidateModel,'emp_official_email',array('class'=>'form-control')); ?> <?php echo $form->error($candidateModel,'emp_official_email'); ?>
               </div>
             </div> <!-- col -->


           </div> <!-- row -->

           <h4 class=" m-b-20">Candidate Account Information </h4>

           <div class="row">
             <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="form-group">
                 <label for="">Candidate User ID</label>
                  <?php echo $form->textField($candidateModel,'candidate_ID',array('class'=>'form-control')); ?> <?php echo $form->error($candidateModel,'candidate_ID'); ?>
               </div>
             </div> <!-- col --> 
             <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="form-group">
                 <label for="">Candidate Password</label>
                  <?php echo $form->passwordField($candidateModel,'emp_password',array('class'=>'form-control')); ?> <?php echo $form->error($candidateModel,'emp_password'); ?>
               </div>
             </div> <!-- col -->
           </div> <!-- row -->

             <div class="row">
             <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="form-group">
                 <label for="">Upload Photo</label>
                  <?php echo $form->fileField($candidateModel, 'profile_mg',array('class'=>'form-control'));?>
               </div>
             </div> <!-- col --> 
             <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="form-group">
                 <label for="">Mobile Number</label>
                 <?php echo $form->textField($candidateModel,'mobile',array('class'=>'form-control')); ?> <?php echo $form->error($candidateModel,'mobile'); ?>
               </div>
             </div> <!-- col -->
           </div> <!-- row -->

           <div class="row">
             <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="form-group">
                 <label for="">Candidate Personal Email Address</label>
                 <?php echo $form->textField($candidateModel,'email',array('class'=>'form-control')); ?> <?php echo $form->error($candidateModel,'email'); ?>
               </div>
             </div> <!-- col --> 
             
           </div> <!-- row -->

         
           
         
           <button type="submit" class="btn btn-success">On Boarded</button>
         <?php $this->endWidget(); ?>