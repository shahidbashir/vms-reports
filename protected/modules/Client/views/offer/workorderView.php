<?php 
	$approvalManager = Client::model()->findByPk($workOrderModel->approval_manager);

	$workOrderStatus = UtilityManager::workorderStatus();

	if($workOrderModel->workorder_status == 0){
		$className ="tag label-pending-aproval";
	}else if($workOrderModel->workorder_status == 1){
		$className ="tag label-new-request";
	}else{
		$className ="tag label-rejected";
	}
?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">
	  	<?php echo $this->pageTitle =  'Work Order ('.$workOrderModel->id.')'; ?>
      	<a href="<?php echo $this->createAbsoluteUrl('offer/workorderList'); ?>" class="btn btn-sm btn-default pull-right"> Back to WorkOrder Listing</a>
      </h4>
      <p class="m-b-30">Complete information of Workorder.</p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
		<?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <div class="row">

      	<?php if($workOrderModel->workorder_status == 0){ ?>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> 
        <form action="" method="POST" role="form">
            <button type="submit" name="accepted" class="btn btn-sm btn-success">Work Order Accept</button>
            <a data-toggle="modal" href='#modal-id' class="btn btn-sm btn-default-2">Work Order Rejected</a> 
        </form>
          
       
        </div>
        
        <?php }else{ ?>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> 
            
        </div>
        
        <?php } ?>
        
        
        
        
        <?php 
		//dropdown of workorder status 
		//when will have need then will uncomment it.
		/*$form=$this->beginWidget('CActiveForm', array(
				 'id'=>'vendor-form',
				 'enableAjaxValidation'=>false,
			 )); ?>
			<div class="row">
			  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<div class="form-group">
				  <label for="">WorkOrder Status</label>
				  <?php 

	  $list = UtilityManager::workorderStatus();
				
	  echo $form->dropDownList($workOrderModel,'workorder_status', $list,
			  array('empty' => 'Change Status','class'=>'form-control','required'=>'required','options' => array($offerModel->workorder_status=>array('selected'=>true)))); ?>
				  <?php echo $form->error($workOrderModel,'status'); ?> </div>
			  </div>
			  <!-- col -->
			  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<label>&nbsp;</label>
				<p style="margin-top: 5px;">
				  <button type="submit" class="btn  btn-primary">Update</button>
				  <a href="#modal-id" data-toggle="modal" class="btn btn-danger">Reject WorkOrder</a>
				</p>
			  </div>
			</div>
			<!-- row -->
			
	<?php $this->endWidget();*/ ?>
       
        
        
<?php $Candidates = Candidates::model()->findByPk($workOrderModel->candidate_id); ?>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"> 
        	<span class="<?php echo $className; ?>"><?php echo $workOrderStatus[$workOrderModel->workorder_status]; ?></span> 
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
          <div class="simplify-tabs blue-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <li class="nav-item"> <a class="nav-link active"  href="#rates" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Information</a> </li>
              </ul>
              <div class="tab-content   p-r-0 p-l-0">
                <div class="tab-pane active" id="rates"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table class="table tab-table">
                        <tbody>
                        <tr>
                          <td>Name</td>
                          <td><?php if($Candidates){ echo $Candidates->first_name.' '.$Candidates->last_name; } ?></td>
                        </tr>
                          <tr>
                            <td> Offer ID : </td>
                            <td><a href=""><?php echo $offerData->id;?></a></td>
                          </tr>
                          <tr>
                            <td> WorkOrder ID : </td>
                            <td><a href=""><?php echo $workOrderModel->id;?></a></td>
                          </tr>
                          <tr>
                            <td> Offer Accept Date : </td>
                            <td> <?php echo date('F d,Y H:i:s',strtotime($offerData->offer_accept_date)); ?> </td>
                          </tr>
                          <?php if($workOrderModel->workorder_status==1){ ?> 
                          <tr>
                            <td> WorkOrder Accept Date : </td>
                            <td> <?php echo date('F d,Y H:i:s',strtotime($workOrderModel->accept_date)); ?> </td>
                          </tr>
                          <?php } ?>
                          <tr>
                            <th> Pay Rate ( For Candidate ) </th>
                            <th> </th>
                          </tr>
                          <tr>
                            <td>Pay Rate</td>
                            <td>$ <?php echo $workOrderModel->wo_pay_rate;?></td>
                          </tr>
                          <tr>
                            <td>Over Time</td>
                            <td>$ <?php echo $workOrderModel->wo_over_time;?></td>
                          </tr>
                          <tr>
                            <td>Double Time</td>
                            <td>$ <?php echo $workOrderModel->wo_double_time;?></td>
                          </tr>
                          <tr>
                            <th> Bill Rate ( For Client ) </th>
                          </tr>
                          <tr>
                            <td>Bill Rate</td>
                            <td>$ <?php echo $workOrderModel->wo_bill_rate; ?></td>
                          </tr>
                          <tr>
                            <td>Over Time</td>
                            <td>$ <?php echo $workOrderModel->wo_client_over_time;?></td>
                          </tr>
                          <tr>
                            <td>Double Time</td>
                            <td>$ <?php echo $workOrderModel->wo_client_double_time;?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
          </div>
          <!-- simplify-tabs --> 
          
        </div>
        <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
          <div class="simplify-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
              <?php if($workOrderModel->workorder_status != 1){ ?>
                <li class="nav-item"> <a class="nav-link"  href="#rejected" role="tab" data-toggle="tab" data-placement="bottom" title="Rejected"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Rejected</a> </li>
              <?php } ?>
                <li class="nav-item"> <a class="nav-link active"  href="#skills" role="tab" data-toggle="tab" data-placement="bottom" title="Skills"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/workorder@512px.svg"></i> Work Order</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#resume" role="tab" data-toggle="tab" data-placement="bottom" title="Resume"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Notes </a> </li>
                <li class="nav-item"> <a class="nav-link " <?php echo $workOrderModel->workorder_status==1?'':'style="pointer-events: none;"'; ?>  href="#comments" role="tab" data-toggle="tab" data-placement="bottom" title="Comments"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> On-Bording</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#background-verification" role="tab" data-toggle="tab" data-placement="bottom" title="Comments"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Background Verification</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#exhibit" role="tab" data-toggle="tab" data-placement="bottom" title="Comments"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Exhibit</a> </li>
              </ul>
              <div class="tab-content ">
                <div class="tab-pane" id="rejected"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <?php if($workOrderModel->reason_rejection_workorder){ ?>
                      <div class="alert-box m-t-10">
                        <p class="bold m-b-10">Reason for Rejection</p>
                        <p class="m-b-10"><?php echo $workOrderModel->reason_rejection_workorder; ?></p>
                        <p class="bold m-b-10">Notes:</p>
                        <p class="m-b-10"><?php echo $workOrderModel->notes_workorder; ?></p>
                        <p><strong>Posted By :</strong> Person Name at 10:30 PM on 10th May 2015</p>
                      </div>
					  <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane active m--31" id="skills"  role="tabpanel">
                  <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
                    <thead>
                      <tr>
                        <th>Start Date</th>
                        <th> End Date</th>
                        <th>Time Sheet Approval</th>
                        <th style="text-align: center;">Location of Work</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><?php echo date('F d,Y',strtotime($workOrderModel->wo_start_date)); ?></td>
                        <td> <?php echo date('F d,Y',strtotime($workOrderModel->wo_end_date)); ?> </td>
                        <td> <?php echo $approvalManager->first_name.' '.$approvalManager->last_name; ?> </td>
                        <td style="text-align: center;"> <?php 
						  $location = Location::model()->findByPk($offerData->approver_manager_location);
						  echo $location->name ;?> </td>
                      </tr>
                    </tbody>
                    <thead>
                      <tr>
                        <th colspan="4">Cost Center Code</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach($costCenters as $costCenters){ ?>
                      <tr>
                        <td colspan="4"><?php echo $costCenters->cost_code; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                    <thead>
                      <tr>
                        <th colspan="4">Projects</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($projects as $projects){ ?>
                      <tr>
                        <td colspan="4"><?php echo $projects->project_name; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                    <thead>
                      <tr>
                        <th colspan="4">Time Sheet</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<?php foreach($timeSheetCodes as $timeSheetCodes){ ?>
                      <tr>
                        <td  colspan="4"><?php echo $timeSheetCodes->time_code; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                
                <div class="tab-pane m--31" id="background-verification"  role="tabpanel">
                  <div class="row">
                   <?php 
				   if($workOrderModel->verification_status==1){
				   		$this->renderPartial('/contract/contract_background',array('client'=>$client,'workorder'=>$workOrderModel));
				   } ?>
                  </div>
                </div>
                <!--  tab-pane -->
                
                <div class="tab-pane m--31" id="exhibit"  role="tabpanel">
                	<?php if($workOrderModel->exhibit_status==1){ 
							$this->renderPartial('/contract/contract_exibit',array('workorder'=>$workOrderModel));
					} ?>
                </div>
                
                <div class="tab-pane" id="resume"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <p class="bold m-b-10">Internal Note</p>
                      <div class="well">
                        <p><?php echo $workOrderModel->wo_internal_notes ?></p>
                      </div>
                      <p class="bold m-b-10">MSP Note</p>
                      <div class="well">
                        <p><?php echo $workOrderModel->wo_msp_notes ?></p>
                      </div>
                      <p class="bold m-b-10">Vendor Note</p>
                      <div class="well">
                        <p><?php echo $workOrderModel->wo_vendor_notes ?></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="comments"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <?php $this->renderPartial('on_boarding',array('workOrderModel'=>$workOrderModel,'candidateModel'=>$candidateModel,'submissionModel'=>$submissionModel)); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
            
          </div>
          <!-- simplify-tabs --> 
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<div class="modal fade" id="modal-id">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Reject WorkOrder</h4>
      </div>
      <form action="" method="POST" role="form">
      <div class="modal-body">


        <div class="form-group">
          <label for="">Reason for Rejection</label>
          <?php $setting = Setting::model()->findAll(array('condition'=>'category_id=51')); ?>
          <select name="reason_for_rejection" id="input" class="form-control" value="" required  title="">
         		<option value="">Select Reason</option>
				<?php foreach($setting as $value){ echo "<option value='".$value->title."'>".$value->title."</option>";} ?>
          </select>
        </div>
        
        
        <div class="form-group">
          <label for="">Note</label>
         <textarea name="notes" id="input" class="form-control" rows="3" required></textarea>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="submit" name="rejected" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>