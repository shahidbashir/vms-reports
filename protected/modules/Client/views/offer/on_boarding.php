<div class="tab-pane" id="comments"  role="tabpanel">
  <div class="row">
    <?php $job = Job::model()->findByPk($workOrderModel->job_id);
        $contract = Contract::model()->findByAttributes(array('workorder_id'=>$workOrderModel->id));
    ?>
  <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'job-form',
			'action' => Yii::app()->createUrl('Client/offer/onBoarding',array('id'=>$_GET['id'])),
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
          ));
	//$vendor = Vendor::model()->findByPk($workOrderModel->vendor_id);
	$vendor = Vendor::model()->findByPk($candidateModel->emp_msp_account_mngr);
     ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="h4-small m-t-20 m-b-20">Candidate Information</h4>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Candidate Name</label>
            <input type="text" value="<?php echo $candidateModel->first_name.' '.$candidateModel->last_name; ?>" readonly="readonly" class="form-control" id="" placeholder="">
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Personal Email Address</label>
            <input type="text" value="<?php echo $candidateModel->email; ?>" name="Candidates[email]" readonly="readonly" class="form-control" id="" placeholder="">
          </div>
        </div>
      </div>
      <!-- row -->
      
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Upload Profile Pic</label>
            <input type="file" class="form-control" id="" placeholder=""  disabled="disabled">
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Account Manager ( Dropdown )</label>
            <select name="" id="input " class="form-control select2" disabled="disabled">
              <option value=""><?php echo $vendor->first_name.' '.$vendor->last_name; ?></option>
            </select>
          </div>
        </div>
      </div>
      <!-- row -->
      
      <h4 class="h4-small m-t-20 m-b-20">On-Boarding Details</h4>

      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Job Title</label>
            <input type="text" name="job_title" class="form-control" id="" placeholder="" value="<?php echo $job->title ?>"  readonly="readonly">
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <?php $modelSetting = Setting::model()->findAll(array('condition'=>'category_id=4','order'=>'id ASC')); ?>
            <label for="">Department</label>
            <select name="candidate_dept" id="input " class="form-control select2">
              <?php foreach ($modelSetting as $value){ ?>
              <option value="<?php echo $value->id; ?>" <?php if(isset($contract) && $contract->candidate_dept==$value->id){ echo "Selected"; } ?>><?php echo $value->title; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="form-group">
          <label for="">Supervisor Name</label>
          <input type="text" name="supervisor_name" class="form-control" id="" placeholder="" value="<?php if(isset($contract)){ echo $contract->supervisor_name; } ?>" required="required">
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="form-group">
          <label for="">Supervisor Email</label>
          <input type="text" name="supervisor_email" class="form-control" id="" placeholder="" value="<?php if(isset($contract)){ echo $contract->supervisor_email; } ?>" required="required">
        </div>
      </div>
</div>

      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Supervisor Phone</label>
            <input type="text" name="supervisor_phone" class="form-control" id="" placeholder="" value="<?php if(isset($contract)){ echo $contract->supervisor_phone; } ?>">
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Official Email Address</label>
            <?php echo $form->textField($candidateModel,'emp_official_email',array('class'=>'form-control' , 'required' => 'required')); ?>
            <?php echo $form->error($candidateModel,'emp_official_email'); ?>
            <?php /*?><input type="text" value="<?php echo $candidateModel->emp_official_email; ?>" class="form-control" id="" placeholder=""><?php */?>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> </div>
      </div>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Start Date</label>
            <?php 			
			if($workOrderModel->onboard_changed_start_date=='0000-00-00'){
				$onboardStartDate = date('m/d/Y',strtotime($workOrderModel->wo_start_date));
				}else{
					 $onboardStartDate = date('m/d/Y',strtotime($workOrderModel->onboard_changed_start_date));
					 }
					 
			if($workOrderModel->onboard_changed_end_date=='0000-00-00'){
				$onboardEndDate = date('m/d/Y',strtotime($workOrderModel->wo_end_date));
				}else{
					 $onboardEndDate = date('m/d/Y',strtotime($workOrderModel->onboard_changed_end_date));
					 }
			
			?>
            <input type="text" name="onboard_changed_start_date" value="<?php echo $onboardStartDate; ?>" class="form-control singledatepicker" id="" placeholder="">
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">End Date</label>
            <input type="text" name="onboard_changed_end_date" value="<?php echo $onboardEndDate; ?>" id="input" class="form-control singledatepicker" required="required" >
          </div>
        </div>
      </div>
      <!-- row -->
      

      <!-- row -->
      
      <h4 class="h4-small m-t-20 m-b-20">Candidate Portal Details</h4>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label for="">Candidate Customer ID</label>
            <?php echo $form->textField($candidateModel,'candidate_ID',array('class'=>'form-control')); ?> 
			<?php echo $form->error($candidateModel,'candidate_ID'); ?>
            <?php /*?><input type="text" value="<?php echo $candidateModel->candidate_ID; ?>" class="form-control" id="" placeholder=""><?php */?>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label style="display: block;" for="">Candidate Password <a href="#" id="showpass" class="pull-right"></a></label>
            <input type="password" value="<?php echo $candidateModel->emp_password; ?>" name="Candidates[emp_password]" id="pwd" class="form-control" required="required" >
          </div>
        </div>
      </div>
      <!-- row -->
      
      <div class="form-group m-t-20">
      <?php if($workOrderModel->on_board_status==0){ ?>
      	<button type="submit" class="btn btn-success" >On Boarded</button>
      <?php } ?>
      </div>
    </div>
    
    <?php $this->endWidget(); ?>
  </div>
</div>
