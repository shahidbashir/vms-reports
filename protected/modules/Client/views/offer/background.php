<?php $this->renderPartial('overview_menu',array('offerModel'=>$offerModel));?>
  <div class="tab-content">
     <?php /*?><div class="row">
          <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'job-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
          ));

      $cateSetting = Setting::model()->findAll(array('condition'=>'category_id=41')); 
    ?>
        
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            
                <div class="form-group">
                  <label for="">Back ground Verification</label>
                   <select name="OfferBackground[background_title]" class="form-control" required="required">
                <option value="">Select Background Verification</option>
              <?php foreach($cateSetting as $catValue){
                 echo '<option value="'.$catValue->title.'">'.$catValue->title.'</option>'; 
               }?>
          </select>
                  
                </div>
                    
          </div>

          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <label>Upload</label>
                <div class="form-group">
   <?php echo $form->fileField($backgroundModel, 'file',array('class'=>'form-control'));?>
   </div>
   </div>

          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <div class="form-group">
                <label for="">&nbsp; </label>
                <p style="padding-top: 10px; ">
                <button type="submit" class="btn btn-success">Update</button>
              </div>
          </div>
           <?php $this->endWidget(); ?>
        </div> <?php */?>
     <!-- row -->

        <div class="row">

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              
              <div class="table-default">
              <?php if($backgroundModelList) {?>
                
                
                
                <div class="table-responsive">

                   <table class="table table-striped table-hover">
                    <thead>
                      <th>S.No</th>
                      <th>Date of Upload</th>
                      <th>Background Verification</th>
                      <th>Files</th>
                      
                      <th style="text-align: right; padding-right: 30px;" >
                        Actions
                      </th>

                    </thead>
                      <tbody>
                      <?php $i=1;foreach($backgroundModelList as $value){ ?>
                        <tr>
                          <td><?php echo $i;$i++;?></td>
                          <td><?php echo date('m/d/Y',strtotime($value->date_created));?></td>

                          <td><?php echo $value->background_title;?></td>
                          <td>
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('offer/downloadBackground',array('id'=>$value->id)); ?>"><?php echo $value->file;?></a>
                          </td>
                          
                          <td style="text-align: right; padding-right: 30px;" >

                          <a href="<?php echo Yii::app()->createAbsoluteUrl('offer/deleteBackground',array('id'=>$value->id)); ?>" data-placement="top" data-toggle="tooltip" class="delete" data-original-title="Delete" onclick="return confirm('Are you sure you want to delete this Background Verification?');"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                         <?php } ?>
                    </tbody>
                  </table>

                </div>
                <?php } ?>

              </div>

          </div> <!-- col -->
          
        </div> <!-- row -->
        <br>

        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 m-t-20 m-b-20">
          <?php $form=$this->beginWidget('CActiveForm'); 
		  if($offerModel->background_status == 1){ ?>
          <a href="#" class="btn btn-success" disabled >Background Verification Completed</a>
          <?php }else{ ?>
           <button type="submit" name="completed" class="btn btn-success">Background Verification Completed</button>
            <?php } $this->endWidget(); ?>
          </div>
         </div>

      </div> <!-- bg-vefication -->