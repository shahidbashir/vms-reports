<?php $this->pageTitle =  'Work Order'; 
/*echo '<pre>';
print_r($offerModel);
exit;*/

?>

<div class="main-content">
<div class="row">
<div class="row m-0">
<div class="col-md-12">
<div class="panel panel-default panel-table">
<div class="panel-body">
<div class="row m-0">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="text-right m-t-10 " > </div>
  </div>
  <!-- col --> 
</div>
<!-- row -->

<h4 class="" style="margin:20px 20px 30px;">Work Order ID : <?php echo $offerModel->id;?> <a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/job/offer'); ?>" class="btn btn-default pull-right" style="margin-right: 10px;">Back to Workorder List</a>
  <?php /*?><?php if($offerModel->workorder_status==0 && $offerModel->status==1){ ?>
<a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/offer/view',array('id'=>$offerModel->id,'workorder-status'=>2)); ?>" class="btn btn-danger pull-right" data-toggle="modal"  style="margin-right: 10px;">WORKORDER  REJECTED </a>
rejected
<a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/offer/view',array('id'=>$offerModel->id,'workorder-status'=>1)); ?>" class="btn btn-success pull-right" style="margin-right: 10px;">WORKORDER ACCEPT </a>

<?php }else{ ?>
<a href="#" disabled class="btn btn-danger pull-right" style="margin-right: 10px;" >WORKORDER  REJECTED</a>
<a href="#" disabled class="btn btn-success pull-right" style="margin-right: 10px;" >WORKORDER ACCEPT</a>
<?php } ?><?php */?>
</h4>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  
  <?php if($offerModel->workorder_status == 2){ ?>
  <p class="bold m-b-10">Offer Rejection</p>
  <div class="well well-yellow">
    <p><strong>Reason:</strong> <?php echo $offerModel->reason_rejection_workorder; ?></p>
    <p><strong>Note :</strong> <?php echo $offerModel->notes_workorder; ?></p>
  </div>
  <?php }  ?>
</div>
<?php $action = $this->action->id;?>
<div class="default-tabs">
<div role="tabpanel">
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li class="<?php echo $action=='view'?'active':''?>"> <a href="<?php echo $this->createUrl('offer/view',array('id'=>$offerModel->id));?>">Offer Info</a> </li>
  <li class="<?php echo $action=='background'?'active':''?>"> <a href="<?php echo $this->createUrl('offer/background',array('id'=>$offerModel->id));?>">Background Verifications</a> </li>
  <li class="<?php echo $action=='exhibit'?'active':''?>"> <a href="<?php echo $this->createUrl('offer/exhibit',array('id'=>$offerModel->id));?>">Documents (Exhibit)</a> </li>
  <?php /*?><li class="<?php echo $action=='notes'?'active':''?>">
   <a href="<?php echo $this->createUrl('offer/notes',array('id'=>$offerModel->id));?>">Notes</a>
   </li>

    <li class="<?php echo $action=='other'?'active':''?>">
   <a href="<?php echo $this->createUrl('offer/other',array('id'=>$offerModel->id));?>">Others</a>
   </li><?php */?>
  <li class="<?php echo $action=='onBoarding'?'active':''?>"> <a href="<?php echo $this->createUrl('offer/onBoarding',array('id'=>$offerModel->id));?>" <?php echo $offerModel->workorder_status==0?'style="pointer-events: none;"':''; ?> >On Boarding</a> </li>
  <?php /*?><li class="<?php echo $action=='offBoarding'?'active':''?>">
   <a href="<?php echo $this->createUrl('offer/offBoarding',array('id'=>$offerModel->id));?>">Off Boarding</a>
   </li><?php */?>
  <li class="<?php echo $action=='addProject'?'active':''?>"> <a href="<?php echo $this->createUrl('offer/addProject',array('id'=>$offerModel->id));?>" <?php echo $offerModel->workorder_status==0?'style="pointer-events: none;"':''; ?> >Project / Task</a> </li>
</ul>
