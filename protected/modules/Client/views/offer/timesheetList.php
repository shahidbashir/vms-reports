<script>
  function myFunction() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      td1 = tr[i].getElementsByTagName("td")[1];
      td2 = tr[i].getElementsByTagName("td")[2];
      td3 = tr[i].getElementsByTagName("td")[3];
      td4 = tr[i].getElementsByTagName("td")[4];
      td5 = tr[i].getElementsByTagName("td")[5];
      td6 = tr[i].getElementsByTagName("td")[6];
      td7 = tr[i].getElementsByTagName("td")[7];
      td8 = tr[i].getElementsByTagName("td")[8];
      if (td || td1 || td2 || td3 || td4 || td5 || td6 || td7 || td8) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        }else if(td1.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td2.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td3.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td4.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td5.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td6.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td7.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td8.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }
        else {
          tr[i].style.display = "none";
        }

      }
    }
  }
</script>
<?php $this->pageTitle = 'Time Sheet'; ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List of Time Sheet <!--<a href=""  class="pull-right"><i class="fa fa-question"></i></a> <a  class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
      <p class="m-b-20"> A list of timesheet for candiate and there status.</p>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="sdata" placeholder="Search Time Sheet">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <br>
      <div class="">
        <div  class="tab-pane active"  id="list-view">
          <table id="myTable" class="table m-b-40 without-border" >
            <thead class="thead-default">
              <tr>
                <th  style="width: 10px;">Status</th>
                <th>ID </th>
                <th>Name</th>
                <th> Vendor </th>
                <th>Hour</th>
                <th>Pay Rate</th>
                <th>Bill Rate</th>
                  <th>Duration</th>
                  <th>Location</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="subdata">
              <?php if($record){ 
				  foreach($record as $value){
                      $offer = Offer::model()->findByPk($value['offer_id']);
					  $candidate = Candidates::model()->findByPk($value['candidate_id']);
					  $postingstatus = UtilityManager::timeSheetStatus();
					  $vendor = Vendor::model()->findByPk($candidate->vendor_id);
					  switch ($postingstatus[$value['timesheet_status']]) {
						case "Approved":
							$color = 'tag label-new-request';
							break;
						case "Pending":
							$color = 'tag label-hold';
							break;
						case "Rejected":
							$color = 'tag label-pending-aproval';
							break;
						default:
							$color = 'tag label-re-open';
					}
				  ?>
              <tr>
                <td><span class="<?php echo $color; ?>"><?php echo $postingstatus[$value['timesheet_status']]; ?></span></td>
                <td><a href=""><?php echo $value['id']; ?></a></td>
                <td><a href="">
                  <?php if($candidate){ echo $candidate->first_name.' '.$candidate->last_name; } ?>
                  </a></td>
                <td><a href="">
                  <?php if($vendor){ echo $vendor->organization; } ?>
                  </a></td>
                <td><?php echo $value['total_hours']; ?> <i class="fa fa-question" 
                                       data-toggle="tooltip" data-placement="right" title="Regular Hr: <?php echo $value['total_regular_hours']; ?>, Over Time: <?php echo $value['total_overtime_hours']; ?>, Double Time: <?php echo $value['total_doubletime_hours']; ?>"
                                       ></i></td>
                <td>$<?php echo $value['total_payrate']; ?></td>
                <td>$<?php echo $value['total_billrate']; ?></td>
                  <td><?php echo date("m-d-Y", strtotime($value['invoice_start_date'])).' To '.date("m-d-Y", strtotime($value['invoice_end_date'])); ?></td>
                  <td><?php if($offer) {
                          $location = Location::model()->findByPk($offer->approver_manager_location);
                          echo $location->name;
                      }
                          ?></td>
                <td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('offer/timesheetDetail',array('id'=>$value['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
        <!-- list-view -->
        
        <!--<div  class="tab-pane"  id="stack-view">
          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>-->
        <!-- list-view --> 
        
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="clearfix" style="padding: 10px 20px 10px; ">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <?php
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                    'header' => '',
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class' => 'pagination m-t-0',
                    )
                ))
                ?>
    </div>
    
  </div>
  <div class="seprater-bottom-100"></div>
</div>
<input type="hidden" name="url" id="url" value="<?php echo Yii::app()->controller->action->id; ?>">
<script>
  $(document).ready(function(){
    /*$("input").keydown(function(){
     $("input").css("background-color", "yellow");
     });*/
    $('#sdata').keyup(function(){
      var subValue=$(this).val();
      var action = $("#url").val();
      $.ajax({
        'url':'<?php echo $this->createUrl('costCenter/timesheetSearch') ?>',
        type: "POST",
        data: { subValue: subValue,action: action},
        'success':function(html){
          //alert(html);
          $("#subdata").html(html)
        }
      });
      //$("input").css("background-color", "pink");
    });
  });
</script>
