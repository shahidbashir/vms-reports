<?php 
	$this->pageTitle = 'Set Up Account';
	$offerID = $_GET['offerID'];
	$offerData = Offer::model()->findByPk($offerID);
	$submissionData = VendorJobSubmission::model()->findByPk($offerData->submission_id);
	$vendorData = Vendor::model()->findByPk($submissionData->vendor_id);
	$candidateData = Candidates::model()->findByPk($submissionData->candidate_Id);
	$candidateProfile = CandidatesProfile::model()->findByAttributes(array('candidate_id'=>$submissionData->candidate_Id));
	$canPersonalMail = CandidatesEmail::model()->findByAttributes(array('candidate_id'=>$submissionData->candidate_Id,'type'=>'Personal'));
	//$canOfficailMail = CandidatesEmail::model()->findByAttributes(array('candidate_id'=>$submissionData->candidate_Id,'type'=>'Offical'));
	$approvalManager = Client::model()->findByPk($offerData->approval_manager);
	$username = 'VMS-'.time();
	$jobData = Job::model()->findByPk($submissionData->job_id);
	$allTeamMembers = Client::model()->findAllByAttributes(array('super_client_id'=>$jobData->user_id));
	$coordinatorIDs = AdminMemerToClient::model()->findAllByAttributes(array('client_id'=>$jobData->user_id));
	$ids = array();
	foreach($coordinatorIDs as $value){
		$ids[] = $value->admin_member_id;
		}
	
	$mspCoordinator = Admin::model()->findAllByAttributes(array('id'=>$ids,'staf_type'=>'MSP Coordinator'));
	/*echo '<pre>';
	print_r($offerData->rates_regular);
	exit;*/

 ?>

<?php if(Yii::app()->user->hasFlash('success')):?>
     <?php echo Yii::app()->user->getFlash('success'); ?>
<?php endif; ?>

<div class="row">
  <div class="col-md-12">
    <div class="white-box">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div role="tabpanel"> 
            <!-- Nav tabs -->
            
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="jobs">
                <div class="row">
                  <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                    <form action="" method="POST" role="form">
                      <div class="two-flieds">
                        <div class="form-group">
                          <label for="">Start Date</label>
                          <input type="text" class="form-control dateField" value="<?php echo $offerData->job_start_date; ?>" id="dateField" name="emp_start_date">
                        </div>
                        <div class="form-group">
                          <label for="">MSP Account Manager</label>
                          <div class="single">
                            <select name="emp_msp_account_mngr" class="ui fluid search dropdown">
                              <?php foreach($mspCoordinator as $coordinators){ ?>                              
                              <option value="<?php echo $coordinators->id; ?>">
							  	<?php echo $coordinators->first_name.' '.$coordinators->last_name; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <!-- two-flieds -->
                      
                      <div class="two-flieds">
                        <div class="form-group">
                          <label for="">Client Account Manager</label>
                          <div class="single">
                            <select name="emp_client_account_mngr" class="ui fluid search dropdown">
                              <!--<option value=""></option>-->
                              <?php foreach($allTeamMembers as $allMembers){ ?>                              
                              <option value="<?php echo $allMembers->id; ?>"><?php echo $allMembers->first_name.' '.$allMembers->last_name.'('.$allMembers->member_type.')'; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="">Client Time Sheet Approver</label>
                          <div class="single">
                            <select name="emp_time_sheet_aprover" class="ui fluid search dropdown">
                              <option value="<?php echo $offerData->approval_manager; ?>"><?php echo $approvalManager->first_name.' '.$approvalManager->last_name; ?></option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <!-- two-flieds -->
                      
                      <div class="two-flieds">
                        <div class="form-group">
                          <label for="">Candidate Username:</label>
                          <input type="text" class="form-control" value="<?php echo $username; ?>" id="" name="emp_user_name">
                        </div>
                        <div class="form-group">
                          <label for="">Candidate Personal Email ID:</label>
                          <input type="text" class="form-control" value="<?php if($canPersonalMail) echo $canPersonalMail->email ?>" id="" name="personal_email">
                        </div>
                      </div>
                      <!-- two-flieds -->
                      
                      <div class="two-flieds">
                        <div class="form-group">
                          <label for="">Candidate Official EmailID</label>
                          <input type="text" class="form-control" value="" id="" name="emp_official_email">
                        </div>
                        <div class="form-group">
                          <label for="">Location</label>
                          <div class="single">
                            <select name="emp_location" class="ui fluid search dropdown">
							  <option value="<?php echo $offerData->location; ?>"><?php echo $offerData->location; ?></option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="">Note</label>
                        <textarea name="emp_note" id="input" class="form-control" rows="3" required="required"></textarea>
                      </div>
                      
                      
                      <?php 
					  $rate = $offerData->rates_regular + $offerData->rates_regular*($offerData->over_time/100) + $offerData->rates_regular*($offerData->double_time/100);
					  
					  ?>
                      
                      
                      <div class="three-flieds">
                        <div class="form-group">
                          <label for="">Pay Rate</label>
                          <input type="text" value="<?php echo $offerData->rates_regular.' + '.$offerData->rates_regular*($offerData->over_time/100).' + '.$offerData->rates_regular*($offerData->double_time/100); ?>" class="form-control" id="" name="emp_pay_rate">
                          <input type="hidden" value="<?php echo $rate; ?>" class="form-control" id="" name="emp_pay_rate">
                        </div>
                        <div class="form-group">
                          <label for="">Over Time</label>
                          <input type="text" value="<?php echo $offerData->over_time; ?>" class="form-control" id="" name="emp_over_time">
                        </div>
                        <div class="form-group">
                          <label for="">Double Time</label>
                          <input type="text" value="<?php echo $offerData->double_time; ?>" class="form-control" id="" name="emp_double_time">
                        </div>
                      </div>
                      <button type="submit" name="setup_account" class="btn btn-success">Setup Account</button>
                      <button type="reset" class="btn btn-default">Cancel</button>
                    </form>
                  </div>
                  <!-- col --> 
                </div>
                <!-- row --> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- col --> 
      </div>
      <!-- row --> 
    </div>
  </div>
</div>
