<?php $this->pageTitle =  'Boarding'; ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List of On Boarding <!--<a href="" class="pull-right"><i class="fa fa-question"></i></a> <a href="" class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
      <p class="m-b-20">
          A List of candidate who are waiting for onboarding. During the onboarding process the candidate get user name and password for the candidate portal.</p>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="search" placeholder="Search On Boarding">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <?php /*?><div class="row m-b-20">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <form method="post">
            <div class="two-fields-70-30">
              <div class="form-group form-control-2">
                <label for="" style="text-align: left;">Sort by Job Name</label>
                <select name="" id="input" class="form-control  select2" required>
                  <option value="">Select</option>
                  <option value="">Java Developer (291)</option>
                  <option value="">Data Mining</option>
                </select>
              </div>
              <div class="form-group">
                <label for="" >&nbsp; </label>
                <p>
                  <button type="button" class="btn btn-success">Show</button>
                </p>
              </div>
            </div>
          </form>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> </div>
      </div><?php */?>
      <div class="simplify-tabs m-b-50">
        <div role="tabpanel"> 
          <!-- Nav tabs -->
          <ul class="nav nav-tabs no-hover" role="tablist">
            <li class="nav-item"> <a class="nav-link <?php if(Yii::app()->controller->action->id=='todayboarding'){ echo 'active'; } ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/todayboarding'); ?>" role="tab" 
                                          data-toggle="tooltip" data-placement="bottom" title="Today Joiners"> Today Joiners</a> </li>
            <li class="nav-item"> <a class="nav-link <?php if(Yii::app()->controller->action->id=='pendingboarding'){ echo 'active'; } ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/pendingboarding'); ?>"role="tab" 
                                          data-toggle="tooltip" data-placement="bottom" title="Pending Joiners"> Pending Joiners</a> </li>
            <!--<li class="nav-item"> <a class="nav-link <?php /*if(Yii::app()->controller->action->id=='onBoardingList'){ echo 'active'; } */?>"  href="<?php /*echo Yii::app()->createAbsoluteUrl('Client/offer/onBoardingList'); */?>" role="tab"
                                          data-toggle="tooltip" data-placement="bottom" title="Calendar View"> All Joiners</a> </li>-->
          </ul>
          <div class="tab-content ">
            <div class="tab-pane active" id="home" role="tabpanel">
              <div class="row table-row-to-remove-margin" style="margin-top: -37px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <table class="table m-b-30 m-t-20 without-border">
                    <thead class="thead-default">
                      <tr>
                        <th style="width: 10px;">S.No</th>
                        <th><a href="">Candidate Name</a></th>
                        <th>Location</th>
                        <th> Job Title ( Job ID ) </th>
                        <th>Work Order ID</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
							$i = 0;
							if($models){
							foreach($models as $model){
								$candiadte = Candidates::model()->findByPk($model['candidate_id']);
								$jobs = Job::model()->findByPk($model['job_id']);
								$offer = Offer::model()->findByPk($model['offer_id']);
								$location = Location::model()->findByPk($offer->approver_manager_location);
							$i++;
							?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><a href="<?php echo $this->createAbsoluteUrl('offer/workorderView',array('id'=>$model['id'])); ?>"><?php echo $candiadte->first_name.' '.$candiadte->last_name; ?></a></td>
                        <td><?php echo $location->name; ?></td>
                        <td><a href=""><?php echo $jobs->title.'('.$jobs->id.')'; ?></a></td>
                        <td><?php echo $model['id']; ?></td>
                        <td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('offer/workorderView',array('id'=>$model['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                      </tr>
                      <?php } }else{ ?>
                      <tr>
                        <td colspan="7">Sorry No Record Found</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <div class="row m-b-10" style="padding: 10px 0 10px; ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <?php
						$this->widget('CLinkPager', array(
							  'pages' => $pages,
							  'header' => '',
							  'nextPageLabel' => 'Next',
							  'prevPageLabel' => 'Prev',
							  'selectedPageCssClass' => 'active',
							  'hiddenPageCssClass' => 'disabled',
							  'htmlOptions' => array(
								  'class' => 'pagination m-t-0 m-b-0',
							  )
						  ))
						  ?>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <!--<p class="text-right"> Showing 10 to 20 of 50 entries </p>-->
                    </div>
                  </div>
                </div>
              </div>
              <!-- row --> 
            </div>
            <!-- tab-pane --> 
          </div>
        </div>
      </div>
    </div>
    <!-- col --> 
  </div>
  <!-- row -->
  <div class="clearfix" style="padding: 10px 20px 30px; ">
    <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h5 class="m-b-10">Quick Note</h5>
      <p class="m-b-10"> <span class="tag label-interview-pending">Pending</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-interview-cancelled">Cancelled</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-interview-reschedule">Reschedule</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-interview-completed">Completed</span> Lorem ipsum dolor sit amet. </p>
    </div>-->
  </div>
  <div class="seprater-bottom-100"></div>
</div>
