<?php $this->pageTitle = 'Time Sheet'; ?>

<div class="main-content">
  <div class="row"> 
    <!--Condensed Table--> 
    
    <!--Hover table-->
    
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-border-color panel-border-color-primary">
          <div class="panel-heading"></div>
          <div class="panel-body">
          
            <?php /*?><div class="row">
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group"> <a href="log-timesheet.html" class="btn btn-success">Log Time Sheet</a> </div>
              </div>
              <!-- col --> 
            </div><?php */?>
            <!-- row -->
            
            <hr>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-striped dataTable no-footer">
                  <thead>
                  
                   
                  
                  
                    <tr>
                      <th>ID </th>
                      <th style="text-align: left">Candidate Name</th>
                      <th style="text-align: left">Job</th>
                      <th style="text-align: center">Location</th>
                      <th style="text-align: center">Work Order ID</th>
                      <th style="text-align: center">Date</th>
                      <th style="text-align: center">RT</th>
                      <th style="text-align: center">OT</th>
                     <th style="text-align: center">DT</th>
                       <!--<th style="text-align: center">Time Off</th>-->
                      <th style="text-align: center">Total</th>
                      <th style="text-align: center">Status</th>
                      <th style="text-align: center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  
                  	<?php 
				    
					if($record){
						$i=0;
						foreach($record as $value){
					$postingstatus = UtilityManager::workorderStatus();
					
					
					switch ($postingstatus[$value['status']]) {
						case "Approved":
							$color = 'label-success';
							break;
						case "Pending":
							$color = 'label-info';
							break;
						case "Rejected":
							$color = 'label-danger';
							break;
						default:
							$color = 'label-primary';
					}
					
						$i++;
						$total = 0;$regular = 0;$overtimeTotal = 0;$regulartimeTotal = 0;
						$cptimesheet = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$value['id'],'type'=>'Regular Hour'));
						$overTime = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$value['id'],'type'=>'Over Time'));
						$regularTime = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$value['id'],'type'=>'Double Time'));
						$candidate = Candidates::model()->findByPk($value['candidate_id']);
						$candidatelocation = Offer::model()->findByPk($value['offer_id']);
						
						$jobData = Job::model()->findByPk($candidatelocation->job_id);
						
						foreach($cptimesheet as $cptimesheetValue){
							$regular += $cptimesheetValue->total;
							$projectID = $cptimesheetValue->project_id;
							$logID = $cptimesheetValue->timesheet_log_id;
							}
							
						foreach($overTime as $overTime){
							$overtimeTotal += $overTime->total;
							}
						foreach($regularTime as $regularTime){
							$regulartimeTotal += $regularTime->total;
							}
								
						?>
                  
                  
                    <tr>
                      <td style="text-align: left"><?php echo $value['id']; //$i; ?></td>
                      <td style="text-align: left"> <?php echo $candidate->first_name.' '.$candidate->last_name ?> </td>
                      <td style="text-align: center"> <?php echo $jobData->title.'('.$jobData->id.')'; ?>  </td>
                      <td style="text-align: left"> <?php echo $candidatelocation->location; ?> </td>
                      <td style="text-align: left"> <?php echo $value['offer_id']; ?> </td>
                      <td style="text-align: center"> <?php 
							$dates = UtilityManager::getStartAndEndDate($value['week_number'],$value['year']);
							echo date("jS M Y",strtotime($dates[0])).' to '.date("jS M Y",strtotime($dates[1])) ;
							?> </td>
                      <td style="text-align: center"> <?php echo $regular; ?> </td>
                      <td style="text-align: center"> <?php echo $overtimeTotal; ?> </td>
                      <td style="text-align: center"> <?php echo $regulartimeTotal; ?> </td>
                      <td style="text-align: center"> <?php echo $regular + $overtimeTotal + $regulartimeTotal; ?> </td>
                      <td style="text-align: center"><span class="label <?php echo $color; ?>"><?php echo $postingstatus[$value['status']]; ?> </span></td>
                      <td style="text-align: center">
                      <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/timesheetDetail',array('logID'=>$value['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon mdi mdi mdi-eye"></i></a>
                      </td>
                    </tr>
                    
                    
                    <?php } } ?>
                    
                  </tbody>
                </table>
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                    'header' => '',
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class' => 'pagination',
                    )
                ))
                ?>
              </div>
              <!-- col --> 
            </div>
            <!-- row --> 
            
            <br>
          </div>
        </div>
      </div>
    </div>
    
    <!--Hover table--> 
    
  </div>
</div>
