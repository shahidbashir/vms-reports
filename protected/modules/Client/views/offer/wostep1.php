<?php 	
$this->pageTitle = 'Create New Work Order';	
if(isset(Yii::app()->session['wo_first_form'])){		
	unset(Yii::app()->session['wo_first_form']);
}	
$jobId = '';	
if(isset($_GET['jobId'])){		
	$jobId = $_GET['jobId'];
} ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Create New Work Order</h4>
      <p class="m-b-40">Step by Step instruction to create a workorder for the vendor on behalf of the candidate.</p>
      <form action="" method="POST" role="form">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group ">
            <label>Select Jobs</label>
            <select name="job_id" id="Offer_jobs1" class="form-control select2" required>
                    <option value=""></option>
                    <?php foreach($jobData as $key=>$value){ ?>
                    <option value="<?php echo $value->id ?>" <?php //echo ($value->id==$jobId)?'selected':''; ?>><?php echo '('.$value->id.') '.$value->title ?></option>
                    <?php } ?>
                  </select>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group ">
            <label>Select Candidate</label>
            <select  name="can_submission_id" id="candidates" class="form-control select2" required>
                <option value=""></option>
              </select>
          </div>
        </div>
      </div>
      <div class="row m-b-20 m-t-50">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <p class="text-left">
            <button name="first_form" type="submit" class="btn btn-success">Continue</button>
            <button type="reset" class="btn btn-space btn-default">Cancel</button>
          </p>
        </div>
      </div>
      </form>
      <div class="seprater-bottom-100"></div>
      <div class="seprater-bottom-100"></div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row --> 
  
</div>
