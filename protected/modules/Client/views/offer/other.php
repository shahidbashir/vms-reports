<?php $this->renderPartial('overview_menu',array('offerModel'=>$offerModel));?>
  <div class="tab-content">
     <div role="tabpanel" class="workorder-notes ">
             <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                 
                 <li role="presentation" class="active">
                     <a href="#team-msp" aria-controls="team-msp" role="tab" data-toggle="tab">MSP</a>
                 </li>
                 <li role="presentation">
                     <a href="#team-vendor" aria-controls="team-vendor" role="tab" data-toggle="tab">Vendor</a>
                 </li>
             </ul>
         
             <!-- Tab panes -->
             <div class="tab-content">
               
                 <div role="tabpanel" class="tab-pane active" id="team-msp">
                  <table class="table table-striped table-hover">
                    <thead>
                       <tr>
                         <th>S.No </th>
                         <th>Name</th>
                         <th>Role</th>
                         <th>Email Address</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                       </tr>
                     </tbody>
                   </table>
                 </div>
                  <div role="tabpanel" class="tab-pane" id="team-vendor">
                    <table class="table table-striped table-hover"> 
                     <thead>
                       <tr>
                         <th>S.No vendor</th>
                         <th>Name</th>
                         <th>Role</th>
                         <th>Email Address</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>

                       </tr>
                     </tbody>
                   </table>

                  </div>
             </div>
         </div> 
