<?php 
	$this->pageTitle =  'Offer';
	$workorder = Workorder::model()->findAllByAttributes(array('offer_id'=>$offerModel->id,'workorder_status'=>array(0,1)));
 	
 ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

  <div class="cleafix " style="padding: 30px 20px; ">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Offer (<?php echo $offerModel->id;?>)
      <a href="<?php echo $this->createAbsoluteUrl('job/offer'); ?>" class="btn btn-sm btn-default pull-right"> Back to Offer Listing</a>
      <?php if($offerModel->status==1 && empty($workorder)){ ?>
      	<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('type'=>'workorder','id'=>$offerModel->id)); ?>" class="btn btn-sm btn-success pull-right">Create WorkOrder</a>
	  <?php } ?>
        
      </h4>
      <p class="m-b-42">Location : <?php
        $location = Location::model()->findByPk($offerModel->approver_manager_location);
        echo $location->name.'-'.$location->address1 ;//echo $offers['location'];?></p>
      <div class="row add-job">
        <div class="col-md-12">
          <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-heading"></div>
            <div class="panel-body">
              <div class="col-md-12">
                <?php if(Yii::app()->user->hasFlash('success')):
                  echo Yii::app()->user->getFlash('success');
                endif; ?>
                <?php if($offerModel->status == 2){ ?>
              <div class="alert-box m-t-10 m-b-40">
                <p class="bold m-b-10">Reason for Rejection</p>
                <p class="m-b-10"><?php echo $offerModel->reason_for_rejection; ?></p>

                <p class="bold m-b-10">Notes:</p>
                <p class="m-b-10"><?php echo $offerModel->notes; ?></p>


                <!--<p><strong>Posted By :</strong> Person Name at 10:30 PM on 10th May 2015</p>-->
              </div>
                <?php } ?>
              <table class="table table-condensed table-50 ">
                <tbody>
                <tr>
                  <td>Candidate Name</td>
                  <td><?php echo $offers['first_name'].' '.$offers['last_name'];?></td>
                </tr>
                <tr>
                  <td>Current Location</td>
                  <td><?php
                    $candidate = Candidates::model()->findByPk($offers['c_id']);
                    //$location = Location::model()->findByPk($offerModel->approver_manager_location);
                    if($candidate){
                    echo $candidate->current_location ; } ?></td>
                </tr>
                <tr>
                  <td>Vendor Name</td>
                  <?php $vendor = Vendor::model()->findByPk($offerModel->vendor_id)?>
                  <td><?php if($vendor){ echo $vendor->organization; } ?></td>
                </tr>
                <?php if($offerModel->status == 1){ ?>
                
                <tr>
                    <td>Offer Accepted By</td>
                    <td><?php  if($offerModel->modified_by_type =="MSP"){
                          $admin = Admin::model()->findByAttributes(array('id'=>$offerModel->modified_by_id));
                          echo $admin->first_name.' '.$admin->last_name;
                        }else if($offerModel->modified_by_type =="Vendor"){
                          $vendor = Vendor::model()->findByAttributes(array('id'=>$offerModel->modified_by_id));
                          echo $vendor->organization.' ('.$vendor->first_name.' '.$vendor->last_name.')';
                      } else if($offerModel->modified_by_type =="Client"){
                          $client = Client::model()->findByAttributes(array('id'=>$offerModel->modified_by_id));
                          echo $client->business_name.' ('.$client->first_name.' '.$client->last_name.')';
                      }  ?>
                    </td>
                </tr>
                <tr>
                  <td>Offer Accept Date</td>
                  <td><?php
                    if(date('Y',strtotime($offerModel->offer_accept_date)) != '1970') {
                      echo date('F d,Y h:i:s A', strtotime($offerModel->offer_accept_date));
                    }else{
                      echo 'NULL';
                    }?></td>
                </tr>
                <?php } ?>
                


                </tbody>

              </table>

              <br>
              <hr>

              <br>

              <h4 class="m-b-20 h4-small">Job Start Date and Other Information</h4>

              <table class="table table-condensed table-50 ">
                <tbody>
                <tr>
                  <td>Start Date</td>
                  <td><?php //echo date('m-d-Y',strtotime($offerModel->job_start_date)); ?>
                  
                  <?php if(date('Y',strtotime($offerModel->job_start_date)) != '1970'){ 
				  echo date('F d,Y',strtotime($offerModel->job_start_date));
				  }else{
					    echo 'Null';
					   }
				  ?>
                  </td>
                </tr>
                <tr>
                  <td>End Date</td>
                  <td>
				  <?php //echo date('m-d-Y',strtotime($offerModel->job_end_date)); ?>
                  <?php if(date('Y',strtotime($offerModel->job_end_date)) != '1970'){ 
				  echo date('F d,Y',strtotime($offerModel->job_end_date));
				  }else{
					    echo 'Null';
					   }
				  ?>
                  </td>
                </tr>
                <tr>
                  <td>Location</td>
                  <td><?php
                    $location = Location::model()->findByPk($offerModel->approver_manager_location);
                    echo $location->name ;//echo $offers['location'];?></td>
                </tr>
                <tr>
                  <td>Location Manager</td>
                  <td><?php echo $offerModel->location_manager; ?></td>
                </tr>

                </tbody>
              </table>

              <br>
              <hr>
              <br>

              <h4 class="m-b-20 h4-small">Rates</h4>

              <table class="table table-condensed table-50 ">

                <tbody>

                <tr>
                  <td>Pay Rate</td>
                  <td>$<?php echo $offerModel->offer_pay_rate;?></td>
                </tr>

                <tr>
                  <td>Bill Rate</td>
                  <td>$<?php echo $offerModel->offer_bill_rate;?></td>
                </tr>


                <tr>
                  <td>Over Time Rate for Candidate</td>
                  <td>$<?php echo $offerModel->over_time;?></td>
                </tr>

                <tr>
                  <td>Double Time Rate for Candidate</td>
                  <td>$<?php echo $offerModel->double_time;?></td>
                </tr>

                <tr>
                  <td>Over Time Rate for Client</td>
                  <td>$<?php echo $offerModel->client_over_time;?></td>
                </tr>

                <tr>
                  <td>Double Time Rate for Client</td>
                  <td>$<?php echo $offerModel->client_double_time;?></td>
                </tr>

                <tr>
                  <td>Offer Expire Date</td>
                  <td><?php if(date('Y',strtotime($offerModel->valid_till_date)) != '1970'){ 
				  echo date('F d,Y',strtotime($offerModel->valid_till_date));
				  }else{
					    echo 'Null';
					   }
				  ?></td>
                </tr>

                </tbody>
              </table>

              <br>
              <hr>
              <br>

              <div class="row no-margin">
                <div class="col-md-12">
                  <h4 class="h4-small">Notes &amp; Comments</h4>
                  <div class="simplify-tabs m-b-42">

                    <div role="tabpanel">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs no-hover" role="tablist">

                        <li class="nav-item active">
                          <a class="nav-link "  href="#internal" role="tab" data-toggle="tab" data-placement="bottom" title="Overview">
                            <i class=" nav-icon"></i>
                            Internal</a>
                        </li>

                        <li class="nav-item">
                          <a class="nav-link "  href="#msp" role="tab" data-toggle="tab" data-placement="bottom" title="Overview">
                            <i class=" nav-icon"></i>
                            MSP</a>
                        </li>

                        <li class="nav-item">
                          <a class="nav-link "  href="#vendor" role="tab" data-toggle="tab" data-placement="bottom" title="Overview">
                            <i class=" nav-icon"></i>
                            Vendor</a>
                        </li>

                      </ul>

                      <div class="tab-content ">
                        <div class="tab-pane active" id="internal"  role="tabpanel">
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                              <div class="form-group">
                                <label for="">Note</label>
                                <textarea name="" id="input" class="form-control" rows="5" required readonly><?php echo $offerModel->internal_notes; ?></textarea>
                              </div>


                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="msp"  role="tabpanel">
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                              <div class="form-group">
                                <label for="">Note</label>
                                <textarea name="" id="input" class="form-control" rows="5" required readonly><?php echo  $offerModel->for_admin_notes; ?></textarea>
                              </div>


                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="vendor"  role="tabpanel">
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                              <div class="form-group">
                                <label for="">Note</label>
                                <textarea name="" id="input" class="form-control" rows="5" required readonly><?php echo  $offerModel->ref_notes_vendor; ?></textarea>
                              </div>


                            </div>
                          </div>
                        </div>
                      </div>


                    </div> <!-- tabpanel -->
                  </div> <!-- simplify-tabs -->


                  <hr>
                  <br>

                  <?php /*?><h4 class="h4-small">Teams</h4>


                  <div class="simplify-tabs m-b-42">

                    <div role="tabpanel">

                      <ul class="nav nav-tabs no-hover" role="tablist">

                        <li class="nav-item active">
                          <a class="nav-link "  href="#internal2" role="tab" data-toggle="tab" data-placement="bottom" title="Overview">
                            <i class=" nav-icon"></i>
                            Client</a>
                        </li>


                        <li class="nav-item">
                          <a class="nav-link "  href="#vendor2" role="tab" data-toggle="tab" data-placement="bottom" title="Overview">
                            <i class=" nav-icon"></i>
                            MSP</a>
                        </li>

                      </ul>

                      <div class="tab-content ">
                        <div class="tab-pane active" id="internal2"  role="tabpanel">
                          <div class="row table-row-to-remove-margin" style="margin-top: -37px;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <br>
                              <table class="table m-b-20 without-border">
                                <thead class="thead-default">
                                <tr>
                                  <th>S.No </th>
                                  <th>Name</th>
                                  <th>Role</th>
                                  <th>Email Address</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $i = 0;

                                foreach($invitedTeamMembers as $TeamMembers){

                                $i++;

                                ?>
                                <tr>
                                  <td><?php echo $i; ?></td>

                                  <td><?php echo $TeamMembers['first_name'].' '.$TeamMembers['last_name'] ?></td>

                                  <td><?php echo $TeamMembers['member_type'] ?></td>

                                  <td><?php echo $TeamMembers['email'] ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                              </table>

                            </div>
                          </div>
                        </div>

                        <div class="tab-pane" id="vendor2"  role="tabpanel">
                          <div class="row table-row-to-remove-margin" style="margin-top: -37px;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <br>
                              <table class="table m-b-20 without-border">
                                <thead class="thead-default">
                                <tr>
                                  <th>S.No </th>
                                  <th>Name</th>
                                  <th>Role</th>
                                  <th>Email Address</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                  
                                $i = 0;

                                foreach($mspInvitedTeam as $mspTeam){

                                  $i++;

                                  ?>

                                  <tr>

                                    <td><?php echo $i; ?></td>

                                    <td><?php echo $mspTeam['first_name'].' '.$mspTeam['last_name'] ?></td>

                                    <td><?php echo $mspTeam['staff_type'] ?></td>

                                    <td><?php echo $mspTeam['email'] ?></td>

                                  </tr>

                                <?php } ?>
                                </tbody>
                              </table>

                            </div>
                          </div>
                        </div>
                      </div>


                    </div> 
                  </div><?php */?> <!-- simplify-tabs -->
                </div>
              </div>
              <?php if($digitalDocStatus==1){ ?>
              	<iframe src = "<?php echo Yii::app()->getBaseUrl(true);?>/ViewerJS/#..<?php echo $filePath;?>" width='1024' height='1024' allowfullscreen webkitallowfullscreen></iframe>
              <?php } ?>


              <div>
              <?php /*if($offerModel->status==4){ ?>
                <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('type'=>'accepted','id'=>$offerModel->id)); ?>" class="btn btn-success">Offer Accept</a>
                <a href="#modal-id" data-toggle="modal" class="btn btn-danger">Offer Rejected</a>
			  <?php }*/ ?>
              </div>
              <br>
              <br>
            </div>
            <!-- panel-body -->
          </div>
        </div>
      </div>

    </div>
    <!-- col -->

  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>

</div>
  <div class="modal fade" id="modal-id">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Reject Offer</h4>
        </div>
        <form action="" method="POST" role="form">
        <div class="modal-body">


          <div class="form-group">
            <label for="">Reason for Rejection</label>
            <?php $setting = Setting::model()->findAll(array('condition'=>'category_id=51'));
            ?><select name="reason_for_rejection" id="input" class="form-control" value="" required  title="">
              <?php foreach($setting as $value){ echo "<option value='".$value->title."'>".$value->title."</option>";} ?>
              </select>
          </div>


          <div class="form-group">
            <label for="">Note</label>
            <textarea name="notes" id="input" class="form-control" rows="3" required></textarea>
          </div>

        </div>
        <div class="modal-footer">

          <button type="submit" name="rejected" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
          </form>
      </div>
    </div>
  </div>