<?php    $this->pageTitle =  'Offers';?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List of Offers <!--<a href="" class="pull-right"><i class="fa fa-question"></i></a> <a href="" class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
      <p class="m-b-20">A List of offers issued for candidate towards the job.</p>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="sdata" placeholder="Search Offers">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> </div>
      <div class="row m-b-20">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <form action="" method="post">
            <div class="two-fields-70-30">
              <div class="form-group form-control-2">
                <?php $jobData = Job::model()->findAllByAttributes(array('user_id'=>UtilityManager::superClient(Yii::app()->user->id))); ?>
                <label for="" style="text-align: left;">Sort by Job Name</label>
                <select name="job_id" id="input" class="form-control  select2" required>
                  <option value="">Select</option>
                  <?php foreach ($jobData as $data){ ?>
                  <option value="<?php echo $data->id; ?>"<?php if(isset($_POST['s']) && $_POST['s']==$data->id){ ?> selected="selected" <?php } ?>><?php echo $data->title.'('.$data->id.')';?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="" >&nbsp; </label>
                <p>
                  <button type="submit" class="btn btn-success">Show</button>
                </p>
              </div>
            </div>
          </form>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <p class="bold m-t-40"> <a href="" class="pull-right"> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/listing@512px.svg"></i> </a> <a href="" class="pull-right" style="margin-right: 10px; "> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/menu@512px.svg"></i> </a> </p>
        </div>
      </div>
      <!-- row -->
      <div class="simplify-tabs m-b-50">
        <div role="tabpanel"> <!-- Nav tabs -->
          <ul class="nav nav-tabs no-hover" id="offer" role="tablist">
            <li id='All' class="nav-item"> <a class="nav-link active"  href="#home" role="tab" data-toggle="tab" data-placement="bottom" title="All Offer"> All Offer</a> </li>
            <li  id='Pending' class="nav-item"> <a class="nav-link "  href="#tab" role="tab" data-toggle="tab" data-placement="bottom" title="Pending Offer"> Pending Offer</a> </li>
            <li id='Approved' class="nav-item"> <a class="nav-link "  href="#tab2" role="tab" data-toggle="tab" data-placement="bottom" title="Offer Approved"> Offer Approved</a> </li>
          </ul>
          <div class="tab-content ">
            <div class="tab-pane active" id="home" role="tabpanel">
              <div class="row table-row-to-remove-margin">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                      <tr>
                        <th style="width: 30px;">Status</th>
                        <th style="width: 120px;"><a href="">Offer ID</a></th>
                        <th>Job ID</th>
                        <th>Candidate Name</th>
                        <th>Offer Date</th>
                        <th>Bill Rate</th>
                        <th>Pay Rate</th>
                        <th>Location</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody id="subdata">
                      <?php if($offers) {                                            
					  foreach ($offers as $value) {                                                
					  $location = Location::model()->findByPk($value['approver_manager_location']);                                                
					  $jobModel = Job::model()->findByPk($value['job_id']);                                                
					  $offerStatus = UtilityManager::offerStatus();                                                
					  if ($value['offer_status'] == 1) {                                                    
					  $className = "label-open";                                                
					  } else if ($value['offer_status'] == 2) {                                                    
					  $className = "label-rejected";                                               
					   } elseif ($value['offer_status'] == 3) {                                                    
					   $className = "info";                                                
					   } else if ($value['offer_status'] == 4) {                                                    
					   $className = "label-hold";                                               
					    } else {                                                   
						 $className = "label-re-open";
					    }  ?>
                        <tr>
                        <td><span class="tag <?php echo $className; ?>"><?php echo $offerStatus[$value['offer_status']]; ?></span></td>
                        <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view', array('id' => $value['offer_id'])); ?>"><?php echo $value['offer_id']; ?></a></td>
                        <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$value['job_id'],array('type'=>'info')); ?>"><?php echo $value['job_id']; ?></a></td>
                        <td><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></td>
                        <td><?php echo date('F d,Y', strtotime($value['job_start_date'])); ?></td>
                        <td><?php echo '$' . $value['offer_bill_rate']; ?></td>
                        <td><?php echo '$' . $value['offer_pay_rate']; ?></td>
                        <td><?php if ($location) {
                            echo $location->name;
                          } else {
                            echo 'Null';
                          } ?></td>
                        <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view', array('id' => $value['offer_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                      </tr>
                      <?php }}else{ ?>
                      <tr>
                        <td colspan="9">There is no offer created</td>
                      </tr>
                      <?php }?>
                    </tbody>
                  </table>
                  <div class="row m-b-10" style="padding: 10px 0 10px; ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <?php
                      $this->widget('CLinkPager', array('pages' => $pages,
                          'header' => '',
                          'nextPageLabel' => 'Next',
                          'prevPageLabel' => 'Prev',
                          'selectedPageCssClass' => 'active',
                          'hiddenPageCssClass' => 'disabled',
                          'htmlOptions' => array('class' => 'pagination m-t-0 m-b-0',)));
                      ?>
                    </div>
                  </div>
                </div>
              </div>
              <!-- row --> </div>
            <div class="tab-pane" id="tab" role="tabpanel">
              <div class="row table-row-to-remove-margin">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                      <tr>
                        <th style="width: 30px;">Status</th>
                        <th style="width: 120px;"><a href="">Offer ID</a></th>
                        <th>Job ID</th>
                        <th>Candidate Name</th>
                        <th>Offer Date</th>
                        <th>Bill Rate</th>
                        <th>Pay Rate</th>
                        <th>Location</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody id="pending">
                      <?php if($pendingoffers){ foreach($pendingoffers as $value) {
                        $location = Location::model()->findByPk($value['approver_manager_location']);
                        $jobModel = Job::model()->findByPk($value['job_id']);
                        $offerStatus = UtilityManager::offerStatus();
                        if($value['offer_status'] == 1){
                          $className ="label-open";
                        }else if($value['offer_status'] == 2){
                          $className ="label-rejected";
                        }elseif($value['offer_status'] == 3){
                          $className ="info";
                        }else if($value['offer_status'] == 4){
                          $className ="label-hold";
                        }else {
                          $className ="label-re-open";
                        }
                        ?>
                      <tr>
                        <td><span class="tag <?php echo $className;?>"><?php echo  $offerStatus[$value['offer_status']];?></span></td>
                        <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('id'=>$value['offer_id'])); ?>"><?php echo $value['offer_id'];?></a></td>
                        <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$value['job_id'],array('type'=>'info')); ?>"><?php echo $value['job_id'];?></a></td>
                        <td><?php echo $value['first_name'].' '.$value['last_name'];?></td>
                        <td><?php echo date('F d,Y',strtotime($value['job_start_date']));?></td>
                        <td><?php echo '$'.$value['offer_bill_rate'];?></td>
                        <td><?php echo '$'.$value['offer_pay_rate'];?></td>
                        <td><?php if($location){ echo $location->name; }else{ echo 'Null'; } ?></td>
                        <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('id'=>$value['offer_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                      </tr>
                      <?php } }else{ ?>
                      <tr>
                        <td colspan="9">There is no offer created</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <div class="row m-b-10" style="padding: 10px 0 10px; ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <?php                                            $this->widget('CLinkPager', array(                                                'pages' => $pages1,                                                'header' => '',                                                'nextPageLabel' => 'Next',                                                'prevPageLabel' => 'Prev',                                                'selectedPageCssClass' => 'active',                                                'hiddenPageCssClass' => 'disabled',                                                'htmlOptions' => array(                                                    'class' => 'pagination m-t-0 m-b-0',                                                )                                            ));                                            ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tab-pane -->
            <div class="tab-pane " id="tab2" role="tabpanel">
              <div class="row table-row-to-remove-margin">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                      <tr>
                        <th style="width: 30px;">Status</th>
                        <th style="width: 120px;"><a href="">Offer ID</a></th>
                        <th>Job ID</th>
                        <th>Candidate Name</th>
                        <th>Offer Date</th>
                        <th>Bill Rate</th>
                        <th>Pay Rate</th>
                        <th>Location</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody id="approved">
                      <?php if($approvedoffers){ foreach($approvedoffers as $value) {
                        $location = Location::model()->findByPk($value['approver_manager_location']);
                        $jobModel = Job::model()->findByPk($value['job_id']);
                        $offerStatus = UtilityManager::offerStatus();
                        if($value['offer_status'] == 1){
                          $className ="label-open";
                        }else if($value['offer_status'] == 2)
                        {
                          $className ="label-rejected";
                        }elseif($value['offer_status'] == 3){
                          $className ="info";
                        }else if($value['offer_status'] == 4){
                          $className ="label-hold";
                        }else {
                          $className ="label-re-open";
                        }
                        ?>
                      <tr>
                        <td><span class="tag <?php echo $className;?>"><?php echo  $offerStatus[$value['offer_status']];?></span></td>
                        <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('id'=>$value['offer_id'])); ?>"><?php echo $value['offer_id'];?></a></td>
                        <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$value['job_id'],array('type'=>'info')); ?>"><?php echo $value['job_id'];?></a></td>
                        <td><?php echo $value['first_name'].' '.$value['last_name'];?></td>
                        <td><?php echo date('F d,Y',strtotime($value['job_start_date']));?></td>
                        <td><?php echo '$'.$value['offer_bill_rate'];?></td>
                        <td><?php echo '$'.$value['offer_pay_rate'];?></td>
                        <td><?php if($location){ echo $location->name; }else{ echo 'Null'; } ?></td>
                        <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('id'=>$value['offer_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                      </tr>
                      <?php } }else{ ?>
                      <tr>
                        <td colspan="9">There is no offer created</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <div class="row m-b-10" style="padding: 10px 0 10px; ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <?php
                      $this->widget('CLinkPager', array('pages' => $pages2,
                          'header' => '',
                          'nextPageLabel' => 'Next',
                          'prevPageLabel' => 'Prev',
                          'selectedPageCssClass' => 'active',
                          'hiddenPageCssClass' => 'disabled',
                          'htmlOptions' => array('class' => 'pagination m-t-0 m-b-0',)));
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tab-pane --> </div>
        </div>
      </div>
    </div>
    <!-- col --> </div>
  <!-- row -->
  <div class="clearfix" style="padding: 10px 20px 30px; ">
    <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h5 class="m-b-10">Quick Note</h5>
      <p class="m-b-10"> <span class="tag label-rejected">Reject</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-open">Accept</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-hold">Hold</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-re-open">Cancelled</span> Lorem ipsum dolor sit amet. </p>
    </div>-->
  </div>
  <div class="seprater-bottom-100"></div>
</div>
<script>
  $(document).ready(function(){
    $('#sdata').keyup(function(){
      var status = $("#offer").find("a.active").parent('li').attr('id')
      var subValue=$(this).val();
      $.ajax({
        'url':'<?php echo $this->createUrl('costCenter/offerSearch') ?>',
        type: "POST",
        data: { subValue: subValue,status:status},
        'success':function(html){
          //alert(html);
          if(status == 'Pending'){
            $("#pending").html(html)
          }else if(status =='Approved'){
            $("#approved").html(html)
          }else {
            $("#subdata").html(html)
          }
        }
      });
    });
  });
</script>