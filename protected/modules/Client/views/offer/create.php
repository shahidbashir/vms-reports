<header class="db-header">
  <div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
      <?php 
	  		$this->pageTitle = '';
	  		$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']);
			$jobData = Job::model()->findByPk($submissionData->job_id);
			$candidateData = Candidates::model()->findByPk($submissionData->candidate_Id);
			$workFlowData = Worklflow::model()->findByPk($jobData->work_flow);
			$client = Client::model()->findByPk($jobData->user_id);
			//$BillingcodeDepartment = BillingcodeDepartment::model()->
	 		/*echo '<pre>';
			print_r($workFlowData);
			exit;*/
	  
	   ?>
      <h4>
        <?php  //echo $jobmodel->title; ?>
      </h4>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
      <div class="right">
        <ul class="list-inline pull-right">
          <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobListing'); ?>">Back to Job Listing</a></li>
          <li><a href="">Previous Job</a></li>
          <li><a href="">Next Job</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- row --> </header>
<div class="page gray-bg">
<div class="card p-0">
  <div class="job-detail">
    <div class="row m-0">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="client-listing-nav">
          <nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display --> <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex2-collapse">
              <?php $this->renderPartial('/job/jobsdetail_tabs_header'); ?>
            </div>
            <!-- /.navbar-collapse --> </nav>
        </div>
        <!-- client-listing-nav --> </div>
    </div>
    <!-- row -->
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-wraper">
          <div class="db-content-wraper">
            <div class="page-content-outer">
              <div class="page-content">
                <div class="job-detail-inner">
                  <div class="text-right m-t-10"> <!--<a href="" class="btn btn-success">Approve</a> <a href="" class="btn btn-default">Reject</a> <a href="" class="btn btn-default">Re-Issue Offer</a>--> </div>
                  <br>
                  <div class="interview-submission job-offer">
                    <div class="job-offer-header">
                      <table class="table">
                        <tbody>
                          <tr>
                            <td style="width: 46px; padding-right: 0;"><div class="avatar-small-text"> <span>SB</span> </div></td>
                            <td style="padding-right: 0;"><p class="bold"><?php echo $candidateData->first_name.' '.$candidateData->last_name; ?></p>
                            
                            
                            <?php 
								
								if($submissionData->start_date_status == 1){
									$intDate = $submissionData->interview_start_date.' at '.$submissionData->interview_end_date_time;
									}elseif($submissionData->alt1_date_status == 1){
										$intDate = $submissionData->interview_alt1_start_date.' at '.$submissionData->interview_alt1_start_date_time;
										}elseif($submissionData->alt2_date_status == 1){
											$intDate = $submissionData->interview_alt2_start_date.' at '.$submissionData->interview_alt2_start_date_time;
											}elseif($submissionData->alt3_date_status == 1){
												$intDate = $submissionData->interview_alt3_start_date.' at '.$submissionData->interview_alt3_start_date_time;
												}else{
													  $intDate = $submissionData->interview_start_date.' at '.interview_end_date_time;
													 }
								
								
								
								
								//echo $intDate ?>
                            
                            
                              <p><small><?php if($_GET['offerId']!='Null'){ echo date('jS M Y', strtotime($model->issued_offer_date)); ?> - <?php echo date('jS M Y', strtotime($model->valid_till_date)); }else{ echo 'Null'; } ?></small></p></td>
                            <td style="padding-right: 0;"><p class="bold">Offer ID: <a href=""><?php echo $_GET['offerId']; ?></a></p>
                            <?php if(($_GET['offerId']!='Null')){ ?>
                              <p><small>Created on <?php echo date('jS M Y', strtotime($model->date_created)); ?> 
                              <?php if(($model->status=='Approved')){ ?>
                              Approved on <?php echo date('jS M Y', strtotime($model->approval_date));}else{
								  echo ' ( Pending For Approval )';
								  } ?>
                              
                              </small></p>
                            <?php }else{ ?>
                            <p><small>Created on <?php echo date('jS M Y'); ?> </p></small>
                            <?php } ?>
                              </td>
                          </tr>
                          <!-- //end  -->
                          
                        </tbody>
                      </table>
                      </table>
                    </div>
                    <div class="interview-view">
                      <div class="interview-view-header">
                        <div class="row">
                          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Account Manager</p>
                            <p>Null</p>
                          </div>
                          <!-- col -->
                          
                          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Work Flow Approval</p>
                            <p><?php  echo $workFlowData->flow_name; ?> <!--(10th May 2014)--></p>
                          </div>
                          <!-- col -->
                          
                          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Rates (Actual)</p>
                            <p><?php  echo $jobData->rate.' '.$jobData->currency.' '.$jobData->payment_type; ?></p>
                          </div>
                          <!-- col --> 
                          
                        </div>
                        <!-- row --> 
                      </div>
                      <!-- interview-view-header -->
                      
                      <div class="interview-view-header">
                        <div class="row">
                          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Job Duration</p>
                            <p><!--10th May 2014 to 10th May 2016 ""--> <?php  echo $jobData->job_po_duration; ?></p>
                          </div>
                          <!-- col -->
                          
                          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Shift Duration</p>
                            <p><?php  echo $jobData->shift; ?></p>
                          </div>
                          <!-- col -->
                          
                          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <p>Hours Per Week</p>
                            <p><?php  echo $jobData->hours_per_week; ?></p>
                          </div>
                          <!-- col --> 
                          
                        </div>
                        <!-- row --> 
                      </div>
                      <!-- interview-view-header -->
                      
				  <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'offer-form',
                        'enableAjaxValidation'=>false,
                    )); ?>
                      
                      
                      <div class="interview-view-header">
                        <h4>Billing & Rates</h4>
                        <br>
                        <div class="row">
                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <p>Job Request ID</p>
                            <p class="p-t-10"><?php  $request_dept = unserialize($jobData->request_dept); echo $request_dept[0]; ?></p>
                          </div>
                          <!-- col -->
                          
                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <p>Billing Code</p>
                            <p class="p-t-10">NULL</p>
                          </div>
                          <!-- col -->
                          
                          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <p>Rates</p>
                            <div class="three-flieds">
                            
                              <div class="form-group">
                                <?php echo $form->textField($model,'rates_regular',array('placeholder'=>'Regular Hours','class'=>'form-control')); ?>
								<?php echo $form->error($model,'rates_regular'); ?>
                              </div>
                              <div class="form-group">
                              	<?php echo $form->textField($model,'rates_extra',array('placeholder'=>'Extra Hours','class'=>'form-control')); ?>
								<?php echo $form->error($model,'rates_extra'); ?>
                              </div>
                              
                              <div class="form-group">
                                <?php echo $form->textField($model,'double_time',array('placeholder'=>'Double Time','class'=>'form-control')); ?>
								<?php echo $form->error($model,'double_time'); ?>
                              </div>
                              
                              <div class="form-group">
                                <div class="single">
                                  <?php 
								   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
								   echo $form->dropDownList($model, 'rates_status', $list , array('class'=>'ui fluid search dropdown','empty' => '')); 				
								   ?>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- col --> 
                          
                        </div>
                        <!-- row --> 
                      </div>
                      <!-- interview-view-header -->
                      
                      <div class="interview-view-content">
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                              <label for="">Internal Note</label>
                              <?php echo $form->textArea($model,'internal_notes',array('rows'=>3, 'cols'=>50,'class'=>'form-control')); ?>
							 <?php echo $form->error($model,'internal_notes'); ?>
                            </div>
                          </div>
                          <!-- col --> 
                        </div>
                        <!-- row -->
                        
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                              <label for="">Notes for MSP</label>
                              <?php echo $form->textArea($model,'for_admin_notes',array('rows'=>3, 'cols'=>50,'class'=>'form-control')); ?>
							 <?php echo $form->error($model,'for_admin_notes'); ?>
                            </div>
                          </div>
                          <!-- col --> 
                        </div>
                        <!-- row -->
                        <hr>
                        <div class="row">
                          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="two-flieds-2 p-t-10">
                              <div class="form-group has-feedback">
                                <label for="">Date of issue of Offer</label>
                                <input type="text" value="<?php echo date("m/d/Y", strtotime($model->issued_offer_date)) ?>" name="Offer[issued_offer_date]" id="dateField3" class="form-control dateField">
                                <i class=" fa  fa-calendar form-control-feedback"></i> </div>
                              <div class="form-group has-feedback">
                                <label for="">Validate of the Offer</label>
                                <input type="text" value="<?php echo date("m/d/Y", strtotime($model->valid_till_date)) ?>" name="Offer[valid_till_date]" id="dateField-end3" class="form-control dateField">
                                <i class=" fa  fa-calendar form-control-feedback fcf-2"></i> </div>
                            </div>
                          </div>
                          <!-- col --> 
                        </div>
                        <!-- row -->
                        
                        <hr>
                        <div class="row">
                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <p>Hiring Team: </p>
                            <p>Client: <?php echo $client->first_name.' '.$client->last_name; ?></p>
                            <p>MSP: David Webb, Monjo </p>
                            <br>
                            
                            <?php if($_GET['offerId']=='Null'){
							echo CHtml::submitButton($model->isNewRecord ? 'Send Offer' : 'Save',array('class'=>'btn btn-success'));
							}
							 ?>
                            <br>
                            <br>
                          </div>
                          <!-- col --> 
                        </div>
                        <!-- row --> 
                        
                      </div>
                    </div>
                    <!-- interview-view --> 
                    
                   <?php $this->endWidget(); ?>
                    
                    
                  </div>
                </div>
                <!-- interviews --> 
                <!-- page-content --> </div>
                
                <div class="quick-detail hide">
                    <?php $this->renderPartial('/job/quickData',array('jobData'=>$jobmodel)); ?>
                  </div>
                </div>
              <!-- page-content-outer -->
              
              <!-- page-sidebar -->
              <?php $this->renderPartial('/job/sidebar',array('jobData'=>$jobmodel,'invitedTeamMembers'=>$invitedTeamMembers)); ?>
              <!-- page-sidebar --> </div>
            <!-- db-content-wraper --> </div>
          <!-- page-wraper --> </div>
      </div>
      <!-- row --> </div>
  </div>
</div>
