<?php $this->pageTitle =  'On Boarding';?>

<div class="main-content">
  <div class="row"> 
    <!--Condensed Table--> 
    
    <!--Hover table-->
    <div class="col-sm-12">
      <div class="panel panel-default panel-table">
        <div class="panel-heading">On Boarding </div>
        <div class="panel-body">
          <table class="table table-striped table-hover">
            <thead>
            <thead>
              <tr>
                <th>Work ID</th>
                <th>Candidate Name</th>
                <th>Start Date</th>
                <th>Rate</th>
                <th>Job</th>
                <th>Status</th>
                <th style="text-align: center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($offers as $value) {
   $jobModel = Job::model()->findByPk($value['job_id']);
  ?>
              <tr>
                <td><a href="<?php //echo Yii::app()->createAbsoluteUrl('Admin/offer/view',array('id'=>$value['offer_id'])); ?>"><?php echo $value['offer_id'];?></a></td>
                <td><?php echo $value['first_name'].' '.$value['last_name'];?></td>
                <td><?php echo date('m/d/Y',strtotime($value['estimate_start_date']));?></td>
                <td><?php echo $value['rates_regular'].' '.$value['payment_type'];?></td>
                <td><a href="<?php //echo Yii::app()->createAbsoluteUrl('job/workspace',array('id'=>$jobModel->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Workspace"><?php echo $jobModel->title;?></a></td>
                <td>
				<?php 
			   $offerStatus = UtilityManager::workorderStatus();
			   $status = $offerStatus[$value['workorder_status']];
			
			   if($value['workorder_status'] == 1){
				$className ="success";
			   }else if($value['workorder_status'] == 2){
				   $className ="danger";
			   }if($value['workorder_status'] == 0){
				  $className ="info";
			   }else{
					$className ="info";
					} ?>
                  <span class="label label-<?php echo $className;?>"><?php echo  $status;?></span></td>
                <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('id'=>$value['offer_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Workspace"><i class="mdi mdi-eye"></i></a> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/deleteOffer',array('id'=>$value['offer_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" mdi mdi-delete"></i></i></a></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
