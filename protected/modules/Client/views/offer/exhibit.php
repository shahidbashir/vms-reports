<?php $this->renderPartial('overview_menu',array('offerModel'=>$offerModel));?>
  <div class="tab-content">
      <?php /*?><div class="row">
           <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'job-form',
            'enableAjaxValidation'=>false,
             'htmlOptions' => array('enctype' => 'multipart/form-data'),
          ));

      
      $jobModel = Job::model()->findByPk($offerModel->job_id);
      $clientExhibit = Yii::app()->db->createCommand("select * from vms_client_exhibit
 where client_id=".$jobModel->user_id)->query()->readAll();
       
     ?>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            
                <div class="form-group">
                  <label for="">Select Document</label>
                   <select name="OfferExhibit[exhibit_name]" class="form-control" required="required">
            <option value="">Select </option>
             <?php foreach($clientExhibit as $exhValue){
                 echo '<option value="'.$exhValue['id'].'">'.$exhValue['name'].'</option>'; 
               }?>
          </select>
                
                </div>
                    
          </div>

          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label>Upload</label>
               <?php echo $form->fileField($exhibitModel, 'file',array('class'=>'form-control'));?>
          </div>

          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              
              <div class="form-group">
                <label for="">&nbsp; </label>
                <p style="padding-top: 10px; ">
                <button type="submit" class="btn btn-success">Add</button></p>
              </div>
              
          </div>
          <?php $this->endWidget(); ?>
        </div><?php */?> <!-- row -->

        <div class="row">

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              
              <div class="table-default">
                
                
                <?php if($exhibitModelList){?>
                <div class="table-responsive">

                   <table class="table table-striped table-hover">
                    <thead>
                      <th>S.No</th>
                      <th>Date of Upload</th>
                      <th>Background Verification</th>
                      <th>Files</th>
                      
                      <th style="text-align: right; padding-right: 30px;" >
                        Actions
                      </th>

                    </thead>
                      <tbody>
                          <?php  $i=1;foreach($exhibitModelList as $value){ ?>
                        <tr>
                           <td><?php echo $i;$i++;?></td>
                          <td>
                            <?php echo date('m/d/Y',strtotime($value->date_created));?>
                          </td>

                          <td>
                             <?php echo $clientExhibit['name'];?>
                          </td>
                          <td>
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('offer/downloadExhibit',array('id'=>$value->id)); ?>"><?php echo $value->file;?></a>
                          </td>
                          
                          <td style="text-align: right; padding-right: 30px;" >
                             <a href="<?php echo Yii::app()->createAbsoluteUrl('offer/deleteExhibit',array('id'=>$value->id)); ?>" data-placement="top" data-toggle="tooltip" class=" delete" data-original-title="Delete" onclick="return confirm('Are you sure you want to delete this Exhibit?');"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                        <?php }?>

                    </tbody>
                  </table>

                </div>
                <?php } ?>

              </div>

          </div> <!-- col -->
        </div> <!-- row -->
        <br>

         <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 m-t-20 m-b-20">
           <?php $form=$this->beginWidget('CActiveForm');
		   if($offerModel->exhibit_status == 1){ ?>
          <a href="#" class="btn btn-success" disabled >Document Exhibit Completed</a>
          <?php }else{ ?>
           <button type="submit" name="completed" class="btn btn-success">Document Exhibit Completed</button>
            <?php } $this->endWidget(); ?>
          </div>
         </div>
      </div> <!-- exhibit -->
     
            