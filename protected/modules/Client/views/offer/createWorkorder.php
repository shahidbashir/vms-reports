<div class="main-content">
    <!--<div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Title!</strong> Alert body ...
    </div>
    <div class="alert alert-success" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Title!</strong> Alert body ...
    </div>-->
    <div class="row">
        <!--Condensed Table-->
        <!--Hover table-->
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border-color panel-border-color-primary">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="">Select Job</label>
                                        <select name="job_id" id="creatework_order" class="form-control" required>
                                            <option value=""></option>
                                            <?php foreach($jobData as $key=>$value){ ?>
                                                <option value="<?php echo $value->id ?>" <?php if(!empty($Job->id) && $value->id==$Job->id) { echo'selected'; } ?>><?php echo '('.$value->id.') '.$value->title ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Select Candidate</label>
                                        <select name="" id="input" class="form-control" required="required">
                                            <option value="">Select</option>
                                            <?php foreach($submissionData as $key=>$value){
                                            $candidateData = Candidates::model()->findByPk($value['candidate_Id']);
                                            echo '<option value="'.$value['id'].'">'.$candidateData->first_name.' '.$candidateData->last_name.'</option>';
                                            } ?>
                                        </select>
                                    </div>
                                    <hr class="m-t-30 m-b-30">
                                    <p class="m-b-10 bold">Personal Information</p>
                                    <div class="three-flieds">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Middle Name</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <!-- three-flieds -->
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="">Security ID</label>
                                                <input type="text" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                        <!-- col -->
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="">Confirm Security ID</label>
                                                <input type="text" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                        <!-- col -->
                                    </div>
                                    <!-- row -->
                                    <p class="m-b-10 bold">Other Information</p>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="">Available Date </label>
                                                <input type="text" class="form-control datetimepicker" id="" placeholder="">
                                            </div>
                                        </div>
                                        <!-- col -->
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Submitted to other job posting</label>
                                                <select name="" id="input" class="form-control" required="required">
                                                    <option value="">Select</option>
                                                    <option value="">Yes</option>
                                                    <option value="">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- col -->
                                    </div>
                                    <!-- row -->
                                    <div class="form-group">
                                        <label for="">Worker Pay Type</label>
                                        <select name="" id="input" class="form-control" required="required">
                                            <option value="">Select</option>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Resume / CV Upload</label>
                                        <input type="file" name="" id="input" class="form-control" value="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="">Comment</label>
                                        <textarea name="" id="input" class="form-control" rows="5" required="required"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="">Is this contractor working under a W2 directly with your company ?</label>
                                                <select name="" id="input" class="form-control" required="required">
                                                    <option value="">Select</option>
                                                    <option value="">Yes</option>
                                                    <option value="">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- col -->
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="">Is this contractor a US Citizen or US Permanent Resident ?</label>
                                                <select name="" id="input" class="form-control" required="required">
                                                    <option value="">Select</option>
                                                    <option value="">Yes</option>
                                                    <option value="">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- col -->
                                    </div>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="">Former Contractor for Client Name or one of its affiliates ?</label>
                                                <select name="" id="input" class="form-control" required="required">
                                                    <option value="">Select</option>
                                                    <option value="">Yes</option>
                                                    <option value="">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- col -->
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="">Reason for leaving last Employer</label>
                                                <input type="text" class="form-control" id="" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- row -->

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <table class="table sub-table table-condensed gray-table  submission-skill-table">
                                                <thead>
                                                <th>Primary Skill</th>
                                                <th>Year of Experience</th>
                                                <th>Rating</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(!empty($Job->skills)){
                                                $Primeryskills = explode(',',$Job->skills);

                                                foreach($Primeryskills as $value){
                                                ?>
                                                <tr>

                                                    <td><?php echo $value; ?></td>
                                                    <td>
                                                        <select name="" id="input" class="form-control" required="required">
                                                            <option value="">Select</option>
                                                            <option value="">2-4 Years</option>
                                                            <option value="">5-8 Years</option>
                                                            <option value="">9-12 Years</option>
                                                            <option value="">13-16 Years</option>
                                                            <option value="">17-20 Years</option>
                                                            <option value="">21-24 Years</option>
                                                            <option value="">25-28 Years</option>
                                                            <option value="">29-32 Years</option>
                                                            <option value="">33-36 Years</option>
                                                            <option value="">37-40 Years</option>
                                                            <option value="">41-44 Years</option>
                                                            <option value="">45-50 Years</option>
                                                        </select>
                                                    </td>
                                                    <td>

                                                        <select name="" id="input" class="form-control" required="required">
                                                            <option value="">Select</option>
                                                            <option value="">Beginners ( 0%-33% )</option>
                                                            <option value="">Intermediates ( 34%-67% )</option>
                                                            <option value="">Advanced ( 68%-99% )</option>
                                                        </select>
                                                    </td>

                                                </tr>
                                                <?php } }?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- col -->
                                    </div>
                                    <!-- row -->

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <table class="table sub-table table-condensed gray-table  submission-skill-table">
                                                <thead>
                                                <th>Secondary Skill</th>
                                                <th>Year of Experience</th>
                                                <th>Rating</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(!empty($Job->skills1)){
                                                $secondrykills = explode(',',$Job->skills1);

                                                foreach($secondrykills as $value){
                                                ?>
                                                <tr>
                                                    <td><?php echo $value; ?></td>
                                                    <td>
                                                        <select name="" id="input" class="form-control" required="required">
                                                            <option value="">Select</option>
                                                            <option value="">2-4 Years</option>
                                                            <option value="">5-8 Years</option>
                                                            <option value="">9-12 Years</option>
                                                            <option value="">13-16 Years</option>
                                                            <option value="">17-20 Years</option>
                                                            <option value="">21-24 Years</option>
                                                            <option value="">25-28 Years</option>
                                                            <option value="">29-32 Years</option>
                                                            <option value="">33-36 Years</option>
                                                            <option value="">37-40 Years</option>
                                                            <option value="">41-44 Years</option>
                                                            <option value="">45-50 Years</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="" id="input" class="form-control" required="required">
                                                            <option value="">Select</option>
                                                            <option value="">Beginners ( 0%-33% )</option>
                                                            <option value="">Intermediates ( 34%-67% )</option>
                                                            <option value="">Advanced ( 68%-99% )</option>
                                                        </select>
                                                    </td>

                                                </tr>
                                                <?php } } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- col -->
                                    </div>
                                    <!-- row -->

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <table class="table sub-table table-condensed gray-table  submission-skill-table">
                                                <thead>
                                                <th>Good to Have</th>
                                                <th>Year of Experience</th>
                                                <th>Rating</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(!empty($Job->skills2)){
                                                $Goodtohave = explode(',',$Job->skills2);

                                                foreach($Goodtohave as $value){
                                                ?>
                                                <tr>
                                                    <td><?php echo $value; ?></td>
                                                    <td>
                                                        <select name="" id="input" class="form-control" required="required">
                                                            <option value="">Select</option>
                                                            <option value="">Select</option>
                                                            <option value="">2-4 Years</option>
                                                            <option value="">5-8 Years</option>
                                                            <option value="">9-12 Years</option>
                                                            <option value="">13-16 Years</option>
                                                            <option value="">17-20 Years</option>
                                                            <option value="">21-24 Years</option>
                                                            <option value="">25-28 Years</option>
                                                            <option value="">29-32 Years</option>
                                                            <option value="">33-36 Years</option>
                                                            <option value="">37-40 Years</option>
                                                            <option value="">41-44 Years</option>
                                                            <option value="">45-50 Years</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="" id="input" class="form-control" required="required">
                                                            <option value="">Select</option>
                                                            <option value="">Beginners ( 0%-33% )</option>
                                                            <option value="">Intermediates ( 34%-67% )</option>
                                                            <option value="">Advanced ( 68%-99% )</option>
                                                        </select>
                                                    </td>

                                                </tr>
                                                <?php } } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- col -->
                                    </div>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <table class="table sub-table table-condensed add-submission-table">
                                                <tbody>
                                                <tr>
                                                    <td>Recommended Start Date</td>
                                                    <td>
                                                        <?php if(!empty($Job->desired_start_date)){ echo $Job->desired_start_date; } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Vendor Markup</td>
                                                    <td> <?php if(!empty($Job->markup)){ echo $Job->markup; } ?> </td>
                                                </tr>
                                                <tr>
                                                    <td>Job Mark Up Category</td>
                                                    <td>Technology</td>
                                                </tr>
                                                <tr>
                                                    <td>Recommended Pay Rate</td>
                                                    <td><input name="pay_rate" value="<?php if(!empty($Job->pay_rate)){ echo $Job->pay_rate; } ?>" type="text" class="form-control" id="" placeholder=""></td>
                                                </tr>
                                                <tr>
                                                    <td>Candidate Pay Rate</td>
                                                    <td><input type="text" name="candidate_pay_rate" value="" class="form-control" id="" placeholder=""></td>
                                                </tr>
                                                <tr>
                                                    <td>Over Time Rate</td>
                                                    <td><input type="text" name="over_time" value="<?php if(!empty($Job->over_time)){ echo $Job->over_time==1?'Yes':'No'; } ?>" class="form-control" id="" placeholder=""></td>
                                                </tr>
                                                <tr>
                                                    <td>Double Time Rate</td>
                                                    <td><input type="text" name="doubel_time" value="<?php if(!empty($Job->double_time)){ echo $Job->double_time==1?'Yes':'No'; } ?>" class="form-control" id="" placeholder=""></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- col -->
                                    </div>
                                    <!-- row -->
                                </div>
                                <br>
                                <br>
                                <div class="clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group m-t-30">
                                            <a href="" class="btn btn-success">Continue</a>
                                        </div>
                                    </div>
                                    <!-- col -->
                                </div>
                                <!-- row -->
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Hover table-->
</div>