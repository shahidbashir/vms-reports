<?php $this->pageTitle = 'Time Sheet Detail'; ?>

<div class="main-content">
  <div class="row"> 
    <!--Condensed Table-->
    
    <?php 
		$loginUserID = Yii::app()->user->id;
		$cpProjects = CpProjects::model()->findAllByAttributes(array('candidate_id'=>$loginUserID));
		$offerData = Offer::model()->findByAttributes(array('candidate_id'=>$loginUserID));
		
		if(isset(Yii::app()->session['selectTimesheet'])){		
			$week = Yii::app()->session['selectTimesheet']['week_number'];
			$year = date('Y');
			$weekStart_end_date = UtilityManager::getStartAndEndDate($week,$year);	
		}else{
			 $this->redirect(array('selectTimesheet'));
			 }
	?>
    
    <!--Hover table-->
    <div class="col-sm-12">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-heading"></div>
            <div class="panel-body">
              <div class="date-box">
                <table class="">
                  <tbody>
                    <tr>
                      <td><a href="" class=""><i class="fa fa-chevron-left"></i></a></td>
                      <td><input type="button" name="" id="input" class="form-control" value=" <?php echo date('M  d',strtotime($weekStart_end_date[0])); ?> - <?php echo date('M  d',strtotime($weekStart_end_date[1])); ?> , <?php echo date('Y'); ?>"></td>
                      <td><a href="" class=""><i class="fa fa-chevron-right"></i></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <br>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="week-view-table">
                    <div class="default-tabs" style="margin-right: -5px; margin-left: -5px;">
                      <div role="tabpanel"> 
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"> <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Regular Hour</a> </li>
                          <!--
                          <li role="presentation"> <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Time Off</a> </li>-->
                          <li role="presentation"> <a href="#tab2" aria-controls="tab" role="tab" data-toggle="tab">Over Time</a> </li>
                          <li role="presentation"> <a href="#tab3" aria-controls="tab" role="tab" data-toggle="tab">Double Time</a> </li>
                          <li role="presentation"> <a href="#tab4" aria-controls="tab" role="tab" data-toggle="tab"> Approval</a> </li>
                        </ul>
                        
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="home">
                            <form method="POST" id="regularHourForm">
                              <table class="table table-striped dataTable no-footer tableregularhr">
                                <thead>
                                  <tr>
                                    <th style=" width: 20%;">Projects</th>
                                    <th style=" width: 74px; text-align: center;">Mon</th>
                                    <th style=" width: 74px; text-align: center;">Tue</th>
                                    <th style=" width: 74px; text-align: center;">Wed</th>
                                    <th style=" width: 74px; text-align: center;">Thu</th>
                                    <th style=" width: 74px; text-align: center;">Fri</th>
                                    <th style=" width: 74px; text-align: center;">Sat</th>
                                    <th style=" width: 74px; text-align: center;">Sun</th>
                                    <th style=" width: 74px; text-align: center;">Pay Rate</th>
                                    <th style=" width: 100px; text-align: center;">Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <input type="hidden" name="offer_id" value="<?php //echo $offerData->id; ?>" />
                                <?php foreach($model as $modelKey=>$modelValues){
		  $project = CpProjects::model()->findByPk($modelValues->project_id);
		   ?>
                                <tr>
                                  <td ><select name="project_id[]" id="input" class="form-control" required="required" readonly="readonly">
                                      <option value=""><?php echo $project->project_name; ?></option>
                                    </select></td>
                                  <td><input type="text" name="mon[]" id="input" class="form-control days" value="<?php echo $modelValues->mon; ?>" readonly="readonly"></td>
                                  <td><input type="text" name="tue[]" id="input" class="form-control days" value="<?php echo $modelValues->tue; ?>" readonly="readonly"></td>
                                  <td><input type="text" name="wed[]" id="input" class="form-control days" value="<?php echo $modelValues->wed; ?>" readonly="readonly"></td>
                                  <td><input type="text" name="thu[]" id="input" class="form-control days" value="<?php echo $modelValues->thu; ?>" readonly="readonly"></td>
                                  <td><input type="text" name="fri[]" id="input" class="form-control days" value="<?php echo $modelValues->fri; ?>" readonly="readonly"></td>
                                  <td><input type="text" name="sat[]" id="input" class="form-control days" value="<?php echo $modelValues->sat; ?>" readonly="readonly"></td>
                                  <td><input type="text" name="sun[]" id="input" class="form-control days" value="<?php echo $modelValues->sun; ?>" readonly="readonly"></td>
                                  <td><input type="text" name="pay_rate[]" id="input" class="form-control payrate" value="<?php echo $modelValues->pay_rate; ?>" readonly="readonly" readonly="readonly"></td>
                                  <td ><input type="text" name="total[]" id="total" class="form-control text-right" value="<?php echo $modelValues->total; ?>" readonly="readonly"></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                
                              </table>
                              <p class="m-t-20"> 
                                <!--<button type="button" name="regular_time" class="btn btn-success">Submit Timesheet</button>--> 
                              </p>
                            </form>
                          </div>
                          <div role="tabpanel" class="tab-pane" id="tab">
                            <table class="table table-striped dataTable no-footer tableofftime">
                              <thead>
                                <tr>
                                  <th style=" width: 20%;">Job</th>
                                  <th style=" width: 74px; text-align: center;">Mon</th>
                                  <th style=" width: 74px; text-align: center;">Tue</th>
                                  <th style=" width: 74px; text-align: center;">Wed</th>
                                  <th style=" width: 74px; text-align: center;">Thu</th>
                                  <th style=" width: 74px; text-align: center;">Fri</th>
                                  <th style=" width: 74px; text-align: center;">Sat</th>
                                  <th style=" width: 74px; text-align: center;">Sun</th>
                                  <th style=" width: 74px; text-align: center;">Pay Rate</th>
                                  <th style=" width: 100px; text-align: center;">Total</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td style=" width: 20%;"><select name="" id="input" class="form-control" required="required">
                                      <option value="">Select a leave type</option>
                                      <option value="">Paid Leave</option>
                                      <option value="">Non Paid Leave</option>
                                    </select></td>
                                  <td style=" width: 74px; text-align: center;"><input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title=""></td>
                                  <td style=" width: 74px; text-align: center;"><input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title=""></td>
                                  <td style=" width: 74px; text-align: center;"><input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title=""></td>
                                  <td style=" width: 74px; text-align: center;"><input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title=""></td>
                                  <td style=" width: 74px; text-align: center;"><input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title=""></td>
                                  <td style=" width: 74px; text-align: center;"><input type="text" name="" id="input" class="form-control " value="" required="required" pattern="" title=""></td>
                                  <td style=" width: 74px; text-align: center;"><input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title=""></td>
                                  <td style=" width: 74px; text-align: center;"><input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title=""></td>
                                  <td style=" width: 100px; text-align: center;" ><input type="text" name="" id="input" class="form-control text-right" value="0.00" required="required" pattern="" title=""></td>
                                </tr>
                              </tbody>
                            </table>
                            <p class="m-b-20 m-t-20"><a href="#" class="btn btn-default" onClick="addRow(this, 'tableofftime')">Add Row</a></p>
                            <p class="m-t-20">
                              <button type="button" class="btn btn-success">Save</button>
                            </p>
                          </div>
                          <!-- end tab -->
                          
                          <div role="tabpanel" class="tab-pane" id="tab2">
                          <table class="table table-striped dataTable no-footer overtimetable">
                            <thead>
                              <tr>
                                <th style=" width: 20%;">Job</th>
                                <th style=" width: 74px; text-align: center;">Mon</th>
                                <th style=" width: 74px; text-align: center;">Tue</th>
                                <th style=" width: 74px; text-align: center;">Wed</th>
                                <th style=" width: 74px; text-align: center;">Thu</th>
                                <th style=" width: 74px; text-align: center;">Fri</th>
                                <th style=" width: 74px; text-align: center;">Sat</th>
                                <th style=" width: 74px; text-align: center;">Sun</th>
                                <th style=" width: 74px; text-align: center;">Pay Rate</th>
                                <th style=" width: 100px; text-align: center;">Total</th>
                              </tr>
                            </thead>
                            <tbody>
      <input type="hidden" name="offer_id" value="<?php //echo $offerData->id; ?>" />
      
      <?php foreach($overTime as $modelKey=>$modelValues){
		  $project = CpProjects::model()->findByPk($modelValues->project_id);
		   ?>
        <tr>
          <td ><select name="project_id[]" id="input" class="form-control" required="required" readonly="readonly">
              <option value=""><?php echo $project->project_name; ?></option>
            </select>
            </td>
          <td><input type="text" name="mon[]" id="input" class="form-control days" value="<?php echo $modelValues->mon; ?>" readonly="readonly"></td>
          <td><input type="text" name="tue[]" id="input" class="form-control days" value="<?php echo $modelValues->tue; ?>" readonly="readonly"></td>
          <td><input type="text" name="wed[]" id="input" class="form-control days" value="<?php echo $modelValues->wed; ?>" readonly="readonly"></td>
          <td><input type="text" name="thu[]" id="input" class="form-control days" value="<?php echo $modelValues->thu; ?>" readonly="readonly"></td>
          <td><input type="text" name="fri[]" id="input" class="form-control days" value="<?php echo $modelValues->fri; ?>" readonly="readonly"></td>
          <td><input type="text" name="sat[]" id="input" class="form-control days" value="<?php echo $modelValues->sat; ?>" readonly="readonly"></td>
          <td><input type="text" name="sun[]" id="input" class="form-control days" value="<?php echo $modelValues->sun; ?>" readonly="readonly"></td>
          <td><input type="text" name="pay_rate[]" id="input" class="form-control payrate" value="<?php echo $modelValues->pay_rate; ?>" readonly="readonly"></td>
          <td ><input type="text" name="total[]" id="total" class="form-control text-right" value="<?php echo $modelValues->total; ?>" readonly="readonly"></td>
        </tr>
      <?php } ?>
      </tbody>
                          </table>
                        </div>
                          <!-- end tab -->
                            
                          <div role="tabpanel" class="tab-pane" id="tab3">
                              <table class="table table-striped dataTable no-footer doubletime">
                                <thead>
                                  <tr>
                                    <th style=" width: 20%;">Job</th>
                                    <th style=" width: 74px; text-align: center;">Mon</th>
                                    <th style=" width: 74px; text-align: center;">Tue</th>
                                    <th style=" width: 74px; text-align: center;">Wed</th>
                                    <th style=" width: 74px; text-align: center;">Thu</th>
                                    <th style=" width: 74px; text-align: center;">Fri</th>
                                    <th style=" width: 74px; text-align: center;">Sat</th>
                                    <th style=" width: 74px; text-align: center;">Sun</th>
                                    <th style=" width: 74px; text-align: center;">Pay Rate</th>
                                    <th style=" width: 100px; text-align: center;">Total</th>
                                  </tr>
                                </thead>
                                <tbody>
          <input type="hidden" name="offer_id" value="<?php //echo $offerData->id; ?>" />
          
          <?php foreach($doubleTime as $modelKey=>$modelValues){
              $project = CpProjects::model()->findByPk($modelValues->project_id);
               ?>
            <tr>
              <td ><select name="project_id[]" id="input" class="form-control" required="required" readonly="readonly">
                  <option value=""><?php echo $project->project_name; ?></option>
                </select>
                </td>
              <td><input type="text" name="mon[]" id="input" class="form-control days" value="<?php echo $modelValues->mon; ?>" readonly="readonly"></td>
              <td><input type="text" name="tue[]" id="input" class="form-control days" value="<?php echo $modelValues->tue; ?>" readonly="readonly"></td>
              <td><input type="text" name="wed[]" id="input" class="form-control days" value="<?php echo $modelValues->wed; ?>" readonly="readonly"></td>
              <td><input type="text" name="thu[]" id="input" class="form-control days" value="<?php echo $modelValues->thu; ?>" readonly="readonly"></td>
              <td><input type="text" name="fri[]" id="input" class="form-control days" value="<?php echo $modelValues->fri; ?>" readonly="readonly"></td>
              <td><input type="text" name="sat[]" id="input" class="form-control days" value="<?php echo $modelValues->sat; ?>" readonly="readonly"></td>
              <td><input type="text" name="sun[]" id="input" class="form-control days" value="<?php echo $modelValues->sun; ?>" readonly="readonly"></td>
              <td><input type="text" name="pay_rate[]" id="input" class="form-control payrate" value="<?php echo $modelValues->pay_rate; ?>" readonly="readonly"></td>
              <td ><input type="text" name="total[]" id="total" class="form-control text-right" value="<?php echo $modelValues->total; ?>" readonly="readonly"></td>
            </tr>
          <?php } ?>
          </tbody>
                              </table>
                            </div>
                          <!-- end tab -->
                          
                          
                          
                          <div role="tabpanel" class="tab-pane" id="tab4">
                          	<?php $form=$this->beginWidget('CActiveForm', array(
									'id'=>'client-form',
									'enableAjaxValidation'=>false,
								)); ?>
                              <div class="form-group">
                                                             
                                <label for="">Reason for Time Sheet Rejection :</label>
                                
								<?php 
                                $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=48','order'=>'id DESC')),'title', 'title');
                                echo $form->dropDownList($model1, 'rejection_reason',$list, array('class'=>'form-control','empty' => 'Select Reason'));
								?>
                              </div>
                              <div class="form-group">
                                <label for="">Note :</label>
                                <textarea name="CpTimesheetLog[notes]" id="input" class="form-control" rows="3"></textarea>
                              </div>
                              
                              <?php if($model1->status == 0){ ?>
                              <div class="form-group">
                                <button type="submit" name="approve" class="btn btn-success">Approve</button>
                                <button type="submit" name="reject" class="btn btn-danger">Reject</button>
                              </div>
                              
                              <?php  } $this->endWidget(); ?>
                              <hr>
                              
                              
                              
                              <!--Rejected timesheet history-->
                              
                              <?php if($history){ ?>
                              <p class="bold m-b-10">Rejected Time Sheet</p>
							  <?php foreach($history as $val){
								  if($val->status==2){
                                                            $historyDetail = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$val->id,'type'=>'Regular Hour'),array('order'=>'id desc'));
                                                            $historyDetail1 = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$val->id,'type'=>'Over Time'),array('order'=>'id desc'));
                                                            $historyDetail2 = CpTimesheet::model()->findAllByAttributes(array('timesheet_log_id'=>$val->id,'type'=>'Double Time'),array('order'=>'id desc'));
                                                             ?>
                              <div class="row">
                                <div class="col-xs-12 col-sm-4 ">
                                  <p class="bold m-b-10">Date of Rejection</p>
                                  <p><?php echo date('m-d-Y',strtotime($val->rejection_date)); ?></p>
                                </div>
                                <!-- col -->
                                
                                <div class="col-xs-12 col-sm-4 ">
                                  <p class="bold m-b-10">Reason for Rejection</p>
                                  <p><?php echo $val->rejection_reason; ?></p>
                                </div>
                                <!-- col -->
                                
                                <div class="col-xs-12 col-sm-4 ">
                                  <p class="bold m-b-10">Posted by</p>
                                  <p>Null</p>
                                </div>
                                <!-- col --> 
                                
                              </div>
                              <!-- row --> 
                              <br>
                              <div class="row">
                                <div class="col-xs-12 col-sm-12 ">
                                  <p class="bold m-b-10">Note / Comments</p>
                                  <p><?php echo $val->notes; ?></p>
                                </div>
                                <!-- col --> 
                              </div>
                              <!-- row --> 
                              <br>
                              <br>
                              <p><strong>Regular Hours</strong></p>
                              <br />
                              <table class="table table-striped dataTable no-footer">
                                <thead>
                                  <tr>
                                    <th>S.No </th>
                                    <th style="text-align: left">Projects</th>
                                    <th style="text-align: center">Mon</th>
                                    <th style="text-align: center">Tue</th>
                                    <th style="text-align: center">Wed</th>
                                    <th style="text-align: center">Thu</th>
                                    <th style="text-align: center">Fri</th>
                                    <th style="text-align: center">Sat</th>
                                    <th style="text-align: center">Sun</th>
                                    <th style="text-align: center">Pay Rate </th>
                                    <th style="text-align: center">Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($historyDetail as $detailKey=>$detailVal){ ?>
                                  <tr>
                                    <td><?php echo $detailKey + 1; ?></td>
                                    <td><?php $projects = CpProjects::model()->findByPk($modelValues->project_id);
                            echo $projects->project_name; ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->mon ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->tue ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->wed ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->thu ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->fri ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->sat ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->sun ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->pay_rate ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->total ?></td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                              <hr>
                              <p><strong>Over Time</strong></p>
                              <br />
                              <table class="table table-striped dataTable no-footer">
                                <thead>
                                  <tr>
                                    <th>S.No </th>
                                    <th style="text-align: left">Projects</th>
                                    <th style="text-align: center">Mon</th>
                                    <th style="text-align: center">Tue</th>
                                    <th style="text-align: center">Wed</th>
                                    <th style="text-align: center">Thu</th>
                                    <th style="text-align: center">Fri</th>
                                    <th style="text-align: center">Sat</th>
                                    <th style="text-align: center">Sun</th>
                                    <th style="text-align: center">Pay Rate </th>
                                    <th style="text-align: center">Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($historyDetail1 as $detailKey=>$detailVal){ ?>
                                  <tr>
                                    <td><?php echo $detailKey + 1; ?></td>
                                    <td><?php $projects = CpProjects::model()->findByPk($modelValues->project_id);
                            echo $projects->project_name; ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->mon ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->tue ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->wed ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->thu ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->fri ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->sat ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->sun ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->pay_rate ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->total ?></td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                              <hr>
                              <p><strong>Double Time</strong></p>
                              <br />
                              <table class="table table-striped dataTable no-footer">
                                <thead>
                                  <tr>
                                    <th>S.No </th>
                                    <th style="text-align: left">Projects</th>
                                    <th style="text-align: center">Mon</th>
                                    <th style="text-align: center">Tue</th>
                                    <th style="text-align: center">Wed</th>
                                    <th style="text-align: center">Thu</th>
                                    <th style="text-align: center">Fri</th>
                                    <th style="text-align: center">Sat</th>
                                    <th style="text-align: center">Sun</th>
                                    <th style="text-align: center">Pay Rate </th>
                                    <th style="text-align: center">Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($historyDetail2 as $detailKey=>$detailVal){ ?>
                                  <tr>
                                    <td><?php echo $detailKey + 1; ?></td>
                                    <td><?php $projects = CpProjects::model()->findByPk($modelValues->project_id);
                            echo $projects->project_name; ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->mon ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->tue ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->wed ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->thu ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->fri ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->sat ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->sun ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->pay_rate ?></td>
                                    <td style="text-align:center"><?php echo $detailVal->total ?></td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                              <hr>
                              <?php  } } } ?>
                              
                              
                              
                              
                              
                            </div>
                          <!-- end tab --> 
                          
                        </div>
                      </div>
                    </div>
                    <!-- default-tabs --> 
                    
                  </div>
                  <br>
                  <br>
                </div>
                <!-- col --> 
              </div>
              <!-- row --> 
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!--Hover table--> 
    
  </div>
</div>
