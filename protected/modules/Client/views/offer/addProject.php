<?php $this->renderPartial('overview_menu',array('offerModel'=>$workOrderModel)); ?>

<?php 
	/*echo '<pre>';
	print_r($timeSheetCodes);
	exit;*/
?>
<div class="tab-content">
<div class="panel-body">
  <h3>Project Table</h3>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Project Name</th>
        <th>Department</th>
        <th>Cost Center</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($projects as $projects){
		$costCenter = CostCenter::model()->findByPk($projects->cost_center);
		 ?>
      <tr>
        <td><?php echo $projects->project_name; ?></td>
        <td><?php echo $costCenter->department; ?></td>
        <td><?php echo $costCenter->cost_code; ?></td>
      </tr>
    <?php } ?>
    </tbody>
  </table>
  <br>
  <h3>Cost Center Table</h3>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Cost Center Code</th>
        <th>Department</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($costCenters as $costCenters){ ?>
      <tr>
        <td><?php echo $costCenters->cost_code; ?></td>
        <td><?php echo $costCenters->department; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  <br>
  <h3>TimeSheet Code Table</h3>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
      	<th>TimeSheet Code</th>
        <th>Project Name</th>
        <th>Cost Center</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($timeSheetCodes as $timeSheetCodes){
		  $project = Project::model()->findByPk($timeSheetCodes->project_id);
		  $costCenter = CostCenter::model()->findByPk($projects->cost_center);
		   ?>
      <tr>
        <td><?php echo $timeSheetCodes->time_code; ?></td>
        <td><?php echo $project->project_name; ?></td>
        <td><?php echo $costCenter->cost_code; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  <br>
</div>
