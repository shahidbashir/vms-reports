<?php $this->pageTitle =  'WorkOrders'; ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List of Work Order <!--<a href=""  class="pull-right"><i class="fa fa-question"></i></a> <a  class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
      <p class="m-b-20">A list of work order which is created for candidate who have accept the offer.</p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
      <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="sdata" placeholder="Search WorkOrder">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <div class="row m-b-20">
        <form name="search" method="post" action="">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="two-fields-70-30">
              <?php $job = Job::model()->findAllByAttributes(array('user_id'=>UtilityManager::superClient(Yii::app()->user->id))); ?>
              <div class="form-group form-control-2">
                <label for="" style="text-align: left;">Sort by Job Name</label>
                <select name="job_id" id="input" class="form-control  select2" required>
                  <option value="">Select</option>
                  <?php foreach($job as $value){ ?>
                  <option value="<?php echo $value->id; ?>"><?php echo $value->title.'('.$value->id.')'; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="" >&nbsp; </label>
                <p>
                  <button type="submit" name="jobId" class="btn btn-success">Show</button>
                </p>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="">
        <div  class="tab-pane active"  id="list-view">
          <table class="table m-b-40 without-border" >
            <thead class="thead-default">
              <tr>
                <th  style="width: 10px;">Status</th>
                <th style="width: 140px;"> Work Order ID </th>
                <th>Job ID</th>
                <th>Candidate Name</th>
                <th>Vendor Name</th>
                  <th>Start Date</th>
                <th> Bill Rate </th>
                <th style="width: 157px;">Location of Work</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="subdata">
              <?php if($workorders){			  
					$i = 0;
					foreach($workorders as $value) {
						//removing this b/c rohit said
                        /*if(date('m-d-Y',strtotime($value['onboard_changed_start_date']))=='01-01-1970'){
                            $onboardStartDate = date('F d,Y',strtotime($value['wo_start_date']));
                        }else{
                            $onboardStartDate = date('F d,Y',strtotime($value['onboard_changed_start_date']));
                        }*/
						$onboardStartDate = date('F d,Y',strtotime($value['wo_start_date']));
						$i++;
					$jobModel = Job::model()->findByPk($value['job_id']);
					$vendor = Vendor::model()->findByPk($value['vendor_id']);
					$offerStatus = UtilityManager::workorderStatus();
					$offerdata = Offer::model()->findByPk($value['offer_id']);
					$location = Location::model()->findByPk($offerdata->approver_manager_location);

					if($value['workorder_status'] == 1){
						$className ="label-hold";
					}else if($value['workorder_status'] == 2){
						$className ="label-pending-aproval";
					}elseif($value['workorder_status'] == 0){
						$className ="label-new-request";
					}else{
						$className ="info";
					}
					?>
              <tr>
                <td><span class="tag <?php echo $className;?>"><?php echo  $offerStatus[$value['workorder_status']];?></span></td>
                <td><a href=""><?php echo $value['workorder_id'];?></a></td>
                <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobModel->id,array('type'=>'info')); ?>"><?php echo $jobModel->id;?></a></td>
                <td><a href=""><?php echo $value['first_name'].' '.$value['last_name'];?></a></td>
                <td><?php echo $vendor->organization; ?></td>
                  <td><?php echo $onboardStartDate; ?></td>
                <td><?php echo '$ '.$value['wo_bill_rate']; ?></td>
                <td><?php echo $location->name; ?></td>
                <td style="text-align: center" class="actions"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/workorderView',array('id'=>$value['workorder_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a> 
                  <!--<a href="#" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg"></i></a>--></td>
              </tr>
              <?php } }else{ ?>
              <tr>
                <td colspan="7">Sorry no record found...</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- list-view -->
        
        <!--<div  class="tab-pane"  id="stack-view">
          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>-->
        <!-- list-view --> 
        
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
    <?php if(Yii::app()->controller->action->id=='workorderList'){ ?>
  <div class="clearfix" style="padding: 10px 20px 10px; ">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <?php
		$this->widget('CLinkPager', array(
			'pages' => $pages,
			'header' => '',
			'nextPageLabel' => 'Next',
			'prevPageLabel' => 'Prev',
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination m-t-0',
			)
		))
		?>
    </div>
  </div>
    <?php } ?>
  <div class="seprater-bottom-100"></div>
</div>
<script>
    $(document).ready(function(){
        /*$("input").keydown(function(){
         $("input").css("background-color", "yellow");
         });*/
        $('#sdata').keyup(function(){
            var subValue=$(this).val();
            $.ajax({
                'url':'<?php echo $this->createUrl('costCenter/workorderSearch') ?>',
                type: "POST",
                data: { subValue: subValue},
                'success':function(html){
                    //alert(html);
                    $("#subdata").html(html)
                }
            });
            //$("input").css("background-color", "pink");
        });
    });
</script>