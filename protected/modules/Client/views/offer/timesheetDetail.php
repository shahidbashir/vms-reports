<?php 
	$this->pageTitle = 'TimeSheet View';
	$timesheetDetails = CpTimesheetDetails::model()->findAllByAttributes(array('timesheet_id'=>$model->id),array('order'=>'id asc'));
	$projectDetail = CpTimesheetProjectdetails::model()->findAllByAttributes(array('timesheet_id'=>$model->id));
?>
 <div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix timesheet-log" style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    
    <?php if(Yii::app()->user->hasFlash('success')):?>
		<?php echo Yii::app()->user->getFlash('success'); ?>
    <?php endif; ?>
    
    <h4 class="m-b-10">Time Sheet of the following month</h4>
    <p class="m-b-10"><?php echo date('m-d-Y',strtotime($model->invoice_start_date)).' to '.date('m-d-Y',strtotime($model->invoice_end_date)); ?></p>
      <?php
      $candidate = Candidates::model()->findByPk($model->candidate_id);
      $offer = Offer::model()->findByPk($model->offer_id);
      $location = Location::model()->findByPk($offer->approver_manager_location);
      ?>
      <p class="m-b-20">Candidate : <?php echo $candidate->first_name.' '.$candidate->last_name; ?></p>
      <p class="m-b-40">Location : <?php echo $location->name; ?></p>
    <?php if($model->timesheet_status==0){ ?>
    <div class="alert alert-warning">
     <strong>Waiting</strong> For Approval
    </div>
    <?php } if($model->timesheet_status==1){ ?>
    <div class="alert alert-success">
     <strong>Approved</strong> at <?php echo $model->approve_date_time; ?>  
    </div>
    <?php } if($model->timesheet_status==2){ ?>
    <div class="alert alert-danger">
     <strong>Rejected</strong> <br />
     <strong>Reason: </strong><?php echo $model->reason_of_rejection; ?><br />
     <strong>Notes: </strong><?php echo $model->rejection_notes; ?><br />
    </div>
    <?php } ?>
    
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <table class="table  without-border time-sheet-table">
            <thead class="thead-default">
              <tr>
                <th>Date</th>
                <th>Start Day</th>
                <th>Type</th>
                <th>TimeSheet Code</th>
                <th>Hours</th>
                <th>Pay Rate( Total )</th>
                <th>Bill Rate( Total )</th>
                <!--<th>Total Bill Rate</th>-->
              </tr>
            </thead>
            <?php foreach($timesheetDetails as $key=>$value){
		switch ($value->hours_type) {
					case "Regular Hour":
						$class = 'tag label-new-request';
						break;
					case "Over Time":
						$class = 'tag label-re-open';
						break;
					case "Double Time":
						$class = 'tag label-pending-aproval';
						break;
					default:
						$class = 'tag label-pending-aproval';
				}
			
			//showing client project and code on the type of client
			if($clientType == 1){
			  $project = Project::model()->findByPk($value->project_code_id);
			  $name = $project->project_name;
			}else{
				 $timesheetCode = TimeSheetCode::model()->findByPk($value->project_code_id);
				 $name = $timesheetCode->time_code;
				 }
		 ?>
            <tbody>
              <tr>
                 <td><?php echo date('m-d-Y',strtotime($value->creation_day)); ?></td>
                  <td><?php echo date('l',strtotime($value->creation_day)); ?></td>
                  <td><span class="<?php echo $class; ?>"><?php echo $value->hours_type; ?></span></td>
                  <td><strong><?php echo $name; ?></strong></td>
                  <td><?php echo $value->no_of_hours; ?> Hrs </td>
                  <td><?php echo $value->total_payrate; ?> $ </td>
                  <td><?php echo $value->total_billrate; ?> $ </td>
                 <!-- <td><?php /*echo $value->total_payrate + $value->total_billrate; */?> $ </td>-->
              </tr>
            </tbody>
            <?php } ?>
          </table>
          <br>
          <p class="bold m-b-10">Cost Estimate</p>
          <table class="table  without-border">
            <thead class="thead-default">
              <tr>
                <th>Time</th>
                <th>Hours</th>
                <th>Pay Rate</th>
                <th>Bill Rate</th>
                <?php /*?><th>Total</th><?php */?>
              </tr>
            </thead>
            <tbody>
              <?php if($model->total_regular_hours){ ?>
              <tr>
                <td> Regular Hour </td>
                <td><?php echo $model->total_regular_hours; ?></td>
                <td> $<?php echo $model->total_regular_payrate; ?> ( $<?php echo $model->regular_payrate; ?> per hour ) </td>
                <td> $<?php echo $model->total_regular_billrate; ?> ( $<?php echo $model->regular_billrate; ?> per hour ) </td>
                <?php /*?><td> <?php echo $model->total_regular_billrate; ?> </td><?php */?>
              </tr>
              <?php  } if($model->total_overtime_hours){ ?>
              <tr>
                <td> Over Time </td>
                <td><?php echo $model->total_overtime_hours; ?></td>
                <td> $<?php echo $model->total_overtime_payrate; ?> ( $<?php echo $model->overtime_payrate; ?> per hour ) </td>
                <td> $<?php echo $model->total_overtime_billrate; ?> ( $<?php echo $model->overtime_billrate; ?> per hour ) </td>
                <?php /*?><td> <?php echo $model->total_overtime_billrate; ?> </td><?php */?>
              </tr>
              <?php } if($model->total_doubletime_hours){ ?>
              <tr>
                <td> Double Time </td>
                <td><?php echo $model->total_doubletime_hours; ?></td>
                <td> $<?php echo $model->total_doubletime_payrate; ?> ( $<?php echo $model->doubletime_payrate; ?> per hour ) </td>
                <td> $<?php echo $model->total_doubletime_billrate; ?> ( $<?php echo $model->doubletime_billrate; ?> per hour ) </td>
                <?php /*?><td> <?php echo $model->total_doubletime_billrate; ?> </td><?php */?>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <table class="table  without-border">
            <tbody>
              <tr>
                <td style="width: 230px;"> Total Cost </td>
                <td class="text-left"><?php echo $model->total_hours; ?></td>
                <td class="text-left">$<?php echo $model->total_payrate; ?></td>
                <td class="text-left">$<?php echo $model->total_billrate; ?></td>
                <!--<td class="text-center" style="padding-left: 12px;"> 30 </td>--> 
              </tr>
            </tbody>
          </table>
          
          <br>
          <?php if($projectDetail){ ?>
          <p class="bold m-b-10">Project Cost Estimate</p>
          <table class="table  without-border">
            <thead class="thead-default">
              <tr>
                <th>Project Name</th>
                <th>Regualr Hours</th>
                <th>OverTime Hours</th>
                <th>DoubleTime Hours</th>
                <th>Pay Rate</th>
                <th>Bill Rate</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($projectDetail as $valueP){
				  if($clientType == 1){
					$project = Project::model()->findByPk($valueP->project_code_id);
					$name = $project->project_name;
					}else{
						 $timesheetCode = TimeSheetCode::model()->findByPk($valueP->project_code_id);
						 $name = $timesheetCode->time_code;
						 }
				   ?>
              <tr>
                <td><?php echo $name; ?></td>
                <td><?php echo $valueP->total_regular_hours; ?></td>
                <td> <?php echo $valueP->total_overtime_hours; ?></td>
                <td> <?php echo $valueP->total_doubletime_hours; ?></td>
                <td> $<?php echo $valueP->total_payrate; ?></td>
                <td> $<?php echo $valueP->total_billrate; ?></td>
              </tr>
              <?php  }  ?>
            </tbody>
          </table>
          <?php } ?>
          
          <?php if($model->timesheet_status==0){ ?>
          <form action="" method="POST" role="form">
              <button name="approve" class="btn btn-success">Accept TimeSheet</button>
              <a href="#modal-id" data-toggle="modal" class="btn btn-danger">Reject TimeSheet</a>
          </form>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Reject TimeSheet</h4>
        </div>
        <form action="" method="POST" role="form">
        <div class="modal-body">


          <div class="form-group">
            <label for="">Reason for Rejection</label>
            <?php $setting = Setting::model()->findAll(array('condition'=>'category_id=48'));
            ?><select name="reason_for_rejection" id="input" class="form-control" value="" required  title="">
            	<option value="">Select Reason</option>
              <?php foreach($setting as $value){ echo "<option value='".$value->title."'>".$value->title."</option>";} ?>
              </select>
          </div>


          <div class="form-group">
            <label for="">Note</label>
            <textarea name="rejection_notes" id="input" class="form-control" rows="3" required></textarea>
          </div>

        </div>
        <div class="modal-footer">

          <button type="submit" name="reject" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
          </form>
      </div>
    </div>
  </div>