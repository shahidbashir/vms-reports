
<?php $this->renderPartial('overview_menu',array('offerModel'=>$workOrderModel));?>
  <div class="row">
    <div class="row m-0">
      <div class="col-md-12">
        <!--error messages code-->
        <div class="panel panel-default panel-table">
          <div class="panel-body">
            
            <?php /*?><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              
              <?php if($workOrderModel->workorder_status == 2){ ?>
              <p class="bold m-b-10">WorkOrder Rejection</p>
              <div class="well well-yellow">
                <p><strong>Reason:</strong> <?php echo $workOrderModel->reason_rejection_workorder; ?></p>
                <p><strong>Note :</strong> <?php echo $workOrderModel->notes_workorder; ?></p>
              </div>
              <?php }  ?>
            </div><?php */?>
            
            <?php $action = $this->action->id;?>
            <div class="default-tabs">
              <div role="tabpanel"> 
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="offer-detail">
                    <table class="table table-hover no-table-border">
                      <thead>
                        <tr>
                          <th>Candidate Name</th>
                          <th>Location</th>
                          <th>Start Date</th>
                          <th>End Date</th>
                          <th>Location Approving Manager</th>
                          <!--<th>Time Sheet Approving</th>-->
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><?php echo $workOrders['first_name'].' '.$workOrders['last_name'];?></td>
                          <td>
						  <?php 
						  $location = Location::model()->findByPk($offerData->approver_manager_location);
						  //echo $offerData->approver_manager_location;
						  //var_dump($location);
						  echo $location->name ;?>
                          </td>
                          <td><?php echo date('m-d-Y',strtotime($offerData->job_start_date)); ?></td>
                          <td><?php echo date('m-d-Y',strtotime($offerData->job_end_date)); ?></td>
                          <td><?php echo $offerData->location_manager; ?></td>
                          
                          <?php /*?><td><?php $client = Client::model()->findByPk($offers['approval_manager']);
                    			echo isset($client)?$client->first_name.' '.$client->last_name:'NULL';

                    		?></td><?php */?>
                          
                        </tr>
                      </tbody>
                    </table>
                    <br>
                    <hr>
                    <br>
                    <h4 class=" m-b-20" style="padding-left: 20px; ">Bill Rate</h4>
                    <table class="table table-striped dataTable no-footer">
                      <thead>
                      	<!--<th> Pay Rate </th>-->
                        <th> Bill Rate </th>
                        <th> Shift </th>
                        <th> Shift Hour </th>
                        <th> Over Time Rate </th>
                        <th> Double Time Rate </th>
                          </thead>
                      <tbody>
                        <tr>
                          <?php /*?><td>$ <?php echo $offerModel->rates_regular;?></td><?php */?>
                          <td>$ <?php echo $workOrderModel->wo_bill_rate; //.' '.$workOrderModel->payment_type;?></td>
                          <td><?php echo $offerData->shift;?></td>
                          <td><?php echo $offerData->hours_per_week;?></td>
                          <td><?php echo $workOrderModel->wo_client_over_time;?></td>
                          <td><?php echo $workOrderModel->wo_client_double_time;?></td>
                        </tr>
                      </tbody>
                    </table>
                    <br />
                    
                    <h4 class=" m-b-20" style="padding-left: 20px; ">Pay Rates</h4>
                    <table class="table table-striped dataTable no-footer">
                      <thead>
                      	<th> Pay Rate </th>
                        <!--<th> Bill Rate </th>-->
                        <th> Shift </th>
                        <th> Shift Hour </th>
                        <th> Over Time Rate </th>
                        <th> Double Time Rate </th>
                          </thead>
                      <tbody>
                        <tr>
                          <td>$ <?php echo $workOrderModel->wo_pay_rate;?></td>
                          <?php /*?><td>$ <?php echo $workOrderModel->offer_bill_rate; //.' '.$workOrderModel->payment_type;?></td><?php */?>
                          <td><?php echo $offerData->shift;?></td>
                          <td><?php echo $offerData->hours_per_week;?></td>
                          <td><?php echo $workOrderModel->wo_over_time;?></td>
                          <td><?php echo $workOrderModel->wo_double_time;?></td>
                        </tr>
                      </tbody>
                    </table>
                    
                    
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 m-t-20 m-b-20">
                      
                        <p class="bold m-b-10">Internal Note</p>
                        <div class="well">
                          <p>
                            <?php echo $workOrderModel->wo_internal_notes; ?>
                          </p>
                        </div>
                        <p class="bold m-b-10">MSP Note</p>
                        <div class="well">
                          <p>
                            <?php echo  $workOrderModel->wo_msp_notes; ?>
                          </p>
                        </div>
                        <p class="bold m-b-10">Vendor Note</p>
                        <div class="well">
                          <p>
                            <?php echo  $workOrderModel->wo_vendor_notes; ?>
                          </p>
                        </div>
                        <?php $form=$this->beginWidget('CActiveForm', array(
                 'id'=>'vendor-form',
                 'enableAjaxValidation'=>false,
             )); ?>
                        <div class="row">
                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                              <label for="">WorkOrder Status</label>
                              <?php 
      
				  $list = UtilityManager::workorderStatus();
				  			
				  echo $form->dropDownList($workOrderModel,'workorder_status', $list,
						  array('empty' => 'Change Status','class'=>'form-control','required'=>'required','options' => array($offerModel->workorder_status=>array('selected'=>true)))); ?>
                              <?php echo $form->error($workOrderModel,'status'); ?> </div>
                          </div>
                          <!-- col -->
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label>&nbsp;</label>
                            <p style="margin-top: 5px;">
                              <button type="submit" class="btn  btn-primary">Update</button>
                              <a href="#modal-id" data-toggle="modal" class="btn btn-danger">Reject WorkOrder</a>
                            </p>
                          </div>
                        </div>
                        <!-- row -->
                        
                        <?php $this->endWidget(); ?>
                        
                      </div>
                    </div>
                    <!-- row --> 
                  </div>
                  <!-- offer-detail --> 
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>



<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Reject WorkOrder</h4>
        </div>
        <form action="" method="POST" role="form">
        <div class="modal-body">


          <div class="form-group">
            <label for="">Reason for Rejection</label>
            <?php $setting = Setting::model()->findAll(array('condition'=>'category_id=51'));
            ?><select name="reason_for_rejection" id="input" class="form-control" value="" required  title="">
              <?php foreach($setting as $value){ echo "<option value='".$value->title."'>".$value->title."</option>";} ?>
              </select>
          </div>


          <div class="form-group">
            <label for="">Note</label>
            <textarea name="notes" id="input" class="form-control" rows="3" required></textarea>
          </div>

        </div>
        <div class="modal-footer">

          <button type="submit" name="rejected" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
          </form>
      </div>
    </div>
  </div>
  
  
  
  
  
  
