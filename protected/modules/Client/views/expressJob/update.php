<?php
/* @var $this ExpressJobController */
/* @var $model ExpressJob */

$this->breadcrumbs=array(
	'Express Jobs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ExpressJob', 'url'=>array('index')),
	array('label'=>'Create ExpressJob', 'url'=>array('create')),
	array('label'=>'View ExpressJob', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ExpressJob', 'url'=>array('admin')),
);
?>

<h1>Update ExpressJob <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>