<?php
/* @var $this ExpressJobController */
/* @var $model ExpressJob */

$this->breadcrumbs=array(
	'Express Jobs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ExpressJob', 'url'=>array('index')),
	array('label'=>'Create ExpressJob', 'url'=>array('create')),
	array('label'=>'Update ExpressJob', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ExpressJob', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ExpressJob', 'url'=>array('admin')),
);
?>

<h1>View ExpressJob #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'user_type',
		'job_title',
		'bill_rate',
		'location',
		'status',
		'date_created',
		'date_updated',
	),
)); ?>
