<script>
  function myFunction() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      td1 = tr[i].getElementsByTagName("td")[1];
      td2 = tr[i].getElementsByTagName("td")[2];
      td3 = tr[i].getElementsByTagName("td")[3];
      td4 = tr[i].getElementsByTagName("td")[4];
      if (td || td1 || td2 || td3 || td4 ) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        }else if(td1.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td2.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td3.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }else if(td4.innerHTML.toUpperCase().indexOf(filter) > -1){
          tr[i].style.display = "";
        }
        else {
          tr[i].style.display = "none";
        }

      }
    }
  }
</script>
<?php $this->pageTitle='Express Jobs'; $loginUserId = Yii::app()->user->id;  //echo '<pre>';print_r($expressJobs); exit; ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List of Express Jobs <!--<a href="" class="pull-right"><i class="fa fa-question"></i></a> <a href="" class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
      <p class="m-b-20">A List of Express Job request created.</p>
      <div class="m-b-30"> <a data-toggle="modal" href='#express-jobs' class="btn btn-sm btn-default-2">Add Express Job</a> </div>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search Jobs">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <table id="myTable" class="table m-b-40 without-border">
        <thead class="thead-default">
          <tr>
            <th style="width: 6%">Status</th>
            <th >Request ID</th>
            <th >Job Title</th>
            <th>Location</th>
            <th>Bill Rate</th>
            <th class="actions">Actions</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach($expressJobs as $expressJobs):
		
			$postingstatus = UtilityManager::expressJobStatus();
			switch ($postingstatus[$expressJobs->status]) {
                    case "Open":
                        $class = 'tag label-new-request';
                        break;
                    case "Published":
                        $class = 'tag label-re-open';
                        break;
                    default:
                        $class = 'tag label-hold';
                }
		 ?>
         <div class="modal fade" id="view-express-job<?php echo $expressJobs->id ?>">
           <div class="modal-dialog">
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Job View</h4>
               </div>
               <div class="modal-body">
                 <p class="bold m-t-10">Job Title</p>
                 <p class=" m-t-10"><?php echo $expressJobs->job_title; ?></p>
                 <p class=" bold m-t-10">Summary</p>
                 <p class=" m-t-10"><?php echo $expressJobs->job_description; ?></p>
                 <p class="bold m-t-10">Bill Rate</p>
                 <p class=" m-t-10">$<?php echo $expressJobs->bill_rate; ?></p>
                 <p class="bold m-t-10">Location</p>
                 <p class=" m-t-10"><?php $locations = Location::model()->findAllByAttributes(array('id'=>explode(',',$expressJobs->location))); foreach($locations as $location){
                     echo $location->name.', ';
                   }  ?></p>
               </div>
               <div class="modal-footer m-t-20">
               	<?php if($expressJobs->job_id != 0){ ?>
                 <!--<button type="button" class="btn btn-success">View Job</button>-->
                 <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview',array('id'=>$expressJobs->job_id,'type'=>'info')); ?>" class="btn btn-success">View Job</a>
                 <?php } ?>
               </div>
             </div>
           </div>
        </div>
          <tr>
            <td><span class="<?php echo $class ?>"><?php echo $postingstatus[$expressJobs->status] ?></span></td>
            <td><?php echo $expressJobs->id; ?></td>
            <td><a data-toggle="modal" href='#view-express-job<?php echo $expressJobs->id ?>' data-placement="top" class="underline"><?php echo $expressJobs->job_title ?></a> </td>
            <?php $locations = Location::model()->findAllByAttributes(array('id'=>explode(',',$expressJobs->location))); ?>
            <td>
				<?php foreach($locations as $location){
					echo $location->name.', ';
				} ?>
            </td>
            <td>$<?php echo $expressJobs->bill_rate ?></td>
            <td class="actions"><a data-toggle="modal" href='#view-express-job<?php echo $expressJobs->id ?>' data-placement="top" title="View" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="clearfix" style="padding: 10px 20px 10px; ">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <?php
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header' => '',
		'nextPageLabel' => 'Next',
		'prevPageLabel' => 'Prev',
		'selectedPageCssClass' => 'active',
		'hiddenPageCssClass' => 'disabled',
		'htmlOptions' => array(
			'class' => 'pagination m-t-0',
		)
	))
	?>
      
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      
    </div>
  </div>
  
  <div class="seprater-bottom-100"></div>
</div>
<div class="modal fade" id="express-jobs">
  <div class="modal-dialog" style="max-width:800px !important">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add Express Job</h4>
      </div>
      <div class="modal-body"> 
        
        <!-- widget content -->
        <div class="widget-body">
          <div class="row">
          <?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'wizard-1',
				'enableAjaxValidation'=>false,
			)); ?>
            <!--<form id="wizard-1" novalidate="novalidate">-->
              <div id="bootstrap-wizard-1" class="col-sm-12">
                <div class="form-bootstrapWizard">
                  <ul class="bootstrapWizard form-wizard">
                    <li data-target="#step1" class="active first" > <a href="#tab1" data-toggle="tab" class="active"> <span class="step">1</span></a> </li>
                    <li data-target="#step2" class="second"> <a href="#tab2" data-toggle="tab"> <span class="step two">2</span></a> </li>
                    <li data-target="#step3" class="third"> <a href="#tab3" data-toggle="tab"> <span class="step">3</span></a> </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab1">
                    <div class="form-group">
                      <label for="">Job Tilte</label>
                      <?php echo $form->textField($model,'job_title',array('size'=>60,'maxlength'=>555,'class'=>'form-control','id'=>"autocomplete")); ?>
                    </div>
                    <div class="form-group">
                      <label for="">Description</label>
                      <textarea name="ExpressJob[job_description]" id="job_description" class="textarea wysihtml5 form-control editorText" style="width: 100%; min-height: 250px"></textarea>
                      <!--<textarea name="ExpressJob[job_description]" id="job_description" class="form-control" rows="3" required="required"></textarea>-->
                    </div>
                    <div class="text-center"> <a href="#tab1" data-target="#tab2" data-toggle="tab" class="btn btn-success"> Continue </a> </div>
                  </div>
                  <div class="tab-pane" id="tab2">
                    <div class="form-group">
                      <label for="">Select the Location</label>
                      <select multiple  class="form-control  select2" name="ExpressJob[location][]" required >
                          <option value=""></option>
                          <?php
                          $location = Location::model()->findAllByAttributes(array('reference_id'=>UtilityManager::superClient(Yii::app()->user->id)));
                          foreach($location as $location){ ?>
                          <option value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                          <?php  } 	?>
                        </select>
                    </div>
                    <div class="text-center"> <a href="#tab2" data-target="#tab3" data-toggle="tab" class="btn btn-success"> Continue </a> </div>
                  </div>
                  <div class="tab-pane" id="tab3">
                    <div class="form-group">
                      <label for="">Bill Rate</label>
                      <?php echo $form->numberField($model,'bill_rate',array('class'=>'form-control','min'=>'.1','step' => "any",'required'=>'required')); ?>
                    </div>
                    <div class="form-group">
                      <label for="">Estimated Job Start Date </label>
                      <input type="text" class="form-control daterange" name="express_job_s_date" id="daterange"  required="required">
                    </div>
                    <?php /*?><div class="form-group">
                        <label for="">Job End Date</label>
                        <input type="text" class="form-control daterange" name="job_po_duration_endDate" id="daterange1" placeholder=""  required="required">
                    </div><?php */?>
                    <div class="form-group">
                      <label for="">Target Closing Date for Job</label>
                      <input type="text" class="form-control" name="express_job_e_date" required="required" id="dateField">
                    </div>
                    <div class="text-center"> 
                    	<?php echo CHtml::submitButton($model->isNewRecord ? 'Save Job' : 'Save Job',array('class'=>'btn btn-success')); ?>
                    </div>
                  </div>
                </div>
              </div>
            <!--</form>-->
            <?php $this->endWidget(); ?>
          </div>
        </div>
        <!-- end widget content --> 
        
      </div>
    </div>
  </div>
</div>

<?php //$this->jobTitles(); ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(document).on('click', 'a[href="#tab1"]' , function(event) {
      event.preventDefault();
      $('ul.bootstrapWizard').find('li').removeClass('active')
      $('ul.bootstrapWizard li.second').addClass('active'); 
    });
    $(document).on('click', 'a[href="#tab2"]' , function(event) {
      event.preventDefault();
       $('ul.bootstrapWizard').find('li').removeClass('active')
      $('ul.bootstrapWizard li.third').addClass('active'); 
    });
	
	
	$('#autocomplete').autocomplete({
		lookup: <?php $this->jobTitles(); ?>,
		onSelect: function (suggestion) {
			//$('#job_description').val(suggestion.data);
			//$('.editorText').html(suggestion.data);
			
			var editorObj = $("#job_description").data('wysihtml5');
			var editor = editorObj.editor;
			editor.setValue(suggestion.data);
			
			//alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
		}
	});
	
	
  });
	

</script> 
