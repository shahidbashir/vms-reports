<?php
/* @var $this ExpressJobController */
/* @var $model ExpressJob */

$this->breadcrumbs=array(
	'Express Jobs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ExpressJob', 'url'=>array('index')),
	array('label'=>'Manage ExpressJob', 'url'=>array('admin')),
);
?>

<h1>Create ExpressJob</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>