<?php
$verification = Setting::model()->findAllByAttributes(array('category_id'=>45));
$clientData = Client::model()->findByPk(Yii::app()->user->id);

$verificationValues = explode(",",$clientData->backgroundverification);
/*print_r($verificationValues);
exit;*/
?>
<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Background Verification</h4>
      <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>

      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <?php if(Yii::app()->user->hasFlash('error')):?>
        <?php echo Yii::app()->user->getFlash('error'); ?>
      <?php endif; ?>
      <form id="client-form" action="" method="post">

        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


            <div class="form-group">
              <?php foreach($verification as $key => $data){ ?>
              <div class="be-checkbox">
                <input id="check<?php echo $key; ?>" <?php echo in_array($data['id'],$verificationValues)?'checked="checked"':''; ?> name="check[<?php echo $key; ?>]" value="<?php echo $data['id']; ?>" type="checkbox">
                <label for="check<?php echo $key; ?>"><?php echo $data['title']; ?></label>
              </div>
              <?php } ?>
            </div>


          </div> <!-- col -->


        </div> <!-- row -->


        <button type="submit" name="Backgroundverification" class="btn btn-success">Save</button>
        <a href="" class="btn btn-default">Cancel</a>
      </form><br>



    </div> <!-- col -->
    
  </div> <!-- row -->
  <div class="seprater-bottom-100"></div>
  <div class="seprater-bottom-100"></div>
  <div class="seprater-bottom-100"></div>
  <div class="seprater-bottom-100"></div>
</div>
