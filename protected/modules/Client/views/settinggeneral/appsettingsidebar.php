<?php
$this->pageTitle= 'Settings';
?>

<div class="col-lg-3 p-a-0 hidden-md-down messages-sidebar scroll-y flexbox-xs full-height">
  <div class="p-a-1">
    <nav>
      <ul class="nav nav-pills nav-stacked m-b-1">
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/index'); ?>">Company Profile</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createdepartment'); ?>">Department</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/createteammember'); ?>">Team Members</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createjobrequest'); ?>">Job Request</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createbillingcode'); ?>">Bill Code</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/costCenter/create'); ?>">Cost Center</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/categoryConfiguration'); ?>">Category Configuration</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/workflows'); ?>">Work Flow</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/contractWorkFlows'); ?>">Contract Work Flow</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/workflowConfiguration'); ?>">Work Flow - Configuration</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/workflowType'); ?>">Work Flow - Type</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/project/create'); ?>">Project</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/clientbaseOn'); ?>">Project / TimeSheet Client Setting</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/workHourconfig'); ?>">Work Hour configuration</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/settingExhibit'); ?>">Exhibit</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/backgroundverification'); ?>">Backgroud Verification</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createlocation'); ?>">Location</a> </li>
        <!--<li class="nav-item"><a class="nav-link" href="#">Job Alerts</a></li>-->
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/timesheetConfig'); ?>">Time Hour Configuraiton</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/extension'); ?>">Extension</a> </li>
        <!--<li class="nav-item"><a class="nav-link" href="#">Organizational Chart</a>
                </li>-->
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/timeSheetcode'); ?>">Time Sheet Code</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/expenses'); ?>">Expenses</a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/logReport'); ?>">Show Log</a>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/addCName'); ?>">Client Name</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/notificationAlert'); ?>">Notification Alert</a></li>
         <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/addLocation'); ?>">Client Location Configuraiton</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/digitalDocs/index'); ?>">Digital Template</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/importRecord'); ?>">Import Data</a></li>
      </ul>
    </nav>
  </div>
</div>
