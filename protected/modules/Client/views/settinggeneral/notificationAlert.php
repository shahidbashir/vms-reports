<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Notification Alert</h4>
      <p class="m-b-40"></p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
      )); ?>
        <div class="form-group">
          <label for="">Select Type</label>
          <select name="type" id="input" class="form-control" required="required">
              <option value="">Select</option>
             <option value="1">Offer Accept</option>
             <option value="2">Work Order Accept</option>
             <option value="3">Onboarded</option>
           </select>
        </div>
        <div class="form-group">
          <label for="">Name</label>
          <input value="" class="form-control" required="required" name="user_name" id="user_name" type="text"> 
   
        </div>
        <div class="form-group">
          <label for="">Email Address</label>
          <input value="" class="form-control" required="required" name="email_addres" id="email_addres" type="text"> 
         </div>
        <button type="submit" name="add_alert" class="btn btn-success">Add</button>
        <a href="" class="btn btn-default">Cancel</a>
      <?php $this->endWidget(); ?>
    </div> <!-- col -->
    
  </div> <!-- row -->
  <div class="clearfix p-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-30 table-heading">List of Client Location Configuration</h4>
      <?php if($notifications) { ?>
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
          <tr>
            <th>Sr. No:</th>
            <th>Name</th>
            <th>Email Address</th>
            <th>Type</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
        <?php
       		$i = 0;
        	foreach($notifications as $notification) {
         	 $i++;
			 $type = $notification['type'];
          ?>
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php echo $notification['name']; ?></td>
          <td><?php echo $notification['email']; ?></td>
          <td><?php  if($type == 1)  echo "Offer Accept"; elseif($type == 2) echo "Work Order Accept"; else echo  "Onboarded";?></td>
          <td class="text-center">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deleteNotificationAlert',array('id'=> $notification['id']))?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
      <?php } ?>
    </div> <!-- col -->
  </div> <!-- row -->
 <div class="seprater-bottom-100"></div>
</div>
<div class="modal fade" id="bsModal3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="mySmallModalLabel">Saved</h4>
      </div>
      <div class="modal-body">
        Your setting has been saved...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
