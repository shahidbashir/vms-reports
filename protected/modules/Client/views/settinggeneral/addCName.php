<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Clients</h4>
      <p class="m-b-40"></p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
      )); ?>
        <div class="form-group">
          <label for="">Client Name</label>
          <input type="text" class="form-control" name="name" pattern="[a-zA-z &-]{2,50}"  required>
        </div>
        <br>
        <button type="submit" name="submit" class="btn btn-success">Add Client</button>
        <a href="" class="btn btn-default">Cancel</a>
      <?php $this->endWidget(); ?>
    </div> <!-- col -->
    
  </div> <!-- row -->
  <div class="clearfix p-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-30 table-heading">List of Clients</h4>
      <?php if($record) { ?>
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
        <tr>
          <th>S.No &nbsp;&nbsp;&nbsp;&nbsp;Client Name</th>
          <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach($record as $value) {
          $i++;
          ?>
        <tr>
          <td><?php echo $i .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$value->name; ?></td>
          <td class="text-center">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deleteCName',array('id'=>$value->id))?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
      <?php } ?>
    </div> <!-- col -->
  </div> <!-- row -->
  <div class="clearfix p-20">
  <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
  <div class="form-group">
    <label for="">Enable For Job</label>
    <select name="client_job_status" id="client_job_status" class="form-control">
      <option value="Yes" <?php if($clientInfo->client_job_status=='Yes'){ echo 'Selected'; } ?> >Yes</option>
      <option value="No" <?php if($clientInfo->client_job_status=='No'){ echo 'Selected'; } ?> >No</option>
    </select>
  </div>
    </div>
    </div>
  <div class="seprater-bottom-100"></div>
</div>
<div class="modal fade" id="bsModal3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="mySmallModalLabel">Saved</h4>
      </div>
      <div class="modal-body">
        Your setting has been saved...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
