<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px; ">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h4 class="m-b-10"> Work Flow - Configuration </h4>

            <p class="m-b-40"></p>

            <?php if(Yii::app()->user->hasFlash('success')):?>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            <?php endif; ?>

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'project-form',
                'enableAjaxValidation'=>false,
            )); ?>

                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        <div class="form-group">
                            <label for="">Select Category</label>
                            <?php
                            $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'title', 'title');
                            echo $form->dropDownList($model, 'category', $list , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                        </div>

                    </div> 
                    <!-- col -->
                    
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        <div class="form-group">
                            <label for="">Team Member</label>
                            <select class="form-control" name="WorkflowConfiguration[member_id]">
                            	<option></option>
                                <?php foreach($teamMembers as $teamMembers){ ?>
                                	<option value="<?php echo $teamMembers->id; ?>"><?php echo $teamMembers->first_name.' '.$teamMembers->last_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                    <!-- col -->

                </div> <!-- row -->

                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        <div class="form-group">
                            <label for="">Start Range</label>
                            <?php echo $form->numberField($model,'start_rang',array('class'=>'form-control','required'=>'required','min' => 0)); ?>
                        </div>

                    </div> <!-- col -->

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        <div class="form-group">
                            <label for="">End Range</label>
                            <?php echo $form->numberField($model,'end_rang',array('class'=>'form-control','required'=>'required','min' => 0)); ?>
                        </div>

                    </div> <!-- col -->

                </div> <!-- row -->

                <div class="row">
                
                	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        <div class="form-group">
                            <label for="">Select Work Order</label>
                            <?php
                            $list = CHtml::listData(Worklflow::model()->findAll("client_id=".Yii::app()->user->id),'id', 'flow_name');
                            echo $form->dropDownList($model, 'workflow_id', $list , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                        </div>

                    </div> 
                    <!-- col -->

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        <div class="form-group">
                            <label for="">Duration of Approval</label>
                            <?php echo $form->numberField($model,'duration',array('class'=>'form-control','required'=>'required','min' => 0)); ?>
                        </div>

                    </div> <!-- col -->

                </div> <!-- row -->

                <button type="submit" class="btn btn-success">Add Work Flow Configuration</button>
                <a href="" class="btn btn-default">Cancel</a>
            <?php $this->endWidget(); ?>


        </div> <!-- col -->
        
    </div> <!-- row -->
    <div class="clearfix p-20">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <h4 class="m-b-20 table-heading">Work Flow - Configurations</h4>
            <?php if($WorkflowConfiguration){
            ?>
            <table class="table m-b-40 without-border">
                <thead class="thead-default">
                <tr>
                    <th>Work Flow</th>
                    <th>Category</th>
                    <th>Team Member</th>
                    <th>Budget Range</th>
                    <th>Duration</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach($WorkflowConfiguration as $value){
                $workflow = Worklflow::model()->findByPk($value['workflow_id']);
                ?>
                <tr>

                    <td>
                        <?php  if(!empty( $workflow->flow_name )) { 
						echo $workflow->flow_name ;
						} ?>
                    </td>

                    <td>
                        <?php echo $value['category']; ?>
                    </td>
                    <td>
                        <?php 
						
						$clientMember = Client::model()->findByPk($value['member_id']);
						if($clientMember)
						echo $clientMember->first_name.' '.$clientMember->last_name; ?>
                    </td>
                    <td>
                        <?php echo '$ '.$value['start_rang'].'  to  $ '.$value['end_rang']; ?>
                    </td>

                    <td>
                        <?php echo $value['duration']; ?>
                    </td>

                    <td style="text-align: center">
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deleteWorkflowConfiguration',array('id'=>$value['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                    </td>


                </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php } ?>
        </div> <!-- col -->
    </div> <!-- row -->
    <div class="seprater-bottom-100"></div>
</div>
