<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Bill Code</h4>
      <p class="m-b-40"></p>

      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>

      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
      )); ?>
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Billing Code</label>
              <input type="text" name="BillingcodeDepartment[billingcode]" class="form-control" required="required" pattern="[A-Za-z 0-9 -]{1,50}" title="Bill Code must be letter digit and -">
              <?php echo $form->error($model,'email'); ?>
            </div>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Department</label>
              <?php
              $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=4 or client_id='.Yii::app()->user->id)),'title', 'title');
              echo $form->dropDownList($model, 'department',$list, array('class'=>'form-control','empty' => '','required'=>'required')); ?>
            </div>
          </div>
          <!-- col -->
        </div>
        <!-- row -->
        <br>
        <button type="submit" class="btn btn-success">Add Bill Code</button>
      <button type="reset" class="btn btn-default">Cancel</button>
      <?php $this->endWidget(); ?>



    </div> <!-- col -->
    
  </div> <!-- row -->
  <?php if($modelBillingcodeDepartment) { ?>
  <div class="clearfix p-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-30 table-heading">List of Billing Code</h4>
      <table class="table m-b-40 without-border">

        <thead class="thead-default">
        <tr>
          <th>Billing Code & Department</th>

          <th class="text-center">Action</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach( $modelBillingcodeDepartment as $value ) { ?>
        <tr>
          <td>
            <p><?php echo $value->billingcode;?> </p>
            <span class="cell-detail-description"><?php echo $value->department;?></span>
          </td>
          <td style="text-align: center; padding-right: 30px;">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deletebillingcode',array('id'=>$value->id))?>" data-toggle="popover" data-placement="bottom" data-content="Delete" class="delete" data-original-title="" title=""><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
    </div> <!-- col -->
  </div> <!-- row -->
  <?php } ?>
  <div class="seprater-bottom-100"></div>
</div>

