<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px; ">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h4 class="m-b-10">Job Category Configuration</h4>
            <p class="m-b-40"></p>

            <?php if(Yii::app()->user->hasFlash('success')):?>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            <?php endif; ?>

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'project-form',
                'enableAjaxValidation'=>false,
            )); ?>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Select Job Category</label>
                            <?php
                            $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'title', 'title');
                            echo $form->dropDownList($model, 'category_id', $list , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                        </div>
                    </div>
                    <!-- col -->
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Select Cost Center </label>
                            <select name="cost_center[]" id="categoryconfigs" multiple="multiple" class="select2" style="width: 100%;" required="required">
                                <?php $costcenter = CostCenter::model()->findAllByAttributes(array('client_id'=>Yii::app()->user->id));
                                foreach($costcenter as $value){
                                    ?>
                                    <option value="<?php echo $value['cost_code'] ?>"><?php echo $value['cost_code'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
                <br>
                <button type="submit" class="btn btn-success">Add</button>
                <a href="" class="btn btn-default">Cancel</a>
            <?php $this->endWidget(); ?>


        </div> <!-- col -->


    </div> <!-- row -->
    <?php if($categoryConfiguration) {?>
    <div class="clearfix p-20">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <h4 class="m-b-30 table-heading">List Job Category Configuration</h4>
            <table class="table m-b-40 without-border">
                <thead class="thead-default">
                <tr>
                    <th >S.No</th>
                    <th>Job Category Name</th>
                    <th>Cost Center Code</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach($categoryConfiguration as $configurationData){
                $i++;
                ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>

                    <td>
                        <?php echo $configurationData['category_id']; ?>
                    </td>

                    <td>
                        <?php $costcenter = explode(',',$configurationData['cost_center']);
                        foreach($costcenter as $value){
                            if($value){
                            ?>
                            <span class="label label-default"><?php echo $value; ?></span>
                        <?php } } ?>
                    </td>

                    <td style="text-align: center; padding-right: 30px;">
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deletecategoryconfig',array('id'=>$configurationData['id'])); ?>" data-toggle="popover" data-placement="bottom" data-content="Delete" class="delete" data-original-title="" title=""><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php } ?>
                </tbody>
            </table>

        </div> <!-- col -->
    </div> <!-- row -->
    <?php } ?>
    <div class="seprater-bottom-100"></div>
</div>
