<!-- db-setting-sidebar -->
  <?php
    $clientModel = Client::model()->findByPk(Yii::app()->user->id);
   ?>
  
  <ul class="list-unstyled">
  <?php if($clientModel->super_client_id == 0){ ?>
    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/index'); ?>">Company Profile</a></li>
    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/createdepartment'); ?>">Departments</a></li>
    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/createlocation'); ?>">Location</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/createteammember'); ?>">Invite Team Members</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/createjobrequest'); ?>">Job Request Type</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/createbillingcode'); ?>">Operating Billing Code</a></li>
    <?php } ?>
    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/editProfile'); ?>">Edit Profile</a></li>
    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/changPassword'); ?>">Change Password</a></li>
    <?php if($clientModel->super_client_id == 0){ ?>
    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/workflows'); ?>">Approval Workflow</a></li>
    <?php } ?>
  </ul>
