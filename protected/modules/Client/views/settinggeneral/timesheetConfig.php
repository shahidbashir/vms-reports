<?php 
	$this->renderPartial('appsettingsidebar');
	$clientID = Yii::app()->user->id;
	$mainclient = Yii::app()->user->id;
	$client = Client::model()->findByPk($mainclient);
	if($client->member_type!=NULL){
		$client = Client::model()->findByPk($client->super_client_id);
		$mainclient = $client->id;
	}
 ?>

<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Time Sheet Configurtion</h4>
      <p class="m-b-40"></p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <form id="client-form" action="" method="post">
        <div class="row">
          <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <div class="form-group">
              <label for="">Select Location & Rule</label>
              <select name="location" id="input" class="form-control" required="required">
                <option value="">Select</option>
                <?php
				  $location = Location::model()->findAllByAttributes(array('reference_id'=>$mainclient));
				  foreach($location as $location){ ?>
				  <option value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
				 <?php  } 	?>
              </select>
            </div>
          </div>
          <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
            <div class="form-group">
              <label for="">&nbsp;</label>
              <select name="type" id="input" class="form-control" required="required">
                <option value="">Select</option>
                <option value="daily">Maximum Allowed Daily Hours</option>
                <option value="weekly">Maximum Allowed Weekly Hours</option>
              </select>
            </div>
          </div>
        </div>
        <br>
        <button type="submit" name="first_form" class="btn btn-success" >Continue</button>
      </form>
    </div>
    <!-- col -->
      <?php if($model){ ?>
      <div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
          <div class="cleafix " style="padding: 30px 20px; ">
      <table class="table m-b-40 without-border">
          <thead class="thead-default">
          <tr>
              <th>S.No</th>
              <th>Location</th>
              <th>Type</th>
              <th>Hours</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          $i = 0;
          foreach ($model as $modeldata) {
              $i++;
              ?>
          <tr>
              <td>
                <?php echo $i; ?>
              </td>
              <td>
                  <?php echo $modeldata->location_name; ?>
              </td>
              <td>
                  <?php echo $modeldata->hours_type; ?>
              </td>
              <td>
                  <?php echo $modeldata->total_hours; ?>
              </td>
          </tr>
          <?php } ?>
          </tbody>
      </table>
              </div>
          </div>
      <?php } ?>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <!--<h4 class="m-b-10">Excepteur sint</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>-->
    </div>
    <!-- col --> 
  </div>
  <!-- row -->
  
  <div class="seprater-bottom-100"></div>
  <div class="seprater-bottom-100"></div>
  <div class="seprater-bottom-100"></div>
</div>
