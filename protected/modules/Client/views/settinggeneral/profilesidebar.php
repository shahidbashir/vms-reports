<div class="col-lg-3 p-a-0 hidden-md-down messages-sidebar scroll-y flexbox-xs full-height">
  <div class="p-a-1">
    <nav>
      <ul class="nav nav-pills nav-stacked m-b-1">
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/editProfile'); ?>">My Profile</a> </li>
        <li class="nav-item"><a class="nav-link" href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/changPassword'); ?>">Change Password</a> </li>
      </ul>
    </nav>
  </div>
</div>
