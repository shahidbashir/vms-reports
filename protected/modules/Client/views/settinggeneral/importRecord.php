<?php $this->renderPartial('appsettingsidebar');
$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    <?php endif; ?>
    <div class="clearfix p-20">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-b-30 table-heading">List of Imported Data</h4>
            <table class="table m-b-40 without-border" >
                <thead class="thead-default">
                <tr>
                    <!--<th  style="width: 10px;">Status</th>-->
                    <!--<th style="width: 150px;"> Work Order ID </th>-->
                    <th style="width: 150px;"> Contract ID </th>
                    <th>Candidate</th>
                    <th>Date of Joining</th>
                    <th>Location</th>
                    <th>Job </th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if($models){
                    foreach($models as $model){
                        //print_r($model);
                        $candidates = Candidates::model()->findByPk($model->candidate_id);
                        $workorder = Workorder::model()->findByPk($model->workorder_id);
                        $jobs = Job::model()->findByPk($model->job_id);
                        $allTeamMembers = Yii::app()->db->createCommand('SELECT * FROM `vms_client` Where super_client_id='.$jobs->user_id.' or (id='.$loginUserId.' and super_client_id=0)')->query()->readAll();
                        $offer = Offer::model()->findByPk($model->offer_id);
                        $location = Location::model()->findByPk($offer->approver_manager_location);
                        $workorderStatus = UtilityManager::workorderStatus();
                        if($workorder->workorder_status == 1){
                            $className ="label-re-open";
                        }else if($workorder->workorder_status == 2){
                            $className ="label-pending-aproval";
                        }elseif($workorder->workorder_status == 0){
                            $className ="label-rejected";
                        }elseif($workorder->workorder_status == 3){
                            $className ="label-re-open";
                        }else{
                            $className ="label-new-request";
                        }
                        ?>
                        <tr>
                            <!--<td>
                                <?php /*if($model->termination_status == 2 && strtotime($model->termination_date) <= time()){*/?>
                                    <span class="tag label-rejected">Closed</span>
                                <?php /*}elseif($model->ext_status == 2 ){*/?>
                                    <span class="tag label-re-open">Open</span>
                                    <span class="tag label-rejected">Renewed</span>
                                <?php /*}else {*/?>
                                    <span class="tag <?php /*echo $className;*/?>"><?php /*echo  $workorderStatus[$workorder->workorder_status];*/?></span>
                                <?php /*}*/?>
                            </td>-->
                            <!--<td><a href=""><?php /*echo $model->workorder_id; */?></a></td>-->
                            <td><a href=""><?php echo $model->id; ?></a></td>
                            <td><?php echo $candidates->first_name.' '.$candidates->last_name; ?></td>
                            <td><?php echo $model->start_date; ?></td>
                            <td><?php echo $location->name; ?></td>
                            <td><a href=""><?php echo $jobs->title.' ('.$jobs->id.')'; ?></a></td>
                            <td style="text-align: center">
                                <a class="btn btn-primary" data-toggle="modal" href='#modal-id<?php echo $model->id; ?>'>Edit</a>
                                <div class="modal fade" id="modal-id<?php echo $model->id; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title"><?php echo $candidates->first_name.' '.$candidates->last_name; ?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <form id="client-form" method="post">
                                                    <input type="hidden" name="contract_id" value="<?php echo $model->id; ?>">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="">Bill Rate</label>
                                                                <input class="form-control" name="bill_rate" type="text" value="<?php echo $workorder->wo_bill_rate; ?>">
                                                            </div>
                                                        </div>
                                                        <!-- col-12 -->

                                                        <div class="col-xs-12 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="" class="">Pay Rate </label>
                                                                <input class="form-control" name="pay_rate" type="text" value="<?php echo $workorder->wo_pay_rate; ?>">
                                                            </div>
                                                            <!-- col-12 -->

                                                        </div>

                                                        <div class="col-xs-12 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="">Over Time</label>
                                                                <input class="form-control" name="over_time" type="text" value="<?php echo $workorder->wo_over_time; ?>">
                                                            </div>
                                                        </div>
                                                        <!-- col-12 -->

                                                        <div class="col-xs-12 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="" class="">Double Time </label>
                                                                <input class="form-control" name="double_time" type="text" value="<?php echo $workorder->wo_double_time; ?>">
                                                            </div>
                                                            <!-- col-12 -->

                                                        </div>
                                                        <!-- row -->

                                                        <div class="clearfix">
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="">Client Over Time</label>
                                                                    <input class="form-control" name="client_over_time" type="text" value="<?php echo $workorder->wo_client_over_time; ?>">
                                                                </div>
                                                            </div>
                                                            <!-- col-12 -->

                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="" class="">Client Double Time</label>
                                                                    <input class="form-control" name="client_double_time" type="text" value="<?php echo $workorder->wo_client_double_time; ?>">
                                                                </div>
                                                            </div>
                                                            <!-- col-12 -->

                                                        </div>
                                                        <!-- row -->

                                                        <div class="clearfix">
                                                            <!--<div class="col-xs-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="">Approval Manager</label>
                                                                    <select name="approval_manager" id="input" class="form-control" required="required">
                                                                        <option value="">Select</option>
                                                                    </select>
                                                                </div>
                                                            </div>-->
                                                            <!-- col-12 -->

                                                            <div class="col-xs-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="" class="">Time Sheet Approving Manager</label>
                                                                    <select name="approval_manager" id="input" class="form-control select2" required="required">
                                                                        <option value=""> Select Time Sheet Approving Manager</option>
                                                                        <?php foreach($allTeamMembers as $allMembers){ ?>
                                                                            <option value="<?php echo $allMembers['id']; ?>" <?php if($allMembers['id']==$workorder->approval_manager){ echo "Selected";} ?>><?php echo $allMembers['first_name'].' '.$allMembers['last_name'].'('.$allMembers['member_type'].')'; ?></option>
                                                                        <?php } ?>
                                                                    </select>                 </div>
                                                            </div>
                                                            <!-- col-12 -->

                                                        </div>
                                                        <!-- row -->

                                                        <div class="clearfix">
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="">Start Date</label>
                                                                    <input type="text" name="start_date" class="form-control datetimepickermodified dateField123" value="<?php echo date('m/d/Y',strtotime($workorder->onboard_changed_start_date)); ?>">

                                                                </div>
                                                            </div>
                                                            <!-- col-12 -->

                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="" class="">End Date</label>
                                                                    <input type="text" name="end_date" class="form-control datetimepickermodified dateField1234" value="<?php echo date('m/d/Y',strtotime($workorder->onboard_changed_end_date)); ?>">
                                                                </div>
                                                            </div>
                                                            <!-- col-12 -->

                                                        </div>
                                                        <!-- row -->

                                                        <div class="clearfix m-t-20">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" name="ImportData" class="btn btn-primary">Save changes</button>
                                                            </div> <!-- col -->
                                                        </div> <!-- row -->

                                                        <br>
                                                        <br>
                                                    </div>
                                                </form>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } }else{ ?>
                    <tr>
                        <td colspan="6"> Sorry No Record Found.. </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div> <!-- col -->
    </div> <!-- row -->
    <div class="seprater-bottom-100">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <?php
            $this->widget('CLinkPager',
                array('pages' => $pages,
                    'header' => '',
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array('class' => 'pagination m-t-0 m-b-0',)))
            ?>
        </div>
    </div>
</div>
