<?php

$this->pageTitle = 'My Profile';

$this->renderPartial('profilesidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                        <div class="cleafix " style="padding: 30px 20px; ">
                          <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <h4 class="m-b-10">Change Password</h4>
                            <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>

                            <?php if(Yii::app()->user->hasFlash('success')):?>

                          <?php echo Yii::app()->user->getFlash('success'); ?>

                            <?php endif; ?>

                            <?php $form = $this->beginWidget('CActiveForm', array(

              'id' => 'chnage-password-form',

              'enableClientValidation' => true,

              'htmlOptions' => array('class' => ''),

              'clientOptions' => array(

                'validateOnSubmit' => true,

              ),

            ));

            ?>
                               <div class="row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                     <div class="form-group">
                                        <label for="">Current password</label>
                                        <input type="password" name="old_password" class="form-control black-placeholder" id="" placeholder="Current Password" required>
                                     </div>

                                      <div class="form-group">
                                        <label for="">New password</label>
                                       <input type="password" name="new_password" class="form-control black-placeholder" id="pass1" placeholder="New Password" required>

                                     </div>

                                      <div class="form-group">
                                        <label for="">Confirm password</label>
                                        <input type="password" name="repeat_password" class="form-control black-placeholder" id="pass2" onkeyup="checkPass(); return false;" placeholder="Confirm Your Password" required>
                                     </div>
                                  </div>
                                  <!-- col -->
                                 
                               </div>
                               <!-- row -->
                               <br>
                               <button type="submit" name="updatepassword" class="btn btn-success">Update Password</button>
                               <a href="" class="btn btn-default">Cancel</a>
                            <?php $this->endWidget(); ?>
                             
                            
                          </div> <!-- col -->

                          
                        </div> <!-- row -->
                        
                        <div class="seprater-bottom-100"></div>
                    </div>