<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Departments</h4>
      <p class="m-b-40"></p>

      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>

      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
      )); ?>

        <div class="form-group">
          <label for="">Add Departments</label>
          <input type="text" class="form-control" name="department" pattern="[a-zA-z &-]{2,50}" id="" placeholder="" required>
        </div>

        <br>

        <button type="submit" class="btn btn-success">Add Departments</button>
        <a href="" class="btn btn-default">Cancel</a>

      <?php $this->endWidget(); ?>



    </div> <!-- col -->

    
  </div> <!-- row -->
  <div class="clearfix p-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-30 table-heading">List of Departments</h4>
      <?php if($modelSetting) { ?>
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
        <tr>
          <th>S.No &nbsp;&nbsp;&nbsp;&nbsp;Department Name</th>
          <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach($modelSetting as $value ) {
          $i++;
          ?>
        <tr>
          <td><?php echo $i .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$value->title; ?></td>
          <td class="text-center">

            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deletedepartment',array('id'=>$value->id))?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>

          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
      <?php } ?>
    </div> <!-- col -->
  </div> <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>

