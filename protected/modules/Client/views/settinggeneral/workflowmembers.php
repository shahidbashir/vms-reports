<?php $this->pageTitle = 'Approval Workflows';//$this->renderPartial('header'); ?>
<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 0; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <p class="m-b-40"></p>
      <p class="m-b-20">
      <h4 class="" style="margin:20px 20px 30px;"><?php echo $model->flow_name; ?> Workflow Members List <a href="<?php echo $this->createAbsoluteUrl('/Client/settinggeneral/workflows'); ?>" class="btn btn-default pull-right">Back to Work Flow Builder</a></h4>
      </p>
      <?php if(Yii::app()->user->hasFlash('error')):?>
        <?php echo Yii::app()->user->getFlash('error'); ?>
      <?php endif; ?>


    </div> <!-- col -->
  </div> <!-- row -->
  <div class="clearfix plr-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
        <tr>
          <th>S.No</th>
          <th>Department</th>
          <th>Team Member Name</th>
          <th>Email</th>
          <th class="text-center">Action</th>
        </tr>
        </thead>

        <tbody>
        <?php
        if(isset($_GET['id'])){
          $oldData = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_GET['id']),array( 'order' => 'order_approve asc'  ));
          //$oldData = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_GET['id']));
          if($oldData){ $i = 0 ; foreach($oldData as $oldData){ $i++;
            if($oldData['client_id'] != 0){
              $clientDatav = Client::model()->findByPk($oldData['client_id']);
              ?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $oldData['department_id'] ?></td>
                <td><?php echo $clientDatav->first_name.' '.$clientDatav->last_name; ?></td>
                <td><?php echo $clientDatav->email; ?></td>
                <td style="text-align: center" class="actions">
                  <a href="<?php echo $this->createAbsoluteUrl('settinggeneral/deleteWFmember',array('id'=>$oldData['id'],'workflow_id'=>$_GET['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" fa fa-trash"></i></a>
                </td>
              </tr>
            <?php } } } } ?>
        </tbody>
      </table>
    </div> <!-- col -->
  </div> <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
