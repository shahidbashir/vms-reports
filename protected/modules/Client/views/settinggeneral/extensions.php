<?php
	$client = Client::model()->findByPk(Yii::app()->user->id);
?>
<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Extension</h4>
      <p class="m-b-40"></p>

      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>

      <div class="row ">
        <div class="col-md-12">

          <div class="well well-sm">
            <p class="m-b-10">Google Calendar Sync</p>
            <?php /*?><button type="button" class="btn btn-default-2">Google Calendar Sync Permission</button><?php */?>
            <?php if($client->google_access_token==''){ ?>
              <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/oAuth2Callback'); ?>" class="btn btn-default-2">Google Calendar Sync Permission </a>
            <?php }else{ ?>
              <button type="button" class="btn btn-default disabled">Google Calendar Sync Permission</button>
            <?php } ?>
          </div>

          <div class="well well-sm">
            <p class="m-b-10">Microsoft Sync</p>
            <?php if($client->outlook_access_token==''){ ?>
              <a href="<?php echo oAuthService::getLoginUrl($this->createAbsoluteUrl('offlineAccess/authorize')); ?>" class="btn btn-default-2">Microsoft Sync Permission </a>
            <?php }else{ ?>
              <button type="button" class="btn btn-default disabled">Microsoft Sync Permission</button>
            <?php } ?>
          </div>


          <div class="well well-sm">
            <p class="m-b-10">Office 365 Calendar Sync</p>
            <button type="button" class="btn btn-default-2">Office 365 Calendar Permission</button>
          </div>




        </div>
      </div>



    </div> <!-- col -->
    
  </div> <!-- row -->
  <div class="seprater-bottom-100"></div>
  <div class="seprater-bottom-100"></div>
</div>
