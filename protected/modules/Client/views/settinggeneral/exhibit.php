<?php $this->renderPartial('/settinggeneral/appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Exhibit</h4>
      <p class="m-b-40"></p>

      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <?php if(Yii::app()->user->hasFlash('error')):?>
        <?php echo Yii::app()->user->getFlash('error'); ?>
      <?php endif; ?>

      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
          'htmlOptions' => array('enctype' => 'multipart/form-data'),
      )); ?>
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Document Name</label>
              <input type="text" name="file_name" value="" class="form-control" id="" placeholder="" required="required">
            </div>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">&nbsp;</label>
              <?php echo $form->fileField($model, 'file',array('class'=>'form-control'));  ?>
            </div>
          </div>
          <!-- col -->
        </div>
        <!-- row -->
        <br>
        <button type="submit" name="Save" class="btn btn-success">Add</button>
        <a href="" class="btn btn-default">Cancel</a>
      <?php $this->endWidget(); ?>



    </div> <!-- col -->
    
  </div> <!-- row -->
  <div class="clearfix p-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-30 table-heading">List of Documents</h4>
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
        <tr>
          <th>S. No</th>
          <th>Document Name</th>
          <th>Files</th>
          <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $loginUserId = Yii::app()->user->id;
        $data = ClientExhibit::model()->findAllByAttributes(array('client_id'=>$loginUserId));
        $i = 0;
        foreach($data as $key=>$value){
        $i++;
        ?>
        <tr>
          <td><?php echo $i; ?></td>

          <td>
            <p> <?php echo $value['name'];?> </p>

          </td>

          <td>
            <?php $file = Yii::app()->request->baseUrl.'/images/exhibitfiles/'.$value['file']; ?>
            <a href="<?php echo $file; ?>" download><?php echo $value['file'];?></a>
          </td>


          <td style="text-align: right; padding-right: 40px;">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deletefileExhibit',array('id'=>$value['id'])); ?>" data-toggle="popover" data-placement="bottom" data-content="Delete" class="delete" data-original-title="" title=""><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
    </div> <!-- col -->
  </div> <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>


<style>
  .table-striped {
    border: 1px solid #eee;
  }
</style>