<?php $this->renderPartial('appsettingsidebar'); ?>
  <div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px; ">
      <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <?php if($model->super_client_id==0){ ?>
        <h4 class="m-b-10">Company Profile</h4>
        <?php }else{ ?>
          <h4 class="m-b-10">Update Your Profile</h4>
        <?php } ?>
        <p class="m-b-20"></p>

        <?php if(Yii::app()->user->hasFlash('success')):?>
          <?php echo Yii::app()->user->getFlash('success'); ?>
        <?php endif; ?>

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'client-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),

        )); ?>
          <div class="row">
            <?php if($model->super_client_id==0){ ?>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label for="">Your Business Name </label>
                <?php echo $form->textField($model,'business_name',array('value'=>$model->business_name,'class'=>'form-control', "required"=>"required" )); ?>
                <?php echo $form->error($model,'business_name'); ?>
              </div>
            </div>
            <?php } ?>
            <!-- col-12 -->

            <div class="col-xs-12 col-sm-6">
              <label for="" class="">Your Full Name </label>
              <div class="two-flieds-2">
                <div class="form-group">
                  <?php echo $form->textField($model,'first_name',array('class'=>'form-control', "required"=>"required" ,'placeholder'=>'First Name')); ?>
                  <?php echo $form->error($model,'first_name',array('class'=>'error-font')); ?>
                </div>
                <div class="form-group">
                  <?php echo $form->textField($model,'last_name',array('class'=>'form-control', "required"=>"required",'placeholder'=>'Last Name')); ?>
                  <?php echo $form->error($model,'last_name',array('class'=>'error-font')); ?>
                </div>
              </div>
            </div>
            <!-- col-12 -->

          </div>
          <!-- row -->

          <div class="row">
            <?php if($model->super_client_id==0){ ?>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label for="">Your Business Website URL</label>
                <?php
                if(!$model->website_url) {
                  echo $form->textField($model,'website_url',array('value'=>'http://www.','class'=>'form-control','onblur'=>'return check();'));
                }else {
                  echo $form->textField($model,'website_url',array('class'=>'form-control','onblur'=>'return check();'));
                }

                ?>
                <?php echo $form->error($model,'website_url'); ?>
              </div>
            </div>
            <!-- col-12 -->

            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label for="" class="">Your Business Category </label>
                <?php
                $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=6')),'title', 'title');
                echo $form->dropDownList($model, 'business_type', $list , array('class'=>'form-control','empty' => '')); ?>
              </div>
              <!-- col-12 -->

            </div>
            <!-- row -->

            <div class="clearfix">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="">Total Number of Staff Members </label>
                  <?php
                  $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=7')),'title', 'title');
                  echo $form->dropDownList($model, 'total_staff', $list , array('class'=>'form-control', "required"=>"required" ,'empty' => '')); ?>
                </div>
              </div>
              <!-- col-12 -->

              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="" class="">Revenue Range ( Last Year )</label>
                  <?php
                  $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=8')),'title', 'title');
                  echo $form->dropDownList($model, 'revenue_range',$list, array('class'=>'form-control','empty' => '')); ?>
                </div>
              </div>
              <!-- col-12 -->

            </div>
            <!-- row -->

            <div class="clearfix">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="" class="">Describe your Business</label>
                  <?php echo $form->textArea($model,'describe_business',array("class"=>"form-control", "rows"=>"6")); ?> <?php echo $form->error($model,'describe_business'); ?>
                </div>
              <!-- col-12 -->

            </div>
            <!-- row -->
              </div>
          <?php } ?>
            <div class="clearfix">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="">LinkedIn Profile</label>
                  <?php echo $form->textField($model,'linkedin_profile',array("class"=>"form-control")); ?> <?php echo $form->error($model,'linkedin_profile'); ?>
                </div>
                </div>
              <!-- col-12 -->

              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="" class="">Twitter Profile</label>
                  <?php echo $form->textField($model,'twitter_profile',array("class"=>"form-control")); ?> <?php echo $form->error($model,'twitter_profile'); ?>
                </div>
              <!-- col-12 -->

            </div>
            <!-- row -->
              </div>

            <div class="clearfix">
              <?php if($model->super_client_id==0){ ?>
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="">Currency</label>
                  <?php
                  $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=12')),'title', 'title');
                  echo $form->dropDownList($model, 'currency', $list , array('class'=>'form-control','empty' => '')); ?>
                </div>
              </div>
              <!-- col-12 -->
              <?php } ?>

              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="" class="">Profile Image</label>
                  <?php echo $form->fileField($model, 'profile_image',array('class'=>'form-control')); ?>
                </div>
              </div>
              <!-- col-12 -->

            </div>
            <!-- row -->

            <div class="clearfix m-t-20">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <button type="submit" class="btn btn-success" onclick="return check();">Save &amp; Continue</button>
              </div> <!-- col -->
            </div> <!-- row -->

            <br>
            <br>
          </div>
        <?php $this->endWidget(); ?>
      </div> <!-- col -->


     
    </div> <!-- row -->
  </div>


  <style>
    .error-font{
      color: red;
    }
  </style>
  <script>
    function check(){

      var fileType = document.getElementById('Client_website_url').value.split('.');
      if(fileType[fileType.length - 1]!=='com' && fileType[fileType.length - 1]!=='net'){
        alert('Wrong Url. Please enter valid url.');
        return false;
      }
      return true;
    }
    </script>
