<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 0; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10"> Work Flow Builder</h4>

      <p class="m-b-40"></p>
      <p class="m-b-20">
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/AddWorkFlow'); ?>" class="btn btn-success">Create a New Workflow</a>
      </p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>


    </div> <!-- col -->
  </div> <!-- row -->
  <div class="clearfix plr-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
        <tr>
          <th>S.No</th>
          <th>WorkFlow Name</th>
          <th>Number of People</th>
          <th class="text-center">Action</th>
        </tr>
        </thead>

        <tbody>
        <?php $deptCounter = 0;
        $deptArray = array();
        $i = 0;
        if($model)
        foreach($model as $modelVal){
        $i++;
        $WorkflowUsers = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$modelVal->id));
          $countingUsers = count($WorkflowUsers);
        ?>
        <tr>

          <td>
            <?php echo $i; ?>
          </td>

          <td><?php echo $modelVal->flow_name; ?></td>
          <td><?php echo $countingUsers; ?></td>

          <td style="text-align: center" class="actions">
            <a href="<?php echo $this->createAbsoluteUrl('settinggeneral/workFlowMembers',array('id'=>$modelVal->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class=" fa fa-eye"></i></a>

            <a href="<?php echo $this->createAbsoluteUrl('settinggeneral/deleteWorkflow',array('id'=>$modelVal->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" fa fa-trash"></i></a>
          </td>


        </tr>
       <?php } ?>

        </tbody>
      </table>
    </div> <!-- col -->
  </div> <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>

