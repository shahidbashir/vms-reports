<?php $this->pageTitle = 'Approval Workflows';//$this->renderPartial('header'); ?>
<div class="main-content">
  <?php if(Yii::app()->user->hasFlash('error')):?>
  <?php echo Yii::app()->user->getFlash('error'); ?>
  <?php endif; ?>
  <div class="row"> <!--Condensed Table--> <!--Hover table-->
    <div class="row m-0">
      <div class="col-md-12">
        <div class="panel panel-default panel-table">
          <div class="panel-body">
            <div class="row m-0">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="text-right m-t-10 " > </div>
              </div>
              <!-- col --> </div>
            <!-- row -->
            <h4 class="" style="margin:20px 20px 30px;"><?php echo $model->flow_name; ?>" Workflow Members List <a href="<?php echo $this->createAbsoluteUrl('/Client/settinggeneral/workflows'); ?>" class="btn btn-default pull-right">Back to Work Order</a></h4>
            <table class="table table-striped dataTable no-footer">
              <tbody>
                <?php                            
				if(isset($_GET['id'])){                            
				$oldData = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_GET['id']),array( 'order' => 'order_approve asc'  ));                            
				//$oldData = WorklflowMember::model()->findAllByAttributes(array('workflow_id'=>$_GET['id']));
				if($oldData){ foreach($oldData as $oldData){                            
				if($oldData['client_id'] != 0){                            
				$clientDatav = Client::model()->findByPk($oldData['client_id']);                            
				?>
                <tr>
                  <td><?php echo $oldData['department_id'] ?></td>
                  <td><?php echo $clientDatav->first_name.' '.$clientDatav->last_name; ?></td>
                  <td><?php echo $clientDatav->email; ?></td>
                  <td style="text-align: center" class="actions"> 
                  	<a href="<?php echo $this->createAbsoluteUrl('settinggeneral/deleteWFmember',array('id'=>$oldData['id'],'workflow_id'=>$_GET['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" mdi mdi-delete"></i></a>
                  </td>
                </tr>
                <?php } } } } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Hover table--></div>
