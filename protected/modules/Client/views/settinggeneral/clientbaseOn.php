<?php $this->pageTitle = 'Time Sheet Configurtion'; ?>
<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Clients</h4>
      <p class="m-b-40"></p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <?php if($model->status==0 || empty($model->status)){ ?>
        <form id="client-form" method="post">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="radio">
                <label>
                  <input type="radio" name="type" id="input" <?php echo $model->type==1?'checked="checked"':''; ?> value="1" >
                  Project Name </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="type" id="input" <?php echo $model->type==2?'checked="checked"':''; ?> value="2" >
                  Time Sheet Code </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="type" id="input" <?php echo $model->type==3?'checked="checked"':''; ?> value="3" >
                  No Code </label>
              </div>
            </div>
          </div>
          <br>
          <button type="submit" name="typeForm" class="btn btn-success">Save</button>
          <button type="reset" class="btn btn-default">Cancel</button>
        </form>
      <?php }else{ ?>

        <form id="client-form" method="post">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="radio">
                <label>
                  <input type="radio" name="type" id="input" value="1" <?php echo $model->type==1?'checked="checked"':''; ?> disabled="disabled">
                  Project Name </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="type" id="input" value="2" <?php echo $model->type==2?'checked="checked"':''; ?> disabled="disabled" >
                  Time Sheet Code </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="type" id="input" value="3" <?php echo $model->type==3?'checked="checked"':''; ?> disabled="disabled" >
                  No Code </label>
              </div>
            </div>
          </div>
          <br>
        </form>

      <?php } ?>
    </div> <!-- col -->

  </div> <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>

