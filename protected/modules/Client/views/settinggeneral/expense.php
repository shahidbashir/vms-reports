<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Expenses</h4>
      <p class="m-b-40"></p>

      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>

      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
      )); ?>
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Expense Name</label>
              <input type="text" name="Expenses[expens_name]" class="form-control" required="required">
              <?php echo $form->error($model,'email'); ?>
            </div>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Expense Code</label>
              <input type="text" name="Expenses[expens_code]" class="form-control" required="required">
               </div>
          </div>
          <!-- col -->
        </div>
        <!-- row -->
        <br>
        <button type="submit" class="btn btn-success">Add Expense</button>
      <button type="reset" class="btn btn-default">Cancel</button>
      <?php $this->endWidget(); ?>



    </div> <!-- col -->
    
  </div> <!-- row -->
  <?php if($expenseRecord) { ?>
  <div class="clearfix p-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-30 table-heading">List of Expenses</h4>
      <table class="table m-b-40 without-border">

        <thead class="thead-default">
        <tr>
          <th>Expense Name & Code</th>

          <th class="text-center">Action</th>
        </tr>
        </thead>

        <tbody>
        <?php

        foreach( $expenseRecord as $value ) { ?>
        <tr>
          <td>
            <p><?php echo $value->expens_code;?> </p>
            <span class="cell-detail-description"><?php echo $value->expens_name;?></span>
          </td>
          <td style="text-align: center; padding-right: 30px;">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deleteExpense',array('id'=>$value->id))?>" data-toggle="popover" data-placement="bottom" data-content="Delete" class="delete" data-original-title="" title=""><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
    </div> <!-- col -->
  </div> <!-- row -->
  <?php } ?>
  <div class="seprater-bottom-100"></div>
</div>

