<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px; ">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h4 class="m-b-10">Location</h4>
            <p class="m-b-40"></p>

            <?php if(Yii::app()->user->hasFlash('success')):?>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            <?php endif; ?>

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'location-form',
                'enableAjaxValidation'=>false,
            )); ?>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="form-group">
                            <label for="">Location Name & Code</label>
                            <?php echo $form->textField($model,'name',array('maxlength'=>100,'class'=>'form-control','required'=>'required','placeholder'=>"Add Location",'pattern'=>".{4,}"  , 'required'=>'required', 'title'=>"4 characters minimum")); ?>
                            <?php echo $form->error($model,'name'); ?>
                        </div>

                    </div> <!-- col -->
                </div> <!-- row -->

                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Address 1</label>
                            <input id="autocomplete"
                                   onFocus="geolocate()" type="text" class="form-control" name="Location[address1]" placeholder="" required></input>
                            <input id="route" type="hidden"></input>
                        </div>
                    </div>
                    <!-- col -->
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Address 2</label>
                            <?php echo $form->textField($model,'address2',array('id'=>'route2','maxlength'=>100,'class'=>'form-control')); ?>
                        </div>
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
            <input type="hidden" name="state" id="administrative_area_level_1"  />
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">City</label>
                            <?php echo $form->textField($model,'city',array('id'=>'locality','maxlength'=>30,'class'=>'form-control','required'=>'required','pattern'=>".{6,}"  , 'required'=>'required', 'title'=>"6 characters minimum")); ?>
                        </div>
                    </div>
                    <!-- col -->
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Zip Code / Postal Code</label>
                            <?php echo $form->textField($model,'zip_code',array('id'=>'postal_code','maxlength'=>30,'class'=>'form-control','pattern'=>".{5,}"  , 'required'=>'required', 'title'=>"5 characters minimum")); ?>
                        </div>
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->

                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Country</label>
                            <?php echo $form->textField($model,'country',array('id'=>'country','maxlength'=>100,'class'=>'form-control')); ?>
                        </div>
                    </div>
                    <!-- col -->
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Account Manager</label>
                            <?php $list = Client::model()->findAll(array('condition'=>'super_client_id='.Yii::app()->user->id));
                            $array = array();
                            foreach($list as $listValue){
                                $array[$listValue->first_name.' '.$listValue->last_name] = $listValue->first_name.' '.$listValue->last_name;
                            }

                            echo $form->dropDownList($model, 'location_account_mananger',$array, array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                        </div>
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
                <br>
                <button type="submit" class="btn btn-success">Add Location</button>
                <a href="" class="btn btn-default">Cancel</a>
            <?php $this->endWidget(); ?>



        </div> <!-- col -->
        
    </div> <!-- row -->
    <?php if($allLocation) { ?>
    <div class="clearfix p-20">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-b-30 table-heading">List of Current Locations</h4>
            <table class="table m-b-40 without-border">
                <thead class="thead-default">
                <tr>
                    <th>Location</th>
                    <th>Address</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach( $allLocation as $value ) { ?>
                <tr>
                    <td>
                        <p><?php echo $value->name;?></p>
                        <span class="cell-detail-description"><?php echo $value->location_account_mananger;?></span>
                    </td>

                    <td>
                        <p><?php echo $value->address1; ?></p>
                    </td>


                    <td style="text-align: center; padding-right: 30px;">
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deletelocation',array('id'=>$value->id)); ?>" data-toggle="popover" data-placement="bottom" data-content="Delete" class="delete" data-original-title="" title=""><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div> <!-- col -->
    </div> <!-- row -->
    <?php } ?>
    <div class="seprater-bottom-100"></div>
</div>
<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        //street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          console.log(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;

          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

 $('#autocomplete').live('change',function(){
 	$('#route').val($(this).value);
 });
    </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw9D1ilDTssilN8RzWxTTP5vCwaUDB4RA&libraries=places&callback=initAutocomplete"
        async defer></script>
