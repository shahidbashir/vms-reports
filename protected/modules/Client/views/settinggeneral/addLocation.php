<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Client Location Configuration</h4>
      <p class="m-b-40"></p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
      )); ?>
        <div class="form-group">
          <label for="">Select Client</label>
          <select name="client_id" id="input" class="form-control" required="required">
            <option value="">Select</option>
            <?php 
				foreach($record as $name){
					echo '<option value="'.$name['client_id'].'">'.$name['name'].'</option>';
				}
			?>
          </select>
        </div>
        <div class="form-group">
          <label for="">Select Location</label>
          <select name="location_id" id="input" class="form-control" required="required">
            <option value="">Select</option>
            <?php 
				foreach($locations as $location){
					echo '<option value="'.$location['id'].'">'.$location['name'].'</option>';
				}
			?>
          </select>
   
        </div>
        <button type="submit" name="add_location" class="btn btn-success">Add</button>
        <a href="" class="btn btn-default">Cancel</a>
      <?php $this->endWidget(); ?>
    </div> <!-- col -->
    
  </div> <!-- row -->
  <div class="clearfix p-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-30 table-heading">List of Client Location Configuration</h4>
      <?php if($client_locations) { ?>
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
          <tr>
            <th>Sr. No:</th>
            <th>Client</th>
            <th>Location</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach($client_locations as $client_location) {
          $i++;
          ?>
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php echo $client_location->client_name; ?></td>
          <td><?php echo $client_location->location_name; ?></td>
          <td class="text-center">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deleteCLocation',array('id'=>$client_location->id))?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
      <?php } ?>
    </div> <!-- col -->
  </div> <!-- row -->
 <div class="seprater-bottom-100"></div>
</div>
<div class="modal fade" id="bsModal3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="mySmallModalLabel">Saved</h4>
      </div>
      <div class="modal-body">
        Your setting has been saved...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
