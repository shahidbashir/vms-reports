<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px; ">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h4 class="m-b-10">Time Sheet Code</h4>
            <p class="m-b-40"></p>

            <?php if(Yii::app()->user->hasFlash('success')):?>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            <?php endif; ?>

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'project-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation'=>false,
            )); ?>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Time Sheet Unique Code</label>
                            <?php echo $form->textField($model,'time_code',array('class'=>'form-control')); ?>
                            <?php echo $form->error($model,'time_code'); ?>
                        </div>
                    </div>
                    <!-- col -->
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Select Project</label>
                            <?php
                            $userId = UtilityManager::superClient(Yii::app()->user->id);
                            $list = CHtml::listData(Project::model()->findAllByAttributes(array('client_id'=>$userId)),'id', 'project_name');
                            echo $form->dropDownList($model, 'project_id', $list, array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                        </div>
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
                <br>
                <button type="submit" class="btn btn-success">Add</button>
                <a href="" class="btn btn-default">Cancel</a>
            <?php $this->endWidget(); ?>



        </div> <!-- col -->
        
    </div> <!-- row -->
    <div class="clearfix p-20">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <h4 class="m-b-30 table-heading">List Time Sheet Code</h4>
            <?php if($modelTimesheet){ ?>
            <table class="table m-b-40 without-border">
                <thead class="thead-default">
                <tr>
                    <th>Time Sheet Code</th>
                    <th>Project Name</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($modelTimesheet as $value){
                $Project = Project::model()->findByPk($value['project_id']);
                ?>
                <tr>
                    <td>
                        <?php echo $value['time_code']; ?>
                    </td>

                    <td>
                        <?php echo $Project->project_name; ?>
                    </td>
                    <td style="text-align: center">
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deleteTimesheetcode',array('id'=>$value['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php } ?>
        </div> <!-- col -->
    </div> <!-- row -->
    <div class="seprater-bottom-100"></div>
</div>