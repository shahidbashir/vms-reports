<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-12 col-lg-12">
      <h4 class="m-b-10"> Add Contract Work Order

      </h4>

      <p class="m-b-40"></p>

      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>

      <p class="m-b-20">
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/contractWorkFlows'); ?>" class=" btn btn-xs btn-default-2">Back to Contract Work Order</a>
      </p>

      <?php $form=$this->beginWidget('CActiveForm', array('id'=>'worklflow-form',
          'enableAjaxValidation'=>false)); ?>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
              <label for="">Contract WorkOrder Name</label>
              <?php echo $form->textField($model,'contract_flow_name',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
          </div>
          </div>
          <!-- col -->
          
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Rate Range From </label>
              <?php echo $form->numberField($model,'range_from',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
          </div>
          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Rate Range To </label>
              <?php echo $form->numberField($model,'range_to',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
          </div>
          </div>
          
        </div>
        <!-- row -->
        
        
        <div class="three-fields">
          <div class="form-group">
            <label for="">Select Department</label>
            <?php $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=4')),'title', 'title');
            echo $form->dropDownList($model1, 'department_id', $list , array('class'=>'form-control','empty' => 'Select Department')); ?>
          </div>
          <div class="form-group">
            <label for="">Select Team Member</label>
            <select name="ContractWorklflowMember[client_id]" id="team_members" class="form-control" required="required">
              <option value="0">Select Team Member</option>
            </select>
          </div>
          <div class="form-group">
            <label for="">Order of Listing </label>
            <?php echo $form->textField($model1,'order_approve',array('class'=>'form-control','placeholder'=>'Order of Approval')); ?>
          </div>
        </div>
        <br>
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save',array('class'=>'btn btn-success')); ?>
    <?php $this->endWidget(); ?>
      <br>
      <br>
      <div class="clearfix">
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/contractWorkFlows'); ?>" class="btn  btn-success">Save Contract Workflow</a> </div>
        

    </div> <!-- col -->

  </div> <!-- row -->

  <div class="clearfix p-20">
    <?php if(isset($_GET['id']))
    {
    $oldData = ContractWorklflowMember::model()->findAllByAttributes(array('contract_flow_id'=>$_GET['id']),array('order' => 'order_approve asc'));

    ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
        <tr>
          <th>Sr. No:</th>
          <th>Team Member Name</th>
          <th>Department</th>
          <th>Order Of Approval</th>
          <th class="text-center">Action</th>
        </tr>
        </thead>

        <tbody>
        <?php  $i = 0;
        foreach($oldData as $oldData){
        $i++;
        if($oldData['client_id'] != 0){
        $clientDatav = Client::model()->findByPk($oldData['client_id']);
        ?>
        <tr>

          <td><?php echo $i; ?></td>
          <td><?php echo $clientDatav->first_name.' '.$clientDatav->last_name; ?></td>
          <td><?php echo $oldData['department_id'] ?></td>
          <td><?php echo $oldData['order_approve'] ?></td>

          <td style="text-align: center" class="actions">


            <a href="<?php echo $this->createAbsoluteUrl('settinggeneral/deleteCWFmember',array('id'=>$oldData['id'],'workflow_id'=>$_GET['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" fa fa-trash"></i></a>
          </td>


        </tr>
        <?php } } ?>

        </tbody>
      </table>
    </div> <!-- col -->
    <?php } ?>
  </div> <!-- row -->
</div>
