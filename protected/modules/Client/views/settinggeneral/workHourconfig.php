<?php $this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px; ">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h4 class="m-b-10">Work Hour configuration</h4>
            <br>

            <?php if(Yii::app()->user->hasFlash('success')):?>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            <?php endif; ?>

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'client-form',
                'enableAjaxValidation'=>false,
            )); ?>


                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="">Working Hour - Start Time</label>
                            <input type="text" name="WorkHourConfiguration[start_time]" id="input" class="form-control time-picker ui-timepicker-input" <?php if($WorkHourConfiguration){?> value="<?php echo $WorkHourConfiguration->start_time; ?>" readonly="readonly" <?php } ?>>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        <div class="form-group">
                            <label for="">Working Hour - End Time</label>
                            <input type="text" name="WorkHourConfiguration[end_time]" id="input" class="form-control time-picker ui-timepicker-input" <?php if($WorkHourConfiguration){?> value="<?php echo $WorkHourConfiguration->end_time; ?>" readonly="readonly" <?php } ?> >
                        </div>

                    </div>
                </div>


                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="">Minimum Non Billable Hour in Days</label>
                            <input type="number" name="WorkHourConfiguration[billable_hour]" min="0" id="input" class="form-control" <?php if($WorkHourConfiguration){?> value="<?php echo $WorkHourConfiguration->billable_hour; ?>" readonly="readonly" <?php } ?> >
                        </div>
                    </div>

                </div>

                <br>
            <?php if(empty($WorkHourConfiguration)){ ?>
            <button type="submit" class="btn btn-success">Save</button>
            <?php } ?>

            <?php $this->endWidget(); ?>



        </div> <!-- col -->

        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <!--<h4 class="m-b-10">Excepteur sint</h4>


            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>-->
        </div> <!-- col -->
    </div> <!-- row -->

    <div class="seprater-bottom-100"></div>
    <div class="seprater-bottom-100"></div>
    <div class="seprater-bottom-100"></div>
</div>