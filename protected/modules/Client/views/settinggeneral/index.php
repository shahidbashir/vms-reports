<div class="db-content-inner">
  <div class="db-content no-padding">
    <div class="db-setting-sidebar">
      <ul class="list-unstyled">
        <li class="m-b-10"><a href="setting-genral.html" class="bold ">Genral Setting</a></li>
        <li><a href="#">Company Profile</a></li>
        <li><a href="setting-departments.html">Departments</a></li>
        <li><a href="setting-locations.html">Location</a></li>
        <li><a href="setting-teammember.html">Invite Team Members</a></li>
        <li><a href="setting-jobrequest.html">Job Request Type</a></li>
        <li><a href="setting-billingcode.html">Operating Billing Code</a></li>
      </ul>
    </div>
    <!-- db-setting-sidebar -->
    <div class="db-setting-content">
      <div class="db-setting-content-inner">
        <div class="db-g-header">
          <h4>Genral Setting</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis corporis eos eaque, ex ad porro provident vel iusto dolor.</p>
        </div>
        <div class="departments">
          <form action="" method="POST" role="form">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="">Your Business Name </label>
                  <input type="text" class="form-control" id="" placeholder="">
                </div>
              </div>
              <!-- col-12 -->
              
              <div class="col-xs-12 col-sm-6">
                <label for="" class="">Your Full Name </label>
                <div class="form-group two-inputs">
                  <input type="text" class="form-control" id="" placeholder="First Name">
                  <input type="text" class="form-control" id="" placeholder="Last Name">
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="">Your Business Website URL</label>
                  <input type="text" class="form-control" id="" placeholder="">
                </div>
              </div>
              <!-- col-12 -->
              
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="" class="">Your Business Category </label>
                  <div class="single">
                    <select name="skills" class="ui fluid search dropdown">
                      <option value=""></option>
                      <option value="angular">Angular</option>
                      <option value="css">CSS</option>
                      <option value="design">Graphic Design</option>
                    </select>
                  </div>
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="">Total Number of Staff Members </label>
                  <div class="single">
                    <select name="skills" class="ui fluid search dropdown">
                      <option value=""></option>
                      <option value="angular">Angular</option>
                      <option value="css">CSS</option>
                      <option value="design">Graphic Design</option>
                    </select>
                  </div>
                </div>
              </div>
              <!-- col-12 -->
              
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="" class="">Revenue Range ( Last Year )</label>
                  <div class="single">
                    <select name="skills" class="ui fluid search dropdown">
                      <option value=""></option>
                      <option value="angular">Angular</option>
                      <option value="css">CSS</option>
                      <option value="design">Graphic Design</option>
                    </select>
                  </div>
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="" class="">Describe your Business</label>
                  <textarea name="" id="input" class="form-control" rows="6"></textarea>
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="">Linked Profile</label>
                  <input type="text" class="form-control" id="" placeholder="">
                </div>
              </div>
              <!-- col-12 -->
              
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="" class="">Twitter Profile</label>
                  <input type="text" class="form-control" id="" placeholder="">
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            
            <button type="submit" class="btn btn-green">SAVE & CONTINUE</button>
          </form>
        </div>
        <!-- departments --> 
      </div>
      <!-- db-setting-content-inner --> 
      
    </div>
    <!-- db-setting-sidebar --> 
    
  </div>
  <!-- db-content --> 
</div>
<!-- db-content-inner -->