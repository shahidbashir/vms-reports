<?php

$this->pageTitle = 'My Profile';

$this->renderPartial('profilesidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                        <div class="cleafix " style="padding: 30px 20px; ">
                          <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <h4 class="m-b-10">My Profile</h4>
                            <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>

                            <?php if(Yii::app()->user->hasFlash('success')):?>

                              <?php echo Yii::app()->user->getFlash('success'); ?>

                              <?php endif; ?>


                            <?php $form=$this->beginWidget('CActiveForm', array(

                            'id'=>'client-form',

                            'enableAjaxValidation'=>false,
                            'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),

                              )); ?>
                               <div class="two-flieds-2">
                                  <div class="form-group">
                                     <label for="">First Name</label>
                                     <input type="text" name="Client[first_name]" value="<?php echo $model->first_name; ?>" class="form-control" pattern="[a-zA-Z]+" title="First name should be a Letter/character/Alphabet" id="" placeholder="" required="required">
                                  </div>
                                  <div class="form-group">
                                     <label for=""> Last Name</label>
                                     <input type="text" name="Client[last_name]" value="<?php echo $model->last_name; ?>" class="form-control" id="" pattern="[a-zA-Z]+" title="Last name should be a Letter/character/Alphabet" placeholder="" required="required">
                                  </div>
                               </div>
                               <div class="form-group">
                                  <label for="">Email Address</label>
                                  <input type="text" name="Client[email]" value="<?php echo $model->email; ?>" disabled="disabled" class="form-control" id="" placeholder="">
                               </div>
                               <div class="form-group">
                                  <label for="">Profile Image</label>
                               <div class="input-group">
                                     <?php echo $form->fileField($model, 'profile_image',array('class'=>'form-control')); ?>
                                  </div>
                               <div class="form-group">
                                  <label for="">Mobile Number</label>
                                  <input type="text" name="Client[mobile]" value="<?php echo $model->mobile; ?>"  pattern="[0-9]{5,10}" title="9 to 13 digits" class="form-control" required >
                               </div>
                               <div class="form-group">
                                  <label for="">Phone Number</label>
                                  <input type="text" name="Client[phone]" value="<?php echo $model->phone; ?>" pattern="[0-9]{5,10}" title="9 to 13 digits" class="form-control" required >
                               </div>
                               <button type="submit" class="btn btn-success">Submit</button>
                            <?php $this->endWidget(); ?>
                            
                          </div> <!-- col -->

                          
                        </div> <!-- row -->
                        
                        <div class="seprater-bottom-100"></div>
                    </div>