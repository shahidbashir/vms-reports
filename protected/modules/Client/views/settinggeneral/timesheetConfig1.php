<?php 
	$this->renderPartial('appsettingsidebar');
	$clientID = Yii::app()->user->id;
	$mainclient = Yii::app()->user->id;
	$client = Client::model()->findByPk($mainclient);
	if($client->member_type!=NULL){
		$client = Client::model()->findByPk($client->super_client_id);
		$mainclient = $client->id;
	}
 ?>

<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="dailyDiv">
      <h4 class="m-b-10">Time Sheet Configurtion</h4>
      <p class="m-b-40"></p>
      
      <!--<div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Title!</strong> Alert body ... </div>
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Title!</strong> Alert body ... </div>
        -->
        
      <?php 
	  
	  if(empty($model->id)){
	  
	  if(Yii::app()->session['first_form']['type']=='daily'){ ?>
        
      <form id="client-form" action="" method="post">
        <p class="m-b-10">Daily Hours</p>
        <table class="table m-b-40 without-border">
          <thead class="thead-default">
            <tr>
              <th>Total Hrs</th>
              <th>Monday</th>
              <th>Tuesday</th>
              <th>Wesdnesday</th>
              <th>Thursday</th>
              <th>Friday</th>
              <th>Saturday</th>
              <th>Sunday</th>
            </tr>
          </thead>
          <tbody class="dailyForm">
            <tr id="myTableRow">
              <td><input type="number" name="TimesheetHoursConfig[total_hours]" id="days_total" class="form-control totalF" value="0" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[mon]" id="input" class="form-control regulardays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[tue]" id="input" class="form-control regulardays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[wed]" id="input" class="form-control regulardays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[thu]" id="input" class="form-control regulardays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[fri]" id="input" class="form-control regulardays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[sat]" id="input" class="form-control" value="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[sun]" id="input" class="form-control" value="0" ></td>
            </tr>
          </tbody>
        </table>
        <p class="inline-checkboxes">
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox1" disabled="disabled" checked="checked" value="option1">
            Double Time - Saturday </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox2" disabled="disabled" checked="checked" value="option2">
            Double Time - Sunday </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox3" disabled="disabled" checked="checked" value="option3">
            Over Time - Anytime over the regular daily work hours </label>
            
            <input type="hidden" value="1" name="TimesheetHoursConfig[doubletime_sat]"  />
            <input type="hidden" value="1" name="TimesheetHoursConfig[doubletime_sun]"  />
            <input type="hidden" value="1" name="TimesheetHoursConfig[overtime]"  />
        </p>
        <br>
        <br>
        <button type="submit" name="daily_form" class="btn btn-success" >Save</button>
      </form>
      
      
      
      
      
      <?php }else{ ?>
      
  	  <form id="client-form" action="" method="post">
        <p class="m-b-10">Weekly Hours</p>
        <table class="table m-b-40 without-border">
          <thead class="thead-default">
            <tr>
              <th>Total Hrs</th>
              <th>Monday</th>
              <th>Tuesday</th>
              <th>Wesdnesday</th>
              <th>Thursday</th>
              <th>Friday</th>
              <th>Saturday</th>
              <th>Sunday</th>
            </tr>
          </thead>
          <tbody>
            <tr id="myTableRow1">
              <td><input type="number" name="TimesheetHoursConfig[total_hours]" id="input" class="form-control totalOver" value="0" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[mon]" id="input" class="form-control overtimedays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[tue]" id="input" class="form-control overtimedays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[wed]" id="input" class="form-control overtimedays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[thu]" id="input" class="form-control overtimedays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[fri]" id="input" class="form-control overtimedays" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[sat]" id="input" class="form-control" value="0" min="0" ></td>
              <td><input type="number" name="TimesheetHoursConfig[sun]" id="input" class="form-control" value="0" min="0" ></td>
            </tr>
          </tbody>
        </table>
        <p class="inline-checkboxes">
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox1" disabled="disabled" checked="checked" value="option1">
            Double Time - Saturday </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox2" disabled="disabled" checked="checked" value="option2">
            Double Time - Sunday </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox3" disabled="disabled" checked="checked" value="option3">
            Over Time - Anytime over the regular Weekly work hours </label>
            
            <input type="hidden" value="1" name="TimesheetHoursConfig[doubletime_sat]"  />
            <input type="hidden" value="1" name="TimesheetHoursConfig[doubletime_sun]"  />
            <input type="hidden" value="1" name="TimesheetHoursConfig[overtime]"  />
        </p>
        <br>
        <br>
        <button type="submit" name="weekly_form" class="btn btn-success" >Save</button>
        <!--<a href="" type="submit" class="btn btn-success">Save</a>-->
      </form>
      
      <?php } 
	  }else{ ?>
		   <div class="alert alert-warning">
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   <strong>Warning!</strong> Already Your <?php echo ucwords($model->hours_type) ?> Hours Setting For This Location Is Done. 
           </div>
          
        <p class="m-b-10"><?php echo ucwords($model->hours_type) ?> Hours</p>
        <table class="table m-b-40 without-border">
          <thead class="thead-default">
            <tr>
              <th>Total Hrs</th>
              <th>Monday</th>
              <th>Tuesday</th>
              <th>Wesdnesday</th>
              <th>Thursday</th>
              <th>Friday</th>
              <th>Saturday</th>
              <th>Sunday</th>
            </tr>
          </thead>
          <tbody>
            <tr id="myTableRow1">
              <td><input type="number" name="TimesheetHoursConfig[total_hours]" id="input" class="form-control totalOver" value="<?php echo $model->total_hours ?>" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[mon]" id="input" class="form-control overtimedays" value="<?php echo $model->mon ?>" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[tue]" id="input" class="form-control overtimedays" value="<?php echo $model->tue ?>" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[wed]" id="input" class="form-control overtimedays" value="<?php echo $model->wed ?>" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[thu]" id="input" class="form-control overtimedays" value="<?php echo $model->thu ?>" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[fri]" id="input" class="form-control overtimedays" value="<?php echo $model->fri ?>" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[sat]" id="input" class="form-control" value="<?php echo $model->sat ?>" min="0" readonly="readonly" ></td>
              <td><input type="number" name="TimesheetHoursConfig[sun]" id="input" class="form-control" value="<?php echo $model->sun ?>" min="0" readonly="readonly" ></td>
            </tr>
          </tbody>
        </table>
        <p class="inline-checkboxes">
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox1" disabled="disabled" checked="checked" value="option1">
            Double Time - Saturday </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox2" disabled="disabled" checked="checked" value="option2">
            Double Time - Sunday </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox3" disabled="disabled" checked="checked" value="option3">
            Over Time - Anytime over the regular Weekly work hours </label>
            
            <input type="hidden" value="1" name="TimesheetHoursConfig[doubletime_sat]"  />
            <input type="hidden" value="1" name="TimesheetHoursConfig[doubletime_sun]"  />
            <input type="hidden" value="1" name="TimesheetHoursConfig[overtime]"  />
        </p>
        <br>
        <br>
           
           
	<?php   }   ?>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  
  <div class="seprater-bottom-100"></div>
  <div class="seprater-bottom-100"></div>
  <div class="seprater-bottom-100"></div>
</div>
