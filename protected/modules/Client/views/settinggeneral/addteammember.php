<?php
error_reporting(0);
$this->renderPartial('appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">Team Members</h4>
      <p class="m-b-40"></p>

      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>

      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
      )); ?>
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="two-flieds-2">
              <div class="form-group">
                <label for="">First Name</label>
                <?php echo $form->textField($model,'first_name',array('class'=>'form-control','pattern'=>'[a-zA-z ]{2,25}','title'=>'First name should contain only character','placeholder'=>'','required'=>'required')); ?>
                <?php
                echo $form->error($model,'first_name',array('class'=>'error-font'));
                ?>
              </div>
              <div class="form-group">
                <label for=""> Last Name</label>
                <?php echo $form->textField($model,'last_name',array('class'=>'form-control','pattern'=>'[a-zA-z ]{2,25}','title'=>'last name should contain only character','placeholder'=>'')); ?>
                <?php
                echo $form->error($model,'last_name',array('class'=>'error-font'));
                ?>
              </div>
            </div>
          </div>
          <!-- col-12 -->
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="">Email Address</label>
              <?php echo $form->textField($model,'email',array('class'=>'form-control','required'=>'required')); ?>
              <?php
              echo $form->error($model,'email',array('class'=>'error-font'));
              ?>
            </div>
          </div>
        </div>
        <!-- row -->
        <!-- row -->
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="">Department</label>
              <?php
              $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=4 or client_id='.Yii::app()->user->id)),'title', 'title');
              echo $form->dropDownList($model, 'department', $list, array('class'=>'form-control','empty' => '','required'=>'required')); ?>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="">Roles Access</label>
              <select class="form-control" name="Client[member_type]" required >
              	<option value="">Select</option>
                <option value="Administrator">Administrator</option>
                <option value="Account Manager">Account Manager</option>
                <option value="Hiring Manager">Hiring Manager</option>
              </select>
              <?php
			  //these are all the roles list but on demand of rohait just putting few static roles for now.
              //$list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=3')),'title', 'title');
              //echo $form->dropDownList($model, 'member_type',$list, array('class'=>'form-control','required'=>'required')); 
			  ?>
            </div>
          </div>
        </div>
        <!-- row -->
        <br>
        <button type="submit" class="btn btn-success">Add Team Member</button>
        <button type="button" onclick="resetform();" class="btn btn-default">Cancel</button>
      <?php $this->endWidget(); ?>



    </div> <!-- col -->

    
  </div> <!-- row -->
  <?php if($allTeam) { ?>
  <div class="clearfix p-20">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-30 table-heading">List of Team Members</h4>
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
        <tr>
          <th>S.No</th>
          <th>Team Member</th>
          <th>Roles Access</th>
          <th></th>
          <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach( $allTeam as $value ) {
          $i++;
          ?>
        <tr>
          <td><?php echo $i; ?></td>
          <td class="avatar-cell">
            <div class="inline-block">
              <div class="avatar-small-text">
                <span><?php echo ucfirst($value->first_name[0]).' '.ucfirst($value->last_name[0]);?></span>
              </div>
            </div>
            <div class="inline-block">
              <strong><?php echo $value->first_name.' '.$value->last_name;?></strong>
              <br>
              <span class="cell-detail-description"><?php echo $value->department;?></span>
            </div>
          </td>
          <td>
            <strong><?php echo $value->email;?></strong>
            <br>
            <span class="cell-detail-description"><?php echo $value->member_type;?></span>
          </td>
          <td>
            <?php if($value->profile_approve=='Yes'){ ?>
            <i class="fa fa-check" style="color: #7FC35C"></i>
            <?php }else{ ?>
              <i class="fa fa-remove" style="color: red"></i>
            <?php }?>
          </td>
          <td style="text-align: center">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/deleteteammember',array('id'=>$value->id))?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>
            <a href="<?php echo $this->createAbsoluteUrl('settinggeneral/jobpostingConfiguration',array('id'=>$value->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Jon Posting Configuration"><i class="glyphicon glyphicon-asterisk"></i></a>
            <a id="<?php echo $value->id; ?>" data-placement="top" class="tooltips" data-original-title="View" onclick="Viewdetail(this);"><i class=" fa fa-eye"></i></a>
          </td>
        </tr>
        <?php } ?>
        </tbody>
      </table>
    </div> <!-- col -->
  </div> <!-- row -->
  <?php }?>
  <div class="seprater-bottom-100"></div>
</div>

<div class="modal fade" id="modal-id">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">User Info</h4>
      </div>
      <div class="modal-body" id="teaMmemmber">
        <table>
          <tbody>
          <tr><td>Name</td>
            <td>Name</td>
          </tr>

          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<style>
  .error-font{
    color: red;
  }
</style>
<script>

  function Viewdetail(elem) {
    var teammemberId = ($(elem).attr('id'));
    $.ajax({
      'url':'<?php echo $this->createUrl('settinggeneral/teaminfo') ?>',
      type: "POST",
      data: { teammemberId: teammemberId},
      'success':function(html){
        //alert(html);
        document.getElementById('teaMmemmber').innerHTML = html;
        //$(".teaMmemmber").html(html);
        $("#modal-id").modal('show');

      }
    });
  }
    function resetform()
    {
      //alert('hi');
      var myform = document.getElementById("client-form");
      var texbixs = $(myform).find('input');
      var dropdown = $(myform).find('select');
      //console.log(dropdown);
     // return false;
      for(var i=0;i<texbixs.length;i++){
        //alert('hi');
        texbixs[i].value = "";
      }
      for(var i=0;i<dropdown.length;i++){
        //alert('hi');
        dropdown[i].selectedIndex = -1;
      }
      //console.log(texbixs);
      //document.getElementById("client-form").reset();
      //document.forms["client-form"].reset(); //and then reset the form values
    }


</script>