<?php $this->renderPartial('appsettingsidebar'); ?>

<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h4 class="m-b-10">WorkFlow Approval Setting</h4>
      <p class="m-b-40"></p>
      
      <?php if(Yii::app()->user->hasFlash('success')):?>
      	<?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      
      <?php 
	  if(empty($model->workflow_process)){
	  $form=$this->beginWidget('CActiveForm', array(
          'id'=>'client-form',
          'enableAjaxValidation'=>false,
      )); ?>
      <div class="form-group">
        <label for="" class="">Need Work Flow Approval </label>
        <select class="form-control" name="workflow_process" id="workflow_process">
          <option value=""></option>
          <option value="Yes">Yes</option>
          <option value="No">No</option>
        </select>
      </div>
      <br>
      <button type="submit" name="submit" class="btn btn-success">Save Setting</button>
      <?php $this->endWidget();
	  }else{ ?>
      	<div class="form-group">
            <label for="" class="">Need Work Flow Approval </label>
            <select class="form-control" name="workflow_process" id="workflow_process" disabled="disabled">
              <option value="<?php echo $model->workflow_process; ?>"><?php echo $model->workflow_process; ?></option>
            </select>
          </div>
      <?php } ?>
    </div>
    <!-- col -->
    
     
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
