<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td1 = tr[i].getElementsByTagName("td")[1];
            td6 = tr[i].getElementsByTagName("td")[6];
            td7 = tr[i].getElementsByTagName("td")[7];
            if (td || td1 || td6 || td7 ) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                }else if(td1.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td6.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td7.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }

            }
        }
    }
</script>
<?php $this->pageTitle = 'Contracts'; ?>
<?php $model = $dataProvider->getData(); ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

    <div class="cleafix " style="padding: 30px 20px 10px; ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-b-10 page-title">Contract Renewals

            </h4>
            <p class="m-b-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>


            <div class="search-box">
                <div class="two-fields">

                    <div class="form-group">
                        <label for=""></label>
                        <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search Contracts">
                    </div>

                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <button type="button" class="btn btn-primary btn-block">Search</button>
                    </div>

                </div> <!-- two-flieds -->
            </div>



            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="simplify-tabs m-b-50">

                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs no-hover" role="tablist">

                                <li class="nav-item">
                                    <a class="nav-link active"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractrenewalsList'); ?>" role="tab"
                                       data-toggle="tooltip" data-placement="bottom" title="Overview">
                                        <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-overview@512px.svg"></i>
                                        Renewals</a>
                                </li>


                                <li class="nav-item">
                                    <a class="nav-link "  href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractclosedList'); ?>" role="tab"
                                       data-toggle="tooltip" data-placement="bottom" title="Overview">
                                        <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-overview@512px.svg"></i>
                                        Closed Contracts</a>
                                </li>

                            </ul>

                            <div class="tab-content ">
                                <div class="tab-pane active" id="home"  role="tabpanel">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                            <table class="table m-b-40 without-border" id="myTable" >
                                                <thead class="thead-default">
                                                <tr>
                                                    <th  style="width: 10px;">S.No</th>
                                                    <th style="width: 150px;">
                                                        Candidate Name
                                                    </th>
                                                    <th>Vendor Name</th>
                                                    <th>Bill Rate</th>
                                                    <th>Location </th>
                                                    <th>Duration</th>
                                                    <th>Days to Completion</th>

                                                    <th class="text-center">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 0; foreach($model as $val) {
                                                    $todaydate = date('Y-m-d');
                                                    $i++;
                                                    $candidate = Candidates::model()->findByPk($val['candidate_id']);
                                                    $vendor = Vendor::model()->findByPk($val['vendor_id']);
                                                    $workorder = Workorder::model()->findByPk($val['workorder_id']);
                                                    $offer = Offer::model()->findByPk($val['offer_id']);
                                                    $location = Location::model()->findByPk($offer->approver_manager_location);
                                                    ?>
                                                <tr>

                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $candidate->first_name.' '.$candidate->last_name; ?></td>
                                                    <td><?php echo $vendor->organization; ?></td>
                                                    <td><?php echo '$ '.$workorder->wo_bill_rate; ?></td>
                                                    <td><?php echo $location->name; ?></td>
                                                    <?php if($val['termination_status']==0){
                                                        $enddate = $val['end_date'];
                                                    }else{
                                                        $enddate = $val['termination_date'];
                                                    } ?>
                                                    <td><?php echo $val['start_date'].' - '.$enddate; ?></td>
                                                    <?php if($val['termination_status']==0){
                                                        //$diff = date_diff($val['end_date'],$todaydate);
                                                        $today = new DateTime('now');
                                                        $date = new DateTime($val['end_date']);
                                                        $diff = date_diff($today,$date);
                                                    }else{
                                                        $today = new DateTime('now');
                                                        $date = new DateTime($val['termination_date']);
                                                        $diff = date_diff($today,$date);
                                                    } ?>
                                                    <td><?php echo $diff->format("%R%a days"); ?></td>
                                                    <td style="display: none;"><?php echo $val['id']; ?></td>
                                                    <td style="text-align: center">
                                                        <a href="<?php echo $this->createAbsoluteUrl('contract/contractView', array('id' => $val['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                                                    </td>
                                                </tr>
                                                <?php } ?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div> <!-- tabpanel -->

                    </div> <!-- simplify-tabs -->
                </div>
            </div>



        </div>
        <!-- col -->

    </div>
    <!-- row -->
    <div class="clearfix" style="padding: 10px 20px 10px; ">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <?php
            $this->widget('CLinkPager', array(
                'pages' => $dataProvider->pagination,
                'header' => '',
                'nextPageLabel' => 'Next',
                'prevPageLabel' => 'Prev',
                'selectedPageCssClass' => 'active',
                'hiddenPageCssClass' => 'disabled',
                'htmlOptions' => array(
                    'class' => 'pagination m-t-0',
                )
            ))
            ?>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <!--<p class="text-right">
                Showing 10 to 20 of 50 entries
            </p>-->
        </div>
    </div>



    <div class="seprater-bottom-100"></div>


</div>