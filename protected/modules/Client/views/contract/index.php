<?php $this->pageTitle = 'Contracts'; error_reporting(0); ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List of Contracts <!--<a href=""  class="pull-right"><i class="fa fa-question"></i></a> <a  class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
          A Complete list of candidate who are under contract.</p>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="sdata" placeholder="Search Contracts">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <?php /*?><div class="row m-b-20">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="two-fields-70-30">
            <div class="form-group form-control-2">
              <label for="" style="text-align: left;">Sort by Job Name</label>
              <select name="" id="input" class="form-control  select2" required>
                <option value="">Select</option>
                <option value="">Java Developer (291)</option>
                <option value="">Data Mining</option>
              </select>
            </div>
            <div class="form-group">
              <label for="" >&nbsp; </label>
              <p>
                <button type="button" class="btn btn-success">Show</button>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> </div>
      </div><?php */?>
      <div class="">
        <div  class="tab-pane active"  id="list-view">
          <table class="table m-b-40 without-border" >
            <thead class="thead-default">
              <tr>
                <th  style="width: 10px;">Status</th>
                <th style="width: 150px;"> Work Order ID </th>
                <th>Candidate</th>
                <th>Onboarding Date</th>
                <th>Location</th>
                <th>Job </th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="subdata">
              <?php 
				  if($models){
				  foreach($models as $model){
					  //print_r($model);
					  $candidates = Candidates::model()->findByPk($model->candidate_id);
					$workorder = Workorder::model()->findByPk($model->workorder_id);
				  $jobs = Job::model()->findByPk($model->job_id);
					$offer = Offer::model()->findByPk($model->offer_id);
					$location = Location::model()->findByPk($offer->approver_manager_location);
					$workorderStatus = UtilityManager::workorderStatus();
					if($workorder->workorder_status == 1){
						$className ="label-re-open";
					}else if($workorder->workorder_status == 2){
						$className ="label-pending-aproval";
					}elseif($workorder->workorder_status == 0){
						$className ="label-rejected";
					}elseif($workorder->workorder_status == 3){
						$className ="label-re-open";
					}else{
						$className ="label-new-request";
					}
 				  ?>
              <tr>
                <td>
                     <?php if($model->termination_status == 2 && strtotime($model->termination_date) <= time()){ ?>
                    	<span class="tag label-rejected">Closed</span>
                    <?php }elseif($model->ext_status == 2 ){?>
                    	<span class="tag label-re-open">Open</span>
                    	<span class="tag label-rejected">Renewed</span>
                    <?php }else {?>
                    	<span class="tag <?php echo $className;?>"><?php echo  $workorderStatus[$workorder->workorder_status];?></span>
                    <?php }?>
                </td>
                
                <td><a href=""><?php echo $model->workorder_id; ?></a></td>
                <td><?php echo $candidates->first_name.' '.$candidates->last_name; ?></td>
                <td><?php echo date('F d,Y',strtotime($model->start_date)); ?></td>
                <td><?php echo $location->name; ?></td>
                <td><a href=""><?php echo $jobs->title.' ('.$jobs->id.')'; ?></a></td>
                <td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('contract/contractView', array('id' => $model->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
              </tr>
              <?php } }else{ ?>
              <tr>
                <td colspan="6"> Sorry No Record Found.. </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- list-view -->
        
        <!--<div  class="tab-pane"  id="stack-view">
          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>-->
        <!-- list-view --> 
        
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="clearfix" style="padding: 10px 20px 10px; ">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <?php
		$this->widget('CLinkPager',
		  array('pages' => $pages,
				'header' => '',
				'nextPageLabel' => 'Next',
				'prevPageLabel' => 'Prev',
				'selectedPageCssClass' => 'active',
				'hiddenPageCssClass' => 'disabled',
				'htmlOptions' => array('class' => 'pagination m-t-0 m-b-0',)))
		?>
    </div>
  </div>
  <div class="clearfix" style="padding: 10px 20px 30px; ">
    <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h5 class="m-b-10">Quick Note</h5>
      <p class="m-b-10"> <span class="tag label-re-open">Completed</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-open">Open</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-hold">Submitted</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-rejected">MSP Review</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-new-request">Shortlisted</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-filled">Review</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-open">Interview</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-pending-aproval">Offer</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-re-open">WorkOrder Release</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-new-request">Approved</span> Lorem ipsum dolor sit amet. </p>
    </div>-->
  </div>
  <div class="seprater-bottom-100"></div>
</div>
<script>
    $(document).ready(function(){
        /*$("input").keydown(function(){
         $("input").css("background-color", "yellow");
         });*/
        $('#sdata').keyup(function(){
            var subValue=$(this).val();
            $.ajax({
                'url':'<?php echo $this->createUrl('costCenter/contractSearch') ?>',
                type: "POST",
                data: { subValue: subValue},
                'success':function(html){
                    //alert(html);
                    $("#subdata").html(html)
                }
            });
            //$("input").css("background-color", "pink");
        });
    });
</script>