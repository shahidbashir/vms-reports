<?php 
	$workOrderExibit = OfferExhibit::model()->findAllByAttributes(array('offer_id'=>$workorder->id),array('order'=>'id desc'));
?>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="clearfix">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4 class="m-b-20 h4-small">Exhibit</h4>
        <!--<div class="wel m-b-20 m-t-10 well-yellow">
          <p class="m-t-10 m-b-10">Exhibit Verification was completed on 10th May 2015 at 11:30</p>
        </div>-->
      </div>
    </div>
    <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
      <thead>
        <tr>
          <th>S.No</th>
          <th>Document Name</th>
          <th>Date of Upload</th>
          <th style="text-align: right; padding-right: 30px;"> Actions </th>
        </tr>
      </thead>
      <tbody>
      <?php if($workOrderExibit){ $i = 1; foreach($workOrderExibit as $value){
		  $exhibit = ClientExhibit::model()->findByPk($value->exhibit_name);
		  ?>
        <tr>
          <td> <?php echo $i; ?> </td>
          <td> <?php echo $exhibit->name; ?> </td>
          <td> <?php echo date('F d,Y',strtotime($value->date_created)); ?><!--14-Aug-2016--> </td>
          <td style="text-align: right; padding-right: 30px;">
          	<a href="<?php echo $this->createAbsoluteUrl('offer/downloadExhibit',array('id'=>$value->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a> 
         	<?php /*?><a href="job-info.html" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg"></i></a><?php */?>
          </td>
        </tr>
      <?php  $i++; } }else{ ?>
      <tr>
        <td colspan="4">
          Sorry no Record Found
        </td>
      </tr>
      <?php }?>
      </tbody>
    </table>
  </div>
  </br>
  </br>
  <?php if($model->hellosign_signer1_status==1 || $model->hellosign_signer2_status==1){
    if( date('Y',strtotime($model->hellosign_signer2_date))!='1970'){
     $date =  date('F j,Y H:i:s',strtotime($model->hellosign_signer2_date));
    }else{
      $date =  date('F j,Y H:i:s',strtotime($model->hellosign_signer1_date));
    }
    ?>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="clearfix">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        </br>
        <h4 class="m-b-20 h4-small">Digital Signed Document</h4>

        <!--<div class="wel m-b-20 m-t-10 well-yellow">
          <p class="m-t-10 m-b-10">Exhibit Verification was completed on 10th May 2015 at 11:30</p>
        </div>-->
      </div>
    </div>
    <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
      <thead>
      <tr>
        <th>S.No</th>
        <th>Document Name</th>
        <th>Signed On</th>
        <th style="text-align: right; padding-right: 30px;"> Actions </th>
      </tr>
      </thead>
      <tbody>
        <tr>
          <td> 1</td>
          <td> <?php echo 'OnBoard'; ?> </td>
          <td> <?php echo $date; ?>  </td>
          <td style="text-align: right; padding-right: 30px;">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/digitalDocs/downloadContractDigitalDoc',array('id'=>$model->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Download"><i class="fa fa-download" style="color:#cfcfcf"></i></a>
          </td>
        </tr>
        <?php $offer = Offer::model()->findByPk($model->offer_id) ?>
        <tr>
          <td> 2</td>
          <td> <?php echo 'Offer'; ?> </td>
          <td> <?php if(date('m-d-Y',strtotime($offer->doc_signed_date)=='01-01-1970')){ echo 'NULL'; }else{ echo date('F d,Y',strtotime($offer->doc_signed_date)); }; ?>  </td>
          <td style="text-align: right; padding-right: 30px;">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/digitalDocs/downloadOfferDigitalDoc',array('id'=>$offer->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Download"><i class="fa fa-download" style="color:#cfcfcf"></i></a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <?php } ?>
</div>
