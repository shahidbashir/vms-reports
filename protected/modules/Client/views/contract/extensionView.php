<?php 
	$this->pageTitle = 'Contract Extension View'; 
	$reason = Setting::model()->findByPk($contractExtReq->reason_of_extension);
	//echo '<pre>';print_r($contract);exit;
 ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Extension Veiw <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractExtensionList'); ?>" class="btn btn-sm btn-default pull-right"> Back to Extensions</a> </h4>
      
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
          <div class="simplify-tabs blue-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <li class="nav-item"> <a class="nav-link active"  href="#submission" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Extension</a> </li>
              </ul>
              <div class="tab-content   p-r-0 p-l-0">
                <div class="tab-pane active" id="submission"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table class="table tab-table m-b-20">
                        <tbody>
                          <tr>
                            <td>Reason for Extention:</td>
                            <td><?php echo $reason->title; ?></td>
                          </tr>
                          <tr>
                            <td>Notes :</td>
                            <td><?php echo $contractExtReq->note_of_extension; ?></td>
                          </tr>
                          <tr>
                            <th>New end date of contract:</th>
                            <th><?php echo date('m/d/Y',strtotime($contractExtReq->new_contract_end_date)); ?></th>
                          </tr>
                          <?php if($contract->ext_vendor_approval==2){ ?>
                          		<tr>
                                  <td>Pay Rate:</td>
                                  <td><?php echo $workOrder->wo_pay_rate; ?></td>
                                </tr>
                                <tr>
                                  <td>Bill Rate:</td>
                                  <td><?php echo $workOrder->wo_bill_rate; ?></td>
                                </tr>
                                <tr>
                                  <td>Over Time Rate:</td>
                                  <td><?php echo $workOrder->wo_over_time; ?></td>
                                </tr>
                                <tr>
                                  <td>Double Time Rate:</td>
                                  <td> <?php echo $workOrder->wo_double_time; ?></td>
                                </tr>
                           <?php }else { ?>
                                <tr>
                                  <td>Pay Rate:</td>
                                  <td><?php echo $contractExtReq->pay_rate; ?></td>
                                </tr>
                                <tr>
                                  <td>Bill Rate:</td>
                                  <td><?php echo $contractExtReq->bill_rate; ?></td>
                                </tr>
                                <tr>
                                  <td>Over Time Rate:</td>
                                  <td><?php echo $contractExtReq->overtime_payrate; ?></td>
                                </tr>
                                <tr>
                                  <td>Double Time Rate:</td>
                                  <td> <?php echo $contractExtReq->doubletime_payrate; ?></td>
                                </tr>
                               
                             <?php } ?>
                              <tr>
                                  <td>Total Estimated Cost:</td>
                                  <td> <?php echo $contractExtReq->new_estimate_cost; ?></td>
                              </tr>
                              <tr>
                                <td>Current Contract:</td>
                                <td><?php echo date('m-d-Y',strtotime($workOrder->onboard_changed_start_date)); ?> to <?php echo date('m-d-Y',strtotime($workOrder->onboard_changed_end_date)); ?></td>
                              </tr>
                          <tr>
                            <td>On Boarding Date :</td>
                            <td><?php echo date('m-d-Y',strtotime($contractExtReq->date_created)); ?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
          </div>
          <!-- simplify-tabs --> 
          
        </div>
        <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
          <div class="simplify-tabs m-t-20">
            <div role="tabpanel"> 
               <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <li class="nav-item"> <a class="nav-link active"  href="#rejected" role="tab" data-toggle="tab" data-placement="bottom" title="Rejected"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Action for Extension</a> </li>
                <?php if($contractExtReq->bill_request_status != 0 ) {?>
                <li class="nav-item"> <a class="nav-link"  href="#skills" role="tab" data-toggle="tab" data-placement="bottom" title="Skills"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Bill Request</a> </li>  
                <?php }?>
                <li class="nav-item"> <a class="nav-link"  href="#comments" role="tab" data-toggle="tab" data-placement="bottom" title="Notes & Comment"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Notes & Comment</a> </li>
                <?php if($contract->ext_vendor_approval==2){ ?>
                        <li class="nav-item"  onclick="workFlowById(<?php echo $contract->contract_work_flow;?>,<?php echo $contractExtReq->id?>,<?php echo $contract->id ?>)"> <a class="nav-link"  href="#workflow" role="tab" data-toggle="tab" data-placement="bottom" title="workflow"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/work-flow-approval@512px.svg"></i>Workflow Approval</a> </li>
                  <?php }?>
              </ul>
              <div class="tab-content ">
                <div class="tab-pane active" id="rejected"  role="tabpanel">
                  <div class="row m-t-10">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <!--<p class="m-b-20">Please accept the extension request.</p>-->
                      <?php if($contract->ext_vendor_approval==1){ ?>
                      	Waiting For Approval Of <strong><?php echo $vendor->first_name.' '.$vendor->last_name; ?></strong>.
                      <?php }else if($contract->ext_vendor_approval==2){ ?>
                      	Contract Extension Is Approved By <strong><?php echo $vendor->first_name.' '.$vendor->last_name; ?></strong>.
                      <?php }else{ ?>
                      	Contract Extension Is Rejected By <strong><?php echo $vendor->first_name.' '.$vendor->last_name; ?></strong>.
                      <?php } ?>
                      <!--It was accepted / rejected by the person on date.-->
                      
                    </div>
                  </div>
                </div>
                <?php if($contractExtReq->bill_request_status != 0 ) {?>
                	<div class="tab-pane  m--31" id="skills"  role="tabpanel">
                  <div class="clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="well">
                        <p class="m-b-20">Request new bill Rate</p>
                        <div class="row">
                          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                              <label for="">Request new bill rate</label>
                              <input type="text" class="form-control" id="" value="<?php echo $contractExtReq->vendor_bill_rate;?>" placeholder="" readonly="readonly">
                            </div>
                          </div>
                          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                              <label for="">Pay Rate</label>
                              <input type="text" class="form-control" id="" value="<?php echo $contractExtReq->vendor_pay_rate;?>" placeholder="" readonly="readonly">
                            </div>
                          </div>
                          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                              <label for="">Over Time</label>
                              <input type="text" class="form-control" id="" value="<?php echo $contractExtReq->vendor_overtime_payrate;?>" placeholder="" readonly="readonly">
                            </div>
                          </div>
                          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                              <label for="">Double Time</label>
                              <input type="text" class="form-control" id="" value="<?php echo $contractExtReq->vendor_doubletime_payrate;?>" placeholder="" readonly="readonly">
                            </div>
                          </div>
                        </div>
                        <div class="row m-b-20">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <p>Extension Duration</p>
                            <p><?php echo date('m-d-Y',strtotime($workOrder->onboard_changed_start_date)); ?> to <?php echo date('m-d-Y',strtotime($workOrder->onboard_changed_end_date)); ?></p>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <p>Total Estimated Cost</p>
                            <p>$<?php echo $contractExtReq->vendor_estimate_cost;?></p>
                          </div>
                        </div>
                        <?php 
							$clientInfo = Client::model()->findByPk($contractExtReq['bill_req_apprej_by']);
							if(empty($clientInfo))
								$clientInfo = Admin::model()->findByPk($contractExtReq['bill_req_apprej_by']);
							if($contractExtReq->bill_request_status == 2 || $contractExtReq->bill_request_status == 3) {?>
                              <p>It was <?php if($contractExtReq->bill_request_status == 2) echo 'accepted';?>
							  <?php if($contractExtReq->bill_request_status == 3) echo 'rejected';?> by <?php echo '<strong>'.$clientInfo->first_name.' '.$clientInfo->last_name.'</strong>';?> on <?php echo date('m-d-Y',strtotime($contractExtReq->bill_req_rejection_date));?>.</p>
                        <?php }else{?>
                        <p> <a href="<?php echo $this->createAbsoluteUrl('contract/approveRejectBillRequest', array('id' => $contractExtReq->id)); ?>" class="btn btn-success">Accept</a> <a href="#modal-id" data-toggle="modal" class="btn btn-danger">Reject</a></p>
                        <?php }?>
                      <div style="margin-top:10px;">
                             <table width="100%" border="1" cellspacing="1" cellpadding="1">
                                <tr>
                                  <td align="center" valign="middle"><strong>By</strong></td>
                                  <td align="center" valign="middle"><strong>Bill Rate</strong></td>
                                  <td align="center" valign="middle"><strong>Pay Rate</strong></td>
                                  <td align="center" valign="middle"><strong>Over Time </strong></td>
                                  <td align="center" valign="middle"><strong>Double Time</strong></td>
                                </tr>
                                <tr>
                                  <td align="center" valign="middle">Client</td>
                                  <td align="center" valign="middle">$<?php echo $contractExtReq->bill_rate;?></td>
                                  <td align="center" valign="middle">$<?php echo $contractExtReq->pay_rate;?></td>
                                  <td align="center" valign="middle">$<?php echo $contractExtReq->overtime_payrate;?></td>
                                  <td align="center" valign="middle">$<?php echo $contractExtReq->doubletime_payrate;?></td>
                                </tr>
                                <tr>
                                  <td align="center" valign="middle">Vendor</td>
                                  <td align="center" valign="middle">$<?php echo $contractExtReq->vendor_bill_rate;?></td>
                                  <td align="center" valign="middle">$<?php echo $contractExtReq->vendor_pay_rate;?></td>
                                  <td align="center" valign="middle">$<?php echo $contractExtReq->vendor_overtime_payrate;?></td>
                                  <td align="center" valign="middle">$<?php echo $contractExtReq->vendor_doubletime_payrate;?></td>
                                </tr>
                              </table>
                          </div>
                      </div>
                       <!-- well --> 
                     </div>
                  </div>
                  <!-- row --> 
                 </div>
                <?php }?>
                 <div class="tab-pane " id="comments"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="interview-notes-comments ">
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <?php if(Yii::app()->user->hasFlash('success')){
                              echo Yii::app()->user->getFlash('success');
                            }
                            if(Yii::app()->user->hasFlash('error')){
                              echo Yii::app()->user->getFlash('error');
                            }?>

                            <div class="media no-border"> <a class="pull-left" href="#"> <img class="media-object" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/avatar.jpg" alt="Image"> </a>
                              <div class="media-body">
                                <?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'job-form',
                                    'enableAjaxValidation'=>false,
                                    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                                ));
                                ?>

                                <textarea name="comment" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                                <div class="form-group m-b-10 m-t-20">
                                  <label data-toggle="modal" class="a-blue">Attachement</label>
                                  <?php echo $form->fileField($model,'media',array('rows'=>5, 'class'=>'form-control')); ?>
                                </div>
                                <div class="be-checkbox">
                                  <input id="check1" name="is_public" type="checkbox">
                                  <label for="check1">Share the notes with Client</label>
                                </div>
                                <button type="submit" name="ExtensionComments" class="btn btn-success m-t-10">Add Comment</button>
                                <?php $this->endWidget(); ?>
                              </div>
                            </div>
                            <!-- media -->
                            <?php
                            foreach($comments as $comment){
                              $userInfo = Vendor::model()->findByPk($comment['user_id']);
                              $clientinfo = Client::model()->findByPk($comment['user_id']);
                              if(!empty($userInfo)){
                                $name = $userInfo['first_name'].' '.$userInfo['last_name'];
                              }
                              if(!empty($clientinfo)){

                                $name =  $clientinfo['first_name'].' '.$clientinfo['last_name'];
                              }
                              ?>
                              <div class="media no-border"> <a class="pull-left" href="#">
                                  <?php if($userInfo['profile_image'] != '') { ?>
                                    <img class="media-object" src="<?php echo Yii::app()->baseUrl; ?>'/new-theme/template-assets/images/avatar.jpg" alt="<?php echo $userInfo['first_name'].' '.$userInfo['last_name'] ;?>">
                                  <?php }else{?>
                                  <img class="media-object" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/avatar.jpg" alt="<?php echo $userInfo['first_name'].' '.$userInfo['last_name'] ;?>"> </a>
                                <?php }?>
                                <div class="media-body">
                                  <p class="m-b-10 bold"><?php echo $userInfo['first_name'].' '.$userInfo['last_name'] ;?></p>
                                  <p>
                                    <?php echo $comment['comment'];?>
                                  </p>
                                  <?php if($comment['media']){?>
                                  <div class="attachment">
                                        <p>Attachments</p>
                                      <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/downloadExtensionNote',array('id'=>$comment['id'])); ?>" class=""><?php echo $comment['media']; ?><i class="fa fa-cloud-download"></i></a>
                                   </div>
                                   <?php }?>
                                  <p class="meta-inner">Posted <?php echo date('l \a\t H:ia' ,strtotime($comment['date_added']));?></p>
                                </div>
                              </div>
                              <!-- media -->
                            <?php }?>
                            <!-- media -->

                          </div>
                          <!-- col -->
                        </div>
                        <!-- row -->
                      </div>
                      <!-- interview-notes-comments -->

                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="workflow"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    	<div id="workflow">
                        </div>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
            
          </div>
          <!-- simplify-tabs --> 
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<div class="modal fade" id="modal-id">
  <div class="modal-dialog">
    <form action="<?php echo $this->createAbsoluteUrl('contract/approveRejectBillRequest', array('id' => $contractExtReq->id)) ?>" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Reject Extension</h4>
          </div>
          <div class="modal-body">
    
    
            <div class="form-group">
              <label for="">Reason for Bill Request Rejection</label>
              <select name="reason" id="reason" class="form-control" required="required">
                 <?php
                $reason = Setting::model()->findAll(array('condition'=>'category_id=63'));
                foreach($reason as $reason){ ?>
                  <option value="<?php echo $reason->id; ?>"><?php echo $reason->title; ?></option>
                  <?php  } 	?>
              </select>
            </div>
            
            
            <div class="form-group">
              <label for="">Note for Rejection</label>
             <textarea name="note" id="note" class="form-control" rows="3" required="required"></textarea>
            </div>
            
          </div>
          <div class="modal-footer">
            <input type="hidden" value="<?php  echo $contractExtReq->id ;?>" name="id" />
            <button type="submit" name="rejectbill" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
    </form>
  </div>
</div>
<script type="text/javascript">
 	function workFlowById(id,extensionId, contractId){
 		$.ajax({
				'url':'<?php echo $this->createUrl('contract/ContractWflowById') ?>',
				type: "POST",
				data: { id: id , extension_id : extensionId, contract_id : contractId},
				'success':function(rep){
					$("#workflow").html(rep);
				}
			});
		}
</script>