<?php
$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
$jobs = Job::model()->findByPk($model->job_id);
$clientRateData = ClientRate::model()->findByAttributes(array('client_id'=>$jobs->user_id));
$over_time = 0;
$half_time = 0;
$double_time = 0;
if($clientRateData){
    $over_time = $clientRateData->over_time;
    $double_time = $clientRateData->double_time;
}
$jobs = Job::model()->findByPk($model->job_id);
$submissionData = VendorJobSubmission::model()->findByPk($model->submission_id);
$categoryID = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$jobs->cat_id));
$mark_up = $submissionData->makrup;
$workorder = Workorder::model()->findByPk($model->workorder_id);
$allTeamMembers = Yii::app()->db->createCommand('SELECT * FROM `vms_client` Where super_client_id='.$jobs->user_id.' or (id='.$loginUserId.' and super_client_id=0)')->query()->readAll();
$jobLocations = Location::model()->findAllByAttributes(array('id'=>unserialize($jobs->location)));
$costcenter = CostCenter::model()->findAllByAttributes(array('client_id'=>$jobs->user_id));

?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="clearfix">
        
        </div>


                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><?php echo $candidates->first_name.' '.$candidates->last_name; ?></h4>
                    </div>
                    <div class="modal-body">
                        <form id="client-form" method="post">
                            <input type="hidden" readonly name="payment_type" id="payment_type" class="form-control" value="<?php echo $jobs->payment_type; ?>" required="required">
                            <input type="hidden" readonly name="hours_per_week" id="hours_per_week" class="form-control" value="<?php echo $jobs->hours_per_week; ?>" required="required">
                            <input type="hidden" readonly name="shift" id="shift" class="form-control" value="<?php echo $jobs->shift; ?>" required="required">
                            <!--on ajax call find the vendor highest markup and fill its value in this field-->
                            <input type="hidden" name="Job[vendor_client_markup]" id="vendor_client_markup123" class="form-control" value="<?php echo $mark_up; ?>">
                            <input type="hidden" id="calculated_value" name="calculated_value" value="" class="form-control"  />
                            <input type="hidden" name="contract_id" value="<?php echo $model->id; ?>">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="">Bill Rate</label>
                                        <input class="form-control" id="bill_rate" name="bill_rate" type="text" value="<?php echo $workorder->wo_bill_rate; ?>" required="required" onchange='calculate("billrate")'>
                                    </div>
                                </div>
                                <!-- col-12 -->

                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="" class="">Pay Rate </label>
                                        <input class="form-control" id="pay_rate" name="pay_rate" type="text" value="<?php echo $workorder->wo_pay_rate; ?>" required="required" onchange="calculate('payrate')" readonly="readonly">
                                    </div>
                                    <!-- col-12 -->

                                </div>

                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="">Over Time</label>
                                        <input class="form-control" id="over_time" readonly="readonly" name="over_time" type="text" value="<?php echo $workorder->wo_over_time; ?>">
                                    </div>
                                </div>
                                <!-- col-12 -->

                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="" class="">Double Time </label>
                                        <input class="form-control" name="double_time" id="double_time" readonly="readonly" type="text" value="<?php echo $workorder->wo_double_time; ?>">
                                    </div>
                                    <!-- col-12 -->

                                </div>
                                <!-- row -->

                                <div class="clearfix">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="">Client Over Time</label>
                                            <input class="form-control" id="client_over_time" readonly="readonly" name="client_over_time" type="text" value="<?php echo $workorder->wo_client_over_time; ?>">
                                        </div>
                                    </div>
                                    <!-- col-12 -->

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="" class="">Client Double Time</label>
                                            <input class="form-control" id="client_double_time" readonly="readonly" name="client_double_time" type="text" value="<?php echo $workorder->wo_client_double_time; ?>">
                                        </div>
                                    </div>
                                    <!-- col-12 -->

                                </div>
                                <!-- row -->
                               
                                <div class="clearfix">
                                  <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                      <label for="">Location</label>
                                      <select name="approver_manager_location" id="approver_manager_location" class="form-control" required>
                                        <option value=""></option>
                                        <?php  foreach($jobLocations as $jobLocations){ ?>
                                        <option value="<?php echo $jobLocations->id; ?>" <?php echo $offer->approver_manager_location==$jobLocations->id?'selected':''; ?>><?php echo $jobLocations->name; ?></option>
                                        <?php }  ?>
                                      </select>
                                    </div>
                                  </div>
                                  <!-- col-12 -->
                                  
                                  <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                      <label for="" class="">Location Manager</label>
                                      <select name="location_manager" id="location_manager" class="form-control" >
                                        <option value="<?php echo $offer->location_manager; ?>"><?php echo $offer->location_manager; ?></option>
                                      </select>
                                    </div>
                                  </div>
                                  <!-- col-12 --> 
                                </div>
								<?php //echo $workorder->wo_cost_center; ?>
                                <div class="clearfix">
                                    <?php /*?><div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="">Cost Center</label>
                                            <!--<input type="text" class="contractEdit">-->
                                            <select  name="cost_center[]" id="cost_center" multiple="" class="select2">
											  <?php if($costcenter){
                                                    foreach($costcenter as $costdata){
                                                    ?>
                                              <option value="<?php echo $costdata['id']; ?>"><?php echo $costdata['cost_code']; ?></option>
                                              <?php } } ?>
                                            </select>
                                        </div>
                                    </div><?php */?>
                                     
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="" class="">Time Sheet Approving Manager</label>
                                            <select name="approval_manager" id="input" class="form-control select2" required="required" readonly="readonly">
                                                <option value=""> Select Time Sheet Approving Manager</option>
                                                <?php foreach($allTeamMembers as $allMembers){ ?>
                                                    <option value="<?php echo $allMembers['id']; ?>" <?php if($allMembers['id']==$workorder->approval_manager){ echo "Selected";} ?>><?php echo $allMembers['first_name'].' '.$allMembers['last_name'].'('.$allMembers['member_type'].')'; ?></option>
                                                <?php } ?>
                                            </select>                 </div>
                                    </div>
                                    <!-- col-12 -->

                                </div>
                                <!-- row -->

                                <div class="clearfix">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="">Start Date</label>
                                            <input type="text" name="start_date" class="form-control datetimepickermodified dateField123" value="<?php echo date('m/d/Y',strtotime($workorder->onboard_changed_start_date)); ?>">

                                        </div>
                                    </div>
                                    <!-- col-12 -->

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="" class="">End Date</label>
                                            <input type="text" name="end_date" class="form-control datetimepickermodified dateField1234" value="<?php echo date('m/d/Y',strtotime($workorder->onboard_changed_end_date)); ?>">
                                        </div>
                                    </div>
                                    <!-- col-12 -->

                                </div>
                                <!-- row -->

                                <div class="clearfix m-t-20">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <button type="submit" name="ImportData" class="btn btn-primary">Save changes</button>
                                    </div> <!-- col -->
                                </div> <!-- row -->

                                <br>
                                <br>
                            </div>
                        </form>
                    </div>
                </div>


    </div>
</div>
<script>
    //calculating rates of per hour job.
    function calculate(type){
        var Job_bill_rate = document.getElementById("bill_rate").value;
        var Job_pay_rate = document.getElementById("pay_rate").value;
        var Job_payment_type = document.getElementById("payment_type").value;
        var Job_hours_per_week = document.getElementById("hours_per_week").value;
        var firstDate = $(".dateField123").val();
        var secondDate = $(".dateField1234").val();
        var markup = document.getElementById("vendor_client_markup123").value;
        //var bill_rate_hidden = document.getElementById("bill_rate_hidden").value;
        //Usage
        var startDate = new Date(firstDate);
        var endDate = new Date(secondDate);
        var numOfDates = getBusinessDatesCount(startDate,endDate);
        //alert(numOfDates);
        var str = Job_hours_per_week;
        var numberHours = str.split(" ",1);
        var hoursPerDay = numberHours/5;
        var str = Job_hours_per_week;
        var numberHours = str.split(" ",1);
        var estimatedSallary = Job_pay_rate * numOfDates * hoursPerDay;
        if(type == 'billrate'){
            var pay_rate = Job_bill_rate * 100/( 100 + parseFloat(markup));
            //rounded value of pay rate
            var r_payrate = pay_rate.toFixed(3);
            document.getElementById("pay_rate").value = r_payrate;
            <!--client bill rate-->
            document.getElementById("client_over_time").value = (parseFloat(Job_bill_rate) + parseFloat(Job_bill_rate * <?php echo $over_time ?>/100)).toFixed(3);
            document.getElementById("client_double_time").value = (parseFloat(Job_bill_rate) + parseFloat(Job_bill_rate * <?php echo $double_time ?>/100)).toFixed(3);
            document.getElementById("over_time").value = (parseFloat(r_payrate) + parseFloat(r_payrate * <?php echo $over_time ?>/100)).toFixed(3);      	  
			document.getElementById("double_time").value = (parseFloat(r_payrate) + parseFloat(r_payrate * <?php echo $double_time ?>/100)).toFixed(3);
        }
		/*else if(type == 'payrate'){
            var bill_rate = parseFloat(Job_pay_rate) + (parseFloat(Job_pay_rate) * parseFloat(markup)/100);
            document.getElementById("bill_rate").value = bill_rate.toFixed(3);
            //document.getElementById("pre_total_estimate_cost").innerHTML = estimatedSallary +' USD for the Period of '+firstDate+' to '+secondDate;
            document.getElementById("estimate_cost").value = estimatedSallary;
            <!--client bill rate-->
            document.getElementById("client_over_time").value = (parseFloat(bill_rate) + parseFloat(bill_rate * <?php //echo $over_time ?>/100)).toFixed(3);
            document.getElementById("client_double_time").value = (parseFloat(bill_rate) + parseFloat(bill_rate * <?php //echo $double_time ?>/100)).toFixed(3);
            document.getElementById("over_time").value = (parseFloat(Job_pay_rate) + parseFloat(Job_pay_rate * <?php //echo $over_time ?>/100)).toFixed(3);
            document.getElementById("double_time").value = (parseFloat(Job_pay_rate) + parseFloat(Job_pay_rate * <?php //echo $double_time ?>/100)).toFixed(3);
        }*/
    }
    calculate();
    function getBusinessDatesCount(startDate, endDate) {
        var count = 0;
        var curDate = startDate;
        while (curDate <= endDate) {
            var dayOfWeek = curDate.getDay();
            if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
                count++;  curDate.setDate(curDate.getDate() + 1);
        }    return count;  }
</script>