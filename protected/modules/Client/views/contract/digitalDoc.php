<?php    $this->pageTitle =  'Digital Document';?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px 10px; ">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-b-10 page-title">List of Offer Digital Document <!--<a href="" class="pull-right"><i class="fa fa-question"></i></a> <a href="" class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
            <p class="m-b-20">All offer digital signed document are stored here.</p>
            <div class="search-box">
                <form name="search" method="post">
                <div class="two-fields">
                    <div class="form-group">
                        <label for=""></label>
                        <input type="text" name="candidate_name" class="form-control" id="search" placeholder="Search Offers">
                    </div>
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="row m-b-20">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                </div>
            </div>
            <!-- row -->
            <div class="simplify-tabs m-b-50">

                    <div class="tab-content ">
                        <div class="tab-pane active" id="home" role="tabpanel">
                            <div class="row table-row-to-remove-margin">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <table class="table m-b-40 without-border">
                                        <thead class="thead-default">
                                        <tr>
                                            <th style="width: 120px;"><a href="">Job ID</a></th>
                                            <th>Name</th>
                                            <th>Job Name</th>
                                            <th>Type</th>
                                            <th>Vendor Name</th>
                                            <th>Signed On</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($digitalDoc) {
                                            foreach ($digitalDoc as $value) {
                                                $jobModel = Job::model()->findByPk($value['jobId']);
                                                ?>
                                                <tr>
                                                    <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$value['jobId'],array('type'=>'info')); ?>"><?php echo $value['jobId']; ?></a></td>
                                                    <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$value['sub_id'])); ?>"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></a></td>
                                                    <td><?php echo $jobModel->title; ?></td>
                                                    <td><?php echo 'Offer'; ?></td>
                                                    <td><?php echo  $value['v_organization']; ?></td>
                                                    <td><?php if(date('m-d-Y',strtotime($value['doc_signed_date'])=='01-01-1970')){ echo 'NULL'; }else{ echo date('F d,Y',strtotime($value['doc_signed_date'])); } ?></td>
                                                    <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/digitalDocs/downloadOfferDigitalDoc',array('id'=>$value['offerId'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Download"><i class="fa fa-download" style="color:#cfcfcf"></i></a>
                                                    </td>
                                                </tr>
                                            <?php }}else{ ?>
                                            <tr>
                                                <td colspan="9">There is no document signed</td>
                                            </tr>
                                        <?php }?>
                                        </tbody>
                                    </table>
                                    <div class="row m-b-10" style="padding: 10px 0 10px; ">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <?php
                                            $this->widget('CLinkPager', array('pages' => $pages,
                                                'header' => '',
                                                'nextPageLabel' => 'Next',
                                                'prevPageLabel' => 'Prev',
                                                'selectedPageCssClass' => 'active',
                                                'hiddenPageCssClass' => 'disabled',
                                                'htmlOptions' => array('class' => 'pagination m-t-0 m-b-0',)));
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- row --> </div>
                        <!-- tab-pane --> </div>
            </div>
        </div>
        <!-- col --> </div>
    <!-- row -->
    <div class="clearfix" style="padding: 10px 20px 30px; ">
    </div>
    <div class="seprater-bottom-100"></div>
</div>
