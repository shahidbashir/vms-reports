 <?php $offercandidate = Candidates::model()->findByPk($offer->candidate_id);
	$offervendor = Vendor::model()->findByPk($offer->vendor_id);
	$location = Location::model()->findByPk($offer->approver_manager_location);
?>
                 <h4 class="m-b-20 m-t-10 h4-small p-l-15">Candidate Info</h4>
                  <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
                    <tbody>
                      <tr>
                        <td>Candidate Name</td>
                        <td><?php echo $offercandidate->first_name.' '.$offercandidate->last_name; ?></td>
                      </tr>
                      <tr>
                        <td>Current Location</td>
                        <td><?php echo $offercandidate->current_location; ?></td>
                      </tr>
                      <tr>
                        <td>Vendor Name</td>
                        <td><?php echo $offervendor->organization; ?></td>
                      </tr>
                    </tbody>
                  </table>
                  <br>
                  <h4 class="m-b-20 h4-small p-l-15">Job Start Date and Other Information</h4>
                  <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
                    <tbody>
                      <tr>
                        <td>Start Date</td>
                        <td>
							<?php 
							if(date('Y',strtotime($offer->job_start_date)) != '1970'){ 
							  echo date('F d,Y',strtotime($offer->job_start_date));
							  }else{
									echo 'Null';
								   }
							
							//echo $offer->job_start_date; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>End Date</td>
                        <td>
							<?php 
							if(date('Y',strtotime($offer->job_end_date)) != '1970'){ 
							  echo date('F d,Y',strtotime($offer->job_end_date));
							  }else{
									echo 'Null';
								   }
							//echo $offer->job_end_date; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Location</td>
                        <td><?php echo $location->name; ?></td>
                      </tr>
                      <tr>
                        <td>Location Manager</td>
                        <td><?php echo $offer->location_manager; ?></td>
                      </tr>
                    </tbody>
                  </table>
                  <br>
                  <h4 class="m-b-20 h4-small p-l-15">Rates</h4>
                  <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
                    <tbody>
                      <tr>
                        <td>Pay Rate</td>
                        <td><?php echo $offer->offer_pay_rate; ?></td>
                      </tr>
                      <tr>
                        <td>Bill Rate</td>
                        <td><?php echo $offer->offer_bill_rate; ?></td>
                      </tr>
                      <tr>
                        <td>Over Time Rate for Candidate</td>
                        <td><?php echo $offer->over_time; ?></td>
                      </tr>
                      <tr>
                        <td>Double Time Rate for Candidate</td>
                        <td><?php echo $offer->double_time; ?></td>
                      </tr>
                      <tr>
                        <td>Over Time Rate for Client</td>
                        <td><?php echo $offer->client_over_time; ?></td>
                      </tr>
                      <tr>
                        <td>Double Time Rate for Client</td>
                        <td><?php echo $offer->client_double_time; ?></td>
                      </tr>
                      <tr>
                        <td>Offer Expire Date</td>
                        <td><?php 
						if(date('Y',strtotime($offer->valid_till_date)) != '1970'){ 
						  echo date('F d,Y',strtotime($offer->valid_till_date));
						  }else{
								echo 'Null';
							   }
						
						//echo $offer->valid_till_date; ?></td>
                      </tr>
                    </tbody>
                  </table>
                  <br>
                  <h4 class="m-b-20 h4-small p-l-15">Notes &amp; Comments</h4>
                  <div class="simplify-tabs m-b-42">
                    <div role="tabpanel"> 
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs no-hover" role="tablist">
                        <li class="nav-item active"> <a class="nav-link "  href="#internal" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"></i> Internal</a> </li>
                        <li class="nav-item"> <a class="nav-link "  href="#msp" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"></i> MSP</a> </li>
                        <li class="nav-item"> <a class="nav-link "  href="#vendor" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"></i> Vendor</a> </li>
                      </ul>
                      <div class="tab-content ">
                        <div class="tab-pane active" id="internal"  role="tabpanel">
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="form-group">
                                <label for="">Note</label>
                                <textarea name="" id="input" class="form-control" rows="5" required>
                                	<?php echo $offer->internal_notes; ?>
                                </textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="msp"  role="tabpanel">
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="form-group">
                                <label for="">Note</label>
                                <textarea name="" id="input" class="form-control" rows="5" required>
                                	<?php echo $offer->for_admin_notes; ?>
                                </textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="vendor"  role="tabpanel">
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="form-group">
                                <label for="">Note</label>
                                <textarea name="" id="input" class="form-control" rows="5" required>
                                	<?php echo $offer->ref_notes_vendor; ?>
                                </textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- tabpanel --> 
                  </div>
                  <!-- simplify-tabs --> 