<?php $this->pageTitle = 'Contracts'; ?>

<style>
.testRowHide{ display:none;}
.testRowShow{ display:block;}
</style>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">Contract Termination <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractTermination'); ?>" class="btn btn-sm btn-default pull-right">Back to Contract Termination</a> </h4>
      <p class="m-b-20">Step to cancel contract.</p>
      <p class="m-b-20"> </p>
      <div class="search-box">
        <div class="form-group">
          <label for=""></label>
          <input type="text" class="form-control" id="autocomplete" placeholder="Search Contracts">
        </div>
      </div>
      <div class="row <?php  if($id != 0) echo 'testRowShow'; else echo 'testRowHide'; ?>" id="Termresponse" >
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <div class="card-2 ">
            <table class="table tab-table border-top-0 border-bottom-0">
              <tbody>
                <tr>
                  <td>Work Order ID:</td>
                  <td id="twoid">654654</td>
                </tr>
                <tr>
                  <td>Contract ID:</td>
                  <td id="tcid">6546</td>
                </tr>
                <tr>
                  <td>Bill Rates:</td>
                  <td id="tbrate">$ 45.00</td>
                </tr>
                <tr>
                  <td>Pay Rates:</td>
                  <td id="tprate">$ 45.00</td>
                </tr>
                <tr>
                  <td>Location:</td>
                  <td id="tlocation">$ 45.00</td>
                </tr>
                <tr>
                  <td>Job ID:</td>
                  <td id="tjid">654986</td>
                </tr>
                <tr>
                  <td>Vendor Name: </td>
                  <td id="tvendor">ABC</td>
                </tr>
                <tr>
                  <td>Start Date: </td>
                  <td id="tstart_date">ABC</td>
                </tr>
                <tr>
                  <td>End Date: </td>
                  <td id="tend_date">ABC</td>
                </tr>
                 <tr>
                  <td>Time Sheet Approval: </td>
                  <td id="manager_name">ABC</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <div class="card-2 p-20">
          <form method="post" >
            <p class="m-b-20 lg">Contract Termination</p>
            <div class="form-group">
              <label for="">Reason for Termination</label>
              <select class="form-control" name="reason_of_termination" required >
                  <option value=""></option>
                  <?php
                $location = Setting::model()->findAll(array('condition'=>'category_id=60'));
                foreach($location as $location){ ?>
                  <option value="<?php echo $location->id; ?>"><?php echo $location->title; ?></option>
                  <?php  } 	?>
                </select>
            </div>
            <div class="form-group">
              <label for="">Notes / Comments</label>
              <textarea name="termination_notes" id="input" class="form-control" rows="3" required="required"></textarea>
            </div>
            <div class="form-group">
              <label for="">Candidate Feedback</label>
              <textarea name="termination_can_feedback" id="input" class="form-control" rows="3" required="required"></textarea>
            </div>
            <div class="form-group">
              <label for="">Date of Termination / Contract Closing</label>
              <input type="text" name="termination_date" class="form-control singledate" id="" placeholder="">
            </div>
            
            <div class="form-group">
              <input type="hidden" name="contract_id" class="form-control" value="" id="tcontract_id" placeholder="">
            </div>
            
            <!--
            <p class="bold m-b-20">Candidate Feedback</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              -->
            <p class="m-t-20 m-b-20">
            	<button type="submit" name="terminationForm" class="btn btn-block btn-danger">Submit for Contract Closing</button> 
            </p>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  
  <div class="clearfix" style="padding: 10px 20px 30px; ">
     
  </div>
  <div class="seprater-bottom-100"></div>
</div>
<script type="text/javascript">
  jQuery(document).ready(function($) {
	$('#autocomplete').autocomplete({
		lookup: <?php $this->candidates(); ?>,
		onSelect: function (suggestion) {
			calculateTermination(suggestion.data);
		}
	});
	
	<?php if($id != 0) { ?>
			calculateTermination(<?php echo $id;?>)
	<?php }?>		
  });
</script>