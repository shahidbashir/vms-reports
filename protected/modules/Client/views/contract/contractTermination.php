<?php $this->pageTitle = 'Contracts'; ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">Contract Termination </h4>
      <p class="m-b-20">Step to cancel contract.</p>
      <p class="m-b-20"> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/terminationStep1'); ?>" class="btn btn-success">Contract Termination</a> </p>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="sdata" placeholder="Search Contracts">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <div class="">
        <div  class="tab-pane active"  id="list-view">
          <table class="table m-b-40 without-border" >
            <thead class="thead-default">
              <tr>
              	<th  style="width: 10px;">Status</th>
                <th  style="width: 10px;">S.No</th>
                <th  style="width: 10px;">ID</th>
                <th style="width: 150px;"> Candidate Name </th>
                <th>Job ID</th>
                <th>Date of Termination</th>
                <th>Location </th>
                <th>Vendor Name</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="subdata">
            <?php 
			  if($models){
				  $i = 1;
			  foreach($models as $model){
 				$candidates = Candidates::model()->findByPk($model->candidate_id);
				$vendor = Vendor::model()->findByPk($model->vendor_id);
				$workorder = Workorder::model()->findByPk($model->workorder_id);
			    $jobs = Job::model()->findByPk($model->job_id);
				$offer = Offer::model()->findByPk($model->offer_id);
				$location = Location::model()->findByPk($offer->approver_manager_location);
				
				$workorderStatus = UtilityManager::terminationStatus();
				if($model->termination_status == 1){
					$className ="label-re-open";
				}else if($model->termination_status == 2){
					$className ="label-pending-aproval";
				}elseif($model->termination_status == 3){
					$className ="label-rejected";
				}else{
					$className ="label-new-request";
				} 
				?>
              <tr>
              	<td><span class="tag <?php echo $className;?>"><?php echo  $workorderStatus[$model->termination_status];?></span></td>
                <td><?php echo $i; ?></td>
                <td><?php echo $model->id; ?></td>
                <td><a href=""><?php echo $candidates->first_name.' '.$candidates->last_name; ?></a></td>
                <td> <?php echo $jobs->title.' ('.$jobs->id.')'; ?> </td>
                <td><?php echo $model->termination_date; ?></td>
                <td><?php echo $location->name; ?></td>
                <td><a href=""><?php echo $vendor->organization. ' ( '.$vendor->first_name.' '.$vendor->last_name.' )'; ?></a></td>
                <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/terminationView',array('id'=>$model->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
              </tr>
              <?php $i++; } }else{ ?>
              <tr>
                <td colspan="6"> Sorry No Record Found.. </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- list-view -->
        
        <div  class="tab-pane"  id="stack-view">
          
        </div>
        <!-- list-view --> 
        
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="clearfix" style="padding: 10px 20px 10px; ">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <?php
		$this->widget('CLinkPager',
		  array('pages' => $pages,
				'header' => '',
				'nextPageLabel' => 'Next',
				'prevPageLabel' => 'Prev',
				'selectedPageCssClass' => 'active',
				'hiddenPageCssClass' => 'disabled',
				'htmlOptions' => array('class' => 'pagination m-t-0 m-b-0',)))
		?>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    </div>
  </div>
  <div class="clearfix" style="padding: 10px 20px 30px; ">
   </div>
  <div class="seprater-bottom-100"></div>
</div>
<script>
    $(document).ready(function(){

        $('#sdata').keyup(function(){
            var subValue=$(this).val();
            $.ajax({
                'url':'<?php echo $this->createUrl('costCenter/terminationSearch') ?>',
                type: "POST",
                data: { subValue: subValue},
                'success':function(html){
                    //alert(html);
                    $("#subdata").html(html)
                }
            });
        });
    });
</script>