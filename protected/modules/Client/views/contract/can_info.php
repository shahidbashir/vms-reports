 <?php $offercandidate = Candidates::model()->findByPk($offer->candidate_id);
	$vendor = Vendor::model()->findByPk($offercandidate->emp_msp_account_mngr);
	$location = Location::model()->findByPk($offer->approver_manager_location);
	$contract = Contract::model()->findByAttributes(array('workorder_id'=>$workorder->id));
	$modelSetting = Setting::model()->findByPk($contract->candidate_dept);
?>
                 <h4 class="m-b-20 m-t-10 h4-small p-l-15">Candidate Info</h4>
                  
                  <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
                    <tbody>
                      <tr>
                        <td>Candidate Name</td>
                        <td><?php echo $offercandidate->first_name.' '.$offercandidate->last_name; ?></td>
                      </tr>
                      <tr>
                        <td>Personal Email Address</td>
                        <td><?php echo $offercandidate->email; ?></td>
                      </tr>
                      <tr>
                        <td>Account Manager</td>
                        <td><?php echo $vendor->first_name.' '.$vendor->last_name; ?></td>
                      </tr>
                      <tr>
                        <td>Job Title</td>
                        <td><?php echo $jobData->title; ?></td>
                      </tr>
                      <tr>
                        <td>Department</td>
                        <td><?php echo $modelSetting->title; ?></td>
                      </tr>
                      <tr>
                        <td>Supervisor Name</td>
                        <td><?php echo $contract->supervisor_name; ?></td>
                      </tr>
                      <tr>
                        <td>Supervisor Email</td>
                        <td><?php echo $contract->supervisor_email; ?></td>
                      </tr>
                      
                      <tr>
                        <td>Supervisor Phone</td>
                        <td><?php echo $contract->supervisor_phone; ?></td>
                      </tr>
                      <tr>
                        <td>Official Email Address</td>
                        <td><?php echo $offercandidate->emp_official_email; ?></td>
                      </tr>
                       <?php 			
						if($workorder->onboard_changed_start_date=='0000-00-00'){
							$onboardStartDate = date('F d,Y',strtotime($workorder->wo_start_date));
							}else{
								 $onboardStartDate = date('F d,Y',strtotime($workorder->onboard_changed_start_date));
								 }
								 
						if($workorder->onboard_changed_end_date=='0000-00-00'){
							$onboardEndDate = date('F d,Y',strtotime($workorder->wo_end_date));
							}else{
								 $onboardEndDate = date('F d,Y',strtotime($workorder->onboard_changed_end_date));
								 }
						
						?>
                      <tr>
                        <td>Start Date</td>
                        <td><?php echo $onboardStartDate; ?></td>
                      </tr>
                      <tr>
                        <td>End Date</td>
                        <td><?php echo $onboardEndDate; ?></td>
                      </tr>
                      <tr>
                        <td>Candidate Customer ID</td>
                        <td><?php echo $offercandidate->candidate_ID; ?></td>
                      </tr>
                    </tbody>
                  </table>
                  <br>
                  <!-- simplify-tabs --> 