<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
                        <thead>
                                        <tr>
                                            <th>Primary Skill</th>
                                            <th>Year of Experience</th>
                                            <th style="text-align: center;">Rating</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php  $primarySkill = unserialize($candidateModel->primary_skill);
                                        if($primarySkill)
                                        foreach($primarySkill['experience'] as $key=>$value) {
                                        ?>
                                            <tr>
                                                <td><?php echo $key; ?></td>
                                                <td>
                                                    <?php echo $primarySkill['experience'][$key]; ?>
                                                </td>
                                                <td style="text-align: right;">
                                                    <?php  $rating = explode('(',$primarySkill['rating'][$key]);
                                                     $prating = trim($rating[0]);
                                                    if($prating=='Intermediates'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                        </p>/ Intermediates
                                                    <?php }
                                                    if($prating=='Advanced'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                        </p>/Advanced
                                                    <?php }
                                                    if($prating=='Beginners'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star-half-full filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                        </p>/ Beginners
                                                    <?php }
                                                    ?>

                                                </td>



                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                        <thead>
                                        <tr>
                                            <th>Secondary Skill</th>
                                            <th>Year of Experience</th>
                                            <th style="text-align: center;">Rating</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        $secondarySkill = unserialize($candidateModel->secondary_skill);
                                        if($secondarySkill)
                                        foreach($secondarySkill['experience'] as $key=>$value) { ?>
                                            <tr>
                                                <td><?php echo $key; ?></td>
                                                <td>
                                                    <?php echo $secondarySkill['experience'][$key]; ?>
                                                </td>
                                                <td style="text-align: right;">
                                                    <?php  $rating = explode('(',$secondarySkill['rating'][$key]);
                                                     $srating = trim($rating[0]);
                                                    if($srating=='Intermediates'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                        </p>/Intermediates
                                                    <?php }
                                                    if($srating=='Advanced'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                        </p>/Advanced
                                                    <?php }
                                                    if($srating=='Beginners'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star-half-full filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                        </p>/Beginners
                                                    <?php }
                                                    ?>
                                                </td>


                                            </tr>
                                        <?php } ?>
                                        </tbody>





                                        <thead>
                                        <tr>
                                            <th>Other skills</th>
                                            <th>Year of Experience</th>
                                            <th style="text-align: center;">Rating</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $otherSkill = unserialize($candidateModel->other_skill);
                                        if($otherSkill)
                                        foreach($otherSkill['experience'] as $key=>$value) {
                                        ?>
                                            <tr>
                                                <td><?php echo $key; ?></td>
                                                <td>
                                                    <?php echo $otherSkill['experience'][$key]; ?>
                                                </td>
                                                <td style="text-align: right;">
                                                    <?php  $otherSkill['rating'][$key]; ?>
                                                    <?php  $rating = explode('(',$otherSkill['rating'][$key]);
                                                     $orating = trim($rating[0]);
                                                    if($orating=='Intermediates'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                        </p>/Intermediates
                                                    <?php }
                                                    if($orating=='Advanced'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star filled"></i>
                                                        </p>/Advanced
                                                    <?php }
                                                    if($orating=='Beginners'){ ?>
                                                        <p class="stars">
                                                            <i class="fa fa-star filled"></i>
                                                            <i class="fa fa-star-half-full filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                            <i class="fa fa-star-o filled"></i>
                                                        </p>/Beginners
                                                    <?php }
                                                    ?>

                                                </td>


                                            </tr>
                                        <?php } ?>
                                        </tbody>
                      </table>
                    </div>