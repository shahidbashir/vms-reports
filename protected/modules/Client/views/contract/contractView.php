<?php $this->pageTitle = 'Contract'; error_reporting(0);
	if($workorder->onboard_changed_start_date=='0000-00-00'){
		$onboardStartDate = date('F d,Y',strtotime($workorder->wo_start_date));
		}else{
			 $onboardStartDate = date('F d,Y',strtotime($workorder->onboard_changed_start_date));
			 }
			 
	if($workorder->onboard_changed_end_date=='0000-00-00'){
		$onboardEndDate = date('F d,Y',strtotime($workorder->wo_end_date));
		}else{
			 $onboardEndDate = date('F d,Y',strtotime($workorder->onboard_changed_end_date));
			 }
	$aprovermanager = Client::model()->findByPk($workorder->approval_manager);
	$Location = Location::model()->findByPk($offer->approver_manager_location);
	/*echo '<pre>';
	print_r($model);
	echo '</pre>';*/
	?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10"><?php echo $candidate->first_name.' '.$candidate->last_name; ?> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/index'); ?>" class="btn btn-sm btn-default pull-right"> Back to Contract Listing</a> </h4>
      <p class="m-b-30">Part of Vendor <?php echo $vendor->organization; ?></p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"> <span class="tag label-re-open">Completed</span> </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="simplify-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tsablist">
                <li class="nav-item"> <a class="nav-link active"  href="#rejected" role="tab" data-toggle="tab" data-placement="bottom" title="Rejected"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/workorder@512px.svg"></i> WorkOrder </a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#can-info" role="tab" data-toggle="tab" data-placement="bottom" title="Skills"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/on-boarding@512px.svg"></i> Candidate Info</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#skills" role="tab" data-toggle="tab" data-placement="bottom" title="Skills"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/jobs@512px.svg"></i> Offer</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#resume" role="tab" data-toggle="tab" data-placement="bottom" title="Resume"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Resume</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#comments" role="tab" data-toggle="tab" data-placement="bottom" title="Comments"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Skills</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#background-verification" role="tab" data-toggle="tab" data-placement="bottom" title="Comments"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Background Verification</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#exhibit" role="tab" data-toggle="tab" data-placement="bottom" title="Comments"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Exhibit</a> </li>
                 <?php if($model->termination_status != 2 ){?>
                 <li class="nav-item" id="extensionTab"> <a class="nav-link " role="tab" data-toggle="tab" data-placement="bottom" title="Extension" href="#extension" onclick="calculateExt(<?php echo $model->id ?>)"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/contract@512px.svg"></i>Extension</a> </li>
                <?php }?>
        			<li class="nav-item"> <a class="nav-link "  href="#termination"  onclick="calculateTermination(<?php echo $model->id ?>)" role="tab" data-toggle="tab" data-placement="bottom" title="Termination"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/contract@512px.svg"></i>Termination</a> </li>
       			<li class="nav-item"> <a class="nav-link "  href="#edit" role="tab" data-toggle="tab" data-placement="bottom" title="Edit"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/contract@512px.svg"></i> Edit</a> </li>
              </ul>
              <div class="tab-content ">
              	<div class="tab-pane m--31" id="edit"  role="tabpanel">
                  <?php $this->renderPartial('editrate',array('candidates'=>$candidate,'model'=>$model,'offer'=>$offer,'workorder'=>$workorder)); ?>
                </div>
                <div class="tab-pane  active m--31" id="rejected"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table class="table sub-table  tab-table gray-table m-t-20  submission-skill-table m-b-0">
                        <thead>
                          <tr>
                            <th>Start Date</th>
                            <th> End Date</th>
                            <th>Time Sheet Approval</th>
                            <th style="text-align: center;">Location of Work</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><?php echo $onboardStartDate; ?></td>
                            <td> <?php echo $onboardEndDate; ?> </td>
                            <td> <?php echo $aprovermanager->first_name.' '.$aprovermanager->last_name; ?> </td>
                            <td style="text-align: center;"> <?php echo $Location->name; ?> </td>
                          </tr>
                        </tbody>
                      </table>
                      <br>
                      <br>
                      <table class="table tab-table">
                        <tbody>
                          <tr>
                            <td> WorkOrder ID: </td>
                            <td><a href=""><?php echo $workorder->id; ?></a></td>
                          </tr>
                          <tr>
                            <td> WorkOrder Issue Date: </td>
                            <td> <?php echo date('F d,Y',strtotime($workorder->date_created)); ?> </td>
                          </tr>
                          <tr>
                            <td> WorkOrder Accepted Date: </td>
                            <td> <?php echo date('F d,Y',strtotime($workorder->accept_date)); ?> </td>
                          </tr>
                          <tr>
                            <td> On - Boarding Start Date: </td>
                            <td> <?php echo date('F d,Y',strtotime($workorder->onboard_changed_start_date)); ?> </td>
                          </tr>
                          <tr>
                            <td> On - Boarding End Date: </td>
                            <td> <?php echo date('F d,Y',strtotime($workorder->onboard_changed_end_date)); ?> </td>
                          </tr>
                           <tr>
                            <td> Supervisor Name: </td>
                            <td> <?php echo $model->supervisor_name; ?> </td>
                          </tr>
                          <tr>
                            <td> Supervisor Phone: </td>
                            <td> <?php echo $model->supervisor_phone; ?> </td>
                          </tr>
                          <tr>
                            <td> Supervisor Email Addres: </td>
                            <td> <?php echo $model->supervisor_email; ?> </td>
                          </tr> 
                          <tr>
                            <th> Pay Rate ( For Candidate ) </th>
                            <th> </th>
                          </tr>
                          <tr>
                            <td>Pay Rate</td>
                            <td>$ <?php echo $workorder->wo_pay_rate;?></td>
                          </tr>
                          <tr>
                            <td>Over Time</td>
                            <td>$ <?php echo $workorder->wo_over_time;?></td>
                          </tr>
                          <tr>
                            <td>Double Time</td>
                            <td>$ <?php echo $workorder->wo_double_time;?></td>
                          </tr>
                          <tr>
                            <th> Bill Rate ( For Client ) </th>
                          </tr>
                          <tr>
                            <td>Bill Rate</td>
                            <td>$ <?php echo $workorder->wo_bill_rate;?></td>
                          </tr>
                          <tr>
                            <td>Over Time</td>
                            <td>$ <?php echo $workorder->wo_client_over_time;?></td>
                          </tr>
                          <tr>
                            <td>Double Time</td>
                            <td>$ <?php echo $workorder->wo_client_double_time;?></td>
                          </tr>
                        </tbody>
                      </table>
                      <br>
                      <br>
                      <table class="table tab-table">
                        <thead>
                          <tr>
                            <th colspan="4">Cost Center Code</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($costCenters as $costCenters){ ?>
                          <tr>
                            <td colspan="4"><?php echo $costCenters->cost_code; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                      <br>
                      <br>
                      <table class="table tab-table">
                        <thead>
                          <tr>
                            <th colspan="4">Projects</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($projects as $projects){ ?>
                          <tr>
                            <td colspan="4"><?php echo $projects->project_name; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                      <br>
                      <br>
                      <table class="table tab-table">
                        <thead>
                          <tr>
                            <th colspan="4">Time Sheet</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($timeSheetCodes as $timeSheetCodes){ ?>
                          <tr>
                            <td  colspan="4"><?php echo $timeSheetCodes->time_code; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane m--31" id="can-info"  role="tabpanel">
                 <?php $this->renderPartial('can_info',array('offer'=>$offer,'jobData'=>$jobData,'workorder'=>$workorder)); ?>
                </div>
                <div class="tab-pane m--31" id="skills"  role="tabpanel">
                 <?php $this->renderPartial('contract_offer',array('offer'=>$offer)); ?>
                </div>
                <div class="tab-pane" id="resume"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <h4 class="m-b-20 m-t-10 h4-small p-l-15">Candidate Resume</h4>
                      <?php $this->renderPartial('contract_resume',array('candidateModel'=>$candidate)); ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane m--31" id="comments"  role="tabpanel">
                  <div class="row">
                    <?php $this->renderPartial('contract_skills',array('candidateModel'=>$candidate)); ?>
                  </div>
                </div>
                <!--  tab-pane -->
                
                <div class="tab-pane m--31" id="background-verification"  role="tabpanel">
                  <div class="row">
                   <?php $this->renderPartial('contract_background',array('client'=>$client,'workorder'=>$workorder)); ?>
                  </div>
                </div>
                <!--  tab-pane -->
                
                <div class="tab-pane m--31" id="exhibit"  role="tabpanel">
                	<?php $this->renderPartial('contract_exibit',array('workorder'=>$workorder,'model'=>$model)); ?>
                </div>
                <!--  tab-pane --> 
                
                 <div class="tab-pane m--31" id="extension"  role="tabpanel">
                  <div class="row" id="response" style="display:none;">
                      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="card-2 ">
                          <table class="table tab-table border-top-0 border-bottom-0">
                            <tbody>
                              <tr>
                                <td>Work Order ID:</td>
                                <td id="woid"></td>
                              </tr>
                              <tr>
                                <td>Contract ID:</td>
                                <td id="cid"></td>
                              </tr>
                              <tr>
                                <td>Bill Rates:</td>
                                <td id="brate"></td>
                              </tr>
                              <tr>
                                <td>Pay Rates:</td>
                                <td id="prate"></td>
                              </tr>
                              <tr>
                                <td>Location:</td>
                                <td id="location"></td>
                              </tr>
                              <tr>
                                <td>Job ID:</td>
                                <td id="jid"></td>
                              </tr>
                              <tr>
                                <td>Vendor Name: </td>
                                <td id="extvendor"></td>
                              </tr>
                               <tr>
                                <td>Start Date: </td>
                                <td id="extstart_date">ABC</td>
                              </tr>
                              <tr>
                                <td>End Date: </td>
                                <td id="extend_date">ABC</td>
                              </tr>
                               <tr>
                                <td>Time Sheet Approval: </td>
                                <td id="extmanager_name">ABC</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <?php $form=$this->beginWidget('CActiveForm', array(
                          'id'=>'extension-form',
                          'enableAjaxValidation'=>false,
                      )); ?>
                        <div class="card-2 p-20">
                        	<div id="contract_success"></div>
                          <p class="m-b-20 lg">Contract Extention</p>
                          <div class="form-group">
                            <label for="">Reason for Extention</label>
                            <div id="reason_status">
                            <select name="ContractExtensionReq[reason_of_extension]" id="reason_of_extension" class="form-control" required="required">
                              <option value=""></option>
                                <?php
                              $reason = Setting::model()->findAll(array('condition'=>'category_id=62'));
                              foreach($reason as $reason){ ?>
                                <option value="<?php echo $reason->id; ?>"><?php echo $reason->title; ?></option>
                                <?php  } 	?>
                            </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="">Notes / Comments</label>
                            <textarea name="ContractExtensionReq[note_of_extension]" id="note_of_extension" class="form-control" rows="3" required="required"></textarea>
                          </div>
                          <div class="form-group">
                            <label for="">New end date of contract</label>
                            <input type="text" name="ContractExtensionReq[new_contract_end_date]" class="form-control" id="new_job_end_date" required="required">
                           </div>
                          <div class="form-group">
                            <label for="">Pay Rate</label>
                            <input type="text" name="ContractExtensionReq[pay_rate]" class="form-control" id="ext_pay_rate">
                          </div>
                          <div class="form-group">
                            <label for="">Bill Rate</label>
                            <input type="text" name="ContractExtensionReq[bill_rate]" class="form-control" id="ext_bill_rate" readonly="readonly">
                          </div>
                          <div class="form-group">
                            <label for="">Over Time Pay Rate</label>
                            <input type="text" name="ContractExtensionReq[overtime_payrate]" class="form-control" id="ext_over_time_prate" readonly="readonly">
                          </div>
                          <div class="form-group">
                            <label for="">Double Time Pay Rate</label>
                            <input type="text" name="ContractExtensionReq[doubletime_payrate]" class="form-control" id="ext_double_time_prate" readonly="readonly">
                          </div>
                          <div class="form-group">
                            <label for="">Over Time Bill Rate</label>
                            <input type="text" name="ContractExtensionReq[overtime_billrate]" class="form-control" id="ext_over_time_brate" readonly="readonly">
                          </div>
                          <div class="form-group">
                            <label for="">Double Time Bill Rate</label>
                            <input type="text" name="ContractExtensionReq[doubletime_billrate]" class="form-control" id="ext_double_time_brate" readonly="readonly">
                          </div>
                          <div class="form-group">
                            <label for="">Total Estimated Cost</label>
                            <input type="text" name="ContractExtensionReq[new_estimate_cost]" class="form-control" id="ext_estimated_cost" readonly="readonly">
                           </div>
                          <input type="hidden" name="markup" id="markup" value="" />
                         
                          <input type="hidden" name="ContractExtensionReq[contract_id]" id="contract_id" value="" />
                          <input type="hidden" name="job_start_date" id="job_start_date" value="" />
                          <input type="hidden" name="hours_per_week" id="hours_per_week" value="" />
                          <input type="hidden" name="new_assigned_job_end_date" id="new_assigned_job_end_date" value="0" />
                           <span id="cwfa">
                              <!--<p class="bold m-b-20" id="cwfa">Contract Wrok Flow Approval</p>
                              <p class="bold m-b-20">Name of the person</p>-->
                          </span>
                          
                          
                          
                          <br>
                          <p class="m-t-20 m-b-20" id="ext_button"> 
                              <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit for Contract Extention' : 'Submit for Contract Extention',array('class'=>'btn btn-block btn-danger' , 'onclick' => 'submitExtensionForm()')); ?>
                          </p>
                        </div>
                      <?php $this->endWidget(); ?>
                      </div>
                    </div>
                 </div>
                  <div class="tab-pane m--31" id="termination"  role="tabpanel">
                     <div class="row" id="Termresponse"  style="display:none;">
                         <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                          <div class="card-2 ">
                            <table class="table tab-table border-top-0 border-bottom-0">
                              <tbody>
                                <tr>
                                  <td>Work Order ID:</td>
                                  <td id="twoid">654654</td>
                                </tr>
                                <tr>
                                  <td>Contract ID:</td>
                                  <td id="tcid">6546</td>
                                </tr>
                                <tr>
                                  <td>Bill Rates:</td>
                                  <td id="tbrate">$ 45.00</td>
                                </tr>
                                <tr>
                                  <td>Pay Rates:</td>
                                  <td id="tprate">$ 45.00</td>
                                </tr>
                                <tr>
                                  <td>Location:</td>
                                  <td id="tlocation">$ 45.00</td>
                                </tr>
                                <tr>
                                  <td>Job ID:</td>
                                  <td id="tjid">654986</td>
                                </tr>
                                <tr>
                                  <td>Vendor Name: </td>
                                  <td id="tvendor">ABC</td>
                                </tr>
                                 <tr>
                                  <td>Start Date: </td>
                                  <td id="tstart_date">ABC</td>
                                </tr>
                                <tr>
                                  <td>End Date: </td>
                                  <td id="tend_date">ABC</td>
                                </tr>
                                 <tr>
                                  <td>Time Sheet Approval: </td>
                                  <td id="manager_name">ABC</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                          <div class="card-2 p-20">
                          	<div id="statusTxt"></div>
                            <div id="success"></div>
                          <form method="post" id="termination">
                            <p class="m-b-20 lg">Contract Termination</p>
                            <div class="form-group">
                              <label for="">Reason for Termination</label>
                              <div id="termination_reason">
                              <select class="form-control" name="reason_of_termination" required >
                                  <option value=""></option>
                                  <?php
                                $location = Setting::model()->findAll(array('condition'=>'category_id=60'));
                                foreach($location as $location){ ?>
                                  <option value="<?php echo $location->id; ?>"><?php echo $location->title; ?></option>
                                  <?php  } 	?>
                                </select>
                               </div>
                            </div>
                            <div class="form-group">
                              <label for="">Notes / Comments</label>
                              <textarea name="termination_notes" id="termination_comments" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            <div class="form-group">
                              <label for="">Candidate Feedback</label>
                              <textarea name="termination_can_feedback" id="termination_can_feedback" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            <div class="form-group">
                              <label for="">Date of Termination / Contract Closing</label>
                              <input type="text" name="termination_date" class="form-control singledate" id="termination_date" placeholder="">
                            </div>
                            
                            <div class="form-group">
                              <input type="hidden" name="contract_id" class="form-control" value="" id="tcontract_id" placeholder="">
                            </div>
                            
                            <!--
                            <p class="bold m-b-20">Candidate Feedback</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                              -->
                            <p class="m-t-20 m-b-20" id="button">
                                <button type="submit" onclick="submitTermination();" name="terminationForm" class="btn btn-block btn-danger">Submit for Contract Closing</button> 
                            </p>
                             <p class="m-t-20 m-b-20" style="display:none" id="appr_rej">
                                <button type="button" onclick="TerminationApprRej(1)" name="approved" class="btn btn-danger">Approved</button> 
                                <button type="button" onclick="TerminationApprRej(2)"  name="reject" class="btn btn-default">Cancelled</button> 
                               </p>
                            </form>
                          </div>
                        </div>
                      </div>
      			  </div>
                <!--  tab-pane --> 
                
              </div>
            </div>
            <!-- tabpanel --> 
            
          </div>
          <!-- simplify-tabs --> 
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>