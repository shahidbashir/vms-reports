<?php
	$verification = Setting::model()->findAllByAttributes(array('category_id'=>45));
	$verificationValues = explode(",",$client->backgroundverification);
	$backgroundverification = OfferBackground::model()->findAllByAttributes(array('offer_id'=>$workorder->id),array('order'=>'id desc'));
	?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="clearfix">
    <!--
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="wel m-b-20 m-t-10 well-yellow">
        <p class="m-t-10 m-b-10">Background Verification was completed on 10th May 2015 at 11:30</p>
      </div>
    </div>
    -->
  </div>
  <div class="clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="well  m-b-20 well-yellow">
        <h4 class="h4-small m-b-20">Back Ground Certification Check List</h4>

        <?php foreach($backgroundverification as $key => $data){ ?>
          <div class="be-checkbox">
            <input id="check<?php echo $key; ?>" type="checkbox" checked disabled>
            <label for="check<?php echo $key; ?>"><?php echo $data['background_title']; ?></label>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
    <thead>
      <tr>
        <th>S.No</th>
        <th>Document Name</th>
        <th>Date of Upload</th>
        <th style="text-align: right; padding-right: 30px;"> Actions </th>
      </tr>
    </thead>
    <tbody>
    <?php if($backgroundverification){
			$i = 0;
			foreach($backgroundverification as $backgrounddata){
				$i++;
	?>
      <tr>
        <td><?php echo $i; ?> </td>
        <td> <?php echo $backgrounddata->background_title; ?> </td>
        <td> <?php
		$dateuploaded = explode(' ',$backgrounddata->date_created);
		echo date('F d,Y',strtotime($dateuploaded[0])); ?> </td>
        <td style="text-align: right; padding-right: 30px;">
         <a href="<?php echo $this->createAbsoluteUrl('offer/downloadBackground',array('id'=>$backgrounddata->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
         <?php /*?><a href="<?php echo $this->createAbsoluteUrl('contract/deleteBackgroundverification',array('id'=>$backgrounddata->id,'contractID'=>$_GET['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg"></i></a><?php */?>
         </td>
      </tr>
     <?php } } ?>
    </tbody>
  </table>
</div>
