<?php $this->pageTitle = 'Contracts Extension List'; ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">Extension Request </h4>
      <p class="m-b-20"> </p>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="" placeholder="Search Request">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <table class="table m-b-40 without-border">
            <thead class="thead-default">
              <tr>
                <th>S.No</th>
                <th>Request ID</th>
                <th>Name</th>
                <th>End Date</th>
                <th>Extension Date</th>
                <th>Bill Rate</th>
                <th>Status</th>
                <th style="text-align: center">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php 
			  if($models){
				  $i = 1;
			  foreach($models as $model){
				$candidates = Candidates::model()->findByPk($model->candidate_id);
				$workorder = Workorder::model()->findByPk($model->workorder_id);
				//$vendor = Vendor::model()->findByPk($model->vendor_id);
			    //$jobs = Job::model()->findByPk($model->job_id);
				//$offer = Offer::model()->findByPk($model->offer_id);
				//$location = Location::model()->findByPk($offer->approver_manager_location);
				
				$extensionRequest = ContractExtensionReq::model()->findByAttributes(array('contract_id'=>$model->id),array('order'=>'id desc'));
				
				$extensionStatus = UtilityManager::extensionStatus();
				if($model->ext_status == 1){
					$className ="label-re-open";
				}else if($model->ext_status == 2){
					$className ="label-pending-aproval";
				}elseif($model->ext_status == 3){
					$className ="label-rejected";
				}else{
					$className ="label-new-request";
				} 
				if(!empty($extensionRequest->id)){
				?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $extensionRequest->id; ?></td>
                <td><?php echo $candidates->first_name.' '.$candidates->last_name; ?></td>
                <td><?php echo date('m-d-Y',strtotime($model->start_date)); ?></td>
                <td><?php echo date('m-d-Y',strtotime($extensionRequest->new_contract_end_date)); ?></td>
                <td><?php echo $extensionRequest->bill_rate; ?></td>
                <td><span class="tag <?php echo $className; ?>"><?php echo  $extensionStatus[$model->ext_status];?></span></td>
                <td style="text-align: center">
                	<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/extensionView',array('id'=>$extensionRequest->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i>
                </a></td>
              </tr>
              <?php } }$i++; }else{ ?>
              <tr>
                <td colspan="6"> Sorry No Record Found.. </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  
  <div class="seprater-bottom-100"></div>
</div>
