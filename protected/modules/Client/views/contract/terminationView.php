<?php 
	$this->pageTitle = 'Contracts'; 
	$setting = Setting::model()->findByPk($contract->reason_of_termination);
 	$client = Client::model()->findByPk($contract->term_by_id);
  ?>
 <div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
  <?php if(Yii::app()->user->hasFlash('success')):
      echo Yii::app()->user->getFlash('success');
    endif; ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">Contract Termination 
      <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractTermination'); ?>" class="btn btn-sm btn-default pull-right">Back to Contract Termination</a> </h4>
      <p class="m-b-20">Step to cancel contract.</p>
      <div class="alert alert-danger">Contract was terminated by <?php echo $client->first_name.' '.$client->last_name;?> on <?php echo date('d-m-Y',strtotime($contract->termination_date));?></div>
      <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <div class="card-2 ">
            <table class="table tab-table border-top-0 border-bottom-0">
              <tbody>
                <tr>
                  <td>Work Order ID:</td>
                  <td><?php echo $workorder->id; ?></td>
                </tr>
                <tr>
                  <td>Contract ID:</td>
                  <td><?php echo $contract->id; ?></td>
                </tr>
                <tr>
                  <td>Bill Rates:</td>
                  <td>$ <?php echo $workorder->wo_bill_rate; ?></td>
                </tr>
                <tr>
                  <td>Pay Rates:</td>
                  <td>$ <?php echo $workorder->wo_pay_rate; ?></td>
                </tr>
                <tr>
                  <td>Location:</td>
                  <td> <?php echo $location->name; ?></td>
                </tr>
                <tr>
                  <td>Job ID:</td>
                  <td><?php echo $contract->job_id; ?></td>
                </tr>
                <tr>
                  <td>Vendor Name: </td>
                  <td><?php echo $vendor->first_name.' '.$vendor->last_name.' ('.$vendor->organization.')'; ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <div class="card-2 p-20">
            <p class="m-b-20 lg">Contract Termination</p>
            <div class="form-group">
              <label for="">Reason for Termination</label>
              <input type="text" value="<?php echo $setting->title; ?>" class="form-control" id="" readonly="">
            </div>
            <div class="form-group">
              <label for="">Notes / Comments</label>
              <textarea name="" id="input" class="form-control" readonly rows="3" required="required"><?php echo $contract->termination_notes; ?></textarea>
            </div>
            <div class="form-group">
              <label for="">Date of Termination / Contract Closing</label>
              <input type="text" class="form-control" id="" value="<?php echo date('d-m-Y',strtotime($contract->termination_date)); ?>" readonly placeholder="">
            </div>
            <p class="bold m-b-20">Candidate Feedback</p>
            <p><?php echo $contract->termination_can_feedback; ?></p>
            <?php if($contract->termination_status == 1){ ?>
            <p class="m-t-20 m-b-20">
            	<form method="post"> 
                	<button type="submit" name="approved" class="btn btn-danger">Approved</button> 
                	<button type="submit" name="reject" class="btn btn-default">Cancelled</button> 
                </form>
            </p>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
    <!-- col -->
    
  </div>
  <!-- row -->
  
  <div class="clearfix" style="padding: 10px 20px 30px; ">
   </div>
  <div class="seprater-bottom-100"></div>
</div>
