<?php 
	$this->pageTitle = 'Contract Extension'; 
	//$setting = Setting::model()->findByPk($contract->reason_of_termination);
	//populating all the data on ajax call
	//billrate and payrate values are picked from the workorder rather then job b/c we made changes in offer as well as in workorder process
	
?>
<style>
.testRowHide{ display:none;}
.testRowShow{ display:block;}
</style>

 <div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">Contract Extention <a href="" class="btn btn-sm btn-default pull-right">Back to Contract</a> </h4>
      <p class="m-b-20">Step to contract extension.</p>
      <p class="m-b-20"> </p>
      <div class="search-box">
        <div class="form-group">
          <label for=""></label>
          <input type="text" class="form-control" id="autocomplete" placeholder="Search Contracts">
        </div>
      </div>
      <div class="row <?php  if($id != 0) echo 'testRowShow'; else echo 'testRowHide'; ?>" id="response" >
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <div class="card-2 ">
            <table class="table tab-table border-top-0 border-bottom-0">
              <tbody>
                <tr>
                  <td>Work Order ID:</td>
                  <td id="woid"></td>
                </tr>
                <tr>
                  <td>Contract ID:</td>
                  <td id="cid"></td>
                </tr>
                <tr>
                  <td>Bill Rates:</td>
                  <td id="brate"></td>
                </tr>
                <tr>
                  <td>Pay Rates:</td>
                  <td id="prate"></td>
                </tr>
                <tr>
                  <td>Location:</td>
                  <td id="location"></td>
                </tr>
                <tr>
                  <td>Job ID:</td>
                  <td id="jid"></td>
                </tr>
                <tr>
                  <td>Vendor Name: </td>
                  <td id="extvendor"></td>
                </tr>
                 <tr>
                  <td>Start Date: </td>
                  <td id="extstart_date">ABC</td>
                </tr>
                <tr>
                  <td>End Date: </td>
                  <td id="extend_date">ABC</td>
                </tr>
                 <tr>
                  <td>Time Sheet Approval: </td>
                  <td id="extmanager_name">ABC</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'express-job-form',
			'enableAjaxValidation'=>false,
		)); ?>
          <div class="card-2 p-20">
            <p class="m-b-20 lg">Contract Extention</p>
            <div class="form-group">
              <label for="">Reason for Extention</label>
              <select name="ContractExtensionReq[reason_of_extension]" id="input" class="form-control" required="required">
                <option value=""></option>
                  <?php
                $reason = Setting::model()->findAll(array('condition'=>'category_id=62'));
                foreach($reason as $reason){ ?>
                  <option value="<?php echo $reason->id; ?>"><?php echo $reason->title; ?></option>
                  <?php  } 	?>
              </select>
            </div>
            <div class="form-group">
              <label for="">Notes / Comments</label>
              <textarea name="ContractExtensionReq[note_of_extension]" id="input" class="form-control" rows="3" required="required"></textarea>
            </div>
            <div class="form-group">
              <label for="">New end date of contract</label>
              <input type="text" name="ContractExtensionReq[new_contract_end_date]" class="form-control" id="new_job_end_date" required="required" >
            </div>
            <div class="form-group">
              <label for="">Pay Rate</label>
              <input type="text" name="ContractExtensionReq[pay_rate]" class="form-control" id="ext_pay_rate">
            </div>
            <div class="form-group">
              <label for="">Bill Rate</label>
              <input type="text" name="ContractExtensionReq[bill_rate]" class="form-control" id="ext_bill_rate" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="">Over Time Pay Rate</label>
              <input type="text" name="ContractExtensionReq[overtime_payrate]" class="form-control" id="ext_over_time_prate" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="">Double Time Pay Rate</label>
              <input type="text" name="ContractExtensionReq[doubletime_payrate]" class="form-control" id="ext_double_time_prate" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="">Over Time Bill Rate</label>
              <input type="text" name="ContractExtensionReq[overtime_billrate]" class="form-control" id="ext_over_time_brate" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="">Double Time Bill Rate</label>
              <input type="text" name="ContractExtensionReq[doubletime_billrate]" class="form-control" id="ext_double_time_brate" readonly="readonly">
            </div>
            <div class="form-group">
              <label for="">Total Estimated Cost</label>
              <input type="text" name="ContractExtensionReq[new_estimate_cost]" class="form-control" id="ext_estimated_cost" readonly="readonly">
            </div>
            <input type="hidden" name="markup" id="markup" value="" />
            <input type="hidden" name="ContractExtensionReq[contract_id]" id="contract_id" value="" />
            <input type="hidden" name="job_start_date" id="job_start_date" value="" />
            <input type="hidden" name="hours_per_week" id="hours_per_week" value="" />
            <input type="hidden" name="new_assigned_job_end_date" id="new_assigned_job_end_date" value="0" />
             <span id="cwfa">
            	<!--<p class="bold m-b-20" id="cwfa">Contract Wrok Flow Approval</p>
            	<p class="bold m-b-20">Name of the person</p>-->
            </span>
            
            
            
            <br>
            <p class="m-t-20 m-b-20"> 
            	<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit for Contract Extention' : 'Submit for Contract Extention',array('class'=>'btn btn-block btn-danger')); ?>
            </p>
          </div>
        <?php $this->endWidget(); ?>
        </div>
      </div>
     </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  
  <div class="seprater-bottom-100"></div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		  $('#autocomplete').autocomplete({
			  lookup: <?php $this->extensionCandidates(); ?>,
			  onSelect: function (suggestion) {
				  calculateExt(suggestion.data);
			  }
		  }); 
     });
 </script>
