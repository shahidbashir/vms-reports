  <?php $this->pageTitle =  'Consolidate Invoice';
    
   $total = 0;

    $candidateModel = Candidates::model()->findByPk($generatedModel->candidate_id);
    $timesheetDetail = CpTimesheet::model()->findByAttributes(array('id'=>$generatedModel->timsheet_id));
    $vendorModel = Vendor::model()->findByPk($generatedModel->vendor_id); ?>

 <div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                     
                     <div class="cleafix " style="padding: 30px 20px; ">   
                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                           <h4 class="m-b-10"> Invoice

                           
                           <a href="<?php echo $this->createUrl('adminStatistic');?>" class="btn btn-default pull-right">Back</a></h4> 
                            
                            <div class="clearfix"></div><br />
                            

                        <div class="card">
                            <div class="card-block">
                              <div class="p-b-2 clearfix">
                                <div class="pull-right text-xs-right">
                                  <h5 class="bold m-b-0">
                                    Invoice #<?php echo $generatedModel->serial_number; ?>
                                  </h5>
                                  <p class="m-b-0">
                                    Issued on <?php echo date("m/d/Y",strtotime($generatedModel->date_created)); ?>
                                  </p>
                                  <p class="m-b-0">
                                    Payment due by ...
                                  </p>
                                </div>
                                
                                <div class="overflow-hidden">
                                <p class="m-b-0">
                                    <strong>
                                      Client Details <br />
                                    </strong>
                                  </p>
                                  <p class="m-b-0">
                                    <?php echo $clientModel->business_name?>
                                  </p>
                                  <p class="m-b-0">
                                    <?php echo $clientModel->email?>
                                  </p>

                                  <p class="m-b-0">
                                  <strong> <br />
                                      Timesheet Aproved By 
                                    </strong>
                                    <?php $clientApprover = Client::model()->findByPk($timesheetDetail->approval_manager);
                                    echo $clientApprover->first_name.' '.$clientApprover->last_name;?>
                                  </p>
                                  <p class="m-b-0">
                                  <strong> 
                                      Timesheet Aprovel Date
                                    </strong>
                                    <?php 
                                    echo date("m/d/Y H:i:s",strtotime($timesheetDetail->approve_date_time));?>
                                  </p>

                                  <p class="m-b-0">
                                  <strong> 
                                      Timesheet Date
                                    </strong>
                                    <?php 
                                    echo date("m/d/Y",strtotime($timesheetDetail->date_created));?>
                                  </p>
                                </div>
                              </div>
                              <div class="p-t-2 p-b-2 clearfix">
                                
                                <div class="overflow-hidden">
                                  <p class="m-b-0">
                                    <strong>
                                      Candidate Details
                                    </strong>
                                  </p>
                                  
                                  <p class="m-b-0">
                                     <?php echo $candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name." (".$candidateModel->id.")";?>
                                  </p>
                                  <p class="m-b-0">
                                  
                                    <?php echo $vendorModel->organization.' ('.$vendorModel->first_name.' '.$vendorModel->last_name.')';?>
                                  </p>
                                
                                </div>
                              </div>
                              <div class="table-responsive p-t-2 p-b-2">
                                <table class="table table-bordered m-b-0">
                                  <thead>
                                    <tr>
                                      <th style="width: 60%">
                                        Description
                                      </th>
                                      <th style="width: 13.33%">
                                        Hours
                                      </th>
                                      <th style="width: 13.33%">
                                        Unit Price
                                      </th>
                                      <th style="width: 13.33%">
                                        Quantity
                                      </th>
                                      <th style="width: 13.33%">
                                        Amount
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
   
 <tr>
        <th colspan="5">
         Regular Hours
        </th>
      </tr>
      <!--<tr>
        <td>
          <?php echo $candidateModel->id;?> - <?php echo $candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name;?> /  <?php echo $generatedModel->total_hours;?> / <?php echo $vendorModel->organization;?>
        </td>
         <td>
          <?php echo $timesheetDetail->total_regular_hours;?>
        </td>
        <td>
          $<?php echo $generatedModel->total_billrate;?>
        </td>
        <td>
          1
        </td>
        <td>
          $<?php echo $generatedModel->total_billrate;?>
        </td>
      </tr> -->
       <tr>
        <td>
          <?php echo $candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name."(".$candidateModel->id.")";?>
        </td>
        <td>
          <?php echo $timesheetDetail->total_regular_hours;?>
        </td>
        <td>
          $<?php echo $timesheetDetail->total_regular_billrate;?>
        </td>
        <td>
          1
        </td>
        <td>
          $<?php echo $timesheetDetail->total_regular_billrate;?>
        </td>
      </tr>
       <tr>
        <th colspan="5">
         Over Time
        </th>
      </tr>
      <tr>
        <td>
          <?php echo $candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name."(".$candidateModel->id.")";?>
        </td>
        <td>
          <?php echo $timesheetDetail->total_overtime_hours;?>
        </td>
        <td>
          $<?php echo $timesheetDetail->total_overtime_billrate;?>
        </td>
        <td>
          1
        </td>
        <td>
          $<?php echo $timesheetDetail->total_overtime_billrate;?>
        </td>
      </tr>
       <tr>
        <th colspan="5">
         Double Time
        </th>
      </tr>
      <tr>
        <td>
          <?php echo $candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name."(".$candidateModel->id.")";?>
        </td>
        <td>
          <?php echo $timesheetDetail->total_doubletime_hours;?>
        </td>
        <td>
          $<?php echo $timesheetDetail->total_doubletime_billrate;?>
        </td>
        <td>
          1
        </td>
        <td>
          $<?php echo $timesheetDetail->total_doubletime_billrate;?>
        </td>
      </tr>
      <tr>
        <th>
          Total
        </th>
         <th>
          <?php echo $generatedModel->total_hours;?>
        </th>
        <th>
          $<?php echo $generatedModel->total_billrate;?>
        </th>
        <th>
          1
        </th>
        <th>
          $<?php echo $generatedModel->total_billrate;?>
        </th>
      </tr>
        <?php $total = $total+$generatedModel->total_billrate;?>
    </tbody>
  </table>

                                <br>

                                <!--<table class="table table-bordered m-b-0">
                                  
                                  <tbody>
                                    
                                    <tr>
                                      <td style="width: 86.66%">
                                        tax
                                      </td>
                                     
                                      <td colspan="3" style="width: 13.33%">
                                        -
                                      </td>
                                    </tr>

                                  </tbody>
                                </table>-->

                              </div>
                              <div class="invoice-totals p-t-2 p-b-2">
                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Subtotal
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $<?php $sTotal = $total;echo $sTotal;?>
                                  </span>
                                </div>
                                <?php 
                                $tax = 0;
                                 foreach($invoiceInvoiceTax as $taxValue) {

                                  $invoice= InvoiceTax::model()->findByPk($taxValue->invoice_tax_id);

                                 
                                ?>
                                  <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    <!--<?php echo $invoice->type;?>- --><?php echo $invoice->label;?>
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $<?php    $taxValue = $invoice->value/100*$sTotal.' ('.$invoice->value.'%)';
                                         echo  $taxValue;
                                         $tax = $tax+$taxValue;
                                      ?>
                                  </span>
                                </div>
                                <?php } ?>
                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Total
                                  </strong>
                                  <span class="invoice-totals-value">
                                   $<?php echo $dueAmount = $total+$tax;?>
                                  </span>
                                </div>
                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Amount Paid
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $0.00
                                  </span>
                                </div>
                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title" >
                                    <b style="font-weight: 800">Amount Due</b>
                                  </strong>
                                  <span class="invoice-totals-value">
                                     <b style="font-weight: 800">$<?php echo $dueAmount;?></b>
                                  </span>
                                </div>
                                  <a href="<?php echo $this->createUrl('pdfDownload',array('id'=>$generatedModel->id));?>" class="btn btn-info pull-left" style="margin-right: 10px;">PDF Download</a>
                                  <a href="<?php echo $this->createUrl('pdfDownload',array('id'=>$generatedModel->id,'summmary'=>'yes'));?>" class="btn btn-info pull-left">PDF Download With Summary</a>

                              </div>
                              <small class="p-t-2">
                                <strong>
                                  PAYMENT TERMS AND POLICIES
                                </strong>
                                All accounts are to be paid within 7 days from receipt of invoice. To be paid by cheque or credit card or direct payment online. If account is not paid within 7 days the credits details supplied as confirmation of work undertaken will be charged the agreed
                                quoted fee noted above. If the Invoice remails unpaid. our dept recovery agency, Urban, may charge you a fee of 25% of the unpaid portion of the
                                invoice amount and other legal and collection costs not covered by the fee.
                              </small>
                              <br>
                              <br>

                              <br>


                            </div> 

                            


                               
                           

                        </div>
                        <!-- col -->

                     </div>
                     <!-- row -->
                     <div class="seprater-bottom-100"></div>

            </div>
      