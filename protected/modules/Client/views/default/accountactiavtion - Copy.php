<?php /*?>
<div class="message activated">
  <?php if($this->action->id !='resetpassword') { ?>
  <p>Almost there <br />
    We've just sent you an email. Please check your inbox to validate your account. In a few seconds, you'll be on your personal dashboard!</p>
  <?php } ?>
  <?php if(Yii::app()->user->hasFlash('success')):?>
  <div class="info"> <?php echo Yii::app()->user->getFlash('success'); ?> </div>
  <?php endif; ?>
</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="form-group">
  <label for="">Create Passowrd</label>
  <?php echo $form->passwordField($model,'password',array('value'=>'','class'=>'form-control','required'=>'required')); ?> <?php echo $form->error($model,'password'); ?> </div>
<input type="hidden" name="client_id" value="<?php echo $_POST['client_id']; ?>" />
<div class="form-group">
  <label for="">Confirm Password</label>
  <input type="password" name="Client_password_confirm" value='' class="form-control" placeholder="" id="Client_password_confirm" onkeyup="checkPass(); return false;" required>
  <span id="confirmMessage" class="confirmMessage"></span> </div>
<button type="submit" class="btn-round btn-blue btn-signin">Continue</button>
<?php $this->endWidget(); ?>
<?php */?>

<span id="confirmMessage"></span>

  
  
  
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('class'=>'form-horizontal form-material'),
)); ?>

<form class="form-horizontal form-material" id="loginform" action="index.html">
  <h3 class="box-title m-b-20">Setup your Account Password</h3>
  <div class="form-group ">
    <div class="col-xs-12">
    	<?php echo $form->passwordField($model,'password',array('value'=>'','class'=>'form-control','required'=>'required','placeholder'=>"Create Password")); ?>
        <input type="hidden" name="client_id" value="<?php echo $_GET['client_id']; ?>" />
    </div>
  </div>
  <div class="form-group">
  	<div class="col-xs-12">
    	<input type="password" name="Client_password_confirm" value='' class="form-control" placeholder="Confirm Password" id="Client_password_confirm" onkeyup="checkPass(); return false;" required>
  <!--<span id="confirmMessage" class="confirmMessage"></span>-->
  
    </div>
  </div>
  <div class="form-group text-center m-t-20">
    <div class="col-xs-12">
      <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Continue</button>
    </div>
  </div>
</form>

<?php $this->endWidget(); ?>



<script>
    function checkPass()
    {
        //Store the password field objects into variables ...
        var pass1 = document.getElementById('Client_password');
        var pass2 = document.getElementById('Client_password_confirm');
        //Store the Confimation Message Object ...
        var message = document.getElementById('confirmMessage');
        //Set the colors we will be using ...
        var goodColor = "#66cc66";
        var badColor = "#ff6666";
        //Compare the values in the password field
        //and the confirmation field
        if(pass1.value == pass2.value){
            //The passwords match.
            //Set the color to the good color and inform
            //the user that they have entered the correct password
            pass2.style.backgroundColor = goodColor;
            //message.style.color = goodColor;
            message.innerHTML = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+'<strong>Success!</strong> Your Password Matched.</div>'
        }else{
            //The passwords do not match.
            //Set the color to the bad color and
            //notify the user.
            pass2.style.backgroundColor = badColor;
            //message.style.color = badColor;
            message.innerHTML = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+'<strong>Error!</strong> Password does not matched.</div>' 
        }
    }

</script>