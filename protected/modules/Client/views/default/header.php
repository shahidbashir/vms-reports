<?php /*?><header class="db-header">
  <div class="row">
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      <h4>Step Your Business</h4>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
      <div class="right">
        <div class="form-group"  style=" float: right" > <a type="submit" class="btn btn-green">Add New Jobs</a> </div>
      </div>
    </div>
  </div>
  <!-- row --> 
</header><?php */?>
<?php
		$loginUserId = Yii::app()->user->id;
		$Client = Client::model()->findByPk($loginUserId);
	?>
    <?php
	if(!empty($Client->profile_image)) {
	  $images_path = Yii::app()->baseUrl.'/images/profile_img/'.$Client->profile_image;
		}else{
			$images_path = Yii::app()->request->baseUrl.'/theme-assets/plugins/images/users/varun.jpg';
			}
	  ?>

<!-- Navigation -->

<nav class="navbar navbar-default navbar-static-top m-b-0">
  <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
    <div class="top-left-part" style="margin-right: 20px;"><a class="logo" href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>"><b><img src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/eliteadmin-logo.png" alt="home" /></b><span class="hidden-xs"><img src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/eliteadmin-text.png" alt="home" /></span></a></div>
    <ul class="nav navbar-top-links navbar-left hidden-xs">
      &nbsp;
      <li>
        <form role="search" class="app-search hidden-xs">
          <input type="text" placeholder="Search..." class="form-control">
          <a href=""><i class="fa fa-search"></i></a>
        </form>
      </li>
    </ul>
    <ul class="nav navbar-top-links navbar-right pull-right">
      <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
        <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
        </a>
        
        <!-- /.dropdown-messages --> 
      </li>
      <!-- /.dropdown -->
      <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i>
        <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
        </a>
        
        <!-- /.dropdown-tasks --> 
      </li>
      <!-- /.dropdown -->
      <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="<?php echo $images_path;?>" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $Client->first_name.' '.$Client->last_name; ?></b> </a>
        <ul class="dropdown-menu dropdown-user animated flipInY">
          <!--<li><a href="#"><i class="ti-user"></i> My Profile</a></li>
          <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
          <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
          <li role="separator" class="divider"></li>-->
          <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/logout'); ?>"><i class="fa fa-power-off"></i> Logout</a></li>
        </ul>
        <!-- /.dropdown-user --> 
      </li>
      <li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
      <!-- /.dropdown -->
    </ul>
  </div>
  <!-- /.navbar-header --> 
  <!-- /.navbar-top-links --> 
  <!-- /.navbar-static-side --> 
</nav>
