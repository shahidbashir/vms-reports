<?php

$error = Yii::app()->session->get('error');

if(!empty($error))

{  ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $error; ?>
    </div>
<?php } ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>


<?php endif; ?>
<div class="login-form">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="login-inner">

                <div class="text-center">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/logo-large.png">
                </div>

                <?php $form=$this->beginWidget('CActiveForm', array(

                    'id'=>'login-form',

                    'enableAjaxValidation'=>true,

                )); ?>
                    <h4 class="m-b-30">Reset Password</h4>

                    <div class="form-group">
                        <label for="">Your Email Address</label>
                        <input class="form-control" type="text" required="" name="Client[email]" placeholder="Enter your Email Address">
                    </div>




                    <button type="submit" class="btn btn-success">Submit</button>
                    <p class="m-t-10"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/login'); ?>" class="forgot-password">Login</a></p>

                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div> <!-- col -->
</div> <!-- row -->



