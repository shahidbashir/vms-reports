<?php $this->pageTitle = 'Dashboard'; 
$year = array();
//$date = date('Y-m-d',strtotime(date('Y-m-d').' -4 months'));
for($i=0;$i<date('m');$i++){
    $year[] = date('Y-m-d',strtotime(date('Y-m-d').' - '.$i.' months'));
}


$loginClientData = Client::model()->findByPk(Yii::app()->user->id);
if($loginClientData->member_type==NULL){
    $clientID = Yii::app()->user->id;
}else{
    $clientID = $loginClientData->super_client_id;
}
$months = ''; $submissionTotal = ''; $interviewTotal = ''; $offerTotal = ''; $hiredTotal1 = '';
//submission weekly counting chart data
foreach(array_reverse($year) as $exactDate){
    $dates = date("M",strtotime($exactDate));
    $query = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status IN(3,4,5,6,7,8,9) and rejected_type != 'msp' and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
    $submissionCount = Yii::app()->db->createCommand( $query )->query()->count();

    //$query1 = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status IN(7) and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
    $query1 = "SELECT id FROM vms_offer WHERE client_id=$clientID and status = 4 and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
    $offerCount = Yii::app()->db->createCommand( $query1 )->query()->count();

    //$query21 = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status IN(8) and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
    $query21 = "SELECT id FROM vms_contract WHERE client_id=$clientID and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
    $hiredCount = Yii::app()->db->createCommand( $query21 )->query()->count();

    $months .= "'".$dates."',";
    $submissionTotal .= $submissionCount.",";
    $offerTotal .= $offerCount.",";
    $hiredTotal1 .= $hiredCount.",";
    $totalClientReview = Yii::app()->db->createCommand( "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status=3" )->query()->count();
    $totalOfferPending = Yii::app()->db->createCommand( "SELECT id FROM vms_offer WHERE client_id=$clientID and status=4" )->query()->count();
    $totalCurrentHeadCount = Yii::app()->db->createCommand( "SELECT id FROM vms_contract WHERE client_id=$clientID" )->query()->count();

}


$loginClientData = Client::model()->findByPk(Yii::app()->user->id);
if($loginClientData->member_type==NULL){
    $loginUser = Yii::app()->user->id;
}else{
    $loginUser = $loginClientData->super_client_id;
}
//yearly report for headcount
$year = array();
for($i=0;$i<12;$i++){
    $year[] = date('Y-m-d',strtotime(date('Y-1-d').' + '.$i.' months'));
}
$hiredTotal = ''; $totalBill = ''; $totalhours = ''; $tableData = array();
foreach($year as $exactDate) {
    $dateElements = explode('-', $exactDate);
    $month1 = $dateElements[1];
    $currentMonth =  date("Y-m-d");
    $dateElements1 = explode('-', $currentMonth);
    $month2 = $dateElements1[1];
    if($month1 == $month2){
        $dates = date("M", strtotime($exactDate));
        //overall billrate query
        $query2 = "SELECT sum(total_bilrate_with_tax) as totalBill, sum(total_hours) as totalhour FROM vms_generated_invoice WHERE client_id=$loginUser and Month(invoice_start_date) = Month('" . $exactDate . "') AND Year(invoice_start_date) = Year('" . $exactDate . "')";
        $totalBillQ = Yii::app()->db->createCommand($query2)->query()->read();
        $totalBill .= $totalBillQ['totalBill'];
        //$totalhours .= $totalBillQ['totalhour'];
        $query2 = "SELECT sum(total_hours) as totalBill FROM cp_timesheet WHERE client_id=$loginUser and Month(invoice_start_date) = Month('".$exactDate."') AND Year(invoice_start_date) = Year('".$exactDate."')";
        $totalBillQ = Yii::app()->db->createCommand( $query2 )->query()->read();
        $totalhours .= $totalBillQ['totalBill'];
    }
}

?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

    <div class="cleafix " style="padding: 30px 20px; ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-b-10">Dashboard</h4>
            <p class="m-b-40">Overview of your workforce management - Hiring / Interviews &amp; Submission.</p>
            <div class="dashbaord-two">


                <div class="row ">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="card b-l-blue">
                            <p class="pull-left">Resume to Review</p>
                            <h4 class="pull-right"><?php echo $totalClientReview; ?></h4>
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="card b-l-blue">
                            <p class="pull-left">Spend (<?php echo $dates; ?>)</p>
                            <h4 class="pull-right"><?php if(!empty($totalBill)){ ?>$ <?php echo $totalBill; } ?></h4>
                        </div>
                    </div>


                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="card b-l-blue">
                            <p class="pull-left">Hours (<?php echo $dates; ?>)</p>
                            <h4 class="pull-right"><?php if(!empty($totalhours)){ ?> <?php echo $totalhours; } ?></h4>
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="card b-l-blue">
                            <p class="pull-left">New On-boarding</p>
                            <?php $countcontract = Contract::model()->countByAttributes(array('client_id'=> $clientID)); ?>
                            <h4 class="pull-right"><?php echo $countcontract; ?></h4>
                        </div>
                    </div>

                </div> <!-- row -->

                <br>

                <div class="row ">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">


                        <div class="row m-b-15">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card-custom">
                                    <div class="card-custom-header">
                                        <p class="bold">Head Count Report ( By Category )</p>
                                    </div>
                                    <div class="card-custom-body">
                                        <div id="job-cat-report" style="width: 368px; height: 260px;"></div>
                                    </div>

                                </div>

                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card-custom">
                                    <div class="card-custom-header">
                                        <p class="bold">Spend Reports (<?php echo date('F'); ?>)</p>
                                    </div>
                                    <div class="card-custom-body">
                                        <div id="spend-report" style="width: 420px; height: 260px;"></div>
                                    </div>

                                </div>
                            </div>
                        </div> <!-- row -->

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card-custom">
                                    <div class="card-custom-header">
                                        <p class="bold">Job Category Reports</p>
                                    </div>
                                    <div class="card-custom-body">
                                        <div id="job-cat-report-3" style="width: 368px; height: 260px;"></div>
                                    </div>

                                </div>

                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="card-custom">
                                    <div class="card-custom-header">
                                        <p class="bold">Head Count ( By Location )</p>
                                    </div>
                                    <div class="card-custom-body">
                                        <div id="job-cat-report-4" style="width: 368px; height: 260px;"></div>
                                    </div>

                                </div>

                            </div>
                        </div> <!-- row -->

                    </div>


                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="card-custom">
                            <div class="card-custom-header">
                                <p class="bold">Your Activities</p>
                            </div>

                            <div class="card-custom-body">
                                <table class="table tab-table m-b-20">
                                    <tbody>
                                    <?php
                                    $criteria=new CDbCriteria;
                                    $criteria->condition = 'user_id='. $clientID;
                                    $criteria->condition = "jobStatus = 1 AND jobstep2_complete = 1";
                                    $employees = Job::model()->findAll( $criteria );
                                    $numberOfjob = count($employees);
                                    ?>
                                    <tr>

                                        <td>
                                            <div class="row">
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <span class="count"><?php echo $numberOfjob; ?></span>
                                                    <p>Jobs Waiting for approval</p>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/dashboard-icons/blue/job-aproval@512px-red.svg" class="db-icon">


                                                </div>
                                            </div>


                                        </td>


                                    </tr>
                                            <?php
                                            $sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".$clientID."' and w.status=1";
                                            $interview = Yii::app()->db->createCommand($sql)->query()->readAll();
                                            $nubmerOfpendingInterview = count($interview);
                                            ?>

                                    <tr>
                                        <td>

                                            <div class="row">
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <span class="count"><?php echo $nubmerOfpendingInterview; ?></span>
                                                    <p>New Interview Request</p>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/dashboard-icons/green/new-interview@512px.svg" class="db-icon">

                                                </div>
                                            </div>



                                        </td>


                                    </tr>
                                        <?php
                                        $timeSheet = CpTimesheet::model()->countByAttributes(array('timesheet_status'=>0,'client_id'=>$clientID));
                                        ?>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <span class="count"><?php echo $timeSheet; ?></span>
                                                    <p>Time Sheet Approval</p>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/dashboard-icons/green/time-sheet-approval@512px.svg" class="db-icon">

                                                </div>
                                            </div>

                                        </td>


                                    </tr>


                                    </tbody>
                                </table>
                            </div>


                        </div>

                        <div class="card-custom m-t-30">
                            <div class="card-custom-header">
                                <p class="bold">Reports</p>
                            </div>

                            <div class="card-custom-body">
                                <table class="table tab-table m-b-20">
                                    <tbody>
                                    <?php
                                    $criteria=new CDbCriteria;
                                    $criteria->condition = 'user_id='. $clientID;
                                    $criteria->condition = "jobStatus = 11 AND jobstep2_complete = 1";
                                    $employees = Job::model()->findAll( $criteria );
                                    $newjob = count($employees);
                                    ?>
                                    <tr>

                                        <td>
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <span class="count"><?php echo $newjob; ?></span>
                                                    <p>Active Job Requestions</p>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/dashboard-icons/green/active-jobs@512px.svg" class="db-icon">


                                                </div>

                                            </div>



                                        </td>


                                    </tr>


                                    <tr>
                                        <td>
                                             <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <span class="count"><?php echo $submissionCount; ?></span>
                                                    <p>Total Submissions</p>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/dashboard-icons/green/total-submission@512px.svg" class="db-icon">


                                                </div>

                                            </div>


                                        </td>


                                    </tr>

                                    <tr>
                                        <td>

                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <span class="count"><?php echo $offerCount; ?></span>
                                                    <p>Pending Offers</p>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/dashboard-icons/green/pending-offers@512px.svg" class="db-icon">


                                                </div>

                                            </div>

                                        </td>


                                    </tr>

                                    <tr>
                                        <td>
                                            <?php
                                            $WorkOrder = Workorder::model()->countByAttributes(array('workorder_status'=>0,'client_id'=>$clientID));
                                            ?>
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <span class="count"><?php echo $WorkOrder; ?></span>
                                                    <p>Pending WorkOrder</p>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/dashboard-icons/green/pending-work-order@512px.svg" class="db-icon">


                                                </div>

                                            </div>

                                        </td>


                                    </tr>

                                    </tbody>
                                </table>
                            </div>



                        </div>
                    </div>

                </div>

            </div> <!-- dashbaord-two -->

            <div class="simplify-tabs m-b-50">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs no-hover" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#home" role="tab" data-toggle="tab" data-placement="bottom" title="Today Interview" aria-expanded="true">
                                Today Interview( <?php echo date('m-d-Y'); ?> )</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#tab2" role="tab" data-toggle="tab" data-placement="bottom" title="Calendar View" aria-expanded="false">
                                Calendar View</a>
                        </li>
                    </ul>
                    <div class="tab-content ">
                        <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                            <div class="row table-row-to-remove-margin" style="margin-top: -37px;">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <table class="table m-b-30 m-t-20 without-border">
                                        <thead class="thead-default">
                                        <tr>
                                            <th style="width: 10px;">Status</th>
                                            <th>Type</th>
                                            <th style="width: 109px;"><a href="">ID</a></th>
                                            <th>Name</th>
                                            <th> Job( Job ID ) </th>
                                            <th>Date</th>
                                            <th style="width: 96px;">Start Time</th>
                                            <th style="width: 96px;">End Time</th>
                                            <!--<th style="width: 100px;">Location</th>-->
                                            <th class="text-center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $model = $todayInterview->getData();
                                        if($model){
                                            foreach($model as $allData){

                                                $location = Location::model()->findByPk($allData['interview_where']);
                                                $postingstatus = UtilityManager::interviewStatus();
                                                $candidateData = Candidates::model()->findByPk($allData['candidate_Id']);

                                                switch ($postingstatus[$allData['status']]) {
                                                    case "Approved":
                                                        $color = 'label-open';
                                                        break;
                                                    case "Waiting for Approval":
                                                        $color = 'label-interview-pending';
                                                        break;
                                                    case "Cancelled":
                                                        $color = 'label-interview-cancelled';
                                                        break;
                                                    case "Reschedule":
                                                        $color = 'label-interview-reschedule';
                                                        break;
                                                    case "Interview Completed":
                                                        $color = 'label-interview-completed';
                                                        break;
                                                    default:
                                                        $color = 'label label-primary';
                                                }


                                                if($allData['start_date_status']){
                                                    $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
                                                }

                                                else if($allData['alt1_date_status']){
                                                    $date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
                                                }
                                                else if($allData['alt2_date_status']){
                                                    $date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
                                                }
                                                else if($allData['alt3_date_status']){
                                                    $date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
                                                }else{
                                                    $date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
                                                }

                                                $newTime = explode(' ',$date);

                                                $jobData = Job::model()->findByPk($allData['job_id']);
                                                ?>
                                                <tr>
                                                    <td><span class="tag <?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span></td>
                                                    <td><?php echo $allData['interview_type'] ?></td>
                                                    <td><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>"><?php echo $allData['id'] ?></a></td>
                                                    <td><a href=""><?php echo $jobData->title.'('. $jobData->id.')'; ?></a></td>
                                                    <td><a href="">
                                                            <?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?>
                                                        </a></td>
                                                    <td><?php echo date('m-d-Y',strtotime($newTime[0])); ?></td>
                                                    <td><?php echo $newTime[1] ?></td>
                                                    <td><?php echo $newTime[3]; ?></td>
                                                    <!--<td ><?php /*echo $location->name; */?></td>-->
                                                    <td style="text-align: center"><a href="<?php echo $this->createAbsoluteUrl('job/scheduleInterview',array('id'=>$allData['job_id'],'submission-id'=>$allData['submissionID'],'interviewId'=>$allData['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                                                </tr>
                                            <?php }
                                        }else{ ?>
                                            <tr>
                                                <td colspan="10">Sorry No Record Found</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                    <div class="row m-b-10" style="padding: 10px 0 10px; ">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <?php
                                            $this->widget('CLinkPager', array(
                                                'pages' => $todayInterview->pagination,
                                                'header' => '',
                                                'nextPageLabel' => 'Next',
                                                'prevPageLabel' => 'Prev',
                                                'selectedPageCssClass' => 'active',
                                                'hiddenPageCssClass' => 'disabled',
                                                'htmlOptions' => array(
                                                    'class' => 'pagination m-t-0 m-b-0',
                                                )
                                            ))
                                            ?>
                                        </div>
                                        <!--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                   <p class="text-right"> Showing 10 to 20 of 50 entries </p>
                               </div>-->
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                        </div>
                        <!-- tab-pane -->
                        <div class="tab-pane" id="tab2" role="tabpanel" aria-expanded="false">
                            <?php $this->renderPartial('/interview/calendarView',array('completedInterviews'=>$completedInterviews)); ?>
                        </div>
                        <!-- tab-pane -->
                    </div>
                </div>
            </div>


            <div class="row dashbaord-two">
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                    <div class="card-custom">
                        <div class="card-custom-header">
                            <p class="bold">Trends</p>
                        </div>
                           <canvas id="myChart" style="height: 599px; width: 1199px;" width="1199" height="599"></canvas>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 right">
                      <div class="data-box">
                        <?php  
                            $criteria=new CDbCriteria;
                            $criteria->condition = 'user_id='. $clientID;
                            $criteria->condition = "jobStatus != 1 AND jobStatus != ''";
                            $employees = Job::model()->findAll( $criteria );
                            $numberOfjob = count($employees);
                          ?>
                        <h4><?php echo $numberOfjob; ?></h4>
                        <p>Jobs</p>
                      </div>
                      <div class="data-box">
                         <h4><?php echo $submissionCount; ?></h4>
                        <p>Submission</p>
                      </div>
                      <div class="data-box">
                        <h4><?php echo $offerCount; ?></h4>
                        <p>Offer</p>
                      </div>
                      <div class="data-box no-border">
                        <?php $countcontract = Contract::model()->countByAttributes(array('client_id'=> $clientID)); ?>
                        <h4><?php echo $countcontract; ?></h4>
                        <p>Hired</p>
                      </div>
                    </div>
                
               </div>
          </div>
        <!-- col -->

    </div>
    <!-- row -->
    <div class="seprater-bottom-100"></div>

</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart-ui.css">
<?php
/* query for head count report bt category */


$jobgategory = Setting::model()->findAllByAttributes(array('category_id'=>'9'),array('order'=>'id asc'));
$number = '';
$string = '';
if($jobgategory){
    foreach ($jobgategory as $catvalue){
        $sql = 'SELECT j.* FROM vms_job as j INNER JOIN vms_contract as c on j.id = c.job_id and j.cat_id="'.$catvalue->title.'" and c.client_id='.$loginUser;

        $categoryreports = Yii::app()->db->createCommand($sql)->query()->readAll();
        $number = count($categoryreports);
        if($number >0) {
            $string .= '[' . '"' . $catvalue->title . '"' . ',' . $number . '],';
        }
    } }

/* query for job category report graph */


$job_cat_reportData = '';
$jobCategory = Setting::model()->findAll(array('condition'=>'category_id=9'));
foreach($jobCategory as $cat){
    $jobs = Job::model()->countByAttributes(array('user_id'=>$loginUser,'cat_id'=>$cat->title));
    $job_cat_reportData .= "['".$cat->title."', $jobs],";
}
/* query for head count by location graph */


$HeadcountBylocation = '';
$Location = Location::model()->findAll();
foreach($Location as $key=>$value) {
    $query4 = "SELECT count(vms_contract.id) as numberOfhired FROM vms_contract RIGHT JOIN vms_offer ON vms_contract.offer_id = vms_offer.id and vms_offer.approver_manager_location= '" . $value->id . "' WHERE vms_contract.client_id = $loginUser";
    $record = Yii::app()->db->createCommand($query4)->query()->read();
    $numberOfHired = $record['numberOfhired'];
    $HeadcountBylocation .= "['".$value->name."', $numberOfHired],";
}

/* spend report graph */
$serias = '';
$cat ='';
$monthly = '';
$yearly = '';
$monthly_array = array();
$yearly_array = array();
$query4 = "SELECT  vms_generated_invoice.invoice_start_date as dated , sum(vms_generated_invoice.total_bilrate_with_tax) as total_bilrate_with_tax,vms_job.cat_id as category FROM vms_generated_invoice INNER JOIN vms_job ON vms_generated_invoice.job_id = vms_job.id WHERE client_id = $loginUser  Group By vms_job.cat_id ORDER BY dated";
$tableData2 = Yii::app()->db->createCommand( $query4 )->query()->readAll();
$loginUserId = UtilityManager::superClient(Yii::app()->user->id);
$now = new \DateTime('now');
$currentM = $now->format('m');
foreach($tableData2 as $key=>$value){
    $query5 = "SELECT sum(vms_generated_invoice.total_bilrate_with_tax) as total_bilrate_with_tax FROM vms_generated_invoice INNER JOIN vms_job ON vms_generated_invoice.job_id = vms_job.id and vms_job.cat_id= '".$value['category']."' and DATE_FORMAT(vms_generated_invoice.invoice_start_date,'%m')='".$currentM."' WHERE client_id = $loginUserId  Group By vms_job.cat_id";
    $record = Yii::app()->db->createCommand( $query5 )->query()->read();
if($record) {
    $monthly_array[$value['category']] = $record ['total_bilrate_with_tax'];
}
}
    $loginUserId = UtilityManager::superClient(Yii::app()->user->id);
    $now = new \DateTime('now');
    $currentY = $now->format('y');
    foreach($tableData2 as $key=>$value){
        $query6 = "SELECT sum(vms_generated_invoice.total_bilrate_with_tax) as total_bilrate_with_tax FROM vms_generated_invoice INNER JOIN vms_job ON vms_generated_invoice.job_id = vms_job.id and vms_job.cat_id= '".$value['category']."' and DATE_FORMAT(vms_generated_invoice.invoice_start_date,'%y')='".$currentY."' WHERE client_id = $loginUserId  Group By vms_job.cat_id";
        $record = Yii::app()->db->createCommand( $query6 )->query()->read();
        if($record) {
            $yearly_array[$value['category']] = $record ['total_bilrate_with_tax'];
        }
}
$i=0;
$monthName=date('F');
$yearName=date('Y');
$monthlydata ='';
$yearlydata ='';
if(!empty($yearly_array)) {
    foreach ($yearly_array as $key => $value) {
        $i++;
        if (!empty($monthly_array)) {
            $monthly .= $monthly_array[$key] . ',';
        }
        $yearly .= $value . ',';
        $serias .= 'var seriesData_' . $i . ' = data.mapAs({x: [0], value: [' . $i . ']});';
        $cat .= ' var series' . $i . ' = chart.bar(seriesData_' . $i . ');
        series' . $i . '.name("' . $key . '");';
    }
}
$monthlydata = '["Monthly('.$monthName.')", '.$monthly .']';
$yearlydata = '["Yearly('.$yearName.')", '.$yearly .']';
?>
<script>

    anychart.onDocumentLoad(function() {

        // create an instance of a pie chart with data
        var hiringHeadCountData = [<?php echo $string; ?>];

        var hiringHeadCountChart_2 = anychart.pie( hiringHeadCountData );

        // pass the container id, chart will be displayed there
        hiringHeadCountChart_2.container("job-cat-report");
        // call the chart draw() method to initiate chart display
        hiringHeadCountChart_2.draw();


        //////////////////////////////
        // create an instance of a pie chart with data
        var headCountData =
            [
                {name:"Open", value:100},
                {name:"Pending Approval", value:70},
                {name:"Re-open", value:57},
                {name:"Rejected", value:38},
                {name:"Filled", value:15},
                {name:"Hold", value:10},
                {name:"Draft", value:38},
                {name:"New Request", value:15}
            ];

        var headCountChart_3 = anychart.funnel( headCountData );
        headCountChart_3.title("Overall Job Report");
        // pass the container id, chart will be displayed there
        // set chart legend settings
        var legend = headCountChart_3.legend();
        legend.enabled(true);
        legend.position("center");
        legend.itemsLayout("horizontal");
        legend.align("top center");
        headCountChart_3.saveAsPdf();
        // set chart base width settings
        headCountChart_3.baseWidth("70%");
        // set the neck height
        headCountChart_3.neckHeight("0%");

        headCountChart_3.container("overall-jobreport");
        headCountChart_3.background('white');
        // call the chart draw() method to initiate chart display
        headCountChart_3.draw();


        /////////////////////////////////////////////////////////////////////////////

        var data = anychart.data.set([
            <?php echo $monthlydata.','.$yearlydata; ?>
        ]);

        // map the data
        <?php echo $serias; ?>

        // create a chart
        chart = anychart.bar();

        // // enable the percent stacking mode
        // chart.yScale().stackMode("percent");
        // set stack mode
        chart.yScale().stackMode('value');

        // create area series, set the data
        <?php echo $cat; ?>





        // set the container id
        chart.container("spend-report");

        // initiate drawing the chart
        chart.draw();

        //////////////////////////////
        // create an instance of a pie chart with data
        var headCountData =
            [
                {name:"Open", value:100},
                {name:"Pending Approval", value:70},
                {name:"Re-open", value:57},
                {name:"Rejected", value:38},
                {name:"Filled", value:15},
                {name:"Hold", value:10},
                {name:"Draft", value:38},
                {name:"New Request", value:15}
            ];

        var headCountChart = anychart.funnel( headCountData );
        headCountChart.title("Overall Job Report");
        // pass the container id, chart will be displayed there
        // set chart legend settings
        var legend = headCountChart.legend();
        legend.enabled(true);
        legend.position("center");
        legend.itemsLayout("horizontal");
        legend.align("top center");
        headCountChart.saveAsPdf();
        // set chart base width settings
        headCountChart.baseWidth("70%");
        // set the neck height
        headCountChart.neckHeight("0%");

        headCountChart.container("overall-jobreport");
        headCountChart.background('white');
        // call the chart draw() method to initiate chart display
        headCountChart.draw();


        // create an instance of a pie chart with data
        var hiringHeadCountData = [

            <?php echo $job_cat_reportData; ?>
        ];

        var hiringHeadCountChart = anychart.pie( hiringHeadCountData );

        // pass the container id, chart will be displayed there
        hiringHeadCountChart.container("job-cat-report-3");
        // call the chart draw() method to initiate chart display
        hiringHeadCountChart.draw();


        //////////////////////////////
        // create an instance of a pie chart with data
        var headCountData =
            [
                {name:"Open", value:100},
                {name:"Pending Approval", value:70},
                {name:"Re-open", value:57},
                {name:"Rejected", value:38},
                {name:"Filled", value:15},
                {name:"Hold", value:10},
                {name:"Draft", value:38},
                {name:"New Request", value:15}
            ];

        var headCountChart_4 = anychart.funnel( headCountData );

        // pass the container id, chart will be displayed there
        // set chart legend settings
        var legend = headCountChart_4.legend();
        legend.enabled(true);
        legend.position("center");
        legend.itemsLayout("horizontal");
        legend.align("top center");
        headCountChart_4.saveAsPdf();
        // set chart base width settings
        headCountChart_4.baseWidth("70%");
        // set the neck height
        headCountChart_4.neckHeight("0%");

        headCountChart_4.container("overall-jobreport");
        headCountChart_4.background('white');
        // call the chart draw() method to initiate chart display
        headCountChart_4.draw();


        // create an instance of a pie chart with data
        var hiringHeadCountData = [ <?php echo $HeadcountBylocation; ?>];

        var hiringHeadCountChart_6 = anychart.pie( hiringHeadCountData );

        // pass the container id, chart will be displayed there
        hiringHeadCountChart_6.container("job-cat-report-4");
        // call the chart draw() method to initiate chart display
        hiringHeadCountChart_6.draw();






    });

</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        if ( $('#myChart').length ) {
            var ctx = $('#myChart');
            console.log(ctx);
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [<?php echo $months; ?>],
                    datasets: [{
                        label: 'Submission',
                        backgroundColor: '#4CC3F0',
                        data: [<?php echo $submissionTotal; ?>]
                    }, {
                        label: 'Offer',
                        backgroundColor: '#D26D54',
                        data: [<?php echo $offerTotal; ?>]
                    },
                        {
                            label: 'Hired',
                            backgroundColor: '#7FC35C',
                            data: [<?php echo $hiredTotal1; ?>]
                        }]
                }
            });
        };
    });
</script>
