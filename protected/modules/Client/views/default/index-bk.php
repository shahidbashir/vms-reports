<?php $this->pageTitle = 'Dashboard';
/*$baseUrl = Yii::app()->getBaseUrl(true);
echo CHtml::link($baseUrl.'/candidate', $baseUrl.'/candidate');*/
?>
<!--<a href="<?php /*echo $homeUrl; */?>candidate/index.php?r=site/login" style="color:#069;font-size:13px;line-height:1.4;text-decoration:none" target="_blank">click here</a>
--><div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Dashboard</h4>
      <?php /*?><button onclick="_pcq.push(['triggerOptIn',{httpWindowOnly: true}]);">
        GET NOTIFICATIONS
        </button>
        <a onclick="_pcq.push(['triggerOptIn',{httpWindowOnly: true}]);">
        GET NOTIFICATIONS
        </a><?php */?>
      <p class="m-b-40"> Overview of your workforce management - Hiring / Interviews &amp; Submission.
        <?php 
			$year = array();
			//$date = date('Y-m-d',strtotime(date('Y-m-d').' -4 months'));
			for($i=0;$i<12;$i++){
				$year[] = date('Y-m-d',strtotime(date('Y-m-d').' - '.$i.' months'));
			}
			
			$loginClientData = Client::model()->findByPk(Yii::app()->user->id);
			if($loginClientData->member_type==NULL){
				$clientID = Yii::app()->user->id;
				}else{
					 $clientID = $loginClientData->super_client_id;
					 }
			
			$months = ''; $submissionTotal = ''; $interviewTotal = ''; $offerTotal = ''; $hiredTotal = '';
			//submission weekly counting chart data
			foreach(array_reverse($year) as $exactDate){
					$dates = date("M",strtotime($exactDate));
					$query = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status IN(3,4,5,6,7,8,9) and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
					$submissionCount = Yii::app()->db->createCommand( $query )->query()->count();
					
					//$query1 = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status IN(7) and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
					$query1 = "SELECT id FROM vms_offer WHERE client_id=$clientID and status IN(1,4) and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
					$offerCount = Yii::app()->db->createCommand( $query1 )->query()->count();
					
					//$query2 = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status IN(8) and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
					$query2 = "SELECT id FROM vms_contract WHERE client_id=$clientID and Month(date_created) = Month('".$exactDate."') AND Year(date_created) = Year('".$exactDate."')";
					$hiredCount = Yii::app()->db->createCommand( $query2 )->query()->count();
					
					$months .= "'".$dates."',";
					$submissionTotal .= $submissionCount.",";
					$offerTotal .= $offerCount.",";
					$hiredTotal .= $hiredCount.",";
				}
			
			
			$totalClientReview = Yii::app()->db->createCommand( "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status=4" )->query()->count();
			$totalOfferPending = Yii::app()->db->createCommand( "SELECT id FROM vms_offer WHERE client_id=$clientID and status=4" )->query()->count();
			$totalCurrentHeadCount = Yii::app()->db->createCommand( "SELECT id FROM vms_contract WHERE client_id=$clientID" )->query()->count();	
			
			?>
      </p>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-block">
              <p>Resumes to be reviewed</p>
              <h4><a href="<?php echo $this->createAbsoluteUrl('client/admin'); ?>" class=""><?php echo $totalClientReview; ?></a></h4>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-block">
              <p>Offers pending</p>
              <h4><a href="<?php echo $this->createAbsoluteUrl('job/offer'); ?>" class=""><?php echo $totalOfferPending; ?></a></h4>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-block">
              <p>Current Headcount</p>
              <h4><a href="<?php echo $this->createAbsoluteUrl('contract/index'); ?>" class=""><?php echo $totalCurrentHeadCount; ?></a></h4>
            </div>
          </div>
        </div>
          <?php
          $loginClientData = Client::model()->findByPk(Yii::app()->user->id);
          if($loginClientData->member_type==NULL){
              $loginUser = Yii::app()->user->id;
          }else{
              $loginUser = $loginClientData->super_client_id;
          }
          //yearly report for headcount
          $year = array();
          for($i=0;$i<12;$i++){
              $year[] = date('Y-m-d',strtotime(date('Y-1-d').' + '.$i.' months'));
          }
          $hiredTotal = ''; $totalBill = ''; $totalhours = ''; $tableData = array();
          foreach($year as $exactDate) {
              $dateElements = explode('-', $exactDate);
              $month1 = $dateElements[1];
              $currentMonth =  date("Y-m-d");
              $dateElements1 = explode('-', $currentMonth);
              $month2 = $dateElements1[1];
              if($month1 == $month2){
              $dates = date("M", strtotime($exactDate));
              //overall billrate query
              $query2 = "SELECT sum(total_bilrate_with_tax) as totalBill, sum(total_hours) as totalhour FROM vms_generated_invoice WHERE client_id=$loginUser and Month(invoice_start_date) = Month('" . $exactDate . "') AND Year(invoice_start_date) = Year('" . $exactDate . "')";
              $totalBillQ = Yii::app()->db->createCommand($query2)->query()->read();
              $totalBill .= $totalBillQ['totalBill'];
                  $totalhours .= $totalBillQ['totalhour'];
              }
          }

          ?>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-block">
              <p>Total Spend (<?php echo $dates; ?>)</p>
              <h4><a href="<?php echo $this->createAbsoluteUrl('reports/spendReport'); ?>" class=""><?php if(!empty($totalBill) && !empty($totalhours)){ ?>$ <?php echo $totalBill; ?> ( Total Hour :<?php echo $totalhours; ?> )<?php }else{ echo '0'; }?></a></h4>
            </div>
          </div>
        </div>
      </div>
      <div class="card dashoard-chart">
        <div class="card-block">
          <div class="row">
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <p> Trends</p>
              <hr>
              <canvas id="myChart" style="height:250px; width:250px;" ></canvas>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 right">
              <div class="data-box">
                <?php  
				  	$criteria=new CDbCriteria;
				  	$criteria->condition = 'user_id='. $clientID;
				  	$criteria->condition = "jobStatus != 1 AND jobStatus != ''";
					$employees = Job::model()->findAll( $criteria );
				  	$numberOfjob = count($employees);
				  ?>
                <h4><?php echo $numberOfjob; ?></h4>
                <p>Jobs</p>
              </div>
              <div class="data-box">
                <?php $countsubmission = VendorJobSubmission::model()->countByAttributes(array('client_id'=> $clientID,'resume_status'=>array(3,4,5,6,7,8,9))); ?>
                <h4><?php echo $countsubmission; ?></h4>
                <p>Submission</p>
              </div>
              <div class="data-box">
                <?php $countoffer = Offer::model()->countByAttributes(array('client_id'=> $clientID,'status'=>array(1,4))); ?>
                <h4><?php echo $countoffer; ?></h4>
                <p>Offer</p>
              </div>
              <div class="data-box no-border">
                <?php $countcontract = Contract::model()->countByAttributes(array('client_id'=> $clientID)); ?>
                <h4><?php echo $countcontract; ?></h4>
                <p>Hired</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="simplify-tabs">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <li class="nav-item"> <a class="nav-link active"  href="#home" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/interviews@512px.svg"></i> Interview Schedule</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#home2" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/workorder@512px.svg"></i> Work Flow Approval</a> </li>
              </ul>
              <div class="tab-content tab-content-scrollable">
                <div class="tab-pane active" id="home"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <?php
						$todaydate = date('Y-m-d');
						$date = strtotime($todaydate);
						$date = strtotime("+2 day", $date);
						$tomorrowdate = date('Y-m-d', $date);
						$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".$clientID."'
						and ((w.interview_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.start_date_status =1) or (w.interview_alt1_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt2_date_status =1) or (w.interview_alt3_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt3_date_status =1))";
						$interview = Yii::app()->db->createCommand($sql)->query()->readAll();
						?>
                      <p class="m-b-10 p-l-15"> Interview schedule for next 72 hours </p>
                      <table class="table tab-table m-b-20">
                        <tbody>
                          <?php
							if($interview){
							foreach($interview as $allData){
							$candidateData = Candidates::model()->findByPk($allData['candidate_Id']);
							  $jobData = Job::model()->findByPk($allData['job_id']);
							  if($allData['start_date_status']){
								$date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
							  }
							  else if($allData['alt1_date_status']){
								$date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
							  }
							  else if($allData['alt2_date_status']){
								$date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
							  }
							  else if($allData['alt3_date_status']){
								$date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
							  }else{
								$date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
							  }
							  $newTime = explode(' ',$date);
								$postingstatus = UtilityManager::interviewStatus();
										switch ($postingstatus[$allData['status']]) {
											case "Approved":
												$color = 'label label-success';
												break;
											case "Waiting for Approval":
												$color = 'label-interview-pending';
												break;
											case "Cancelled":
												$color = 'label-interview-cancelled';
												break;
											case "Reschedule":
												$color = 'label-interview-reschedule';
												break;
									   case "Interview Completed":
												$color = 'label-interview-completed';
												break;
											default:
												$color = 'label-new-request';
										}
										?>
                          <tr>
                            <td><?php echo date('m-d-Y',strtotime($newTime[0])); ?> - <strong>
                              <?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?>
                              </strong> ( <?php echo $allData['interview_type'] ?> ) <br>
                              <span class="tag <?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span> <?php echo $newTime[1].' '.$newTime[2].' to '.$newTime[4].' '.$newTime[5]; ?> - Interview ID : <a href="" class="underline"><?php echo $allData['id'] ?></a></td>
                          </tr>
                          <?php } }else{ ?>
                          <tr>
                            <td>Sorry No Interview in Next 72 hours</td>
                          </tr>
                          <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!--  tab-pane -->
                
                <div class="tab-pane " id="home2"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <?php 
						$loginUserId = Yii::app()->user->id;
						$pendingApproval = JobWorkflow::model()->findAllByAttributes(array('client_id'=>$loginUserId,'job_status'=>'Pending'));
						$numberofjob = count($pendingApproval);
						 ?>
                      <p class="m-b-10 p-l-15"> You have currently
                        <?php if($numberofjob){ echo $numberofjob; }else{ echo '0'; } ?>
                        jobs to approve, you are part of the approval </p>
                      <table class="table tab-table m-b-20">
                        <tbody>
                          <?php if($pendingApproval){ 
								foreach($pendingApproval as $jobs){
									$jobData = Job::model()->findByPk($jobs->job_id);
								?>
                          <tr>
                            <td><span class="tag label-pending-aproval">Pending Approval</span> - <?php echo $jobData->title; ?> ( <a href="" class="underline"><?php echo $jobData->id; ?></a> ), <?php echo $jobData->bill_rate; ?> <br>
                              <?php echo $jobData->num_openings; ?> Openings </td>
                          </tr>
                          <?php } }else{ ?>
                          <tr>
                            <td>Sorry No WorkFlow Approval</td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!--  tab-pane --> 
                
              </div>
            </div>
            <!-- tabpanel --> 
          </div>
          <!-- simplify-tabs --> 
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="simplify-tabs">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <li class="nav-item"> <a class="nav-link active"  href="#submission" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-overview@512px.svg"></i> New Submission</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#offers" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-overview@512px.svg"></i> Offers</a> </li>
              </ul>
              <div class="tab-content tab-content-scrollable">
                <div class="tab-pane active" id="submission"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <p class="m-b-10 p-l-15"> Latest submissions on jobs </p>
                      <table class="table tab-table m-b-20">
                        <tbody>
                          <?php 
							$date_start = date('Y-m-d');
							$date = strtotime($todaydate);
							$date = strtotime("+3 day", $date);
							$date_end = date('Y-m-d', $date);
							$loginUserId = $clientID;
							$criteria = new CDbCriteria();
							$criteria->addCondition("client_id", $loginUserId);
							$criteria->addInCondition('resume_status',array(3,4,5,6,7,8,9));
							$criteria->addBetweenCondition("date_created",$date_start,$date_end,'AND');
							$submission = VendorJobSubmission::model()->findAll($criteria);
							if($submission){
								foreach($submission as $value){
									$Jobsdata = Job::model()->findByPk($value->job_id);
									$status = UtilityManager::resumeStatus();
							 switch ($status[$value->resume_status]) {
								 case "Submitted":
									 $color = 'label-hold';
									 break;
								 case "MSP Review":
									 $color = 'label-filled';
									 break;
								 case "MSP Shortlisted":
									 $color = 'label-new-request';
									 break;
								 case "Client Review":
									 $color = 'label-filled';
									 break;
								 case "Interview Process":
									 $color = 'label-open';
									 break;
								 case "Rejected":
									 $color = 'label-rejected';
									 break;
								 case "Offer":
									 $color = 'label-pending-aproval';
									 break;
								 case "Approved":
									 $color = 'label-new-request';
									 break;
								 case "Work Order Release":
									 $color = 'label-re-open';
									 break;
								 default:
									 $color = 'label-new-request';
							 }
							?>
                          <tr>
                            <td><span class="tag <?php echo $color; ?>"><?php echo $status[$value->resume_status]; ?></span> Submission  ID : <a href="" class="underline"><?php echo $value->id; ?></a> - <?php echo $Jobsdata->title; ?> , <?php echo $value->date_created; ?> <br>
                              Pay Rate: <?php echo $Jobsdata->pay_rate; ?>, Bill Rate: <?php echo $Jobsdata->bill_rate; ?></td>
                          </tr>
                          <?php } }else{ ?>
                          <tr>
                            <td>Sorry No Submission</td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="offers"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <p class="m-b-10 p-l-15"> Pending Offers Approval </p>
                      <table class="table tab-table m-b-20">
                        <tbody>
                          <?php $pendingOffers = Offer::model()->findAllByAttributes(array('status'=>4,'client_id'=>$clientID));
								if($pendingOffers){
									foreach($pendingOffers as $offervalue){
										$job = Job::model()->findByPk($offervalue->job_id);
										$candidate = Candidates::model()->findByPk($offervalue->candidate_id);
										$locationdat = Location::model()->findByPk($offervalue->approver_manager_location);
								?>
                          <tr>
                            <td><span class="tag label-hold">Waiting for Approval</span> <?php echo $candidate->first_name.' '.$candidate->last_name; ?> - <?php echo $job->title; ?> (<a href="" class="underline"><?php echo $job->id; ?></a>) <br>
                              Location: <?php echo $locationdat->name; ?>, Payrate: <?php echo $offervalue->offer_pay_rate; ?></td>
                          </tr>
                          <?php } }else{ ?>
                          <tr>
                            <td>Sorry No Offer Created</td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
            
          </div>
          <!-- simplify-tabs --> 
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		if ( $('#myChart').length ) {
         var ctx = $('#myChart');
       console.log(ctx); 
       var myChart = new Chart(ctx, {
         type: 'bar',
         data: {
            labels: [<?php echo $months; ?>],
           datasets: [{
             label: 'Submission',
             backgroundColor: '#4CC3F0',
             data: [<?php echo $submissionTotal; ?>]
           }, {
             label: 'Offer',
             backgroundColor: '#D26D54',
             data: [<?php echo $offerTotal; ?>]
           },
           {
             label: 'Hired',
             backgroundColor: '#7FC35C',
             data: [<?php echo $hiredTotal; ?>]
           }]
         }
       });
      }; 
	  });
</script> 
