<div class="login-inner">

             <div class="text-center">
                 <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/logo-large.png">
             </div>

             <?php

             $error = Yii::app()->session->get('error');

             if(!empty($error))

             {  ?>
             <div class="alert alert-danger">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 <strong>Title!</strong> <?php echo $error; ?>.
             </div>
             <?php } ?>

             <?php if(Yii::app()->user->hasFlash('success')):?>

                 <?php echo Yii::app()->user->getFlash('success'); ?>

             <?php endif; ?>

             <?php $form=$this->beginWidget('CActiveForm', array(

                 'id'=>'login-form',

                 'enableAjaxValidation'=>true,

             )); ?>

                 <h4 class="m-b-30">Member Sign In</h4>

                 <div class="form-group">
                     <label for="">Your Email Address</label>
                     <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'Email Address','id'=>'username','required'=>true)); ?>

                 </div>


                 <div class="form-group">
                     <label for="">Password</label>
                     <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Password','id'=>'password','required'=>true)); ?>
                 </div>



                 <button type="submit" class="btn btn-success">Login</button>
                 <p class="m-t-10"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/forgotPassword'); ?>" class="forgot-password">Forgot Password?</a></p>

             <?php $this->endWidget(); ?>
         </div>


