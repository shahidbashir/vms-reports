 <?php $this->pageTitle = 'Search Result'; ?>
                    <div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                     
                     <div class="cleafix " style="padding: 30px 20px 10px; ">  
                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <h4 class="m-b-10 page-title">Search Results
                              
                              <a href="" class="pull-right"><i class="fa fa-question"></i></a>
                              <a href="" class="pull-right"><i class="fa fa-info"></i></a>
                              

                           </h4> 
                           <p class="m-b-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>

                            <div class="m-b-30">
                            
                           </div>
                           

                          <!--<div class="search-box">
                             <div class="two-fields">

                                 <div class="form-group">
                                   <label for=""></label>
                                   <input type="text" class="form-control" id="" placeholder="Search Keyword">
                                 </div>

                                  <div class="form-group">
                                   <label for="">&nbsp;</label>
                                   <button type="button" class="btn btn-primary btn-block">Search</button>
                                 </div>
                                 
                             </div> 
                          </div>-->

                          <div class="row">

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                              <p class="m-b-10 bold">Job Requistions</p>

                                <table class="table">
                                  <tbody>
                                   <?php if($jobs){
									
										foreach($jobs as $jobvalue){ ?>
                                    <tr>
                                      <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview',array('id'=>$jobvalue['id'],'type'=>'info')); ?>" class="underline"><?php echo $jobvalue['title']; ?></a>
                                        <br>
                                        <span><?php echo 'Bill Rate : $'.$jobvalue['bill_rate'].' / ' .'Number :'.$jobvalue['number_submission']; ?></span></td>
                                    </tr>
									<?php } } ?>

                                  </tbody>
                                </table>
                            </div>
                            

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                              
                              <p class="m-b-10 bold"> Submission</p>

                              <table class="table">
                                  <tbody>
                                   <?php if($submission){ 
									  foreach($submission as $subvalue){
										  $candidates = Candidates::model()->findByPk($subvalue['candidate_Id']);
									  ?>
                                    <tr>
                                      <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$subvalue['id'])); ?>" class="underline"><?php echo $candidates->first_name.' '.$candidates->last_name; ?></a>
                                        <br>
                                        <span><?php echo 'Pay Rate : $'.$subvalue['candidate_pay_rate'].' / ' .'Current Location :'.$candidates->current_location; ?></span></td>
                                    </tr>
                                    <?php } } ?>

                                  </tbody>
                                </table>
                            </div>
                            

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                              <p class="m-b-10 bold">Interview</p>
                              <table class="table">
                                  <tbody>
                                   <?php if($interview){
									  foreach($interview as $intervalue){
									  ?>
                                    <tr>
                                      <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/scheduleInterview',array('id'=>$intervalue['job_id'],'submission-id'=>$intervalue['submission_id'],'interviewId'=>$intervalue['id'])); ?>" class="underline"><?php echo $intervalue['interview_event_name']; ?></a>
                                        <br>
                                          <?php
                                          if($intervalue['start_date_status']){
                                              $date = $intervalue['interview_start_date'].' '.$intervalue['interview_start_date_time'].' to '. $intervalue['interview_end_date_time'];
                                          }
                                          else if($intervalue['alt1_date_status']){
                                              $date = $intervalue['interview_alt1_start_date'].' '.$intervalue['interview_alt1_start_date_time'].' to '.$intervalue['interview_alt1_end_date_time'];
                                          }
                                          else if($intervalue['alt2_date_status']){
                                              $date = $intervalue['interview_alt2_start_date'].' '.$intervalue['interview_alt2_start_date_time'].' to '.$intervalue['interview_alt2_end_date_time'];
                                          }
                                          else if($intervalue['alt3_date_status']){
                                              $date = $intervalue['interview_alt3_start_date'].' '.$intervalue['interview_alt3_start_date_time'].' to '.$intervalue['interview_alt3_end_date_time'];
                                          }else{
                                              $date = $intervalue['interview_start_date'].' '.$intervalue['interview_start_date_time'].' to '.$intervalue['interview_end_date_time'];
                                          }
                                          $newTime = explode(' ',$date);
                                          ?>
                                        <span>Date of Interview: <?php echo date('m-d-Y',strtotime($newTime[0])); ?> / Type of Interview: <?php echo $intervalue['interview_type']; ?> / ID: <?php echo $intervalue['id']; ?> </span></td>
                                    </tr>
									<?php } } ?>
                                  </tbody>
                                </table>
                            </div>
                            

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                              <p class="m-b-10 bold">Offers</p>
                              <table class="table">
                                  <tbody>
                                   <?php if($offer){ 
									  foreach($offer as $offervalue){
										  $candidates = Candidates::model()->findByPk($offervalue['candidate_id']);
                                          $loaction = Location::model()->findByPk($offervalue['approver_manager_location']);
									  ?>
                                    <tr>
                                      <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/view',array('id'=>$offervalue['id'])); ?>" class="underline"><?php echo $candidates->first_name.' '.$candidates->last_name; ?></a>
                                        <br>
                                          <span>Bill Rate: <?php echo $offervalue['offer_bill_rate']; ?> / Location of Work: <?php echo $loaction->name; ?></span></td>
                                    </tr>
									<?php } }else{ 
										if($offer){
									  foreach($offer as $canoffervalue){
										  $offer = Offer::model()->findByAttributes(array('candidate_id'=>$canoffervalue['id']));
                                          $loaction = Location::model()->findByPk($offer->approver_manager_location);
									  ?>
                                    <tr>
                                      <td><a href="" class="underline"><?php echo $canoffervalue->first_name.' '.$canoffervalue->last_name; ?></a>
                                        <br>
                                        <span>Bill Rate: <?php echo $offer->offer_bill_rate; ?> / Location of Work: <?php echo $loaction->name; ?></span></td>
                                    </tr>
									<?php } } } ?>
                                  </tbody>
                                </table>
                            </div>
                            

                          </div>

                            



                        </div>
                        <!-- col -->

                     </div>
                     <!-- row -->
                    <!--<div class="clearfix" style="padding: 10px 20px 10px; ">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                           <ul class="pagination m-t-0">
                              <li><a href="#">&laquo;</a></li>
                              <li><a href="#">1</a></li>
                              <li><a href="#">2</a></li>
                              <li><a href="#">3</a></li>
                              <li><a href="#">4</a></li>
                              <li><a href="#">5</a></li>
                              <li><a href="#">&raquo;</a></li>
                           </ul>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                           <p class="text-right">
                              Showing 10 to 20 of 50 entries
                           </p>
                        </div>
                     </div>-->


                     <div class="seprater-bottom-100"></div>


                  </div>