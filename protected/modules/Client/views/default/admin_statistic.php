<?php $this->pageTitle =  'Invoice Listing'; ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

<div class="cleafix " style="padding: 30px 20px; ">   

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
<h4 class="m-b-10">Invoice Listing</h4> 
<?php if(Yii::app()->user->hasFlash('success')) { ?>
<div class="alert alert-success">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo Yii::app()->user->getFlash('success');?>
</div>      
<?php }else if(Yii::app()->user->hasFlash('error')) {
?>

<div class="alert alert-danger" >
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo Yii::app()->user->getFlash('error');?>
</div>

<?php };?>



<div class="search-box">
<div class="two-fields">
 <form action="" method="post">
<div class="form-group">
<label for=""></label>
 <input type="text" class="form-control" name="s" placeholder="Search Invoice" value="<?php if(!empty($_POST['s'])){ echo$_POST['s']; }?>">
</div>

<div class="form-group">
<label for="">&nbsp;</label>
<button type="submit" class="btn btn-primary btn-block">Search</button>
</div>
</form>
</div> <!-- two-flieds -->
</div>
<p class="m-b-20">Invoice by Month</p>

<table class="table m-b-40 without-border">
    <thead class="thead-default">
      <tr>
        <th>Month</th>
        <th>Contract Staff</th>
        <th>Approved Time Sheet</th>
        <th>Pending</th>
        <th>Hours</th>
        <th>Total Invoice</th>
        <th class="text-center">Action</th>
      </tr>
    </thead>
    <tbody> 
    <?php foreach($invoiceArr as $key=>$value){?>
      <tr>
        <td>
           <?php echo $key;?>
        </td>
        <td>
          <?php
            echo $value['tContract']
           ?>
        </td>
        <td>
            <?php
              echo $value['tTimesheet'];
           ?>
        </td>
        <td>
          <?php
              echo $value['tPendingTimesheet'];
           ?>
        </td>
        <td>
           <?php echo $value['totalHours'];?>
        </td>
        <td>
          <?php echo "$".$value['totalBilrateWithTax'];?>
        </td>
        <td class="text-center">
          <a href="<?php echo $this->createUrl('default/admin',array('month'=>$key));?>" data-toggle="tooltip" data-placement="top" title="View" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
        </td>

      </tr>
      <?php } ?>
      <!-- <?php foreach($modelList as $value){?>
      <tr>
        <td>
           <?php echo $value->client_month;?>
        </td>
        <td>
          <?php
              $sql = "select count(contract_id) as t_contract,count(*) as t_timesheet from cp_timesheet tsheet where timesheet_status=1 and invoice_start_date ='".$value->invoice_start_date."' and invoice_end_date='".$value->invoice_end_date."'";
               $countReader = Yii::app()->db->createCommand($sql)->queryRow();
               echo $countReader['t_contract'];
           ?>
        </td>
        <td>
            <?php
              echo $countReader['t_timesheet'];
           ?>
        </td>
        <td>
          <?php
              $sql = "select count(*) as pending_timesheet from cp_timesheet tsheet where timesheet_status=0 and invoice_start_date ='".$value->invoice_start_date."' and invoice_end_date='".$value->invoice_end_date."'";
               $countReader = Yii::app()->db->createCommand($sql)->queryRow();
               echo $countReader['pending_timesheet'];
           ?>
        </td>
        <td>
           <?php echo $value->total_hours;?>
        </td>
        <td>
          <?php echo $value->total_bilrate_with_tax;?>
        </td>
        <td class="text-center">
          <a href="invoice-view.html" data-toggle="tooltip" data-placement="top" title="View" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/themeassets/assets/images/svg/view@512px-grey.svg"></i></a>
        </td>

        
      </tr> 
      <?php } ?>-->

    </tbody>
  </table>
  </div></div></div>
