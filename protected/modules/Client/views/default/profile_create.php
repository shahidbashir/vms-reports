<div class="page-head">

          <h2 class="page-head-title">Step 1 - Business Information</h2>

          <ol class="breadcrumb page-head-nav">

            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>">Dashbord</a></li>

            

            <li class="active">Step 1 - Business Information</li>

          </ol>

        </div>

        <div class="main-content">

        

         

            <!--Condensed Table-->

           

           



              <!--Hover table-->

            

           <div class="row">

            <div class="col-md-12">

              <div class="panel panel-border-color panel-border-color-primary">

                

                <div class="panel-body">

                    

                  <div class="white-box">

                     

                     <div class="row m-0">

                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">

                           <div class="business-setup">

                              <div class="form-wraper">

                                 <br>

                  <?php $form=$this->beginWidget('CActiveForm', array(

						'id'=>'client-form',

						'enableAjaxValidation'=>false,

						'htmlOptions' => array('enctype' => 'multipart/form-data'),

					)); ?>

                    <?php if($form->errorSummary($model)) { ?>

                        <div class="alert alert-danger">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <strong>Error!</strong> There is error submitting form. 

                        </div>

                    <?php } ?>

                    <div class="row">

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="">Your Business Name</label>

                          <?php echo $form->textField($model,'business_name',array('value'=>$model->organization,'class'=>'form-control', "required"=>"required" )); ?>

                     	  <?php echo $form->error($model,'business_name'); ?>

                        </div>

                      </div>

                      <!-- col-12 -->

                      

                      <div class="col-xs-12 col-sm-6">

                        <label for="" class="">Your Full Name</label>

                        <div class="two-flieds">

                        

                        

                          <div class="form-group">

                            <?php echo $form->textField($model,'first_name',array('class'=>'form-control', "required"=>"required" ,'placeholder'=>'First Name')); ?>

							<?php echo $form->error($model,'first_name'); ?>

                          </div>

                          <div class="form-group">

                          	<?php echo $form->textField($model,'last_name',array('class'=>'form-control', "required"=>"required",'placeholder'=>'Last Name' )); ?>

                        	<?php echo $form->error($model,'last_name'); ?>

                          </div>

                        </div>

                      </div>

                      <!-- col-12 --> 

                      

                    </div>

                    <!-- row -->

                    

                    <div class="row">

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="">Your Business Website URL</label>

                          <?php echo $form->textField($model,'website_url',array('value'=>'http://www.','class'=>'form-control')); ?>

                     	  <?php echo $form->error($model,'website_url'); ?>

                        </div>

                      </div>

                      <!-- col-12 -->

                      

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="" class="">Your Business Category</label>

                          <div class="single">

                            	<?php 

                                $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=6')),'title', 'title');

                             	echo $form->dropDownList($model, 'business_type', $list , array('class'=>'ui fluid search dropdown selection form-control','empty' => '')); ?>

                              

                          </div>

                        </div>

                      </div>

                      <!-- col-12 --> 

                      

                    </div>

                    <!-- row -->

                    

                    <div class="row">

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="">Total Number of Staff Members</label>

                          <div class="single">

                          <?php 

                            $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=7')),'title', 'title');

                            echo $form->dropDownList($model, 'total_staff', $list , array('class'=>'ui fluid search dropdown selection form-control', "required"=>"required" ,'empty' => '')); ?>

                          </div>

                        </div>

                      </div>

                      <!-- col-12 -->

                      

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="" class="">Revenue Range ( Last Year )</label>

                          <div class="single">

                          	<?php 

                            $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=8')),'title', 'title');

                            echo $form->dropDownList($model, 'revenue_range',$list, array('class'=>'ui fluid search dropdown selection form-control','empty' => '')); ?>

                          </div>

                        </div>

                      </div>

                      <!-- col-12 --> 

                      

                    </div>

                    <!-- row -->

                    

                    <div class="row">

                      <div class="col-xs-12 col-sm-12">

                        <div class="form-group">

                          <label for="" class="">Describe your Business</label>

                          <?php echo $form->textArea($model,'describe_business',array("class"=>"form-control", "rows"=>"6", "required"=>"required" )); ?>

                    	  <?php echo $form->error($model,'describe_business'); ?>

                          <!--<textarea name="" id="input" class="form-control" rows="6" required></textarea>-->

                        </div>

                      </div>

                      <!-- col-12 --> 

                      

                    </div>

                    <!-- row -->

                    

                    <div class="row">

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="">Linked Profile</label>

                          <?php echo $form->textField($model,'linkedin_profile',array("class"=>"form-control")); ?>

                    	  <?php //echo $form->error($model,'linkedin_profile'); ?>

                          <!--<input type="text" class="form-control" id="" placeholder="">-->

                        </div>

                      </div>

                      <!-- col-12 -->

                      

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="" class="">Twitter Profile</label>

                          <?php echo $form->textField($model,'twitter_profile',array("class"=>"form-control")); ?>

                    	  <?php echo $form->error($model,'twitter_profile'); ?>

                          <!--<input type="text" class="form-control" id="" placeholder="">-->

                        </div>

                      </div>

                      <!-- col-12 --> 

                      

                    </div>

                    <!-- row --> 

                    

                    <div class="row">

                     <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                        <div class="form-group">

                           <label for="" class="">Phone Number</label>

                           <?php echo $form->textField($model,'phone',array("class"=>"form-control")); ?>

                    	  <?php echo $form->error($model,'phone'); ?>

                        </div>

                     </div> <!-- col -->



                     <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                        <div class="form-group">

                           <label for="" class="">Mobile Number</label>

                           <?php echo $form->textField($model,'mobile',array("class"=>"form-control")); ?>

                    	  <?php echo $form->error($model,'mobile'); ?>

                        </div>

                     </div> <!-- col -->



 <div class="form-group profile-input">

 <label for="" class="">Profile Image</label>

 <div class="input-group">

      <input type="text" class="form-control" readonly>

            <label class="input-group-btn">

                <span class="btn btn-success" style="padding-top: 10px;">

                    Browse&hellip; <?php echo $form->fileField($model, 'profile_image',array('style'=>'display:none;')); ?>

                </span>

            </label>

            

    </div>

</div>


                  </div> <!-- row -->

                    

                    <br>

                    <br>

                    <button type="submit" class="btn btn-success">SAVE &amp; CONTINUE</button>

                  <?php $this->endWidget(); ?>

                  <br>

                  <br>

                </div>

              </div>

              <!-- business-setup --> 

            </div>

            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2"> <br>

              <br>

              <br>

              <div class="hep-text-block"> </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

  <!-- container --> 

  

  <br>

  <br>

  <br>

  <?php $this->renderPartial('footer'); ?>

</div>



<!-- page-wrapper -->





