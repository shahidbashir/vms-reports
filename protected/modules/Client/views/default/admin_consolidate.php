<?php  if(Yii::app()->controller->action->id=='approvedInvoice'){ $this->pageTitle = 'Approved Invoice'; }else{ $this->pageTitle = 'Pending Invoice'; } ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

<div class="cleafix " style="padding: 30px 20px; ">

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
   <h4 class="m-b-10">Consolidate Invoice</h4>
  <!-- <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>-->
    <!--<div class="m-b-30">
      <a data-toggle="modal" href='#select-month' class="btn btn-sm btn-default-2">Consolidate Invoice</a>
    </div>-->

     <div class="search-box">
     <div class="two-fields">
          <form action="" method="post">
         <div class="form-group">
           <label for=""></label>
           <input type="text" class="form-control" name="s" placeholder="Search Invoice By invoice number , amount " value="<?php if(!empty($_POST['s'])){ echo$_POST['s']; }?>">
         </div>

          <div class="form-group">
           <label for="">&nbsp;</label>
           <button type="submit" class="btn btn-primary btn-block">Search</button>
         </div>
         </form>

     </div> <!-- two-flieds -->
   </div>


    <table class="table m-b-40 without-border">
    <thead class="thead-default">
      <tr>

        <th>Status</th>
        <th>Invoice Number</th>
        <th>Duration</th>
        <th>Amount</th>
        <th>Hours</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
    foreach($consolidateModel as $value) {

    	$status = $value->status;
        $invoiceStatis = UtilityManager::invoiceConsolidate();
	    if($status==0){
	    	$class = "open";
	    }else if($status==1){
	        $class = "pending-aproval";
	    }else if($status==2){
	        $class = "new-request";
	    }
        $invoiceModel = GeneratedInvoice::model()->findByPk($value->generated_invoice_id);
	    $canidateModel= Candidates::model()->findByPk($invoiceModel->candidate_id);
	    $vendorModel  = Vendor::model()->findByPk($invoiceModel->vendor_id);
        $invoiceIDs = '"'. implode('","', explode(',', $value->generated_invoice_id)) .'"';


        $sql = "select sum(total_hours) as t_hours from vms_generated_invoice where id in(".$invoiceIDs.")";
        $invoiceReader = Yii::app()->db->createCommand($sql)->queryRow();
    	?>
      <tr>

        <td>
           <span class="tag label-<?php echo $class;?>"><?php echo $invoiceStatis[$status];?></span>
        </td>
        <td><a href=""><?php echo $value->serial_number;?></a></td>
        <td>
            <?php echo date('m/d/Y',strtotime($value->duration_start_date)).' to '.date('m/d/Y',strtotime($value->duration_end_date));?>
        </td>
        <td>
         <?php echo '$ '.$value->amount_due;?>
        </td>
        <td><?php echo $invoiceReader['t_hours'];?></td>
        <td>
          <a href="<?php echo $this->createUrl('consolidateView',array('id'=>$value->id));?>" data-toggle="tooltip" data-placement="top" title="View" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
        </td>


      </tr>
      <?php } ?>
    </tbody>
  </table>


</div>
<!-- col -->

</div></div>


<div class="modal fade" id="select-month">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Select Month</h4>
      </div>
      <form action="<?php echo $this->createUrl('generatedInvoice/adminconsolidate2');?>" method="get">
      <div class="modal-body">
      	<div class="form-group">
      <label>Select Client</label>
        <select name="client_id" id="client_id" class="form-control" required="required">
   			<option value="">Select</option>
    		<?php

    		$clientModel = Client::model()->findAll(array('condition'=>'super_client_id=0'));
    		 foreach($clientModel as $value) {?>
      		<option value="<?php echo $value->id;?>"><?php echo $value->business_name.' ('.$value->first_name.' '.$value->last_name.')';?></option>
    	<?php } ?>

  </select></div>

        <div class="form-group">
          <label for="">Select Month</label>
           <select name="client_month" id="client_month" class="form-control" required="required">
                <option value="">Select</option>
                 <?php  $currentMonth    =  date('m',strtotime('first day of January '));
                     for ($m=$currentMonth; $m<=12; $m++) {
                      $month_year = date('F-Y', mktime(0,0,0,$m, 1, date('Y')));
                    ?>
                         <option value="<?php echo $month_year; ?>"><?php echo $month_year; ?></option>
                         <?php } ?>
           </select>
        </div>

         <div class="form-group">
          <label for="">Select Invoice Cycle</label>
           <select name="cycle" id="cycle" class="form-control select2" required="required"><option value="">Select</option>
            </select>
        </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Continue</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

      </div>
        </form>
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#client_id").change(function(event){
      event.preventDefault();

        var client_id = $("#client_id").val();
        var client_month = $("#client_month").val();
        $("#location_cont").show();

        $.ajax({
         type: "POST",
         dataType:"json",
         url: "<?php echo Yii::app()->createUrl('invoiceClientVend/clientLocation'); ?>",
         data: { client_id: client_id,client_month:client_month},
          beforeSend: function() { }
         })
        .done(function(results) {
          console.log(results);
            $('#load_location').html(results.option);
        });

    });

    $("#client_month").change(function(event){
      event.preventDefault();

        var client_id = $("#client_id").val();
        var client_month = $("#client_month").val();
        $.ajax({
         type: "POST",
         dataType:"json",
         url: "<?php echo Yii::app()->createUrl('invoiceClientVend/clientLoadCycle'); ?>",
         data: { client_id: client_id,client_month:client_month},
          beforeSend: function() { }
         })
        .done(function(results) {
          console.log(results);
            $('#cycle').html(results.cycle);
        });

    });




 });

</script>
