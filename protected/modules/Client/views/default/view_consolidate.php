  <?php $this->pageTitle =  'Consolidate Invoice';?>
 <div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                     
                     <div class="cleafix " style="padding: 30px 20px; ">   
                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                           <h4 class="m-b-10">Consolidate Invoice
                           <a href="<?php echo $this->createUrl('adminconsolidate');?>" class="btn btn-default pull-right">Back</a></h4> 

                           <p class="m-b-40"><!--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.--></p>
                           
                            

                        <div class="card">
                            <div class="card-block">
                              <div class="p-b-2 clearfix">
                                <div class="pull-right text-xs-right">
                                  <h5 class="bold m-b-0">
                                    Invoice #<?php echo $consolidateModel->id; ?>
                                  </h5>
                                  <p class="m-b-0">
                                    Issued on <?php echo date("m/d/Y",strtotime($consolidateModel->date_created)); ?>
                                  </p>
                                  <!--<p class="m-b-0">
                                    Payment due by ...
                                  </p>-->
                                </div>
                                
                                <div class="overflow-hidden">
                                  <p class="m-b-0">
                                    <?php echo $clientModel->email?>
                                  </p>
                                  <p class="m-b-0">
                                    <?php echo $clientModel->phone?>
                                  </p>
                                </div>
                              </div>
                              <div class="p-t-2 p-b-2 clearfix">
                                
                                <div class="overflow-hidden">
                                  <p class="m-b-0">
                                    <strong>
                                      Client Details
                                    </strong>
                                  </p>
                                  <p class="m-b-0">
                                    <?php echo $clientModel->first_name." ".$clientModel->last_name?>
                                  </p>
                                  <p class="m-b-0">
                                    <?php echo $clientModel->business_name?>
                                  </p>
                                </div>
                              </div>
                              <div class="table-responsive p-t-2 p-b-2">
                                <table class="table table-bordered m-b-0">
                                  <thead>
                                  <tr>
                                    <th style="width: 6%">
                                      #
                                    </th>
                                    <th style="width: 17%">
                                      Candidate Name
                                    </th>
                                    <th style="width: 6%">
                                      Hours
                                    </th>
                                    <th style="width: 17%">
                                      Vendor
                                    </th>

                                    <th style="width: 14%">
                                      Price ( Without Tax )
                                    </th>
                                    <th style="width: 12%">
                                      Tax Amount
                                    </th>
                                    <th style="width: 9%">
                                      MSP Fees
                                    </th>

                                    <th style="width: 5%">
                                      Discount
                                    </th>
                                    <th style="width: 10%">
                                      Amount.<br />(After Tax)
                                    </th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <?php
                                  if($consolidateModel){
                                    $generatedIDs = $consolidateModel->generated_invoice_id;
                                  }else{
                                    $generatedIDs = $consolidateData['generated_invoice_id'];
                                  }
                                  $invoiceIDs = explode(",", $generatedIDs);
                                  $total = 0;
                                  $totalTaxAmount = 0;
                                  $totalMSPFees = $totalDiscount=$totalHours = $totalIndiPrice = $totalMSPFeesPercent=$totalDiscountPercent = 0;
                                  if($invoiceIDs)
                                    foreach($invoiceIDs as $value) {
                                      $sql = "select invoice.*,can.first_name,can.middle_name,can.last_name from vms_generated_invoice invoice,vms_candidates as can where invoice.candidate_id=can.id and invoice.id=".$value;
                                      $invoiceReader = Yii::app()->db->createCommand($sql)->queryRow();

                                      $vendorModel = Vendor::model()->findByPk($invoiceReader['vendor_id']);

                                      ?>
                                      <tr>
                                        <td>
                                          <a href="<?php echo $this->createUrl('view',array('id'=>$value));?>" style="color: green;"><?php echo $value;?></a>
                                        </td>
                                        <td>
                                          <?php echo $invoiceReader['first_name']." ".$invoiceReader['middle_name']." ".$invoiceReader['last_name'];?>
                                        </td>
                                        <td>
                                          <?php echo $invoiceReader['total_hours'];
                                          $totalHours = $totalHours+$invoiceReader['total_hours'];?>
                                        </td>
                                        <td>
                                          <?php echo $vendorModel->organization;?>
                                        </td>
                                        <td>
                                          $<?php echo round($invoiceReader['total_billrate'],2);
                                          $totalIndiPrice = $totalIndiPrice+round($invoiceReader['total_billrate'],2);
                                          ?>
                                        </td>
                                        <td>
                                          $<?php echo round($invoiceReader['total_bilrate_with_tax']-$invoiceReader['total_billrate'],2);
                                          $totalTaxAmount=  $totalTaxAmount+round($invoiceReader['total_bilrate_with_tax']-$invoiceReader['total_billrate'],2);
                                          ?>
                                        </td>
                                        <td>
                                          <?php $mspFees = 0;
                                          $toApply = $invoiceReader['total_billrate'];
                                          if(isset(Yii::app()->session['adminConsolidateMspFees'])) {
                                            $mspFeesArr = Yii::app()->session['adminConsolidateMspFees'];
                                            foreach($mspFeesArr as $taxValue) {
                                              echo $mspFeesCurrent = round($taxValue['value']/100*$toApply,2);
                                              // echo '('.$taxValue['value'].'%)';
                                              $mspFees = $mspFees+$mspFeesCurrent;
                                              $totalMSPFeesPercent=$taxValue->value;
                                            }
                                          }
                                          foreach($consolidatedTaxModel as $taxValue) {
                                            if($taxValue->type=="MSP Fees"){
                                              $mspFeesCurrent = round($taxValue->value/100*$toApply,2);
                                              echo '$'.$mspFeesCurrent;
                                              // echo '('.$taxValue->value.'%)';
                                              $mspFees = $mspFees+$mspFeesCurrent;
                                              $totalMSPFeesPercent=$taxValue->value;
                                            }
                                          }
                                          if($mspFees==0){
                                            echo "$".$mspFees;
                                          }
                                          $totalMSPFees = $totalMSPFees+$mspFees;
                                          ?>

                                        </td>
                                        <td>
                                          <?php $discount = 0;
                                          if(isset(Yii::app()->session['adminConsolidateDiscount'])) {
                                            $discountArr = Yii::app()->session['adminConsolidateDiscount'];
                                            foreach($discountArr as $taxValue) {
                                              $discountCurrent = round($taxValue['value']/100*$toApply,2);
                                              echo '$'.$discountCurrent;
                                              // echo '('.$taxValue['value'].'%)';
                                              $discount = $discount+$discountCurrent;
                                              $totalDiscountPercent=$taxValue['value'];
                                            }
                                          }
                                          foreach($consolidatedTaxModel as $taxValue) {
                                            if($taxValue->type=="Discount"){
                                              $discountCurrent = round($taxValue->value/100*$toApply,2);
                                              echo '$'.$discountCurrent;
                                              //  echo '('.$taxValue->value.'%)';
                                              $discount = $discount+$discountCurrent;
                                              $totalDiscountPercent=$taxValue['value'];
                                            }
                                          }
                                          if($discount==0){
                                            echo "$".$discount;
                                          }
                                          $totalDiscount = $totalDiscount+$discount;
                                          ?>
                                        </td>
                                        <td align="right">
                                          $<?php echo $invoiceReader['total_bilrate_with_tax']+$mspFees-$discount;?>
                                        </td>
                                      </tr>

                                      <?php $total = $total+$invoiceReader['total_bilrate_with_tax']+$mspFees-$discount;} ?>
                                  <tr>
                                    <td colspan="2"><strong>Total</strong></td>
                                    <td><strong><?php echo $totalHours;?> </strong></td>
                                    <td></td>
                                    <td><strong><?php echo "$".$totalIndiPrice;?></strong></td>
                                    <td><?php echo "$".$totalTaxAmount;?></td>
                                    <td><strong><?php echo "$".$totalMSPFees.'('.$totalMSPFeesPercent.'%)';?></strong></td>
                                    <td><strong><?php echo "$".$totalDiscount.'('.$totalDiscountPercent.'%)';?></strong></td>
                                    <td align="right"><strong><?php echo "$".$total;?></strong></td>
                                  </tr>
                                  </tbody>
                                </table><br> </div>
                              <div class="invoice-totals p-t-2 p-b-2">
                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Subtotal
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $<?php $sTotal = $total;echo round($sTotal,2);?>
                                  </span>
                                </div>
                                <?php
                                /*$mspFees = 0;
                                if(isset(Yii::app()->session['adminConsolidateMspFees'])) {
                                  $mspFeesArr = Yii::app()->session['adminConsolidateMspFees'];
                                 foreach($mspFeesArr as $taxValue) { ?>

                                  <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    MSP Fees
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $<?php
                                    echo $mspFeesCurrent = round($taxValue['value']/100*$sTotal,2)
                                    //echo $mspFees = round($mspFees+$taxValue['value']/100*$sTotal,2);?> (<?php echo $taxValue['value'];?>%)
                                    <?php  $mspFees = $mspFees+$mspFeesCurrent;?>
                                  </span>
                                </div>

                                <?php } }  foreach($consolidatedTaxModel as $taxValue) {
                                if($taxValue->type=="MSP Fees"){?>
                                  <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    MSP Fees
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $<?php echo $mspFeesCurrent = round($taxValue->value/100*$sTotal,2);
                                          ?> (<?php echo $taxValue->value;?>%)
                                          <?php echo  $mspFees = $mspFees+$mspFeesCurrent;?>

                                  </span>
                                </div>

                                <?php } } ?>
                                <?php
                                $discount = 0;
                                 if(isset(Yii::app()->session['adminConsolidateDiscount'])) {
                                  $discountArr = Yii::app()->session['adminConsolidateDiscount'];
                                 foreach($discountArr as $taxValue) {?>
                                  <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Discount
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $<?php echo $discountCurrent = round($taxValue['value']/100*$sTotal,2);?> (<?php echo $taxValue['value'];?>%)
                                    <?php  $discount = $discount+$discountCurrent;?>
                                  </span>
                                </div>

                                <?php } } foreach($consolidatedTaxModel as $taxValue) {
                                if($taxValue->type=="Discount"){?>
                                  <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Discount
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $<?php echo $discountCurrent = round($taxValue->value/100*$sTotal,2);?> (<?php echo $taxValue->value;?>%)
                                     <?php  $discount = $discount+$discountCurrent;?>
                                  </span>
                                </div>

                                <?php } } ?>

                                <?php */
                                $gTax = 0;

                                if(isset(Yii::app()->session['adminConsolidateGenralTax'])) {
                                  $gTaxArr = Yii::app()->session['adminConsolidateGenralTax'];
                                  foreach($gTaxArr as $taxValue) {?>
                                    <div class="invoice-totals-row">
                                      <strong class="invoice-totals-title">
                                        Genral Tax - <?php echo $taxValue->label;?>
                                      </strong>
                                  <span class="invoice-totals-value">
                                    $<?php echo $currentValue = $taxValue['value']/100*$sTotal;
                                    $gTax = $gTax+$currentValue;
                                    ?>
                                  </span>
                                    </div>

                                  <?php } }
                                foreach($consolidatedTaxModel as $taxValue) {
                                  if($taxValue->type=="Genral Tax"){?>
                                    <div class="invoice-totals-row">
                                      <strong class="invoice-totals-title">
                                        Genral Tax - <?php echo $taxValue->label;?>
                                      </strong>
                                  <span class="invoice-totals-value">
                                    $<?php echo $currentValue = $taxValue->value/100*$sTotal;
                                    $gTax = $gTax+$currentValue;
                                    ?>
                                  </span>
                                    </div>

                                  <?php } }
                                $cTax = 0;
                                if(isset(Yii::app()->session['adminConsolidateCustomizableTax'])) {
                                  $cTaxArr = Yii::app()->session['adminConsolidateCustomizableTax'];
                                  foreach($cTaxArr as $taxValue) { ?>
                                    <div class="invoice-totals-row">
                                      <strong class="invoice-totals-title">
                                        Customizable Tax - <?php echo $taxValue->label;?>
                                      </strong>
                                  <span class="invoice-totals-value">
                                    $<?php echo $currentValue = $taxValue['value']/100*$sTotal;
                                    $cTax = $cTax+$currentValue;
                                    ?>
                                  </span>
                                    </div>

                                  <?php } }  foreach($consolidatedTaxModel as $taxValue) {
                                  if($taxValue->type=="Customizable Tax"){?>
                                    <div class="invoice-totals-row">
                                      <strong class="invoice-totals-title">
                                        Customizable Tax - <?php echo $taxValue->label;?>
                                      </strong>
                                  <span class="invoice-totals-value">
                                    $<?php echo $currentValue = $taxValue->value/100*$sTotal;
                                    $cTax = $cTax+$currentValue;
                                    ?>
                                  </span>
                                    </div>

                                  <?php } } ?>
                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Total
                                  </strong>
                                  <span class="invoice-totals-value">
                                   $<?php //echo $dueAmount = round($total+$mspFees+$gTax+$cTax-$discount,2);
                                    echo $dueAmount = $total;?>
                                  </span>
                                </div>
                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Amount Paid
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $0.00
                                  </span>
                                </div>

                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    MSP Fees
                                  </strong>
                                  <span class="invoice-totals-value">
                                    <?php echo "$".$totalMSPFees.'('.$totalMSPFeesPercent.'%)';?>
                                  </span>
                                </div>

                                <div class="invoice-totals-row">
                                  <strong class="invoice-totals-title">
                                    Amount Due
                                  </strong>
                                  <span class="invoice-totals-value">
                                    $<?php echo $dueAmount;?>
                                  </span>
                                </div>
                                <?php if($consolidateModel){ ?>
                                  <a href="<?php echo $this->createUrl('pdfDownloadConsolidate',array('id'=>$consolidateModel->id));?>" class="btn btn-info pull-left" style="margin-right: 10px;">PDF Download</a>

                                  <a href="<?php echo $this->createUrl('pdfDownloadConsolidate',array('id'=>$consolidateModel->id,'summmary'=>'yes'));?>" class="btn btn-info pull-left" style="margin-right: 10px;">PDF Download With Summary</a>
                                <?php } ?>
                              </div>
                              <small class="p-t-2">
                                <strong>
                                  PAYMENT TERMS AND POLICIES
                                </strong>
                                All accounts are to be paid within 7 days from receipt of invoice. To be paid by cheque or credit card or direct payment online. If account is not paid within 7 days the credits details supplied as confirmation of work undertaken will be charged the agreed
                                quoted fee noted above. If the Invoice remails unpaid. our dept recovery agency, Urban, may charge you a fee of 25% of the unpaid portion of the
                                invoice amount and other legal and collection costs not covered by the fee.
                              </small>
                              <br>
                              <br>
                              <?php if($consolidateModel && $consolidateModel->status==1) {?>
                                <a data-toggle="modal" href='#approve-model' class="pull-left btn btn-sm btn-success">Approve</a>
                                <a data-toggle="modal" href='#reject-model' class="pull-left btn btn-sm btn-danger" style="margin-left: 5px;">Reject</a>
                                <div class="clearfix"></div> <br />
                              <?php }
                              if($consolidateModel && $consolidateModel->status==2){ ?>
                                <a data-toggle="modal" href='' disabled="disabled" class="pull-left btn btn-sm btn-success">Approved</a>
                              <?php }
                              if($consolidateModel && $consolidateModel->status==3){ ?>
                                <a data-toggle="modal" href='' disabled="disabled" class="pull-left btn btn-sm btn-danger">Rejected</a>
                              <?php }
                              ?>

                              </div>  <!-- well -->

                            </div> 

                            


                               
                           

                        </div>
                        <!-- col -->

                     </div>
                     <!-- row -->
                     <div class="seprater-bottom-100"></div>

            </div>

  <div class="modal fade" id="reject-model">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Consolidate Invoice Rejection</h4>
        </div>
        <form action="" method="post">
          <div class="modal-body">

            <div class="form-group">
              <?php //$cateSetting = Setting::model()->findAll(array('condition'=>'category_id=56'));
              $cateSetting = array('Consolidate Rejection 1'=>'Consolidate Rejection 1','Consolidate Rejection 2'=>'Consolidate Rejection 2');
              ?>

              <label for="">Reason of Rejection</label>
              <select name="reason_for_rejection" id="input" class="form-control" required>
                <option value="">Select</option>
                <?php foreach($cateSetting as $catValue){?>
                  <option value="<?php echo $catValue;?>" <?php //if( $catValue->id==$_POST['opt_out_vendor']) echo 'selected'; ?>>
                    <?php echo $catValue;?></option>
                <?php } ?>
              </select>
            </div>


            <div class="form-group">
              <label for="">Note</label>
              <textarea name="reject_notes" id="input" class="form-control" rows="3" required></textarea>
            </div>


          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          </div>
        </form>
      </div></div>
  </div>




  <div class="modal fade" id="approve-model">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Consolidate Invoice Approval</h4>
        </div>
        <form action="" method="post">
          <div class="modal-body">


            <div class="form-group">
              <label for="">Note</label>
              <textarea name="approved_notes" id="input" class="form-control" rows="3" required></textarea>
            </div>


          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          </div>
        </form>
      </div></div>
  </div>
      