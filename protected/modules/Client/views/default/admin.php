<?php $this->pageTitle =  'Invoice Listing'; ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

<div class="cleafix " style="padding: 30px 20px; ">   

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
<h4 class="m-b-20 clearfix">Invoice Listing
<a href="<?php echo $this->createUrl('default/adminStatistic');?>" class="btn btn-default-2 pull-right">Back to Invoice Listing</a></h4>
<p class="m-b-40 m-t- 10 clearfix"><?php echo $month;?>
<!--<a href="" class="btn btn-info pull-right">Send Reminder for timesheet approval</a>--></p>
<?php if(Yii::app()->user->hasFlash('success')) { ?>
<div class="alert alert-success">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo Yii::app()->user->getFlash('success');?>
</div>      
<?php }else if(Yii::app()->user->hasFlash('error')) {
?>

<div class="alert alert-danger" >
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo Yii::app()->user->getFlash('error');?>
</div>

<?php };?>

<!--<div class="m-b-30">
<a href="<?php echo $this->createUrl('invoiceClientVend/generate');?>" class="btn btn-sm btn-default-2">Add Invoice</a>
</div> -->

<div class="search-box">
<div class="two-fields">
 <form action="" method="post">
<div class="form-group">
<label for=""></label>
 <input type="text" class="form-control" name="s" placeholder="Search Invoice" value="<?php if(!empty($_POST['s'])){ echo$_POST['s']; }?>">
</div>

<div class="form-group">
<label for="">&nbsp;</label>
<button type="submit" class="btn btn-primary btn-block">Search</button>
</div>
</form>
</div> <!-- two-flieds -->
</div>

<div class="simplify-tabs"> 

<div role="tabpanel">
<!-- Nav tabs -->
<ul class="nav nav-tabs no-hover" role="tablist">
<li class="nav-item">
   <a class="nav-link "  href="#home" role="tab" data-toggle="tab"
      data-toggle="tooltip" data-placement="bottom" title="Overview">
      <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-overview@512px.svg"></i>
      Approved</a>
</li>
<li class="nav-item">
   <a class="nav-link "  href="#home2" role="tab" data-toggle="tab"
      data-toggle="tooltip" data-placement="bottom" title="Overview">
      <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-overview@512px.svg"></i>
      Pending Invoice</a>
</li>

<li class="nav-item active">
   <a class="nav-link"  href="#home3" role="tab" data-toggle="tab"
      data-toggle="tooltip" data-placement="bottom" title="Overview">
      <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-overview@512px.svg"></i>
      Invoice Generated</a>
</li>


</ul>

<div class="tab-content ">
 <div class="tab-pane" id="home"  role="tabpanel">
     <div class="row table-row-to-remove-margin"">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table class="table m-b-40 without-border">
              <thead class="thead-default">
                <tr>
                  <th>Status</th>
                  <th>Invoice Number</th>
                  <th>Invoice Duration</th>
                  <th>Candidate Name</th>
                  <th>Vendor Name</th>
                  <th>Hours</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody> 
              	<?php foreach($modelApproved as $value) {

              			$candidateModel = Candidates::model()->findByPk($value->candidate_id);
              			$vendorModel = Vendor::model()->findByPk($value->vendor_id);

              				$status = $value->status;
              				$invoiceStatis = UtilityManager::invoiceStatus();
              				if($status==0){
              					$class = "open";
              				}else if($status==1){
              					$class = "pending-aproval";
              				}else if($status==2){
              					$class = "new-request";
              				}
              		?>
                <tr>
                  <td>
                     <span class="tag label-<?php echo $class;?>"><?php echo $invoiceStatis[$status];?></span>
                  </td>
                  <td><a href=""><?php echo $value->serial_number;?></a></td>
                  <td><?php echo date('m/d/Y',strtotime($value->duration_start_date)).' to '.date('m/d/Y',strtotime($value->duration_end_date));?></td>
                  <td>
                     <?php echo $candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name;?>
                  </td>
                  <td>
                   <?php echo $vendorModel->organization;?>
                  </td>
                  <td><?php echo $value->total_hours;?> <i class="fa fa-question" 
                     data-toggle="popover" title="Hour Detail" data-content="Regular Hr: <?php echo $value->total_regular_hours;?>, Over Time: <?php echo $value->total_overtime_hours;?>, Double Time: <?php echo $value->total_doubletime_hours;?>"
                     ></i></td>
                  <td>
                    <a href="<?php echo $this->createUrl('default/view',array('id'=>$value->id));?>" data-toggle="tooltip" data-placement="top" title="View" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                  </td>

                  
                </tr>
                <?php } ?>

               
              </tbody>
            </table>
      </div>
     </div>
</div>

<div class="tab-pane" id="home2"  role="tabpanel">
     <div class="row table-row-to-remove-margin"">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table m-b-40 without-border">
              <thead class="thead-default">
                <tr>
                  <th>Status</th>
                  <th>Invoice Number</th>
                  <th>Invoice Duration</th>
                  <th>Candidate Name</th>
                  <th>Vendor Name</th>
                  <th>Hours</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody> 
              	<?php foreach($modelPending as $value) {

              			$candidateModel = Candidates::model()->findByPk($value->candidate_id);
              			$vendorModel = Vendor::model()->findByPk($value->vendor_id);

              				$status = $value->status;
              				$invoiceStatis = UtilityManager::invoiceStatus();
              				if($status==0){
              					//$class = "open";
                        $class = "pending-aproval";
              				}else if($status==1){
              					$class = "pending-aproval";
              				}else if($status==2){
              					$class = "new-request";
              				}
              		?>
                <tr>
                  <td>
                     <span class="tag label-<?php echo $class;?>"><?php echo $invoiceStatis[$status];?></span>
                  </td>
                  <td><a href=""><?php echo $value->serial_number;?></a></td>
                  <td><?php echo date('m/d/Y',strtotime($value->duration_start_date)).' to '.date('m/d/Y',strtotime($value->duration_end_date));?></td>
                  <td>
                     <?php echo $candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name;?>
                  </td>
                  <td>
                   <?php echo $vendorModel->organization;?>
                  </td>
                  <td><?php echo $value->total_hours;?> <i class="fa fa-question" 
                     data-toggle="popover" title="Hour Detail" data-content="Regular Hr: <?php echo $value->total_regular_hours;?>, Over Time: <?php echo $value->total_overtime_hours;?>, Double Time: <?php echo $value->total_doubletime_hours;?>"
                     ></i></td>
                  <td>
                    <a href="<?php echo $this->createUrl('default/view',array('id'=>$value->id));?>" data-toggle="tooltip" data-placement="top" title="View" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                  </td>

                  
                </tr>
                <?php } ?>

               
              </tbody>
            </table>

      </div>
     </div>
</div>

<div class="tab-pane active" id="home3"  role="tabpanel">
     <div class="row table-row-to-remove-margin"">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        	<table class="table m-b-40 without-border">
              <thead class="thead-default">
                <tr>
                  <th>Status</th>
                  <th>Invoice Number</th>
                  <th>Invoice Duration</th>
                  <th>Candidate Name</th>
                  <th>Vendor Name</th>
                  <th>Hours</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody> 
              	<?php foreach($modelConsolidated as $value) {

              			$candidateModel = Candidates::model()->findByPk($value->candidate_id);
              			$vendorModel = Vendor::model()->findByPk($value->vendor_id);

              				$status = $value->status;
              				$invoiceStatis = UtilityManager::invoiceStatus();
              				if($status==0){
              					$class = "open";
              				}else if($status==1){
              					$class = "pending-aproval";
              				}else if($status==2){
              					$class = "new-request";
              				}
              		?>
                <tr>
                  <td>
                     <span class="tag label-<?php echo $class;?>"><?php echo $invoiceStatis[$status];?></span>
                  </td>
                  <td><a href=""><?php echo $value->serial_number ;?></a></td>
                  <td><?php echo date('m/d/Y',strtotime($value->duration_start_date)).' to '.date('m/d/Y',strtotime($value->duration_end_date));?></td>
                  <td>
                     <?php echo $candidateModel->first_name." ".$candidateModel->middle_name." ".$candidateModel->last_name;?>
                  </td>
                  <td>
                   <?php echo $vendorModel->organization;?>
                  </td>
                  <td><?php echo $value->total_hours;?> <i class="fa fa-question" 
                     data-toggle="popover" title="Hour Detail" data-content="Regular Hr: <?php echo $value->total_regular_hours;?>, Over Time: <?php echo $value->total_overtime_hours;?>, Double Time: <?php echo $value->total_doubletime_hours;?>"
                     ></i></td>
                  <td>
                    <a href="<?php echo $this->createUrl('default/view',array('id'=>$value->id));?>" data-toggle="tooltip" data-placement="top" title="View" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                  </td>

                  
                </tr>
                <?php } ?>

               
              </tbody>
            </table>
      </div>
     </div>
</div>
</div></div></div></div></div></div>
