<?php $this->pageTitle = 'Dashboard'; ?>
<div class="page gray-bg">
  <div class="seprater-bottom-50"></div>
  <div class="card">
    <div class="row m-0">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="seprater-bottom-50"></div>
        <div class="thankyou">
          <h1>Thank You </h1>
          <div class="text-center">
            <p><span class="fa fa-check"></span></p>
          </div>
          <p class="message"> Thank you our team will review you profile and give approval on your account. </p>
          <div class="seprater-bottom-50"></div>
        </div>
      </div>
    </div>
    <!-- row --> 
  </div>
  <div class="seprater-bottom-50"></div>
</div>
