<?php $this->renderPartial('/settinggeneral/appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
	<div class="cleafix " style="padding: 30px 20px; ">
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
			<h4 class="m-b-10">Cost Center</h4>
			<p class="m-b-40"></p>

			<?php if(Yii::app()->user->hasFlash('success')):?>
				<?php echo Yii::app()->user->getFlash('success'); ?>
			<?php endif; ?>

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'cost-center-form',
				// Please note: When you enable ajax validation, make sure the corresponding
				// controller action is handling ajax validation correctly.
				// There is a call to performAjaxValidation() commented in generated controller code.
				// See class documentation of CActiveForm for details on this.
				'enableAjaxValidation'=>false,
			)); ?>
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="">Cost Center Code</label>
							<?php echo $form->textField($model,'cost_code',array('class'=>'form-control')); ?>
							<?php echo $form->error($model,'cost_code'); ?>
						</div>
					</div>
					<!-- col -->
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="">Department</label>
							<?php
							$list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=4 or client_id='.Yii::app()->user->id)),'title', 'title');
							echo $form->dropDownList($model, 'department', $list, array('class'=>'form-control','empty' => '','required'=>'required')); ?>
						</div>
					</div>
					<!-- col -->
				</div>
				<!-- row -->
				<br>
				<button type="submit" class="btn btn-success">Add</button>
				<a href="" class="btn btn-default">Cancel</a>
			<?php $this->endWidget(); ?>



		</div> <!-- col -->

		
	</div> <!-- row -->
	<?php if($modelCost) { ?>
	<div class="clearfix p-20 m-b-40">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<h4 class="m-b-30">List Cost Center</h4>
			<table class="table m-b-40 without-border">
				<thead class="thead-default">
				<tr>
					<th>S.No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cost Center Code</th>
					<th class="text-center">Action</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$i=0;
				foreach($modelCost as $value){
				$i++; ?>
				<tr>
					<td>
						<?php echo $i.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$value['cost_code'].'('.$value['department'].')'; ?>
					</td>
					<td style="text-align: center">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/costCenter/delete',array('id'=>$value['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
				<?php } ?>
				</tbody>
			</table>

		</div> <!-- col -->
	</div> <!-- row -->
	<?php } ?>
	<div class="seprater-bottom-100"></div>
</div>