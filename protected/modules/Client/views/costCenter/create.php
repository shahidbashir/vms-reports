<?php
/* @var $this CostCenterController */
/* @var $model CostCenter */

$this->breadcrumbs=array(
	'Cost Centers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CostCenter', 'url'=>array('index')),
	array('label'=>'Manage CostCenter', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model,'modelCost'=>$modelCost)); ?>