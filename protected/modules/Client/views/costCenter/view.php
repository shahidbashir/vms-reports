<?php
/* @var $this CostCenterController */
/* @var $model CostCenter */

$this->breadcrumbs=array(
	'Cost Centers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CostCenter', 'url'=>array('index')),
	array('label'=>'Create CostCenter', 'url'=>array('create')),
	array('label'=>'Update CostCenter', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CostCenter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CostCenter', 'url'=>array('admin')),
);
?>

<h1>View CostCenter #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'client_id',
		'cost_code',
		'department_id',
		'date_created',
	),
)); ?>
