<?php
/* @var $this CostCenterController */
/* @var $model CostCenter */

$this->breadcrumbs=array(
	'Cost Centers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CostCenter', 'url'=>array('index')),
	array('label'=>'Create CostCenter', 'url'=>array('create')),
	array('label'=>'View CostCenter', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CostCenter', 'url'=>array('admin')),
);
?>

<h1>Update CostCenter <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>