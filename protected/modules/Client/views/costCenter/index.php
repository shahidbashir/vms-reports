<?php
/* @var $this CostCenterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cost Centers',
);

$this->menu=array(
	array('label'=>'Create CostCenter', 'url'=>array('create')),
	array('label'=>'Manage CostCenter', 'url'=>array('admin')),
);
?>

<h1>Cost Centers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
