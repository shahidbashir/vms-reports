<?php $this->pageTitle = 'Token Field';
//require_once(Yii::app()->basePath.'\names.json');

 ?>

<?php
function select_options($selected = array()){
    $output = '';
    foreach(json_decode(file_get_contents(Yii::app()->basePath.'\names.json'), true) as $item){
        $output.= '<option value="' . $item['value'] . '"' . (in_array($item['value'], $selected) ? ' selected' : '') . '>' . $item['text'] . '</option>';
    }
    return $output;
}
?>


    <div class="container">

        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Tokenize2</a>
            </div>
        </nav>

        <div class="row">

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">Default usage</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-sample-demo1" multiple>
                            <?php echo select_options() ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">Remote data source</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-remote-demo1" multiple></select>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">Limit the number of tokens</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-limit-demo1" multiple>
                            <?php echo select_options() ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">One token behavior</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-limit-demo2" multiple>
                            <?php echo select_options() ?>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">Sortable tokens</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-sortable-demo1" multiple>
                            <?php echo select_options(array('CH', 'FR', 'IT', 'DE')) ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">Placeholder</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-ph-demo1" multiple>
                            <?php echo select_options() ?>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">Custom tokens allowed</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-custom-demo1" multiple>
                            <?php echo select_options() ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">Custom dataSource (callable)</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-callable-demo1" multiple></select>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="panel-title">Override dropdownItemFormat function</h2>
                    </div>
                    <div class="panel-body">
                        <select class="tokenize-override-demo1" multiple>
                            <?php echo select_options() ?>
                        </select>
                    </div>
                </div>
            </div>

        </div>

        <script>
            $('.tokenize-sample-demo1').tokenize2();
            $('.tokenize-remote-demo1').tokenize2({
                dataSource: "<?php echo Yii::app()->basePath.'/remote.php' ?>"
            });
            $('.tokenize-limit-demo1').tokenize2({
                tokensMaxItems: 5
            });
            $('.tokenize-limit-demo2').tokenize2({
                tokensMaxItems: 1
            });
            $('.tokenize-ph-demo1').tokenize2({
                placeholder: 'Please add new tokens'
            });
            $('.tokenize-sortable-demo1').tokenize2({
                sortable: true
            });
            $('.tokenize-custom-demo1').tokenize2({
                tokensAllowCustom: true
            });

            $('.tokenize-callable-demo1').tokenize2({
                dataSource: function(search, object){
                    $.ajax('remote.php', {
                        data: { search: search, start: 0 },
                        dataType: 'json',
                        success: function(data){
                            var $items = [];
                            $.each(data, function(k, v){
                                $items.push(v);
                            });
                            object.trigger('tokenize:dropdown:fill', [$items]);
                        }
                    });
                }
            });

            $('.tokenize-override-demo1').tokenize2();
            $.extend($('.tokenize-override-demo1').tokenize2(), {
                dropdownItemFormat: function(v){
                    return $('<a />').html(v.text + ' override').attr({
                        'data-value': v.value,
                        'data-text': v.text
                    })
                }
            });

            $('#btnClear').on('mousedown touchstart', function(e){
                e.preventDefault();
                $('.tokenize-demo1, .tokenize-demo2, .tokenize-demo3').trigger('tokenize:clear');
            });
        </script>
