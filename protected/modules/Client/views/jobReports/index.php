<?php $this->pageTitle = 'Jobs Report'; ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Jobs Report</h4>
      <p class="m-b-40"></p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-block">
              <div id="overall-jobreport" style="width: 500px; height: 400px;"></div>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-block">
              <div id="job-cat-report" style="width: 500px; height: 400px;"></div>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="card-block">
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <form name="search" method="post">
                  <div class="three-fields">
                    <div class="form-group">
                      <label for="">Select Date Range</label>
                      <input type="text" name="start_date" class="form-control singledate" id="start_date" placeholder="">
                    </div>
                    <div class="form-group">
                      <label for="">&nbsp;</label>
                      <input type="text" name="end_date" class="form-control singledate" id="end_date" placeholder="">
                    </div>
                    <div class="form-group">
                      <label for="">&nbsp;</label>
                      <p>
                        <button type="button" id="report" class="btn btn-success">Show Report</button>
                      </p>
                    </div>
                  </div>
                    </form>
                </div>
              </div>
              <div id="monthly-job-report" style="height: 400px;"></div>
            </div>
          </div>
        </div>
      </div>
      <?php /*?><br>
      <div class="card">
        <div class="card-header">
          <p>Job Request Report</p>
        </div>
        <div class="card-block">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Select Category</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="two-flieds-2">
                <div class="form-group">
                  <label for="">Select Date Range</label>
                  <input type="text" class="form-control singledate" id="" placeholder="">
                </div>
                <div class="form-group">
                  <label for="">&nbsp;</label>
                  <input type="text" class="form-control singledate" id="" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="">Show - Rate Card Range</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for=""></label>
                <p>
                  <button type="button" class="btn btn-success">Show Report</button>
                </p>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div id="job-request-report" style="width: 500px; height: 400px;"></div>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">
          <p>Location Based Job Report</p>
        </div>
        <div class="card-block">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Select Category</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for="">Location</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for=""></label>
                <p>
                  <button type="button" class="btn btn-success">Show Report</button>
                </p>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div id="job-location-report" style="width: 500px; height: 400px;"></div>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">
          <p>Job Hiring Manager Report</p>
        </div>
        <div class="card-block">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Select Category</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for="">Location</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for=""></label>
                <p>
                  <button type="button" class="btn btn-success">Show Report</button>
                </p>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div id="job-hiring-manager-report" style="width: 500px; height: 400px;"></div>
            </div>
          </div>
        </div>
      </div><?php */?>
        </br>
        <div class="card">


            <div class="card-block p-b-0">

                <div class="row list-of-categories">

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 left">
                        <p class="m-b-20 lg">List of Categories</p>
                        <?php $jobcategory = Setting::model()->findAllByAttributes(array('category_id'=>9));
                        $numberofCat = count($jobcategory);
                        ?>
                        <ul class="list-unstyled job-cat-nav">
                            <?php if($jobcategory){
                                foreach ($jobcategory as $cat){
                                    ?>
                                    <li><a class="catreport"  ><?php echo $cat->title; ?></a></li>
                                <?php } } ?>
                        </ul>
                    </div>

                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 right">
                        <div class="clearfix m-b-20">
                            <p class="pull-left lg">Total Nubmer of Jobs in Category: <?php echo $numberofCat; ?></p>
                            <p class="pull-right ">
                                <a href="">View Jobs</a>
                                <a href="">Download Report</a>
                            </p>
                        </div>


                        <div id="job-by-cat" style="width: 500px; height: 400px; margin-right: auto; margin-left: auto; "></div>


                    </div>
                </div>

            </div>




        </div>
        </br>
        <table class="table m-b-40 without-border ">
            <thead class="thead-default">
            <tr>
                <th>Job Category</th>
                <th class="text-center">Download Reports</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    All Jobs
                </td>
                <td style="text-align: center">
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobReports/xcel',array('type'=>'all')); ?>" class="m-r-15">All Job Reports </a>&nbsp&nbsp
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobReports/xcel',array('type'=>'approved')); ?>" class="m-r-15">Job Approved </a>&nbsp&nbsp
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobReports/xcel',array('type'=>'pending')); ?>"> Job Pending Approval</a>&nbsp&nbsp
                </td>
            </tr>
            <?php $category = Setting::model()->findAllByAttributes(array('category_id'=>9));
            if($category){
                foreach ($category as $catvalue){
            ?>
            <tr>

                <td>
                    <?php echo $catvalue->title; ?>
                </td>
                <td style="text-align: center">
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobReports/categoryXcel',array('type'=>'all','cat'=>$catvalue->title)); ?>" class="m-r-15">All Job Reports </a> &nbsp&nbsp
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobReports/categoryXcel',array('type'=>'approved','cat'=>$catvalue->title)); ?>" class="m-r-15">Job Approved </a> &nbsp&nbsp
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobReports/categoryXcel',array('type'=>'pending','cat'=>$catvalue->title)); ?>"> Job Pending Approval</a> &nbsp&nbsp
                </td>
            </tr>
            <?php } } ?>
            </tbody>
        </table>
    </div>

    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart-ui.css">
<script>

    anychart.onDocumentLoad(function() {
        // create an instance of a pie chart with data
        var headCountData = [


            ["Open", <?php echo $this->JobCountingOnStatus(3); ?>],
            ["Pending Approval", <?php echo $this->JobCountingOnStatus(1); ?>],
            ["Re-open", <?php echo $this->JobCountingOnStatus(6); ?>],
            ["Rejected", <?php echo $this->JobCountingOnStatus(5); ?>],
            ["Filled", <?php echo $this->JobCountingOnStatus(4); ?>],
            ["Hold", <?php echo $this->JobCountingOnStatus(10); ?>],
            ["New Request", <?php echo $this->JobCountingOnStatus(11); ?>],
            ["Draft", <?php echo $this->JobCountingOnStatus(2); ?>]
        ];

        var headCountChart = anychart.funnel( headCountData );
        headCountChart.title("Overall Job Report");
        // pass the container id, chart will be displayed there
        headCountChart.container("overall-jobreport");
        // call the chart draw() method to initiate chart display
        headCountChart.draw();


        // create an instance of a pie chart with data
        var hiringHeadCountData = [
		<?php echo $job_cat_reportData; ?>
        ];

        var hiringHeadCountChart = anychart.pie( hiringHeadCountData );
        hiringHeadCountChart.title("Job Category Reports");
        // pass the container id, chart will be displayed there
        hiringHeadCountChart.container("job-cat-report");
        // call the chart draw() method to initiate chart display
        hiringHeadCountChart.draw();

        var data = anychart.data.set([
            <?php echo $monthlyJobReport; ?>
        ]);

        // map data for the each series
        var Sales2003 = data.mapAs({x: [0], value: [1]});

        // chart type
        chart = anychart.column();

        // set title
        chart.title("Monthly Job Report");

        // set series data
        chart.column(Sales2003);

        // set axes titles
        var xAxis = chart.xAxis();
        xAxis.title("Status");
        var yAxis = chart.yAxis();
        yAxis.title("Jobs");

        // draw
        chart.container("monthly-job-report");
        chart.draw();





        // create an instance of a pie chart with data
        var jobRequestData = [

            ["Pending Approval", 5],
            ["Open", 2],
            ["Re-open", 4],
            ["Rejected", 2],
            ["Filled", 1],
            ["Hold", 10],
            ["New Request", 5],
            ["Draft", 5]
        ];

        var jobRequestChart = anychart.pie( jobRequestData );
        jobRequestChart.title("Job Request Report");
        // pass the container id, chart will be displayed there
        jobRequestChart.container("job-request-report");
        // call the chart draw() method to initiate chart display
        jobRequestChart.draw();


        // create an instance of a pie chart with data
        var locationBasedData = [

            ["Pending Approval", 5],
            ["Open", 2],
            ["Re-open", 4],
            ["Rejected", 2],
            ["Filled", 1],
            ["Hold", 10],
            ["New Request", 5],
            ["Draft", 5]
        ];

        var locatinBasedChart = anychart.pie( locationBasedData );
        locatinBasedChart.title("Job Request Report");
        // pass the container id, chart will be displayed there
        locatinBasedChart.container("job-location-report");
        // call the chart draw() method to initiate chart display
        locatinBasedChart.draw();


        // create an instance of a pie chart with data
        var jobHiringManagerData = [

            ["Pending Approval", 5],
            ["Open", 2],
            ["Re-open", 4],
            ["Rejected", 2],
            ["Filled", 1],
            ["Hold", 10],
            ["New Request", 5],
            ["Draft", 5]
        ];

        var jobHiringManagerChart = anychart.pie( jobHiringManagerData );
        jobHiringManagerChart.title("Job Request Report");
        // pass the container id, chart will be displayed there
        jobHiringManagerChart.container("job-hiring-manager-report");
        // call the chart draw() method to initiate chart display
        jobHiringManagerChart.draw();


        var jobsbycatData = [

            ["Pending Approval", <?php echo $this->JobCountingOnCategory('Architecture and Engineering',1); ?>],
            ["Open", <?php echo $this->JobCountingOnCategory('Architecture and Engineering',3); ?>],
            ["Re-open", <?php echo $this->JobCountingOnCategory('Architecture and Engineering',6); ?>],
            ["Rejected", <?php echo $this->JobCountingOnCategory('Architecture and Engineering',5); ?>],
            ["Filled", <?php echo $this->JobCountingOnCategory('Architecture and Engineering',4); ?>],
            ["Hold", <?php echo $this->JobCountingOnCategory('Architecture and Engineering',10); ?>],
            ["New Request", <?php echo $this->JobCountingOnCategory('Architecture and Engineering',11); ?>],
            ["Draft", <?php echo $this->JobCountingOnCategory('Architecture and Engineering',2); ?>]
        ];

        var jobsbycatChart = anychart.pie( jobsbycatData );
        jobsbycatChart.title("Job Category Request Report");
        // pass the container id, chart will be displayed there
        jobsbycatChart.container("job-by-cat");
        // call the chart draw() method to initiate chart display
        jobsbycatChart.draw();


    });
    $( document ).ready(function() {
        $('#report').on('click', function () {
            var first_date = $("#start_date").val();
            var end_date = $("#end_date").val();
            if (first_date == '' || end_date == '') {
                alert('Please Select date Range');
                return false;
            }
            $.ajax({
                'url': '<?php echo $this->createUrl('jobReports/monthlyReports'); ?>',
                type: "POST",
                data: {first_date: first_date ,end_date: end_date},
                'success': function (html) {
                    $("#monthly-job-report").html(html);
                }
            });
        });



        $('.catreport').on('click', function (e) {
            e.preventDefault();

            var category = $(this).text();
            $.ajax({
                'url': '<?php echo $this->createUrl('jobReports/categoryjobreport'); ?>',
                type: "POST",
                data: {category: category},
                'success': function (html) {
                    $("#job-by-cat").html(html);
                }
            });


            return false;
        });
    });
</script> 
