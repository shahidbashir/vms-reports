<?php $this->pageTitle =  'Job Reports';?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Jobs Report</h4>
      <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>
      <div class="card">
        <div class="card-header">
          <p>Job Category Report</p>
        </div>
        <div class="card-block">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Select Category </label>
                <?php $jobCategory = Setting::model()->findAll(array('condition'=>'category_id=9')); ?>
                <select name="first_category" id="first_category" class="form-control select2" required="required">
                  <option value="">Select</option>
                  <?php foreach($jobCategory as $cat){ ?>
                  <option value="<?php echo $cat->title; ?>"><?php echo $cat->title; ?></option>
                  <?php } ?>
                </select>
                
                
              </div>
              <div class="form-group">
                <label for=""></label>
                <p>
                  <button type="button" id="first_report" class="btn btn-success">Show Report</button>
                </p>
              </div>
            </div>
            
            
           
            
            
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="chart pie" id="first_report_reponse" style="height:250px"></div>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">
          <p>Job Request Report</p>
        </div>
        <div class="card-block">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Select Category</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for="">Select Date Range</label>
                <input type="text" name="" id="input" class="form-control drp" value="" required="required" pattern="" title="">
              </div>
              <div class="form-group">
                <label for="">Show - Rate Card Range</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for=""></label>
                <p>
                  <button type="button" class="btn btn-success">Show Report</button>
                </p>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="chart pie2" style="height:250px"></div>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">
          <p>Job Template Report</p>
        </div>
        <div class="card-block">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Select Job template</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for="">Select Date Range</label>
                <input type="text" name="" id="input" class="form-control drp" value="" required="required" pattern="" title="">
              </div>
              <div class="form-group">
                <label for="">Show Rate</label>
                <select name="" id="input" class="form-control select2" required="required">
                  <option value="">Select</option>
                </select>
              </div>
              <div class="form-group">
                <label for=""></label>
                <p>
                  <button type="button" class="btn btn-success">Show Report</button>
                </p>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <div class="chart pie3" style="height:250px"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/flot/jquery.flot.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/flot/jquery.flot.resize.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/flot/jquery.flot.categories.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/flot/jquery.flot.stack.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/flot/jquery.flot.time.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/flot/jquery.flot.pie.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/flot-spline/js/jquery.flot.spline.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/scripts/charts/flot.js"></script>
<script>
 $( document ).ready(function() {
    $('#first_report').on('click',function(){
      var job_category = $("#first_category").val();
	  if(job_category == '' ){
		  alert('Please Select Job Category');
		  return false;
		  }
      $.ajax({
        'url':'<?php echo $this->createUrl('reports/jobFirstReport') ?>',
        type: "POST",
        data: { job_category: job_category},
        'success':function(html){
		  $("#first_report_reponse").html(html);
        }
      });
    });
	
	 
	 //first chart
	 var browserDataFirst = [{
		label: 'Pending Approval',
		data: <?php echo $this->JobCountingOnStatus(1); ?>,
		color: '#ff5a5f'
	  }, {
		label: 'Open',
		data: <?php echo $this->JobCountingOnStatus(3); ?>,
		color: '#ffb400'
	  }, {
		label: 'Re-open',
		data: <?php echo $this->JobCountingOnStatus(6); ?>,
		color: '#007a87'
	  }, {
		label: 'Rejected',
		data: <?php echo $this->JobCountingOnStatus(5); ?>,
		color: '#6c6441'
	  },{
		label: 'Filled ',
		data: <?php echo $this->JobCountingOnStatus(4); ?>,
		color: '#b4a76c'
	  },
	  {
		label: 'Hold',
		data: <?php echo $this->JobCountingOnStatus(10); ?>,
		color: '#7b0051'
	  },
	  {
		label: 'New Request',
		data: <?php echo $this->JobCountingOnStatus(11); ?>,
		color: '#8ce071'
	  },
	   {
		label: 'Draft',
		data: <?php echo $this->JobCountingOnStatus(2); ?>,
		color: '#9F00FF'
	  }];
	  
	  $.plot($('.pie'), browserDataFirst, {
		series: {
		  pie: {
			show: true,
			innerRadius: 0.5,
			stroke: {
			  width: 0
			},
			label: {
			  show: true
			}
		  }
		},
		legend: {
		  show: true
		}
	  });
	
	
 });
</script> 
