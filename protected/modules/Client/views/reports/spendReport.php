<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart-ui.css">
<script>

    anychart.onDocumentLoad(function() {
        // create column chart
        headchartnumberofhired = anychart.column();

        // turn on chart animation
        headchartnumberofhired.animation(true);

        // set headchartnumberofhired title text settings
        headchartnumberofhired.title('Top Hiring by Category');

        // create area series with passed data
        var series = headchartnumberofhired.column([
			<?php echo $totalBill; ?>
		]);

        // set series tooltip settings
        series.tooltip().titleFormatter(function() {
            return this.x
        });

        series.tooltip().textFormatter(function() {
            return '$' + parseInt(this.value).toLocaleString()
        });
        series.tooltip().position('top').anchor('bottom').offsetX(0).offsetY(5);

        // set scale minimum
        headchartnumberofhired.yScale().minimum(0);

        // set yAxis labels formatter
        headchartnumberofhired.yAxis().labels().textFormatter("${%Value}");

        // tooltips position and interactivity settings
        headchartnumberofhired.tooltip().positionMode('point');
        headchartnumberofhired.interactivity().hoverMode('byX');

        // axes titles
        headchartnumberofhired.xAxis().title('Months');
        headchartnumberofhired.yAxis().title('Total Spent');

        // set container id for the headchartnumberofhired
        headchartnumberofhired.container('containernumberofhire');

        // initiate headchartnumberofhired drawing
        headchartnumberofhired.draw();


    });

</script>
<?php $this->pageTitle =  'Spend Report'; //echo '<pre>'; print_r($tableData); exit; ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Spend Report</h4>
      <p class="m-b-40"></p>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="card-block">
              <div id="containernumberofhire" style="width: 100%; height: 400px;"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <table class="table m-b-40 without-border">
            <thead class="thead-default">
              <tr>
                <th style="width: 20px; ">S.No</th>
                <th>Month</th>
                <th>Number of Employee</th>
                <th>Invoice For Employee</th>
                <th>Regular Hour</th>
                <th>Over Time Hour</th>
                <th>Double Time Hour</th>
                <th>Total Spend</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($tableData['month'] as $key=>$value){
				if($key < date('m')){
				  ?>
              <tr>
                <td><?php echo $key+1; ?></td>
                <td><?php echo $value; ?></td>
                <td><?php echo $tableData['no_of_emp'][$key]; ?></td>
                <td><?php echo $tableData['total_invoices'][$key]; ?></td>
                <td><?php echo $tableData['total_regular_hours'][$key]; ?></td>
                <td><?php echo $tableData['total_overtime_hours'][$key]; ?></td>
                <td><?php echo $tableData['total_doubletime_hours'][$key]; ?></td>
                <td>$<?php echo $tableData['total_bilrate_with_tax'][$key]; ?></td>
              </tr>
           <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
      <br>
      <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>
                        <th style="width: 20px; ">S.No</th>
                        <th>Category</th>
                        <th>Total Spend Amount. ( Overall ) </th>
                        <th>Month  (<?php echo date('F - Y'); ?>)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $loginUserId = UtilityManager::superClient(Yii::app()->user->id);
                    $now = new \DateTime('now');
                     $currentM = $now->format('m');
                    foreach($tableData2 as $key=>$value){
                        $query4 = "SELECT sum(vms_generated_invoice.total_bilrate_with_tax) as total_bilrate_with_tax FROM vms_generated_invoice INNER JOIN vms_job ON vms_generated_invoice.job_id = vms_job.id and vms_job.cat_id= '".$value['category']."' and DATE_FORMAT(vms_generated_invoice.invoice_start_date,'%m')='".$currentM."' WHERE client_id = $loginUserId  Group By vms_job.cat_id";
                        $record = Yii::app()->db->createCommand( $query4 )->query()->read();
                            ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo $value['category'];  ?></td>
                                <td><?php echo '$ '.$value['total_bilrate_with_tax']; ?></td>
                                <td><?php if(!empty($record ['total_bilrate_with_tax'])){ echo '$ '.$record ['total_bilrate_with_tax']; }else{ echo '$ 0'; } ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <br>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
