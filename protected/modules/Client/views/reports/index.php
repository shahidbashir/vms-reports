<?php $this->pageTitle =  'Reports';?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Job Reports </h4>
      <p class="m-b-30">A glance of job requisition under each job category.</p>
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
          <div class="simplify-tabs blue-tabs m-t-20">
            <div role="tabpanel">
              <div class="tab-content">
                <div class="tab-pane active m--31" id="rates"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <h4 class="h4-small p-l-15 m-b-20">Job Categories</h4>
                      <table class="table tab-table m-b-20">
                        <tbody>
                          <?php if($jobgategory){ 
									foreach($jobgategory as $categorydata){
										$number_of_jobs = Job::model()->countByAttributes(array('cat_id'=>$categorydata->title));
									?>
                          <tr>
                            <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/index',array('cat'=>$categorydata->title)); ?>" class="underline"><?php echo $categorydata->title; ?></a></td>
                            <td><?php  echo $number_of_jobs; ?></td>
                          </tr>
                          <?php } } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
          </div>
          <!-- simplify-tabs --> 
          
        </div>
        <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
          <div class="simplify-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <div class="tab-content p-0 ">
                <div class="tab-pane active" id="rejected"  role="tabpanel">
                  <div class="clearfix reports-header">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <h4 class="h4-small m-b-20 m-t-10">Job Category</h4>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                      <div class="two-fields">
                        <div class="form-group">
                          <input type="text" class="form-control" id="search-cate" placeholder="Search Jobs">
                        </div>
                        <div class="form-group">
                          <button type="button" class="btn btn-primary btn-block">Search</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <table class="table search-cat table-border-lr-0">
                    <tbody>
                      <?php if(isset($_GET['cat'])){ 
                                                          $job = Job::model()->findAllByAttributes(array('cat_id'=>$_GET['cat']));
																
																foreach($job as $jobsvalue){
																	$countHired = Workorder::model()->countByAttributes(array('job_id'=>$jobsvalue->id,'workorder_status'=>1,'on_board_status'=>1));
																	$now = time(); // or your date as well
																$your_date = strtotime($jobsvalue->desired_start_date);
																$datediff = $your_date - $now;

																$countDays = floor($datediff / (60 * 60 * 24)) + 1;
																	if($countDays >= 0){
																	$color = 'available-jobs';
															}else{
																 $color = 'running-out';
																 }
															?>
                      <tr>
                        <td><a href="" class="underline"><?php echo $jobsvalue->title; ?></a></td>
                        <td><?php echo $jobsvalue->num_openings; ?></td>
                        <td><?php echo $countHired; ?></td>
                        <td><span class="<?php echo $color; ?>"><?php echo $countDays.' days Left'; ?></span></td>
                      </tr>
                      <?php }
																}else{ 
															if($firstcategory){
																foreach($firstcategory as $categorydata){
															   $job = Job::model()->findAllByAttributes(array('cat_id'=>$categorydata->title));
																
																foreach($job as $jobsvalue){
																	$countHired = Workorder::model()->countByAttributes(array('job_id'=>$jobsvalue->id,'workorder_status'=>1,'on_board_status'=>1));
																	
																$now = time(); // or your date as well
																$your_date = strtotime($jobsvalue->desired_start_date);
																$datediff = $your_date - $now;

																$countDays = floor($datediff / (60 * 60 * 24)) + 1;
																	if($countDays >= 0){
																	$color = 'available-jobs';
															}else{
																 $color = 'running-out';
																 }
															?>
                      <tr>
                        <td><a href="" class="underline"><?php echo $jobsvalue->title; ?></a></td>
                        <td><?php echo $jobsvalue->num_openings; ?></td>
                        <td><?php echo $countHired; ?></td>
                        <td><span class="<?php echo $color; ?>"><?php echo $countDays.' days Left'; ?></span></td>
                      </tr>
                      <?php }  } } } ?>
                      <!--<tr>
                                                            <td><a href="" class="underline">ASP.Net Developer</a></td>

                                                            <td>54</td>
                                                            <td>52</td>

                                                            <td><span class="available-jobs"> 5 days Left</span></td>
                                                          </tr>

                                                          <tr>
                                                            <td>
                                                              <a href="" class="underline">ASP.Net Developer</a>
                                                            </td>

                                                            <td>54</td> 
                                                            <td>53</td>

                                                            <td><span class="running-out">-3 days Left</span></td>
                                                          </tr>-->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
            
          </div>
          <!-- simplify-tabs --> 
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
