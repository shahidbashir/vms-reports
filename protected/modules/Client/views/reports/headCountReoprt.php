<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart-ui.css">
<script>

    anychart.onDocumentLoad(function() {
        // create an instance of a pie chart with data
        var headCountData = [ <?php echo $string; ?>];

        var headCountChart = anychart.pie( headCountData );
        headCountChart.title("Overall Head Count");
        // pass the container id, chart will be displayed there
        headCountChart.container("container");
        // call the chart draw() method to initiate chart display
        headCountChart.draw();

        // create an instance of a pie chart with data
        var hiringHeadCountData = [ <?php echo $string1; ?>];

        var hiringHeadCountChart = anychart.pie( hiringHeadCountData );
        hiringHeadCountChart.title("Hiring Manager head count");
        // pass the container id, chart will be displayed there
        hiringHeadCountChart.container("container2");
        // call the chart draw() method to initiate chart display
        hiringHeadCountChart.draw();


        var data = anychart.data.set([
            <?php echo $monthlyReport; ?>
        ]);

        // map data for the each series
        var Sales2003 = data.mapAs({x: [0], value: [1]});
        var Sales2004 = data.mapAs({x: [0], value: [2]});

        // chart type
        chart = anychart.column();

        // set title
        chart.title("Monthly Head Count");

        // set series data
        chart.column(Sales2003);
        //chart.column(Sales2004);

        // set axes titles
        var xAxis = chart.xAxis();
        xAxis.title("Dates");
        var yAxis = chart.yAxis();
        yAxis.title("Hiring");

        // draw
        chart.container("monthly-head-count");
        chart.draw();
		
		
		
		//new chart for yearly
		var data1 = anychart.data.set([
            <?php echo $hiredTotal; ?>
        ]);

        // map data for the each series
        var Sales2003 = data1.mapAs({x: [0], value: [1]});
        var Sales2004 = data1.mapAs({x: [0], value: [2]});

        // chart type
        chart = anychart.column();

        // set title
        chart.title("Yearly Head Count");

        // set series data
        chart.column(Sales2003);
        //chart.column(Sales2004);

        // set axes titles
        var xAxis = chart.xAxis();
        xAxis.title("Months");
        var yAxis = chart.yAxis();
        yAxis.title("Hiring");

        // draw
        chart.container("monthly-head-count1");
        chart.draw();


    });

</script>
<?php $this->pageTitle =  'Head Count Reports'; ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Head Count Report</h4>
      <p class="m-b-40"></p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-block">
              <div id="container" style="width: 500px; height: 400px;"></div>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-block">
              <div id="container2" style="width: 500px; height: 400px;"></div>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="card-block">
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <form name="datesearch" method="post" action="">
                    <div class="three-fields">
                      <div class="form-group">
                        <label for="">Select Date Range</label>
                        <input type="text" name="first_date" class="form-control singledate" id="" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="">&nbsp;</label>
                        <input type="text" name="last_date" class="form-control singledate" id="" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="">&nbsp;</label>
                        <p>
                          <button type="submit" name="datesearch" class="btn btn-success">Show Report</button>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div id="monthly-head-count" style="height: 400px;"></div>
          	  <br />
            </div>
          </div>
        </div>
      </div>
      <br>
      
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="card-block">
              <div id="monthly-head-count1" style="height: 400px;"></div>
              </div>
            </div>
          </div>
      </div>
        <div class="row">
            <h4>Headcount by Location</h4>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>
                        <th style="width: 20px; ">S.No</th>
                        <th>Location Name</th>
                        <th>Number of Hires</th>
                        <!--<th>Download Report</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $loginUserId = UtilityManager::superClient(Yii::app()->user->id);
                    foreach($Location as $key=>$value){
                        $query4 = "SELECT count(vms_contract.id) as numberOfhired FROM vms_contract RIGHT JOIN vms_offer ON vms_contract.offer_id = vms_offer.id and vms_offer.approver_manager_location= '".$value->id."' WHERE vms_contract.client_id = $loginUserId";
                        $record = Yii::app()->db->createCommand( $query4 )->query()->read();
                        ?>
                        <tr>
                            <td><?php echo $key + 1; ?></td>
                            <td><?php echo $value->name; ?></td>
                            <td><?php echo $record['numberOfhired']; ?></td>
                            <!--<td >
                                <a href="<?php /*echo Yii::app()->createAbsoluteUrl('Client/reports/xcel/month/'.$value,array('type'=>'single','number'=>$monthHiredT[$key])); */?>" data-toggle="tooltip" data-placement="top" title="Download Report" >
                                    Download Report
                                </a>
                                /
                                <a href="<?php /*echo Yii::app()->createAbsoluteUrl('Client/reports/xcel/month/'.$value,array('type'=>'all')); */?>" data-toggle="tooltip" data-placement="top" title="Download All Report" >
                                    Download All Report
                                </a>
                            </td>-->
                        </tr>
                    <?php }  ?>
                    </tbody>
                </table>
            </div>
        </div>
        </br>
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table class="table m-b-40 without-border">
              <thead class="thead-default">
                <tr>
                  <th style="width: 20px; ">S.No</th>
                  <th>Month</th>
                  <th>Number of Hires</th>
                  <th>Download Report</th>
                </tr>
              </thead>
              <tbody>
              	<?php foreach($monthsT as $key=>$value){ if($key < date('m')){ ?>
                    <tr>
                      <td><?php echo $key + 1; ?></td>
                      <td><?php echo $monthYear[$key]; ?></td>
                      <td><?php echo $monthHiredT[$key]; ?></td>
                        <td >
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/xcel/month/'.$value,array('type'=>'single','number'=>$monthHiredT[$key])); ?>" data-toggle="tooltip" data-placement="top" title="Download Report" >
                                Download Report
                            </a>
                            /
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/xcel/month/'.$value,array('type'=>'all')); ?>" data-toggle="tooltip" data-placement="top" title="Download All Report" >
                               Download All Report
                            </a>
                        </td>
                    </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
	<br>
        <?php $contract = Contract::model()->countByAttributes(array('client_id'=>$loginUserId)); ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>
                        <th style="width: 20px; ">S.No</th>
                        <th>Total Head Count</th>
                        <th>Download Report</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td><?php echo $contract; ?></td>
                            <td >
                                <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/allXcel'); ?>" data-toggle="tooltip" data-placement="top" title="Download Report" >
                                    Download Report
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
