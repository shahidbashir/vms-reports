<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart.min.js"
        xmlns="http://www.w3.org/1999/html"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart-ui.css">

<script>
    $( document ).ready(function() {
        anychart.onDocumentLoad(function () {
            // create an instance of a pie chart with data
            var headCountData =
                [
                    <?php echo $monthlysubReport; ?>
                ];

            var headCountChart = anychart.funnel(headCountData);
            headCountChart.title("Overall Submission Status");
            // pass the container id, chart will be displayed there
            // set chart legend settings
            var legend = headCountChart.legend();
            legend.enabled(true);
            legend.position("center");
            legend.itemsLayout("horizontal");
            legend.align("top center");
            headCountChart.saveAsPdf();
            // set chart base width settings
            headCountChart.baseWidth("70%");
            // set the neck height
            headCountChart.neckHeight("0%");

            headCountChart.container("submission-rejrection-report");
            headCountChart.background('white');
            // call the chart draw() method to initiate chart display
            headCountChart.draw();


            // create an instance of a pie chart with data
            var hiringHeadCountData = [

                <?php echo $rejectionReport; ?>
            ];

            var hiringHeadCountChart = anychart.pie(hiringHeadCountData);
            hiringHeadCountChart.title("Submission Rejection Status");
            // pass the container id, chart will be displayed there
            hiringHeadCountChart.container("job-cat-report");
            // call the chart draw() method to initiate chart display
            hiringHeadCountChart.draw();

        });
        $(".go-btn").on('click', function () {
            var cat = $("#subcat").val();
            $.ajax({
                'url': '<?php echo $this->createUrl('Reports/monthlysubReports'); ?>',
                type: "POST",
                data: {cat: cat},
                'success': function (html) {
                    //alert(html);
                    var arr = html.split('@');
                    //alert(arr[0]);
                    //alert(arr[1]);
                    $("#submission-rejrection-report").html(arr[0]);
                    $("#job-cat-report").html(arr[1]);

                }
            });
        });
    });
</script>

<?php $this->pageTitle =  'Submission Report';?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

    <div class="cleafix " style="padding: 30px 20px; ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-b-10">Submission Report</h4>
            <p class="m-b-40"><!--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.--></p>



            <div class="card m-b-30">
                <div class="card-block">
                    <form name="category" action="">
                    <div class="row">

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                            <div class="form-group">
                                <label for="">Select Job Categegory</label>
                                <select name="subcat" id="subcat" class="form-control">
                                    <option value="">Select Category</option>
                                    <?php
                                    $jobCategory = Setting::model()->findAllByAttributes(array('category_id'=>9));
                                    foreach($jobCategory as $cat){ ?>
                                        <option value="<?php echo $cat->title; ?>"><?php echo $cat->title; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="">&nbsp;</label>
                            <p>
                                <button type="button" class="btn btn-success go-btn">Load Report</button>
                            </p>
                        </div>

                    </div>
                    </form>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                    <div class="card">
                        <div class="card-block">
                            <div id="submission-rejrection-report" style="width: 500px; height: 400px;"></div>
                        </div>
                    </div>

                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                    <div class="card">
                        <div class="card-block">
                            <div id="job-cat-report" style="width: 500px; height: 400px;"></div>
                        </div>
                    </div>



                </div>


            </div>

            <br>










        </div>
        <!-- col -->

    </div>
    <!-- row -->




    <div class="seprater-bottom-100"></div>

</div>