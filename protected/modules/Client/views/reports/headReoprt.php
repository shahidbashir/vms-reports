<?php $this->pageTitle =  'Head Reports';?>
                    <div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                     
                     <div class="cleafix " style="padding: 30px 20px; ">  
                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <h4 class="m-b-10">Job Reports

                        </h4> 
                           <p class="m-b-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>


                           

                           <div class="row">

                              <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                 
                                 <div class="simplify-tabs blue-tabs m-t-20"> 

                                    <div role="tabpanel">
                                      

                                        <div class="tab-content">
                                          

                                          <div class="tab-pane active m--31" id="rates"  role="tabpanel">
                                               <div class="row">
                                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <h4 class="h4-small p-l-15 m-b-20">Job Categories</h4>
                                                     <table class="table tab-table m-b-20">
                                                        <tbody>
                                                          
                                                         <?php if($jobgategory){ 
															foreach($jobgategory as $categorydata){
																$jobs = Job::model()->findAllByAttributes(array('cat_id'=>$categorydata->title));
															$jobid = array();
																if($jobs){
																foreach($jobs as $jobvalue){
																	$jobid[] = $jobvalue->id;
																}
																	}
																$contract = Contract::model()->countByAttributes(array('job_id'=>$jobid));
															?>
                                                          <tr>

                                                            <td>
                                                              <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/headReoprt',array('cat'=>$categorydata->title)); ?>" class="underline"><?php echo $categorydata->title; ?></a>
                                                            </td>
                                                            <td>
                                                             <?php  echo $contract; ?>
                                                            </td>
                                                            
                                                          </tr>
  															<?php } } ?>
                                                          
                                                      </tbody>
                                                   </table>   
                                                  </div>
                                               </div>
                                          </div>
                                       </div>


                                     </div> <!-- tabpanel -->
                                 </div> <!-- simplify-tabs -->


                              </div>

                              <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
                                 
                                 <div class="simplify-tabs m-t-20"> 

                                    <div role="tabpanel">
                                       <!-- Nav tabs -->
                                        <div class="tab-content p-0 ">

                                           <div class="tab-pane active" id="rejected"  role="tabpanel">
                                               
                                                    <div class="clearfix reports-header">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                                                          <h4 class="h4-small m-b-20 m-t-10">Job Category</h4>

                                                        </div>
                                                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                          
                                                          <div class="two-fields">

                                                                 <div class="form-group">
                                                                  
                                                                   <input type="text" class="form-control" id="" placeholder="Search Jobs">
                                                                 </div>

                                                                  <div class="form-group">
                                                                   <button type="button" class="btn btn-primary btn-block">Search</button>
                                                                 </div>
                                                                 
                                                             </div>
                                                          
                                                        </div>
                                                      </div> 

                                                      <table class="table table-border-lr-0">
                                                       
                                                        <tbody>
                                                         <?php if(isset($_GET['cat'])) { 
															
                                                          $jobs = Job::model()->findAllByAttributes(array('cat_id'=>$_GET['cat']));
																$jobid = array();
																if($jobs){
																foreach($jobs as $jobvalue){
																	$jobid[] = $jobvalue->id;
																}
																	
																$contract = Contract::model()->findByAttributes(array('job_id'=>$jobid));
                                                                    $costcenter = '';
                                                                    if($contract){
																$workorder = Workorder::model()->findByPk($contract->workorder_id);

                                                                    $costcenter = CostCenter::model()->findAllByAttributes(array('id'=>explode(',',$workorder->wo_cost_center)));
                                                                 }
                                                                     ?>
                                                          <?php
                                                                    if($costcenter){
																$costdepartment = array();
																foreach($costcenter as $costvalue){
															  ?>
                                                          <tr>
                                                           
                                                            <td><a href="" class="underline"><?php echo $costvalue->department; ?></a></td>
                                                        

                                                            <td><?php echo $costvalue->cost_code; ?></td> 
                                                            <td><?php if($contract){ echo $contract->end_date; } ?></td>
                                                          </tr>
                                                          
                                                          
                                                          <?php } } } }else{
															if($firstcategory){ 
															
																$jobs = Job::model()->findAllByAttributes(array('cat_id'=>$firstcategory->title));
																$jobid = array();
																if($jobs){
																foreach($jobs as $jobvalue){
																	$jobid[] = $jobvalue->id;
																}
                                                                    $costcenter = '';
																$contract = Contract::model()->findByAttributes(array('job_id'=>$jobid));
                                                                    if($contract){
																$workorder = Workorder::model()->findByPk($contract->workorder_id);


																$costcenter = CostCenter::model()->findAllByAttributes(array('id'=>explode(',',$workorder->wo_cost_center)));
                                                                     }
                                                                        ?>
                                                          <?php

																$costdepartment = array();
                                                                    if($costcenter){
																foreach($costcenter as $costvalue){
															  ?>
                                                          <tr>
                                                           
                                                            <td><a href="" class="underline"><?php echo $costvalue->department; ?></a></td>
                                                        

                                                            <td><?php echo $costvalue->cost_code; ?></td> 
                                                            <td><?php if($contract){ echo $contract->end_date; } ?></td>
                                                          </tr>
                                                          
                                                          <?php } } } } } ?>
                                                        </tbody>
                                                      </table>

                                                   
                                          </div>
                                       </div>


                                     </div> <!-- tabpanel -->


                                 </div> <!-- simplify-tabs -->
                              </div>

                           </div>

                        </div>
                        <!-- col -->

                     </div> 
                     <!-- row -->
                     <div class="seprater-bottom-100"></div>

                  </div>