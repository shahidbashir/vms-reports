<?php
$reportGroup = UtilityManager::reportGroup();
$accessGroup = UtilityManager::accessGroup();
$runday = UtilityManager::day();
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart-ui.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/datatable/datatables.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/datatable/datatables.min.css" />
<script>

	anychart.onDocumentReady(function() {
	  // create pie chart with passed data
	  data = anychart.data.set([
		  <?php
		  foreach($headcountByCategory as $headcount){ ?>
				['<?php echo $headcount['job_category'] ?>',<?php echo $headcount['headcount'] ?>]
				<?php  if ($headcount !== end($headcountByCategory))
        					echo ',';?>
		  <?php } ?>
	  ]);


	  var wealth = data.mapAs({
	    x: [0],
	    value: [1]
	  });

	  chart = anychart.pie(wealth);
	  chart.labels()
	    .hAlign("center")
	    .position('outside');

	  // set chart title text settings
	  chart.title('Head Count by Category');

	  // set legend title text settings
	  chart.legend()
	    .title(false)
	    // set legend position and items layout
	    .position('bottom')
	    .itemsLayout('horizontal')
	    .align('center');

	  // set container id for the chart
	  chart.container('headcountByCategory');
	  // initiate chart drawing
	  chart.draw();
	});


	anychart.onDocumentReady(function() {
		  // create pie chart with passed data
		  data = anychart.data.set([
			  <?php
			  foreach($headcountByDepartment as $headcount){ ?>
					['<?php echo $headcount['department'] ?>',<?php echo $headcount['headcount'] ?>]
					<?php  if ($headcount !== end($headcountByDepartment))
	        					echo ',';?>
			  <?php } ?>

		  ]);


		  var wealth = data.mapAs({
		    x: [0],
		    value: [1]
		  });

		  chart = anychart.pie(wealth);
		  chart.labels()
		    .hAlign("center")
		    .position('outside');;

		  // set chart title text settings
		  chart.title('Head Count by Department');

		  // set legend title text settings
		  chart.legend()
		    .title(false)
		    // set legend position and items layout
		    .position('bottom')
		    .itemsLayout('horizontal')
		    .align('center');

		  // set container id for the chart
		  chart.container('headcountByDepartment');
		  // initiate chart drawing
		  chart.draw();
		});

	anychart.onDocumentReady(function() {
		  // create pie chart with passed data
		  data = anychart.data.set([
			  <?php
			  foreach($headcountBySupplier as $headcount){ ?>
					['<?php echo $headcount['supplier_name'] ?>',<?php echo $headcount['headcount'] ?>]
					<?php  if ($headcount !== end($headcountBySupplier))
	        					echo ',';?>
			  <?php } ?>

		  ]);


		  var wealth = data.mapAs({
		    x: [0],
		    value: [1]
		  });

		  chart = anychart.pie(wealth);
		  chart.labels()
		    .hAlign("center")
		    .position('outside');;

		  // set chart title text settings
		  chart.title('Head Count by Supplier');

		  // set legend title text settings
		  chart.legend()
		    .title(false)
		    // set legend position and items layout
		    .position('bottom')
		    .itemsLayout('horizontal')
		    .align('center');

		  // set container id for the chart
		  chart.container('headcountBySupplier');
		  // initiate chart drawing
		  chart.draw();
		});


		anychart.onDocumentReady(function() {
			  // create pie chart with passed data
			  data = anychart.data.set([
				  <?php
				  foreach($headcountByLocation as $headcount){ ?>
						['<?php echo $headcount['location'] ?>',<?php echo $headcount['headcount'] ?>]
						<?php  if ($headcount !== end($headcountByLocation))
		        					echo ',';?>
				  <?php } ?>

			  ]);


			  var wealth = data.mapAs({
			    x: [0],
			    value: [1]
			  });

			  chart = anychart.pie(wealth);
			  chart.labels()
			    .hAlign("center")
			    .position('outside');;

			  // set chart title text settings
			  chart.title('Head Count by Location');

			  // set legend title text settings
			  chart.legend()
			    .title(false)
			    // set legend position and items layout
			    .position('bottom')
			    .itemsLayout('horizontal')
			    .align('center');

			  // set container id for the chart
			  chart.container('headcountByLocation');
			  // initiate chart drawing
			  chart.draw();
			});

			
</script>

<style>
	.rpt-menu-item{
		color: #23527c;
		padding: 10px 20px 10px 20px;
		display: inline-block;
		font-size: 1.1em;
	}

	.rpt-menu-item.active{
		background-color: #EEE;
	}	
	
	.rpt-menu-item:hover{
		cursor: pointer;
		background-color: #EEE;
	}
	
	.rpt-menu-item-detail{
		background-color: #EEE;
		padding: 10px 30px 10px 30px;
	}
	
	 #sortable1, #sortable2 {
            border: 1px solid #eee;
            width: 142px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
            background-color: #FFF;
            width: 100%;
        }
        
        #sortable1, #sortable2:hover {
             cursor: pointer;
        }
        
        #sortable1 li, #sortable2 li {
            margin: 0 5px 5px 5px;
            padding: 2px 5px 2px 5px;
            font-size: 1.2em;
            border-style: dashed;
            border-color: #EEE;
            border-width: 1px;
        }
        
        .rpt-column-list-move{
            vertical-align: middle;
            text-align: center;
            padding-top: 60px;
        }
        
        .rpt-column-list-move button{
        	margin: 5px 0px;
        }
 
 		.btn-chart-table {
 			margin-top: 3px;
 		}
 
         .btn-menu{
            background-color:#FFF;
            -webkit-box-shadow: none;
			-moz-box-shadow: none;
			box-shadow: none;
            color: #7fc35c;
            border: 1px;
            border-style: solid;
            border-color: #7fc35c;
            font-size: 1em;
            border-left-color: #fff;
        }
        
        .btn-menu:hover{
            color: #000;
        }
                     
        .btn-menu.active{
            background-color:#7fc35c;
            -webkit-box-shadow: none;
			-moz-box-shadow: none;
			box-shadow: none;
            color: #FFF;
        }

        .btn-menu.active:hover{
            color: #000;
        }

        .btn-menu.active:hover{
            color: #000;
        }
        
		.column-list-header{
			font-size: 1.2em;
		}

		.btn-cancel{
			background-color: #fff;
		}
		
		.btn-chartype.active{
			background-color: rgb(25, 118, 210);
			color: #fff;
		}
		
		.download-excel{
			color: green;
		}

		.btn-chartype {
		    font-size: 1em;
		    padding: 6px;
		    margin-right: 2px;
		}
		
		#dataTable_filter input{
			border-color: rgba(0, 0, 0, 0.1);
		    border-radius: 2px;
		    box-shadow: none;
		    padding: 5px;
		}
		
		#dataTable_length select.form-control.input-sm{
			padding: 4px;
	    	height: 32px;
		}
		
		#dataTable tr{
			line-height: 1;
		}
		
		#dataTable td{
			border: none;
		}
		
		#dataTable th{
			background-color: #ddd;
		}
		
		#dataTable tr.even{
			background-color: #f6f6f6;
		}
	
</style>
<?php $this->pageTitle =  'Headcount Reports'; ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border-bottom-width:1px; border-bottom-style: solid; border-bottom-color: #EEE;">
	<div class="rpt-menu-item" id="menuFilter" onclick="showMenuItemDetail('Filter')">Filter</div>
	<div class="rpt-menu-item" id="menuCustomize" onclick="showMenuItemDetail('Customize')">Customize</div>
	<div class="rpt-menu-item" id="menuSchedule" onclick="showMenuItemDetail('Schedule')">Schedule</div>
	<div class="rpt-menu-item" id="menuSave" onclick="showMenuItemDetail('Save')">Save</div>
	
    <div class="btn-group btn-chart-table pull-right" role="group">
    	<button type="button" id="btnChart" onclick="toggleChart()" class="btn btn-menu active"><span class="glyphicon glyphicon-object-align-left"></span> Graph Chart</button>
        <button type="button" id="btnDataTable" onclick="toggleDataTable()" class="btn btn-menu active"><span class="glyphicon glyphicon-th"></span> Data Table</button>
    </div>
</div>

<div>
    <div id="rptFilter" class="rpt-menu-itemdetail" style="display:none;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rpt-menu-item-detail">

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Job Category</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="job_category" id="job_category">
                                <option value=""></option> 
		                    <?php foreach($jobCategoryList as $item){ ?>
									<option value="<?php echo $item['job_category'] ?>"<?php if($filterCategory == $item['job_category']) echo "selected" ?>><?php echo $item['job_category'] ?></option>
							<?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Location</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="location" id="location">
                            <option value=""></option>
		                    <?php foreach($locationList as $item){ ?>
									<option value="<?php echo $item['location'] ?>"<?php if($filterLocation == $item['location']) echo "selected" ?>><?php echo $item['location'] ?></option>
							<?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Department</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="department" id="department">
                            <option value=""></option> 
		                    <?php foreach($headcountByDepartment as $item){ ?>
									<option value="<?php echo $item['department'] ?>"><?php echo $item['department'] ?></option>
							<?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Supplier</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="supplier" id="supplier">
                            <option value=""></option>
		                    <?php foreach($headcountBySupplier as $item){ ?>
									<option value="<?php echo $item['supplier_name'] ?>"><?php echo $item['supplier_name'] ?></option>
							<?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top: 15px; padding-bottom: 20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <button class="btn btn-primary" id="btnApplyFilter" >Apply Filters</button>
                    <button type="button" class="btn btn-cancel" onclick="cancelMenuAction();">Cancel</button>
                </div>
            </div>
        </div>
     </div>
     

     <div id="rptCustomize" class="rpt-menu-itemdetail" style="display:none;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rpt-menu-item-detail">
            <div class="row">
                <div class="rpt-column-list col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <label class="m-b-4 column-list-header">Available Columns</label>
                    <ul id="sortable1" class="connectedSortable">
                        <li class=""><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Job Id</li>
                        <li class=""><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Candidate Id</li>
                        <li class=""><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Manager Name</li>
                        <li class=""><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Start Date</li>
                        <li class=""><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>End Date</li>
                    </ul>
                </div>
                <div class="rpt-column-list-move col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <button class="btn btn-default">&gt</button>
                    <br/>
                    <button class="btn btn-default">&gt&gt</button>
                    <br/>
                    <button class="btn btn-default"n>&lt</button>
                </div>
                <div class="rpt-column-list col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <label class="m-b-4 column-list-header">Selected Columns</label>
                    <ul id="sortable2" class="connectedSortable">
                        <li class="ui-column-list-selected"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Candidate Name</li>
                        <li class="ui-column-list-selected"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Title</li>
                        <li class="ui-column-list-selected"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Job Category</li>
                        <li class="ui-column-list-selected"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Location</li>
                        <li class="ui-column-list-selected"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Deparment</li>
                    </ul>
                </div>
            </div>

            <div class="row" style="padding-top: 15px; padding-bottom: 20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <button class="btn btn-primary" id="btnApplyCustomization" >Update Data Table</button>
                    <button class="btn btn-default" onclick="cancelMenuAction();" >Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div id="rptSchedule" class="rpt-menu-itemdetail" style="display:none;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rpt-menu-item-detail">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Report Name</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="report_id" id="report_id">
                            <option value="">Select Report</option>
                            <?php if($SavedReport){
                            foreach ($SavedReport as $value){ ?>
                            <option value="<?php echo $value->id; ?>"><?php echo $value->report_name; ?></option>
                            <?php  } } ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Report Group</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="rptr_group" id="rptr_group">
                            <option value="">Select Group</option>
                            <?php foreach ($reportGroup as $key=>$value){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Run Frequency</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="rpt_run_frequency" id="rpt_run_frequency">
                            <option value=""></option> 
                            <option value="Weekly">Weekly</option>
                            <option value="Monthly">Monthly</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Run Day</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="rpt_run_day" id="rpt_run_day">
                            <option value=""></option> 
                            <?php foreach ($runday as $key=>$value){ ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Start Date</label>
                        <i class="required-field"></i>
                        <input class="form-control" required="required" name="startDate" id="startDate" type="date" maxlength="10" placeholder="MM/DD/YYYY">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">End Date</label>
                        <i class="required-field"></i>
                        <input class="form-control" required="required" name="endDate" id="endDate" type="date" maxlength="10" placeholder="MM/DD/YYYY">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Run Time</label>
                        <i class="required-field"></i>
                        <input class="form-control" required="required" name="runTime" id="runTime" type="text" maxlength="10" placeholder="HH:MM AM/PM">	
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Format</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="rpt_formate" id="rpt_formate">
                            <option value=""></option> 
                            <option value="Excel">Excel</option>
                            <!--<option value="PDF">PDF</option>
                            <option value="CSV">CSV</option>-->
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="">Receivers</label>
                        <i class="required-field"></i>
                        <input class="form-control" required="required" name="rpt_receivers" id="rpt_receivers" type="text" maxlength="255" placeholder="Email Addresses">	
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top: 15px; padding-bottom: 20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <button class="btn btn-primary" id="btnCreateRptSchedule" >Schedule Report</button>
                    <button class="btn btn-default" onclick="cancelMenuAction();">Cancel</button>
                </div>
            </div>
        </div>
    </div>                    

    <div id="rptSave" class="rpt-menu-itemdetail" style="display:none;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rpt-menu-item-detail">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Report Name</label>
                        <i class="required-field"></i>
                        <input class="form-control" required="required" name="reportName" id="reportNamt" type="text" maxlength="255" placeholder="Name">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Report Group</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="rpt_group" id="rpt_group">
                                <option value="">Select Group</option>
                               <?php foreach ($reportGroup as $key=>$value){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Access Group</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="rpt_access_group" id="rpt_access_group">
                            <option value="">Select Access Group</option>
                            <?php foreach ($accessGroup as $key=>$value){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="">Format</label>
                        <i class="required-field"></i>
                        <select class="form-control" name="rpt_format" id="rpt_format">
                            <option value="">Select Format</option>
                            <option value="Excel">Excel</option>
                            <!--<option value="PDF">PDF</option>
                            <option value="CSV">CSV</option>-->
                        </select>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top: 15px; padding-bottom: 20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <button class="btn btn-primary"  id="btnRptSave" >Save Report</button>
                    <button class="btn btn-default" onclick="cancelMenuAction();">Cancel</button>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table" id="savedReports">
                <thead>
                <tr>
                    <td>
                        Report Name
                    </td>
                    <td>
                        Report Group
                    </td>
                    <td>
                        Access Group
                    </td>
                    <td>
                        Format
                    </td>
                    <td>
                        Action
                    </td>
                </tr>
                </thead>
                <tbody>
                <?php if($SavedReport){
                    foreach ($SavedReport as $value){
                        $location = Location::model()->findByAttributes(array('name'=>$value->location));
                        $dataUrl = array();
                        if($value->job_category){
                            $dataUrl['category']= trim($value->job_category);
                        }
                        if($value->location){
                            $dataUrl['location']= trim($location->id);
                        }
                        if($value->department){
                            $dataUrl['department']= trim($value->department);
                        }
                        if($value->supplier){
                            $dataUrl['supplier']= trim($value->supplier);
                        }
                        if($value->access_group){
                            $dataUrl['access_group']= trim($value->access_group);
                        }
                        if($value->access_group){
                            $dataUrl['report_group']= trim($value->report_group);
                        }
                        if($value->formate){
                            $dataUrl['formate']= trim($value->formate);
                        }

                        ?>
                        <tr>
                            <td>
                                <?php echo $value->report_name ?>
                            </td>
                            <td>
                                <?php echo $reportGroup[$value->report_group]; ?>
                            </td>
                            <td>
                                <?php echo $accessGroup[$value->access_group]; ?>
                            </td>
                            <td>
                                <?php echo $value->formate ?>
                            </td>
                            <td>

                                    <span class="glyphicon glyphicon-download-alt"></span>
                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/filterReport',$dataUrl) ?>" data-toggle="tooltip" data-placement="top" title="Download Data Sheet" >
                                        Download
                                    </a>

                            </td>
                        </tr>
                    <?php } } ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-lg-12" class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-10 m-b-20">Head Count Report</h4>
            
            <div class="pull-right download-excel" >
               <span class="glyphicon glyphicon-download-alt"></span>
               <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/HeadCountExcel') ?>" data-toggle="tooltip" data-placement="top" title="Download Excel Data Sheet" >
                    Download Excel Data Sheet
            </a>
            
            </div>
            
            <div id="divChart" class="row">
	            <div>
	            	<button id="btnChartByCategory" class="btn btn-chartype active" onclick="showHideChart('Category')">Head Count by Category</button>
	            	<button id="btnChartByLocation" class="btn btn-chartype active" onclick="showHideChart('Location')">Head Count by Location</button>
	            	<button id="btnChartByDepartment" class="btn btn-chartype " onclick="showHideChart('Department')">Head Count by Department</button>
	            	<button id="btnChartBySupplier" class="btn btn-chartype " onclick="showHideChart('Supplier')">Head Count by Supplier</button>
	            </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div id="chartContainerCategory" class="card-block col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div id="headcountByCategory" style="width: 100%; height: 400px;"></div>
                        </div>
                        <div id="chartContainerLocation" class="card-block col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div id="headcountByLocation" style="width: 100%; height: 400px;"></div>
                        </div>
                        <div id="chartContainerDepartment" class="card-block col-xs-12 col-sm-6 col-md-6 col-lg-6" style="display:none">
                            <div id="headcountByDepartment" style="width: 100%; height: 400px;" ></div>
                        </div>
                        <div id="chartContainerSupplier" class="card-block col-xs-12 col-sm-6 col-md-6 col-lg-6" style="display:none">
                            <div id="headcountBySupplier" style="width: 100%; height: 400px;" ></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <p class="m-b-40"></p>
            <div id="divDataTable" class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table id="dataTable" class="table m-b-40 without-border">
                        <thead class="thead-default">
                            <tr>
                                <th>Candidate</th>
                                <th>Job Title</th>
                                <!--  <th>Location</th>-->
                                <th>Department</th>
                                <th>Hiring Manager</th>
                                <th>Job Category</th>
                                <th>Supplier</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        <?php
                        foreach($headcountData as $headcount){ ?>
                        	<tr>
                        		<td><?php echo $headcount['candidate_name'] ?></td>
                        		<td><?php echo $headcount['job_title'] ?></td>
                                <td><?php echo $headcount['hiring_mgr_department'] ?></td>
                                <td><?php echo $headcount['hiring_mgr_name'] ?></td>
                                <td><?php echo $headcount['job_category'] ?></td>
                                <td><?php echo $headcount['supplier_name'] ?></td>          
                            </tr>
                        <?php } ?>
                                        
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
        </div>
        <!-- row -->
        <div class="seprater-bottom-100"></div>
    </div>
</div>
</div>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
               Data Saved..
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer"><button type="button" class="btn btn-primary">OK</button></div>
        </div>
    </div>
</div>
<script>

    $( document ).ready(function() {
        $("#btnRptSave").on("click", function(e){
            var Category =''; var location=''; var department=''; var supplier='';
            var reportNamt='';var rpt_group=''; var rpt_access_group='';  var rpt_format='';
            if($("#job_category").val()){
                var Category = $("#job_category").val();
            }
            if($("#location").val()){
                var location = $("#location").val();
            }
            if($("#department").val()){
                var department = $("#department").val();
            }
            if($("#supplier").val()){
                var supplier = $("#supplier").val();
            }
            if($("#reportNamt").val()){
                var reportNamt = $("#reportNamt").val();
            }
            if($("#rpt_group").val()){
                var rpt_group = $("#rpt_group").val();
            }
            if($("#rpt_access_group").val()){
                var rpt_access_group = $("#rpt_access_group").val();
            }
            if($("#rpt_format").val()){
                var rpt_format = $("#rpt_format").val();
            }
            $.ajax({
                'url':'<?php echo $this->createUrl('reports/saveReaports') ?>',
                type: "POST",
                data: { Category: Category,location:location,department:department,supplier:supplier,
                    reportNamt:reportNamt,rpt_group:rpt_group,rpt_access_group:rpt_access_group,rpt_format:rpt_format},
                'success':function(html){
                    //window.location.href = "<?php echo $this->createUrl('reports/headcountCurrent') ?>";
                     $("#savedReports").html(html)
                }
            });
        });

        $("#btnCreateRptSchedule").on("click", function(e){
            var report_id =''; var rptr_group=''; var rpt_run_frequency=''; var rpt_run_day='';
            var startDate='';var endDate=''; var rpt_formate='';  var rpt_receivers='';
            if($("#report_id").val()){
                var report_id = $("#report_id").val();
            }
            if($("#rptr_group").val()){
                var rptr_group = $("#rptr_group").val();
            }
            if($("#rpt_run_frequency").val()){
                var rpt_run_frequency = $("#rpt_run_frequency").val();
            }
            if($("#rpt_run_day").val()){
                var rpt_run_day = $("#rpt_run_day").val();
            }
            if($("#startDate").val()){
                var startDate = $("#startDate").val();
            }
            if($("#endDate").val()){
                var endDate = $("#endDate").val();
            }
            if($("#rpt_formate").val()){
                var rpt_formate = $("#rpt_formate").val();
            }
            if($("#rpt_receivers").val()){
                var rpt_receivers = $("#rpt_receivers").val();
            }
            $.ajax({
                'url':'<?php echo $this->createUrl('reports/saveSchedule') ?>',
                type: "POST",
                data: { report_id: report_id,rptr_group:rptr_group,rpt_run_frequency:rpt_run_frequency,rpt_run_day:rpt_run_day,
                    startDate:startDate,endDate:endDate,rpt_formate:rpt_formate,rpt_receivers:rpt_receivers},
                'success':function(html){
                    if(html=='Success') {
                        //$('#myModal').modal('toggle');
                        window.location.href = "<?php echo $this->createUrl('reports/headcountCurrent') ?>";
                    }else{
                            alert('Having some Issue')
                    }
                }
            });
        });

    });
	function showMenuItemDetail(menuItem){
	    $(".rpt-menu-itemdetail").hide();
	    $("#rpt" + menuItem).show();
	    $(".rpt-menu-item").removeClass("active");
	    $("#menu" + menuItem).addClass("active");
	};
	
	function cancelMenuAction(){
		$(".rpt-menu-itemdetail").hide();
		$(".rpt-menu-item").removeClass("active");
	};
	
	function toggleChart(){
		if( $("#divChart").is(":visible")){
			$("#divChart").hide();
			$("#btnChart").removeClass("active");
		}
		else{
			$("#divChart").show();
			$("#btnChart").addClass("active");
		}
	}
	
	function toggleDataTable(){
		if( $("#divDataTable").is(":visible")){
			$("#divDataTable").hide();
			$("#btnDataTable").removeClass("active");
		}
		else{
			$("#divDataTable").show();
			$("#btnDataTable").addClass("active");
		}
	}

	function showHideChart(chartName){
		if($("#chartContainer" + chartName).is(":visible")){
			$("#chartContainer" + chartName).hide();
			$("#btnChartBy" + chartName).removeClass("active");
		}
		else{
			$("#chartContainer" + chartName).show();
			$("#btnChartBy" + chartName).addClass("active");
		}
	}
	
	$(function() {
		$( "#sortable1, #sortable2" ).sortable({
			connectWith: ".connectedSortable"
			}).disableSelection();
		
		$("#btnApplyFilter").on("click", function(e){
			var filterUrl = '';
			if($("#job_category").val()){
				filterUrl += '/category/' + $("#job_category").val();
			}
			if($("#location").val()){
				filterUrl += '/location/' + $("#location").val();
			}
		    $("#rptFilter").toggle();
		    $("#menuFilter").removeClass("active");
		    window.location.href = "<?php echo Yii::app()->createAbsoluteUrl('Client/reports/headcountCurrent') ?>" +  filterUrl;
		});
		
		$("#btnCancelMenuAction").on("click", function(e){
		    $(".rpt-menu-itemdetail").hide();
		    $("nav").removeClass("active");
		});

		 $('#dataTable').DataTable( {
		        "order": [[ 3, "desc" ]]
		    } );

	});

</script>
<script>
    $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal").modal('hide');     // dismiss the dialog
        });
    });
    $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });

    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });

    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true                     // ensure the modal is shown immediately
    });
</script>


