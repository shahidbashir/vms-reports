<?php

$clientID = Yii::app()->user->id;
$client = Client::model()->findByPk($clientID);
if($client->member_type!=NULL){
	$client = Client::model()->findByPk($client->super_client_id);
	$clientID = $client->id;
	}
$ClientRate = ClientRate::model()->findByAttributes(array('client_id'=>$clientID));
$this->pageTitle = 'Job Step2';

$template = JobTemplates::model()->findByPk($model->template_id);

$recommendedBillRate = '';
//suggesting billrate to client on with the help of template rates

if($template){
    $templateexp = explode(',',$template->temp_experience);
    $temp_min_billrate = explode(',',$template->temp_min_billrate);
    $temp_max_billrate = explode(',',$template->temp_max_billrate);
    foreach($templateexp as $key=>$value){
        if($value==$model->experience){
            $recommendedBillRate = '$ '.$temp_min_billrate[$key].' to $ '.$temp_max_billrate[$key];
        }
    }
}


$settingData = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$model->cat_id));
$noMarkupCat = NoMarkupCate::model()->findByAttributes(array('category_id'=>$settingData->id));

if($noMarkupCat->bill_rate){
	$noMarkupBillrate = $noMarkupCat->bill_rate;
	}else{
		 $noMarkupBillrate = 0;
		 }
/*echo '<pre>';
print_r($noMarkupCat->bill_rate);
if(empty($noMarkupCat)){
	echo 'hahahaha';
	}
exit;*/

?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

    <div class="cleafix " style="padding: 30px 20px; ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-b-10"><?php echo $model->title; ?>
                <a href="<?php echo $this->createAbsoluteUrl('noMarkup/jobStep1',array('id'=>$_GET['id'])); ?>" class="btn btn-sm btn-default-3 pull-right">Edit prevous form</a>
            </h4>
            <p class="m-b-40">Following are the instruction to create a job requisition.</p>

            <?php
            if($recommendedBillRate){ ?>
            <div class="well well-yellow" id="well-yellow">
                <p>Recommended Bill Rate for this Job : <?php echo $recommendedBillRate; ?></p>
            </div>
            <?php } ?>

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'job-form',
                'enableAjaxValidation'=>false,
            ));
            $loginUserId = Yii::app()->user->id;
            ?>
            <input type="hidden" id="calculated_value" name="calculated_value" value="" class="form-control"  />
            <input type="hidden" id="bill_rate_hidden" name="bill_rate_hidden" value="" class="form-control"  />
            <input type="hidden" name="Job[vendor_client_markup]" id="vendor_client_markup1" class="form-control" value="<?php echo $model->markup; ?>">
            <input type="hidden" value="<?php echo $model->cat_id; ?>" id="cat_id" name="cat_id">
            <div class="row add-job">
                <div class="col-md-12">
                    <div class="panel panel-border-color panel-border-color-primary">
                        <div class="panel-heading"></div>
                        <div class="panel-body" style="overflow-y: inherit;">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="">Bill Rate</label>
                                        <i class="required-field fa fa-asterisk"></i>
                                        <?php echo $form->numberField($model,'bill_rate',array('class'=>'form-control','min'=>'.1','value'=>$noMarkupBillrate, 'step' => "any",'onChange'=>'calculate("billrate")','required'=>'required')); ?>
                                    </div>
                                </div>
                                <?php if($ClientRate->type == 'Fixed Markup'){ ?>
                                    <!--<td style="width: 4%;"> or </td>
                                    <td style="width: 48%;"><div class="form-group">
                                        <label for="">Pay Rate</label>-->
                                    <?php echo $form->hiddenField($model,'pay_rate',array('class'=>'form-control','min'=>'.1','step' => "any", 'onChange'=>'calculate("payrate")')); ?>
                                    <!--</div>--></td>

                                <?php }else{ ?>
                                    <input type="hidden" name="Job[pay_rate]" id="Job_pay_rate" value="0" class="form-control" />
                                <?php } ?>

                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="">Select Pay Rate Type</label>
                                        <i class="required-field fa fa-asterisk"></i>
                                        <?php
                                        $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                                        echo $form->dropDownList($model, 'payment_type', $list , array('class'=>'form-control','empty' => '','required'=>'required')/*,'onChange'=>'calculate()'*/);  
										?>
                                    </div>
                                </div>
                                <!-- col -->
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="">Shift</label>
                                        <?php
                                        $shift = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=15')),'title', 'title');
                                        echo $form->dropDownList($model, 'shift', $shift , array('class'=>'form-control','empty' => '')); 
										?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="">Hours Per Week</label>
                                        <i class="required-field fa fa-asterisk"></i>
                                        <?php
                                        $hours_per_week = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=16')),'title', 'title');
                                        echo $form->dropDownList($model, 'hours_per_week', $hours_per_week , array('class'=>'form-control','empty' => '','onChange'=>'calculate()','required'=>'required')); 
										?>
                                    </div>
                                </div>
                                <!-- col -->
                            </div>
                            <!-- row -->

                            <br>
                            <input type="text" name="job_po_duration" id="daterange" value="<?php echo $model->job_po_duration; ?>" style="display:none">
                            <input type="text" name="job_po_duration_endDate" id="daterange1" value="<?php echo $model->job_po_duration_endDate; ?>" style="display:none">
                            <input type="hidden" id="dateField" name="dateField" value="<?php echo $model->desired_start_date; ?>">
                            <!-- well -->
                            <div class="well well-info">
                                <h4>Internal Reference Section</h4>
                                <br>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="">Request Department</label>
                                            <select class="form-control" name="Job[request_dept]">
                                                <option value=""></option>
                                                <?php
                                                $JobrequestDepartment = JobrequestDepartment::model()->findAllByAttributes(array('client_id'=>$clientID));
                                                foreach($JobrequestDepartment as $JobrequestDepartment){ ?>
                                                    <option value="<?php echo $JobrequestDepartment->department; ?>"><?php echo $JobrequestDepartment->job_request; ?></option>
                                                <?php  }  ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- col -->
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="">Billcode</label>
                                            <select class="form-control" name="Job[billing_code]">
                                                <option value=""></option>
                                                <?php
                                                $BillingcodeDepartment = BillingcodeDepartment::model()->findAllByAttributes(array('client_id'=>$clientID));
                                                foreach($BillingcodeDepartment as $BillingcodeDepartment){ ?>
                                                    <option value="<?php echo $BillingcodeDepartment->billingcode; ?>"><?php echo $BillingcodeDepartment->billingcode; ?></option>
                                                <?php  }  ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- col -->
                                </div>
                                <!-- row -->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <label for="">Rate</label>
                                        <br>
                                        <div class="be-checkbox">
                                            <input id="check1"  name="Job[over_time]" type="checkbox"  value="1">
                                            <label for="check1">OverTime</label>
                                        </div>
                                        <div class="be-checkbox">
                                            <input id="check2" type="checkbox" name="Job[double_time]" value="1">
                                            <label for="check1">Double Time</label>
                                        </div>
                                    </div>
                                    <!-- col -->
                                </div>
                                <!-- row -->
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="">Reference Code / Project Code </label>
                                            <input type="text" id="pre_reference_code"  name="Job[pre_reference_code]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="">Total Estimation Cost </label>
                                            <input type="text" id="pre_total_estimate_code"  name="Job[pre_total_estimate_code]" class="form-control" required="required" readonly="readonly">
                                        </div>
                                    </div>
                                    <!-- col-12 -->
                                </div>
                                <!-- row -->

                                <div class="row" id="workflow_member">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="">Job Workflow approval</label>
                                            <select name="" id="input" class="form-control" readonly="readonly">
                                                <option value=""></option>
                                            </select>
                                        </div>

                                        <p class="m-t-10 m-b-10">Following are the Member for Job Approva</p>

                                        <table class="table table-white">
                                            <thead>
                                            <tr>
                                                <th>Approval</th>
                                                <th>Name</th>
                                                <th>Department</th>
                                                <th>Work Flow Order</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td> </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="">Status of Job</label>
                                            <select name="Job[jobStatus]" class="form-control" disabled="disabled">
                                                <?php $jobStatus = UtilityManager::jobStatus();
                                                foreach($jobStatus as $key=>$jobStatus){ ?>
                                                    <option value='<?php echo $key; ?>'><?php echo $jobStatus; ?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" name="Job[jobStatus]" value="1"  />
                                        </div>
                                    </div>
                                    <!-- col -->
                                </div>
                                <!-- row -->
                            </div>
                            <!-- well -->

                            <br>
                            <div>
                                <button type="submit" name="Post_secondForm" class="btn btn-success">Submit</button>
                                <button type="submit" name="save_draft" class="btn btn-default-2">Save as draft</button>
                                <button type="submit" name="publish_template" class="btn btn-info">Submit & Save as template</button>
                            </div>
                            <br>
                            <br>
                        </div>
                        <!-- panel-body -->
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <!-- col -->

    </div>
    <!-- row -->
    <div class="seprater-bottom-100"></div>

</div>





<?php if(empty($noMarkupCat)){ ?>

<script type="text/javascript">
    //calculating rates of per hour job.
    function calculate(type,payrate){
        var Job_bill_rate = document.getElementById("Job_bill_rate").value;
        var Job_pay_rate = document.getElementById("Job_pay_rate").value;
        var Job_payment_type = document.getElementById("Job_payment_type").value;
        var Job_hours_per_week = document.getElementById("Job_hours_per_week").value;
        var daterange = document.getElementById("daterange").value;
        var daterange1 = document.getElementById("daterange1").value;
        var dateField = document.getElementById("dateField").value;
        var markup = document.getElementById("vendor_client_markup1").value;
        var bill_rate_hidden = document.getElementById("bill_rate_hidden").value;
        var cat_id = document.getElementById("cat_id").value;
        
        var numOfDates = getBusinessDatesCount(new Date(daterange),new Date(daterange1));
        //alert(numOfDates);
        if(numOfDates==0){
            var numOfDates = 1;
        }

        var str = Job_hours_per_week;
        var numberHours = str.split(" ",1);

        var hoursPerDay = numberHours/5;

        //alert(hoursPerDay);

        if(type == 'billrate'){
            var pay_rate = Job_bill_rate * 100/( 100 + parseFloat(markup));
            document.getElementById("Job_pay_rate").value = pay_rate.toFixed(3);
            document.getElementById("calculated_value").value = pay_rate.toFixed(3);
            //billrate value will be saved to this hidden field
            document.getElementById("bill_rate_hidden").value = Job_bill_rate;
            //alert(Job_bill_rate);
        }else if(type == 'payrate'){
            var bill_rate = parseFloat(Job_pay_rate) + (parseFloat(Job_pay_rate) * parseFloat(markup)/100);
            document.getElementById("Job_bill_rate").value = bill_rate.toFixed(3);
            document.getElementById("calculated_value").value = bill_rate.toFixed(3);
            document.getElementById("bill_rate_hidden").value = bill_rate.toFixed(3);
        }
        //replacing bill_rate_hidden with Job_bill_rate  b/c now we have only bill rate
        //var estimate_cost = bill_rate_hidden * numOfDates * hoursPerDay;
        //document.getElementById("pre_total_estimate_code").value = bill_rate_hidden * numOfDates * hoursPerDay;
        var estimate_cost = Job_bill_rate * numOfDates * hoursPerDay;
        document.getElementById("pre_total_estimate_code").value = Job_bill_rate * numOfDates * hoursPerDay;
        workflowConfiguration(estimate_cost , cat_id);
        //}

    }


</script>

<?php }else{ ?>

<script>
function calculate(){
	var Job_bill_rate = document.getElementById("Job_bill_rate").value;
	var Job_hours_per_week = document.getElementById("Job_hours_per_week").value;
	var daterange = document.getElementById("daterange").value;
	var daterange1 = document.getElementById("daterange1").value;
	var cat_id = document.getElementById("cat_id").value;
	
	var numOfDates = getBusinessDatesCount(new Date(daterange),new Date(daterange1));
	if(numOfDates==0){
		var numOfDates = 1;
	}

	var str = Job_hours_per_week;
	var numberHours = str.split(" ",1);

	var hoursPerDay = numberHours/5;
	var estimate_cost = Job_bill_rate * numOfDates * hoursPerDay;
	document.getElementById("pre_total_estimate_code").value = Job_bill_rate * numOfDates * hoursPerDay;
	workflowConfiguration(estimate_cost , cat_id);
}
	
</script>

<?php } ?>


<script>
	//testing this one
    function getBusinessDatesCount(startDate, endDate) {
        var count = 0;
        var curDate = startDate;
        while (curDate <= endDate) {
            var dayOfWeek = curDate.getDay();
            if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
                count++;
            curDate.setDate(curDate.getDate() + 1);
        }
        return count;
    }
	
	function workflowConfiguration(estimate_cost , cat_id){
        //alert(cat_id);
        $.ajax({
            'url':'<?php echo $this->createUrl('job/workflowConfiguration') ?>',
            type: "POST",
            data: { estimate_cost: estimate_cost ,cat_id: cat_id},
            'success':function(html){
                //$("#workflow_member").valhtml = html;
                document.getElementById("workflow_member").innerHTML = html;
                //$("#vendor_client_markup").val(html);
            }
        });
    }
</script>