<?php $this->pageTitle = 'Job'; ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

  <div class="cleafix " style="padding: 30px 20px; ">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Add Job</h4>
      <p class="m-b-40">You can create job from Job template or from the start.</p>



      <table class="table table-verticle no-border" >
        <tbody>
        <tr>
          <td  style="width: 47%; text-align: left;">
            <p class="m-b-10">Create a job from a existing template.</p>
            <form method="post" action="<?php echo Yii::app()->createAbsoluteUrl('Client/noMarkup/selectetemplate'); ?>">
            <div class="search-box-2">
              <div class="form-group form-control-2">
                <label for="" style="text-align: left;">Existing Job Catalog</label>
                <i class="required-field fa fa-asterisk"></i>
                <select name="templet_id" id="input" class="form-control  select2" required="required">
                  <option value="">Select Template </option>
                <?php
				$clientID = Yii::app()->user->id;
				$client = Client::model()->findByPk($clientID);
				if($client->member_type!=NULL){
					$client = Client::model()->findByPk($client->super_client_id);
					$clientID = $client->id;
					}
				  
                  $model = JobTemplates::model()->findAllByAttributes(array('client_id'=>$clientID));
                  if($model){
                    foreach($model as $value){
                      ?>
                      <option value="<?php echo $value->id; ?>"><?php echo $value->job_title; ?></option>
                    <?php } } ?>
                </select>
              </div>
              <input type="submit" name="continue" value="Continue" class="btn btn-success ">
            </div>
          </form>
          </td>
          <td style="width: 6%; text-align: center;">Or</td>
          <td  style="width: 47%; text-align: left;">

            <p class="m-b-10">Create a job from start, it would take like 5 min to create a complete job requisition</p>
            <form name="" method="post">
            <div class="search-box-2">
              <div class="form-group ">
                <label for="" style="text-align: left;">
                  <p>Create from Scratch</p>
                </label>
                <i class="required-field fa fa-asterisk"></i>
                <input type="text" name="new_job_title" required="required" id="input" class="form-control">
              </div>
              <input type="submit" name="new_job" value="Continue" class="btn btn-success " />

            </div>
            </form>
          </td>
        </tr>
        </tbody>
      </table>

    </div>
    <!-- col -->

  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>

</div>
