<?php
/* @var $this JobController */
/* @var $model Job */

$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Job', 'url'=>array('index')),
	array('label'=>'Manage Job', 'url'=>array('admin')),
);
?>
<?php $this->pageTitle= 'Job Step1'; ?>
<?php //$this->renderPartial('_form', array('model'=>$model)); ?>
<?php 

unset(Yii::app()->session['job_id']);

$this->renderPartial('jobStep1', array('model'=>$model)); ?>