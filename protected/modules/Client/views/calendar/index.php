<?php
$this->pageTitle=Yii::app()->name;
?>
<?php if(Yii::app()->user->hasFlash('success')):?>
        <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('error')):?>
        <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
<?php endif; ?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'header' => 'ID',
            'name' => 'event_id'
        ),
        array(
            'header' => 'Subject',
            'name' => 'subject'
        ),
        array(
            'header' => 'Content',
            'name' => 'content'
        ),
        array(
            'header' => 'Start Date',
            'name' => 'start'
        ),
        array(
            'header' => 'End Date',
            'name' => 'end'
        ),
        array(
            'header' => 'End Date',
            'name' => 'end'
        ),
        array(
            'header' => '',
            'value' => array($this, 'getLink'),
        ),
    ),
));
?>