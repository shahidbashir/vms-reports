 <?php 
 if($_GET['type']=='offers'){
	 $this->pageTitle = 'Offers';
	 }else{
 	$this->pageTitle = 'Workorder';
	 }
 $this->renderPartial('_menu',array('model'=>$model));

 ?>


<div class="clearfix">
  <table class="table table-striped dataTable no-footer">
    <thead>
      <tr>
        <th> Sr. No: </th>
        <th> Work Order ID </th>
        <th> Candidate Name </th>
        <th>Pay Rate ( Before )</th>
   		<th>Pay Rate  ( After )</th>
        <!--<th> Date </th>-->
        <th> Status </th>
        <th style="text-align: center"> Action </th>
      </tr>
    </thead>
    <tbody>
    
    <?php foreach($offers as $key=>$value) {?>
      <tr>
        <td> <?php echo $key + 1;?> </td>
        <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Admin/offer/view',array('id'=>$value['offer_id'])); ?>" class="blue"><?php echo $value['offer_id'];?></a></td>
        <td> <?php echo $value['first_name'].' '.$value['last_name'];?> </td>
        <td><?php echo $value['before_rate'];?></td>
   		<td><?php echo $value['after_rate'];?></td>
        <!--<td> 14-10-2016 </td>-->
        <?php 
       $offerStatus = UtilityManager::workorderStatus();
       $status = $offerStatus[$value['workorder_status']];
	
       if($value['workorder_status'] == 1){
        $className ="success";
       }else if($value['workorder_status'] == 2){
           $className ="danger";
       }if($value['workorder_status'] == 3){
          $className ="info";
       }else{
		    $className ="info";
		    } ?>
            
        <td><span class="label label-<?php echo $className;?>"><?php echo $status;?></span></td>
        
        <td style="text-align: center" class="actions">
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/offerView',array('id'=>$value['offer_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" mdi mdi-eye"></i></a> 
        
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/jobWorkers',array('offerID'=>$value['offer_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" mdi mdi-delete"></i></a></td>
      </tr>
      
    <?php } ?>
    </tbody>
  </table>
</div>



<div class="modal fade" id="submision-job-select">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Select Job</h4>
      </div>
         <form action="<?php echo $this->createAbsoluteUrl('/Admin/job/createsubmission');?>" method="post" role="form">
      <div class="modal-body">
      <?php $assignedVendor =  AssignedVendorClient::model()->findAll(array('condition'=>'client_id='.$model->user_id));
      if($assignedVendor){
      ?>
     
          
        <input type="hidden" name="id" value="<?php echo $model->id;?>">
          <div class="form-group">
            <label for=""> Select Vendor</label>
             
             <div class="single">
                <select name="vendor-id" class="ui fluid search dropdown">
                 <option value=""></option>
                <?php foreach($assignedVendor as $value){
                    $vendor = Vendor::model()->findByPk($value->vendor_id);
                  ?>  
                  <option value="<?php echo $vendor->id;?>"><?php echo $vendor->organization.'('.$vendor->first_name.' '.$vendor->last_name.')';?></option>
                  <?php } ?>
                     
                }
                </select>
              </div>
          </div>
        
          <?php } ?>
       
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Continue</button>
        <a type="button" class="btn btn-default" data-dismiss="modal">Close</a>
        
      </div>
      </form>
    </div>
  </div>
</div>