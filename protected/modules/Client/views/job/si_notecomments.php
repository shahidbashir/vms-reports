<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="interview-notes-comments ">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="media no-border">
                        <a class="pull-left" href="#">
                           <?php $clientdata = Client::model()->findByPk(Yii::app()->user->id);
							if(!empty($clientdata->profile_image)) {

                  $images_path = Yii::app()->baseUrl.'/images/profile_img/'.$clientdata->profile_image;

                }else{
					$images_path =  Yii::app()->request->baseUrl.'/new-theme/assets/images/avatar.jpg';
					}
							?>
                            <img class="media-object" src="<?php echo $images_path ?>" alt="Image" style="    max-width: 29px;">
                        </a>
                        <form name="notes" method="post" action="" enctype="multipart/form-data">
                        <div class="media-body">
                            <textarea name="comments" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                            <div class="form-group m-b-10 m-t-20">
                                <label data-toggle="modal" class="a-blue">Attachement</label>
                                <input type="file" name="InterviewComments[file_name]" >
                            </div>
                            <div class="be-checkbox">
                                <input id="check1" name="JobComments[show_msp]" type="checkbox" value="1">
                                <label for="check1">Share the notes with MSP</label>
                            </div>
                            <button type="submit" name="Notes" class="btn btn-success m-t-10">Add Comment</button>
                        </div>
                            </form>

                    </div>
                    <!-- media -->

                    <?php $model = InterviewComments::model()->findAllByAttributes(array('interview_id'=>$_GET['interviewId'],'user_id'=>Yii::app()->user->id));

                    if($model){
                    foreach($model as $commentsdata){
					$clientdata	= Client::model()->findByPk($commentsdata['user_id']);
						if(!empty($clientdata->profile_image)) {

                  	$images_path = Yii::app()->baseUrl.'/images/profile_img/'.$clientdata->profile_image;

						}else{
								$images_path =  Yii::app()->request->baseUrl.'/new-theme/assets/images/avatar.jpg';
							}
                    $attachment = Yii::app()->baseUrl . '/../submissionattachment/' . $model['file_name'];

                    ?>

                    <div class="media no-border">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="<?php echo $images_path; ?> " alt="Image" style="    max-width: 29px;">
                        </a>
                        <div class="media-body">
                            <p class="m-b-10 bold"><?php echo $commentsdata['user_name']; ?></p>
                            <p><?php echo $commentsdata['comments']; ?></p>
                            <div class="attachment">
                                <p>Attachments</p>
                                <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/downloadResum',array('id'=>$commentsdata['id'])); ?>" class="">Download Attachment<i class="fa fa-cloud-download"></i></a>

                            </div>
                            <p class="meta-inner">Posted <?php echo $commentsdata['posted_date']; ?></p>
                        </div>
                    </div> <!-- media -->
                    <?php } } ?>

                </div>
                <!-- col -->
            </div>
            <!-- row -->
        </div>
        <!-- interview-notes-comments -->


    </div>
</div>