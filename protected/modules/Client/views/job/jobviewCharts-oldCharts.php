<?php
	
	$startEndDate = UtilityManager::getStartAndEndDate(date('W'),date('Y'));
	
	$week = array();
	$month = array();
	$threeMonths = array();
	
	$jobID = $_GET['id'];
		
	for($i=0;$i<7;$i++){
		$week[] = date('Y-m-d',strtotime(date('Y-m-d') . " -".$i." days"));
		}
	for($i=0;$i<30;$i++){
		$month[] = date('Y-m-d',strtotime(date('Y-m-d') . " -".$i." days"));
		}
	for($i=0;$i<90;$i++){
		$threeMonths[] = date('Y-m-d',strtotime(date('Y-m-d') . " -".$i." days"));
		}
	
	$submissionData = VendorJobSubmission::model()->findAllByAttributes(array('job_id'=>$jobID));
	//first loop variables
	$weekDays = ''; $submissionTotal = ''; $interviewTotal = ''; $offerTotal = ''; $hiredTotal = '';
	
	//second loop variables
	$monthDays = ''; $submissionMonthTotal = ''; $interviewMonthTotal = ''; $offerMonthTotal = ''; $hiredMonthTotal = ''; 
	
	//third loop variables
	$TmonthDays = ''; $submissionTMonthTotal = ''; $interviewTMonthTotal = ''; $offerTMonthTotal = ''; $hiredTMonthTotal = ''; 
	
	//submission weekly counting chart data
	foreach($week as $exactDate){
		
			$submissionCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>array(3,4,5,6,7,8,9),'date_created'=>$exactDate));
			//$interviewCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>5,'date_created'=>$exactDate));
			$query = "SELECT id FROM vms_interview WHERE job_id=$jobID and DATE_FORMAT(interview_creation_date, '%Y-%m-%d') = '".$exactDate."' group by job_id";
			$interviewCount = Yii::app()->db->createCommand( $query )->query()->count();
			
			//$offerCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>7,'date_created'=>$exactDate));
			$query1 = "SELECT id FROM vms_offer WHERE job_id=$jobID and status IN(1,4) and DATE_FORMAT(date_created, '%Y-%m-%d') = '".$exactDate."'";
			$offerCount = Yii::app()->db->createCommand( $query1 )->query()->count();
			
			//$hiredCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>8,'date_created'=>$exactDate));
			$query2 = "SELECT id FROM vms_contract WHERE job_id=$jobID and DATE_FORMAT(date_created, '%Y-%m-%d') ='".$exactDate."'";
			$hiredCount = Yii::app()->db->createCommand( $query2 )->query()->count();
			
			$weekDays .= "'".date('D',strtotime($exactDate))."',";
			$submissionTotal .= $submissionCount.",";
			$interviewTotal .= $interviewCount.",";
			$offerTotal .= $offerCount.",";
			$hiredTotal .= $hiredCount.",";
		}
	//submission Monthly counting chart data
	foreach($month as $exactDate){
		
			$submissionMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>array(3,4,5,6,7,8,9),'date_created'=>$exactDate));
			//$interviewMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>5,'date_created'=>$exactDate));
			$query = "SELECT id FROM vms_interview WHERE job_id=$jobID and DATE_FORMAT(interview_creation_date, '%Y-%m-%d') = '".$exactDate."' group by job_id";
			$interviewMonthCount = Yii::app()->db->createCommand( $query )->query()->count();
			
			//$offerMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>7,'date_created'=>$exactDate));
			$query1 = "SELECT id FROM vms_offer WHERE job_id=$jobID and status IN(1,4) and DATE_FORMAT(date_created, '%Y-%m-%d') = '".$exactDate."'";
			$offerMonthCount = Yii::app()->db->createCommand( $query1 )->query()->count();
			
			//$hiredMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>8,'date_created'=>$exactDate));
			$query2 = "SELECT id FROM vms_contract WHERE job_id=$jobID and DATE_FORMAT(date_created, '%Y-%m-%d') ='".$exactDate."'";
			$hiredMonthCount = Yii::app()->db->createCommand( $query2 )->query()->count();
			
			
			$monthDays .= "'".date('D',strtotime($exactDate))."',";
			$submissionMonthTotal .= $submissionMonthCount.",";
			$interviewMonthTotal .= $interviewMonthCount.",";
			$offerMonthTotal .= $offerMonthCount.",";
			$hiredMonthTotal .= $hiredMonthCount.",";
		}
		
	//submission Three Months counting chart data
	foreach($threeMonths as $exactDate){
		
			$submissionTMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>array(3,4,5,6,7,8,9),'date_created'=>$exactDate));
			//$interviewTMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>5,'date_created'=>$exactDate));
			$query = "SELECT id FROM vms_interview WHERE job_id=$jobID and DATE_FORMAT(interview_creation_date, '%Y-%m-%d') = '".$exactDate."' group by job_id";
			$interviewTMonthCount = Yii::app()->db->createCommand( $query )->query()->count();
			
			//$offerTMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>7,'date_created'=>$exactDate));
			$query1 = "SELECT id FROM vms_offer WHERE job_id=$jobID and status IN(1,4) and DATE_FORMAT(date_created, '%Y-%m-%d') = '".$exactDate."'";
			$offerTMonthCount = Yii::app()->db->createCommand( $query1 )->query()->count();
			
			//$hiredTMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>8,'date_created'=>$exactDate));
			$query2 = "SELECT id FROM vms_contract WHERE job_id=$jobID and DATE_FORMAT(date_created, '%Y-%m-%d') ='".$exactDate."'";
			$hiredTMonthCount = Yii::app()->db->createCommand( $query2 )->query()->count();
			
			$TmonthDays .= "'".date('D',strtotime($exactDate))."',";
			$submissionTMonthTotal .= $submissionTMonthCount.",";
			$interviewTMonthTotal .= $interviewTMonthCount.",";
			$offerTMonthTotal .= $offerTMonthCount.",";
			$hiredTMonthTotal .= $hiredTMonthCount.",";
		}
?>

<div class="tab-content ">
  <div class="tab-pane active" id="one" role="tabpanel">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="simplify-tabs">
          <div role="tabpanel"> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs no-hover" role="tablist">
              <li class="nav-item"> <a class="nav-link active"  href="#day-7" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/report@512px.svg"></i> 7 Day</a> </li>
              <li class="nav-item"> <a class="nav-link "  href="#month" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/report@512px.svg"></i> Month</a> </li>
              <li class="nav-item"> <a class="nav-link "  href="#month-3" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/report@512px.svg"></i> 3 Month</a> </li>
            </ul>
            <div class="tab-content ">
              <div class="tab-pane active" id="day-7"  role="tabpanel">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <canvas id="Chartday-7" style="height:250px; width:250px;" ></canvas>
                  </div>
                </div>
              </div>
              <div class="tab-pane active" id="month"  role="tabpanel">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <canvas id="Chartmonth" style="height:250px; width:250px;" ></canvas>
                  </div>
                </div>
              </div>
              <div class="tab-pane active" id="month-3"  role="tabpanel">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <canvas id="Chartmonth-3" style="height:250px; width:250px;" ></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- tabpanel --> 
        </div>
        <!-- simplify-tabs -->
        <!--query for next 48 hour interview schedule -->
          <?php
			$todaydate = date('Y-m-d');
			$date = strtotime($todaydate);
			$date = strtotime("+2 day", $date);
			$tomorrowdate = date('Y-m-d', $date);

			$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".Yii::app()->user->id."' and s.job_id= '".$_GET['id']."'
			and ((w.interview_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.start_date_status =1) or (w.interview_alt1_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt2_date_status =1) or (w.interview_alt3_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt3_date_status =1))";

			$interview = Yii::app()->db->createCommand($sql)->query()->readAll();
			$numberofInterview = count($interview);
			?>
        <hr class="full-hr-30">
        <br>
        <p class="bold m-b-10">Interview Schedule ( For Next 48 hrs ) <span class="pull-right" style="color: #e64a33"><?php if($numberofInterview){ echo $numberofInterview; }else{ echo '0'; } ?> Interviews are Scheduled</span></p>
        <table class="table m-b-10 without-border">
          <thead class="thead-default">
            <tr>
              <th style="width: 10px;">Status</th>
              <th>Type</th>
              <th>Candidate Name</th>
              <th>Date</th>
              <th>Time In</th>
              <th>Time Out</th>
              <th>Location</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
           <?php
			if($interview){
			foreach($interview as $allData){
			$locationInterview = Location::model()->findByPk($allData['interview_where']);
			$candidateData = Candidates::model()->findByPk($allData['candidate_Id']);
			  $jobData = Job::model()->findByPk($allData['job_id']);
			  if($allData['start_date_status']){
				$date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
			  }

			  else if($allData['alt1_date_status']){
				$date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
			  }
			  else if($allData['alt2_date_status']){
				$date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
			  }
			  else if($allData['alt3_date_status']){
				$date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
			  }else{
				$date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
			  }

			  $newTime = explode(' ',$date);
				$postingstatus = UtilityManager::interviewStatus();
						switch ($postingstatus[$allData['status']]) {

							case "Approved":

								$color = 'label label-success';

								break;

							case "Waiting for Approval":

								$color = 'label-interview-pending';

								break;

							case "Cancelled":

								$color = 'label-interview-cancelled';

								break;

							case "Reschedule":

								$color = 'label-interview-reschedule';

								break;

					   case "Interview Completed":

								$color = 'label-interview-completed';

								break;

							default:

								$color = 'label-new-request';

						}
						?>
            <tr>
              <td><span class="tag <?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span></td>
              <td><?php echo $allData['interview_type']; ?></td>
              <td><?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?></td>
              <td><?php echo $newTime[0]; ?></td>
              <td><?php echo $newTime[1].' '.$newTime[2]; ?></td>
              <td><?php echo $newTime[4].' '.$newTime[5]; ?></td>
              <td><?php echo $locationInterview->name; ?></td>
              <td style="text-align: center"><a href="" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>
        <!--<p class="m-b-30"> <a href="" class="blue">More Interviews</a> </p>-->
        <br>
        <!--query for submission for next 24 hour  -->
        <?php
			$date_start = date('Y-m-d');
			$date = strtotime($todaydate);
			$date = strtotime("+2 day", $date);
			$date_end = date('Y-m-d', $date);
			$loginUserId = Yii::app()->user->id;
			$criteria = new CDbCriteria();
			$criteria->addCondition("job_id", $_GET['id'] And "client_id", $loginUserId);
			$criteria->addInCondition('resume_status',array(3,4,5,6,7,8,9));
			$criteria->addBetweenCondition("date_created",$date_start,$date_end,'AND');
			$submission = VendorJobSubmission::model()->findAll($criteria);
			$numberofnsubmission = count($submission);
		  ?>
        <p class="bold m-b-10">Submission ( Today Submission ) <span class="pull-right" style="color: #e64a33"><?php if($numberofnsubmission){ echo $numberofnsubmission; }else{ echo '0'; } ?> New Submission</span> </p>
        <table class="table m-b-40 without-border">
          <thead class="thead-default">
            <tr>
              <th  style="width: 10px;">Status</th>
              <th>Candidate Name</th>
              <th>Start Date</th>
              <th>Pay Rate</th>
              <th>Bill Rate</th>
              <th>Current Location</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
           <?php 										
				if($submission){
					foreach($submission as $value){
						$Jobsdata = Job::model()->findByPk($value->job_id);
						$candidates = Candidates::model()->findByPk($value->candidate_Id);
						$status = UtilityManager::resumeStatus();
				 switch ($status[$value->resume_status]) {

					 case "Submitted":

						 $color = 'label-hold';

						 break;

					 case "MSP Review":

						 $color = 'label-filled';

						 break;

					 case "MSP Shortlisted":

						 $color = 'label-new-request';

						 break;

					 case "Client Review":

						 $color = 'label-filled';

						 break;

					 case "Interview Process":

						 $color = 'label-open';

						 break;

					 case "Rejected":

						 $color = 'label-rejected';

						 break;

					 case "Offer":

						 $color = 'label-pending-aproval';

						 break;

					 case "Approved":

						 $color = 'label-new-request';

						 break;

					 case "Work Order Release":

						 $color = 'label-re-open';

						 break;

					 default:

						 $color = 'label-new-request';

				 }

				?>
            <tr>
              <td><span class="tag <?php echo $color; ?>"><?php echo $status[$value->resume_status]; ?></span></td>
              <td><?php echo $candidates->first_name.' '.$candidates->last_name; ?></td>
              <td><?php echo $value->date_created; ?></td>
              <td><?php echo $Jobsdata->pay_rate; ?></td>
              <td><?php echo $Jobsdata->bill_rate; ?></td>
              <td>Null</td>
              <td style="text-align: center"><a href="" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
      <br>
    </div>
  </div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		
       if ($('#Chartday-7').length) {
        var ctx = $('#Chartday-7');
        console.log(ctx); 
        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
           labels: [<?php echo $weekDays; ?>],
           
            datasets: [{
              label: 'Submission',
              backgroundColor: '#4CC3F0',
              data: [<?php echo $submissionTotal; ?>]
            },
            {
              label: 'Interview',
              backgroundColor: '#F0C54C',
              data: [<?php echo $interviewTotal; ?>]
            }, {
              label: 'Offer',
              backgroundColor: '#D26D54',
              data: [<?php echo $offerTotal; ?>]
            },
            {
              label: 'Hired',
              backgroundColor: '#7FC35C',
              data: [<?php echo $hiredTotal; ?>]
            }]
          }
        });
       };
       
       $('a[href="#month"]').on('shown.bs.tab', function (e) {
        console.log('month')
        if ( $('#Chartmonth').length ) {
         var ctx = $('#Chartmonth');
         console.log(ctx); 
         var myChart = new Chart(ctx, {
           type: 'bar',
           data: {
             labels: [<?php echo $monthDays; ?>],
             datasets: [{
               label: 'Submission',
               backgroundColor: '#4CC3F0',
               data: [<?php echo $submissionMonthTotal; ?>]
             }, {
               label: 'Interview',
               backgroundColor: '#F0C54C',
               data: [<?php echo $interviewMonthTotal; ?>]
             },
             {
               label: 'Offer',
               backgroundColor: '#D26D54',
               data: [<?php echo $offerMonthTotal; ?>]
             },
             {
               label: 'Hired',
               backgroundColor: '#7FC35C',
               data: [<?php echo $hiredMonthTotal; ?>]
             }]
           }
         });

        };
        
      })
      
       $('a[href="#month-3"]').on('shown.bs.tab', function (e) {
        if ($('#Chartmonth-3').length) {
         var ctx = $('#Chartmonth-3');
          console.log(ctx); 
          var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: [<?php echo $TmonthDays; ?>],
              datasets: [{
                label: 'Submission',
                backgroundColor: '#4CC3F0',
                data: [<?php echo $submissionTMonthTotal; ?>]
              },
              {
               label: 'Interview',
               backgroundColor: '#F0C54C',
               data: [<?php echo $interviewTMonthTotal; ?>]
             },{
                label: 'Offer',
                backgroundColor: '#D26D54',
                data: [<?php echo $offerTMonthTotal; ?>]
              },
              {
                label: 'Hired',
                backgroundColor: '#7FC35C',
                data: [<?php echo $hiredTMonthTotal; ?>]
              }]
            }
          });

        };
      })
	  });
</script>
