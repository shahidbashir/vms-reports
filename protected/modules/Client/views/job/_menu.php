
<?php
$this->pageTitle = 'Job Overview';
	if(isset($_GET['id'])){
		$jobID = $_GET['id'];
		}else{
			 $jobID = $_GET['jobid'];
			 }
	$Client = Client::model()->findByPk($model->user_subclient_id);

?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
<div class="cleafix " style="padding: 30px 20px 10px; ">
<?php if(Yii::app()->user->hasFlash('success')):
      echo Yii::app()->user->getFlash('success');
    endif; ?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h4 class="m-b-10 page-title"><?php echo $model->title;?> ( <?php echo $model->id;?> )
  <?php $closingdate = date('Y-m-d', strtotime('-4 day', strtotime($model->desired_start_date)));
        if($closingdate== date('Y-m-d')){
          ?>
  <i class="icon svg-fire title"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/fire@512px-red.svg"></i>
  <?php } ?>
  <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>" class="btn btn-sm btn-default-3 pull-right">Back Job Listing</a> </h4>
<?php
      $postingstatus = UtilityManager::jobStatus();
      switch ($postingstatus[$model->jobStatus]) {
        case "Pending Approval":
          $color = 'label-pending-aproval';
          break;
        case "Open":
          $color = 'label-open';
          break;
        case "Filled":
          $color = 'label-filled';
          break;
        case "Rejected":
          $color = 'label-rejected';
          break;
        case "Re-open":
          $color = 'label-re-open';
          break;
        case "Hold":
          $color = 'label-hold';
          break;
        case "New Request":
          $color = 'label-new-request';
          break;
        default:
          $color = 'label-new-request';
      }

      ?>
<p class="m-b-20"> <span class="tag <?php echo $color; ?>"><?php echo $postingstatus[$model->jobStatus]; ?> </span> Created by: <?php if($Client) echo $Client->first_name.' '.$Client->last_name; ?> on <?php echo date('m-d-Y',strtotime($model->date_created));?></p>
<div class="yellow-box">
  <p>
    <?php 
			$JobWorkflow = JobWorkflow::model()->findByAttributes(array('job_id'=>$jobID,'job_status'=>'Rejected'));
			if($JobWorkflow){
				$datetime = explode(' ',$JobWorkflow->status_time);
				$ClientData = Client::model()->findByPk($JobWorkflow->client_id);
				echo 'Reason of Rejection: '.$JobWorkflow->rejection_dropdown.'</br>';
				echo 'Notes: '.$JobWorkflow->rejection_reason.'</br>';
				echo 'Rjected By: '.$ClientData->first_name.' '.$ClientData->last_name.' at '.$datetime[0].' on '.$datetime[1];
			}else{
		$now = time(); // or your date as well
		$your_date = strtotime($model->desired_start_date);
		$datediff = $your_date - $now;
		
		$countDays = floor($datediff / (60 * 60 * 24)) + 1;
		if($countDays >= 0){
			echo $countDays.' More days left to close this job';
			}else{
				 echo 'Over Due — Please look into this job opening ASAP.';
				 }
				}
		//echo $model->desired_start_date; ?>
  </p>
</div>

<!-- below query execute for the rejected jobs which remove the botton -->
<?php $JobWorkflow = JobWorkflow::model()->findByAttributes(array('job_id'=>$jobID,'job_status'=>'Rejected'));
		if($JobWorkflow){  }else{
		?>
<div class="m-t-20"> 
<?php  
//don't show these button if the job is in draft mode
if($model->jobStatus != 2 && $model->jobStatus != 1){
?>
<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/interviewStep1'); ?>" class="btn btn-sm btn-success">Schedule a Interview</a> 
<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>" class="btn btn-sm btn-default-2">Create Offer </a> 
<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/workorderStep1'); ?>" class="btn btn-sm btn-default-2">Create Work Order </a> 
  <?php } ?>
  <!--for demo just commenting this link-->
  <?php if($model->jobStatus==1){ ?>
  <?php /*?><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/deletejob',array('id'=>$jobID)); ?>" class="btn btn-sm btn-danger">Cancel</a><?php */?>
  <?php } if($model->jobStatus==2){ ?>
  <p class="clearfix">
  <a data-toggle="modal" href='#edit-job' class="pull-right edit">
      <i class="fa fa-pencil"></i>
   </a> </p>
  <?php } /*?><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/editJob',array('id'=>$model->id)); ?>" class="pull-right edit"> <i class="fa fa-pencil"></i> </a> </div><?php */?>
<?php } ?>
<div class="simplify-tabs">
<div role="tabpanel">
<ul class="nav nav-tabs no-hover" role="tablist">
  
  <!-- below query executes for the rejected jobs which remove some tabs -->
  <?php $JobWorkflow = JobWorkflow::model()->findByAttributes(array('job_id'=>$jobID,'job_status'=>'Rejected'));
		if($JobWorkflow){
			//menu will be empty for rejected jobs
		}else{
			 
		//if job is pending for approval or in draft status then few tabs will be not there
		if($model->jobStatus==2 || $model->jobStatus==1){ 
			//will not these menus if the job is pending for approval or draft status.
		 }else{ ?>
          <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='overview'?'active':''; ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview',array('id'=>$jobID,'type'=>'overview')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-overview@512px.svg"></i>Overview</a> </li>
          <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='bill'?'active':''; ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/billMatchAsc',array('id'=>$jobID,'type'=>'bill')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title="Bill Match"> <i class=" nav-icon bill-match"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/billing@512px-2.svg"></i>Bill Match</a> </li>
          <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='submission'?'active':''; ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobsubmission',array('id'=>$jobID,'type'=>'submission','sub'=>0)); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title="Submission"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/submission@512px(1).svg"></i>Submission</a> </li>
          <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='interview'?'active':''; ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/allinterview',array('jobid'=>$jobID,'type'=>'interview','st'=>'all')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title="Interview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/interviews@512px.svg"></i>Interview</a> </li>
            <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='offer'?'active':''; ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/Offer',array('id'=>$jobID,'type'=>'offer')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title="Offer"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/jobs@512px.svg"></i>Offer</a> </li>
            <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='workorder'?'active':''; ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/workorderList',array('id'=>$jobID,'type'=>'workorder')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title="Work order"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/workorder@512px.svg"></i>Work Order</a> </li>

        <?php } } ?>
  <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='info'?'active':''; ?>"  href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview',array('id'=>$jobID,'type'=>'info')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title="Job Info"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/job-info-icons/job-detail@512px.svg"></i>Job Info</a> </li>
  
  <?php if($model->jobStatus != 2 && $model->jobStatus != 1){ ?>
  <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='workflow'?'active':''; ?>"  href="<?php echo $this->createAbsoluteUrl('/Client/job/workflowStatus',array('workflowid'=>$model->work_flow,'jobid'=>$jobID,'clientid'=>Yii::app()->user->id,'type'=>'workflow')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title=" Workflow Approval"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/work-flow-approval@512px.svg"></i> Approval</a> </li>
  <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='team'?'active':''; ?>"  href="<?php echo $this->createAbsoluteUrl('/Client/job/view',array('id'=>$jobID,'type'=>'team')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title=" Invite Team Members"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/invite-team-member@512px.svg"></i> Member</a> </li>
  
   <?php /*?><li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='jobnotes'?'active':''; ?>"  href="<?php echo $this->createAbsoluteUrl('/Client/job/jobNotes',array('id'=>$jobID,'type'=>'jobnotes')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title=" Notes & Comments"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Notes</a> </li>
   <?php */?>
   <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='spendBudget'?'active':''; ?>"  href="<?php echo $this->createAbsoluteUrl('/Client/job/spendBudget',array('id'=>$jobID,'type'=>'spendBudget')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title="Budget Spend"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/offers@512px.svg"></i> Budget</a> </li>

  <?php }else{ 
  if($model->work_flow != ''){
  ?>
  
  <li class="nav-item"> <a class="nav-link <?php echo !empty($_GET['type']) && $_GET['type'] =='workflow'?'active':''; ?>"  href="<?php echo $this->createAbsoluteUrl('/Client/job/workflowStatus',array('workflowid'=>$model->work_flow,'jobid'=>$jobID,'clientid'=>Yii::app()->user->id,'type'=>'workflow')); ?>" role="tab" data-toggle="tooltip" data-placement="bottom" title=" Workflow Approval"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/work-flow-approval@512px.svg"></i> Workflow Approval</a> </li>
  
  <?php } } ?>
  
  
  
  
</ul>

<div class="modal fade" id="edit-job">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Edit Job</h4>
      </div>
      <div class="modal-body">
        <div class="edit-job-sections">
          <div class="edit-job-box text-center">
            <a  class="btn btn-default-2" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobEdit/editDetails',array('id'=>$model->id)); ?>">Job Details</a>
            <a  class="btn btn-default-2" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobEdit/editSkills',array('id'=>$model->id)); ?>">Job Skill</a>
            <a  class="btn btn-default-2" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobEdit/editInternalDetails',array('id'=>$model->id)); ?>">Job Internal Details</a>
          </div>
          
          <div class="edit-job-box text-center">
            <a  class="btn btn-default-2" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobEdit/editOtherInfo',array('id'=>$model->id)); ?>">Other Info</a>
            <?php /*?><a  class="btn btn-default-2" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobEdit/editRates',array('id'=>$model->id)); ?>">Job Rate</a><?php */?>
          </div>
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
      </div>
    </div>
  </div>
</div>
