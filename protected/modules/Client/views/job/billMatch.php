<?php 
 
 $this->pageTitle = 'Bill Match';
 $categoryID = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$jobmodel->cat_id)); 
		
//Administrative/Clerical Jobs category id 71
//client id 41
$rateData = VendorClientMarkup::model()->findAllByAttributes(array('client_id'=>Yii::app()->user->id,'category_id'=>$categoryID->id), 
	array( 
		'order' => 'mark_up desc', 
		'limit' => '1' 
	));

		 
if($rateData){ 
	$mark_up = $rateData[0]->mark_up; 
	}else{ 
		 $mark_up = 0; 
		 } 


	//$dates = explode('-',$jobmodel->job_po_duration);
	$hours = explode(' ',$jobmodel->hours_per_week);
	
	$hoursPerDay = $hours[0]/5;
	
	$startDate = date('Y-m-d',strtotime($jobmodel->job_po_duration));
	$endDate = date('Y-m-d',strtotime($jobmodel->job_po_duration_endDate));
	$workingDays = 0;
	 
	$startTimestamp = strtotime($startDate);
	$endTimestamp = strtotime($endDate);
	 
	for($i=$startTimestamp; $i<=$endTimestamp; $i = $i+(60*60*24) ){
		if(date("N",$i) <= 5) $workingDays = $workingDays + 1;
	}
	$hours = $workingDays * $hoursPerDay;
	
	$this->renderPartial('_menu',array('model'=>$jobmodel));
?>


<div class="tab-content ">
    <div class="tab-pane active" id="one" role="tabpanel">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">



                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group m-t-40 slight">

                            <select name="payrate_order" id="sort_by_payrate" class="form-control" required="required">
                                <option value="asc" >Sort By Lowest Bill Rate</option>
                                <option value="desc" <?php echo $this->action->id=='billMatchDesc'?'selected':''; ?>>Sort By Highest Bill rate</option>
                            </select>
                            <input type="hidden" id="job_id" value="<?php echo $_GET['id']; ?>" />
                        </div>
                    </div>
                </div>




                <br>
                <?php if($this->action->id=='billMatchDesc'){?>
                    <p class="m-b-10 bold">Highest Bill Rate</p>
                <?php }else{ ?>
                    <p class="m-b-10 bold">Lowest Bill Rate</p>
                <?php } ?>
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>

                        <th style="width: 10px;">Sr.No:</th>
                        <th>Candidate Name</th>
                        <th>Location</th>
                        <th>Start Rate</th>
                        <th>Bill Rate</th>
                        <th>Pay Rate</th>
                        <th>Job Cost</th>
                        <th>Submission Cost</th>
                        <th>Saved Cost</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach($submissionData as $key=>$values) {
                    $i++;
                    $Candidates = Candidates::model()->findByPk($values['candidate_Id']);
                    $Job = Job::model()->findByPk($values['job_id']);
                    $vendor = Vendor::model()->findByPk($values['vendor_id']);
                        $mark_up = $values['makrup'];
                    //$location = Location::model()->findByPk($values['interview_where']);
                    //echo $mark_up;
                    $billrate = $values['candidate_pay_rate'] + $values['candidate_pay_rate'] * ($mark_up)/100;
                        if($i==1){
                    ?>
                    <tr>

                        <td><?php echo $key + 1;?></td>
                        <td><?php echo $Candidates->first_name.' '.$Candidates->last_name;?></td>
                        <td><?php echo $Candidates->current_location; ?></td>
                        <td><?php echo $values['estimate_start_date']; ?></td>
                        <td>$<?php echo $billrate; ?></td>
                        <td>$<?php echo $values['candidate_pay_rate']; ?></td>
                        <td>$<?php echo $Job->pre_total_estimate_code; ?></td>
                        <td>$<?php echo $hours*$billrate; ?></td>
                        <td>$<?php echo $Job->pre_total_estimate_code - $hours*$billrate; ?></td>
                        <td style="text-align: center">
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$values['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                        </td>
                    </tr>
                    <?php } } ?>
                    </tbody>
                </table>

                <?php if($this->action->id=='billMatchDesc'){?>
                    <p class="m-b-10 bold">Second Highest Bill Rate</p>
                <?php }else{ ?>
                    <p class="m-b-10 bold">Second Lowest Bill Rate</p>
                <?php } ?>
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>

                        <th style="width: 10px;">Sr.No:</th>
                        <th>Candidate Name</th>
                        <th>Location</th>
                        <th>Start Rate</th>
                        <th>Bill Rate</th>
                        <th>Pay Rate</th>
                        <th>Job Cost</th>
                        <th>Submission Cost</th>
                        <th>Saved Cost</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach($submissionData as $key=>$values) {
                        $i++;
                        $Candidates = Candidates::model()->findByPk($values['candidate_Id']);
                        $Job = Job::model()->findByPk($values['job_id']);
                        $vendor = Vendor::model()->findByPk($values['vendor_id']);

                        //$location = Location::model()->findByPk($values['interview_where']);

                        $billrate = $values['candidate_pay_rate'] + $values['candidate_pay_rate'] * ($mark_up)/100;
                        if($i==2){
                            ?>
                            <tr>

                                <td><?php echo $key + 1;?></td>
                                <td><?php echo $Candidates->first_name.' '.$Candidates->last_name;?></td>
                                <td><?php echo $Candidates->current_location; ?></td>
                                <td><?php echo $values['estimate_start_date']; ?></td>
                                <td>$<?php echo $billrate; ?></td>
                                <td>$<?php echo $values['candidate_pay_rate']; ?></td>
                                <td>$<?php echo $Job->pre_total_estimate_code; ?></td>
                                <td>$<?php echo $hours*$billrate; ?></td>
                                <td>$<?php echo $Job->pre_total_estimate_code - $hours*$billrate; ?></td>
                                <td style="text-align: center">
                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$values['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                                </td>
                            </tr>
                        <?php } } ?>
                    </tbody>
                </table>

                <p class="m-b-10 bold">Other Bill Rate</p>
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>

                        <th style="width: 10px;">Sr.No:</th>
                        <th>Candidate Name</th>
                        <th>Location</th>
                        <th>Start Rate</th>
                        <th>Bill Rate</th>
                        <th>Pay Rate</th>
                        <th>Job Cost</th>
                        <th>Submission Cost</th>
                        <th>Saved Cost</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach($submissionData as $key=>$values) {
                        $i++;
                        $Candidates = Candidates::model()->findByPk($values['candidate_Id']);
                        $Job = Job::model()->findByPk($values['job_id']);
                        $vendor = Vendor::model()->findByPk($values['vendor_id']);

                        //$location = Location::model()->findByPk($values['interview_where']);

                        $billrate = $values['candidate_pay_rate'] + $values['candidate_pay_rate'] * ($mark_up)/100;
                        if($i>=3){
                            ?>
                            <tr>

                                <td><?php echo $key + 1;?></td>
                                <td><?php echo $Candidates->first_name.' '.$Candidates->last_name;?></td>
                                <td><?php echo $Candidates->current_location; ?></td>
                                <td><?php echo $values['estimate_start_date']; ?></td>
                                <td>$<?php echo $billrate; ?></td>
                                <td>$<?php echo $values['candidate_pay_rate']; ?></td>
                                <td>$<?php echo $Job->pre_total_estimate_code; ?></td>
                                <td>$<?php echo $hours*$billrate; ?></td>
                                <td>$<?php echo $Job->pre_total_estimate_code - $hours*$billrate; ?></td>
                                <td style="text-align: center">
                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$values['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                                </td>
                            </tr>
                        <?php } } ?>
                    </tbody>
                </table>


                <!--<div class="row m-b-10" style="padding: 10px 0 10px; ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <ul class="pagination m-t-0">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <p class="text-right">
                            Showing 10 to 20 of 50 entries
                        </p>
                    </div>
                </div>--> <!-- row -->


            </div>

            <br>
        </div>
    </div>
</div>
</div>
</div>
<!-- col -->
</div>
<!-- row -->




</div>
</div>


