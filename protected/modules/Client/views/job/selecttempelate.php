<?php
$this->pageTitle = 'Template "'.$jobTempelatemodel->job_title.'"';


$loginUserId = Yii::app()->user->id;
$clientID = UtilityManager::superClient($loginUserId);

$ClientRate = ClientRate::model()->findByAttributes(array('client_id'=>$clientID));
$setting = Setting::model()->findByPk($jobTempelatemodel->cat_id);

/*echo '<pre>';
print_r($ClientRate);
exit;*/
?>
<?php
function select_options($selected = array()){
  $output = '';
  foreach(json_decode(file_get_contents(Yii::app()->basePath.'/names.json'), true) as $item){
    $output.= '<option value="' . $item['value'] . '"' . (in_array($item['value'], $selected) ? 'selected' : '') . '>' . $item['text'] . '</option>';
  }
  return $output;
}
?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">

  <div class="cleafix " style="padding: 30px 20px; ">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Add Job
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>" class="btn btn-sm btn-default-3 pull-right">Back to Job List</a>
      </h4>
      <p class="m-b-40">Following are the instruction to create a job requisition.</p>

      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'job-form',
          'enableAjaxValidation'=>false,
          'action'=>$this->createAbsoluteUrl('/Client/job/jobStep1')
      ));
	  
	  echo $form->errorSummary($model);
	  
      $loginUserId = Yii::app()->user->id;
      ?>
      <input type="hidden" name="template_id" value="<?php echo $jobTempelatemodel->id; ?>"  />
      <div class="row add-job">
        <div class="col-md-12">
          <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-heading"></div>
            <div class="panel-body" style="overflow-y: inherit;">
            
            <?php 
			$ClientInfo = Client::model()->findByPk($clientID);
			if($ClientInfo->client_job_status == 'Yes'){ 
				$dynamicClass = 'col-md-4 col-lg-4';
			}else{
				 $dynamicClass = 'col-md-6 col-lg-6'; 
				 }
			?>
            
              <div class="row">
                <div class="col-xs-12 col-sm-4 <?php echo $dynamicClass ?>">
                  <div class="form-group">
                    <label for="">Job Title</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php echo $form->textField($model,'title',array('class'=>'form-control','value'=>$jobTempelatemodel->job_title,'required'=>'required')); ?> <?php echo $form->error($model,'title'); ?>
                </div>
                </div>

                <!-- col -->
                <div class="col-xs-12 col-sm-4 <?php echo $dynamicClass ?>">
                  <div class="form-group">
                    <label for="">Job Type</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=11')),'title', 'title');
                    echo $form->dropDownList($model, 'type', $list , array('class'=>'form-control select2','empty' => '','required'=>'required'));
                    ?>
                  </div>
                </div>
                <!-- col -->
                <?php 
                if($ClientInfo->client_job_status=='Yes'){
                  ?>
                  <div class="col-xs-12 col-sm-4 <?php echo $dynamicClass ?>">
                    <div class="form-group">
                      <label for="">Client Name</label>
                      <i class="required-field fa fa-asterisk"></i>
                      <?php
                      $list = CHtml::listData(ProClientNames::model()->findAll(array('condition'=>'main_client='.$clientID)),'id', 'name');
                      echo $form->dropDownList($model, 'client_name', $list , array('class'=>'form-control select2','empty' => '','required'=>'required'));
                      ?>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <!-- row -->
              <div class="row">
                
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Category</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'title', 'title');
                    echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'form-control select2','required'=>'required','options' => array($setting->title=>array('selected'=>true)))); ?>
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Cost Center code</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <select  id="cost_center_code" class="form-control" name="Job[cost_center_code]" required >
                      <option value=""></option>
                    </select>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Experiences</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=10')),'title', 'title');
                    echo $form->dropDownList($model, 'experience', $list , array('class'=>'form-control select2','required'=>'required','options' => array($jobTempelatemodel->experience=>array('selected'=>true)))); ?>
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Job Level</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=40')),'title', 'title');
                    echo $form->dropDownList($model, 'job_level', $list , array('class'=>'form-control select2','required'=>'required','options' => array($jobTempelatemodel->job_level=>array('selected'=>true)))); ?>
                  </div>
                </div>
                <!-- col -->
              </div>
              <!-- row -->

              <br>

              <!-- row -->
              <div class="hirring-pipline">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      <label for="">Primary Skill</label>
                      <?php $primary_skills = explode(',',str_replace(' ','',$jobTempelatemodel->primary_skills)) ?>
                      <select class="tokenize-remote-demo1" name="Job[skills][]" id="job_skills" multiple>
                        <?php //print_r($jobTempelatemodel->primary_skills[0]);//echo select_options($primary_skills); ?>
                        <?php
                        foreach($primary_skills as $value){
                          echo '<option selected="selected" value="'.$value.'">'.$value.'</option>';
                        }
                        ?>

                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Secondary Skill</label>
                      <?php $secondary_skills = explode(',',str_replace(' ','',$jobTempelatemodel->secondary_skills)) ?>
                      <select class="tokenize-remote-demo1" name="Job[skills1][]" id="job_skills1"  multiple>
                        <?php //echo select_options($secondary_skills); ?>
                        <?php
                        foreach($secondary_skills as $value1){
                          echo '<option selected="selected" value="'.$value1.'">'.$value1.'</option>';
                        }
                        ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Good to have.</label>
                      <?php $good_to_have = explode(',',str_replace(' ','',$jobTempelatemodel->good_to_have)) ?>
                      <select class="tokenize-remote-demo1" name="Job[skills2][]" id="job_skills2" multiple>
                        <?php //echo select_options($good_to_have); ?>
                        <?php
                        foreach($good_to_have as $value2){
                          echo '<option selected="selected" value="'.$value2.'">'.$value2.'</option>';
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <!-- col-12 -->
                </div>
                <!-- row -->
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for="">Note for Skills</label>
                      <textarea name="Job[skills_notes]" id="input" class="form-control" rows="5" required="required"><?php echo $jobTempelatemodel->notes_for_skills; ?></textarea>
                    </div>
                  </div>
                </div>
                <!-- row -->
              </div>

              <br>

              <!-- hirring-pipline -->
              <div class="search-box-only-border m-b-20">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel-group" id="accordion">
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed">
                              Job Description</a><i class="required-field fa fa-asterisk"></i>
                          </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="Job[description]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $jobTempelatemodel->job_description; ?></textarea>>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false">
                              You Will</a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="Job[you_will]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $jobTempelatemodel->you_will; ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false">
                              Qualifications</a>
                          </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="Job[qualification]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $jobTempelatemodel->qualification; ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info m-b-0">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed" aria-expanded="false">
                              Additional Information</a>
                          </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="Job[add_info]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $jobTempelatemodel->add_info; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- col-12 -->
                </div>
                <!-- row -->
              </div>
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <label for="">Location</label>
                  <i class="required-field fa fa-asterisk"></i>
                  <div class="form-group">
                    <select  multiple="" class="select2" name="Job[location][]" required>
                      <option value=""></option>
                      <?php
                      $location = Location::model()->findAllByAttributes(array('reference_id'=>$clientID));
                      foreach($location as $location){ ?>
                        <option value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                      <?php  } 	?>
                    </select>
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Number of Openings</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php echo $form->numberField($model,'num_openings',array('class'=>'form-control','min'=>'0','required'=>"required")); ?>
                  </div>
                </div>
                <!-- col -->
              </div>
              <!-- row -->


              <br>
              <input type="hidden" name="Job[vendor_client_markup]" id="vendor_client_markup" class="form-control" value="">
              <div class="row">

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group">
                    <label for="">Estimated Job Start Date</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <input type="text" class="form-control singledatepicker readonly2" readonly="readonly" name="job_po_duration" id="daterange" placeholder=""  required="required">
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group">
                    <label for="">Job End Date</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <input type="text" class="form-control singledatepicker readonly2" readonly="readonly" name="job_po_duration_endDate" id="daterange1" placeholder=""  required="required">
                    <?php //echo Yii::app()->user->getFlash('error123'); ?>
                    <?php echo $form->error($model,'job_po_duration_endDate'); ?> 
                  </div>
                </div>
                <!-- col -->

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                  <div class="form-group">
                    <label for="">Target Closing Date for Job</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <input type="text" class="form-control singledatepicker readonly2" readonly="readonly" name="desired_start_date" id="dateField" placeholder="" required="required">
                  </div>

                </div>
                <!-- col -->
              </div>
              <!-- row -->
              <hr>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <label for="">Invite your Team member to Collaborate</label>
                  <div class="form-group">
                    <select multiple="" class="select2" name="Job[invite_team_member][]">
                      <option value=""></option>
                      <?php
                      $friend = Client::model()->findAllByAttributes(array('super_client_id'=>$clientID));
                      foreach($friend as $friend){ ?>
                        <option value="<?php echo $friend->id; ?>"><?php echo $friend->first_name.' ('.$friend->email.')'; ?></option>
                      <?php  } 	?>
                    </select>

                  </div>
                </div>
                <!-- col -->
              </div>
              <!-- row -->

              <br>

              <div class="well">
                <h4> Pre Identity Candidate </h4>
                <br>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for="">Do you have Pre Identity Candidate</label>
                      <select name="Job[pre_candidate]" id="pre_candidate" class="form-control">
                        <option value=""></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="">&nbsp;</label>
                    <div class="row">
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <input type="text" name="Job[pre_name]" class="form-control" id="pre_name" placeholder="Candidate Name">
                        </div>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <input type="text" name="Job[pre_supplier_name]" class="form-control" id="pre_supplier_name" placeholder="Supplier Name">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- row -->
                <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for="">Current Rate</label>
                      <input type="text" class="form-control" id="pre_current_rate"  name="Job[pre_current_rate]">
                    </div>
                  </div>
                  <!-- col -->
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for=""> Payment Type</label>
                      <?php
                      $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                      echo $form->dropDownList($model, 'pre_payment_type', $list , array('id'=>'pre_payment_type','class'=>'form-control select2 pre_fields','empty' => '','required'=>'required')); //,'disabled'=>true ?>
                    </div>
                  </div>
                  <!-- col -->
                </div>
                <!-- row -->
              </div>

              <br>
              <div>
                <button type="submit" id="job-submit" class="btn btn-success">Save</button>
                <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>" class="btn btn-default">Cancel</a>
              </div>
              <br>
              <br>
            </div>
            <!-- panel-body -->
          </div>
        </div>
      </div>
      <?php $this->endWidget(); ?>
    </div>
    <!-- col -->

  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>

</div>
<style>
.tokenize-dropdown > .dropdown-menu li > a  {
    display: block !important;
}
.readonly2{
	background:#fff !important;
	}
.errorMessage, .errorSummary{
	color:red;
	}
</style>