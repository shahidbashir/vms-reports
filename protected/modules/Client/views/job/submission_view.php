
<div class="row bg-title">
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
<h4 class="page-title">Submission ID : <?php echo $vendorSubmission->id;?></h4> 
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobsubmission',array('id'=>$vendorSubmission->job_id,'type'=>'submission','sub'=>0)); ?>" class="btn btn-xs btn-default">Back to Submission</a>
</div>  
<!-- /.col-lg-12 -->
</div> <!-- row -->  

<div class="client-workspace"> 


 <?php if(Yii::app()->user->hasFlash('success')):
           echo Yii::app()->user->getFlash('success');
        endif; ?>

<div class="row"> 
<div class="col-md-12">
<div class="white-box interview-view">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class=" media user-info" style="overflow: visible;">
   
   <a class="pull-left" href="#">
      <img class="media-object" src="<?php echo Yii::app()->baseUrl;?>/theme-assets/plugins/images/users/govinda.jpg" alt="Image">
   </a>
   
   <div class="media-body p-t-10" style="overflow: visible;">
      <div class="row">
      	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
      		<h4 class="media-heading"><?php echo $candidateModel->first_name.' '. $candidateModel->last_name;?> </h4>	
      		<p>
      			Current Location : <?php echo $candidateModel->current_location;?>
      		</p>
      	</div> <!-- col -->
      	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
      		<div class="job-submision-update">
		      		<div class="two-flieds">
						  <?php $form=$this->beginWidget('CActiveForm', array(
                         'id'=>'vendor-form',
                         'enableAjaxValidation'=>false,
                     )); ?>
        <div class="form-group">
            <div class="single">
	<?php 
      //$list= array('Submitted'=>'Submitted','MSP Review'=>'MSP Review','MSP Shortlisted'=>'MSP Shortlisted','Client Review'=>'Client Review','Interview Process'=>'Interview Process','Rejected'=>'Rejected','Offer'=>'Offer');
	  
	  $list= UtilityManager::resumeStatus();
                 

		echo $form->dropDownList($vendorSubmission,'resume_status', $list,
	  array('empty' => 'Select Account Manager','class'=>'ui fluid search dropdown','options' => array($vendorSubmission->resume_status=>array('selected'=>true)))); ?>
		<?php echo $form->error($vendorSubmission,'resume_status'); ?>
            </div>
        </div>
            <div class="form-group text-left">
                <input type="submit" value="Update" class="btn btn-xs btn-success" style="" />
            </div>

						 <?php $this->endWidget(); ?>
		      		</div>
      		</div>
      	</div> <!-- col -->
      </div> <!-- row -->
      
   </div>



</div> <!-- media -->

<br>

</div> <!-- col -->
</div> <!-- row -->

<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

 <div class="panel panel-inverse">
       
   <div class="panel-heading">Vendor Information</div>

  <div class="panel-wrapper collapse in" aria-expanded="true">
     
     <div class="panel-body">
       
       <div class="row">
       	
       	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       		<p>Account Manager : <?php echo $vendor->first_name.' '.$vendor->last_name;?></p>
       	</div> <!-- col -->

       	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       		<p>Team Member : <?php 

       		if($vendorTeam) { echo $vendorTeam->first_name.' '.$vendorTeam->last_name;}
       		else { echo 'NULL';}?></p>
       	</div> <!-- col -->


       </div> <!-- row -->

     </div> <!-- panel-body -->

   </div>

</div> <!-- panel -->


 <div class="panel panel-inverse">
       
   <div class="panel-heading"> Candidate Inforamation

   </div>

  <div class="panel-wrapper collapse in" aria-expanded="true">
     
     <div class="panel-body">
       
       <div class="row">
       	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
       		<p><?php echo $candidateModel->first_name;?>  <?php echo $candidateModel->last_name;?></p>
       	</div> <!-- col -->

       	


       	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
       		<p>Current location : <?php echo $candidateModel->current_location;?></p>
       	</div> <!-- col -->

       	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
       		<p>Willing to Relocated : <?php echo $candidateModel->willing_relocate;?></p>
       	</div> <!-- col -->

       	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
       		<p>Gender : <?php echo $candidateModel->gender;?></p>
       	</div> <!-- col -->


       </div> <!-- row -->
				
     </div> <!-- panel-body -->

   </div>

</div> <!-- panel -->


 <div class="panel panel-inverse"> 
       
   <div class="panel-heading"> Markup & Rules

   </div>

  <div class="panel-wrapper collapse in" aria-expanded="true">
     
     <div class="panel-body">
       
       <?php 
	     $ClientRate = ClientRate::model()->findByAttributes(array('client_id'=>$jobModel->user_id));
 
		$rateData = ClientRate::model()->findAllByAttributes(array('category'=>$jobModel->cat_id,'client_id'=>$jobModel->user_id),
		array(
			'order' => 'mark_up_percentage desc',
			'limit' => '1'
		));
				
		if($rateData){
			$mark_up_percentage = $rateData[0]->mark_up_percentage;
			}else{
				 $mark_up_percentage = 0;
				 }
				
		$billRate = 0;
		$markUp = 0;
		$payRate = 0; 
		if($ClientRate){
			if($ClientRate->type == 'Pay Rate'){
				 $estimatedCost = ($jobModel->pre_total_estimate_code/* + $jobModel->markup*/).' '.$jobModel->currency;			 
				 $billRate = ($jobModel->pre_total_estimate_code).' '.$jobModel->currency;
				 $markUp = $jobModel->pay_rate_candidate_value.' '.$jobModel->currency;
				 $payRate = ($jobModel->pay_rate_candidate - $jobModel->pay_rate_candidate_value).' '.$jobModel->currency;
				 
				 }else if($ClientRate->type == 'Bill Rate'){
					 $estimatedCost = ($jobModel->pre_total_estimate_code/* + $jobModel->markup*/).' '.$jobModel->currency;					
					$billRate = ($jobModel->pre_total_estimate_code + $jobModel->pay_rate_candidate_value).' '.$jobModel->currency;
					$markUp = $jobModel->pay_rate_candidate_value.' '.$jobModel->currency;
					$payRate = ($jobModel->rate - $jobModel->pay_rate_candidate_value).' '.$jobModel->currency;
					
					}else if($ClientRate->type == 'Markup'){
						
						$estimatedCost = ($jobModel->pre_total_estimate_code/* + $jobModel->markup*/).' '.$jobModel->currency;
						$billRate = ($jobModel->rate + $jobModel->pay_rate_candidate_value).' '.$jobModel->currency.' '.$jobModel->payment_type;
						$payRate = ($jobModel->pay_rate_candidate_value) .' '.$jobModel->currency.' '.$jobModel->payment_type;
						
						$markUp = ($jobModel->rate - $jobModel->pay_rate_candidate_value) .' '.$jobModel->currency.' ('.$mark_up_percentage.'%)';
						
						}
			}?>
       
		<div class="row">
        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<p>Total Estimated Cost :
				<?php echo $estimatedCost;?> </p>
			</div> <!-- col -->
            
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<p>Bill Rate :
				<?php echo $billRate;?> </p>
			</div> <!-- col -->

			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<p>Markup : <?php echo $markUp;?> </p>
			</div>

			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<p>PayRate : <?php echo $payRate;?> </p>
			</div>

			


		</div> <!-- row -->

       
     </div> <!-- panel-body -->

   </div>

</div> <!-- panel -->

 <div class="panel panel-inverse"> 
       
   <div class="panel-heading"> Rate Submitted by Vendor Name

   </div>

  <div class="panel-wrapper collapse in" aria-expanded="true">
     
     <div class="panel-body">
       
       
		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<p>Pay Rate : <?php echo "$".$vendorSubmission->pay_rate;?></p>
			</div> <!-- col -->

			

			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<p>Total Budget : 0<?php //echo "$".$vendorSubmission->vendor_estimate_cost;?></p>
			</div>

		</div> <!-- row -->

		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<p class="m-0">Resume Download</p>
				<p class="m-0"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/downloadResume',array('id'=>$candidatesProfile->id,'type'=>'profile')); ?>" class="underline"><?php echo $candidatesProfile->resume;?></a></p>
			</div> <!-- col -->

			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<p>Start Date : <?php echo $vendorSubmission->estimate_start_date;?></p>
			</div>

			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				
			</div>

			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				
			</div>

		</div> <!-- row -->

       
     </div> <!-- panel-body -->

   </div>

</div> <!-- panel -->

<div class="row">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="panel panel-inverse">
        
        <div class="panel-heading"> Reference Notes
         
          <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>

        </div>
       
       <div class="panel-wrapper collapse in" aria-expanded="true">
          
          <div class="panel-body">
            <div class="form-group">
               <textarea name="" id="input" class="form-control" rows="6" required="required"><?php echo $vendorSubmission->client_note;?></textarea>
            </div>
          </div> <!-- panel-body -->

        </div>

      </div> <!-- panel -->


      <div class="panel panel-inverse">
        
        <div class="panel-heading"> Reference Notes for MSP
         
          <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>

        </div>
       
       <div class="panel-wrapper collapse in" aria-expanded="true">
          
          <div class="panel-body">
            <div class="form-group">
               <textarea name="" id="input" class="form-control" rows="6" required="required"><?php echo $vendorSubmission->MSP_note;?></textarea>
            </div>
          </div> <!-- panel-body -->

        </div>

      </div> <!-- panel -->


      <div class="panel panel-inverse">
        
        <div class="panel-heading"> Reference Notes for Vendor
         
          <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>

        </div>
       
       <div class="panel-wrapper collapse in" aria-expanded="true">
          
          <div class="panel-body">
            <div class="form-group">
               <textarea name="" id="input" class="form-control" rows="6" required="required"><?php echo $vendorSubmission->vendor_note;?></textarea>
            </div>
          </div> <!-- panel-body -->

        </div>

      </div> <!-- panel -->
     
   </div>
   <!-- col -->
</div>
<!-- row -->

<br>
<br><br>

<br>
<br><br>

</div> <!-- col -->
</div> <!-- row -->

</div>
</div>
</div>




</div> <!-- client-workspace -->
