<?php 

	$this->pageTitle = 'New Appointment';

	$jobId = '';

	if(isset($_GET['jobId'])){

		$jobId = $_GET['jobId'];

		}

	

?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                     
                     <div class="cleafix " style="padding: 30px 20px; ">  
                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <h4 class="m-b-10">Create a New Appointment.</h4>
                           <p class="m-b-40">Schedule the appointment for the candidate for interview.</p>
                           
                          <form method="post">
                          <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                              
                              <div class="form-group form-control-2">
                                  <label for="" style="text-align: left;">Select Job</label>
                                  <select name="job_id" id="Interview_jobs" class="form-control  select2" required="required">
                                     <option value="">Select</option>
                                     <?php foreach($jobData as $key=>$value){ ?>

                      				<option value="<?php echo $value->id ?>" <?php //echo ($value->id==$jobId)?'selected':''; ?>><?php echo '('.$value->id.') '.$value->title ?></option>

                      				<?php } ?>
                                  </select>
                               </div>

                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                              <div class="form-group ">
                                  <label for="" style="text-align: left;">
                                     <p>Select Candidate</p>
                                  </label>
                                  <select name="submission_id" id="candidates" class="form-control  select2" required="required">
									  <option value="">Select</option>
                                  </select>
                               </div>
                            </div>

                          </div>
							  <input type="submit" class="btn btn-success" name="first-step" value="Continue"></input>
							</form>

                        </div>
                        <!-- col -->

                     </div>
                     <!-- row -->
                     <div class="seprater-bottom-100"></div>

                  </div>