<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td1 = tr[i].getElementsByTagName("td")[1];
            td2 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            td5 = tr[i].getElementsByTagName("td")[5];
            td6 = tr[i].getElementsByTagName("td")[6];
            if (td || td1 || td2 || td3 || td4 || td5 || td6) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                }else if(td1.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td2.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td3.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td4.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td5.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td6.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }
                else {
                    tr[i].style.display = "none";
                }

            }
        }
    }
</script>
<?php $this->renderPartial('_menu',array('model'=>$jobmodel));?>
<?php if(isset($_GET['sub'])){ ?>
<div class="tab-content ">
  <div class="tab-pane active" id="one" role="tabpanel">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="">
          <div class="two-fields">
            <div class="form-group">
              <label for=""></label>
              <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search by keyword">
            </div>
            <div class="form-group">
              <label for="">&nbsp;</label>
              <button type="button" class="btn btn-primary btn-block">Search</button>
            </div>
          </div>
          <!-- two-flieds --> 
          
        </div>
        <br>
        <p class="bold m-b-20"> <a href="" class="pull-right"> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/listing@512px.svg"></i> </a> <a href="" class="pull-right" style="margin-right: 10px; "> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/menu@512px.svg"></i> </a> </p>
        <br>
        <table id="myTable" class="table m-b-40 without-border">
          <thead class="thead-default">
            <tr>
              <th  style="width: 10px;">Status</th>
              <th> Submission Id </th>
              <th>Candidate Name</th>
              <th>Start Date</th>
              <th>Pay Rate</th>
              <th>Bill Rate</th>
              <th style="width: 157px;">Current Location</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
	foreach($submissionData as $key=>$values) {
		$Candidates = Candidates::model()->findByPk($values['candidate_Id']);
        $mark_up = $values['makrup'];
        $billrate = $values['candidate_pay_rate'] + $values['candidate_pay_rate'] * ($mark_up)/100;
		$Job = Job::model()->findByPk($values['job_id']);
		$vendor = Vendor::model()->findByPk($values['vendor_id']);
		$location = Location::model()->findByPk($values['interview_where']);
		$status = UtilityManager::resumeStatus();
		switch ($status[$values['resume_status']]) {
                            case "Submitted":
                                $color = 'label-hold';
                                break;
                            case "MSP Review":
                                $color = 'label-filled';
                                break;
                            case "MSP Shortlisted":
                                $color = 'label-new-request';
                                break;
                            case "Client Review":
                                $color = 'label-filled';
                                break;
                            case "Interview Process":
                                $color = 'label-open';
                                break;
                            case "Rejected":
                                $color = 'label-rejected';
                                break;
                            case "Offer":
                                $color = 'label-pending-aproval';
                                break;
                            case "Approved":
                                $color = 'label-new-request';
                                break;
                            case "Work Order Release":
                                $color = 'label-re-open';
                                break;
                            default:
                                $color = 'label-new-request';
                        }
		 ?>
            <tr>
              <td><span class="tag <?php echo $color; ?>"><?php echo $status[$values['resume_status']]; ?></span></td>
              <td><a href=""><?php echo $values['id'] ?></a></td>
              <td><a href=""><?php echo $Candidates->first_name.' '.$Candidates->last_name;?></a></td>
              <td><?php echo $values['estimate_start_date']; ?></td>
              <td>$<?php echo $values['candidate_pay_rate']; ?></td>
              <td>$<?php echo $billrate; ?></td>
              <td><?php echo $Candidates->current_location;?></td>
              <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$values['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table> 
        
      </div>
      <br>
    </div>
  </div>
</div>
</div>
</div>
<!-- col -->
</div>
<!-- row -->
</div>
</div>
<?php }else{
	$this->pageTitle = 'Interview Schedule';
	 ?>
<div class="page gray-bg">
  <div class="card p-0">
    <div class="job-detail">
      <div class="row m-0">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="client-listing-nav">
            <nav class="navbar navbar-default" role="navigation"> 
              
              <!-- Brand and toggle get grouped for better mobile display --> 
              
              <!-- Collect the nav links, forms, and other content for toggling -->
              
              <div class="collapse navbar-collapse navbar-ex2-collapse">
                <?php $this->renderPartial('jobsdetail_tabs_header'); ?>
              </div>
              
              <!-- /.navbar-collapse --> 
              
            </nav>
          </div>
          
          <!-- client-listing-nav --> 
          
        </div>
      </div>
      
      <!-- row -->
      
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="page-wraper">
            <div class="db-content-wraper">
              <div class="page-content-outer">
                <div class="page-content">
                  <div class="job-detail-inner">
                    <div class="job-submission">
                      <div class="job-approval-header">
                        <h4 class="heading">Interview Schedule ( Approved )</h4>
                        <p></p>
                      </div>
                      <table class="table">
                        <tbody>
                          <?php
$sql = 'SELECT * from vms_vendor_job_submission where job_id = "'.$jobmodel->id.'"  and resume_status="8"  and   (start_date_status !=0 OR alt1_date_status!=0 OR alt2_date_status!=0 OR alt3_date_status!=0)';
$command = Yii::app()->db->createCommand($sql);
$approvedSubmmison = $command->queryAll();
if($approvedSubmmison){
foreach($approvedSubmmison as $approvedData){
    $InterviwModel = Interview::model()->findByAttributes(array('submission_id'=>$approvedData['id']),array('order'=>'id desc'));
    if($InterviwModel)
    $interviwId = $InterviwModel->id;
	else
	  $interviwId =0;
    $candidateInfo = Candidates::model()->findByPk($approvedData['candidate_Id']);
    $f = $candidateInfo->first_name[0];
    $l = $candidateInfo->last_name[0];
    ?>
                          <tr>
                            <td><p>Interview ID: <a href=""><?php echo $interviwId; ?></a></p>
                              <?php
        if($approvedData['start_date_status']==1){
            $interviwDate = $approvedData['interview_start_date'].' at '.$approvedData['interview_start_date_time'];
        }
        if($approvedData['alt1_date_status']==1){
            $interviwDate = $approvedData['interview_alt1_start_date'].' at '.$approvedData['interview_alt1_start_date_time'];
        }
        if($approvedData['alt2_date_status']==1){
            $interviwDate = $approvedData['interview_alt2_start_date'].' at '.$approvedData['interview_alt2_start_date_time'];
        }
        if($approvedData['alt3_date_status']==1){
            $interviwDate = $approvedData['interview_alt3_start_date'].' at '.$approvedData['interview_alt3_start_date_time'];
        }
        ?>
                              <p><small><?php echo $interviwDate; ?></small></p></td>
                            <td style="width: 46px; padding-right: 0;"><div class="avatar-text"> <span><?php echo $f.''.$l ?></span> </div></td>
                            <td style="padding-right: 0;"><p class="bold"><?php echo $candidateInfo->first_name.' '.$candidateInfo->last_name; ?></p>
                              <p class="blue">Interview by: <?php echo $approvedData['interviewer'] ?></p></td>
                            <td class="text-center"><span class="label label-interview">INTERVIEW</span></td>
                            <td><div class="text-right m-t-10"> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/scheduleInterview',array('id'=>$jobmodel->id,'submission-id'=>$approvedData['id'],'type'=>'teammeber')); ?>" class="btn btn-default">View</a> <a href="#" class="btn btn-default">Note</a> </div></td>
                          </tr>
                          <!-- //end  -->
                          
                          <?php } } ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- interview-schedle --> 
                    
                    <br>
                    <br>
                    <?php
/*$sql1 = 'SELECT * from vms_vendor_job_submission where job_id = "'.$jobmodel->id.'" and form_submitted=1  and resume_status="Shortlisted"  and (start_date_status =0 OR alt1_date_status=0 OR alt2_date_status=0 OR alt3_date_status=0)';
$command1 = Yii::app()->db->createCommand($sql1);
$approvedSubmmison1 = $command1->queryAll();*/
$approvedSubmmison1 = VendorJobSubmission::model()->findAllByAttributes(array('job_id'=>$jobmodel->id,'start_date_status'=>0,'alt1_date_status'=>0,'alt2_date_status'=>0,'alt3_date_status'=>0,'form_submitted'=>1));
?>
                    <div class="interview-schedle">
                      <h4 class="heading">Interview Schedule (Pending) </h4>
                      <table class="table">
                        <tbody>
                          <?php
foreach($approvedSubmmison1 as $submmisomData){
	if(isset($_GET['submission-id'])){
		$submissionID = $_GET['submission-id'];
		}else{
			 $submissionID = $submmisomData['id'];
			 }
	
	
    $InterviwModel = Interview::model()->findByAttributes(array('submission_id'=>$submissionID),array('order'=>'id desc'));
	if($InterviwModel)
    $interviwId = $InterviwModel->id;
	else
	  $interviwId =0;
    $candidateInfo = Candidates::model()->findByPk($submmisomData['candidate_Id']);
    $f = $candidateInfo->first_name[0];
    $l = $candidateInfo->last_name[0];
?>
                          <tr>
                            <td><p>Interview ID: <a href=""><?php echo $interviwId; ?></a></p>
                              <p><small><?php echo $submmisomData['interview_start_date'].' at '.$submmisomData['interview_start_date_time']; ?></small></p></td>
                            <td style="width: 46px; padding-right: 0;"><div class="avatar-text"> <span><?php echo $f.''.$l ?></span> </div></td>
                            <td style="padding-right: 0;"><p class="bold"><?php echo $candidateInfo->first_name.' '.$candidateInfo->last_name; ?></p>
                              <p class="blue">Interview by: <?php echo $submmisomData['interviewer'] ?></p></td>
                            <td class="text-center"><span class="label label-shortlisted">Pending</span></td>
                            <td><div class="text-right m-t-10"> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/scheduleInterview',array('id'=>$jobmodel->id,'submission-id'=>$submmisomData['id'],'type'=>'teammeber')); ?>" class="btn btn-default">View</a> <a href="#" class="btn btn-default">Note</a> </div></td>
                          </tr>
                          <!-- //end  -->
                          
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    
                    <!-- job-approval --> 
                    
                  </div>
                  
                  <!-- job-detail-inner --> 
                  
                  <!-- /*  End inner cotent  */ -->
                  
                  <div class="quick-detail hide">
                    <?php $this->renderPartial('quickData',array('jobData'=>$jobmodel)); ?>
                  </div>
                  
                  <!-- quick-detail --> 
                  
                </div>
                
                <!-- page-content --> 
                
              </div>
              
              <!-- page-content-outer -->
              
              <?php $this->renderPartial('sidebar',array('jobData'=>$jobmodel,'invitedTeamMembers'=>$invitedTeamMembers)); ?>
            </div>
            
            <!-- db-content-wraper --> 
            
          </div>
          
          <!-- page-wraper --> 
          
        </div>
      </div>
      
      <!-- row --> 
      
    </div>
  </div>
</div>
<?php } ?>
