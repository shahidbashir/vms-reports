<?php $this->renderPartial('_menu',array('model'=>$model));?>
<div class="tab-content ">
    <div class="tab-pane active" id="one" role="tabpanel">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="well m-t-20 m-b-40">
                    <form action="" method="POST" role="form">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="">Select Team Member</label>
                                    <select name="inviteteammember[]" class="form-control">
                                        <option value=""></option>
                                        <?php foreach($allTeamMember as $value ) {?>
                                            <option value="<?php echo $value->id;?>"><?php echo $value->first_name.' '.$value->last_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group m-0">
                                    <label for="">&nbsp;</label>
                                    <p>
                                        <button type="submit" class="btn btn-success">Invite</button>
                                    </p>
                                </div>
                            </div>
                            <!-- col -->
                        </div>
                    </form>
                    <!-- row -->
                </div>
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>
                        <th>S. No</th>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Department</th>
                        <th>Role</th>
                        <th class="actions">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach($invitedTeamMembers as $value) {
                        $i++;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $value['first_name'].' '.$value['last_name']; ?></td>
                            <td><?php echo $value['email']; ?></td>
                            <td><?php echo $value['department']; ?></td>
                            <td><?php echo $value['member_type']; ?></td>
                            <td class="actions text-center">
                                <i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg"></i>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <br>
        </div>
    </div>
</div>
</div>
