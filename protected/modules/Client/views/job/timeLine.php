<?php $this->pageTitle = 'TimeLine Job'; ?>
<?php $this->renderPartial('_menu',array('model'=>$jobmodel));?>
<br>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 30px 0; 20px">

        <ul class="timeline">
            <?php if($JobHistory){
                foreach($JobHistory as $historyData){
                  $datetime  = explode(" ",$historyData->date_created);
                    $clientdata = Client::model()->findByPk($historyData->client_id);
                    $admindata = Admin::model()->findByPk($historyData->admin_id);
                    $vendordata = Vendor::model()->findByPk($historyData->vendor_id);
                    $images_path = Yii::app()->baseUrl.'/images/profile_img/'.$clientdata->profile_image;
                    $adminimages_path = Yii::app()->baseUrl.'/msp/files/profile-images/'.$admindata->profile_image;
                    $Vendorimages_path = Yii::app()->baseUrl.'/vendor/files/profile-images/'.$vendordata->profile_image;
                ?>
            <li class="timeline-item timeline-item-detailed">
                <div class="timeline-date"><span><?php echo $datetime[0]; ?></span></div>
                <div class="timeline-content">
                    <?php if(!empty($clientdata->profile_image) && !empty($historyData->client_id)){ ?>
                        <div class="timeline-avatar"><img src="<?php echo $images_path; ?>" alt="Avatar" class="circle"></div>
                    <?php }elseif(!empty($admindata->profile_image) && !empty($historyData->admin_id)){ ?>
                        <div class="timeline-avatar"><img src="<?php echo $adminimages_path; ?>" alt="Avatar" class="circle"></div>
                    <?php }elseif(!empty($vendordata->profile_image) && !empty($historyData->vendor_id)){ ?>
                        <div class="timeline-avatar"><img src="<?php echo $Vendorimages_path; ?>" alt="Avatar" class="circle"></div>
                    <?php }else{ ?>
                    <div class="timeline-avatar"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar4.png" alt="Avatar" class="circle"></div>
                    <?php }?>
                        <div class="timeline-header"><span class="timeline-time"><?php echo $datetime[1]; ?></span><span class="timeline-autor"><?php $Clienname = explode(" ",$historyData->description); echo $Clienname[0]; ?></span>
                        <p class="timeline-activity"><a href="#"><?php echo $historyData->operation; ?></a>.</p>
                        <div class="timeline-summary">
                            <p><?php echo $historyData->description; ?></p>
                        </div>
                    </div>
                </div>
            </li>
            <?php } } ?>
            <!--<li class="timeline-item timeline-loadmore"><a href="#" class="load-more-btn">Load more</a></li>-->
        </ul>

    </div> <!-- col -->
</div> <!-- row -->