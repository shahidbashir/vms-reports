<?php
$userID = Yii::app()->user->id;
$ClientRate = ClientRate::model()->findByAttributes(array('client_id'=>$userID));
/*echo '<pre>';
print_r($ClientRate);
exit;*/
?>
<?php

function select_options($selected = array()){

  $output = '';

  foreach(json_decode(file_get_contents(Yii::app()->basePath.'/names.json'), true) as $item){

    $output.= '<option value="' . $item['value'] . '"' . (in_array($item['value'], $selected) ? 'selected' : '') . '>' . $item['text'] . '</option>';

  }

  return $output;

} ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Add Job <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>" class="btn btn-sm btn-default-3 pull-right">Back to Job List</a> </h4>
      <p class="m-b-40">Following are the instruction to create a job requisition.</p>
      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'job-form',
          'enableAjaxValidation'=>false,
      ));
	  
	  echo $form->errorSummary($model);
	  
      $loginUserId = Yii::app()->user->id;
      $clientID = UtilityManager::superClient($loginUserId);

      ?>
      <div class="row add-job">
        <div class="col-md-12">
          <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-heading"></div>
            <div class="panel-body" style="overflow-y: inherit;">
            
            <?php 
			$ClientInfo = Client::model()->findByPk($clientID);
			if($ClientInfo->client_job_status == 'Yes'){ 
				$dynamicClass = 'col-md-4 col-lg-4';
			}else{
				 $dynamicClass = 'col-md-6 col-lg-6'; 
				 }
			?>
            
            
              <div class="row">
                <div class="col-xs-12 col-sm-4 <?php echo $dynamicClass ?>">
                  <div class="form-group">
                    <label for="">Job Title</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <input type="text" name="Job[title]" value="<?php echo ($model->title!='')? $model->title:Yii::app()->session['job_title']; ?>" class="form-control" title="Job title must be letter" required="required">
                    <?php echo $form->error($model,'title'); ?> </div>
                </div>
                <!-- col -->

                <div class="col-xs-12 col-sm-4 <?php echo $dynamicClass ?>">
                  <div class="form-group">
                    <label for="">Job Type</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=11')),'title', 'title');
                    echo $form->dropDownList($model, 'type', $list , array('class'=>'form-control select2','empty' => '','required'=>'required'));
                    ?>
                  </div>
                </div>
                <?php 
                if($ClientInfo->client_job_status=='Yes'){
                ?>
                <div class="col-xs-12 col-sm-4 <?php echo $dynamicClass ?>">
                 <div class="form-group">
                  <label for="">Client Name</label>
                   <i class="required-field fa fa-asterisk"></i>
                   <?php
                    $list = CHtml::listData(ProClientNames::model()->findAll(array('condition'=>'main_client='.$clientID)),'id', 'name');
                  echo $form->dropDownList($model, 'client_name', $list , array('class'=>'form-control select2','empty' => '','required'=>'required'));
                   ?>
                 </div>
             </div>
                <?php } ?>
                <!-- col --> 
              </div>
              <!-- row -->
              <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Category</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'title', 'title');
                    echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'form-control select2','empty' => '','required'=>'required'));
					 ?>
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Cost Center code</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <select  id="cost_center_code" class="form-control" name="Job[cost_center_code]" required >
                      <option value=""></option>
                    </select>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Experiences</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=10')),'title', 'title');
                    echo $form->dropDownList($model, 'experience', $list , array('class'=>'form-control select2','empty' => '','required'=>'required'));
					?>
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Job Level</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=40')),'title', 'title');
                    echo $form->dropDownList($model, 'job_level', $list , array('class'=>'form-control select2','empty' => '','required'=>'required')); 
					?>
                  </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row --> 
              
              <br>
              
              <!-- row -->
              <div class="hirring-pipline">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    
                    <div class="form-group">
                      <label for="">Primary Skill</label>
                      <select class="tokenize-remote-demo1" name="Job[skills][]" id="job_skills" multiple>
                        <?php
						$primary_skills = explode(',',str_replace(' ','',$model->skills)); 
						if($primary_skills){
						foreach($primary_skills as $pValue){
							echo '<option selected="selected" value="'.$pValue.'">'.$pValue.'</option>';
							}
						}
						 ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Secondary Skill</label>
                      <select class="tokenize-remote-demo1" name="Job[skills1][]" id="job_skills1" multiple>
                        <?php
						$secondary_skills = explode(',',str_replace(' ','',$model->skills1)); 
						if($secondary_skills){
						foreach($secondary_skills as $sValue){
							echo '<option selected="selected" value="'.$sValue.'">'.$sValue.'</option>';
							}
						}
						 ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Good to have.</label>
                      <select class="tokenize-remote-demo1" name="Job[skills2][]" id="job_skills2" multiple>
                        <?php
						$good_to_have = explode(',',str_replace(' ','',$model->skills2)); 
						if($good_to_have){
						foreach($good_to_have as $gValue){
							echo '<option selected="selected" value="'.$gValue.'">'.$gValue.'</option>';
							}
						}
						 ?>
                      </select>
                    </div>
                  </div>
                  <!-- col-12 --> 
                </div>
                <!-- row -->
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for="">Note for Skills</label>
                      <textarea name="Job[skills_notes]" id="input" class="form-control" rows="5" ><?php if($model){ echo $model->skills_notes; } ?></textarea>
                    </div>
                  </div>
                </div>
                <!-- row --> 
              </div>
              <br>
              
              <!-- hirring-pipline -->
              <div class="search-box-only-border m-b-20">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel-group" id="accordion">
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed"> Job Description</a><i class="required-field fa fa-asterisk"></i> </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="Job[description]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px" required="required"><?php if($model){ echo $model->description; } ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false"> You Will</a> </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="Job[you_will]"  class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php if($model) { echo $model->you_will; } ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false"> Qualifications</a> </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="Job[qualification]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"> <?php if($model) { echo $model->qualification; } ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info m-b-0">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed" aria-expanded="false"> Additional Information</a> </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="Job[add_info]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php if($model) { echo $model->add_info; } ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- col-12 --> 
                </div>
                <!-- row --> 
              </div>
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <label for="">Location</label>
                  <i class="required-field fa fa-asterisk"></i>
                  <div class="form-group">
                    <?php $loc = unserialize($model->location); ?>
                    <select  multiple="" class="select2" name="Job[location][]" required >
                      <option value=""></option>
                      <?php

                      $location = Location::model()->findAllByAttributes(array('reference_id'=>$clientID));
                      if($loc){
                      foreach($location as $location){
                      foreach($loc as $val){ ?>
                      <option value="<?php echo $location->id; ?>" <?php if($val==$location->id){ echo 'Selected'; } ?>><?php echo $location->name; ?></option>
                      <?php }
                        }
                      }else{
                      foreach($location as $location){ ?>
                          <option value="<?php echo $location->id; ?>" <?php if($val==$location->id){ echo 'Selected'; } ?>><?php echo $location->name; ?></option>
                        <?php } } ?>
                    </select>
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Number of Openings</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <?php echo $form->numberField($model,'num_openings',array('class'=>'form-control','min'=>'0','required'=>"required")); ?> </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row -->
              
              <input type="hidden" name="Job[vendor_client_markup]" id="vendor_client_markup" class="form-control" value="">
              <br>
              <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group">
                    <label for="">Estimated Job Start Date</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <input type="text" class="form-control daterange readonly2" required="required" value="<?php echo $model->job_po_duration; ?>" name="job_po_duration" id="daterange">
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group">
                    <label for="">Job End Date</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <input type="text" class="form-control daterange readonly2" value="<?php echo $model->job_po_duration_endDate; ?>" name="job_po_duration_endDate" id="daterange1"  required="required">
                    <?php //echo $form->error($model,'job_po_duration_endDate',array('class'=>'required-field')); ?>
                    
                    <?php echo $form->error($model,'job_po_duration_endDate'); ?> 
					<?php //echo Yii::app()->user->getFlash('error123'); ?>
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group">
                    <label for="">Target Closing Date for Job</label>
                    <i class="required-field fa fa-asterisk"></i>
                    <input type="text" class="form-control readonly2" value="<?php echo $model->desired_start_date; ?>" name="desired_start_date" required="required" id="dateField" placeholder="">
                  </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row -->
              <hr>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <label for="">Invite your Team member to Collaborate</label>
                  <div class="form-group">
                    <select multiple="" class="select2" name="Job[invite_team_member][]" >
                      <option value=""></option>
                      <?php $team = unserialize($model->invite_team_member);
                      $friend = Client::model()->findAllByAttributes(array('super_client_id'=>$clientID));
                      if($team){
                      foreach($friend as $friend){
                        foreach ($team as $value){
                        ?>
                      <option value="<?php echo $friend->id; ?>" <?php if($value==$friend->id){ echo 'Selected'; } ?>><?php echo $friend->first_name.' ('.$friend->email.')'; ?></option>
                      <?php } } }else{
                      foreach($friend as $friend){ ?>
                        <option value="<?php echo $friend->id; ?>"><?php echo $friend->first_name.' ('.$friend->email.')'; ?></option>
                        <?php
                      }	} ?>
                    </select>
                  </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row -->
              <?php /*?><div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Labor Category</label>
                    <?php
                    $labour_cat = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=14')),'title', 'title');
                    echo $form->dropDownList($model, 'labour_cat', $labour_cat , array('class'=>'form-control select2','empty' => '')); 
					?>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Number of Submission</label>
                    <?php echo $form->numberField($model,'number_submission',array('class'=>'form-control','min' => 1)); ?> </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Background Verification</label>
                    <?php
                    $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=41')),'title', 'title');
                    echo $form->dropDownList($model, 'background_verification', $list , array('class'=>'form-control','empty' => '','required'=>"required")); 
					?>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Type of Job</label>
                    <select name="Job[type_of_job]" id="input" class="form-control" required="required">
                      <option value=""></option>
                      <option value="Non Exempt">Non Exempt</option>
                      <option value="Exempt">Exempt</option>
                    </select>
                  </div>
                </div>
              </div><?php */?>
              <!-- row --> 
              
              <br>
              <div class="well">
                <h4> Pre Identity Candidate </h4>
                <br>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for="">Do you have Pre Identity Candidate</label>
                      <i class="required-field fa fa-asterisk"></i>
                      <select name="Job[pre_candidate]" id="pre_candidate" class="form-control">
                        <option value="Yes"></option>
                        <option value="Yes" <?php if($model->pre_candidate=='Yes'){ echo 'Selected'; } ?>>Yes</option>
                        <option value="No" <?php if($model->pre_candidate=='No'){ echo 'Selected'; } ?>>No</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="">&nbsp;</label>
                    <div class="row">
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <input type="text" name="Job[pre_name]" value="<?php echo $model->pre_name; ?>"  class="form-control" id="pre_name" placeholder="Candidate Name">
                        </div>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <input type="text" name="Job[pre_supplier_name]" value="<?php echo $model->pre_supplier_name; ?>" class="form-control" id="pre_supplier_name" placeholder="Supplier Name">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- row -->
                <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for="">Current Rate</label>
                      <input type="text" class="form-control" value="<?php echo $model->pre_current_rate; ?>" id="pre_current_rate"  name="Job[pre_current_rate]">
                    </div>
                  </div>
                  <!-- col -->
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for=""> Payment Type</label>
                      <?php
                      $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                      echo $form->dropDownList($model, 'pre_payment_type', $list , array('id'=>'pre_payment_type','class'=>'form-control select2 pre_fields','empty' => '','required'=>'required')); //,'disabled'=>true ?>
                    </div>
                  </div>
                  <!-- col --> 
                </div>
                <!-- row --> 
              </div>
              <br>
              <div>
                <button type="submit" id="job-submit" class="btn btn-success">Save & Continue</button>
                 <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>" class="btn btn-default">Cancel</a>
              </div>
              <br>
              <br>
            </div>
            <!-- panel-body --> 
          </div>
        </div>
      </div>
      <?php $this->endWidget(); ?>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<style>
.tokenize-dropdown > .dropdown-menu li > a  {
    display: block !important;
}
.readonly2{
	background:#fff !important;
	}
.errorMessage, .errorSummary{
	color:red;
	}
</style>
