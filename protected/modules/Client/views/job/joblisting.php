<?php
	if($this->action->id=='joblisting1'){
		$this->pageTitle= 'Pending Jobs';
		}else{
			 $this->pageTitle= 'List of Jobs';
			 }
	
	// set default timezone
	date_default_timezone_set('America/Chicago'); // CDT ?>

    <div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
        <?php if(Yii::app()->user->hasFlash('success')):?>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        <?php endif; ?>
        <div class="cleafix " style="padding: 30px 20px 10px; ">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h4 class="m-b-10 page-title">
                	<?php echo $this->pageTitle; ?>
                    <!--<a href="" class="pull-right"><i class="fa fa-question"></i></a>
                    <a href="" class="pull-right"><i class="fa fa-info"></i></a>-->
                </h4>
                <p class="m-b-20">
                <?php
				
				if($this->action->id=='joblisting1'){
				echo 'List of jobs which need approval. A Workflow approval automation has been triggered by the system. All respective member of workflow are notified to approve the job.';
				}else{
					 echo 'A list of job requriement which are posted by the hiring manager and other members.';
					 }
				
                ?>
                </p>
                <div class="m-b-30">
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/addjob'); ?>" class="btn btn-sm btn-default-2">Add Job</a>
                    <?php
						$loginClient = Client::model()->findByPk($loginUserId);
						if($loginClient->member_type != 'Hiring Manager'){
					 ?>
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/addTemplate'); ?>" class="btn btn-sm btn-default-2">Add Job Template</a>
                    <?php } ?>
                </div>
                <div class="search-box">
                    <form action="" method="post">
                    <div class="two-fields">
                        <div class="form-group">
                            <label for=""></label>
                            <input type="text" class="form-control" id="data" name="s" value="<?php if(!empty($_POST['s'])){ echo $_POST['s'];}?>" placeholder="Search Jobs">
                        </div>
                        <div class="form-group">
                            <label for="">&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-block">Search</button>
                        </div>
                    </div> <!-- two-flieds -->
                        </form>
                </div>
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>
                        <!--<th style="width: 10px;">
                            <input type="checkbox" value="">
                        </th>-->
                        <th style="width: 6%"> Status</th>
                        <th style="width: 7%">Job ID</th>
                        <th >Job Title</th>
                        <th>Job Duration</th>
                        <th> Position</th>
                        <th> Hired</th>
                        <th> Submission</th>
                        <th class="actions">Actions</th>

                    </tr>

                    </thead>

                    <tbody id="jobdata">
                    <?php if($jobs){

                    foreach($jobs as $jobKey=>$jobValue){
						
						
					//calculating number of hired and availity of candidates which are on workorder state
					$submission = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobValue->id));
					$countworkorders = Workorder::model()->countByAttributes(array('job_id'=>$jobValue->id,'workorder_status'=>1));
					$countHired = Workorder::model()->countByAttributes(array('job_id'=>$jobValue->id,'workorder_status'=>1,'on_board_status'=>1));
						
                    //if($jobSecondDate[1]==date('m/d/Y')){
                    $postingstatus = UtilityManager::jobStatus();

                    switch ($postingstatus[$jobValue->jobStatus]) {
                        case "Pending Approval":
                            $color = 'label-pending-aproval';
                            break;
                        case "Open":
                            $color = 'label-open';
                            break;
                        case "Filled":
                            $color = 'label-filled';
                            break;
                        case "Rejected":
                            $color = 'label-rejected';
                            break;
                        case "Re-open":
                            $color = 'label-re-open';
                            break;
                        case "Hold":
                            $color = 'label-hold';
                            break;
                        case "New Request":
                            $color = 'label-new-request';
                            break;
						case "Draft":
                            $color = 'tag label-draft';
                            break;

                        default:
                            $color = 'label-new-request';
                    }


                    ?>
                    <tr>

                        <!--<td><input type="checkbox" value=""></td>-->
                        <td><span class="tag <?php echo $color; ?>"><?php echo $postingstatus[$jobValue->jobStatus]; ?></span></td>
                        <td class="user-avatar"><a class="underline" href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobValue->id,array('type'=>'info')); ?>"><?php echo $jobValue->id; ?></a></td>
                        <td>
                           
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobValue->id,array('type'=>'info')); ?>" class="underline"><?php
								$limit = 60;
								if (strlen($jobValue->title) > $limit)
								  $jobValue->title = substr($jobValue->title, 0, strrpos(substr($jobValue->title, 0, $limit), ' ')) . '...';
								  echo $jobValue->title;
								?></a>
                            
                            
                            <?php $closingdate = date('Y-m-d', strtotime('-4 day', strtotime($jobValue->desired_start_date)));
                            if($closingdate== date('Y-m-d')){
                            ?>
                            <i class="icon svg-fire"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/fire@512px-red.svg"></i>
                        <?php } ?>
                                </td>
                        <td><?php echo $jobValue->job_po_duration.' - '.$jobValue->job_po_duration_endDate; ?></td>
                        <td><?php echo $jobValue->num_openings ?></td>
                        <td><?php echo $countHired; ?></td>
                        <td><?php echo $submission;?></td>


                        <td class="actions">
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobValue->id,array('type'=>'info')); ?>" data-toggle="tooltip" data-placement="top" title="View" class="icon">
                                <i class="icon svg-icon">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg">
                                </i>
                            </a>
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/deletejob/id/'.$jobValue->id); ?>" data-toggle="tooltip" data-placement="top" title="Delete" class="icon">
                                <i class="icon svg-icon">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg">
                                </i>
                            </a>
                        </td>

                    </tr>
                    <?php } }?>
                    </tbody>

                </table>
            </div>
            <!-- col -->
        </div>
        <!-- row -->
        <div class="clearfix" style="padding: 10px 20px 10px; ">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                    'header' => '',
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class' => 'pagination m-t-0',
                    )
                ))
                ?>

            </div>
            <!--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <p class="text-right">
                    Showing 10 to 20 of 50 entries
                </p>
            </div>-->
        </div>
        <div class="clearfix" style="padding: 10px 20px 30px; ">
            <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h5 class="m-b-10">Quick Note</h5>
                <p class="m-b-10">
                    <span class="tag label-pending-aproval">Pending Approval</span>
                    Lorem ipsum dolor sit amet.
                </p>
                <p class="m-b-10">
                    <span class="tag label-open">Open</span> Lorem ipsum dolor sit amet.
                </p>
                <p class="m-b-10">
                    <span class="tag label-re-open">Re-open </span>
                    Lorem ipsum dolor sit amet.
                </p>
                <p class="m-b-10">
                    <span class="tag label-rejected">Rejected </span>
                    Lorem ipsum dolor sit amet.
                </p>
                <p class="m-b-10">
                    <span class="tag label-filled ">Filled </span>
                    Lorem ipsum dolor sit amet.
                </p>
                <p class="m-b-10">
                    <span class="tag label-hold">Hold </span>
                    Lorem ipsum dolor sit amet.
                </p>
                <p class="m-b-10">
                    <span class="tag label-new-request">New Request </span>
                    Lorem ipsum dolor sit amet.
                </p>
                <p class="m-b-10">
                    <span class="tag label-draft">Draft </span>
                    Lorem ipsum dolor sit amet.
                </p>
            </div>-->
        </div>
        <div class="seprater-bottom-100"></div>
    </div>