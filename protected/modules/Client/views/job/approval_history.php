<?php 	$JobWorkflow = JobWorkflow::model()->findAllByAttributes(array('job_id'=>$_GET['jobid'],'workflow_id'=>$_GET['workflowid']),array('order'=>"number_of_approval asc"));	 ?>
<table class="table m-b-40 without-border">
  <thead class="thead-default">
    <tr>
      <th>S. No</th>
      <th>Person Name</th>
      <th>Department</th>
      <th>Date &amp; Time</th>
      <th class="actions">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php  
	$i = 0;  
	foreach($JobWorkflow as $key=>$value){    
	$i++;    
	$clientData = Client::model()->findByPk($value->client_id);  ?>
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $clientData->first_name.' '.$clientData->last_name; ?></td>
      <td><?php echo $clientData->department; ?></td>
      <td>
      <?php
		if($value->status_time == '0000-00-00 00:00:00') {
		  echo 'NULL';
		}else{
		  echo date('F d,Y h:i:s A', strtotime($value->status_time));
		}?>
	  <?php //echo  date('F j, Y h:i:s',strtotime($value->status_time)); ?></td>
      <td class="actions text-center"><?php echo $value->job_status; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<!-- row -->