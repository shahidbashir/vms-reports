<div class="quick-info box"> <a href="" class="close">Close</a>
                      <div class="quick-info-header">
                        <h4>QUICK INFORMATION</h4>
                      </div>
                      <div class="quick-info-content">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>JOB TYPE</th>
                              <th>PAYMENT</th>
                              <th>NUMBER OF OPENINGS</th>
                              <th>JOB CLOSING DATE</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><?php  echo $jobData->type; ?></td>
                              <td><?php echo $jobData->rate.' '.$jobData->currency ?> - <?php echo $jobData->payment_type ?></td>
                              <td><?php  echo $jobData->num_openings ?></td>
                              <td><!--10-02-2015 ( 9 More Left) --><?php  echo $jobData->desired_start_date; ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
<br>
<div class="job-workflow-info">
  <ul class="list-inline b-b">
    <li>
      <h4>Pre Indentity Candidate</h4>
      <p><?php  echo $jobData->pre_name; ?></p>
    </li>
    <li>
      <h4>Supplier Name</h4>
      <p><?php  echo $jobData->pre_supplier_name; ?></p>
    </li>
    <li>
      <h4>Rate for Candidate</h4>
      <p><?php  echo $jobData->pre_current_rate; ?></p>
    </li>
  </ul>
  <p class="h4">Internal Reference</p>
  <ul class="list-inline">
    <li>
      <p class="m-6">Reference Code / Project Code</p>
      <h4><?php  echo $jobData->pre_reference_code; ?></h4>
    </li>
    <li>
      <h4>Estimated Cost</h4>
      <p><?php  echo ($jobData->pre_total_estimate_code=='')?0:$jobData->pre_total_estimate_code; ?></p>
    </li>
    <li>
      <h4>Workflow</h4>
      <p><?php $Workflow = Worklflow::model()->findByPk($jobData->work_flow);  echo $Workflow->flow_name; ?></p>
    </li>
    <!--<li> <span class="label">WORKFLOW APPROVED</span> </li>-->
  </ul>
</div>
<!-- job-workflow-info --> <br>
<div class="skills-detail">
  <div class="skills-info box">
    <div class="skills-info-header">
      <h4>SKILLS</h4>
    </div>
    <div class="skills-info-content">
    
    <?php  $skillsArray =  explode(",",$jobData->skills) ;
    foreach($skillsArray as $skillsValue){
     ?>
    
    <span class="label label-info">                        
        <?php echo $skillsValue; ?>
    </span> 
    
    <?php } ?>
    
    
     </div>
  </div>
</div>
<!-- skills-detail --> <br>
<div class="description-detail">
  <div class="description-info box">
    <div class="description-info-header">
      <h4>JOB DESCRIPTION</h4>
    </div>
    <div class="description-info-content">
      <p><?php echo $jobData->description; ?></p>
    </div>
  </div>
</div>
<!-- description-detail --> <br>
<div class="description-detail">
  <div class="description-info box">
    <div class="description-info-header">
      <h4>YOU WILL</h4>
    </div>
    <div class="description-info-content">
      <p><?php echo $jobData->you_will; ?></p>
    </div>
  </div>
</div>
<!-- description-detail --> <br>
<div class="description-detail">
  <div class="description-info box">
    <div class="description-info-header">
      <h4>QUALIFICATIONS</h4>
    </div>
    <div class="description-info-content">
      <p><?php echo $jobData->qualification; ?></p>
    </div>
  </div>
</div>
<!-- description-detail --> <br>
<div class="description-detail">
  <div class="description-info box">
    <div class="description-info-header">
      <h4>ADDITIONAL INFORMATION</h4>
    </div>
    <div class="description-info-content">
      <p><?php echo $jobData->add_info; ?></p>
    </div>
  </div>
</div>
<!-- description-detail --> <br>
<div class="location-detail">
  <div class="location-info box">
    <div class="location-info-header">
      <h4>LOCATIONS</h4>
    </div>
    <div class="location-info-content">
      <div class="row">
        <?php                         $locations = unserialize($jobData->location);                        foreach ($locations as $keyloc => $valueloc) {						  echo '<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">';                          echo 'Location <br/> ';                          echo '<p>';                          echo $valueloc.'</p>';						  echo '</div>';                        } ?>
        <!--<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">                              <p>Location</p>                              <p>2741 Barnes Street</p>                              <p>Orlando, FL 32810</p>                            </div>--> </div>
      <!-- row --> </div>
  </div>
</div>
<!-- description-detail -->
<div class="seprater-bottom-50"></div>