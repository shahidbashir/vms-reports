<?php if(Yii::app()->user->hasFlash('success')):?>
	<?php echo Yii::app()->user->getFlash('success'); ?>
<?php endif; ?>
<?php
	$this->pageTitle = 'Clone "'.$model->title.'"';
	$userID = Yii::app()->user->id; 
	$ClientRate = ClientRate::model()->findByAttributes(array('client_id'=>$userID));
?>
<div class="row">
  <div class="col-md-12">
    <div class="white-box">
      <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
          <div class="add-job">
            <?php 
			$form=$this->beginWidget('CActiveForm', array(
						'id'=>'job-form',
						'enableAjaxValidation'=>false,
					));
					$loginUserId = Yii::app()->user->id;
					?>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="">Job Title</label>
                  <?php echo $form->textField($model,'title',array('class'=>'form-control')); ?> <?php echo $form->error($model,'title'); ?> </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="four-flieds">
                  <div class="form-group">
                    <label for="" class="">Job Type</label>
                    <div class="single">
                      <?php 
					   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=11')),'title', 'title');
					   echo $form->dropDownList($model, 'type', $list , array('class'=>'ui fluid search dropdown','empty' => '')); 				
						   ?>
                    </div>
                  </div>
                  <!-- form-group -->
                  
                  <div class="form-group">
                    <label for="" class="">Category</label>
                    <div class="single">
                      <?php 
					   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'title', 'title');
					   echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                    </div>
                  </div>
                  <!-- form-group -->
                  
                  <div class="form-group">
                    <label for="">Experiences</label>
                    <div class="single">
                      <?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=10')),'title', 'title');
                   echo $form->dropDownList($model, 'experience', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                    </div>
                  </div>
                  <!-- form-group -->
                  
                  <div class="form-group">
                    <label for="">Job Level</label>
                    <div class="single">
                      <?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=40')),'title', 'title');
                   echo $form->dropDownList($model, 'job_level', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                    </div>
                  </div>
                  <!-- form-group --> 
                  
                </div>
              </div>
            </div>
            <!-- row -->
            
            <div class="hirring-pipline">
              <div class="row">
                <div class="col-xs-12 col-sm-12">
                  <div class="form-group">
                    <label for="">Primary Skill</label>
                    <div class="single">
                      <input type="text" class="form-control" id="job_skills1" value="<?php echo $model->skills; ?>" name="Job[skills]">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Secondary Skill</label>
                    <div class="single">
                      <input type="text" class="form-control" id="job_skills" value="<?php echo $model->skills1; ?>" name="Job[skills1]">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Other Skills</label>
                    <div class="single">
                      <input type="text" class="form-control" id="job_skills2" value="<?php echo $model->skills2; ?>" name="Job[skills2]">
                    </div>
                  </div>
                </div>
                <!-- col-12 --> 
                
              </div>
              <!-- row -->
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="">Note for Skills</label>
                    <textarea name="Job[skills_notes]" id="input" class="form-control" rows="5" required><?php echo $model->skills_notes; ?></textarea>
                  </div>
                </div>
              </div>
              <!-- row --> 
              
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="">Location</label>
                  <div class="single">
                    <select class="ui fluid search multiple  dropdown" name="Job[location][]">
                      <option value=""></option>
                      <?php
                    $location = Location::model()->findAllByAttributes(array('reference_id'=>$loginUserId));
                    foreach($location as $location){ ?>
                      <option value="<?php echo $location->name; ?>"><?php echo $location->name; ?></option>
                      <?php  } 	?>
                    </select>
                  </div>
                </div>
              </div>
              <!-- col-12 -->
              
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label for="" class="">Number of Openings</label>
                  
                  <?php echo $form->numberField($model,'num_openings',array('class'=>'form-control','min'=>'0')); ?> </div>
              </div>
              <!-- col-12 --> 
              
            </div>
            <!-- row -->
            
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> Job Description</a> </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <textarea name="Job[description]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->description; ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> You Will</a> </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                      <div class="panel-body">
                        <textarea name="Job[you_will]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->you_will; ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> Qualifications</a> </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                      <div class="panel-body">
                        <textarea name="Job[qualification]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->qualification; ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> Additional Information</a> </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                      <div class="panel-body">
                        <textarea name="Job[add_info]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->add_info; ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            
            <div class="row">
          <div class="col-xs-12 col-sm-12">
           <div class="hirring-pipline">
            <p>Hiring Pipeline</p>
 
            <div class="checkbox checkbox-default">
              <input type="checkbox" name="ch-resume" name="Job[resume_submission]" value="1" <?php echo $model->resume_submission==1?'checked':''; ?> >
              <label for="ch-resume">  Resume Submission </label>
            </div>
            <div class="checkbox checkbox-default">
              <input type="checkbox" name="Job[assignment_process]" id="ch-assig" value="1" <?php echo $model->assignment_process==1?'checked':''; ?> >
              <label for="ch-assig">  Assignemnet Process </label>
            </div>
            <div class="checkbox checkbox-default">
              <input type="checkbox" name="Job[phone_interview]" id="ch-phone-interview" value="1" <?php echo $model->phone_interview==1?'checked':''; ?> >
              <label for="ch-phone-interview">  Phone Interview </label>
            </div>
            <div class="checkbox checkbox-default">
              <input type="checkbox" name="Job[onsite_interview]" id="ch-onsite-interview" value="1" <?php echo $model->onsite_interview==1?'checked':''; ?> >
              <label for="ch-onsite-interview">  Onsite Interview </label>
            </div>
            <div class="checkbox checkbox-default">
              <input type="checkbox" name="Job[offer]" id="ch-offer" value="1" <?php echo $model->offer==1?'checked':''; ?> >
              <label for="ch-offer">  Offer</label>
            </div>
           </div>
          </div> <!-- col -->
         </div> <!-- row -->
            
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="four-flieds">
                  <?php 
				  if($ClientRate){
				  if($ClientRate->type == 'Pay Rate'){ ?>
                  <div class="form-group">
                    <label for="" class="">Markup</label>
                    <input type="text" class="form-control" id="mark_up_percentage" value="<?php echo $ClientRate->mark_up_percentage ?>" name="Job[markup]">
                  </div>
                  <div class="form-group">
                    <label for="" class="">Pay Rate for Candidate</label>
                    <?php echo $form->numberField($model,'pay_rate_candidate',array('class'=>'form-control','placeholder'=>'','step'=>'.1','id'=>'Job_pay_rate','onChange'=>'calculate()')); ?>
                  </div>
                  <?php } } ?>
                  
                   <?php 
				   if($ClientRate){
				   if($ClientRate->type != 'Pay Rate'){ ?>
                   <div class="form-group">
                    <label for="" class="">Rates</label>
                    <?php echo $form->numberField($model,'rate',array('class'=>'form-control','placeholder'=>'Rates','step'=>'.1','onChange'=>'calculate()')); //,'onChange'=>'calculate()' ?> </div>
                   <?php } } ?> 
                    
                  <div class="form-group">
                    <label for="" class="">&nbsp;</label>
                    <div class="single">
                      <?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=12')),'title', 'title');
                   echo $form->dropDownList($model, 'currency', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                    </div>
                  </div>
                  
                  
                  <div class="form-group">
                    <label for="" class="">&nbsp;</label>
                    <div class="single">
                      <?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                   echo $form->dropDownList($model, 'payment_type', $list , array('class'=>'ui fluid search dropdown','empty' => '','onChange'=>'calculate()')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="">Hours Per Week</label>
                    <div class="single">
                      <?php 
                   $hours_per_week = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=16')),'title', 'title');
                   echo $form->dropDownList($model, 'hours_per_week', $hours_per_week , array('class'=>'ui fluid search dropdown','empty' => '','onChange'=>'calculate()')); ?>
                    </div>
                  </div>
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group has-feedback">
                  <label for="">Job Posting Duration</label>
                  <input type="text" class="form-control" id="daterange" name="job_po_duration" value="<?php echo $model->job_po_duration; ?>" required="required" onchange="calculate()">
                  <i class=" fa  fa-calendar form-control-feedback"></i> </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="two-flieds">
                  <div class="form-group has-feedback">
                    <label for="">Desired Start Date</label>
                    <input type="text" name="desired_start_date" id="dateField" value="<?php echo $model->desired_start_date; ?>" class="form-control" /
                    <i class=" fa  fa-calendar form-control-feedback"></i> </div>
                  <div class="form-group">
                    <label for="">Shift</label>
                    <div class="single">
                      <?php 
                   $shift = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=15')),'title', 'title');
                   echo $form->dropDownList($model, 'shift', $shift , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                    </div>
                  </div>
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            
            <div class="row">
              <div class="col-xs-12 col-sm-6"> </div>
              <div class="col-xs-12 col-sm-6"> </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            
            <hr>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="">Invite your Team member to Collaborate</label>
                  <div class="single">
                    <select class="ui fluid search multiple  dropdown" name="Job[invite_team_member][]">
                      <option value="<?php //echo $model->invite_team_member; ?>"><?php //echo $model->invite_team_member; ?></option>
                      <?php
                    $friend = Client::model()->findAllByAttributes(array('super_client_id'=>$loginUserId));
                    foreach($friend as $friend){ ?>
                      <option value="<?php echo $friend->id; ?>"><?php echo $friend->first_name.' ('.$friend->email.')'; ?></option>
                      <?php  } 	?>
                    </select>
                  </div>
                </div>
              </div>
              <!-- col-12 --> 
            </div>
            <!-- row -->
            
            <div class="row">
              <div class="col-xs-12">
                <div class="three-flieds">
                  <div class="form-group">
                    <label for="">Labor Category</label>
                    <div class="single">
                      <?php 
                   $labour_cat = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=14')),'title', 'title');
                   echo $form->dropDownList($model, 'labour_cat', $labour_cat , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Number of Submission</label>
                    <?php echo $form->numberField($model,'number_submission',array('class'=>'form-control')); ?> </div>
                  <div class="form-group">
                    <label for="">Background Verification</label>
                    <div class="single">
                      <?php 
                   echo $form->dropDownList($model, 'background_verification',  array('Yes'=>'Yes','No'=>'No') , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- row --> 
            
            <br>
            <div class="well">
              <h4> Pre Identity Candidate </h4>
              <br>
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Do you have Pre Identity Candidate</label>
                    <div class="single">
                      <select name="Job[pre_candidate]" id="pre_candidate" class="ui fluid search dropdown">
                        <option value=""></option>
                        <option <?php echo $model->pre_candidate=='Yes'?'selected':''; ?> value="Yes">Yes</option>
                        <option <?php echo $model->pre_candidate=='No'?'selected':''; ?> value="No">No</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <label for="">&nbsp;</label>
                  <div class="two-flieds">
                    <div class="form-group">
                      <input type="text" class="form-control" id="pre_name" value="<?php echo $model->pre_name; ?>"  name="Job[pre_name]" placeholder="Candidate Name">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" id="pre_supplier_name" value="<?php echo $model->pre_supplier_name; ?>"  name="Job[pre_supplier_name]" placeholder="Supplier Name">
                    </div>
                  </div>
                </div>
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Current Rate</label>
                    <input type="text" id="pre_current_rate"  name="Job[pre_current_rate]" value="<?php echo $model->pre_current_rate; ?>" class="form-control" id="" placeholder="">
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for=""> Payment Type</label>
                    <div class="single">
                      <?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                   echo $form->dropDownList($model, 'pre_payment_type', $list , array('id'=>'pre_payment_type','class'=>'ui fluid search dropdown pre_fields','empty' => '')); //,'disabled'=>true ?>
                    </div>
                  </div>
                </div>
                <!-- col --> 
                
              </div>
              <!-- row --> 
              
            </div>
            <!-- well --> 
            
            <br>
            <div class="well well-info">
              <h4>Internal Reference Section</h4>
              <br>
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Request Department</label>
                    <div class="single">
                      <select class="ui fluid search dropdown" name="Job[billing_code]">
                        <option value=""></option>
                        <?php
                    $JobrequestDepartment = JobrequestDepartment::model()->findAllByAttributes(array('client_id'=>$loginUserId));
                    foreach($JobrequestDepartment as $JobrequestDepartment){ ?>
                        <option value="<?php echo $JobrequestDepartment->department; ?>"><?php echo $JobrequestDepartment->department; ?></option>
                        <?php  }  ?>
                      </select>
                    </div>
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Billcode</label>
                    <div class="single">
                      <select class="ui fluid search dropdown" name="Job[request_dept]">
                        <option value=""></option>
                        <?php
                    $BillingcodeDepartment = BillingcodeDepartment::model()->findAllByAttributes(array('client_id'=>$loginUserId));
                    foreach($BillingcodeDepartment as $BillingcodeDepartment){ ?>
                        <option value="<?php echo $BillingcodeDepartment->billingcode; ?>"><?php echo $BillingcodeDepartment->billingcode; ?></option>
                        <?php  }  ?>
                      </select>
                    </div>
                  </div>
                </div>
                <!-- col --> 
                
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <label for="">Rate</label>
                  <br>
                  <div class="checkbox radio-custom">
                    <input type="checkbox" <?php echo $model->over_time=='1'?'checked="checked"':''; ?> name="Job[over_time]" id="radio2" value="1">
                    <label for="radio2">OverTime </label>
                  </div>
                  <div class="checkbox radio-custom m-b-20">
                    <input type="checkbox" <?php echo $model->double_time=='1'?'checked="checked"':''; ?> name="Job[double_time]" id="radio2" value="1">
                    <label for="radio2"> Double Time </label>
                  </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="">Reference Code / Project Code </label>
                    <input type="text" id="pre_reference_code"  name="Job[pre_reference_code]" class="form-control" value="<?php echo $model->pre_reference_code; ?>" title="">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label for="">Total Estimate Cost </label>
                    <input type="text" id="pre_total_estimate_code"  name="Job[pre_total_estimate_code]"  class="form-control" value="<?php echo $model->pre_total_estimate_code; ?>" title="">
                  </div>
                </div>
                <!-- col-12 --> 
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="three-flieds">
                    <div class="form-group">
                      <label for="">Status of Job</label>
                      <div class="single">
                        <select name="Job[jobStatus]" class="ui fluid search dropdown" disabled="disabled">
                          <?php 
						  $jobStatus = UtilityManager::jobStatus();
                          foreach($jobStatus as $key=>$jobStatus){ ?>
                          	<option value='<?php echo $key; ?>'><?php echo $jobStatus; ?></option>
                          <?php } ?>
                        </select>
                        <input type="hidden" name="Job[jobStatus]" value="1"  />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="">Select Approval Work Flow</label>
                      <div class="single">
                        <?php 
					   $list = CHtml::listData(Worklflow::model()->findAll("client_id=".Yii::app()->user->id),'id', 'flow_name');
					   echo $form->dropDownList($model, 'work_flow', $list , array('class'=>'ui fluid search dropdown','empty' => '','required'=>'required')); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="">Status Posting Access</label>
                      <div class="single">
                        <?php 
					echo $form->dropDownList($model, 'status_posting_access', UtilityManager::jobPostingStatus() , array('class'=>'ui fluid search dropdown','disabled'=>true)); ?>
                        <input type="hidden" name="Job[status_posting_access]" value="2"  />
                      </div>
                    </div>
                  </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row --> 
              
            </div>
            <!-- well --> 
            
            <br>
            <button type="submit" id="job-submit" class="btn btn-success">Save</button>
            <button type="reset" class="btn btn-default">Cancel</button>
            <?php $this->endWidget(); ?>
          </div>
          <!-- add-job --> 
        </div>
      </div>
      <!-- row --> 
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#Job_num_openings").keypress(function(event) {
			if ( event.which == 45 || event.which == 189 ) {	 
			 }
		}); 
	});
</script>
<?php if($ClientRate){
		if($ClientRate->type == 'Pay Rate'){  ?>
        
<script type="text/javascript">
	//calculating rates of per hour job.
	function calculate(){
		var Job_pay_rate = document.getElementById("Job_pay_rate").value;
		var Job_payment_type = document.getElementById("Job_payment_type").value;
		var Job_hours_per_week = document.getElementById("Job_hours_per_week").value;
		var daterange = document.getElementById("daterange").value;
		var dateField = document.getElementById("dateField").value;
		var markup = document.getElementById("mark_up_percentage").value;
		
		if(Job_payment_type == 'Per Hour Salary'){
			var dates = daterange.split("-");
			var firstDate = new Date(dates[0]);
			var secondDate = new Date(dates[1]);
			
			var numberWeeks = weeks_between(firstDate,secondDate);
			
			var str = Job_hours_per_week;
			var numberHours = str.split(" ",1);
			var estimatedSallary = Job_pay_rate*numberWeeks*numberHours;
			var markUpPercentage = estimatedSallary*markup/100;
						
			document.getElementById("pre_total_estimate_code").value = estimatedSallary+markUpPercentage;
			
			//document.getElementById("pre_total_estimate_code").value = result;
			
			}else if(Job_payment_type == 'Per Month  Salary'){
				
				var dates = daterange.split("-");
				
				var ondDaySallary = Job_pay_rate/30;
				
				var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
				var firstDate = new Date(dates[0]);
				var secondDate = new Date(dates[1]);
				
				var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
				
				var estimatedSallary = ondDaySallary*diffDays;
				var markUpPercentage = estimatedSallary*markup/100;
						
				document.getElementById("pre_total_estimate_code").value = estimatedSallary+markUpPercentage;
				 
				 }else if(Job_payment_type == 'Per Project'){
					 document.getElementById("pre_total_estimate_code").value = Job_pay_rate;
					 }else if(Job_payment_type == ' Per Annum Salary'){
						 var dates = daterange.split("-");
						 var ondDaySallary = Job_pay_rate/365;
						
						var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
						var firstDate = new Date(dates[0]);
						var secondDate = new Date(dates[1]);
						
						var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
						
						var estimatedSallary = ondDaySallary*diffDays;
						var markUpPercentage = estimatedSallary*markup/100;
								
						document.getElementById("pre_total_estimate_code").value = estimatedSallary+markUpPercentage;
						}
		
	}
	
	function weeks_between(date1, date2) {
		// The number of milliseconds in one week
		var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
		// Convert both dates to milliseconds
		var date1_ms = date1.getTime();
		var date2_ms = date2.getTime();
		// Calculate the difference in milliseconds
		var difference_ms = Math.abs(date1_ms - date2_ms);
		// Convert back to weeks and return hole weeks
		return Math.floor(difference_ms / ONE_WEEK);
	}
</script>
<?php }else{ ?>
<script type="text/javascript">
	//calculating rates of per hour job.
	function calculate(){
		var Job_rate = document.getElementById("Job_rate").value;
		var Job_payment_type = document.getElementById("Job_payment_type").value;
		var Job_hours_per_week = document.getElementById("Job_hours_per_week").value;
		var daterange = document.getElementById("daterange").value;
		var dateField = document.getElementById("dateField").value;
		
		if(Job_payment_type == 'Per Hour Salary'){
			var dates = daterange.split("-");
			var firstDate = new Date(dates[0]);
			var secondDate = new Date(dates[1]);
			
			var numberWeeks = weeks_between(firstDate,secondDate);
			
			var str = Job_hours_per_week;
			var numberHours = str.split(" ",1);
			var result = Job_rate*numberWeeks*numberHours;
			
			document.getElementById("pre_total_estimate_code").value = result;
			}else if(Job_payment_type == 'Per Month  Salary'){
				
				var dates = daterange.split("-");
				
				var ondDaySallary = Job_rate/30;
				
				var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
				var firstDate = new Date(dates[0]);
				var secondDate = new Date(dates[1]);
				
				var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
				 document.getElementById("pre_total_estimate_code").value = ondDaySallary*diffDays;
				 }else if(Job_payment_type == 'Per Project'){
					 document.getElementById("pre_total_estimate_code").value = Job_rate;
					 }else if(Job_payment_type == ' Per Annum Salary'){
						 var dates = daterange.split("-");
						 var ondDaySallary = Job_rate/365;
						
						var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
						var firstDate = new Date(dates[0]);
						var secondDate = new Date(dates[1]);
						
						var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
						document.getElementById("pre_total_estimate_code").value = ondDaySallary*diffDays;
						 }
		
	}
	
	function weeks_between(date1, date2) {
		// The number of milliseconds in one week
		var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
		// Convert both dates to milliseconds
		var date1_ms = date1.getTime();
		var date2_ms = date2.getTime();
		// Calculate the difference in milliseconds
		var difference_ms = Math.abs(date1_ms - date2_ms);
		// Convert back to weeks and return hole weeks
		return Math.floor(difference_ms / ONE_WEEK);
	}
</script>
<?php } } ?>