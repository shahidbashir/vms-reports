<!-- Nav tabs -->

<?php $this->renderPartial('_menu',array('model'=>$jobData));  error_reporting(0); ?>
<?php  if($_GET['type'] == 'info'){ ?>
<div class="tab-content p-t-b-0 p-r-12">
  <div class="tab-pane active" id="one" role="tabpanel">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 job-info-left job-info-left-col">
        <p class="m-b-10 bold">Primary Skills</p>
        <?php $skills = $jobData->skills;
				$skills = explode(",",$skills);
				foreach($skills as $value)
				if($value){
					{ ?>
        <span class="label label-info"><?php echo $value; ?></span>
        <?php } } ?>
        <hr>
        <p class="m-b-10 m-t-10 bold">Secondary Skills</p>
        <?php $skills1 = $jobData->skills1;
				$skills1 = explode(",",$skills1);
				foreach($skills1 as $value1)
				if($value1){
					{ ?>
        <span class="label label-default"><?php echo $value1; ?></span>
        <?php } } ?>
        <hr>
        <p class="m-b-10 m-t-10 bold">Good to have</p>
        <?php $skills2 = $jobData->skills2;
				$skills2 = explode(",",$skills2);
				foreach($skills2 as $value2)
				if($value2){
					{ ?>
        <span class="label label-default"><?php echo $value2; ?></span>
        <?php } } ?>
        <hr>
        <p class="m-b-10 m-t-10 bold">Note for Skills</p>
        <div class="well">
          <p><?php echo $jobData->skills_notes ;?></p>
        </div>
        <hr>
        <p class="m-b-10 m-t-10 bold">Job Descriptions</p>
        <div class="well">
          <p><?php echo $jobData->description ;?></p>
        </div>
        <hr>
        <p class="m-b-10 m-t-10 bold">You Will</p>
        <div class="well">
          <p><?php echo $jobData->you_will ;?></p>
        </div>
        <hr>
        <p class="m-b-10 m-t-10 bold">Qualification</p>
        <div class="well">
          <p><?php echo $jobData->qualification ;?></p>
        </div>
        <hr>
        <p class="m-b-10 m-t-10 bold">Additional Information</p>
        <div class="well">
          <p><?php echo $jobData->add_info ;?></p>
        </div>
        <p class="m-b-10 m-t-10 bold">Pre Identity Candidate</p>
        <div class="job-inf-tables left">
          <table class="table table-condensed  ">
            <tbody>
              <tr>
                <td>Do you have Pre Identity Candidate:</td>
                <td><?php if(!empty($jobData->pre_candidate)){ echo $jobData->pre_candidate; }else{ echo 'Null'; } ?></td>
              </tr>
              <tr>
                <td>Candidate Name:</td>
                <td><?php if(!empty($jobData->pre_name)){echo $jobData->pre_name ; }else{ echo 'Null'; }?></td>
              </tr>
              <tr>
                <td>Supplier Name:</td>
                <td><?php if(!empty($jobData->pre_supplier_name)){ echo $jobData->pre_supplier_name ;}else{ echo 'Null'; }?></td>
              </tr>
              <tr>
                <td>Current Rate:</td>
                <td><?php if(!empty($jobData->pre_current_rate)){ echo $jobData->pre_current_rate.' $'; }else{ echo 'Null'; }?></td>
              </tr>
              <tr>
                <td>Payment Type:</td>
                <td><?php if(!empty($jobData->pre_payment_type)){ echo $jobData->pre_payment_type ; }else{ echo 'Null'; }?></td>
              </tr>
            </tbody>
          </table>
        </div>
        <p class="m-b-10 m-t-10 bold">Internal Reference Section</p>
        <div class="job-inf-tables left">
          <?php $JobrequestDepartment = JobrequestDepartment::model()->findByAttributes(array('department'=>$jobData->request_dept,'client_id'=>Yii::app()->user->id)); ?>
          <table class="table table-condensed  ">
            <tbody>
              <tr>
                <td>Request Department:</td>
                <td><?php echo $jobData->request_dept.'('.$JobrequestDepartment->job_request.')' ;?></td>
              </tr>
              <tr>
                <td>Billcode:</td>
                <td><?php echo $jobData->billing_code ;?></td>
              </tr>
              <tr>
                <td>Rate :</td>
                <td> $<?php echo $jobData->bill_rate ;?> </td>
              </tr>
              <tr>
                <td>Reference Code / Project Code:</td>
                <td><?php echo $jobData->pre_reference_code ;?></td>
              </tr>
              <tr>
                <td>Total Estimate Code:</td>
                <td> $<?php echo $jobData->pre_total_estimate_code ;?> </td>
              </tr>
              <tr>
                <td>Status of Job:</td>
                <td><?php $postingstatus = UtilityManager::jobStatus(); echo  $postingstatus[$jobData->jobStatus];?></td>
              </tr>
              <tr>
                <td>Select Approval Work Flow:</td>
                <td><?php $workflow = Worklflow::model()->findByPk($jobData->work_flow); echo $workflow->flow_name ;?>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      
      <!-- col -->
      
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 job-info-right-col">
        <div class="job-inf-tables right ">
          <table class="table table-condensed " >
            <tbody>
              <tr>
                <td style="background: #fff;" colspan="2"><p class="bold">Rates</p></td>
              </tr>
              <?php
              if(!empty($jobData->client_name)){
              $clientName = ProClientNames::model()->findByPk($jobData->client_name);
              if(!empty($clientName)){ ?>
                <tr>
                  <td>client Name:</td>
                  <td><?php echo $clientName->name ;?></td>
                </tr>
              <?php } } ?>
              <tr>
                <td>Bill Rate:</td>
                <td> $<?php echo $jobData->bill_rate ;?> </td>
              </tr>
              <tr>
                <td>Pay Rate:</td>
                <td> $<?php echo $jobData->pay_rate ;?></td>
              </tr>
              <tr>
                <td>Mark Up:</td>
                <td><?php echo $jobData->markup ;?> %</td>
              </tr>
              <tr>
                <td>Estimated Cost:</td>
                <td> $<?php echo $jobData->pre_total_estimate_code ;?></td>
              </tr>
              <tr>
                <td>Days:</td>
                <td><?php
					$dates = explode('-',$jobData->job_po_duration);
					$hours = explode(' ',$jobData->hours_per_week);

					$hoursPerDay = $hours[0]/5;

					$startDate = date('Y-m-d',strtotime($jobData->job_po_duration));
					$endDate = date('Y-m-d',strtotime($jobData->job_po_duration_endDate));
					$workingDays = 0;

					$startTimestamp = strtotime($startDate);
					$endTimestamp = strtotime($endDate);

					for($i=$startTimestamp; $i<=$endTimestamp; $i = $i+(60*60*24) ){
						if(date("N",$i) <= 5) $workingDays = $workingDays + 1;
					}
					echo $workingDays;
					?>
                </td>
              </tr>
              <tr>
                <td>Hours:</td>
                <td><?php echo $workingDays*$hoursPerDay ?></td>
              </tr>
              <tr>
                <td>Cost Center:</td>
                <td><?php echo $jobData->cost_center_code ;?></td>
              </tr>
            </tbody>
          </table>
          <table class="table table-condensed " >
            <tbody>
              <tr>
                <td style="background: #fff; " colspan="2"><p class="bold">Job Tag</p></td>
              </tr>
              <tr>
                <td>Job Type:</td>
                <td><?php echo $jobData->type ;?></td>
              </tr>
              <tr>
                <td>Category:</td>
                <td><?php echo $jobData->cat_id ;?></td>
              </tr>
              <tr>
                <td>Experience:</td>
                <td><?php echo $jobData->experience ;?></td>
              </tr>
              <tr>
                <td>Job Level:</td>
                <td><?php echo $jobData->job_level ;?></td>
              </tr>
              <tr>
                <td>No of Opening</td>
                <td><?php echo $jobData->num_openings ;?></td>
              </tr>
              <tr>
                <td>No of Submission:</td>
                <td><?php echo $jobData->number_submission ;?></td>
              </tr>
              <tr>
                <td>Labour Category:</td>
                <td><?php echo $jobData->labour_cat ;?></td>
              </tr>
            </tbody>
          </table>
          <table class="table table-condensed " >
            <tbody>
              <tr>
                <td style="background: #fff; " colspan="2"><p class="bold">Job Schedule</p></td>
              </tr>
              <tr>
                <td>Job Duration:</td>
                <td><?php echo $jobData->job_po_duration.' - '.$jobData->job_po_duration_endDate ;?></td>
              </tr>
              <tr>
                <td>Hiring Closing Date:</td>
                <td><?php echo $jobData->job_po_duration_endDate ;?></td>
              </tr>
              <tr>
                <td>Shift:</td>
                <td><?php echo $jobData->shift ;?></td>
              </tr>
              <tr>
                <td>Hour Per Week:</td>
                <td><?php echo $jobData->hours_per_week ;?></td>
              </tr>
            </tbody>
          </table>
          <table class="table table-condensed ">
            <tbody>
              <tr>
                <td style="background: #fff; " colspan="2"><p class="bold">Job Location:</p></td>
              </tr>
              <tr>
                <td>Location Name:</td>
                <td>Address</td>
              </tr>
              <?php $Location = unserialize($jobData->location);
					foreach($Location as $value) {
						$location = Location::model()->findByPk($value);
						?>
              <tr>
                <td><?php echo $location->name; ?></td>
                <td><?php echo $location->address1; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      
      <!-- col --> 
      
      <br>
    </div>
  </div>
</div>
<?php } ?>


<?php if($_GET['type']=='overview'){
		$this->renderPartial('jobviewCharts',array('model'=>$jobData));
	 } ?>
</div>
</div>

<!-- col -->

</div>

<!-- row -->

</div>
</div>
