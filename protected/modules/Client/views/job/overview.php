<?php 	/*echo '<pre>';	print_r($jobData);	exit;*/ ?>
<header class="db-header">
  <div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
      <?php $this->pageTitle = ''; ?>
      <h4>
        <?php  echo $jobData->title; ?>
      </h4>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
      <div class="right">
        <ul class="list-inline pull-right">
          <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobListing'); ?>">Back to Job Listing</a></li>
          <li><a href="">Previous Job</a></li>
          <li><a href="">Next Job</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- row --> 
</header>
<div class="page gray-bg">
  <div class="card p-0">
    <div class="job-detail">
      <div class="row m-0">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="client-listing-nav">
            <nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display --> <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse navbar-ex2-collapse">
                <?php $this->renderPartial('jobsdetail_tabs_header'); ?>
              </div>
              <!-- /.navbar-collapse --> </nav>
          </div>
          <!-- client-listing-nav --> </div>
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="page-wraper">
            <div class="db-content-wraper">
              <div class="page-content-outer">
                <div class="page-content">
                  <div class="job-detail-inner">
                  
                  <?php 
				  $postingstatus = UtilityManager::jobPostingStatus();
				  if($postingstatus[$jobData->status_posting_access] == '2'){ ?>
                  <div class="alert alert-danger">
                    <strong>The Job need initial approval  -  Pending Internal Approval</strong>
                 </div>
                  <?php  } ?>
                    <div class="daily-submission">
                      <div class="daily-submission-header">
                        <h4>DAILY SUBMISSON</h4>
                      </div>
                      <!-- daily-submission-header -->
                      <div class="daily-submission-content"> </div>
                    </div>
                    <!-- daily-submission -->
                    <p><a href="" class="btn btn-default daily">DAILY SUBMISSION</a></p>
                    <br>
                    <br>
                    <div class="interviews">
                      <div class="nav-container">
                        <div class="navbar">
                          <ul class="nav navbar-nav">
                            <li class="active"> <a href="#">TODAYS INTERVIEWS</a> </li>
                            <li> <a href="#">COMMENTS</a> </li>
                          </ul>
                          <a href="" class="btn btn-green-parot pull-right">NEW NOTE</a> </div>
                      </div>
                      <!-- nav-container -->
                      <div class="interview"> <span class="date">10TH MAY 2015</span>
                        <div class="media"> <a class="pull-left" href="#"> <span class="time">10:30</span> </a>
                          <div class="media-body">
                            <p>Interview for <a href="">David Web ( 3465741654 ) </a> at 10 Exchange place New Jersey, USA. </p>
                            <p>Kumar Arun ( Moible Nummber will show here ) interview for this schedule.</p>
                            <footer> <a href="">CANCEL INTERVIEW</a> <a href="">RESCHEDULE INTERVIEW</a> </footer>
                          </div>
                        </div>
                      </div>
                      <!-- interview -->
                      <div class="interview"> <span class="date">10TH MAY 2015</span>
                        <div class="media"> <a class="pull-left" href="#"> <span class="time">10:30</span> </a>
                          <div class="media-body">
                            <p>Interview for <a href="">David Web ( 3465741654 ) </a> at 10 Exchange place New Jersey, USA. </p>
                            <p>Kumar Arun ( Moible Nummber will show here ) interview for this schedule.</p>
                            <footer> <a href="">CANCEL INTERVIEW</a> <a href="">RESCHEDULE INTERVIEW</a> </footer>
                          </div>
                        </div>
                      </div>
                      <!-- interview -->
                      <div class="interview"> <span class="date">10TH MAY 2015</span>
                        <div class="media"> <a class="pull-left" href="#"> <span class="time">10:30</span> </a>
                          <div class="media-body">
                            <p>Interview for <a href="">David Web ( 3465741654 ) </a> at 10 Exchange place New Jersey, USA. </p>
                            <p>Kumar Arun ( Moible Nummber will show here ) interview for this schedule.</p>
                            <footer> <a href="">CANCEL INTERVIEW</a> <a href="">RESCHEDULE INTERVIEW</a> </footer>
                          </div>
                        </div>
                      </div>
                      <!-- interview -->
                      <div class="interview"> <span class="date">10TH MAY 2015</span>
                        <div class="media"> <a class="pull-left" href="#"> <span class="time">10:30</span> </a>
                          <div class="media-body">
                            <p>Interview for <a href="">David Web ( 3465741654 ) </a> at 10 Exchange place New Jersey, USA. </p>
                            <p>Kumar Arun ( Moible Nummber will show here ) interview for this schedule.</p>
                            <footer> <a href="">CANCEL INTERVIEW</a> <a href="">RESCHEDULE INTERVIEW</a> </footer>
                          </div>
                        </div>
                      </div>
                      <!-- interview --> </div>
                    <!-- interviews --> </div>
                  <!-- job-detail-inner --> <!-- /*  End inner cotent  */ -->
                  <div class="quick-detail hide">
                    <div class="quick-info box"> <a href="" class="close">Close</a>
                      <div class="quick-info-header">
                        <h4>QUICK INFORMATION</h4>
                      </div>
                      <div class="quick-info-content">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>JOB TYPE</th>
                              <th>PAYMENT</th>
                              <th>NUMBER OF OPENINGS</th>
                              <th>JOB CLOSING DATE</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><?php  echo $jobData->type; ?></td>
                              <td><?php echo $jobData->rate.' '.$jobData->currency ?> - <?php echo $jobData->payment_type ?></td>
                              <td><?php  echo $jobData->num_openings ?></td>
                              <td><!--10-02-2015 ( 9 More Left) --><?php  echo $jobData->desired_start_date; ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <br>
                    <div class="job-workflow-info">
                      <ul class="list-inline b-b">
                        <li>
                          <h4>Pre Indentity Candidate</h4>
                          <p><?php  echo $jobData->pre_name; ?></p>
                        </li>
                        <li>
                          <h4>Supplier Name</h4>
                          <p><?php  echo $jobData->pre_supplier_name; ?></p>
                        </li>
                        <li>
                          <h4>Rate for Candidate</h4>
                          <p><?php  echo $jobData->pre_current_rate; ?></p>
                        </li>
                      </ul>
                      <p class="h4">Internal Reference</p>
                      <ul class="list-inline">
                        <li>
                          <p class="m-6">Reference Code / Project Code</p>
                          <h4><?php  echo $jobData->pre_reference_code; ?></h4>
                        </li>
                        <li>
                          <h4>Estimated Cost</h4>
                          <p><?php  echo ($jobData->pre_total_estimate_code=='')?0:$jobData->pre_total_estimate_code; ?></p>
                        </li>
                        <li>
                          <h4>Workflow</h4>
                          <p><?php $Workflow = Worklflow::model()->findByPk($jobData->work_flow);  echo $Workflow->flow_name; ?></p>
                        </li>
                        <!--<li> <span class="label">WORKFLOW APPROVED</span> </li>-->
                      </ul>
                    </div>
                    <!-- job-workflow-info --> <br>
                    <div class="skills-detail">
                      <div class="skills-info box">
                        <div class="skills-info-header">
                          <h4>SKILLS</h4>
                        </div>
                        <div class="skills-info-content">
                        
                        <?php  $skillsArray =  explode(",",$jobData->skills) ;
						foreach($skillsArray as $skillsValue){
						 ?>
                        
                        <span class="label label-info">                        
                        	<?php echo $skillsValue; ?>
                        </span> 
                        
                        <?php } ?>
                        
                        
                         </div>
                      </div>
                    </div>
                    <!-- skills-detail --> <br>
                    <div class="description-detail">
                      <div class="description-info box">
                        <div class="description-info-header">
                          <h4>JOB DESCRIPTION</h4>
                        </div>
                        <div class="description-info-content">
                          <p><?php echo $jobData->description; ?></p>
                        </div>
                      </div>
                    </div>
                    <!-- description-detail --> <br>
                    <div class="description-detail">
                      <div class="description-info box">
                        <div class="description-info-header">
                          <h4>YOU WILL</h4>
                        </div>
                        <div class="description-info-content">
                          <p><?php echo $jobData->you_will; ?></p>
                        </div>
                      </div>
                    </div>
                    <!-- description-detail --> <br>
                    <div class="description-detail">
                      <div class="description-info box">
                        <div class="description-info-header">
                          <h4>QUALIFICATIONS</h4>
                        </div>
                        <div class="description-info-content">
                          <p><?php echo $jobData->qualification; ?></p>
                        </div>
                      </div>
                    </div>
                    <!-- description-detail --> <br>
                    <div class="description-detail">
                      <div class="description-info box">
                        <div class="description-info-header">
                          <h4>ADDITIONAL INFORMATION</h4>
                        </div>
                        <div class="description-info-content">
                          <p><?php echo $jobData->add_info; ?></p>
                        </div>
                      </div>
                    </div>
                    <!-- description-detail --> <br>
                    <div class="location-detail">
                      <div class="location-info box">
                        <div class="location-info-header">
                          <h4>LOCATIONS</h4>
                        </div>
                        <div class="location-info-content">
                          <div class="row">
                            <?php   $locations = unserialize($jobData->location);                        
							foreach ($locations as $keyloc => $valueloc) {						  
							 echo '<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">';                          
							 echo 'Location <br/> ';                         
							 echo '<p>';                          
							 echo $valueloc.'</p>';						  
							 echo '</div>';                        
							 } ?>
                           </div>
                          <!-- row --> 
                        </div>
                      </div>
                    </div>
                    <!-- description-detail -->
                    <div class="seprater-bottom-50"></div>
                  </div>
                  <!-- quick-detail --> </div>
                <!-- page-content --> </div>
              <!-- page-content-outer -->
              <?php $this->renderPartial('sidebar',array('jobData'=>$jobData,'invitedTeamMembers'=>$invitedTeamMembers)); ?>
              <!-- page-sidebar --> </div>
            <!-- db-content-wraper --> </div>
          <!-- page-wraper --> </div>
      </div>
      <!-- row --> </div>
  </div>
</div>
