
<?php

	if(isset($_GET['id'])){
		$jobID = $_GET['id'];
		}else{
			 $jobID = $_GET['jobid'];
			 }
	
 ?>




<?php
$totalsubmission = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>'Submitted'));
$totalInterview = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>'Interview'));
$totaloffer = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>'Invite for this job'));
$Shortlisted = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>'Shortlisted'));
?>

<div class="page-sidebar">
  <div class="job-title">
    <h3>
      <?php  echo $jobData->title; ?>
    </h3>
    <p><a href="" class="job-detaila">JOB DETIALS</a> </p>
    <span class="fa fa-caret-left"></span> </div>
  <div class="score-card">
    <p class="h4">Hiring Score Card</p>
    <div class="label"> Total Hire:
      <?php  echo $jobData->num_openings; ?>
      Opening </div>
    <div class="table-container">
      <table class="table">
        <tbody>
          <tr>
            <td>Today Submission</td>
            <td><?php if($totalsubmission){ echo $totalsubmission;}else { echo '0'; } ?></td>
          </tr>
          <tr>
            <td>Resume Submision</td>
            <td><?php if($totalsubmission){ echo $totalsubmission;}else { echo '0'; }  ?></td>
          </tr>
          <tr>
            <td>Assignemnet Process</td>
            <td><?php if($Shortlisted){ echo $Shortlisted;}else { echo '0'; } ?></td>
          </tr>
          <tr>
            <td>Phone Interview</td>
            <td><?php if($totalInterview){ echo $totalInterview;}else { echo '0'; }  ?></td>
          </tr>
          <tr>
            <td>Onsite interview</td>
            <td><?php if($totalInterview){ echo $totalInterview;}else { echo '0'; } ?></td>
          </tr>
          <tr>
            <td>Offer</td>
            <td><?php if($totaloffer){ echo $totaloffer;}else { echo '0'; } ?></td>
          </tr>
        </tbody>
      </table>
    </div>
    
    <!-- table-container -->
    
    <div class="member-list">
      <p>Hired Applicant</p>
      <!--<ul class="list-inline">
        <li>
          <div class="avater">MP</div>
        </li>
        <li>
          <div class="avater">MP</div>
        </li>
      </ul>-->
    </div>
    <p>Bill Rates</p>
    <h4><?php echo $jobData->rate.' '.$jobData->currency ?> - <?php echo $jobData->payment_type ?></h4>
    <br>
    <div class="member-list">
      <p>Team Members</p>
      <ul class="list-inline">
        <?php foreach($invitedTeamMembers as $value) {?>
        <li>
          <div class="avater"><?php echo $value['first_name'][0]; ?><?php echo $value['last_name'][0]; ?></div>
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>
</div>
