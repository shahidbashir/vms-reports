

<div class="main-content">

<?php if(Yii::app()->user->hasFlash('success')):?>
	<?php echo Yii::app()->user->getFlash('success'); ?>
<?php endif; ?>

<?php
	$userID = Yii::app()->user->id;
	$ClientRate = ClientRate::model()->findByAttributes(array('client_id'=>$userID));
	/*echo '<pre>';
	print_r($ClientRate);
	exit;*/
	
?>

  <div class="row"> 
    <!--Condensed Table--> 
    
    <!--Hover table-->
    <div class="col-sm-12">
      <div class="row"> </div>
    </div>
    
    <!--Hover table--> 
    
    <!--Hover table-->
    <div class="col-sm-12">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-heading"></div>
            <div class="panel-body">
            
			<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'job-form',
                    'enableAjaxValidation'=>false,
                ));
                $loginUserId = Yii::app()->user->id;
                ?>
            <?php //echo $form->errorSummary($model); ?>
            
            
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="">Job Title</label>
                    <input type="text" name="Job[title]" value="<?php echo Yii::app()->session['job_title']; ?>" class="form-control" title="Job title must be letter" required="required">
                    <?php echo $form->error($model,'title'); ?>
                  </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Job Type</label>
                    <?php
					 $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=11')),'title', 'title');
					 echo $form->dropDownList($model, 'type', $list , array('class'=>'form-control','empty' => '','required'=>'required'));
					 ?>
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Category</label>
                    <?php
				   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'title', 'title');
				   echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Experiences</label>
                    <?php
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=10')),'title', 'title');
                   echo $form->dropDownList($model, 'experience', $list , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Job Level</label>
                    <?php
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=40')),'title', 'title');
                   echo $form->dropDownList($model, 'job_level', $list , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                  </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row -->
              
              
              
              <div class="hirring-pipline">
                <div class="row">
                
                
                
                
                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      <label for="">Primary Skill</label>
                      <select class="tokenize-remote-demo1" name="Job[skills][]" id="job_skills" multiple></select>
                      <!--<input type="text" name="Job[skills]" class="form-control tokenize-remote-demo1" id="job_skills">-->
                    </div>
                    <div class="form-group">
                      <label for="">Secondary Skill</label>
                      <select class="tokenize-remote-demo1" name="Job[skills1][]" id="job_skills1" multiple></select>
                      <!--<input type="text" name="Job[skills1]" class="form-control tokenize-remote-demo1" id="job_skills1">-->
                    </div>
                    <div class="form-group">
                      <label for="">Good to have.</label>
                      <select class="tokenize-remote-demo1" name="Job[skills2][]" id="job_skills2" multiple></select>
                      <!--<input type="text" name="Job[skills2]" class="form-control tokenize-remote-demo1" id="job_skills2">-->
                    </div>
                  </div>
                   
                  
                </div>
                <!-- row -->
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for="">Note for Skills</label>
                      <textarea name="Job[skills_notes]" id="input" class="form-control" rows="5" required="required"></textarea>
                    </div>
                  </div>
                </div>
                <!-- row --> 
                
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Location</label>
                    <select  multiple="" class="select2" name="Job[location][]" required >
                      <option value=""></option>
                      <?php
                    $location = Location::model()->findAllByAttributes(array('reference_id'=>$loginUserId));
                    foreach($location as $location){ ?>
                      <option value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                      <?php  } 	?>
                    </select>
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Number of Openings</label>
                    <?php echo $form->numberField($model,'num_openings',array('class'=>'form-control','min'=>'0')); ?>
                  </div>
                </div>
                <!-- col --> 
                
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="panel-group" id="accordion">
                    <div class="panel panel-info">
                      <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                        <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed"> Job Description</a> </h4>
                      </div>
                      <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                          <textarea name="Job[description]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-info">
                      <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                        <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false"> You Will</a> </h4>
                      </div>
                      <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                          <textarea name="Job[you_will]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-info">
                      <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                        <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false"> Qualifications</a> </h4>
                      </div>
                      <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                          <textarea name="Job[qualification]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-info">
                      <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                        <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed" aria-expanded="false"> Additional Information</a> </h4>
                      </div>
                      <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                          <textarea name="Job[add_info]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- col-12 --> 
              </div>
              <!-- row --> 
              
              <br>
              
              <input type="hidden" id="calculated_value" name="calculated_value" value="" class="form-control"  />
              <input type="hidden" id="bill_rate_hidden" name="bill_rate_hidden" value="" class="form-control"  />
              
              <!--on ajax call find the vendor highest markup and fill its value in this field--> 
              <input type="text" name="Job[vendor_client_markup]" id="vendor_client_markup" class="form-control" value="">
              
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <table class="table no-border">
                    <tbody>
                      <tr>
                         
                         <td style="width: 48%;"><div class="form-group">
                            <label for="">Bill Rate</label>
                            <?php echo $form->numberField($model,'bill_rate',array('class'=>'form-control','min'=>'.1','step' => "any",'onChange'=>'calculate("billrate")','required'=>'required')); ?>
                            
                          </div></td>
                           
                        
                        <?php if($ClientRate->type == 'Fixed Markup'){ ?>
                        <!--<td style="width: 4%;"> or </td>
                        <td style="width: 48%;"><div class="form-group">
                            <label for="">Pay Rate</label>-->
                            <?php echo $form->hiddenField($model,'pay_rate',array('class'=>'form-control','min'=>'.1','step' => "any", 'onChange'=>'calculate("payrate")')); ?>
                          <!--</div>--></td>
                          
                        <?php }else{ ?>
                        <input type="hidden" name="Job[pay_rate]" id="Job_pay_rate" value="0" class="form-control" />
                        <?php } ?>
                        
                       
                        
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- col -->
                
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Select Pay Rate Type</label>
                    <?php
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                   echo $form->dropDownList($model, 'payment_type', $list , array('class'=>'form-control','empty' => '','required'=>'required')/*,'onChange'=>'calculate()'*/);  ?>
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="">Hours Per Week</label>
                    <?php
                   $hours_per_week = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=16')),'title', 'title');
                   echo $form->dropDownList($model, 'hours_per_week', $hours_per_week , array('class'=>'form-control','empty' => '','onChange'=>'calculate()','required'=>'required')); ?>
                  </div>
                </div>
                <!-- col --> 
                
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="">Job Duration</label>
                    <input type="text" class="form-control daterange" name="job_po_duration" id="daterange" placeholder="" onchange="calculate()" required="required">
                  </div>
                </div>
                <!-- col -->
                
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="form-group">
                        <label for="">Closing Date for Job</label>
                        <input type="text" class="form-control" name="desired_start_date" required="required" id="dateField" placeholder="">
                      </div>
                    </div>
                    <!-- col -->
                    
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="form-group">
                        <label for="">Shift</label>
                         <?php
                   $shift = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=15')),'title', 'title');
                   echo $form->dropDownList($model, 'shift', $shift , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                      </div>
                    </div>
                    <!-- col --> 
                    
                  </div>
                  <!-- row --> 
                </div>
                <!-- col --> 
                
              </div>
              <!-- row -->
              
              <hr>
              
              
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="">Invite your Team member to Collaborateel</label>
                    <select multiple="" class="select2" name="Job[invite_team_member][]" >
                      <option value=""></option>
                      <?php
                    $friend = Client::model()->findAllByAttributes(array('super_client_id'=>$loginUserId));
                    foreach($friend as $friend){ ?>
                      <option value="<?php echo $friend->id; ?>"><?php echo $friend->first_name.' ('.$friend->email.')'; ?></option>
                      <?php  } 	?>
                    </select>
                  </div>
                </div>
                <!-- col --> 
              </div>
              <!-- row -->
              
              <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group">
                    <label for="">Labor Category</label>
                     <?php
                   $labour_cat = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=14')),'title', 'title');
                   echo $form->dropDownList($model, 'labour_cat', $labour_cat , array('class'=>'form-control','empty' => '')); ?>
                  </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group">
                    <label for="">Number of Submission</label>
                    <?php echo $form->numberField($model,'number_submission',array('class'=>'form-control','min' => 1)); ?>
                  </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <div class="form-group">
                    <label for="">Background Verification</label>
                    <?php
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=41')),'title', 'title');
                   echo $form->dropDownList($model, 'background_verification', $list , array('class'=>'form-control','empty' => '','required'=>"required")); ?>
                  </div>
                </div>
              </div>
              <!-- row -->
              
              <div class="well">
                <h4> Pre Identity Candidate </h4>
                <br>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for="">Do you have Pre Identity Candidate</label>
                      <select name="Job[pre_candidate]" id="pre_candidate" class="form-control">
                        <option value=""></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="">&nbsp;</label>
                    <div class="row">
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <input type="text" name="Job[pre_name]" class="form-control" id="pre_name" placeholder="Candidate Name">
                        </div>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <input type="text" name="Job[pre_supplier_name]" class="form-control" id="pre_supplier_name" placeholder="Supplier Name">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- row -->
                
                <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for="">Current Rate</label>
                      <input type="text" class="form-control" id="pre_current_rate"  name="Job[pre_current_rate]">
                    </div>
                  </div>
                  <!-- col -->
                  
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for=""> Payment Type</label>
                      <?php
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                   echo $form->dropDownList($model, 'pre_payment_type', $list , array('id'=>'pre_payment_type','class'=>'form-control pre_fields','empty' => '','required'=>'required')); //,'disabled'=>true ?>
                    </div>
                  </div>
                  <!-- col --> 
                  
                </div>
                <!-- row --> 
                
              </div>
              <!-- well -->
              
              <div class="well well-info">
                <h4>Internal Reference Section</h4>
                <br>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for="">Request Department</label>
                      <select class="form-control" name="Job[request_dept]">
                        <option value=""></option>
                        <?php
                    $JobrequestDepartment = JobrequestDepartment::model()->findAllByAttributes(array('client_id'=>$loginUserId));
                    foreach($JobrequestDepartment as $JobrequestDepartment){ ?>
                        <option value="<?php echo $JobrequestDepartment->department; ?>"><?php echo $JobrequestDepartment->job_request; ?></option>
                        <?php  }  ?>
                        </select>
                        
                    </div>
                  </div>
                  <!-- col -->
                  
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <label for="">Billcode</label>
                      <select class="form-control" name="Job[billing_code]">
                        <option value=""></option>
                        <?php
                    $BillingcodeDepartment = BillingcodeDepartment::model()->findAllByAttributes(array('client_id'=>$loginUserId));
                    foreach($BillingcodeDepartment as $BillingcodeDepartment){ ?>
                        <option value="<?php echo $BillingcodeDepartment->billingcode; ?>"><?php echo $BillingcodeDepartment->billingcode; ?></option>
                        <?php  }  ?>
                      </select>
                    </div>
                  </div>
                  <!-- col --> 
                  
                </div>
                <!-- row -->
                
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="">Rate</label>
                    <br>
                    <div class="be-checkbox">
                      <input id="check1"  name="Job[over_time]" type="checkbox"  value="1">
                      <label for="check1">OverTime</label>
                    </div>
                    <div class="be-checkbox">
                      <input id="check2" type="checkbox" name="Job[double_time]" value="1">
                      <label for="check2">Double Time</label>
                    </div>
                  </div>
                  <!-- col --> 
                </div>
                <!-- row --> 
                
                <br>
                <div class="row">
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="">Reference Code / Project Code </label>
                      <input type="text" id="pre_reference_code"  name="Job[pre_reference_code]" class="form-control">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for=""> Total Estimation Cost </label>
                      <input type="text" id="pre_total_estimate_code"  name="Job[pre_total_estimate_code]" class="form-control" required="required" readonly="readonly">
                    </div>
                  </div>
                  <!-- col-12 --> 
                </div>
                <!-- row -->
                
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="three-flieds">
                      <div class="form-group">
                        <label for="">Select Approval Work Flow</label>
                        <?php
					   $list = CHtml::listData(Worklflow::model()->findAll("client_id=".Yii::app()->user->id),'id', 'flow_name');
					   echo $form->dropDownList($model, 'work_flow', $list , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
                      </div>
                      
                      <div class="form-group">
                        <label for="">Status of Job</label>
                        <select name="Job[jobStatus]" class="form-control" disabled="disabled">
                          <?php $jobStatus = UtilityManager::jobStatus();
                        foreach($jobStatus as $key=>$jobStatus){ ?>
                          <option value='<?php echo $key; ?>'><?php echo $jobStatus; ?></option>
                          <?php } ?>
                        </select>
                        <input type="hidden" name="Job[jobStatus]" value="1"  />
                      </div>
                    </div>
                  </div>
                  <!-- col --> 
                </div>
                <!-- row --> 
                
              </div>
              <!-- well --> 
              <br>
              <div>
                <button type="submit" id="job-submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-default">Cancel</button>
              </div>
              
              <?php $this->endWidget(); ?>
              
              <br>
              <br>
            </div>
            <!-- panel-body --> 
          </div>
        </div>
      </div>
    </div>
    
    <!--Hover table--> 
    
  </div>
</div>

<script type="text/javascript">
	//calculating rates of per hour job.
	function calculate(type,payrate){
		var Job_bill_rate = document.getElementById("Job_bill_rate").value;
		var Job_pay_rate = document.getElementById("Job_pay_rate").value;
		var Job_payment_type = document.getElementById("Job_payment_type").value;
		var Job_hours_per_week = document.getElementById("Job_hours_per_week").value;
		var daterange = document.getElementById("daterange").value;
		var dateField = document.getElementById("dateField").value;
		var markup = document.getElementById("vendor_client_markup").value;
		var bill_rate_hidden = document.getElementById("bill_rate_hidden").value;
		
		
		//if(Job_payment_type == 'Per Hour Salary'){
			var dates = daterange.split("-");
			
			//Usage
			var startDate = new Date(dates[0]);
			var endDate = new Date(dates[1]);
			var numOfDates = getBusinessDatesCount(startDate,endDate);
			//alert(numOfDates);
			if(numOfDates==0){
				var numOfDates = 1;
				}
			
			var str = Job_hours_per_week;
			var numberHours = str.split(" ",1);
			
			var hoursPerDay = numberHours/5;
			
			//alert(hoursPerDay);
			
			if(type == 'billrate'){
				var pay_rate = Job_bill_rate * 100/( 100 + parseFloat(markup));
				document.getElementById("Job_pay_rate").value = pay_rate.toFixed(3);
				document.getElementById("calculated_value").value = pay_rate.toFixed(3);
				//billrate value will be saved to this hidden field
				document.getElementById("bill_rate_hidden").value = Job_bill_rate;
			}else if(type == 'payrate'){
				  var bill_rate = parseFloat(Job_pay_rate) + (parseFloat(Job_pay_rate) * parseFloat(markup)/100);
				  document.getElementById("Job_bill_rate").value = bill_rate.toFixed(3);
				  document.getElementById("calculated_value").value = bill_rate.toFixed(3);
				  document.getElementById("bill_rate_hidden").value = bill_rate.toFixed(3);
				 }
				 document.getElementById("pre_total_estimate_code").value = bill_rate_hidden * numOfDates * hoursPerDay;
				
			//}
		
	}	
	
	//testing this one
	function getBusinessDatesCount(startDate, endDate) {
      var count = 0;
      var curDate = startDate;
      while (curDate <= endDate) {
        var dayOfWeek = curDate.getDay();
        if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
           count++;
        curDate.setDate(curDate.getDate() + 1);
      }
      return count;
    }
    
    
    
	
	
	
	
</script>


<!--<style>
  .tokenfield.form-control {
    height: auto !important;
  }
</style>-->