<?php
if(isset($_GET['id'])){
	$jobID = $_GET['id'];
	}else{
		 $jobID = $_GET['jobid'];
		 }
	$today = '';
	$pending = '';
	$cancel = '';
	$all = '';
	if(isset($_GET['st'])){
		if($_GET['st']=='pending'){
			$pending = 'active';
		}else if($_GET['st']=='cancel'){
			 	$cancel = 'active';
			  }else {
			 	$all = 'active';
			  }
	}else{
		 $today = 'active';
		 }
 ?>
<div class="navbar">
  <ul class="nav navbar-nav">
    <li class="<?php echo $today; ?>"> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/index',array('jobid'=>$jobID,'type'=>'interview')); ?>">Today Schedule</a>
    <li class="<?php echo $all; ?>"> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/allinterview',array('jobid'=>$jobID,'type'=>'interview','st'=>'all')); ?>">All Schedule</a> </li>
    <li class="<?php echo $pending; ?>"> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/index',array('jobid'=>$jobID,'type'=>'interview','st'=>'pending')); ?>">Pending Approval </a> </li>
    <li class="<?php echo $cancel; ?>"> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/cancel',array('jobid'=>$jobID,'type'=>'interview','st'=>'cancel')); ?>">Cancelled</a> </li>
    <li> <a href="interview-calendar-schedule.html">Calendar View</a> </li>
  </ul>
</div>
