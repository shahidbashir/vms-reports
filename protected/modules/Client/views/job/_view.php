<?php
/* @var $this JobController */
/* @var $data Job */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cat_id')); ?>:</b>
	<?php echo CHtml::encode($data->cat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reference_id')); ?>:</b>
	<?php echo CHtml::encode($data->reference_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skills')); ?>:</b>
	<?php echo CHtml::encode($data->skills); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
	<?php echo CHtml::encode($data->location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num_openings')); ?>:</b>
	<?php echo CHtml::encode($data->num_openings); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('experience')); ?>:</b>
	<?php echo CHtml::encode($data->experience); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('resume_submission')); ?>:</b>
	<?php echo CHtml::encode($data->resume_submission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('assignment_process')); ?>:</b>
	<?php echo CHtml::encode($data->assignment_process); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_interview')); ?>:</b>
	<?php echo CHtml::encode($data->phone_interview); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('onsite_interview')); ?>:</b>
	<?php echo CHtml::encode($data->onsite_interview); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('offer')); ?>:</b>
	<?php echo CHtml::encode($data->offer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currency')); ?>:</b>
	<?php echo CHtml::encode($data->currency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_type')); ?>:</b>
	<?php echo CHtml::encode($data->payment_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invite_team_member')); ?>:</b>
	<?php echo CHtml::encode($data->invite_team_member); ?>
	<br />

	*/ ?>

</div>