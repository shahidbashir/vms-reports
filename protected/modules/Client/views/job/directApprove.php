<div class="main-content">
  <div class="splash-container">
    <div class="panel panel-default panel-border-color panel-border-color-primary">
    <?php if($this->action->id == 'directApprove'){ ?>
      <div class="panel-body workflow-approval">
        <form action="index.html" method="get">
          <div class="text-center m-t-20 success"> <i class="fa fa-check-circle-o"></i> </div>
          <!--<div class="text-center m-t-20 failed"> <i class="fa fa-close"></i> </div>-->
          <h2 class="text-center m-b-20">Job has been successfully approved.</h2>
          <p style="text-align:center"><strong>Job ID :</strong> <?php echo $model->id; ?> </p>
          <p class="m-b-50" style="text-align:center"><strong>Jon Name :</strong> <?php echo $model->title; ?> </p>
          <div class="text-center m-b-50">
            <!--<button type="button" class="btn btn-success">Continue</button>-->
            <a href="<?php echo Yii::app()->homeUrl; ?>" class="btn btn-success">Continue</a>
          </div>
        </form>
      </div>
    <?php }else{ ?>
      <div class="panel-body workflow-approval">
        <form action="index.html" method="get">
          <div class="text-center m-t-20 success"> <i class="fa fa-check-circle-o"></i> </div>
          <!--<div class="text-center m-t-20 failed"> <i class="fa fa-close"></i> </div>-->
          <h2 class="text-center m-b-20">You have approved interview successfully</h2>
          <p style="text-align:center"><strong>Job ID :</strong> <?php echo $model->id; ?> </p>
          <div class="text-center m-b-50">
            <!--<button type="button" class="btn btn-success">Continue</button>-->
            <a href="<?php echo Yii::app()->homeUrl; ?>" class="btn btn-success">Continue</a>
          </div>
        </form>
      </div>
    <?php } ?>
    </div>
  </div>
</div>
