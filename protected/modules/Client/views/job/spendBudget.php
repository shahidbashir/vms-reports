<?php $this->renderPartial('_menu',array('model'=>$jobData));
$contract = Contract::model()->countByAttributes(array('job_id'=>$jobData->id));
$contractData = Contract::model()->findAllByAttributes(array('job_id'=>$jobData->id));
?>
<div class="tab-content ">
    <div class="tab-pane active" id="one" role="tabpanel">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <p class="bold m-t-10 ">Budget Spend</p>

                <div class="row m-t-10">
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                        <p> <strong>Department Name :</strong><?php echo $jobData->request_dept; ?></p>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                        <p> <strong> Job Category :</strong><?php echo $jobData->cat_id; ?></p>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <p class="text-right"> <strong> Number of Hire :</strong><?php echo $contract; ?></p>
                    </div>

                </div>

                <br>
                <br>
                    <?php if(Yii::app()->controller->action->id=='spendBudget') { ?>
                <table class="table m-b-40 without-border">
                    <thead class="thead-default">
                    <tr>
                        <th  style="width: 10px;">S.No</th>
                        <th>
                            Name of Candidate
                        </th>
                        <th>Total Estimated Cost</th>
                        <th>Bill Rate</th>
                        <th>Hours</th>
                        <th>Actual Estimated</th>
                        <th>Savings</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($contractData){
                        $i=0;
                        foreach ($contractData as $data){
                            $i++;
                            $candidate = Candidates::model()->findByPk($data->candidate_id);
                            $job = Job::model()->findByPk($data->job_id);
                            $submission = VendorJobSubmission::model()->findByPk($data->submission_id);
                            $billrate = $submission->candidate_pay_rate + $submission->candidate_pay_rate * ($submission->makrup)/100;
                            $t1 = StrToTime ($data->end_date);
                            $t2 = StrToTime ($data->start_date);
                            $diff = $t1 - $t2;
                            $hours = $diff / ( 60 * 60 );
                            /*$diff = strtotime($data->end_date) - strtotime($data->start_date);
                            $diff_in_hrs = $diff/3600;
                            print_r($diff_in_hrs);
                            exit;*/
                            $ActualEstimatedCost = $billrate * $hours;
                            $Saving  = $job->pre_total_estimate_code-$ActualEstimatedCost;
                            ?>
                    <tr>

                        <td><?php echo $i; ?></td>
                        <td><a href="<?php echo $this->createAbsoluteUrl('/Client/job/individualspendBudget',array('id'=>$data->job_id,'can_id'=>$candidate->id,'type'=>'spendBudget')); ?>"><?php echo $candidate->first_name.' '.$candidate->last_name; ?> <br> ( <?php echo date('m-d-Y',strtotime($data->start_date)).' To '.date('m-d-Y',strtotime($data->end_date)); ?> )</a></td>
                        <td>$<?php echo $job->pre_total_estimate_code; ?></td>

                        <td>$<?php echo number_format($billrate, 2);?></td>

                        <td><?php echo $hours; ?></td>

                        <td>$<?php echo $ActualEstimatedCost; ?></td>

                        <td>$<?php echo $Saving; ?></td>


                        <td style="text-align: center">
                            <a href="<?php echo $this->createAbsoluteUrl('/Client/job/individualspendBudget',array('id'=>$data->job_id,'can_id'=>$candidate->id,'type'=>'spendBudget')); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                        </td>
                    </tr>
                    <?php } } ?>
                    </tbody>
                </table>
                    <?php }else{ ?>
                        <div style="margin-bottom: 4%;">
                        <button onclick="goBack()" class="btn btn-sm btn-default-3 pull-right">Go Back</button>
                        </div>
                        <table class="table m-b-40 without-border">
                            <thead class="thead-default">
                            <tr>
                                <th  style="width: 10px;">S.No</th>
                                <th>
                                    Month
                                </th>
                                <th>Estimated Hours</th>
                                <th>Estimated Cost</th>
                                <th>Total Hour</th>
                                <th>Actual Cost</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; $totalestimatedhour = 0; $totalMonth =0; $totaleestimatedcost = 0; $totalspendhour = 0; $totalspendcost = 0;
                            foreach ($months as $key=>$values){
                                $i++;

                                $query1 = 'SELECT sum(m.total_hours) as totalhour, sum(m.total_bilrate_with_tax) as total_bilrate_with_tax FROM vms_consolidated_invoice b JOIN vms_generated_invoice m ON FIND_IN_SET(m.id, b.generated_invoice_id) WHERE DATE_FORMAT(m.invoice_start_date, "%m")= '.date('m',strtotime($values)).' and DATE_FORMAT(m.invoice_start_date, "%Y")= '.date('Y',strtotime($values)).' and b.status=2 and m.candidate_id='.$_GET['can_id'];
                                $invovic = Yii::app()->db->createCommand( $query1 )->queryRow();
                                    $totalMonth = count($months);
                                    $totalestimatedhour += $estimatedhours[$key];
                                    $totaleestimatedcost += $estimatedhours[$key]*$jobData->bill_rate;
                                    $totalspendhour +=$invovic['totalhour'];
                                    $totalspendcost += $invovic['total_bilrate_with_tax'];
                                ?>
                                    <tr>

                                        <td><?php echo $i; ?></td>
                                        <td><?php echo date('F-Y',strtotime($values)); ?></td>
                                        <td><?php echo $estimatedhours[$key]; ?></td>

                                        <td>$<?php echo $estimatedhours[$key]*$jobData->bill_rate; ?></td>

                                        <td><?php if(!empty($invovic['totalhour'])) { echo $invovic['totalhour']; }else{ echo '0'; } ?></td>

                                        <td><?php if(!empty($invovic['total_bilrate_with_tax'])) { echo '$ '.$invovic['total_bilrate_with_tax']; }else{ echo '$ 0'; } ?></td>

                                        <td style="text-align: center">
                                            <a href="#" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
                                        </td>
                                    </tr>
                            <?php } ?>
                            <tr style="color: green;">
                                <td>Total</td>
                                <td>
                                    <?php echo $totalMonth.' Months'; ?>
                                </td>
                                <td>
                                    <?php echo $totalestimatedhour; ?>
                                </td>
                                <td>
                                    <?php echo '$ '.$totaleestimatedcost; ?>
                                </td>
                                <td>
                                    <?php echo $totalspendhour; ?>
                                </td>
                                <td>
                                    <?php echo '$ '.$totalspendcost; ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    <?php } ?>

            </div>

            <br>
        </div>
    </div>
</div>
</div>
</div>
<!-- col -->
</div>
<!-- row -->


</div>
</div>

<script>
    function goBack() {
        window.history.back();
    }
</script>