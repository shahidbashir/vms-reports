<?php $this->pageTitle = 'View Job';

$client = Client::model()->findByPk(Yii::app()->user->id);
$interview = Interview::model()->findByAttributes(array('submission_id'=>$_GET['submission-id']),array('order'=>'id desc'));
$candidate = Candidates::model()->findByPk($submissionData->candidate_Id);

?>

<header class="db-header">
  <div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
      <h4><?php echo $jobmodel->title; ?></h4>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
      <div class="right">
        <ul class="list-inline pull-right">
          <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobListing'); ?>">Back to Job Listing</a></li>
          <li><a href="">Previous Job</a></li>
          <li><a href="">Next Job</a></li>
        </ul>
      </div>
    </div>
  </div>
  
  <!-- row --> 
</header>
<div class="page gray-bg">
<div class="card p-0">
  <div class="job-detail">
    <div class="row m-0">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="client-listing-nav">
          <nav class="navbar navbar-default" role="navigation"> 
            <!-- Brand and toggle get grouped for better mobile display --> 
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex2-collapse">
              <?php $this->renderPartial('jobsdetail_tabs_header'); ?>
            </div>
            <!-- /.navbar-collapse --> 
          </nav>
        </div>
        <!-- client-listing-nav --> 
        
      </div>
    </div>
    <!-- row -->
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-wraper">
          <div class="db-content-wraper">
            <div class="page-content-outer">
              <div class="page-content">
                <div class="job-detail-inner">
                  <div class="row interview-button">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right"> 
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobsubmission',array('id'=>$_GET['id'])); ?>" class="btn btn-default">Back to Interview</a> 
                    </div>
                    <!-- col --> 
                  </div>
                  <!-- row --> 
                  
                  <br>
                  <div class="interview-submission">
                    <div class="interview-approval-header">
                      <h4>Interview Schedule</h4>
                      <p></p>
                    </div>
                    <div class="interview-approval-content client-sub">
                      <table class="table">
                        <tbody>
                          <tr>
                            <td><p>Interview ID: <a href=""><?php echo $interview->id; ?></a></p>
                              <p><small><!--10th May 2015 at 10:30 PM--></small></p></td>
                            <td style="width: 46px; padding-right: 0;"><div class="avatar-small-text"> <span>SB</span> </div></td>
                            <td style="padding-right: 0;"><p class="bold"><?php echo $candidate->first_name.' '.$candidate->last_name ?></p>
                              <p><small>Interview by: Null</small></p></td>
                            <td><div class="text-right m-t-10"> </div></td>
                          </tr>
                          <!-- //end  -->
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="interview-view">
                    <?php if($interview->status != 'Rejected'){ ?>
                      <form action="" method="POST" role="form"  class="reject-form">
                        <div class="form-group">
                          <label for="">Status</label>
                          <div class="single">
                            <select name="Interview[status]" id="interivestatus" class="ui fluid search dropdown">
                              <option value=""></option>
                              <option value="Reject" <?php if(isset($_GET['type']) && $_GET['type'] =='Reject'){?> selected="selected"<?php }?>>Rejected</option>
                              <option value="Shortlisted" <?php if(isset($_GET['type']) && $_GET['type'] =='Shortlisted'){?> selected="selected"<?php }?>>Shortlisted</option>
                              <option value="Interview" <?php if(isset($_GET['type']) && $_GET['type'] =='Interview'){?> selected="selected"<?php }?>>Interview</option>
                              <option value="Offer" <?php if(isset($_GET['type']) && $_GET['type'] =='Offer'){?> selected="selected"<?php }?>>Offer</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group"  id="reason_rejection">
                          <label for="">Reason for Rejection</label>
                          <div class="single">
                            <select name="Interview[reason_rejection]" class="ui fluid search dropdown">
                              <option value=""></option>
                              <option value="Reason1">Reason1</option>
                              <option value="Reason2">Reason2</option>
                              <option value="Reason3">Reason3</option>
                            </select>
                          </div>
                        </div>
                        
                        <div id="Offer">
                        <div class="form-group">
                          <label for="">Reason for Offer</label>
                          <div class="single">
                            <select name="Interview[offer]" id="Interview_offer" class="ui fluid search dropdown">
                              <option value=""></option>
                              <option value="offer1">offer1</option>
                              <option value="offer2">offer2</option>
                              <option value="offer3">offer3</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="">Rating</label>
                          <div class="container" id="star-rating">
                            <input type="radio" name="Interview[rating]" class="rating" value="1" />
                            <input type="radio" name="Interview[rating]" class="rating" value="2" />
                            <input type="radio" name="Interview[rating]" class="rating" value="3" />
                            <input type="radio" name="Interview[rating]" class="rating" value="4" />
                            <input type="radio" name="Interview[rating]" class="rating" value="5" />
                          </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                          <label for="">Notes & Comments</label>
                          <textarea name="Interview[notes]" id="input" class="form-control" rows="3" required="required"></textarea>
                        </div>
                        <br>
                        <br>
                        <div class="text-left">
                          <button type="save" name="saveInterviewStatus" class="btn btn-success">Save </button>
                          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                        </div>
                      </form>
                    <?php }else{ echo '<p style="color:red;">Interview of this candidate is already Rejected.</p>'; } ?>
                    </div>
                    <!-- interview-view --> 
                    
                  </div>
                  <!-- job-approval --> 
                  
                </div>
                <!-- job-detail-inner --> 
                
                <!-- /*  End inner cotent  */ -->
                
                <div class="quick-detail hide">
                  <div class="quick-info box"> <a href="" class="close">Close</a>
                    <div class="quick-info-header">
                      <h4>QUICK INFORMATION</h4>
                    </div>
                    <div class="quick-info-content">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>JOB TYPE</th>
                            <th>PAYMENT</th>
                            <th>NUMBER OF OPENINGS</th>
                            <th>JOB CLOSING DATE</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>IT Management</td>
                            <td>34 Per Hour</td>
                            <td>10</td>
                            <td>10-02-2015 ( 9 More Left) </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <br>
                  <div class="skills-detail">
                    <div class="skills-info box">
                      <div class="skills-info-header">
                        <h4>SKILLS</h4>
                      </div>
                      <div class="skills-info-content"> <span class="label label-info">Daily Submissino</span> <span class="label label-info">Daily Submissino</span> <span class="label label-info">Daily Submissino</span> </div>
                    </div>
                  </div>
                  <!-- skills-detail --> 
                  
                  <br>
                  <div class="description-detail">
                    <div class="description-info box">
                      <div class="description-info-header">
                        <h4>JOB DESCRIPTION</h4>
                      </div>
                      <div class="description-info-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      </div>
                    </div>
                  </div>
                  <!-- description-detail --> 
                  
                  <br>
                  <div class="location-detail">
                    <div class="location-info box">
                      <div class="location-info-header">
                        <h4>LOCATIONS</h4>
                      </div>
                      <div class="location-info-content">
                        <div class="row">
                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <p>Location</p>
                            <p>2741 Barnes Street</p>
                            <p>Orlando, FL 32810</p>
                          </div>
                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <p>Location</p>
                            <p>2741 Barnes Street</p>
                            <p>Orlando, FL 32810</p>
                          </div>
                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <p>Location</p>
                            <p>2741 Barnes Street</p>
                            <p>Orlando, FL 32810</p>
                          </div>
                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <p>Location</p>
                            <p>2741 Barnes Street</p>
                            <p>Orlando, FL 32810</p>
                          </div>
                        </div>
                        <!-- row --> 
                      </div>
                    </div>
                  </div>
                  <!-- description-detail --> 
                  
                </div>
                <!-- quick-detail --> 
                <!-- page-content --> 
                
              </div></div>
              <!-- page-content-outer -->
              
              <?php $this->renderPartial('sidebar',array('jobData'=>$jobmodel,'invitedTeamMembers'=>$invitedTeamMembers)); ?>
              <!-- page-sidebar --> 
              
            </div>
            <!-- db-content-wraper --> 
            
          </div>
          <!-- page-wraper --> 
        </div>
      </div>
      <!-- row --> 
    </div>
  </div>
</div>
