<?php $this->pageTitle = 'Job Template'; error_reporting(0); ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List Job Template
      </h4>
      <p class="m-b-20"></p>
      <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
      <?php endif; ?>
      <div class="m-b-30">
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/addTemplate'); ?>" class="btn btn-sm btn-default-2">Add Job Template</a>
      </div>
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
        <tr>
          <th style="width: 7%; ">Sr No:</th>
          <th style="max-width: 100px;">Template Name</th>
          <th>Category</th>
          <th>Number of Jobs</th>
          <th class="actions">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if($model){
        $i = 0;
        foreach($model as $data){

        $i++;
        ?>
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php echo $data->job_title; ?></td>
          <td>
            <?php
            $setting = Setting::model()->findByPk($data->cat_id);
            //if($setting)
            echo $setting->title;
            ?>
          </td>
          <td>
            <?php $numberofJob = Job::model()->countByAttributes(array('template_id'=>$data->id));
            echo $numberofJob ?></td>
          <td class="actions">
          <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/templateView',array('template_id'=>$data->id,'tab'=>'template-view')); ?>" data-toggle="tooltip" data-placement="top" title="View" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a>
          <?php /*?><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/updateTemplate',array('template_id'=>$data->id)); ?>" data-toggle="tooltip" data-placement="top" title="Eidt" class="icon"><i class="icon svg-fire"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/edit@512px-grey.svg"></i></a><?php */?>
          <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/deleteTempelt',array('id'=>$data->id)); ?>" onclick="return confirm('Are you sure?')" data-toggle="tooltip" data-placement="top" title="Delete" class="icon"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg"></i></a>
          </td>
        </tr>
        <?php } } ?>
        </tbody>
      </table>
      <?php
      $this->widget('CLinkPager', array(
          'pages' => $pages,
          'header' => '',
          'nextPageLabel' => 'Next',
          'prevPageLabel' => 'Prev',
          'selectedPageCssClass' => 'active',
          'hiddenPageCssClass' => 'disabled',
          'htmlOptions' => array(
              'class' => 'pagination',
          )
      ))
      ?>
    </div>

    <!-- col -->
  </div>

  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
