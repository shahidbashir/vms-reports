<?php $this->pageTitle = 'Job Template'; error_reporting(0); ?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 0">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Add Job Template <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobtemplates'); ?>" class="btn pull-right btn-sm btn-default-3">Back to Job Catalog</a> </h4>
      <p class="m-b-20"></p>
    </div>
  </div>
  <div class="cleafix " style="padding: 10px 20px 10px; ">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <div class="row add-job">
        <div class="col-md-12">
          <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-heading"></div>
             <?php $form=$this->beginWidget('CActiveForm', array(

				  'id'=>'jobtemplet-form',
	  
				  'enableAjaxValidation'=>false,
	  
			  )); ?>
            <div class="panel-body">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="">Job Title</label>
                    <?php echo $form->textField($model,'job_title',array('class'=>'form-control','required'=>'required')); ?>
                  </div>
                </div>
                
                <!-- col --> 
                
              </div>
              
              <!-- row -->
               <div class="row"> 
                 <!-- col -->
                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="">Category</label>
                  <?php
                  $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'id', 'title');
                  echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'form-control select2','empty' => '' ,'required'=>'required')); ?>
                  </div>
                </div>
              </div>
               <!-- row -->
                <!-- row -->
               <div class="row"> 
                 <!-- col -->
                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="">Experiences</label>
                    <?php
                  $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=10')),'title', 'title');

                  echo $form->dropDownList($model, 'experience', $list , array('class'=>'form-control select2','empty' => '','required'=>'required')); ?>

                  </div>
                </div>
              </div>
               <!-- row -->
                <!-- row -->
               <div class="row"> 
                 <!-- col -->
                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="">Job Level</label>
                   <?php
                  $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=40')),'title', 'title');
                  echo $form->dropDownList($model, 'job_level', $list , array('class'=>'form-control select2','empty' => '','required'=>'required')); ?>
                  </div>
                </div>
              </div>
               <!-- row -->
              
              <div class="hirring-pipline">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      <label for="">Primary Skill</label>
                      <select class="tokenize-remote-demo1" name="JobTemplates[primary_skills][]" id="job_skills" multiple></select>
                    </div>
                    <div class="form-group">
                      <label for="">Secondary Skill</label>
                       <select class="tokenize-remote-demo1" name="JobTemplates[secondary_skills][]" id="job_skills1" multiple></select>
                    </div>
                    <div class="form-group">
                      <label for="">Good to have.</label>
                     <select class="tokenize-remote-demo1" name="JobTemplates[good_to_have][]" id="job_skills2" multiple></select>
                    </div>
                  </div>
                  
                  <!-- col-12 --> 
                  
                </div>
                
                <!-- row -->
                
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for="">Note for Skills</label>
                      <textarea name="JobTemplates[notes_for_skills]" id="input" class="form-control" rows="5" required="required"></textarea>
                    </div>
                  </div>
                </div>
                
                <!-- row --> 
                
              </div>
              
              <!-- hirring-pipline -->
              
              <div class="search-box-only-border">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel-group" id="accordion">
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed"> Job Description</a>  </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="JobTemplates[job_description]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false"> You Will</a> </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                             <textarea name="JobTemplates[you_will]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false"> Qualifications</a> </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <textarea name="JobTemplates[qualification]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-info m-b-0">
                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                          <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed" aria-expanded="false"> Additional Information</a></h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                               <textarea name="JobTemplates[add_info]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <!-- col-12 --> 
                  
                </div>
                
                <!-- row --> 
                
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <p class="m-b-10  m-t-10 bold">Rate Card</p>
                  <table class="table table-striped table-hover job-template-table table-borderd-head">
                    <thead class="thead-default">
                      <tr>
                        <th>Experience</th>
                        <th>Minimum Bill Rate</th>
                        <th>Maximum Bill Rate</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
						$i = 0;
						$templateexp = Setting::model()->findAllByAttributes(array('category_id'=>10));
						foreach($templateexp as $value){
						  $i++;
						?>
						<tr>
						  <td style="width: 40%"><?php echo $value['title']; ?> <input type="hidden" name="temp_experience[]" value="<?php echo $value['title'] ?>"> </td>
						  <td style="width: 30%">
							<input type="number" step="any" name="temp_min_billrate[]" id="input" class="form-control" value="" >
						  </td> 
						  <td style="width: 30%">
							<input type="number" step="any"  name="temp_max_billrate[]" id="input" class="form-control" value="" >
						  </td>
						</tr>
						<?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- col --> 
                
              </div>
              <br>
              <div>
                <button type="submit" class="btn btn-success">Save</button>
                <button type="submit" class="btn btn-default">Cancel</button>
              </div>
              <br>
              <br>
            </div>
             <?php $this->endWidget(); ?>
            <!-- panel-body --> 
            
          </div>
        </div>
      </div>
    </div>
    
    <!-- col -->
    
    <!--<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      <h5 class="m-b-20 m-t-20 font-normal">Aspernatur cum in est </h5>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas possimus dicta fugiat voluptates, ab ipsa nihil alias aliquam quasi nisi id tenetur quam praesentium aspernatur cum in est doloribus blanditiis! Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>-->
  </div>
  
  <!-- row -->
  
  <div class="seprater-bottom-100"></div>
</div>