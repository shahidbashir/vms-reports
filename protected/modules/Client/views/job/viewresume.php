<?php 
	$this->pageTitle = '';
	$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']);
	//$Candidates = Candidates::model()->findByPk($values['candidate_Id']);
	$Job = Job::model()->findByPk($values['job_id']);
	/*echo '<pre>';
	print_r($submittedUsers);
	exit;*/
 ?>
<header class="db-header">
  <div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
      <h4><?php echo $jobmodel->title;?></h4>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
      <div class="right">
        <ul class="list-inline pull-right">
          <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobListing'); ?>">Back to Job Listing</a></li>
          <li><a href="">Previous Job</a></li>
          <li><a href="">Next Job</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- row --> 
  
</header>
<div class="page gray-bg">
  <div class="card p-0">
    <div class="job-detail">
      <div class="row m-0">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="client-listing-nav">
            <nav class="navbar navbar-default" role="navigation"> 
              <!-- Brand and toggle get grouped for better mobile display --> 
              
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse navbar-ex2-collapse">
                <?php $this->renderPartial('jobsdetail_tabs_header'); ?>
              </div>
              <!-- /.navbar-collapse --> 
            </nav>
          </div>
          <!-- client-listing-nav --> 
          
        </div>
      </div>
      <!-- row -->
      
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="page-wraper">
            <div class="db-content-wraper">
              <div class="page-content-outer">
                <div class="page-content">
                  <div class="job-detail-inner">
                    <div class="c-sub-interview">
                      <div class="clearfix">
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4"> <a href="#" class="backy">BACK TO SUBMISSION</a> </div>
                        <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8 text-right"> 
                        <?php if($submissionData->resume_status=='5'){ ?>
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/scheduleInterview',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'])); ?>" <?php //echo $submissionData->resume_status!='Interview'?'disabled':'' ?> class="btn btn-small btn-green-light">Schedule a Interview</a> 
                        <?php }else{ ?>
                        <a href="#" disabled="disabled" class="btn btn-small btn-green-light">Schedule a Interview</a> 
                        <?php } ?>
                        
                        
                        <?php /*?><a href="<?php echo Yii::app()->baseUrl.'/candidate_resumes/'.$submittedUsers['resume']?>" download class="btn  btn-small btn-default">Download Resume</a> <?php */?>
                        <a href="<?php echo $this->createAbsoluteUrl('job/downloadResume',array('id'=>$submissionData->candidate_Id)); ?>" class="btn  btn-small btn-default">Download Resume</a> 
                        <a class="btn btn-small btn-default" data-toggle="modal" data-target="#job-reject">Reject & Achieve</a> </div>
                      </div>
                      <br>
                      <div class="c-sub-interview-info">
                      
						<form name="interviw" method="post">  
                                            
                        <ul class="list-inline">
                          <li>
                            <p>CANDIDATE NAME</p>
                            <h4><?php echo $submittedUsers['first_name'].' '.$submittedUsers['last_name']?></h4>
                          </li>
                          <li>
                            <p>RATES</p>
                            <h4><?php echo $submittedUsers['job_pay'].' '.$Job->currency.' '.$submissionData->type; ?></h4>
                          </li>
                          <li>
                            <p>START DATE</p>
                            <h4><?php echo $submittedUsers['v_start_date']?></h4>
                          </li>
                          <li>
                          <div class="form-group">
                            <label for="">Update Status</label>
                            <div class="single">
                              <select name="Interview[status]" class="ui fluid search dropdown">
                                <option value=""></option>
                                <option <?php echo $submissionData->resume_status=='1'?'selected':''; ?> value="Submitted">Submitted</option>
                                <option <?php echo $submissionData->resume_status=='6'?'selected':''; ?>  value="Reject">Reject</option>
                                <option  <?php echo $submissionData->resume_status=='3'?'selected':''; ?> value="Shortlisted">Shortlisted</option>
                                <option  <?php echo $submissionData->resume_status=='5'?'selected':''; ?> value="Interview">Interview</option>
                                <option  <?php echo $submissionData->resume_status=='7'?'selected':''; ?> value="Offer">Offer</option>
                              </select>
                            </div>
                          </div>
                        </li>
                        <li>
                          <button type="submit" name="saveInterviewStatus" class="btn btn-success">Save</button>
                        </li>
                        </ul>
                        
                        </form>
                        
                        
                      </div>
                      <!-- job-workflow-info -->
                      
                      <div class="sechedule-interview">
                        <div class="sechedule-interview-heading">
                          <h4>Total Experience: 3 Years</h4>
                        </div>
                      </div>
                    </div>
                    <!-- c-sub-interview --> 
                    
                  </div>
                  <!-- job-detail-inner --> 
                  
                  <!-- /*  End inner cotent  */ -->
                  
                  <div class="quick-detail hide">
                    <?php $this->renderPartial('quickData',array('jobData'=>$jobmodel)); ?>
                  </div>
                  <!-- quick-detail --> 
                  
                </div>
                <!-- page-content --> 
                
              </div>
              <!-- page-content-outer -->
              
              <?php $this->renderPartial('sidebar',array('jobData'=>$jobmodel,'invitedTeamMembers'=>$invitedTeamMembers));?>
            </div>
            <!-- db-content-wraper --> 
            
          </div>
          <!-- page-wraper --> 
        </div>
      </div>
      <!-- row --> 
    </div>
  </div>
</div>
