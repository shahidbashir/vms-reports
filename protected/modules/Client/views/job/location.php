<?php $this->pageTitle = 'Job Locations';
$client = Client::model()->findByPk(Yii::app()->user->id);

$locations = unserialize($model->location);
/*echo '<pre>';
print_r($locations);
exit;*/
?>

<div class="client-workspace">
  <?php if(Yii::app()->user->hasFlash('success')):
		  echo Yii::app()->user->getFlash('success');
	endif; ?>
  <?php $this->renderPartial('_menu',array('model'=>$model));?>
</div>

<!-- Tab panes -->

<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="jobs">
    <h4>Locations</h4>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-striped dataTable no-footer  m-t-10">
          <thead>
            <tr>
              <th>Location Name</th>
              <th>Address</th>
              <th>Location Manager</th>
              <th style="text-align: center">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($locations as $key=>$value){
			  $location = Location::model()->findByPk($value);
			   ?>
            <tr>
              <td><?php echo $location->name; ?></td>
              <td><?php echo $location->address1; ?>, <?php echo $location->city; ?> <?php echo $location->state; ?>, <?php echo $location->country; ?> <?php echo $location->zip_code; ?></td>
              <td><?php echo $location->location_account_mananger; ?></td>
              <td style="text-align: right">
              <!--<a data-original-title="Edit" class=" tooltips" data-toggle="tooltip" data-placement="top" href="job-info.html"><i class="fa fa-pencil"></i></a>--> 
              <?php /*?><a data-original-title="Delete" class=" tooltips" data-toggle="tooltip" data-placement="top" href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/deletelocation',array('id'=>$value->id,'team'=>'member'))?>"><i class=" ti-trash"></i></a><?php */?>
              </td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
