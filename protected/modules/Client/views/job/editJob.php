<div class="main-content">
    <?php if(Yii::app()->user->hasFlash('success')):?>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    <?php endif; ?>
<div class="row">
    <!--Condensed Table-->
    <!--Hover table-->
    <div class="col-sm-12">
        <div class="row">
        </div>
    </div>
    <!--Hover table-->
    <!--Hover table-->
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-12">
                <form name="jobedit" method="post" action="">
                <div class="panel panel-border-color panel-border-color-primary">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="">Job Title</label>
                                    <input type="text" name="Job[title]" value="<?php echo $model->title; ?>" class="form-control" id="" readonly placeholder="">
                                </div>
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="">Experiences</label>
                                    <input type="text" name="Job[experience]" value="<?php echo $model->experience; ?>" class="form-control">
                                </div>
                            </div>
                            <!-- col -->

                        </div>
                        <!-- row -->
                        <div class="hirring-pipline">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label for="">Primary Skill</label>
                                        <input type="text" name="Job[skills]" class="form-control tokenfield-input" id="job_skills" value="<?php echo $model->skills; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Secondary Skill</label>
                                        <input type="text" name="Job[skills1]" class="form-control tokenfield-input" id="job_skills1" value="<?php echo $model->skills1; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Good to have.</label>
                                        <input type="text" name="Job[skills2]" class="form-control tokenfield-input" id="job_skills2" value="<?php echo $model->skills2; ?>">
                                    </div>
                                </div>
                                <!-- col-12 -->
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="">Note for Skills</label>
                                        <textarea name="Job[skills_notes]" id="input" class="form-control" rows="5" required="required"><?php echo $model->skills_notes; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                        </div>
                        <!-- hirring-pipline -->

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-info">
                                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed">
                                                    Job Description</a>
                                            </h4>
                                        </div>
                                        <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="panel-body">
                                                <textarea name="Job[description]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->description; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false">
                                                    You Will</a>
                                            </h4>
                                        </div>
                                        <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="panel-body">
                                                <textarea name="Job[you_will]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->you_will; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false">
                                                    Qualifications</a>
                                            </h4>
                                        </div>
                                        <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="panel-body">
                                                <textarea name="Job[qualification]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->qualification; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed" aria-expanded="false">
                                                    Additional Information</a>
                                            </h4>
                                        </div>
                                        <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="panel-body">
                                                <textarea name="Job[add_info]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->add_info; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- col-12 -->
                        </div>
                        <!-- row -->







                        <br>
                        <div>
                            <button type="submit" name="savjob" class="btn btn-success">Save</button>
                            <button type="submit" name="saveandEmai" class="btn btn-success">Save & Publish</button>
                            <button type="reset" class="btn btn-default">Cancel</button>
                        </div>
                        <br>
                        <br>
                    </div>
                    <!-- panel-body -->
                </div>
                </form>
            </div>
        </div>
    </div>
    <!--Hover table-->
</div>
    </div>