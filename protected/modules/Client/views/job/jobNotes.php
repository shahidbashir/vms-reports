<?php $this->renderPartial('_menu',array('model'=>$jobmodel));?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php echo Yii::app()->user->getFlash('success'); ?>
<?php endif; ?>

<div class="tab-content ">
  <div class="tab-pane active" id="one" role="tabpanel">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


        <div class="interview-notes-comments ">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

              <div class="media">
                <a class="pull-left" href="#">
                  <?php $clientInfo = Client::model()->findByPk(Yii::app()->user->id);
                  if($clientInfo->profile_image){
                    $images = Yii::app()->baseUrl.'/images/profile_img/'.$clientInfo->profile_image;
                  }else{
                    $images = Yii::app()->request->baseUrl.'/new-theme/assets/images/avatar.jpg';
                  }
                  ?>
                  <img class="media-object" src="<?php echo $images ;?>" alt="Image">
                </a>
                <form name="jobnotes" method="post" action="" enctype="multipart/form-data">
                <div class="media-body">
                  <textarea name="JobComments[comments]" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"></textarea>
                  <div class="form-group m-b-10 m-t-20">
                    <label data-toggle="modal" class="a-blue">Attachement</label>
                    <input type="file" name="JobComments[attachment]" >
                  </div>
                  <div class="be-checkbox">
                    <input id="check1" name="JobComments[show_msp]" type="checkbox" value="1">
                    <label for="check1">Share the notes with MSP</label>
                  </div>
                  <div class="be-checkbox">
                    <input id="check1" name="JobComments[show_vendor]" type="checkbox" value="1">
                    <label for="check1">Share the notes with Vendor</label>
                  </div>
                  <button type="submit" name="JobNotes" class="btn btn-success m-t-10">Add Comment</button>
                </div>
                </form>
              </div>
              <!-- media -->

              <?php $model = JobComments::model()->findAllByAttributes(array('job_id'=>$_GET['id'],'user_id'=>Yii::app()->user->id));
              if($model){
              foreach($model as $commentsdata){
                $clientData = Client::model()->findByPk($commentsdata['user_id']);
                $vendordata = Vendor::model()->findByPk($commentsdata['user_id']);
                $adminData = Admin::model()->findByPk($commentsdata['user_id']);
                $images_path = '';
                if($clientData && $clientData->profile_image){
                  $images_path = Yii::app()->baseUrl.'/images/profile_img/'.$clientData->profile_image;
                }else if($vendordata && $vendordata->profile_image){
                  $images_path = Yii::app()->request->baseUrl. '/files/profile-images/' . $vendordata->profile_image;
                }else if($adminData && $adminData->profile_image){
                  $images_path = Yii::app()->baseUrl.'/files/profile-images/'.$adminData->profile_image;
                }
              ?>

              <div class="media">
                <a class="pull-left" href="#">
                  <?php if($images_path){ ?>
                    <img class="media-object" src="<?php echo $images_path; ?>" alt="Image">
                 <?php }else{ ?>
                    <img class="media-object" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/avatar.jpg" alt="Image">
                  <?php } ?>
                </a>
                <div class="media-body">
                  <p class="m-b-10 bold"><?php echo $commentsdata['user_name']; ?></p>
                  <p><?php echo $commentsdata['comments']; ?></p>
                  <?php if($commentsdata['attachment']){ ?>
                  <div class="attachment">
                    <p>Attachments</p>
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/downloadJobnote',array('id'=>$commentsdata['id'])); ?>" class=""><?php echo $commentsdata['attachment']; ?><i class="fa fa-cloud-download"></i></a>
				  </div>
                  <?php } ?>
                  <p class="meta-inner">Posted <?php echo $commentsdata['date_created']; ?></p>
                </div>
              </div> <!-- media -->

            <?php } }?>
            </div>
            <!-- col -->
          </div>
          <!-- row -->
        </div>
      </div>
      <!-- row -->
    </div>
  </div>
</div>
</div>