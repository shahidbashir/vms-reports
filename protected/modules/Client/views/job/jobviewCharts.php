<?php
	$loginClientData = Client::model()->findByPk(Yii::app()->user->id);
	$jobID = $_GET['id'];
	if($loginClientData->member_type==NULL){
		$clientID = Yii::app()->user->id;
		}else{
			 $clientID = $loginClientData->super_client_id;
			 }
	//data for the first chart
	$startEndDate = UtilityManager::getStartAndEndDate(date('W'),date('Y'));
	$month = array();
	for($i=0;$i<30;$i++){
		$month[] = date('Y-m-d',strtotime(date('Y-m-d') . " -".$i." days"));
		}
	//second loop variables
	$monthDays = ''; $submissionMonthTotal = '';
	
	//submission Monthly counting chart data
	foreach(array_reverse ($month) as $exactDate){
			//$submissionMonthCount = VendorJobSubmission::model()->countByAttributes(array('job_id'=>$jobID,'resume_status'=>array(1,2,3,4,5,6,7,8,9),'date_created'=>$exactDate));
						
			$subQuery = "SELECT id FROM vms_vendor_job_submission WHERE job_id=$jobID and DATE_FORMAT(date_created, '%Y-%m-%d') = '".$exactDate."' and resume_status IN(1,2,3,4,5,6,7,8,9)";
			$submissionMonthCount = Yii::app()->db->createCommand( $subQuery )->query()->count();
			
			$submissionMonthTotal .= "['".date('m-d-Y',strtotime($exactDate))."',";
			$submissionMonthTotal .= $submissionMonthCount."],";
		}
	
		 
	//data for second chart "job hiring trends"
	$query = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status = 4 and job_id=".$jobID;
	$submissionCount = Yii::app()->db->createCommand( $query )->query()->count();
					
	$query1 = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status = 7 and job_id=".$jobID;
	$offerCount = Yii::app()->db->createCommand( $query1 )->query()->count();
	
	$query2 = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status = 8 and job_id=".$jobID;
	$hiredCount = Yii::app()->db->createCommand( $query2 )->query()->count();
	
	$query3 = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status = 9 and job_id=".$jobID;
	$workorderCount = Yii::app()->db->createCommand( $query3 )->query()->count();
	
	$query4 = "SELECT id FROM vms_vendor_job_submission WHERE client_id=$clientID and resume_status = 5 and job_id=".$jobID;
	$interviewCount = Yii::app()->db->createCommand( $query4 )->query()->count();
?>
<div class="tab-content ">
  <div class="tab-pane active" id="one" role="tabpanel">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row m-t-20">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="card">
              <div class="char-header">
                <h4>Submission Trends</h4>
              </div>
              <div class="card-block">
                <div id="area-chart" style="height: 400px;"></div>
              </div>
            </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="card">
              <div class="char-header">
                <h4>Job Hiring Trends</h4>
              </div>
              <div class="card-block">
                <div id="funnal-chart" style="height: 400px;"></div>
              </div>
            </div>
          </div>
        </div>
        <!-- simplify-tabs -->
        <!--query for next 48 hour interview schedule -->
          <?php
			$todaydate = date('Y-m-d');
			$date = strtotime($todaydate);
			$date = strtotime("+2 day", $date);
			$tomorrowdate = date('Y-m-d', $date);
			$sql = "SELECT w.*, s.id as submissionID,s.candidate_Id,s.job_id FROM vms_interview w LEFT JOIN vms_vendor_job_submission s ON w.submission_id= s.id where s.client_id='".Yii::app()->user->id."' and s.job_id= '".$_GET['id']."'
			and ((w.interview_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.start_date_status =1) or (w.interview_alt1_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt2_date_status =1) or (w.interview_alt3_start_date BETWEEN '".$todaydate."' AND '".$tomorrowdate."' and w.alt3_date_status =1))";
			$interview = Yii::app()->db->createCommand($sql)->query()->readAll();
			$numberofInterview = count($interview);
			?>
        <hr class="full-hr-30">
        <br>
        <p class="bold m-b-10">Interview Schedule ( For Next 48 hrs ) <span class="pull-right" style="color: #e64a33"><?php if($numberofInterview){ echo $numberofInterview; }else{ echo '0'; } ?> Interviews are Scheduled</span></p>
        <table class="table m-b-10 without-border">
          <thead class="thead-default">
            <tr>
              <th style="width: 10px;">Status</th>
              <th>Type</th>
              <th>Candidate Name</th>
              <th>Date</th>
              <th>Time In</th>
              <th>Time Out</th>
              <th>Location</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
           <?php
			if($interview){
			foreach($interview as $allData){
			$locationInterview = Location::model()->findByPk($allData['interview_where']);
			$candidateData = Candidates::model()->findByPk($allData['candidate_Id']);
			  $jobData = Job::model()->findByPk($allData['job_id']);
			  if($allData['start_date_status']){
				$date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '. $allData['interview_end_date_time'];
			  }
			  else if($allData['alt1_date_status']){
				$date = $allData['interview_alt1_start_date'].' '.$allData['interview_alt1_start_date_time'].' to '.$allData['interview_alt1_end_date_time'];
			  }
			  else if($allData['alt2_date_status']){
				$date = $allData['interview_alt2_start_date'].' '.$allData['interview_alt2_start_date_time'].' to '.$allData['interview_alt2_end_date_time'];
			  }
			  else if($allData['alt3_date_status']){
				$date = $allData['interview_alt3_start_date'].' '.$allData['interview_alt3_start_date_time'].' to '.$allData['interview_alt3_end_date_time'];
			  }else{
				$date = $allData['interview_start_date'].' '.$allData['interview_start_date_time'].' to '.$allData['interview_end_date_time'];
			  }
			  $newTime = explode(' ',$date);
				$postingstatus = UtilityManager::interviewStatus();
						switch ($postingstatus[$allData['status']]) {
							case "Approved":
								$color = 'label label-success';
								break;
							case "Waiting for Approval":
								$color = 'label-interview-pending';
								break;
							case "Cancelled":
								$color = 'label-interview-cancelled';
								break;
							case "Reschedule":
								$color = 'label-interview-reschedule';
								break;
					   case "Interview Completed":
								$color = 'label-interview-completed';
								break;
							default:
								$color = 'label-new-request';
						}
						?>
            <tr>
              <td><span class="tag <?php echo $color ?>"><?php echo $postingstatus[$allData['status']]; ?></span></td>
              <td><?php echo $allData['interview_type']; ?></td>
              <td><?php if($candidateData) echo $candidateData->first_name.' '.$candidateData->last_name; ?></td>
              <td><?php echo $newTime[0]; ?></td>
              <td><?php echo $newTime[1].' '.$newTime[2]; ?></td>
              <td><?php echo $newTime[4].' '.$newTime[5]; ?></td>
              <td><?php echo $locationInterview->name; ?></td>
              <td style="text-align: center"><a href="" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>
        <!--<p class="m-b-30"> <a href="" class="blue">More Interviews</a> </p>-->
        <br>
        <!--query for submission for next 24 hour  -->
        <?php
			$date_start = date('Y-m-d');
			$date = strtotime($todaydate);
			$date = strtotime("+2 day", $date);
			$date_end = date('Y-m-d', $date);
			$loginUserId = Yii::app()->user->id;
			$criteria = new CDbCriteria();
			$criteria->addCondition("job_id", $_GET['id'] And "client_id", $loginUserId);
			$criteria->addInCondition('resume_status',array(3,4,5,6,7,8,9));
			$criteria->addBetweenCondition("date_created",$date_start,$date_end,'AND');
			$submission = VendorJobSubmission::model()->findAll($criteria);
			$numberofnsubmission = count($submission);
		  ?>
        <p class="bold m-b-10">Submission ( Today Submission ) <span class="pull-right" style="color: #e64a33"><?php if($numberofnsubmission){ echo $numberofnsubmission; }else{ echo '0'; } ?> New Submission</span> </p>
        <table class="table m-b-40 without-border">
          <thead class="thead-default">
            <tr>
              <th  style="width: 10px;">Status</th>
              <th>Candidate Name</th>
              <th>Start Date</th>
              <th>Pay Rate</th>
              <th>Bill Rate</th>
              <th>Current Location</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
           <?php 										
				if($submission){
					foreach($submission as $value){
						$Jobsdata = Job::model()->findByPk($value->job_id);
						$candidates = Candidates::model()->findByPk($value->candidate_Id);
						$status = UtilityManager::resumeStatus();
				 switch ($status[$value->resume_status]) {
					 case "Submitted":
						 $color = 'label-hold';
						 break;
					 case "MSP Review":
						 $color = 'label-filled';
						 break;
					 case "MSP Shortlisted":
						 $color = 'label-new-request';
						 break;
					 case "Client Review":
						 $color = 'label-filled';
						 break;
					 case "Interview Process":
						 $color = 'label-open';
						 break;
					 case "Rejected":
						 $color = 'label-rejected';
						 break;
					 case "Offer":
						 $color = 'label-pending-aproval';
						 break;
					 case "Approved":
						 $color = 'label-new-request';
						 break;
					 case "Work Order Release":
						 $color = 'label-re-open';
						 break;
					 default:
						 $color = 'label-new-request';
				 }
				?>
            <tr>
              <td><span class="tag <?php echo $color; ?>"><?php echo $status[$value->resume_status]; ?></span></td>
              <td><?php echo $candidates->first_name.' '.$candidates->last_name; ?></td>
              <td><?php echo $value->date_created; ?></td>
              <td><?php echo $Jobsdata->pay_rate; ?></td>
              <td><?php echo $Jobsdata->bill_rate; ?></td>
              <td>Null</td>
              <td style="text-align: center"><a href="" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
      <br>
    </div>
  </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/anychart/anychart-ui.css">
<script type="text/javascript">
  $(function () {
     $('[data-toggle="tooltip"]').tooltip()
     anychart.onDocumentReady(function() {
      // create a data set
	  var data = anychart.data.set([<?php echo $submissionMonthTotal; ?>]);
      //var data = anychart.data.set([['Thu',10],['Wed',0],['Tue',0],['Mon',0],['Sun',0],['Sat',0],['Fri',0],['Thu',0],['Wed',0],['Tue',0],['Mon',0],['Sun',0],['Sat',0],['Fri',0],['Thu',0],['Wed',0],['Tue',0],['Mon',0],['Sun',0],['Sat',0],['Fri',0],['Thu',0],['Wed',0],['Tue',0],['Mon',0],['Sun',0],['Sat',0],['Fri',0],['Thu',0],['Wed',100]]);
      // set the chart type
      chart = anychart.area();
      // create a series and set the data
      chart.area(data);
      // set the chart title
      
      // set the titles of the axes
      var xAxis = chart.xAxis();
      xAxis.title("<?php echo date('F'); ?>");
      var yAxis = chart.yAxis();
      yAxis.title("Submissions");
      // set the container id
      chart.container("area-chart");
      var tooltip = chart.tooltip();
      // adjust tooltip font
      tooltip.fontWeight(400);
      tooltip.fontFamily("Tahoma");
      tooltip.fontSize(12);
      // initiate drawing the chart
      chart.draw();
      var headCountData = 
           [
            {name:"Review", value:<?php echo $submissionCount; ?>},
            {name:"Interview", value:<?php echo $interviewCount; ?>},
            {name:"Offer", value:<?php echo $offerCount; ?>},
            {name:"Work Order", value:<?php echo $workorderCount; ?>},
            {name:"Hired", value:<?php echo $hiredCount; ?>}
          ];  
       var headCountChart = anychart.funnel( headCountData );
      
       // pass the container id, chart will be displayed there
       // set chart legend settings
      var legend = headCountChart.legend();
      legend.enabled(true);
      legend.position("center");
      legend.itemsLayout("horizontal");
      legend.align("top center");
      headCountChart.saveAsPdf();
      // set chart base width settings
      headCountChart.baseWidth("70%");
      // set the neck height
      headCountChart.neckHeight("0%"); 
       headCountChart.container("funnal-chart");
       headCountChart.background('white');
       // call the chart draw() method to initiate chart display
       headCountChart.draw();
    });
   })
</script>
