<?php $this->pageTitle = 'Update Job Template';

$userID = Yii::app()->user->id;
$ClientRate = ClientRate::model()->findByAttributes(array('client_id'=>$userID));
$setting = Setting::model()->findByPk($model->cat_id);
?>

<div class="main-content">
    <div class="row">
        <!--Condensed Table-->

        <!--Hover table-->
        <div class="col-sm-12">
            <div class="row"> </div>
        </div>

        <!--Hover table-->

        <!--Hover table-->
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-12">
                    <?php $form=$this->beginWidget('CActiveForm', array(

                        'id'=>'jobtemplet-form',

                        'enableAjaxValidation'=>false,

                    ));
                    ?>
                    <div class="panel panel-border-color panel-border-color-primary">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="">Job Title</label>
                                        <?php echo $form->textField($model,'job_title',array('class'=>'form-control')); ?> </div>
                                </div>
                                <!-- col -->
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="">Category</label>
                                        <?php
                                        $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'id', 'title');

                                        echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'form-control','options' => array($setting->title=>array('selected'=>true)))); ?>
                                    </div>
                                </div>
                                <!-- col -->

                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="">Experiences</label>
                                        <?php
                                        $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=10')),'title', 'title');

                                        echo $form->dropDownList($model, 'experience', $list , array('class'=>'form-control','options' => array($model->experience=>array('selected'=>true)))); ?>

                                    </div>
                                </div>
                                <!-- col -->

                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="">Job Level</label>
                                        <?php
                                        $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=40')),'title', 'title');

                                        echo $form->dropDownList($model, 'job_level', $list , array('class'=>'form-control','options' => array($model->job_level=>array('selected'=>true)))); ?>

                                    </div>
                                </div>
                            </div>
                            <!-- row -->

                            <div class="hirring-pipline">
                                <div class="row">
                                    <?php /*?><div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="">Primary Skill</label>
                                            <input type="text" name="JobTemplates[primary_skills]" value="<?php echo $model->primary_skills; ?>" class="form-control tokenfield-input" id="job_skills">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Secondary Skill</label>
                                            <input type="text" name="JobTemplates[secondary_skills]" value="<?php echo $model->secondary_skills; ?>" class="form-control tokenfield-input" id="job_skills1">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Good to have</label>
                                            <input type="text" name="JobTemplates[good_to_have]" value="<?php echo $model->good_to_have; ?>" class="form-control tokenfield-input" id="job_skills2">
                                        </div>
                                    </div><?php */?>
                   <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      <label for="">Primary Skill </label>
                      <?php $primary_skills = explode(',',str_replace(' ','',$model->primary_skills)) ?>
                      <select class="tokenize-remote-demo1" name="Job[skills][]" id="job_skills" multiple>
                        <?php //print_r($jobTempelatemodel->primary_skills[0]);//echo select_options($primary_skills); ?>
                        <?php 
						foreach($primary_skills as $value){
							echo '<option selected="selected" value="'.$value.'">'.$value.'</option>';
							}
						 ?>
                        
                      </select>
                      
                      <!--<input type="text" name="Job[skills]" class="form-control tokenize-remote-demo1" id="job_skills">--> 
                      
                    </div>
                    <div class="form-group">
                      <label for="">Secondary Skill</label>
                      <?php $secondary_skills = explode(',',str_replace(' ','',$model->secondary_skills)) ?>
                      <select class="tokenize-remote-demo1" name="Job[skills1][]" id="job_skills1"  multiple>
                        <?php //echo select_options($secondary_skills); ?>
                        <?php 
						foreach($secondary_skills as $value1){
							echo '<option selected="selected" value="'.$value1.'">'.$value1.'</option>';
							}
						 ?>
                      </select>
                      
                      <!--<input type="text" name="Job[skills1]" class="form-control tokenize-remote-demo1" id="job_skills1">--> 
                      
                    </div>
                    <div class="form-group">
                      <label for="">Good to have.</label>
                      <?php $good_to_have = explode(',',str_replace(' ','',$model->good_to_have)) ?>
                      <select class="tokenize-remote-demo1" name="Job[skills2][]" id="job_skills2" multiple>
                        <?php //echo select_options($good_to_have); ?>
                        <?php 
						foreach($good_to_have as $value2){
							echo '<option selected="selected" value="'.$value2.'">'.$value2.'</option>';
							}
						 ?>
                      </select>
                      
                      <!--<input type="text" name="Job[skills2]" class="form-control tokenize-remote-demo1" id="job_skills2">--> 
                      
                    </div>
                  </div>
                                    <!-- col-12 -->

                                </div>
                                <!-- row -->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="">Note for Skills</label>
                                            <textarea name="JobTemplates[notes_for_skills]" id="input" class="form-control" rows="5" required="required"><?php echo $model->notes_for_skills; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- row -->

                            </div>
                            <!-- hirring-pipline -->

                            <div class="row">
                                <div class="">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-info">
                                                <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                                                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed"> Job Description</a> </h4>
                                                </div>
                                                <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <textarea name="JobTemplates[job_description]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->job_description; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                                                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false"> You Will</a> </h4>
                                                </div>
                                                <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <textarea name="JobTemplates[you_will]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->you_will; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                                                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false"> Qualifications</a> </h4>
                                                </div>
                                                <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <textarea name="JobTemplates[qualification]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->qualification; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-heading" style="margin: 0; padding-left: 10px; padding-top: 10px;">
                                                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed" aria-expanded="false"> Additional Information</a> </h4>
                                                </div>
                                                <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <textarea name="JobTemplates[add_info]" class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->add_info; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- col-12 -->

                                </div>
                            </div>
                            <!-- row -->

 <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-striped table-hover job-template-table">
                  <thead>
                  <tr>
                    <th>Experience</th>
                    <th>Minimum Bill Rate</th>
                    <th>Maximum Bill Rate</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $i = 0;
                  $templateexp = explode(',',$model->temp_experience);
				  $temp_min_billrate = explode(',',$model->temp_min_billrate);
				  $temp_max_billrate = explode(',',$model->temp_max_billrate);
                  foreach($templateexp as $key=>$value){
					  if($value){
                    $i++;
                  ?>
                  <tr>
                    <td style="width: 40%"><?php echo $value; ?> <input type="hidden" name="temp_experience[]" value="<?php echo $value; ?>"> </td>
                    <td style="width: 30%">
                      <input type="text" name="temp_min_billrate[]" id="input" class="form-control" value="<?php echo $temp_min_billrate[$key]; ?>" >
                    </td>
                    <td style="width: 30%">
                      <input type="text" name="temp_max_billrate[]" id="input" class="form-control" value="<?php echo $temp_max_billrate[$key]; ?>" >
                    </td>
                  </tr>
                  <?php }
				   } ?>
                  </tbody>
                </table>
              </div> <!-- col -->
            </div>
                                        <br>
                            <div>
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>



                            <br>
                            <br>
                        </div>
                        <!-- panel-body -->
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>

        <!--Hover table-->

    </div>
</div>

<style>
    .tokenfield.form-control {
        height: auto !important;
    }
</style>