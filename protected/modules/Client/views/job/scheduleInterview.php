<?php 
	$jobData = Job::model()->findByPk($_GET['id']);
	$submissionData = VendorJobSubmission::model()->findByPk($_GET['submission-id']);
	$vendorData = Vendor::model()->findByPk($submissionData->vendor_id);
	$candidateData = Candidates::model()->findByPk($submissionData->candidate_Id);
	$candidateProfile = CandidatesProfile::model()->findByAttributes(array('candidate_id'=>$submissionData->candidate_Id));
	
	if(isset($_GET['interviewId'])){
		$interview = Interview::model()->findByPk($_GET['interviewId']);
		}else{
		$interview = Interview::model()->findByAttributes(array('submission_id'=>$_GET['submission-id']),array('order'=>'id desc'));
		}
	
	/*echo '<pre>';
	print_r($submissionData);
	exit;*/
if($submissionData->form_submitted == 0 && !isset($_GET['interviewId'])){ 
$this->pageTitle = 'New Appointment';
?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10">Appointment Schedule Information.</h4>
      <p class="m-b-42"> Schedule the date and time for the interview and type of interview too.</p>
      <div class="row add-job">
        <div class="col-md-12">
          <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-heading"></div>
            <div class="panel-body">
              <table class="table table-condensed table-50 ">
                <tbody>
                  <tr>
                    <td>Candidate Name</td>
                    <td><?php echo $candidateData->first_name.' '.$candidateData->last_name; ?></td>
                  </tr>
                  <tr>
                    <td>Current Location</td>
                    <td><?php echo $candidateData->current_location; ?></td>
                  </tr>
                  <tr>
                    <td>Job Title</td>
                    <td><?php echo $jobData->title.'('.$jobData->id.')';?></td>
                  </tr>
                </tbody>
              </table>
              <br>
              <hr>
              <br>
              <h4 class="m-b-20">Interview Details</h4>
              <?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'interview-form',
						'enableAjaxValidation'=>false,
						'htmlOptions' => array('enctype' => 'multipart/form-data'),
					)); ?>
              <table class="table table-condensed table-50 ">
                <tbody>
                  <tr>
                    <td>Event Name<i class="required-field fa fa-asterisk"></i></td>
                    <td><input type="text" name="interViewModel[interview_event_name]" value="<?php echo 'Interview Schedule for '.$candidateData->first_name.' '.$candidateData->last_name; ?>" class="form-control" id="" placeholder=""></td>
                  </tr>
                  <tr>
                    <td>Interview Type<i class="required-field fa fa-asterisk"></i></td>
                    <td><select name="interViewModel[interview_type]" id="input" class="form-control" required>
                        <option value="Phone Interview">Phone Interview</option>
                        <option value="Online Interview">Online Interview</option>
                        <option value="Face to Face Interview">Face to Face Interview</option>
                        <option value="Assignment Interview">Assignment Interview</option>
                      </select></td>
                  </tr>
                  <tr>
                      <input type="hidden" name="interViewModel[interview_status]" value="1">
                  </tr>
                </tbody>
              </table>
              <br>
              <hr>
              <br>
              <h4 class="m-b-20">Recommended Interview Date</h4>
              <table class="table table-condensed remove-header table-50 ">
                <tbody>
                  <tr>
                    <td>Start Date<i class="required-field fa fa-asterisk"></i></td>
                    <td><input type="text" name="interViewModel[interview_start_date]" class="form-control datetimepickermodified" value="" required="required"></td>
                  </tr>
                  
                  <tr>
                    <td>Start Time<i class="required-field fa fa-asterisk"></i></td>
                    <td>
                    <input type="text" name="interViewModel[interview_start_date_time]" class="form-control time-picker" id="interview_start_date_time">
                      
                    </td>  
                  </tr>
                  <tr>
                    <td>End Time<i class="required-field fa fa-asterisk"></i></td>
                    <td>
                    	<input type="text" name="interViewModel[interview_end_date_time]" class="form-control time-picker" id="end_date_time">
                    </td>
                  </tr>
                </tbody>
              </table>
              <p class="m-b-20"> Alternative Time & Date 1 </p>
              <table class="table table-condensed table-50 ">
                <tbody>
                  <tr>
                    <td>Start Date</td>
                    <td><input type="text" name="interViewModel[interview_alt1_start_date]" class="datetimepickermodified form-control"></td>
                  </tr>
                  <tr>
                    <td>Start Time</td>
                    <td>
                      <input type="text" class="form-control timepicker time-picker" name="interViewModel[interview_alt1_start_date_time]" id="alt1_start_date_time" >
                      
                    </td>
                  </tr>
                  <tr>
                    <td>End Time</td>
                    <td>
                    <input type="text" class="form-control timepicker time-picker" name="interViewModel[interview_alt1_end_date_time]" id="alt1_end_date_time" >
                    </td>
                  </tr>
                </tbody>
              </table>
              <p class="m-b-20"> Alternative Time & Date 2 </p>
              <table class="table table-condensed table-50 ">
                <tbody>
                  <tr>
                    <td>Start Date</td>
                    <td><input type="text" class="datetimepickermodified form-control" name="interViewModel[interview_alt2_start_date]" ></td>
                  </tr>
                  <tr>
                    <td>Start Time</td>
                    <td>
                    <input type="text" class="form-control timepicker time-picker" name="interViewModel[interview_alt2_start_date_time]" id="alt2_start_date_time" >
                      
                    </td>
                  </tr>
                  <tr>
                    <td>End Time</td>
                    <td>
                    <input type="text" class="form-control timepicker time-picker" name="interViewModel[interview_alt2_end_date_time]" id="alt2_end_date_time" >
                    </td>
                  </tr>
                </tbody>
              </table>
              <p class="m-b-20"> Alternative Time & Date 3 </p>
              <table class="table table-condensed table-50 ">
                <tbody>
                  <tr>
                    <td>Start Date</td>
                    <td><input type="text" id="" class="datetimepickermodified form-control" name="interViewModel[interview_alt3_start_date]" ></td>
                  </tr>
                  <tr>
                    <td>Start Time</td>
                    <td>
                    <input type="text" class="form-control timepicker time-picker" name="interViewModel[interview_alt3_start_date_time]" id="alt3_start_date_time" >
                     
                    </td>
                  </tr>
                  <tr>
                  
                    <td>End Time</td>
                    <td>
                    <input type="text" class="form-control timepicker time-picker" name="interViewModel[interview_alt3_end_date_time]" id="alt3_end_date_time" >
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <hr>
              <br>
              <h4 class="m-b-20">Other Details</h4>
              <table class="table table-condensed table-50 ">
                <tbody>
                  <tr>
                    <td>Where</td>
                    <td><select name="interViewModel[interview_where]" id="input" class="form-control" >
                        <option value=""></option>
                        <?php
							$location = Location::model()->findAllByAttributes(array('reference_id'=>$jobData->user_id));
							foreach($location as $location){ ?>
                        <option value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                        <?php  } 	?>
                      </select></td>
                  </tr>
                  <tr>
                    <td>Description</td>
                    <td><textarea name="interViewModel[interview_description]" id="input" class="form-control" rows="3" ></textarea></td>
                  </tr>
                  <tr>
                    <td>Notes For Interview</td>
                    <td><textarea name="interViewModel[notes]" id="input" class="form-control" rows="3" ></textarea></td>
                  </tr>
                  <tr>
                    <td>Attachment</td>
                    <td><?php echo $form->fileField($interViewModel, 'interview_attachment'); ?></td>
                  </tr>
                </tbody>
              </table>
              <br>
              <hr>
              <br>
              <h4 class="m-b-20">Invite Members</h4>
              <table class="table table-condensed table-50 ">
                <tbody>
                  <tr>
                    <td>Invite your Team member </td>
                    <td><select name="interViewModel[invite_team_member][]" multiple="" class="select2">
                        <?php
						$friend = Client::model()->findAllByAttributes(array('super_client_id'=>UtilityManager::superClient(Yii::app()->user->id)));
						foreach($friend as $friend){ ?>
                        <option value="<?php echo $friend->id; ?>"><?php echo $friend->first_name.' ('.$friend->email.')'; ?></option>
                        <?php  } 	?>
                      </select></td>
                  </tr>
                  <tr>
                    <td>Add Guests</td>
                    <td><select name="interview_added_guests[]" id="interview_guests" class="tokenize-remote-demo2" multiple>
                      </select>
                      
                      <!--<input type="text" name="interViewModel[interview_added_guests]" id="interview_guests" class="form-control" value="" placeholder="Enter Guest email Address" >--> 
                      <br>
                      
                      <!--<button type="button" class="btn btn-info">Add</button>--></td>
                  </tr>
                </tbody>
              </table>
              <div class="m-t-20">
                <button type="submit" id="interview-btn" name="resume_status" class="btn btn-success">SEND INVITE FOR INTERVIEW</button>
              </div>
              <br>
              <br>
              <?php $this->endWidget(); ?>
            </div>
            <!-- panel-body --> 
          </div>
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<?php }else{ 
	$vendorjobsubmissionData = $_GET['submission-id'];
	$location = Location::model()->findByPk($interview->interview_where);
	
	$jobId = $_GET['id'];
	$this->pageTitle = 'Interview';
	
	$flag = '';
	 if($interview->start_date_status == 1 || $interview->alt1_date_status == 1 || $interview->alt2_date_status == 1 || $interview->alt3_date_status == 1 || $interview->status==3 || $interview->status==4 || $interview->status==5){
		 $flag = true;
		 }
		 if($flag == true){
			 
			 $oldOffer = Offer::model()->findAll(array('condition'=>'submission_id='.$_GET['submission-id'].' and (status="Pending" or status="Approved") order by id desc limit 1'));
			 
			 if($oldOffer){
				  $offerId = $oldOffer[0]['id'];
				 }else{
					  $offerId = 'Null';
					  }
		 }
			  ?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10"><?php echo 'InterviewID : '.$interview->id; ?> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/interviewList'); ?>" class="btn btn-sm btn-default pull-right"> Back to Interviews</a> </h4>
      <p class="m-b-30">A Detail view of the interview schedule</p>
      <div class="row">
        <?php if(Yii::app()->user->hasFlash('success')):
			  echo Yii::app()->user->getFlash('success');
			endif;
		?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
          
          <!-- if interview is pending then reject and approve buttons will be there. -->
          <?php if ($interview->status==1){ ?>
          <?php /*?><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/aprrovedFirsttime',array('id'=>$jobId,'submissonId'=>$vendorjobsubmissionData,'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-success" <?php echo $interview->status==2 || $interview->status== 5 ?'style="pointer-events: none;"':''; ?>> Accept Interview</a><?php */?>
          <a href="#modal-id" data-toggle="modal" class="btn btn-sm btn-danger"> Reject Interview</a>
          <?php } ?>
          
          <!-- if interview is approved then interview completed button will be there. -->
          <?php if ($interview->status==2){ ?>
          <a href="<?php echo $this->createAbsoluteUrl('job/interviewCompleted',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-success"> Interview Completed</a> <a href="#modal-id" data-toggle="modal" class="btn btn-sm btn-danger"> Reject Interview</a>
          <?php } ?>
          
          <!-- if interview is completed or cancelled then reschedule button will be there. -->
          <?php if($interview->status==3 || $interview->status==5){
			
			//checkinf for pending interveiw for this submission.
			$pendingInterview = Interview::model()->findAllByAttributes(array('submission_id'=>$_GET['submission-id'],'status'=>1),
			array('order'=>'id desc','limit'=>2));
			
			if($pendingInterview){
				//$intID = '/interviewId/'.$pendingInterview[0]['id'];
				}else{  ?>
                
                <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/scheduleInterview',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'])); ?>" class="btn btn-sm btn-default-2"> Schedule New Interview</a>
                	<?php
				     //$intID = '';
					 }
			?>
          
          <?php } ?>
          
          <!-- if the interview is approved then these buttons will be there -->
          <?php if($interview->status==2){ ?>
          <a href="<?php echo $this->createAbsoluteUrl('interview/readyforOffer',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-default-2"> Candidate Ready for Offer</a> <a href="<?php echo $this->createAbsoluteUrl('interview/rejectOfferSub',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-default-2"> Reject Candidate</a>
          <?php } ?>
          
          <!-- if the interview is completed then these buttons will be there -->
          <?php if($interview->status==5){ ?>
          <a href="<?php echo $this->createAbsoluteUrl('interview/readyforOffer',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-default-2"> Candidate Ready for Offer</a> <a href="<?php echo $this->createAbsoluteUrl('interview/rejectOfferSub',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-default-2"> Reject Candidate</a> <a href="#modal-id" data-toggle="modal" class="btn btn-sm btn-danger"> Reject Interview</a>
          <?php } ?>
          <?php /*?><?php if ($interview->status!=3){ 
							if($interview->status!=5){ ?>
          <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/aprrovedFirsttime',array('id'=>$jobId,'submissonId'=>$vendorjobsubmissionData,'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-success" <?php echo $interview->status==2 || $interview->status== 5 ?'style="pointer-events: none;"':''; ?>> Accept Interview</a> <a href="<?php echo $this->createAbsoluteUrl('job/interviewCompleted',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-success"> Interview Completed</a> <a href="#modal-id" data-toggle="modal" class="btn btn-sm btn-danger"> Reject Interview</a>
          <?php } } 
				if($interview->status!=5){
				?>
          <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/scheduleInterview',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'])); ?>" class="btn btn-sm btn-default-2"> Request for Reschedule</a>
          <?php }else{ ?>
          <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/interviewStep1'); ?>"  class="btn btn-sm btn-default-2">Add New Appointment</a>
          <?php }?>
          
          <!-- candidate ready for offer will be disabale until the interview status is 5-->
          <?php if($interview->status==5) {
						if ($interview->status!=3){ 
						?>
          <a href="<?php echo $this->createAbsoluteUrl('interview/ReadyforOffer',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>" class="btn btn-sm btn-default-2"> Candidate Ready for Offer</a>
          <?php } } ?><?php */?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
          <div class="simplify-tabs blue-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <li class="nav-item"> <a class="nav-link active"  href="#submission" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Summary</a> </li>
                <li class="nav-item"> <a class="nav-link"  href="#rates" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Date & Time</a> </li>
              </ul>
              <?php 
				$postingstatus = UtilityManager::interviewStatus();
				switch ($postingstatus[$interview->status]) {
					case "Approved":
						$color = 'label-open';
						break;
					case "Waiting for Approval":
						$color = 'label-interview-pending';
						break;
					case "Cancelled":
						$color = 'label-interview-cancelled';
						break;
					case "Reschedule":
						$color = 'label-interview-reschedule';
						break;
			   case "Interview Completed":
						$color = 'label-interview-completed';
						break;
					default:
						$color = 'label label-primary';
				}
				?>
              <div class="tab-content   p-r-0 p-l-0">
                <div class="tab-pane active" id="submission"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table class="table tab-table m-b-20">
                        <tbody>
                          <tr>
                            <td>Status:</td>
                            <td><span class="tag <?php echo $color ?>"><?php echo $postingstatus[$interview->status]; ?></span></td>
                          </tr>
                          <tr>
                            <td>Name of the Candidate:</td>
                            <td><?php echo $candidateData->first_name.' '.$candidateData->last_name; ?></td>
                          </tr>
                          <tr>
                            <td>Current Location:</td>
                            <td><?php echo $candidateData->current_location; ?></td>
                          </tr>
                          <tr>
                            <td>Type of Inteview</td>
                            <td><?php echo $interview->interview_type; ?></td>
                          </tr>
                          <tr>
                            <td>Job Title:</td>
                            <td><a href=""><?php echo $jobData->title.'('.$jobData->id.')'; ?></a></td>
                          </tr>
                          <tr>
                            <?php
									if($interview->start_date_status){
										$date = $interview->interview_start_date.' '.$interview->interview_start_date_time.' to '. $interview->interview_end_date_time;
										$timeDuration = $interview->first_duration;
									}
									else if($interview->alt1_date_status){
										$date = $interview->interview_alt1_start_date.' '.$interview->interview_alt1_start_date_time.' to '.$interview->interview_alt1_end_date_time;
										$timeDuration = $interview->second_duration;
									}
									else if($interview->alt2_date_status){
										$date = $interview->interview_alt2_start_date.' '.$interview->interview_alt2_start_date_time.' to '.$interview->interview_alt2_end_date_time;
										$timeDuration = $interview->third_duration;
									}
									else if($interview->alt3_date_status){
										$date = $interview->interview_alt3_start_date.' '.$interview->interview_alt3_start_date_time.' to '.$interview->interview_alt3_end_date_time;
										$timeDuration = $interview->fourth_duration;
									}else{
										$date = $interview->interview_start_date.' '.$interview->interview_start_date_time.' to '.$interview->interview_end_date_time;
										$timeDuration = $interview->first_duration;
									}
									$newTime = explode(' ',$date);
									?>
                            <th>Date of Inteview:</th>
                            <th><?php echo date('m-d-Y',strtotime($newTime[0])); ?></br>
                              <?php echo $newTime[1].' to '.$newTime[3].' '.$newTime[4].' '.$newTime[5]; ?></br>
                              <?php //echo $timeDuration.' Minutes'; ?>
                               </th>
                          </tr>
                          <tr>
                            <td>Resume:</td>
                            <td><?php if($candidateData->resume){ ?>
                              <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/downloadResume',array('id'=>$candidateData->id)); ?>" download class="blue"><?php echo $candidateData->resume; ?><i class="fa fa-download"></i></a>
                              <?php } ?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane " id="rates"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table class="table tab-table">
                        <tbody>
                          <?php 
							if($interview->interview_start_date_time != ''){ ?>
                          <tr>
                            <td><?php $string = $interview->interview_start_date;
									$timestamp = strtotime($string);
									echo date("l d M, Y", $timestamp);
									?>
                              <br>
                              <small><?php echo $interview->interview_start_date_time; ?> to <?php echo $interview->interview_end_date_time; ?><br /><?php //echo $interview->first_duration.' Minutes' ?></small></td>
                            <td class="text-center tdbuttons"><?php if($interview->start_date_status==1){ ?>
                              <a>Approved</a>
                              <?php }else{ if($flag == false){ ?>
                              <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/aprrovedFirsttime',array('id'=>$jobId,'submissonId'=>$vendorjobsubmissionData,'interviewId'=>$interview->id)); ?>">Accept</a>
                              <?php } } ?>
                              
                              <!--<a >Reject</a>--></td>
                          </tr>
                          <?php } ?>
                          <?php 
							if($interview->interview_alt1_start_date_time != ''){ ?>
                          <tr>
                            <td><?php $string1 = $interview->interview_alt1_start_date;
									$timestamp1 = strtotime($string1);
									echo date("l d M, Y", $timestamp1);
									?>
                              <br>
                              <small><?php echo $interview->interview_alt1_start_date_time; ?> to <?php echo $interview->interview_alt1_end_date_time; ?><br /><?php //echo $interview->second_duration.' Minutes' ?></small></td>
                            <td class="text-center tdbuttons"><?php if($interview->alt1_date_status==1){ ?>
                              <a>Approved</a>
                              <?php }else{ if($flag == false){ ?>
                              <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/aprrovedSecondtime',array('id'=>$jobId,'submissonId'=>$vendorjobsubmissionData,'interviewId'=>$interview->id)); ?>">Accept</a>
                              <?php } } ?>
                              
                              <!--<a >Reject</a>--></td>
                          </tr>
                          <?php } ?>
                          <?php if($interview->interview_alt2_start_date_time != ''){ ?>
                          <tr>
                            <td><?php $string2 = $interview->interview_alt2_start_date;
								$timestamp2 = strtotime($string2);
								echo date("l d M, Y", $timestamp2);
								?>
                              <br>
                              <small><?php echo $interview->interview_alt2_start_date_time; ?> to <?php echo $interview->interview_alt2_end_date_time; ?><br /><?php //echo $interview->third_duration.' Minutes' ?></small></td>
                            <td class="text-center tdbuttons"><?php if($interview->alt2_date_status==1){ ?>
                              <a >Approved</a>
                              <?php }else{ if($flag == false){ ?>
                              <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/aprrovedThirdtime',array('id'=>$jobId,'submissonId'=>$vendorjobsubmissionData,'interviewId'=>$interview->id)); ?>">Accept</a>
                              <?php } } ?>
                              
                              <!--<a >Reject</a>--></td>
                          </tr>
                          <?php } ?>
                          <?php if($interview->interview_alt3_start_date_time != ''){ ?>
                          <tr>
                            <td><?php $string3 = $interview->interview_alt3_start_date;
									  $timestamp3 = strtotime($string3);
									  echo date("l d M, Y", $timestamp3);
								 ?>
                              <br>
                              <small><?php echo $interview->interview_alt3_start_date_time; ?> to <?php echo $interview->interview_alt3_end_date_time; ?><br /><?php //echo $interview->fourth_duration.' Minutes' ?></small></td>
                            <td class="text-center tdbuttons"><?php if($interview->alt3_date_status==1){ ?>
                              <a >Approved</a>
                              <?php }else{ if($flag == false){ ?>
                              <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/aprrovedfourthtime',array('id'=>$jobId,'submissonId'=>$vendorjobsubmissionData,'interviewId'=>$interview->id)); ?>">Accept</a>
                              <?php } } ?>
                              
                              <!--<a >Reject</a>--></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
          </div>
          <!-- simplify-tabs --> 
        </div>
        <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
          <div class="simplify-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <?php if(!empty($interview->reason_rejection)){ ?>
                <li class="nav-item"> <a class="nav-link"  href="#rejected" role="tab" data-toggle="tab" data-placement="bottom" title="Rejected"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Rejected</a> </li>
                <?php } ?>
                <li class="nav-item"> <a class="nav-link active"  href="#resume" role="tab" data-toggle="tab" data-placement="bottom" title="Resume"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Resume</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#skills" role="tab" data-toggle="tab" data-placement="bottom" title="Skills"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Skills</a> </li>
                <li class="nav-item"> <a class="nav-link "  href="#feedback" role="tab" data-toggle="tab" data-placement="bottom" title="Comments"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Feedback</a> </li>
                <!--<li class="nav-item">
                        <a class="nav-link "  href="#comments" role="tab" data-toggle="tab" data-placement="bottom" title="Comments">
                            <i class=" nav-icon"><img src="<?php //echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i>
                            Notes & Comments</a>
                    </li>-->
                  <?php if(!empty($interview->interview_attachment)) { ?>
                  <li class="nav-item">
                                        <a class="nav-link "  href="#attachment" role="tab" data-toggle="tab" data-placement="bottom" title="Interview attachment">
                                            <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i>
                                            Interview Attachment</a>
                                    </li>
                  <?php } ?>
                  <li class="nav-item">
                      <a class="nav-link " href="#interivew-notes" role="tab" data-toggle="tab" data-placement="bottom" title="Comments">
                          <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i>
                          Notes</a>
                  </li>
              </ul>
              <div class="tab-content ">
                <div class="tab-pane " id="rejected"  role="tabpanel">
                  <?php if(!empty($interview->reason_rejection)){ ?>
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="alert-box m-t-10">
                        <p class="bold m-b-10">Reason for Rejection</p>
                        <p class="m-b-10"><?php echo $interview->reason_rejection; ?>.</p>
                        <p class="bold m-b-10">Notes:</p>
                        <p class="m-b-10"><?php echo $interview->notes; ?>.</p>
                        <!--<p><strong>Posted By :</strong> Person Name at 10:30 PM on 10th May 2015</p>--> 
                      </div>
                    </div>
                  </div>
                  <?php }else{ ?>
                  No Comments
                  <?php } ?>
                </div>
                <div class="tab-pane  m--31" id="skills"  role="tabpanel">
                  <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
                    <thead>
                      <tr>
                        <th>Primary Skill</th>
                        <th>Year of Experience</th>
                        <th style="text-align: center;">Rating</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php  $primarySkill = unserialize($candidateData->primary_skill);
							if($primarySkill)
							foreach($primarySkill['experience'] as $key=>$value) {
							?>
                      <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $primarySkill['experience'][$key]; ?></td>
                        <td style="text-align: right;">
						<?php  $rating = explode('(',$primarySkill['rating'][$key]);
								$prating = trim($rating[0]);
								if($prating=='Intermediates'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          / Intermediates
                          <?php } if($prating=='Advanced'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> </p>
                          /Advanced
                          <?php } if($prating=='Beginners'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star-half-full filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          / Beginners
                          <?php } ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                    <thead>
                      <tr>
                        <th>Secondary Skill</th>
                        <th>Year of Experience</th>
                        <th style="text-align: center;">Rating</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
						$secondarySkill = unserialize($candidateData->secondary_skill);
						if($secondarySkill)
						foreach($secondarySkill['experience'] as $key=>$value) { ?>
                      <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $secondarySkill['experience'][$key]; ?></td>
                        <td style="text-align: right;">
						<?php  $rating = explode('(',$secondarySkill['rating'][$key]);
								$srating = trim($rating[0]);
								if($srating=='Intermediates'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          /Intermediates
                          <?php } if($srating=='Advanced'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> </p>
                          /Advanced
                          <?php }
                                                        if($srating=='Beginners'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star-half-full filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          /Beginners
                          <?php } ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                    <thead>
                      <tr>
                        <th>Other skills</th>
                        <th>Year of Experience</th>
                        <th style="text-align: center;">Rating</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $otherSkill = unserialize($candidateData->other_skill);
							if($otherSkill)
							foreach($otherSkill['experience'] as $key=>$value) {
							?>
                      <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $otherSkill['experience'][$key]; ?></td>
                        <td style="text-align: right;"><?php  $otherSkill['rating'][$key]; ?>
                          <?php  $rating = explode('(',$otherSkill['rating'][$key]);
                                                        $orating = trim($rating[0]);
                                                        if($orating=='Intermediates'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          /Intermediates
                          <?php }
                                                        if($orating=='Advanced'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> </p>
                          /Advanced
                          <?php }
                                                        if($orating=='Beginners'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star-half-full filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          /Beginners
                          <?php }
                                                        ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <div class="modal fade" id="modal-id">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <?php if($interview->status != 'Rejected'){ ?>
                      <form method="POST" action="<?php echo Yii::app()->createUrl('Client/job/scheduleStatus',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>" role="form"  class="reject-form">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Reject Interview</h4>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                            <label for="">Reason for Rejection</label>
                            <select name="Interview[reason_rejection]" id="interivestatus" class="form-control" required>
                              <option value=""></option>
                              <?php
                                                                $reasonData = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=35')),'title', 'title');
                                                                foreach($reasonData as $value){ ?>
                              <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                              <?php	} ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="">Note</label>
                            <textarea name="Interview[notes]" id="Interview[notes]" class="form-control" rows="5" required></textarea>
                          </div>
                        </div>
                        <div class = "modal-footer">
                          <button type="submit" name="saveInterviewStatus" class="btn btn-success">Save</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                      </form>
                    </div>
                    <?php }else{ echo '<p style="color:red;">Interview of this candidate is already Rejected.</p>'; } ?>
                  </div>
                </div>
                <div class="tab-pane active" id="resume"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <?php $this->renderPartial('si_resume'); ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="feedback"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <?php $this->renderPartial('si_feedback',array('interviewID'=>$interview->id)); ?>
                    </div>
                  </div>
                </div>
                <!-- feedback -->
                <div class="tab-pane" id="comments"  role="tabpanel">
                  <?php $this->renderPartial('si_notecomments'); ?>
                </div>
                  <div class="tab-pane" id="attachment"  role="tabpanel">
                      <?php $this->renderPartial('interviewattachment',array('interview'=>$interview)); ?>
                  </div>
                  <div class="tab-pane" id="interivew-notes" role="tabpanel"><div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="interview-notes-comments "><div class="row"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="media no-border"><div class="media-body">
                                                  <?php if($interview->interview_description) { ?>
                                                      <p><h4 class="m-b-10">Description </h4>
                                                      <?php echo $interview->interview_description; ?></p><br /><br />
                                                  <?php } if($interview->notes) { ?>
                                                      <p><h4 class="m-b-10">Notes For Interview </h4>
                                                      <?php echo $interview->notes; ?></p>
                                                  <?php } ?>
                                              </div></div>
                                          <!-- col -->
                                      </div>
                                      <!-- row -->
                                  </div></div></div></div></div>
              </div>
            </div>
            <!-- tabpanel --> 
          </div>
          <!-- simplify-tabs --> 
        </div>
      </div>
    </div>
    <!-- col --> 
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<?php } ?>
<?php /*?><style>
	.table-condensed{ width:100% !important;}
	.table-condensed thead{ display:none !important;}
</style><?php */?>
<div class="modal fade" id="reschedual">
  <div class="modal-dialog">
    <form name="" method="post" action="<?php echo Yii::app()->createUrl('Client/interview/reschedualInterview',array('id'=>$_GET['id'],'submission-id'=>$_GET['submission-id'],'interviewId'=>$interview->id)); ?>">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Rescheduale Interview</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="">Reason for rescheduale</label>
            <select name="reason_for_reschedual" id="input" class="form-control" required>
              <option value="">selecte Reasone</option>
              <option value="Reasone One">Reasone One</option>
              <option value="Reasone two">Reasone two</option>
            </select>
          </div>
          <div class="form-group">
            <label for="">Note</label>
            <textarea name="reschedual_note" id="input" class="form-control" rows="3" required></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" name="Reschedual" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>
  </div>
</div>


