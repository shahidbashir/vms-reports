<?php $model = Interview::model()->findByPk($interviewID);  ?>
<div class="interview-feedback">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php if($model->feedback_status==1){ ?>
      <div class="well">
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Educational Background:</strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->edu_background>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->edu_background>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->edu_background>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->edu_background>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->edu_background>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Prior Work Experience: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->prior_w_exp>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->prior_w_exp>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->prior_w_exp>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->prior_w_exp>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->prior_w_exp>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Technical Qualification / Experience: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->tech_work_exp>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->tech_work_exp>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->tech_work_exp>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->tech_work_exp>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->tech_work_exp>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Verbal Communication: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->verbal_communication>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->verbal_communication>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->verbal_communication>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->verbal_communication>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->verbal_communication>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Candidate Enthusiasm: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->can_enthusiasm>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->can_enthusiasm>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->can_enthusiasm>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->can_enthusiasm>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->can_enthusiasm>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Knowledge of Company: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->knowledge_company>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->knowledge_company>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->knowledge_company>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->knowledge_company>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->knowledge_company>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Team building / interpersonal skills: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->inter_personal_skills>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->inter_personal_skills>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->inter_personal_skills>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->inter_personal_skills>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->inter_personal_skills>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Initiative: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->initiative>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->initiative>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->initiative>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->initiative>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->initiative>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Time Management: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->time_management>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->time_management>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->time_management>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->time_management>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->time_management>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Customer Service: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->customer_service>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->customer_service>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->customer_service>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->customer_service>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->customer_service>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Overall Impression and Recommendation: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="#el" class="static">
              <li class="c-rating__item <?php echo $model->overall_impression>=1?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->overall_impression>=2?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->overall_impression>=3?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->overall_impression>=4?'is-active':''; ?>" ></li>
              <li class="c-rating__item <?php echo $model->overall_impression>=5?'is-active':''; ?>" ></li>
            </div>
          </div>
          <!-- col --> </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="m-b-10 text-right"><strong>Note: </strong></p>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <p class="normal-font"><?php echo $model->rating_notes ?></p>
          </div>
          <!-- col --> </div>
        <!-- row --> <!-- row --> </div>
      <?php }	$list = array('1'=>'Unsatisfactory','2'=>'Below Average','3'=>'Average','4'=>'Above Average','5'=>'Exceptional');	if($model->feedback_status==1){		$class = true;		}else{			 $class = false;			 }	 ?>
      <?php $form=$this->beginWidget('CActiveForm', array(		'id'=>'worklflow-form',		'enableAjaxValidation'=>false,		'htmlOptions' => array('enctype' => 'multipart/form-data'),    )); ?>
      <p><strong>Educational Background: </strong>Does the Candidate have the appropiate educational qualification of traning for this position? </p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'edu_background', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'edu_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Prior Work Experience: </strong>Has the candidate acquired similar skills or qualification through past work experience?</p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'prior_w_exp', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'prior_w_exp_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Technical Qualification / Experience: </strong> Does the candiate have the technical skills necessary for the position? </p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'tech_work_exp', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'tech_work_exp_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Verbal Communication: </strong>How were the candidate's communication skills during the interview? </p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'verbal_communication', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'verbal_communication_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Candidate Enthusiasm: </strong>How much interset did the candidate show in the position and the company?</p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'can_enthusiasm', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'can_enthusiasm_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Knowledge of Company: </strong>Did the candiate research the company prior to the interview?</p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'knowledge_company', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'knowledge_company_notes',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Team building / interpersonal skills: </strong>Did the candiate demostrate, through his or her answers, good team building / interpersonal skills?</p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'inter_personal_skills', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'inter_personal_skills_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Initiative: </strong>Did the candiate demostrate, through his or her answers, good team building / interpersonal skills?</p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'initiative', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'initiative_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Time Management: </strong>Did the canddidate demostrate, through his or her answers, good time management skills?</p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'time_management', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'time_management_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Customer Service: </strong>Did the candiate demostrate, through his or her answers, a high level of Customer Service skills/abilities?</p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'customer_service', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'customer_service_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row -->
      <p><strong>Overall Impression and Recommendation: </strong>Final comments and recommendations for proceeding with the candiate?</p>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Rating</label>
            <?php echo $form->dropDownList($model, 'overall_impression', $list , array('class'=>'form-control','empty' => 'Select','disabled'=>$class)); ?> </div>
        </div>
        <!-- col -->
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group m-t-10">
            <label for="">Comments</label>
            <?php echo $form->textField($model,'overall_impression_comments',array('class'=>'form-control','readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row --> <!--assessment_documents-->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="">Note:</label>
            <?php echo $form->textArea($model,'rating_notes',array('class'=>'form-control','rows'=>"5",'readonly'=>$class)); ?> </div>
        </div>
        <!-- col --> </div>
      <!-- row --> <!--<input type="submit" name="feedback" value="Save" data-toggle="modal" href='#modal-id' class="btn btn-success" />-->
      <div class="modal fade" id="modal-feed">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Interview Feedback</h4>
            </div>
            <div class="modal-body">
              <p>Please confirm if you want to submit your feedback for this candidate. Once submitted you can’t edit the feedback.</p>
            </div>
            <div class="modal-footer">
              <button type="submit" name="feedback" class="btn btn-success">Continue</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>
      <?php $this->endWidget(); ?>
      <?php if($model->feedback_status==0){ ?>
      <div class="form-group"> <a data-toggle="modal" href='#modal-feed' class="btn btn-success">Save</a> </div>
      <?php } ?>
      <br>
    </div>
    <!-- col --> </div>
  <!-- row --></div>
<!-- interview-feedback --><script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/five-star-rating/js/dist/rating.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/five-star-rating/css/rating.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/five-star-rating/css/style.min.css">
