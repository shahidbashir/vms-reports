<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td1 = tr[i].getElementsByTagName("td")[1];
            td2 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            td5 = tr[i].getElementsByTagName("td")[5];
            td6 = tr[i].getElementsByTagName("td")[6];
            if (td || td1 || td2 || td3 || td4 || td5 || td6) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                }else if(td1.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td2.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td3.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td4.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td5.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }else if(td6.innerHTML.toUpperCase().indexOf(filter) > -1){
                    tr[i].style.display = "";
                }
                else {
                    tr[i].style.display = "none";
                }

            }
        }
    }
</script>
<?php
 if($_GET['type']=='offer'){
	 $this->pageTitle = 'Offers';
	 }else{
 	$this->pageTitle = 'Workorder';
	 }
 $this->renderPartial('/job/_menu',array('model'=>$model));
 ?>

 <div class="tab-content ">
     <div class="tab-pane active" id="one" role="tabpanel">
         <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="">
                 <div class="two-fields">
                     <div class="form-group">
                         <label for=""></label>
                         <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search by keyword">
                     </div>
                     <div class="form-group">
                         <label for="">&nbsp;</label>
                         <button type="button" class="btn btn-primary btn-block">Search</button>
                     </div>
                 </div>
                 <!-- two-flieds -->

             </div>
             <br>
             <p class="bold m-b-20"> <a href="" class="pull-right"> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/listing@512px.svg"></i> </a> <a href="" class="pull-right" style="margin-right: 10px; "> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/menu@512px.svg"></i> </a> </p>
             <br>
             
             <table id="myTable" class="table m-b-40 without-border" >
            <thead class="thead-default">
              <tr>
                <th  style="width: 10px;">Status</th>
                <th style="width: 140px;"> Work Order ID </th>
                <th>Candidate Name</th>
                <th>Vendor Name</th>
                <th> Bill Rate </th>
                <th style="width: 157px;">Location of Work</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php if($workorders){			  
					$i = 0;
					foreach($workorders as $value) {
						$i++;
					$jobModel = Job::model()->findByPk($value['job_id']);
					$vendor = Vendor::model()->findByPk($value['vendor_id']);
					$offerStatus = UtilityManager::workorderStatus();
					$offerdata = Offer::model()->findByPk($value['offer_id']);
					$location = Location::model()->findByPk($offerdata->approver_manager_location);

					if($value['workorder_status'] == 1){
						$className ="label-hold";
					}else if($value['workorder_status'] == 2){
						$className ="label-pending-aproval";
					}elseif($value['workorder_status'] == 0){
						$className ="label-new-request";
					}else{
						$className ="info";
					}
					?>
              <tr>
                <td><span class="tag <?php echo $className;?>"><?php echo  $offerStatus[$value['workorder_status']];?></span></td>
                <td><a href=""><?php echo $value['workorder_id'];?></a></td>
                <td><a href=""><?php echo $value['first_name'].' '.$value['last_name'];?></a></td>
                <td><?php echo $vendor->organization; ?></td>
                <td><?php echo $value['wo_bill_rate']; ?></td>
                <td><?php echo $location->name; ?></td>
                <td style="text-align: center" class="actions"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/workorderView',array('id'=>$value['workorder_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a> 
                  <!--<a href="#" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/delete@512px-grey.svg"></i></a>--></td>
              </tr>
              <?php } }else{ ?>
              <tr>
                <td colspan="7">Sorry no record found...</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
             <div class="row m-b-10" style="padding: 10px 0 10px; ">
                 <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                     <?php
                     $this->widget('CLinkPager', array('pages' => $pages,
                         'header' => '',
                         'nextPageLabel' => 'Next',
                         'prevPageLabel' => 'Prev',
                         'selectedPageCssClass' => 'active',
                         'hiddenPageCssClass' => 'disabled',
                         'htmlOptions' => array('class' => 'pagination m-t-0 m-b-0',)));
                     ?>
                 </div>
             </div>
         </div>
             </div>
     </div>
 </div>
 </div>
 </div>
 </div>



<div class="modal fade" id="submision-job-select">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Select Job</h4>
      </div>
         <form action="<?php echo $this->createAbsoluteUrl('/Admin/job/createsubmission');?>" method="post" role="form">
      <div class="modal-body">
      <?php $assignedVendor =  AssignedVendorClient::model()->findAll(array('condition'=>'client_id='.$model->user_id));
      if($assignedVendor){
      ?>


        <input type="hidden" name="id" value="<?php echo $model->id;?>">
          <div class="form-group">
            <label for=""> Select Vendor</label>

             <div class="single">
                <select name="vendor-id" class="ui fluid search dropdown">
                 <option value=""></option>
                <?php foreach($assignedVendor as $value){
                    $vendor = Vendor::model()->findByPk($value->vendor_id);
                  ?>
                  <option value="<?php echo $vendor->id;?>"><?php echo $vendor->organization.'('.$vendor->first_name.' '.$vendor->last_name.')';?></option>
                  <?php } ?>
                </select>
              </div>
          </div>

          <?php } ?>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Continue</button>
        <a type="button" class="btn btn-default" data-dismiss="modal">Close</a>

      </div>
      </form>
    </div>
  </div>
</div>