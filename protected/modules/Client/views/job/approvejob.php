<?php	$this->pageTitle = 'Approve Job';	$client = Client::model()->findByPk(Yii::app()->user->id);?>
<?php $this->renderPartial('_menu',array('model'=>$Job));?>
<div class="tab-content ">
  <div class="tab-pane active" id="one" role="tabpanel">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <br>
        <br>
        <?php if($flag1==true){                    
		$this->renderPartial('approval_history');                
		}else{                
		if($JobWorkflowEmail[0]->job_status=='Pending'){ ?>
        <table class="table m-b-40 without-border">
          <thead class="thead-default">
            <tr>
              <th>S. No</th>
              <th>Person Name</th>
              <th>Department</th>
              <th>Date &amp; Time</th>
              <th class="actions">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td><?php echo $Client->first_name.' '.$Client->last_name; ?></td>
              <td><?php echo $Client->department; ?></td>
              <td><!--Aug 6, 2015 10:30 AM--> Null </td>
              <td class="actions text-center"><?php if($JobWorkflowEmail[0]->email_sent != 0){                                
			  $flag = true;                            
			  }else{                                
			  $flag = false;                            
			  }                           
			   ?>
                <form method="post">
                  <button type="submit" <?php echo $flag==true?'':'disabled' ?>  name="approve" class="text-button">Approve</button>
                  <?php if($flag==true){ ?>
                  <a href="#modal-id" class="text-button" data-toggle="modal" >Rejected</a>
                  <?php } ?>
                </form></td>
            </tr>
          </tbody>
        </table>
        <?php }else if($JobWorkflowEmail[0]->job_status=='Approved' || $JobWorkflowEmail[0]->job_status=='Rejected'){                    $this->renderPartial('approval_history'); ?>
        <?php } } ?>
      </div>
      <br>
    </div>
  </div>
</div>
</div>
</div>
<!-- col -->
</div>
<!-- row -->
</div>
</div>
<!-- row -->
<div class="modal fade" id="modal-id">
  <div class="modal-dialog">
  <form method="POST">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Reject Workflow</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="">Reason for Rejection</label>
          <select name="rejection_dropdown" id="input" class="form-control" required="required">
            <?php  $Setting = Setting::model()->findAll(array('condition'=>'category_id=35'));                        
			foreach($Setting as $SettingKey=>$SettingValue){ ?>
            <option value="<?php echo $SettingValue->title; ?>"><?php echo $SettingValue->title; ?></option>
            <?php  } ?>
          </select>
        </div>
        <div class="form-group">
          <label for="">Note</label>
          <textarea name="reason_details" id="input" class="form-control" rows="3" required="required"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button  type="submit" name="Reason"  class="btn btn-success">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </form>
  </div>
</div>
