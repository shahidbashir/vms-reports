<?php $this->pageTitle = 'Job Template View';
$client = Client::model()->findByPk($model->client_id)
?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title"><?php echo $model->job_title; ?> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobtemplates'); ?>" class="btn btn-sm btn-default-3 pull-right">Back to Template List</a> </h4>
      <p class="m-b-20"></p>
      <hr class="full-hr">
      <div class="simplify-tabs">
        <div role="tabpanel"> 
          
          <!-- Nav tabs -->
          
          <?php $this->renderPartial('template_menu'); ?>
          <?php if(isset($_GET['tab']) && $_GET['tab']=='Requisitions')
                    {
                        $templateData = JobTemplates::model()->findByPk($_GET['template_id']);
                        $clientData = Client::model()->findByPk($templateData->client_id);
                        $criteria=new CDbCriteria();
                        $criteria->select = 'id,title,bill_rate,experience,location';
                        $criteria->condition = 'template_id ='.$_GET['template_id'];
                        $criteria->order = "id desc";

                        $jobRecords = Job::model()->findAll($criteria);
                        $count = Job::model()->count($criteria);
                        ?>
          <div class="tab-content">
            <div class="tab-pane active" id="one" role="tabpanel">
              <p class="m-b-20"> Total number of Job Created using this Template : <?php echo $count; ?> </p>
              <table class="table m-b-40 without-border">
                <thead class="thead-default">
                  <tr>
                    <th>Job Title</th>
                    <th>Bill Rate</th>
                    <th>Experience</th>
                    <th>Location</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($jobRecords as $value){ ?>
                  <tr>
                    <td><?php echo $value->title; ?></td>
                    <td>$ <?php echo $value->bill_rate; ?></td>
                    <td><?php echo $value->experience; ?></td>
                    <td><?php
						$location = unserialize($value->location);
						foreach($location as $locValue){
							$locData = Location::model()->findByPk($locValue);
							echo $locData->name;
						}
						?></td>
                    <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$value->id,array('type'=>'overview')); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php }
			if(isset($_GET['tab']) && $_GET['tab']=='Rate-Card'){
				$templateData = JobTemplates::model()->findByPk($_GET['template_id']);
				$clientData = Client::model()->findByPk($templateData->client_id);
				$criteria=new CDbCriteria();
				$criteria->select = 'id,title,bill_rate,experience,location';
				$criteria->condition = 'template_id ='.$_GET['template_id'];
				$criteria->order = "id desc";

				$jobRecords = Job::model()->findAll($criteria);
				$count = Job::model()->count($criteria);
				?>
          <div class="tab-content">
            <div class="tab-pane active" id="one" role="tabpanel">
              <p class="m-b-20"> Total number of Job Created using this Template : <?php echo $count; ?> </p>
              <table class="table m-b-40 without-border">
                <thead class="thead-default">
                  <tr>
                    <th>Bill Rate </th>
                    <th>Year of Experience</th>
                    <th>Number of Jobs</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
					$templateexp = explode(',',$templateData->temp_experience);
					$temp_min_billrate = explode(',',$templateData->temp_min_billrate);
					$temp_max_billrate = explode(',',$templateData->temp_max_billrate);
					foreach($temp_min_billrate as $key=>$value){
					if($value){
						$criteria=new CDbCriteria();
						$criteria->select = 'id,title,bill_rate,experience,location';
						$criteria->condition = 'template_id ='.$templateData->id.' and bill_rate >='.$value.' and bill_rate <='.$temp_max_billrate[$key];
						$count = Job::model()->count($criteria);
						?>
                  <tr>
                    <td><?php echo '$'.$value.' - $'.$temp_max_billrate[$key]; ?></td>
                    <td><?php echo $templateexp[$key]; ?></td>
                    <td><?php if($count) { echo $count; }else{ echo '0'; } ?></td>
                  </tr>
                  <?php } } ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php }
                    if(isset($_GET['tab']) && $_GET['tab']=='template-view'){ ?>
          <div class="tab-content">
            <div class="tab-pane active" id="one" role="tabpanel">
              <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 job-info-left">
                  <p class="m-b-10 bold">Primary Skills</p>
                  <?php $primery_skills = explode(",",$model->primary_skills);
                                    if($model->primary_skills){
                                    foreach($primery_skills as $skills1){
                                        ?>
                  <span class="label label-info"><?php echo $skills1; ?></span>
                  <?php } } ?>
                  <hr>
                  <p class="m-b-10 m-t-10 bold">Secondary Skills</p>
                  <?php $secondary_skills = explode(",",$model->secondary_skills);
                                    if($model->secondary_skills){
                                    foreach($secondary_skills as $skills2){
                                        ?>
                  <span class="label label-default"><?php echo $skills2; ?></span>
                  <?php } } ?>
                  <hr>
                  <p class="m-b-10 m-t-10 bold">Good to have</p>
                  <?php $good_to_have = explode(",",$model->good_to_have);
                                    if($model->good_to_have){
                                    foreach($good_to_have as $skills3){
                                        ?>
                  <span class="label label-default"><?php echo $skills3; ?></span>
                  <?php } } ?>
                  <hr>
                  <p class="m-b-10 m-t-10 bold">Note for Skills</p>
                  <div class="well">
                    <p><?php echo $model->notes_for_skills; ?></p>
                  </div>
                  <hr>
                  <p class="m-b-10 m-t-10 bold">Job Descriptions</p>
                  <div class="well">
                    <p><?php echo $model->job_description; ?></p>
                  </div>
                  <hr>
                  <p class="m-b-10 m-t-10 bold">You Will</p>
                  <div class="well">
                    <p><?php echo $model->you_will; ?></p>
                  </div>
                  <hr>
                  <p class="m-b-10 m-t-10 bold">Qualification</p>
                  <div class="well">
                    <p><?php echo $model->qualification; ?></p>
                  </div>
                  <hr>
                  <p class="m-b-10 m-t-10 bold">Additional Information</p>
                  <div class="well">
                    <p><?php echo $model->add_info; ?></p>
                  </div>
                </div>
                
                <!-- col -->
                
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                  <div class="job-inf-tables m-t-10">
                    <?php /*?>
					//removing add template link for demo purpose
					<p>Quick Action</p>
                    <p class="m-b-10 m-t-10"> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/updateTemplate',array('template_id'=>$model->id)); ?>" class="btn btn-sm btn-success">Edit &amp; Re-publish</a> </p><?php */?>
                    <table class="table table-condensed table-hover">
                      <tbody>
                        <tr>
                          <td>Category:</td>
                          <td><?php $setting = Setting::model()->findByPk($model->cat_id);
                                                    echo $setting->title; ?></td>
                        </tr>
                        <tr>
                          <td>Experience:</td>
                          <td><?php echo $model->experience;?></td>
                        </tr>
                        <tr>
                          <td>Job Level:</td>
                          <td><?php echo $model->job_level;?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                
                <!-- col --> 
                
                <br>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
      
      <!-- col --> 
      
    </div>
    
    <!-- row --> 
    
  </div>
</div>
