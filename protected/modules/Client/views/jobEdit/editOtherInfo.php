<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php $this->renderPartial('_menu', array('model'=>$model)); ?>
      <div class="job-edit-section add-job">
        <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'job-form',
          'enableAjaxValidation'=>false,
      ));
      ?>
          <div class="well m-t-10 m-b-10">
            <h4> Pre Identity Candidate </h4>
            <br>
            <div class="row">
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                  <label for="">Do you have Pre Identity Candidate</label>
                  <select name="Job[pre_candidate]" id="pre_candidate" class="form-control" required>
                    <option value=""></option>
                        <option value="Yes" <?php echo ($model->pre_candidate== 'Yes')?'selected':''; ?>>Yes</option>
                        <option value="No" <?php echo ($model->pre_candidate== 'No')?'selected':''; ?>>No</option>
                  </select>
                </div>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <label for="">&nbsp;</label>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <input type="text" name="Job[pre_name]" value="<?php if($model->pre_name){ echo $model->pre_name; } ?>" class="form-control" id="pre_name" placeholder="Candidate Name">
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                      <input type="text" name="Job[pre_supplier_name]" value="<?php if($model->pre_supplier_name){ echo $model->pre_supplier_name; } ?>" class="form-control" id="pre_supplier_name" placeholder="Supplier Name">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- row -->
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                  <label for="">Current Rate</label>
                  <input type="text" class="form-control" id="pre_current_rate"  name="Job[pre_current_rate]" value="<?php if($model->pre_current_rate){ echo $model->pre_current_rate; } ?>">
                </div>
              </div>
              <!-- col -->
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                  <label for=""> Payment Type</label>
                  <?php
                      $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                      echo $form->dropDownList($model, 'pre_payment_type', $list , array('id'=>'pre_payment_type','class'=>'form-control select2 pre_fields','empty' => '','required'=>'required')); //,'disabled'=>true ?>
                </div>
              </div>
              <!-- col --> 
            </div>
		<input type="submit" name="editOtherInfo" value="Save To Draft" class="btn btn-sm btn-success">
        <input type="submit" name="publish" value="Publish Job" class="btn btn-sm btn-success">
            <!-- row --> 
          </div>
        <?php $this->endWidget(); ?>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
  </div>

