<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php $this->renderPartial('_menu', array('model'=>$model)); ?>
      <div class="job-edit-section add-job">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'job-form',
                'enableAjaxValidation'=>false,
            )); ?>
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Bill Rate</label>
                <?php echo $form->numberField($model,'bill_rate',array('class'=>'form-control','min'=>'.1','step' => "any",'onChange'=>'calculate("billrate")','required'=>'required')); ?>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Select Pay Rate Type</label>
                <?php
                 $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                 echo $form->dropDownList($model, 'payment_type', $list , array('class'=>'form-control','empty' => '','required'=>'required')/*,'onChange'=>'calculate()'*/);  ?>
              </div>
            </div>
            <!-- col --> 
          </div>
          <!-- row -->
          
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Shift</label>
                <?php
                  $shift = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=15')),'title', 'title');
                   echo $form->dropDownList($model, 'shift', $shift , array('class'=>'form-control','empty' => '','required'=>'required')); ?>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Hours Per Week</label>
                <?php
                  $hours_per_week = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=16')),'title', 'title');
                   echo $form->dropDownList($model, 'hours_per_week', $hours_per_week , array('class'=>'form-control','empty' => '','onChange'=>'calculate()','required'=>'required')); ?>
              </div>
            </div>
            <!-- col --> 
          </div>
          <!-- row -->
          <input type="submit" name="editRates" value="Save To Draft" class="btn btn-sm btn-success">
          
        <?php $this->endWidget(); ?>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
