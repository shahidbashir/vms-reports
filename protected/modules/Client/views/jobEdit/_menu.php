<?php
$this->pageTitle = 'Edit Job';
	if(isset($_GET['id'])){
		$jobID = $_GET['id'];
		}else{
			 $jobID = $_GET['jobid'];
			 }
$Client = Client::model()->findByPk($model->user_id);

      $postingstatus = UtilityManager::jobStatus();
      switch ($postingstatus[$model->jobStatus]) {
        case "Pending Approval":
          $color = 'label-pending-aproval';
          break;
        case "Open":
          $color = 'label-open';
          break;
        case "Filled":
          $color = 'label-filled';
          break;
        case "Rejected":
          $color = 'label-rejected';
          break;
        case "Re-open":
          $color = 'label-re-open';
          break;
        case "Hold":
          $color = 'label-hold';
          break;
        case "New Request":
          $color = 'label-new-request';
          break;
        default:
          $color = 'label-new-request';
      }

      ?>
<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>" class="btn btn-sm btn-default-3 pull-right">Back Job Listing</a> </h4>

<h4 class="m-b-10 page-title"><?php echo $model->title;?> ( <?php echo $model->id;?> ) <i class="icon svg-fire title"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/fire@512px-red.svg"></i> </h4>
<p class="m-b-20"> <span class="tag <?php echo $color; ?>"><?php echo $postingstatus[$model->jobStatus]; ?> </span> Created by: <?php echo $Client->first_name.' '.$Client->last_name; ?> on <?php echo $model->date_created;?></p>
<?php if(Yii::app()->user->hasFlash('success')):?>
	<?php echo Yii::app()->user->getFlash('success'); ?>
<?php endif; ?>
<!--<div class="yellow-box">
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. .</p>
</div>-->
<div class="m-t-20"><!--<input type="submit" name="" value="Publish Job" class="btn btn-sm btn-success">-->  </div>
<hr class="full-hr-30">
<br>
