<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php $this->renderPartial('_menu', array('model'=>$model)); ?>
    <div class="job-edit-section add-job">
      <?php $form=$this->beginWidget('CActiveForm', array(
			  'id'=>'job-form',
			  'enableAjaxValidation'=>false,
		  ));
		  $loginUserId = Yii::app()->user->id;
		  ?>
        
        <!-- well -->
        <div class="well well-info">
          <h4>Internal Reference Section</h4>
          <br>
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Request Department</label>
                <select class="form-control" name="Job[request_dept]">
                    <option value=""></option>
                    <?php
                    foreach($JobrequestDepartment as $JobrequestDepartment){ ?>
                        <option value="<?php echo $JobrequestDepartment->department; ?>" <?php echo ($JobrequestDepartment->department==$model->request_dept)?'selected':''; ?>><?php echo $JobrequestDepartment->job_request; ?></option>
                    <?php  }  ?>
                </select>
              </div>
            </div>
            <!-- col -->
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="">Billcode</label>
                <select class="form-control" name="Job[billing_code]">
                    <option value=""></option>
                    <?php
                    foreach($BillingcodeDepartment as $BillingcodeDepartment){ ?>
                        <option value="<?php echo $BillingcodeDepartment->billingcode; ?>" <?php echo ($BillingcodeDepartment->billingcode==$model->billing_code)?'selected':''; ?>><?php echo $BillingcodeDepartment->billingcode; ?></option>
                    <?php  }  ?>
                </select>
              </div>
            </div>
            <!-- col --> 
          </div>
          <!-- row -->
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <label for="">Rate</label>
              <br>
              <div class="be-checkbox">
                <input id="check1"  name="Job[over_time]" type="hidden"  value="0">
                <input id="check1"  name="Job[over_time]" <?php echo ($model->over_time== 1)?'checked':''; ?> type="checkbox"  value="1">
                <label for="check1">OverTime</label>
              </div>
              <div class="be-checkbox">
              	<input id="check2"  name="Job[double_time]" type="hidden"  value="0">
                <input id="check2" type="checkbox" <?php echo ($model->double_time== 1)?'checked':''; ?> name="Job[double_time]" value="1">
                <label for="check1">Double Time</label>
              </div>
            </div>
            <!-- col --> 
          </div>
          <!-- row --> 
          <br>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label for="">Reference Code / Project Code </label>
                <input type="text" id="pre_reference_code" value="<?php echo $model->pre_reference_code; ?>"  name="Job[pre_reference_code]" class="form-control">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label for="">Total Estimate Code </label>
                <input type="text" id="pre_total_estimate_code" value="<?php echo $model->pre_total_estimate_code; ?>"  name="Job[pre_total_estimate_code]" class="form-control" required="required" readonly="readonly">
              </div>
            </div>
            <!-- col-12 --> 
          </div>
          <!-- row -->
          
          <div class="row">
          
          
          
          	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<div class="form-group">
						<label for="">Job Work flow approval.</label>
						<select name="Job[work_flow]" class="form-control" readonly="readonly">
							<option value='<?php echo $workflow->id; ?>'><?php echo $workflow->flow_name; ?></option>
						</select>
					</div>

					<p class="m-t-10 m-b-10">Following are the Member for Job Approval</p>

					<table class="table table-striped table-hover table-white">
						<thead>
						<tr>
							<th>Approval</th>
							<th>Name</th>
							<th>Department</th>
							<th>Work Flow Order</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($workflowmember as $val ){
							$clientdata = Client::model()->findByPk($val->client_id); ?>
							<tr>
								<td><?php echo $workflow->flow_name; ?></td>
								<td><?php echo $clientdata->first_name . ' ' . $clientdata->last_name; ?></td>
								<td><?php echo $val->department_id; ?></td>
								<td><?php echo $val->order_approve; ?></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="">Status of Job</label>
                <select name="Job[jobStatus]" class="form-control">
					<?php $jobStatus = UtilityManager::jobStatus();
                    foreach($jobStatus as $key=>$jobStatus){ ?>
                        <option value='<?php echo $key; ?>' <?php echo ($key==$model->jobStatus)?'selected':''; ?>><?php echo $jobStatus; ?></option>
                    <?php } ?>
                </select>
              </div>
            </div>
            <!-- col --> 
          </div>
          <!-- row --> 
          <input type="submit" name="internalDetails" value="Save To Draft" class="btn btn-sm btn-success">
          <input type="submit" name="publish" value="Publish Job" class="btn btn-sm btn-success">
        </div>
        <!-- well -->
        
      <?php $this->endWidget(); ?>
    </div>
  </div>
  <!-- col --> 
  
</div>
<!-- row -->
<div class="seprater-bottom-100"></div>
</div>
