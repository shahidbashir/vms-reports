<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php $this->renderPartial('_menu', array('model'=>$model)); ?>
      <div class="job-edit-section add-job">
        <?php $form=$this->beginWidget('CActiveForm', array(
				  'id'=>'job-form',
				  'enableAjaxValidation'=>false,
			  ));
        $loginUser = Yii::app()->user->id;
        $loginUserId = UtilityManager::superClient($loginUser);
			  ?>
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Job Title</label>
              <input type="text" name="Job[title]" value="<?php echo $model->title; ?>" class="form-control" title="Job title must be letter" required="required">
            </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Job Type</label>
              <?php
				$list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=11')),'title', 'title');
				echo $form->dropDownList($model, 'type', $list , array('class'=>'form-control select2','empty' => '','required'=>'required'));
				?>
            </div>
          </div>
        </div>
        <!-- row -->
        
        <div class="row">
          <?php /*?><div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
              <label for="">Category</label>
              <?php
				$list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'title', 'title');
				echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'form-control select2','empty' => '','required'=>'required','id'=>'Job_cat_id1'));
				 ?>
            </div>
          </div><?php */?>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Experiences</label>
              <?php
				$list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=10')),'title', 'title');
				echo $form->dropDownList($model, 'experience', $list , array('class'=>'form-control select2','empty' => '','required'=>'required'));
				?>
            </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Job Level</label>
              <?php
				$list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=40')),'title', 'title');
				echo $form->dropDownList($model, 'job_level', $list , array('class'=>'form-control select2','empty' => '','required'=>'required')); 
				?>
            </div>
          </div>
        </div>
        <!-- row -->        
        
        <div class="search-box-only-border m-b-20">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="panel-group" id="accordion">
                <div class="panel panel-info">
                  <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed"> Job Description</a> </h4>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                      <textarea name="job_description" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->description; ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-info">
                  <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false"> You Will</a> </h4>
                  </div>
                  <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                      <textarea name="you_will" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->you_will; ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-info">
                  <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false"> Qualifications</a> </h4>
                  </div>
                  <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                      <textarea name="qualifications" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->qualification; ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-info m-b-0">
                  <div class="panel-heading" style="margin: 0; padding-left: 10px; padding:8px;">
                    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed" aria-expanded="false"> Additional Information</a> </h4>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                      <textarea name="additional_information" class="textarea wysihtml5 form-control" placeholder="Enter text ..." style="width: 100%; min-height: 250px"><?php echo $model->add_info; ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- col-12 --> 
          </div>
          <!-- row --> 
        </div>
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <label for="">Location</label>
            <div class="form-group">
              <select  multiple="" class="select2" name="Job[location][]" required >
                <option value=""></option>
                <?php
                  $location = Location::model()->findAllByAttributes(array('reference_id'=>$loginUserId));
				  
                  foreach($location as $location){ ?>
                <option value="<?php echo $location->id; ?>" <?php echo in_array($location->id,unserialize($model->location))?'selected':''; ?> ><?php echo $location->name; ?></option>
                <?php  } 	?>
              </select>
            </div>
          </div>
          <!-- col -->
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
              <label for="">Number of Openings</label>
              <?php echo $form->numberField($model,'num_openings',array('class'=>'form-control','min'=>'0')); ?> </div>
          </div>
          <!-- col --> 
        </div>
        <!-- row --> 
        
        <br>
        <?php /*?><div class="row">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
              <label for="">Job Start Date</label>
              <input type="text" class="form-control" name="job_po_duration" id="jobStartDate" value="<?php echo date('m/d/Y',strtotime($model->job_po_duration)) ?>"  required="required">
            </div>
          </div>
          <!-- col -->
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
              <label for="">Job End Date</label>
              <input type="text" class="form-control" name="job_po_duration_endDate" id="jobEndDate" value="<?php echo date('m/d/Y',strtotime($model->job_po_duration_endDate)) ?>"  required="required">
            </div>
          </div>
          <!-- col -->
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
              <label for="">Closing Date for Job</label>
              <input type="text" class="form-control" name="desired_start_date" id="jobClosingDate" value="<?php echo date('m/d/Y',strtotime($model->desired_start_date)) ?>" required="required">
            </div>
          </div>
          <!-- col --> 
        </div><?php */?>
        <!-- row -->
        
        <hr>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label for="">Invite your Team member to Collaborate</label>
            <div class="form-group">
              <select multiple="" class="select2" name="Job[invite_team_member][]" >
                <option value=""></option>
                <?php $team = unserialize($model->invite_team_member);
                $friend = Client::model()->findAllByAttributes(array('super_client_id'=>$loginUserId));
                if($team){
                  foreach($friend as $friend){
                    foreach ($team as $value){
                      ?>
                      <option value="<?php echo $friend->id; ?>" <?php if($value==$friend->id){ echo 'Selected'; } ?>><?php echo $friend->first_name.' ('.$friend->email.')'; ?></option>
                    <?php } } }else{
                  foreach($friend as $friend){ ?>
                    <option value="<?php echo $friend->id; ?>"><?php echo $friend->first_name.' ('.$friend->email.')'; ?></option>
                    <?php
                  }	} ?>
              </select>
            </div>
          </div>
          <!-- col --> 
        </div>
        <!-- row -->
        <div class="row">
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="">Labor Category</label>
              <?php
				$labour_cat = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=14')),'title', 'title');
				echo $form->dropDownList($model, 'labour_cat', $labour_cat , array('class'=>'form-control select2','empty' => '')); 
				?>
            </div>
          </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="">Number of Submission</label>
              <?php echo $form->numberField($model,'number_submission',array('class'=>'form-control','min' => 1)); ?> </div>
          </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="">Background Verification</label>
              <?php
				$list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=41')),'title', 'title');
				echo $form->dropDownList($model, 'background_verification', $list , array('class'=>'form-control','empty' => '','required'=>"required")); 
				?>
            </div>
          </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="">Type of Job</label>
              <select name="Job[type_of_job]" id="input" class="form-control" required="required">
                <option value=""></option>
                <option selected="<?php echo ($model->type_of_job== 'Non Exempt')?'selected':''; ?>" value="Non Exempt">Non Exempt</option>
                <option selected="<?php echo ($model->type_of_job== 'Non Exempt')?'selected':''; ?>" value="Exempt">Exempt</option>
              </select>
            </div>
          </div>
        </div>
        
        
        
         <?php /*?><br /><br /><br />
        
        <input type="text" name="Job[vendor_client_markup]" id="vendor_client_markup" class="form-control" value="<?php //echo $model->markup; ?>">
        <input type="text" id="Job_bill_rate" name="" value="<?php echo $model->bill_rate ?>" class="form-control"  />
        <input type="text" id="Job_pay_rate" name="" value="<?php echo $model->pay_rate ?>" class="form-control"  />
        <input type="text" id="hours_per_week" name="" value="<?php echo $model->hours_per_week ?>" class="form-control"  />
        <input type="text" id="pre_total_estimate_code"  name="Job[pre_total_estimate_code]" class="form-control" required="required" readonly="readonly">
        <input type="text" id="workflow_selected" name="Job[work_flow]" class="form-control"  />
        <br /><br /><br /><?php */?>
        
        
        <input type="submit" name="editdetails" value="Save To Draft" class="btn btn-sm btn-success">
        <input type="submit" name="publish" value="Publish Job" class="btn btn-sm btn-success">
        <!-- row --> 
        <br>
        <?php $this->endWidget(); ?>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>

<?php /*?>
<script>
jQuery(document).ready(function($) {
	//on change of category it will select highest markup of vendors of that category.
	$('#Job_cat_id1').on('change',function(){
		//alert('first');	
		var cat_value = $(this).val();
		$.ajax({
		'url':'<?php echo $this->createUrl('jobEdit/vendorMarkup') ?>',
		type: "POST",
		data: { cat_value: cat_value},
			'success':function(html){
				$("#vendor_client_markup").val(html);
				var startDate = $("#jobStartDate").val();
				var jobEndDate = $("#jobEndDate").val();
				var Job_bill_rate = $("#Job_bill_rate").val();
				var Job_pay_rate = $("#Job_pay_rate").val();
				var markup = html;
				var cat_id = cat_value;
								
				var Job_hours_per_week = $("#hours_per_week").val();
				
				var numOfDates = getBusinessDatesCount(new Date(startDate),new Date(jobEndDate));
				
				if(numOfDates==0){
					var numOfDates = 1;
				}
				var str = Job_hours_per_week;
				var numberHours = str.split(" ",1);
		
				var hoursPerDay = numberHours/5;
				
				var pay_rate = Job_bill_rate * 100/( 100 + parseFloat(markup));
				
				var estimate_cost = Job_bill_rate * numOfDates * hoursPerDay;
				
				console.log(Job_bill_rate,numOfDates,hoursPerDay,pay_rate,estimate_cost);
				
				document.getElementById("pre_total_estimate_code").value = Job_bill_rate * numOfDates * hoursPerDay;
				workflowConfiguration(estimate_cost , cat_id);
			}
		});
	});
	
	var cat_value = $("option:selected","#Job_cat_id1").val();
	$.ajax({
	'url':'<?php echo $this->createUrl('jobEdit/vendorMarkup') ?>',
	type: "POST",
	data: { cat_value: cat_value},
		'success':function(html){
			$("#vendor_client_markup").val(html)
		}
	});
});
</script>
<script type="text/javascript">
    //calculating rates of per hour job.
    
	jQuery(document).ready(function($) {
		setTimeout(function() {
		  $('#jobStartDate,#jobEndDate').on('change',function(){
			  //alert('second');	
				var startDate = $("#jobStartDate").val();
				var jobEndDate = $("#jobEndDate").val();
				var Job_bill_rate = $("#Job_bill_rate").val();
				var Job_pay_rate = $("#Job_pay_rate").val();
				var markup = $("#vendor_client_markup").val();
				var cat_id = $("#Job_cat_id1").val();
				
				var Job_hours_per_week = $("#hours_per_week").val();
				
				var numOfDates = getBusinessDatesCount(new Date(startDate),new Date(jobEndDate));
				
				if(numOfDates==0){
					var numOfDates = 1;
				}
		
				var str = Job_hours_per_week;
				var numberHours = str.split(" ",1);
		
				var hoursPerDay = numberHours/5;
				
				var pay_rate = Job_bill_rate * 100/( 100 + parseFloat(markup));
				
				var estimate_cost = Job_bill_rate * numOfDates * hoursPerDay;
				document.getElementById("pre_total_estimate_code").value = Job_bill_rate * numOfDates * hoursPerDay;
				workflowConfiguration(estimate_cost , cat_id);
				
		  });
	   }, 700);
	  });

    function workflowConfiguration(estimate_cost , cat_id){
        $.ajax({
            'url':'<?php echo $this->createUrl('jobEdit/getWorkFlowID') ?>',
            type: "POST",
            data: { estimate_cost: estimate_cost ,cat_id: cat_id},
            'success':function(html){
                $("#workflow_selected").val(html);
            }
        });
    }


    //testing this one
    function getBusinessDatesCount(startDate, endDate) {
        var count = 0;
        var curDate = startDate;
        while (curDate <= endDate) {
            var dayOfWeek = curDate.getDay();
            if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
                count++;
            curDate.setDate(curDate.getDate() + 1);
        }
        return count;
    }
</script>
<?php */?>
 
 
