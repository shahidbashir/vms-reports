<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php $this->renderPartial('_menu', array('model'=>$model)); ?>
      <div class="job-edit-section add-job">
        <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'job-form',
          'enableAjaxValidation'=>false,
      ));
      $loginUserId = Yii::app()->user->id;
      ?>
        <div class="hirring-pipline">
          <div class="row">
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                <label for="">Primary Skill</label>
                <select class="tokenize-remote-demo1" name="Job[skills][]" id="job_skills" multiple>
                  <?php
                    $primary_skills = explode(',',str_replace(' ','',$model->skills)); 
                    if($primary_skills){
                    foreach($primary_skills as $pValue){
						if($pValue)
                        echo '<option selected="selected" value="'.$pValue.'">'.$pValue.'</option>';
                        }
                    }
                     ?>
                </select>
              </div>
              <div class="form-group">
                <label for="">Secondary Skill</label>
                <select class="tokenize-remote-demo1" name="Job[skills1][]" id="job_skills1" multiple>
                  <?php
                    $secondary_skills = explode(',',str_replace(' ','',$model->skills1)); 
                    if($secondary_skills){
                    foreach($secondary_skills as $sValue){
						if($sValue)
                        echo '<option selected="selected" value="'.$sValue.'">'.$sValue.'</option>';
                        }
                    }
                     ?>
                </select>
              </div>
              <div class="form-group">
                <label for="">Good to have.</label>
                <select class="tokenize-remote-demo1" name="Job[skills2][]" id="job_skills2" multiple>
                  <?php
                    $good_to_have = explode(',',str_replace(' ','',$model->skills2)); 
                    if($good_to_have){
                    foreach($good_to_have as $gValue){
						if($gValue)
                        echo '<option selected="selected" value="'.$gValue.'">'.$gValue.'</option>';
                        }
                    }
                     ?>
                </select>
              </div>
            </div>
            <!-- col-12 --> 
          </div>
          <!-- row -->
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="">Note for Skills</label>
                <textarea name="Job[skills_notes]" id="input" class="form-control" rows="5" required="required"><?php echo $model->skills_notes; ?></textarea>
              </div>
            </div>
          </div>
          <!-- row -->
          <input type="submit" name="editSkills" value="Save To Draft" class="btn btn-sm btn-success">
          <input type="submit" name="publish" value="Publish Job" class="btn btn-sm btn-success">
        </div>
        <br>
        <?php $this->endWidget(); ?>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<style>
.tokenize-dropdown > .dropdown-menu li > a  {
    display: block !important;
}
</style>
