<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/favicon.png">
<title></title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/semantic-ui-dropdown/dropdown.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/semantic-ui-icon/icon.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/semantic-ui-image/image.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/semantic-ui-label/label.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/semantic-ui-transition/transition.css">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap3-wysihtml5-master/bootstrap-wysihtml5.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-datetimepicker-2.3.8/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/assets/css/style.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/assets/css/style.css">
<!-- Token css -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-tokenfield/dist/css/tokenfield-typeahead.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/jquery-ui-1.12.0/jquery-ui.css">
<?php /*?>    <!-- jQuery UI CSS -->
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
    <!-- Tokenfield CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/token-assets/dist/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet"><?php */?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body class="content-wrapper">
<?php
	$loginUserId = Yii::app()->user->id;
	$Client = Client::model()->findByPk($loginUserId);
	$clientModel = Client::model()->findByPk(Yii::app()->user->id);
	?>
    
    <?php
	if(!empty($Client->profile_image)) {
	  $images_path = Yii::app()->baseUrl.'/images/profile_img/'.$Client->profile_image;
		}else{
			$images_path = Yii::app()->request->baseUrl.'/theme-assets/plugins/images/users/varun.jpg';
			}
	  ?>
<div id="wrapper"> 
  
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
      <div class="top-left-part"><a class="logo" href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>"><b><img src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/eliteadmin-logo.png" alt="home" /></b><span class="hidden-xs"><img src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/eliteadmin-text.png" alt="home" /></span></a></div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="ti-menu"></i></a></li>
        <li>
          <form role="search" class="app-search hidden-xs">
            <input type="text" placeholder="Search..." class="form-control">
            <a href=""><i class="fa fa-search"></i></a>
          </form>
        </li>
      </ul>
      <ul class="nav navbar-top-links navbar-right pull-right">
        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
          <?php /*?>
		  <ul class="dropdown-menu mailbox animated bounceInDown">
            
			<li>
              <div class="drop-title">You have 4 new messages</div>
            </li>
            <li>
              <div class="message-center"> <a href="#">
                <div class="user-img"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Pavan kumar</h5>
                  <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                </a> <a href="#">
                <div class="user-img"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Sonu Nigam</h5>
                  <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                </a> <a href="#">
                <div class="user-img"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Arijit Sinh</h5>
                  <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                </a> <a href="#">
                <div class="user-img"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Pavan kumar</h5>
                  <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                </a> </div>
            </li>
            <li> <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a></li>
			
          </ul><?php */?>
          <!-- /.dropdown-messages --> 
        </li>
        <!-- /.dropdown -->
        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
          
        </li>
        <!-- /.dropdown -->
        <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="<?php echo $images_path;?>" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $Client->first_name.' '.$Client->last_name; ?></b> </a>
          <ul class="dropdown-menu dropdown-user animated flipInY">
          <?php if($Client->profile_approve == 'Yes'){ ?>
            <!--<li><a href="#"><i class="ti-user"></i> My Profile</a></li>
            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
            <li role="separator" class="divider"></li>-->
           <?php } ?>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/logout'); ?>"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
          <!-- /.dropdown-user --> 
        </li>
        <li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
        <!-- /.dropdown -->
      </ul>
    </div>
    <!-- /.navbar-header --> 
    <!-- /.navbar-top-links --> 
    <!-- /.navbar-static-side --> 
  </nav>
  <!-- Left navbar-header -->
  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
    
      <ul class="nav" id="side-menu">
        <li class="sidebar-search hidden-sm hidden-md hidden-lg"> 
          <!-- input-group -->
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
          <!-- /input-group --> 
        </li>
        <li class="user-pro"> <a href="#" class="waves-effect"><img src="<?php echo $images_path; ?>" alt="user-img"  class="img-circle"> <span class="hide-menu"> <?php echo $Client->first_name.' '.$Client->last_name; ?><span class="fa arrow"></span></span></a>
        <?php if($Client->profile_approve == 'Yes'){ ?>
          <!--<ul class="nav nav-second-level">
            <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
            <li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Balance</a></li>
            <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
            <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>-->
        <?php } ?>
        </li>
        <li class="nav-small-cap m-t-10">--- Main Menu</li>
        <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><span class="hide-menu" >Dashboard</span></a> </li>
        
        <?php if($Client->profile_approve == 'Yes'){ ?>
        <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="F" class="linea-icon linea-software fa-fw"></i> <span class="hide-menu">Jobs<span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>">Jobs</a> </li>
            <?php if($Client->super_client_id==0){  ?>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/create'); ?>">Add New Jobs</a> </li>
            <?php } ?>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting1',array('tab'=>'pending')); ?>">Jobs for Approval</a> </li>
            <?php if($Client->super_client_id==0){  ?>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Create Work Order</a> </li>
            <?php } ?>
          </ul>
        </li>
        <li> <a href="map-vector.html" class="waves-effect"><i data-icon="S" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu" > Worker</span></a> </li>
        
        <li> <a href="map-vector.html" class="waves-effect"><i class="linea-icon linea-basic fa fa-calendar"></i><span class="hide-menu"> Interview Schedule</span></a>
          <ul class="nav nav-second-level collapse">
            <li><a href="interviews.html">Interview Schedule</a></li>
          </ul>
        </li>
        
        
        <li> <a href="#" class="waves-effect"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Time Sheet<span class="fa arrow"></span> </span></a>
          <ul class="nav nav-second-level">
            <li><a href="panels-wells.html">TimeSheet</a></li>
            <li><a href="buttons.html">Waiting for Approval</a></li>
            <li><a href="sweatalert.html">Create Time Sheet</a></li>
          </ul>
        </li>
        
        
        
        <li> <a href="#" class="waves-effect"><i class="linea-icon linea-basic fa-fw icon-people"></i> <span class="hide-menu">Candidate<span class="fa arrow"></span> </span></a>
          <ul class="nav nav-second-level collapse">
            <li><a href="#">Candidate</a></li>
          </ul>
        </li>
        
        <li> <a href="#" class="waves-effect"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Settings<span class="fa arrow"></span> </span></a>
          <ul class="nav nav-second-level">
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/editProfile'); ?>">Update Profile</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/changPassword'); ?>">Change Password </a></li>
            <?php if($Client->super_client_id==0){  ?>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/index'); ?>">Company Profile</a></li>
            <li><a href="sweatalert.html">Team Member</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/createlocation'); ?>">Location</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/workflows'); ?>">Workflow</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createjobrequest'); ?>">Job Request Type</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createbillingcode'); ?>">Operating Billing Code</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createdepartment'); ?>">Departments</a></li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/settingExhibit'); ?>">Setting Exhibit</a></li>
            <?php } ?>
          </ul>
        </li>
        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/default/logout'); ?>" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
		<?php }else{ ?>
        
        <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="F" class="linea-icon linea-software fa-fw"></i> <span class="hide-menu">Jobs<span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level">
            <li> <a href="#">Jobs</a> </li>
            <li> <a href="javascript:void(0)">Add New Jobs</a> </li>
            <li> <a href="javascript:void(0)">Jobs for Approval</a> </li>
          </ul>
        </li>
        <li> <a href="#" class="waves-effect"><i data-icon="S" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu" > Worker</span></a> </li>
        <li> <a href="#" class="waves-effect"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Time Sheet<span class="fa arrow"></span> </span></a>
          <ul class="nav nav-second-level">
            <li><a href="#">TimeSheet</a></li>
            <li> <a href="#" class="waves-effect"><i class="linea-icon linea-basic fa-fw icon-people"></i> <span class="hide-menu">Candidate<span class="fa arrow"></span> </span></a>
            </li>
            <li><a href="#">Waiting for Approval</a></li>
            <li><a href="#">Create Time Sheet</a></li>
          </ul>
        </li>
        <li> <a href="#" class="waves-effect"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Settings<span class="fa arrow"></span> </span></a>
          <ul class="nav nav-second-level">
            <li><a href="#">Update Profile</a></li>
            <li><a href="#">Change Password </a></li>
            <li><a href="#">Company Profile</a></li>
            <li><a href="#">Team Member</a></li>
            <li><a href="#">Location</a></li>
            <li><a href="#">Workflow</a></li>
            <?php if($Client->super_client_id==0){  ?>
            <li><a href="#">Setting Exhibit</a></li>
            <?php } ?>
          </ul>
        </li>
        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/logout'); ?>" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
        
        <?php } ?>
      </ul>
    
    </div>
  </div>
  <!-- Left navbar-header end --> 
  <!-- Page Content -->
  
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title"><?php echo $this->pageTitle; ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>">Dashboard</a></li>
            <!--<li><a href="#">Ui Elements</a></li>
            <li class="active">Nestable</li>-->
          </ol>
        </div>
        <!-- /.col-lg-12 --> 
      </div>
      <!-- row -->
      
      <!--<div class="row">
        <div class="col-md-12">
          <div class="white-box">-->
          
          	<?php echo $content; ?>
            
          <!--</div>
        </div>
      </div>-->
    </div>
    <!-- container -->
    </div></div></div></div>
    
    <footer class="footer text-center"> 2016 © Elite Admin brought to you by themedesigner.in </footer>
  </div>
  <!-- page-wrapper --> 
  
</div>
<!-- /#wrapper --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/jquery/dist/jquery.min.js"></script> 
<!-- jQuery -->
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/bootstrap/dist/js/bootstrap.min.js"></script> 
<!-- Menu Plugin JavaScript --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script> 
<!--slimscroll JavaScript --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/js/jquery.slimscroll.js"></script> 
<!--Wave Effects --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/js/waves.js"></script> 
<!-- Sparkline charts --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script> 
<!-- EASY PIE CHART JS --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/jquery.easy-pie-chart/easy-pie-chart.init.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script> 
<!-- Custom Theme JavaScript --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/js/custom.js"></script> 
<?php /*?><script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/js/dashboard2.js"></script><?php */?> 
<!--Style Switcher --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/semantic-ui-transition/transition.min.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/semantic-ui-dropdown/dropdown.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap3-wysihtml5-master/wysihtml5-0.3.0.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap3-wysihtml5-master/bootstrap3-wysihtml5.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-daterangepicker/moment.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-datetimepicker-2.3.8/js/bootstrap-datetimepicker.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/assets/js/main.js"></script>
<script type="text/javascript">
$(function() {	
	var dateToday = new Date();
    $('input[name="job_po_duration"]').daterangepicker({minDate: dateToday,});
});
</script>
<script type="text/javascript">
$(function() {
	var dateToday = new Date();
    $('input[name="desired_start_date"]').daterangepicker({
        minDate: dateToday,
        singleDatePicker: true,
        showDropdowns: true,
    }, 
    function(start, end, label) {
        /*var years = moment().diff(start, 'years');
        alert("You are " + years + " years old.");*/
    });
});
</script>
<?php if($this->action->id != 'scheduleInterview') {?>
<script>
	$( document ).ready(function() {
		<?php if($this->action->id == 'create'){ ?>
		document.getElementById("pre_name").disabled = true;
		document.getElementById("pre_supplier_name").disabled = true;
		document.getElementById("pre_current_rate").disabled = true;
		$(".pre_fields").addClass("disabled");
		
		<?php }else{ ?>
		if(document.getElementById("pre_candidate").value == 'No'){
			document.getElementById("pre_name").disabled = true;
			document.getElementById("pre_supplier_name").disabled = true;
			document.getElementById("pre_current_rate").disabled = true;
			$(".pre_fields").addClass("disabled");
			}
		<?php } ?>
		
		$('#pre_candidate').on('change',function(){
		var pre_candidate = $(this).val();
		if(pre_candidate == 'Yes'){
			document.getElementById("pre_name").disabled = false;
			document.getElementById("pre_supplier_name").disabled = false;
			document.getElementById("pre_current_rate").disabled = false;
			$(".pre_fields").removeClass("disabled");
			}else{
				 document.getElementById("pre_name").disabled = true;
				 document.getElementById("pre_supplier_name").disabled = true;
				 document.getElementById("pre_current_rate").disabled = true;
				 $(".pre_fields").addClass("disabled");
				 }
		});
		
	});
</script>
<?php } ?>
<!-- Token Js -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-tokenfield/dist/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bootstrap-tokenfield/dist/bootstrap-tokenfield.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/jquery-ui-1.12.0/jquery-ui.js"></script>
<!-- Token Js end -->
<?php	 
	$skill = Skills::model()->findAll();
	$skills = array();
	//$skills1 = array();
	foreach($skill as $skill){
		$skills[] = "'$skill->name'";
		//$skills1[] = "'$skill->name'";
		}
 ?>
 
<script>
$('#job_skills').tokenfield({
    autocomplete: {
      //source: ['red','blue','green','yellow','violet','brown','purple','black','white'],
	  source: [<?php echo implode(",", $skills); ?>],
      delay: 100
    },
    showAutocompleteOnFocus: true,
	delimiter: [',','-', '_']
  });
  </script>
  <script>
 $('#job_skills1').tokenfield({
    autocomplete: {
	  source: [<?php echo implode(",", $skills); ?>],
      delay: 100
    },
    showAutocompleteOnFocus: true,
	delimiter: [',','-', '_']
  });
  </script>
  <script>
  $('#job_skills2').tokenfield({
    autocomplete: {
	  source: [<?php echo implode(",", $skills); ?>],
      delay: 100
    },
    showAutocompleteOnFocus: true,
	delimiter: [',','-', '_']
  });
  
  $('#interview_guests').tokenfield({
    autocomplete: {
      delay: 100
    },
    showAutocompleteOnFocus: true,
	delimiter: [',','-', '_']
  });
  
  
</script>
<script>
	$( document ).ready(function() {
		$('#Job_cat_id').on('change',function(){
			var cat_value = $(this).val();
			$.ajax({
				'url':'<?php echo $this->createUrl('job/payrates') ?>',
				type: "POST",
				data: { cat_value: cat_value},
				'success':function(html){
					//alert(html);
					$("#mark_up_percentage_val").val(html)
				}
			});
		});
	});
</script>
</body>
</html>
