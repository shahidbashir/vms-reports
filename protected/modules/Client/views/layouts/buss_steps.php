<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/logo-fav.png">
    <title>Beagle</title>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt 
    IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/css/style.css" type="text/css"/>
 <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
</head>
<body>

<?php
$loginUserId = Yii::app()->user->id;
$Client = Client::model()->findByPk($loginUserId);
$clientModel = Client::model()->findByPk(Yii::app()->user->id);
?>
<?php
if(!empty($Client->profile_image)) {
  $images_path = Yii::app()->baseUrl.'/images/profile_img/'.$Client->profile_image;
	}else{
		$images_path = Yii::app()->request->baseUrl.'/new-theme-assets/assets/img/avatar.png';
		}
  ?>
  <div class="be-wrapper">
      <nav class="navbar navbar-default navbar-fixed-top be-top-header">
        <div class="container-fluid">
          <div class="navbar-header"><a href="index.html" class="navbar-brand"></a></div>
          <div class="be-right-navbar">
            <ul class="nav navbar-nav navbar-right be-user-nav">
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar.png"><span class="user-name">Túpac Amaru</span></a>
                <ul role="menu" class="dropdown-menu">
                  <li>
                    <div class="user-info">
                      <div class="user-name"><?php echo $Client->first_name.' '.$Client->last_name; ?></div>
                      <div class="user-position online">Available</div>
                    </div>
                  </li>
                  <li><a href="#"><span class="icon mdi mdi-face"></span> Account</a></li>
                  <li><a href="#"><span class="icon mdi mdi-settings"></span> Settings</a></li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/logout'); ?>"><span class="icon mdi mdi-power"></span> Logout</a></li>
                </ul>
              </li>
            </ul>
            
            <ul class="nav navbar-nav navbar-right be-icons-nav">
              <li class="dropdown"><a href="#" role="button" aria-expanded="false" class="be-toggle-right-sidebar"><span class="icon mdi mdi-settings"></span></a></li>
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-notifications"></span><span class="indicator"></span></a>
                <ul class="dropdown-menu be-notifications">
                  <li>
                    <div class="title">Notifications<span class="badge">3</span></div>
                    <div class="list">
                      <div class="be-scroller">
                        <div class="content">
                          <ul>
                            <li class="notification notification-unread"><a href="#">
                                <div class="image"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar2.png"></div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">Jessica Caruso</span> accepted your invitation to join the team.</div><span class="date">2 min ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar3.png"></div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">Joel King</span> is now following you</div><span class="date">2 days ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar4.png"></div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">John Doe</span> is watching your main repository</div><span class="date">2 days ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar5.png"></div>
                                <div class="notification-info"><span class="text"><span class="user-name">Emily Carter</span> is now following you</span><span class="date">5 days ago</span></div></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="footer"> <a href="#">View all notifications</a></div>
                  </li>
                </ul>
              </li>
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-apps"></span></a>
                <ul class="dropdown-menu be-connections">
                  <li>
                    <div class="list">
                      <div class="content">
                        <div class="row">
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/github.png"><span>GitHub</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/bitbucket.png"><span>Bitbucket</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/slack.png"><span>Slack</span></a></div>
                        </div>
                        <div class="row">
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/dribbble.png"><span>Dribbble</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/mail_chimp.png"><span>Mail Chimp</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/dropbox.png"><span>Dropbox</span></a></div>
                        </div>
                      </div>
                    </div>
                    <div class="footer"> <a href="#">More</a></div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      
    <div class="be-content" style="margin-left: 0; ">
  <?php $loginUserId = Yii::app()->user->id;
  		$Client = Client::model()->findByPk($loginUserId);?>
  <?php echo $content; ?> </div>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/js/main.js" type="text/javascript"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
          <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
         <script type="text/javascript">
           $(document).ready(function(){
            //initialize the javascript
            App.init();
           });
     
           $('.datetimepicker').datetimepicker(); 
           
         </script>
</body>
</html>