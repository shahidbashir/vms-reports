<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/logo-fav.png">
    <title>Beagle</title>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    

    <![endif]-->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/font-awesome/css/font-awesome.css" type="text/css"/>

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/css/style.css" type="text/css"/>

  </head>
  <body class="be-splash-screen">
    <div class="be-wrapper be-login">
      <div class="be-content">
        <?php echo $content; ?>
      </div>
    </div>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/js/main.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
      });
      
    </script>
  </body>
</html>

<style type="text/css">
  .workflow-approval i{
    
      font-size: 150px; 
     
    
  }

  .success {
       color: green;
  }

  .failed {
       color: red;
  }


  .m-b-50 {
    margin-bottom: 50px; 
  }

</style>