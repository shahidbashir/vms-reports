<?php
/*$ipInfo = file_get_contents('http://ip-api.com/json');
$ipInfo = json_decode($ipInfo);
date_default_timezone_set($ipInfo->timezone);*/
$loginUserId = Yii::app()->user->id;
$Client = Client::model()->findByPk($loginUserId);
$clientModel = Client::model()->findByPk(Yii::app()->user->id);
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content=""/>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1"/>
<meta name="msapplication-tap-highlight" content="no">
<meta name="mobile-web-app-capable" content="yes">
<meta name="application-name" content="Milestone">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="Milestone">
<meta name="theme-color" content="#4C7FF0">
<title>Simplify</title>
<?php
  define('CDN', false);
  if(CDN) { ?>
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/bower-jvectormap/jquery-jvectormap-1.2.2.css"/>
<!-- end page stylesheets -->
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/jquery.tagsinput/src/jquery.tagsinput.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/intl-tel-input/build/css/intlTelInput.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/jquery-labelauty/source/jquery-labelauty.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/multiselect/css/multi-select.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/ui-select/dist/select.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/select2/select2.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/selectize/dist/css/selectize.css">
<!-- end page stylesheets -->
<!-- build:css({.tmp,app}) styles/app.min.css -->
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/bootstrap/dist/css/bootstrap.css"/>
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/pace/themes/blue/pace-theme-minimal.css"/>
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/font-awesome/css/font-awesome.css"/>
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/animate.css/animate.css"/>
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/styles/app.css" id="load_styles_before"/>
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/styles/app.skins.css"/>
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/jquery-ui-1.12.0/jquery-ui.css">
<link rel="stylesheet" href="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/bootstrap3-wysihtml5-master/bootstrap-wysihtml5.css">
<link href="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/tokenize/tokenize2.css" rel="stylesheet" />
<link href="http://1288083182.rsc.cdn77.org/new-theme/assets/css/style.min.css" rel="stylesheet">
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/jquery/dist/jquery.js"></script>
<?php }else{ ?>
<!-- page stylesheets -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bower-jvectormap/jquery-jvectormap-1.2.2.css"/>
<!-- end page stylesheets -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/jquery.tagsinput/src/jquery.tagsinput.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/intl-tel-input/build/css/intlTelInput.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/jquery-labelauty/source/jquery-labelauty.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/multiselect/css/multi-select.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/ui-select/dist/select.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/select2/select2.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/selectize/dist/css/selectize.css">
<!-- end page stylesheets -->
<!-- build:css({.tmp,app}) styles/app.min.css -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap/dist/css/bootstrap.css"/>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/pace/themes/blue/pace-theme-minimal.css"/>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/font-awesome/css/font-awesome.css"/>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/animate.css/animate.css"/>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/styles/app.css" id="load_styles_before"/>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/styles/app.skins.css"/>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/jquery-ui-1.12.0/jquery-ui.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/bootstrap3-wysihtml5-master/bootstrap-wysihtml5.css">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/tokenize/tokenize2.css" rel="stylesheet" />
<script type="text/javascript">
  var style = '<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/css/style.css?'+Math.random();;
</script>
<script type="text/javascript">
  document.write('<link href="'+style+'" rel="stylesheet">');
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/jquery/dist/jquery.js"></script>
<?php } ?>
<!-- endbuild -->
<!--push notification plugin code-->
<script type="text/javascript">
    /*(function(p,u,s,h){
        p._pcq=p._pcq||[];
        p._pcq.push(['_currentTime',Date.now()]);
        s=u.createElement('script');
        s.type='text/javascript';
        s.async=true;
        s.src='https://cdn.pushcrew.com/js/4d3794a019d9df9eafefa772ae04f24b.js';
        h=u.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s,h);
    })(window,document);*/
	/*function Notify(){
		var notify;
		if(Notification.permission === 'default'){
			alert('Notification Not allowed');
		} else{
			//alert('Notification allowed.');
			notify = new Notification('New Alert', {
				body: 'This is message body.',
				tag: '123'
				// icon: ''
				});
			notify.onclick = function(){
				console.log(this.body);
				}
		}
	}*/
</script>
<?php /*?><script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async='async'></script>
  <script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "14f020df-d9b1-48f7-b334-8b54cd75024f",
      autoRegister: false,  //Set to true to automatically prompt visitors
      subdomainName: 'https://simplifydemo.onesignal.com',
      //subdomainName: Use the value you entered in step 1.4: http://imgur.com/a/f6hqN
      httpPermissionRequest: {
        enable: true
      },
      notifyButton: {
          enable: true  //Set to false to hide
      }
    }]);
  </script><?php */?>
</head>
<body>
<div class="app expanding"> 
  <!--sidebar panel-->
  <div class="off-canvas-overlay" data-toggle="sidebar"></div>
  <div class="sidebar-panel">
    <div class="brand"> 
      <!-- toggle offscreen menu --> 
      <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen hidden-lg-up"> <i class="material-icons">menu</i> </a> 
      <!-- /toggle offscreen menu --> 
      <!-- logo --> 
      <a class="brand-logo small" href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>"> <img class="expanding-hidden two" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/logo-small.png" alt=""/> </a> <a class="brand-logo large" href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>"> <img class="expanding-hidden two" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/logo-large.png" style="    margin-left: 2px;" alt=""/> </a> 
      <!-- /logo --> 
    </div>
    <div class="nav-profile "> 
      <script>
   	$('#iframe').ready(function() {
		 setTimeout(function() {
			$('#iframe').contents().find('#toolbarRight').remove();
		 }, 100);
	 });
   </script> 
      <!-- <a href="javascript:;" class="dropdown-toggle no-arrow" style="">
        <div class="user-image">
          <img src="template-assets/images/avatar.jpg" class="avatar img-circle" alt="user" title="user"/>
        </div>
      </a> --> 
    </div>
    <!-- main navigation -->
    <nav>
      <p class="nav-title">NAVIGATION</p>
      <ul class="nav">
        <?php
        //if the profile of the candidate is approved then allow him menu
        if($Client->profile_approve == 'Yes'){
        //Menu for Administrator, MSP Coordinator, Main client
        if($Client->type == 'Supper Client' || $Client->member_type == 'Account Manager' || $Client->member_type == 'Administrator'){
        ?>
        <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>" class=""> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/dashboard@512px.svg"> </i> <span>Dashboard</span> </a> </li>
        <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/expressJob/index'); ?>" class=""> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/express-job@512px.svg"> </i> <span>Express Job</span> </a> </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/jobs@512px.svg"> </i> <span>Jobs</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"> <span>Jobs</span> </a> </li>
            <!--<li> <a href="<?php //echo Yii::app()->createAbsoluteUrl('Client/job/addjob'); ?>"> <span>Add Job</span> </a>-->
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/noMarkup/addjob'); ?>"> <span>Add Job<!--Add NoMarkup Job--></span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobtemplates'); ?>"> <span>Job Catalog</span> </a> </li>
            <?php /*?><li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/index'); ?>"> <span>Reports</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/jobReports'); ?>"> <span>Job Reports</span> </a> </li>
            <?php */?>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting1',array('tab'=>'pending')); ?>"> <span>Pending Approval</span> </a> </li>
          </ul>
        </li>
        <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/client/admin'); ?>" class=""> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"> </i> <span>Submission</span> </a> </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/interviews@512px.svg"> </i> <span>Interviews</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/interviewList'); ?>"> <span>List of Interview</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/interviewStep1'); ?>"> <span>Add New Appointment</span> </a> </li>
          </ul>
        </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/jobs@512px.svg"> </i> <span>Offer</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/offer'); ?>"> <span>Offer List</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>"> <span>Create Offer</span> </a> </li>
          </ul>
        </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/workorder@512px.svg"> </i> <span>Work Order</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/workorderList'); ?>" class="" > <span>Work Orders</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/workorderStep1'); ?>"> <span>Create Work Order</span> </a> </li>
          </ul>
        </li>
        <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/todayboarding'); ?>" class=""> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/on-boarding@512px.svg"> </i> <span>On-Boarding</span> </a> </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/contract@512px.svg"> </i> <span>Contract</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/index'); ?>" class="" > <span>Contract</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractTermination'); ?>" class="" > <span>Contract Termination</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractExtension'); ?>" class="" > <span>Contract Extension</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractExtensionList'); ?>" class="" > <span>Contract Extension Request</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/contractrenewalsList'); ?>" class="" > <span>Contract Renewals List</span> </a> </li>
          </ul>
        </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/on-boarding@512px.svg"> </i> <span>Digital Doc</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/digitalDoc'); ?>"> <span>Offer Digital Doc</span> </a> </li>
            <li> <a href="<?php echo $this->createAbsoluteUrl('contract/onboarddigitalDoc'); ?>"> <span>Onboard Digital Doc</span> </a> </li>
          </ul>
        </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/time-sheet@512px.svg"> </i> <span>Time Sheet</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo $this->createAbsoluteUrl('offer/timesheetList'); ?>"> <span>Timesheet</span> </a> </li>
            <li> <a href="<?php echo $this->createAbsoluteUrl('timeSheet/pendingTimeSheets'); ?>"> <span>Pending Timesheet</span> </a> </li>
            <li> <a href="<?php echo $this->createAbsoluteUrl('timeSheet/approvedTimeSheets'); ?>"> <span>Approved Timesheet</span> </a> </li>
            <li> <a href="<?php echo $this->createAbsoluteUrl('timeSheet/rejectedTimeSheets'); ?>"> <span>Rejected Timesheet</span> </a> </li>
          </ul>
        </li>
        <li class="parent"><a href="javascript:;" class=""> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/billing@512px.svg"> </i> <span>Invoices</span> </a>
          <ul class="sub-menu">
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/adminconsolidate'); ?>">Pending invoice</a> </li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/approvedInvoice'); ?>">Approved invoice</a> </li>
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/adminStatistic'); ?>">Individual Invoice</a> </li>
          </ul>
        </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/report@512px.svg"> </i> <span>Reports</span> </a>
          <ul class="sub-menu">
		    <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobReports/index'); ?>" class="" > <span>Report Dashboard</span> </a> </li>
	        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> </i> <span>Activity Reports</span> </a>
	          	<ul class="sub-menu">
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/headcountCurrent'); ?>" class="" > <span>Head Count Report</span> </a> </li>
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/headcountTrend'); ?>" class="" > <span>Head Count Trend</span> </a> </li>
	           	</ul>
	        </li>
	        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> </i> <span>Productivity Reports</span> </a>
	          	<ul class="sub-menu">
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/headcountCurrent'); ?>" class="" > <span>Time to Fill</span> </a> </li>
	           	</ul>
	        </li>
	        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> </i> <span>Performance Reports</span> </a>
	          	<ul class="sub-menu">
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/headcountCurrent'); ?>" class="" > <span>Supplier Scorecard</span> </a> </li>
	           	</ul>
	        </li>
	        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> </i> <span>Custom Reports</span> </a>
	          	<ul class="sub-menu">
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/headcountCurrent'); ?>" class="" > <span>Supplier Scorecard</span> </a> </li>
	           	</ul>
	        </li>
	        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> </i> <span>Old Reports</span> </a>
	          	<ul class="sub-menu">
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/jobReports/index'); ?>" class="" > <span>Job Reports</span> </a> </li>
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/scorecard'); ?>" class="" > <span>Supplier Score Card</span> </a> </li>
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/headCountReoprt'); ?>" class="" > <span>Head Count Report</span> </a> </li>
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/timesheetReport'); ?>" class="" > <span>Timesheet Report</span> </a> </li>
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/spendReport'); ?>" class="" > <span>Spend Report</span> </a> </li>
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/submissionReports'); ?>" class="" > <span>Submission Report</span> </a> </li>
		            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/reports/interviewReports'); ?>" class="" > <span>Interview Report</span> </a> </li>
	           	</ul>
	        </li>
          </ul>
        </li>
        <li>
        <li>
          <hr/>
        </li>
        <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/index'); ?>" class=""> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/setting@512px.svg"> </i> <span>Settings</span> </a> </li>
        <?php }
        if($Client->member_type == 'Hiring Manager'){
        ?>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/jobs@512px.svg"> </i> <span>Jobs</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"> <span>Jobs</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/addjob'); ?>"> <span>Add Job</span> </a> </li>
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting1',array('tab'=>'pending')); ?>"> <span>Pending Approval</span> </a> </li>
          </ul>
        </li>
        <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/client/admin'); ?>" class=""> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"> </i> <span>Submission</span> </a> </li>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/interviews@512px.svg"> </i> <span>Interviews</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/interviewList'); ?>"> <span>List of Interview</span> </a> </li>
          </ul>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/contract@512px.svg"> </i> <span>Contract</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/ContractExtensionWorkflowList'); ?>" class="" > <span>Pending For Approval</span> </a> </li>
          </ul>
        </li>
        </li>
        <?php $cptimsheet = CpTimesheet::model()->findByAttributes(array('approval_manager'=>$loginUserId));
          if(!empty($cptimsheet)){
          ?>
        <li> <a href="javascript:;"> <span class="menu-caret"> <i class="material-icons">arrow_drop_down</i> </span> <i class="icon settings"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/time-sheet@512px.svg"> </i> <span>Time Sheet</span> </a>
          <ul class="sub-menu">
            <li> <a href="<?php echo $this->createAbsoluteUrl('offer/timesheetList'); ?>"> <span>Timesheet</span> </a> </li>
            <li> <a href="<?php echo $this->createAbsoluteUrl('timeSheet/pendingTimeSheets'); ?>"> <span>Pending Timesheet</span> </a> </li>
            <li> <a href="<?php echo $this->createAbsoluteUrl('timeSheet/approvedTimeSheets'); ?>"> <span>Approved Timesheet</span> </a> </li>
            <li> <a href="<?php echo $this->createAbsoluteUrl('timeSheet/rejectedTimeSheets'); ?>"> <span>Rejected Timesheet</span> </a> </li>
          </ul>
        </li>
        <?php } } } ?>
      </ul>
    </nav>
    <!-- /main navigation --> 
  </div>
  <!-- /sidebar panel --> 
  <!-- content panel -->
  <div class="main-panel"> 
    <!-- top header -->
    <nav class="header navbar">
      <div class="header-inner">
        <div class="navbar-item navbar-spacer-right brand hidden-lg-up"> 
          <!-- toggle offscreen menu --> 
          <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen"> <i class="material-icons">menu</i> </a> 
          <!-- /toggle offscreen menu --> 
          <!-- logo -->
          <?php
          if(CDN) { ?>
          <a class="brand-logo hidden-xs-down"> <img src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/images/logo_white.png" alt="logo"/> </a> 
          <!-- /logo -->
          <?php }else{ ?>
          <a class="brand-logo hidden-xs-down"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/images/logo_white.png" alt="logo"/> </a> 
          <!-- /logo -->
          <?php }
          ?>
        </div>
        <a class="navbar-item navbar-spacer-right navbar-heading hidden-md-down" href="#"> <span><?php echo $this->pageTitle; ?></span> </a>
        <div class="navbar-search navbar-item">
          <form class="search-form" action="<?php echo Yii::app()->createAbsoluteUrl('Client/default/search'); ?>" method="get">
            <i class="material-icons">search</i>
            <input name="s" class="form-control" type="text" placeholder="Search by Job ID" />
          </form>
        </div>
        <?php
        $to_date = date("Y-m-d", strtotime("+3 day"));
        $from_date = date("Y-m-d");
        $query = '(select j.title as title,v.id as id,j.id as jobId,v.id as subId,c.first_name as first_name,v.date_created as dated,("submission") as type from vms_vendor_job_submission v,vms_candidates c,vms_job j where v.bell_notification=0 and v.job_id=j.id and v.candidate_Id=c.id and v.client_id = "'.Yii::app()->user->id.'" and v.date_created BETWEEN "'.$from_date.'" AND  "'.$to_date.'" order by id DESC)
     
                 UNION
        
        (select "0" as title,i.id as id,j.id as jobId,v.id as subId,"0" as first_name,i.interview_creation_date as dated , ("interview") as type from vms_interview i,vms_vendor_job_submission v,vms_job j where i.bill_notification = 0 and i.submission_id=v.id and v.job_id=j.id and v.client_id="'.Yii::app()->user->id.'" and i.interview_creation_date BETWEEN "'.$from_date.'" AND  "'.$to_date.'" order by id DESC)';
        $Notifications = Yii::app()->db->createCommand( $query )->query()->readAll();
        $numberOfNotifications = count($Notifications);
        ?>
        <div class="navbar-item nav navbar-nav">
          <div class="nav-item nav-link dropdown"> <a class="dropdown-toggle gantiNotification" data-toggle="dropdown"> <i class="material-icons">notifications</i> <span class="tag tag-danger getspan" style="margin-right: -6px; margin-top: -15px;">
            <?php if(!empty($numberOfNotifications)){ echo $numberOfNotifications; }; ?>
            </span> </a>
            <div class="dropdown-menu dropdown-menu-right notifications">
              <div class="dropdown-item">
                <div class="notifications-wrapper">
                  <ul class="notifications-list">
                    <?php if($Notifications){
                      foreach ($Notifications as $notifi){
                        if($notifi['type']=='interview'){
                      ?>
                    <li> <a href="#" onClick="Notifications(<?php echo $notifi['jobId']; ?>,<?php echo $notifi['id']; ?>,<?php echo $notifi['subId']; ?>)">
                      <div class="notification-icon">
                        <div class="circle-icon bg-success text-white"> <i class="material-icons">arrow_upward</i> </div>
                      </div>
                      <div class="notification-message"> <b>Interview ID</b> <?php echo $notifi['id']; ?> <span class="time"><?php echo $notifi['dated']; ?></span> </div>
                      </a> </li>
                    <?php }else{ ?>
                    <li> <a href="#" onClick="subnotification(<?php echo $notifi['subId']; ?>)">
                      <div class="notification-icon">
                        <div class="circle-icon bg-danger text-white"> <i class="material-icons">check</i> </div>
                      </div>
                      <div class="notification-message"> <b><?php echo $notifi['first_name'] ?></b> <?php echo $notifi['title'] ?> <span class="time"><?php echo $notifi['dated']; ?></span> </div>
                      </a> </li>
                    <?php } } } ?>
                  </ul>
                </div>
                <div class="notification-footer">Notifications</div>
              </div>
            </div>
          </div>
          <div class="nav-item nav-link dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
            <div class="user-image">
              <?php
                $Client = Client::model()->findByPk(Yii::app()->user->id);
                if(!empty($Client->profile_image)) {
                  $images_path = Yii::app()->baseUrl.'/images/profile_img/'.$Client->profile_image;
                }else{
              if(CDN) {
                $images_path = 'http://1288083182.rsc.cdn77.org/new-theme/template-assets/images/avatar.jpg';
              }else{
                $images_path = Yii::app()->request->baseUrl.'/new-theme/template-assets/images/avatar.jpg';
              }
                }
                ?>
              <img src="<?php echo $images_path; ?>"
                     class="avatar img-circle" style="width: 29px;" title="<?php echo $Client->first_name.' '.$Client->last_name; ?>"/> </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right" style="margin-top: 1px;"> <a class="dropdown-item" href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/index');?>">Settings</a> <a class="dropdown-item" href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/changPassword'); ?>">Change Password</a> 
              <!--<a class="dropdown-item" href="javascript:;"> <span>Notifications</span> <span class="tag bg-primary">34</span> </a>-->
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="javascript:;">Help</a> <a class="dropdown-item" href="<?php echo Yii::app()->createAbsoluteUrl('/Client/default/logout');?>">Logout</a> </div>
          </div>
        </div>
      </div>
    </nav>
    <!-- /top header --> 
    <!-- main area -->
    <div class="main-content">
      <div class="content-view" style="padding: 0; ">
        <div class=" layout-xs b-b">
          <div class="layout-column-xs overflow-hidden">
            <div class="row m-x-0 "> <?php echo $content; ?> </div>
          </div>
        </div>
      </div>
      <!-- content-view --> 
      <!-- bottom footer -->
      <div class="content-footer" style="background: #fff;">
        <nav class="footer-right">
          <ul class="nav">
            <!--<li> <a href="javascript:;">Feedback</a> </li>-->
          </ul>
        </nav>
        <nav class="footer-left">
          <ul class="nav">
            <li> <a href="javascript:;"> <span>Copyright</span> &copy; 2017 </a> </li>
            <!--<li class="hidden-md-down"> <a href="javascript:;">Privacy</a> </li>
            <li class="hidden-md-down"> <a href="javascript:;">Terms</a> </li>
            <li class="hidden-md-down"> <a href="javascript:;">help</a> </li>-->
          </ul>
        </nav>
      </div>
      <!-- /bottom footer --> 
    </div>
    <!-- /main area --> 
  </div>
  <!-- /content panel --> 
  <!-- chatepanel --> 
</div>
<!-- build:js({.tmp,app}) scripts/app.min.js --> 
<!-- page scripts -->
<?php
if(CDN){ ?>
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/pace/pace.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/tether/dist/js/tether.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/fastclick/lib/fastclick.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/scripts/constants.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/intl-tel-input//build/js/intlTelInput.min.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/moment/min/moment.min.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/select2/select2.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/typeahead.js/dist/typeahead.bundle.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/multiselect/js/jquery.multi-select.js"></script> 
<script type="text/javascript" src="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/jquery-ui-1.12.0/jquery-ui.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/bootstrap/dist/js/bootstrap.js"></script> 
<!-- initialize page scripts --> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/scripts/forms/plugins.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/Chart.js/dist/Chart.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/scripts/charts/chartjs.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/scripts/charts/easypie.js"></script>
<?php }else{
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/pace/pace.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/tether/dist/js/tether.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/fastclick/lib/fastclick.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/scripts/constants.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/intl-tel-input//build/js/intlTelInput.min.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/moment/min/moment.min.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/select2/select2.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/typeahead.js/dist/typeahead.bundle.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/multiselect/js/jquery.multi-select.js"></script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/jquery-ui-1.12.0/jquery-ui.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap/dist/js/bootstrap.js"></script> 
<!-- initialize page scripts --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/scripts/forms/plugins.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/Chart.js/dist/Chart.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/scripts/charts/chartjs.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/scripts/charts/easypie.js"></script>
<?php } ?>
<!-- end initialize page scripts --> 
<!-- endbuild --> 
<!-- initialize page scripts
<script src="/template-assets/scripts/dashboard/dashboard.js"></script>  --> 
<script>
  $( document ).ready(function() {
    $('#client_job_status').on('change',function(){
      var status = $(this).val();
      $.ajax({
        'url':'<?php echo $this->createUrl('settinggeneral/changeClientstatus') ?>',
        type: "POST",
        data: { status: status},
        'success':function(html){
          //$("#candidates").html(html)
          if(html=='Success') {
            $("#bsModal3").modal('show');
          }
        }
      });
    });
    $('#Offer_jobs').on('change',function(){
      var job_id = $(this).val();
      $.ajax({
        'url':'<?php echo $this->createUrl('offer/offerCandidates') ?>',
        type: "POST",
        data: { job_id: job_id},
        'success':function(html){
          $("#candidates").html(html)
          /*$('#Courses_type').trigger('change');*/
        }
      });
    });
	  $('#Interview_jobs').on('change',function(){
			var job_id = $(this).val();
			$.ajax({
				'url':'<?php echo $this->createUrl('offer/interviewCandidates') ?>',
				type: "POST",
				data: { job_id: job_id},
				'success':function(html){
					$("#candidates").html(html)
					/*$('#Courses_type').trigger('change');*/
				}
			});
		});
    $('#WorklflowMember_department_id,#ContractWorklflowMember_department_id').on('change',function(){
      var dept_id = $(this).val();
      //alert(dept_id);
      $.ajax({
        'url':'<?php echo $this->createUrl('settinggeneral/members') ?>',
        type: "POST",
        data: { dept_id: dept_id},
        'success':function(html){
          //alert(html);
          $("#team_members").html(html)
          //$('#Courses_type').trigger('change');
        }
      });
    });
    $('#schedule-interview-modal .dropdown')
        .dropdown({
          onChange: function(value) {
            var val = value.split("-");
            $('.modal-footer a').attr('href',"<?php echo $this->createAbsoluteUrl('/Client/job/scheduleInterview'); ?>/id/"+ val[1]+'/submission-id/'+val[0]);
          }
        });
  });
</script> 
<script>
  $( document ).ready(function() {
$('#Offer_jobs1').on('change',function(){
      var job_id = $(this).val();
      $.ajax({
        'url':'<?php echo $this->createUrl('offer/workOrderCandidates') ?>',
        type: "POST",
        data: { job_id: job_id},
        'success':function(html){
          $("#candidates").html(html)
          /*$('#Courses_type').trigger('change');*/
        }
      });
      });
      });
</script>
<?php
if(CDN){ ?>
<script src="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/bootstrap3-wysihtml5-master/wysihtml5-0.3.0.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/bootstrap3-wysihtml5-master/bootstrap3-wysihtml5.js"></script> 
<!-- new tokenize plugin scripts--> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/vendor/tokenize/tokenize2.js"></script> 
<!-- initialize page scripts --> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/scripts/forms/plugins.js"></script> 
<!-- end initialize page scripts --> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/template-assets/scripts/main.js"></script> 
<script src="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/jquyery-datetimepicker/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/jquyery-datetimepicker/jquery.datetimepicker.css">
<!--  Full calender plugin --> 
<script type="text/javascript" src="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://1288083182.rsc.cdn77.org/new-theme/assets/plugins/fullcalendar/fullcalendar.min.css">
<script src="http://1288083182.rsc.cdn77.org/new-theme/jquery.autocomplete.js"></script>
<?php }else{ ?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/bootstrap3-wysihtml5-master/wysihtml5-0.3.0.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/bootstrap3-wysihtml5-master/bootstrap3-wysihtml5.js"></script> 
<!-- new tokenize plugin scripts--> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/tokenize/tokenize2.js"></script> 
<!-- initialize page scripts --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/scripts/forms/plugins.js"></script> 
<!-- end initialize page scripts --> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/scripts/main.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/jquyery-datetimepicker/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/jquyery-datetimepicker/jquery.datetimepicker.css">
<!--  Full calender plugin --> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/plugins/fullcalendar/fullcalendar.min.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/jquery.autocomplete.js"></script>
<?php } ?>
<script>
$( document ).ready(function() {
 if ($('.time-picker').length) {
      $('.time-picker').timepicker({ 'scrollDefault': 'now','step': 15  });
  };
  $('.tokenize-remote-demo1').tokenize2({
 		dataSource: "<?php echo Yii::app()->createAbsoluteUrl('Client/costCenter/skills') ?>",
		tokensAllowCustom: true,
 		sortable: true,
 	});
	//add guest field tokens on schedule interview page
	$('.tokenize-remote-demo2').tokenize2({
		tokensAllowCustom: true,
		sortable: true,
     });
});
</script> 
<script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
	$('.textarea.wysihtml5').wysihtml5();
  })
</script> 
<script>
  function subnotification(id) {
    var submissionId = id ;
    //alert(interviewId);
    $.ajax({
      'url':'<?php echo $this->createUrl('costCenter/subNotification') ?>',
      type: "POST",
      data: { submissionId: submissionId},
      'success':function(html){
        if(html=='success'){
          url = "<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission') ?>/submission-id/"+ submissionId +"";
          $( location ).attr("href", url);
        }
      }
    });
  }
  function Notifications(jobid,id,subid) {
    var interviewId = id ;
    //alert(interviewId);
    $.ajax({
      'url':'<?php echo $this->createUrl('costCenter/interviewNotification') ?>',
      type: "POST",
      data: { interviewId: interviewId},
      'success':function(html){
        if(html=='success'){
          url = "<?php echo Yii::app()->createAbsoluteUrl('Client/job/scheduleInterview') ?>/id/"+ jobid +"/interviewId/"+ interviewId +"/submission-id/"+ subid +"";
          $( location ).attr("href", url);
        }
      }
    });
  }
  jQuery(document).ready(function($) {
    $('.sidebar-panel').mouseleave(function(event) {
      /* Act on the event */
      $('.sidebar-panel nav ul.nav li.open').removeClass('open');
    });
  });
$(document).ready(function(){
  /*$('.gantiNotification').on('click', function(event) {
    //alert('a-tag')
    $(this).find('span').remove();
  });
  $('.getspan').on('click', function(event) {
    //alert('span')
    $(this).remove();
  });*/
  $('#search').keyup(function(){
 var val=$(this).val();
        $('table tbody tr').hide();
         var trs=$('table tbody tr').filter(function(d){
         return $(this).text().toLowerCase().indexOf(val)!=-1;
         });
         trs.show();
});
	$('#search-cate').keyup(function(){
 var val=$(this).val();
        $('table.search-cat tbody tr').hide();
         var trs=$('table.search-cat tbody tr').filter(function(d){
         return $(this).text().toLowerCase().indexOf(val)!=-1;
         });
         trs.show();
});
  $('#sort_by_payrate').on('change',function(){
    var payrate_order = $(this).val();
    var jobid = $("#job_id").val();
    if(payrate_order == 'asc'){
      url = "<?php echo Yii::app()->createAbsoluteUrl('Client/job/billMatchAsc') ?>/id/"+ jobid +"/type/bill";
      $( location ).attr("href", url);
    }else{
      url = "<?php echo Yii::app()->createAbsoluteUrl('Client/job/billMatchDesc') ?>/id/"+ jobid +"/type/bill";
      $( location ).attr("href", url);
    }
  });
});
$('#approver_manager_location').on('change',function(){
  var location_id = $(this).val();
  $.ajax({
    'url':'<?php echo $this->createUrl('offer/locationManager') ?>',
    type: "POST",
    data: { location_id: location_id},
    'success':function(html){
      $("#location_manager").html(html)
    }
  });
});
$('#Job_cat_id').on('change',function(){
  var cat_value = $(this).val();
  $.ajax({
    'url':'<?php echo $this->createUrl('job/payrates') ?>',
    type: "POST",
    data: { cat_value: cat_value},
    'success':function(html){
      //document.getElementById("vendor_client_markup").value = 'testing';
      //$("#vendor_client_markup").val(html)
      result = JSON.parse(html);
      //alert(result.markup);
      $("#vendor_client_markup").val(result.markup);
      $("#cost_center_code").html(result.costcenter);
    }
  });
});
    //already selected category value for template action
    <?php //if($this->action->id == 'selectetemplate') { ?>
      var cat_value = $("option:selected","#Job_cat_id").val();
      $.ajax({
        'url':'<?php echo $this->createUrl('job/payrates') ?>',
        type: "POST",
        data: { cat_value: cat_value},
        'success':function(html){
          result = JSON.parse(html);
                  //alert(result.markup);
                  $("#vendor_client_markup").val(result.markup);
                  $("#cost_center_code").html(result.costcenter);
        }
      });
    <?php //} ?>
</script> 
<script type="text/javascript">
  $(function() {
  /**if ( $('.time-picker').length ) {
    $('.time-picker').timepicker({
        defaultTime:  false
        //defaultTime:  '00:00 AM'
      });
  };**/
    var dateToday = new Date();
    $('.datetimepickermodified').daterangepicker({
      autoUpdateInput: false,
      singleDatePicker: true,
      minDate: dateToday,
      locale: {
        cancelLabel: 'Clear'
      }
    });
    $('.datetimepickermodified').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });
    $('.datetimepickermodified').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
  });
  $(function() {
    /**if ( $('.time-picker').length ) {
    $('.time-picker').timepicker({
        defaultTime:  false
        //defaultTime:  '00:00 AM'
      });
  };**/
    //var dateToday = new Date();
    $('.singledate').daterangepicker({
      autoUpdateInput: false,
      singleDatePicker: true,
      locale: {
        cancelLabel: 'Clear'
      }
    });
    $('.singledate').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });
    $('.singledate').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
  });
  
  /*$(function() {
    var dateToday = new Date();
    $('input[name="job_po_duration"],input[name="job_po_duration_endDate"],input[name="desired_start_date"]').daterangepicker({
          minDate: dateToday,
          singleDatePicker: true,
          showDropdowns: true,
        },
        function(start, end, label) {
        });
  });*/
  
  $(function() {
    //var dateToday = new Date();
    $('input[name="onboard_changed_start_date"],input[name="onboard_changed_end_date"]').daterangepicker({
          //minDate: dateToday,
          singleDatePicker: true,
          showDropdowns: true,
        },
        function(start, end, label) {
        });
  });


  $(function() {
    var dateToday = new Date();
    $('input[name="job_po_duration_endDate"],input[name="desired_start_date"]').daterangepicker({
      minDate: dateToday,
      autoUpdateInput: false,
      singleDatePicker: true,
      locale: {
        cancelLabel: 'Clear'
      }
    });
    $('input[name="job_po_duration_endDate"],input[name="desired_start_date"]').daterangepicker({
      minDate: dateToday,
      autoUpdateInput: false,
      singleDatePicker: true,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('input[name="job_po_duration_endDate"],input[name="desired_start_date"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('input[name="job_po_duration_endDate"],input[name="desired_start_date"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

  });
  //on express job creation page defulat date is removed
  //input[name="job_po_duration123"],input[name="desired_start_date123"]
  
  $(function() {

  $('input[name="job_po_duration"],input[name="express_job_s_date"],input[name="express_job_e_date"]').daterangepicker({
      autoUpdateInput: false,
	  singleDatePicker: true,
      locale: {
          cancelLabel: 'Clear'
      }
  });
    $('input[name="job_po_duration"],input[name="express_job_s_date"],input[name="express_job_e_date"]').daterangepicker({
      autoUpdateInput: false,
      singleDatePicker: true,
      locale: {
        cancelLabel: 'Clear'
      }
    });

  $('input[name="job_po_duration"],input[name="express_job_s_date"],input[name="express_job_e_date"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
  });

  $('input[name="job_po_duration"],input[name="express_job_s_date"],input[name="express_job_e_date"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});
  
</script>
<?php if($this->action->id != 'scheduleInterview' ) { ?>
<script>
    $( document ).ready(function() {
      <?php if($this->action->id == 'jobStep1' || $this->action->id == 'selectetemplate'){ ?>
      document.getElementById("pre_name").disabled = true;
      document.getElementById("pre_supplier_name").disabled = true;
      document.getElementById("pre_current_rate").disabled = true;
      document.getElementById("pre_payment_type").disabled=true;
      $(".pre_fields").addClass("disabled");
      <?php }else{ ?>
        if ( $("#pre_candidate").length ) {
      if(document.getElementById("pre_candidate").value == 'No'){
        document.getElementById("pre_name").disabled = true;
        document.getElementById("pre_supplier_name").disabled = true;
        document.getElementById("pre_current_rate").disabled = true;
        document.getElementById("pre_payment_type").disabled=false;
        //$(".pre_fields").addClass("disabled");
      }
    };
      <?php } ?>
      $('#pre_candidate').on('change',function(){
        var pre_candidate = $(this).val();
        if(pre_candidate == 'Yes'){
          document.getElementById("pre_name").disabled = false;
          document.getElementById("pre_supplier_name").disabled = false;
          document.getElementById("pre_current_rate").disabled = false;
          document.getElementById("pre_payment_type").disabled=false;
          //$(".pre_fields").removeClass("disabled");
        }else{
          document.getElementById("pre_name").disabled = true;
          document.getElementById("pre_supplier_name").disabled = true;
          document.getElementById("pre_current_rate").disabled = true;
          document.getElementById("pre_payment_type").disabled=true;
          //$(".pre_fields").addClass("disabled");
        }
      });
      var pre_candidate = $("#pre_candidate :selected").text();
      
      if(pre_candidate == 'Yes'){
        document.getElementById("pre_name").disabled = false;
        document.getElementById("pre_supplier_name").disabled = false;
        document.getElementById("pre_current_rate").disabled = false;
        document.getElementById("pre_payment_type").disabled=false;
        $(".pre_fields").removeClass("disabled");
      }if(pre_candidate == 'No'){
        document.getElementById("pre_name").disabled = true;
        document.getElementById("pre_supplier_name").disabled = true;
        document.getElementById("pre_current_rate").disabled = true;
        document.getElementById("pre_payment_type").disabled=true;
        $(".pre_fields").addClass("disabled");
      }

    });
  </script>
<?php } ?>
<?php
$skill = Skills::model()->findAll();
$skills = array();
//$skills1 = array();
foreach($skill as $skill){
  $skills[] = "'$skill->name'";
  //$skills1[] = "'$skill->name'";
}
?>
<script type="text/javascript">
<?php /*?>
 function customTime(starttime,interviewTime,id){
	 var a = starttime.split(" ");
	 var b = a[0].split(":");
	 if(a[1]=='pm' && b[0] != 12){
		 var hours = parseInt(b[0]) + 12;
		 }else if(a[1]=='pm' && b[0] == 12){
			 var hours = parseInt(b[0]);
			 //alert(hours);
			 }else{
			  var hours = parseInt(b[0]);
			  }
	 var minute = b[1];
	 var totalMinutes = hours * 60 + parseInt(minute + interviewTime);
	 var finalhours = Math.floor(totalMinutes / 60);
	 var finalMinutes = totalMinutes % 60;
	 $('#'+id).timepicker('setTime', finalhours+':'+finalMinutes);
	 }
 //recomended time duration code
 $('#start_date_time_duration,#interview_start_date_time').on('change',function(){
	 var interviewTime = $('#start_date_time_duration').val();
	 var starttime = $('#interview_start_date_time').val();
	 var id = 'end_date_time';
	 customTime(starttime,interviewTime,id);
  });
  //alternate time1 duration code
 $('#alt1_start_date_time_duration,#alt1_start_date_time').on('change',function(){
	 var interviewTime = $('#alt1_start_date_time_duration').val();
	 var starttime = $('#alt1_start_date_time').val();
	 var id = 'alt1_end_date_time';
	 customTime(starttime,interviewTime,id);
  });
  //alternate time2 duration code
 $('#alt2_start_date_time_duration,#alt2_start_date_time').on('change',function(){
	 var interviewTime = $('#alt2_start_date_time_duration').val();
	 var starttime = $('#alt2_start_date_time').val();
	 var id = 'alt2_end_date_time';
	 customTime(starttime,interviewTime,id);
  });
  //alternate time3 duration code
 $('#alt3_start_date_time_duration,#alt3_start_date_time').on('change',function(){
	 var interviewTime = $('#alt3_start_date_time_duration').val();
	 var starttime = $('#alt3_start_date_time').val();
	 var id = 'alt3_end_date_time';
	 customTime(starttime,interviewTime,id);
  });<?php */?>
   jQuery(document).ready(function($) {
      $(function () {
        $('[data-toggle="popover"]').popover()
      })
   });
</script> 
<script>
  $(document).ready(function(){
    $('#data').keyup(function(){
      var searchvalue=$(this).val();
      $.ajax({
        'url':'<?php echo $this->createUrl('costCenter/jobSearch') ?>',
        type: "POST",
        data: { searchvalue: searchvalue},
        'success':function(html){
          //alert(html);
          $("#jobdata").html(html)
        }
      });
    });
	
	
	
	/*$('.regulardays').on('change' ,function() {
		 if ( $(this).val() > 7) {
			var total = parseInt(8) + parseInt($(".totalF").val()); 
			$(".totalF").val(total);
		 }else{
			 var total = parseInt($(this).val()) + parseInt($(".totalF").val()); 
			 $(".totalF").val(total);
		 }
   });*/
   
   
	$('.regulardays').on('change' ,function() {
		var days_total = 0;
		
		$('#myTableRow').find('input.regulardays').each(function()
		{
			var daysVal = parseInt($(this).val());
			
			if ( daysVal > 7) {
				console.log('if');
				days_total = parseInt(8) + parseInt(days_total); 
			 }else{
				 console.log('else');
				 days_total = parseInt(daysVal) + parseInt(days_total); 
			 }
			 
			 $(".totalF").val(days_total);
			
		});
		
	});
   
   /*$('.overtimedays').on('change' ,function() {
		var total = parseInt($(this).val()) + parseInt($(".totalOver").val()); 
		$(".totalOver").val(total);  
	});*/
	
	$('.overtimedays').on('change' ,function() {
		var days_total = 0;
		$('#myTableRow1').find('input.overtimedays').each(function()
		{
			var daysVal = parseInt($(this).val());
			days_total = days_total + daysVal;
			$(".totalOver").val(days_total);
		});
	});
	
	
  });
  // added by Zoobia Humayun 22-03-2017 for contract extension tab in contract section
 	 function calculateExt(id){ 
		$.ajax({
			'url':'<?php echo $this->createUrl('contract/extensionDetails') ?>',
			type: "POST",
			data: { contractID: id },
			'success':function(html){
				//console.log(html);
				result = JSON.parse(html);
				$('#response').show();			
				$("#woid").html(result.woid);
				$("#cid").html(result.cid);
				$("#contract_id").val(result.cid);
				$("#brate").html(result.brate);
				$("#prate").html(result.prate);
				$("#location").html(result.location);
				$("#jid").html(result.jid);
				$("#extvendor").html(result.vendor);
				$("#ext_pay_rate").val(result.ext_pay_rate);
				$("#ext_bill_rate").val(result.ext_bill_rate);
				$("#ext_over_time_prate").val(result.ext_over_time_prate);
				$("#ext_double_time_prate").val(result.ext_double_time_prate);
				$("#ext_over_time_brate").val(result.overtime_billrate);
				$("#ext_double_time_brate").val(result.doubletime_billrate);
				//=$("#new_job_end_date").val(result.job_end_date);
 				$("#contract_id").val(result.cid);
				$("#markup").val(result.markup);
				$("#job_start_date").val(result.contract_end_date);
				$("#hours_per_week").val(result.hours_per_week);
				$("#extstart_date").html(result.start_date);
				$("#extend_date").html(result.end_date);
 				$("#extmanager_name").html(result.name);
				if(result.ext_status == 2 || result.ext_status == 1){
 					$('#reason_status').html(result.ext_reason);
   					$('#note_of_extension').val(result.note_of_extension);
 					$('#new_job_end_date').val(result.new_contract_end_date);
					$('#ext_estimated_cost').val(result.new_estimate_cost);
					$("#ext_button").hide();
					$("#ext_pay_rate").attr('readonly', true);
					$("#note_of_extension").attr('readonly', true);
					$("#new_job_end_date").attr('readonly', true);
 				}
				$("#response").show();
				
 				var markup = result.markup;
				
				var numOfDates = getBusinessDatesCount(new Date(result.job_start_date),new Date(result.job_end_date));
								
				if(numOfDates==0){
					var numOfDates = 1;
				}
				
				var str = result.hours_per_week;
				var numberHours = str.split(" ",1);
				var hoursPerDay = numberHours/5;
				
				
				var pay_rate = result.ext_bill_rate * 100/( 100 + parseFloat(markup));
				document.getElementById("ext_pay_rate").value = pay_rate.toFixed(3);
				
				/*var estimatedCost = (result.ext_bill_rate * numOfDates * hoursPerDay).toFixed(3);
				document.getElementById("ext_estimated_cost").value = estimatedCost;
				workFlowSelection(estimatedCost);*/
				
				 dateField();
				 OnChangesCalculate();
				
				$( "#response,#classremoval" ).removeClass( "testrow" );
				
			}
		  });
	}
	
	function OnChangesCalculate(){
	$('#new_job_end_date').on('apply.daterangepicker', function(ev, picker) { 
 		var calendarDate = new Date(picker.startDate['_d']);
		console.log(calendarDate);
 		var date = calendarDate.getMonth()+1+'/'+calendarDate.getDate()+'/'+calendarDate.getFullYear();
		console.log(calendarDate);
		$("#new_assigned_job_end_date").val(date);
		PayRateChanges();
	});
    
	$('#ext_pay_rate').on("change", function () {
		PayRateChanges();
	});
	}
	function PayRateChanges(){
		
		var cid = $("#contract_id").val();
		var markup = $("#markup").val();
		var job_start_date = $("#job_start_date").val();
		var job_end_date = $("#new_assigned_job_end_date").val();
	  	var payrate = $('#ext_pay_rate').val();
		var hours_per_week = $("#hours_per_week").val();
				
		$.ajax({
			'url':'<?php echo $this->createUrl('contract/rateCalc') ?>',
			type: "POST",
			data: { cid: cid},
			'success':function(html){
				result = JSON.parse(html);
 				var numOfDates = getBusinessDatesCount(new Date(job_start_date),new Date(job_end_date));
								
				if(numOfDates==0){
					var numOfDates = 1;
				}
				var str = hours_per_week;
				var numberHours = str.split(" ",1);
				var hoursPerDay = numberHours/5;
 				
				var bill_rate = parseFloat(payrate) + (parseFloat(payrate) * parseFloat(markup)/100);
				document.getElementById("ext_bill_rate").value = bill_rate.toFixed(3);
								
				var estimatedCost = (bill_rate * numOfDates * hoursPerDay).toFixed(3);
		  		
				document.getElementById("ext_over_time_prate").value = (parseFloat(payrate) + parseFloat(payrate * result.over_time/100)).toFixed(3);
				document.getElementById("ext_double_time_prate").value = (parseFloat(payrate) + parseFloat(payrate * result.double_time/100)).toFixed(3);
				document.getElementById("ext_over_time_brate").value = (parseFloat(bill_rate) + parseFloat(bill_rate * result.over_time/100)).toFixed(3);
				//alert($("ext_over_time_brate").val());
				document.getElementById("ext_double_time_brate").value = (parseFloat(bill_rate) + parseFloat(bill_rate * result.double_time/100)).toFixed(3);
 				if(job_end_date !=0 ){
					document.getElementById("ext_estimated_cost").value = estimatedCost;
				}
				workFlowSelection(estimatedCost);
			}
		});
	
		}
		
	function getBusinessDatesCount(startDate, endDate) {
        var count = 0;
        var curDate = startDate;
        while (curDate <= endDate) {
            var dayOfWeek = curDate.getDay();
            if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
                count++;
            curDate.setDate(curDate.getDate() + 1);
        }
        return count;
    }	
	
	function workFlowSelection(estimatedCost){
		$.ajax({
				'url':'<?php echo $this->createUrl('contract/contractWflow') ?>',
				type: "POST",
				data: { estimatedCost: estimatedCost},
				'success':function(rep){
					$("#cwfa").html(rep);
				}
			});
		}
		
	function dateField(){
		var dateToday = new Date($("#job_start_date").val());
		$('#new_job_end_date').daterangepicker({
			minDate: dateToday,
			autoUpdateInput: false,
			singleDatePicker: true,
			locale: {
			  cancelLabel: 'Clear'
			}
		});
		$('#new_job_end_date').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('MM/DD/YYYY'));
		});
		$('#new_job_end_date').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
	};
	// ******extension section end*****
	// For contract termination tab 
	function calculateTermination(id){
			//alert('Hi');
			$.ajax({
			'url':'<?php echo $this->createUrl('contract/terminationDetails') ?>',
			type: "POST",
			data: { contractID:id },
			'success':function(html){
				result = JSON.parse(html);
				//console.log(html);
				$('#Termresponse').show();			
			    $("#twoid").html(result.woid);
			    $("#tcid").html(result.cid);
				$("#tcontract_id").val(result.cid);
				$("#tbrate").html(result.brate);
			    $("#tprate").html(result.prate);
				$("#tlocation").html(result.location);
			    $("#tjid").html(result.jid);
				$("#tvendor").html(result.vendor);
				$("#tstart_date").html(result.start_date);
				$("#tend_date").html(result.end_date);
				$("#manager_name").html(result.name);
				if(result.termination_status == 2 || result.termination_status == 1 )
				{ // load preselected values and disable the form
					$("#termination_reason").html(result.termination_reason);
					$("#termination_comments").val(result.termination_notes);
 					$("#termination_can_feedback").val(result.termination_can_feedback);
					$("#termination_date").val(result.termination_date);
					$("#statusTxt").html(result.statusTxt);
					$('#termination_comments').attr('readonly', true);
					$('#termination_can_feedback').attr('readonly', true);
					$('#termination_date').attr('readonly', true);
					$("#button").hide();
  				}
				if(result.termination_status == 1){
					$("#appr_rej").show();
				}
 				$( "#Termresponse,#classremoval" ).removeClass( "testrow" );
				$( "#Termresponse").show();
 			}
		  });
	}
	//******* termination tab end ******
	function submitTermination(){
		$('#termination').on('submit', function (e) {
			var form=$("#termination");
           e.preventDefault();
            $.ajax({
            type: 'POST',
            url: '<?php echo $this->createUrl('contract/TerminationStep1') ?>',
            data: {'reason_of_termination':$('select[name=reason_of_termination]').val(), 'termination_notes':$("#termination_comments").val(),'termination_can_feedback':$("#termination_can_feedback").val(),'termination_date':$("#termination_date").val() ,'terminationForm':1, 'isAjax':1,'contract_id':$("#tcontract_id").val()},
            success: function (res) {
               	$("#success").html(res);
				$("#button").hide();
            }
          });

        });
	}
	function TerminationApprRej(status){
		if(status == 1)
		{// Approve request
			var data = {'approved':1, 'isAjax':1};
		}
		if(status == 2)
		{// reject request
			var data = {'reject':1, 'isAjax':1};
		}
		var url = '<?php echo $this->createUrl('contract/TerminationView') ?>';
		url = url+'/id/'+ $("#tcontract_id").val();
		$.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (res) {0
               	$("#success").html(res);
				$("#appr_rej").hide();
				$("#extensionTab").hide();
             }
          });
 	}
	function submitExtensionForm(){
		$('#extension-form').on('submit', function (e) {
            e.preventDefault();
             $.ajax({
				type: 'POST',
				url: '<?php echo $this->createUrl('contract/ContractExtension') ?>',
				data: {'reason_of_extension':$("#reason_of_extension option:selected").val(),'note_of_extension':$("#note_of_extension").val(),'new_contract_end_date':$("#new_job_end_date").val(),'pay_rate':$("#ext_pay_rate").val(),'bill_rate':$("#ext_bill_rate").val(),'overtime_payrate':$("#ext_over_time_prate").val(),'doubletime_payrate':$("#ext_double_time_prate").val(),'overtime_billrate':$("#ext_over_time_brate").val(),'doubletime_billrate':$("#ext_double_time_brate").val(),'new_estimate_cost':$("#ext_estimated_cost").val(),'contract_id': $("#contract_id").val(),'contract_work_flow': $("#contract_work_flow").val(), 'ContractExtensionReq':1 , 'isAjax':1},
				success: function (res) {
					$("#ext_button").hide();
					$("#contract_success").html(res);
				 }	
			});
         });
 	}
  </script> 
<script>

  var c = 0; max_count = 60; logout = true;

  startTimer();



  function startTimer(){

    var session = parseInt(<?php echo Yii::app()->getSession()->getTimeout(); ?>);

    session = session-60;

    setTimeout(function(){

      logout = true;

      c = 0;

      max_count = 60;

      $('#timer').html(max_count);

      $('#logout_popup').modal('show');

      startCount();



    }, 1000*session);

  }



  function resetTimer(){

    logout = false;

    $('#logout_popup').modal('hide');

    startTimer();

  }



  function timedCount() {

    c = c + 1;

    remaining_time = max_count - c;

    if( remaining_time == 0 && logout ){

      $('#logout_popup').modal('hide');

      location.href = '<?php echo Yii::app()->createAbsoluteUrl('/Client/default/login'); ?>';



    }else{

      $('#timer').html(remaining_time);

      t = setTimeout(function(){timedCount()}, 1000);

    }

  }



  function startCount() {

    timedCount();

  }

  

</script>
</body>
</html>
