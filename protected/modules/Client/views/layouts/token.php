<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/logo-fav.png">
<title>VMS V4</title>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
  
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
  
  
    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
  
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.css">
      
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap-tokenfield/dist/css/tokenfield-typeahead.css">
      
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/jquery-ui-1.12.0/jquery-ui.css">
  <!--[if lt 
  IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap3-wysihtml5-master/bootstrap-wysihtml5.css">
  
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
  
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/font-awesome/css/font-awesome.css" type="text/css"/>
    
    
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/css/style.css" type="text/css"/>
  
  <!--live css link-->
  <link rel="stylesheet" href="http://demobetademo.online/clientportal/client/assets/css/style-new.css" type="text/css"/>
  
  <!--local css link-->
  <?php /*?><link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/css/style-new.css" type="text/css"/><?php */?>
</head>
<body>
<?php
$loginUserId = Yii::app()->user->id;
$Client = Client::model()->findByPk($loginUserId);
$clientModel = Client::model()->findByPk(Yii::app()->user->id);
?>
<?php
if(!empty($Client->profile_image)) {
  $images_path = Yii::app()->baseUrl.'/images/profile_img/'.$Client->profile_image;
	}else{
		$images_path = Yii::app()->request->baseUrl.'/new-theme-assets/assets/img/avatar.png';
		}
  ?>
<div class="be-wrapper">
  <nav class="navbar navbar-default navbar-fixed-top be-top-header">
    <div class="container-fluid">
      <div class="navbar-header"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>" class="navbar-brand"></a></div>
      <div class="be-right-navbar">
        <ul class="nav navbar-nav navbar-right be-user-nav">
          <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="<?php echo $images_path;?>"><span class="user-name"><?php echo $Client->first_name.' '.$Client->last_name; ?></span></a>
            <ul role="menu" class="dropdown-menu">
              <li>
                <div class="user-info">
                  <div class="user-name"><?php echo $Client->first_name.' '.$Client->last_name; ?></div>
                  <div class="user-position online">Available</div>
                </div>
              </li>
              <?php if($Client->profile_approve == 'Yes'){ ?>
              <?php /*?><li><a href="#"><span class="icon mdi mdi-face"></span> Account</a></li>
              <li><a href="#"><span class="icon mdi mdi-settings"></span> Settings</a></li><?php */?>
              <?php } ?>
              <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/logout'); ?>"><span class="icon mdi mdi-power"></span> Logout</a></li>
            </ul>
          </li>
        </ul>
        <div class="page-title"><span><?php echo $this->pageTitle; ?></span></div>
        <?php /*?><ul class="nav navbar-nav navbar-right be-icons-nav">
          <li class="dropdown"><a href="#" role="button" aria-expanded="false" class="be-toggle-right-sidebar"><span class="icon mdi mdi-settings"></span></a></li>
          <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-notifications"></span><span class="indicator"></span></a>
            <ul class="dropdown-menu be-notifications">
              <li>
                <div class="title">Notifications<span class="badge">3</span></div>
                <div class="list">
                  <div class="be-scroller">
                    <div class="content">
                      <ul>
                        <li class="notification notification-unread"><a href="#">
                          <div class="image"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar2.png"></div>
                          <div class="notification-info">
                            <div class="text"><span class="user-name">Jessica Caruso</span> accepted your invitation to join the team.</div>
                            <span class="date">2 min ago</span> </div>
                          </a></li>
                        <li class="notification"><a href="#">
                          <div class="image"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar3.png"></div>
                          <div class="notification-info">
                            <div class="text"><span class="user-name">Joel King</span> is now following you</div>
                            <span class="date">2 days ago</span> </div>
                          </a></li>
                        <li class="notification"><a href="#">
                          <div class="image"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar4.png"></div>
                          <div class="notification-info">
                            <div class="text"><span class="user-name">John Doe</span> is watching your main repository</div>
                            <span class="date">2 days ago</span> </div>
                          </a></li>
                        <li class="notification"><a href="#">
                          <div class="image"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar5.png"></div>
                          <div class="notification-info"><span class="text"><span class="user-name">Emily Carter</span> is now following you</span><span class="date">5 days ago</span></div>
                          </a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="footer"> <a href="#">View all notifications</a></div>
              </li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-apps"></span></a>
            <ul class="dropdown-menu be-connections">
              <li>
                <div class="list">
                  <div class="content">
                    <div class="row">
                      <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/github.png"><span>GitHub</span></a></div>
                      <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/bitbucket.png"><span>Bitbucket</span></a></div>
                      <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/slack.png"><span>Slack</span></a></div>
                    </div>
                    <div class="row">
                      <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/dribbble.png"><span>Dribbble</span></a></div>
                      <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/mail_chimp.png"><span>Mail Chimp</span></a></div>
                      <div class="col-xs-4"><a href="#" class="connection-item"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/dropbox.png"><span>Dropbox</span></a></div>
                    </div>
                  </div>
                </div>
                <div class="footer"> <a href="#">More</a></div>
              </li>
            </ul>
          </li>
        </ul><?php */?>
      </div>
    </div>
  </nav>
  <div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">General Tables</a>
      <div class="left-sidebar-spacer">
        <div class="left-sidebar-scroll">
          <div class="left-sidebar-content">
            <ul class="sidebar-elements">
              <li class="divider">Menu</li>
              <li class="active"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>"><i class=" "></i><span>Dashboard</span></a> </li>
              
              <?php 
			  //if the profile of the candidate is approved then allow him menu
			  if($Client->profile_approve == 'Yes'){ 
			  
			  //Menu for Administrator, MSP Coordinator, Main client 
			   
			  if($Client->type == 'Supper Client' || $Client->member_type == 'MSP Coordinator' || $Client->member_type == 'Administrator'){
			  ?>
              
              <li class="parent"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"><i class=" "></i><span>Jobs</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>">Jobs</a> </li>
                  
                  <?php if($Client->super_client_id==0){  ?>
                  	<li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/addjob'); ?>">Add Jobs</a> </li>
                     <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobtemplates'); ?>">Job Template</a> </li>
                  <?php } ?>
                  
                  <?php if($Client->super_client_id==0){  ?>
                  	<li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting1',array('tab'=>'pending')); ?>">Pending Approval</a> </li>
                  <?php } ?>
                </ul>
              </li>
              <li class="parent"><a href="#"><i class=" "></i><span>Submission</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/client/admin'); ?>">Submission</a> </li>
                </ul>
              </li>
              <li class="parent"><a href="#"><i class=""></i><span>Scheduled Interview</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/interviewList'); ?>">List of Interview</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/interview/interviewCancelled'); ?>">Cancelled Interview</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/interviewStep1'); ?>">New Appointments</a> </li>
                </ul>
              </li>

            <li class="parent"><a href=""><i class=""></i><span>Offer</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/offer'); ?>">Offers</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Create Offer</a> </li>
                </ul>
              </li>

              <li class="parent"><a href=""><i class=""></i><span>Work Order</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/workorderList'); ?>">Work Orders</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/workorderStep1'); ?>">Create Work Order</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/pendingWorkorder'); ?>">Pending Approval</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/rejectedWorkorder'); ?>">Reject Work Order</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/onBoardingList'); ?>">On Boarding</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/ofBoardingList'); ?>">Off Boarding</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/approvedworkers'); ?>">Workers</a> </li>
                  
                
                  
                </ul>
              </li>
              <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/timesheetList'); ?>">Time Sheets</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/approvedTimesheet'); ?>">Approved Time Sheet</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/pendingTimesheet'); ?>">Pending Approval</a> </li>
                </ul>
              </li>
              <li class="parent"><a href="#"><i class=""></i><span>App Settings</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/index'); ?>">Company Profile</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/changPassword'); ?>">Change Password</a> </li>
                  <?php /** ?><li><a href="#">Expenses</a> </li><?php **/ ?>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createdepartment'); ?>">Department</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/createteammember'); ?>">Team Members</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createjobrequest'); ?>">Job Request</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createbillingcode'); ?>">Bill Code</a> </li>
                  <?php /** ?><li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Work Order</a> </li><?php **/ ?>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/createlocation'); ?>">Location</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/costCenter/create'); ?>">Cost Center</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/project/create'); ?>">Projects</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/workflows'); ?>">Workflow</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/settingExhibit'); ?>">Exhibit</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/backgroundverification'); ?>">Backgroud Verification</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/logReport'); ?>">Log Report</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/settinggeneral/extension'); ?>">Extensions</a> </li>
                </ul>
              </li>
              <li class="parent"><a href="#"><i class=""></i><span>My Profile</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/editProfile'); ?>">Update Profile</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/settinggeneral/changPassword'); ?>">Change Password</a> </li>
                </ul>
              </li>
              
              <?php 
			  //Procurement Manager menu restriction
			  }else if($Client->member_type == 'Procurement Manager'){ ?>
              
              <li class="parent"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"><i class=" "></i><span>Jobs</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>">Jobs</a> </li>
                </ul>
              </li>

              <li class="parent"><a href=""><i class=""></i><span>Offer</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/offer'); ?>">Offers</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Create Offer</a> </li>
                </ul>
              </li>

              <li class="parent"><a href=""><i class=""></i><span>Work Order</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/offer'); ?>">Work Orders</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Create Work Order</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/pendingWorkorder'); ?>">Pending Approval</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/rejectedWorkorder'); ?>">Reject Work Order</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/onBoardingList'); ?>">On Boarding</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/ofBoardingList'); ?>">Off Boarding</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/approvedworkers'); ?>">Workers</a> </li>
                  
                
                  
                </ul>
              </li>
              <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/timesheetList'); ?>">Time Sheets</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/approvedTimesheet'); ?>">Approved Time Sheet</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/pendingTimesheet'); ?>">Pending Approval</a> </li>
                </ul>
              </li>
              
               <?php 
			  //HR Manager menu restriction
			  }else if($Client->member_type == 'HR Manager'){ ?>
              
              <li class="parent"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"><i class=" "></i><span>Jobs</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>">Jobs</a> </li>
                  
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobtemplates'); ?>">Job Template</a> </li>
                  
                </ul>
              </li>
              
              <?php 
			  //Finance Manager menu restriction
			  }else if($Client->member_type == 'Finance Manager'){ ?>
              
              <li class="parent"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"><i class=" "></i><span>Jobs</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>">Jobs</a> </li>
                </ul>
              </li>
              
              <li class="parent"><a href="#"><i class=" "></i><span>Submission</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/client/admin'); ?>">Submission</a> </li>
                </ul>
              </li>


              <li class="parent"><a href=""><i class=""></i><span>Offer</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/offer'); ?>">Offers</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Create Offer</a> </li>
                </ul>
              </li>
              
              <li class="parent"><a href=""><i class=""></i><span>Work Order</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/offer'); ?>">Work Orders</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Create Work Order</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/pendingWorkorder'); ?>">Pending Approval</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/rejectedWorkorder'); ?>">Reject Work Order</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/onBoardingList'); ?>">On Boarding</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/ofBoardingList'); ?>">Off Boarding</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/approvedworkers'); ?>">Workers</a> </li>
                  
                
                  
                </ul>
              </li>
              
              <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/timesheetList'); ?>">Time Sheets</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/approvedTimesheet'); ?>">Approved Time Sheet</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/pendingTimesheet'); ?>">Pending Approval</a> </li>
                </ul>
              </li>
              
              <?php 
			  //Time Sheet Manager menu restriction
			  }else if($Client->member_type == 'Time Sheet Manager'){ ?>
              
              <li class="parent"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"><i class=" "></i><span>Jobs</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>">Jobs</a> </li>
                  
                  <?php if($Client->super_client_id==0){  ?>
                  	<li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting1',array('tab'=>'pending')); ?>">Pending Approval</a> </li>
                  <?php } ?>
                </ul>
              </li>
              <li class="parent"><a href=""><i class=""></i><span>Offer</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/offer'); ?>">Offers</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Create Offer</a> </li>
                </ul>
              </li>
              
              <li class="parent"><a href=""><i class=""></i><span>Work Order</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/offer'); ?>">Work Orders</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/step1'); ?>">Create Work Order</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/pendingWorkorder'); ?>">Pending Approval</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/rejectedWorkorder'); ?>">Reject Work Order</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/onBoardingList'); ?>">On Boarding</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/ofBoardingList'); ?>">Off Boarding</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/approvedworkers'); ?>">Workers</a> </li>
                  
                
                  
                </ul>
              </li>
              
              <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/timesheetList'); ?>">Time Sheets</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/approvedTimesheet'); ?>">Approved Time Sheet</a> </li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/pendingTimesheet'); ?>">Pending Approval</a> </li>
                </ul>
              </li>
              
              <?php 
			  //Time Sheet Manager menu restriction
			  }else if($Client->member_type == 'Department Manager'){ ?>
              
              <li class="parent"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"><i class=" "></i><span>Jobs</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>">Jobs</a> </li>
                  
                  <?php if($Client->super_client_id==0){  ?>
                  	<li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/addjob'); ?>">Add Jobs</a> </li>
                     <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/jobtemplates'); ?>">Job Template</a> </li>
                  <?php } ?>
                  
                  <?php if($Client->super_client_id==0){  ?>
                  	<li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting1',array('tab'=>'pending')); ?>">Pending Approval</a> </li>
                  <?php } ?>
                </ul>
              </li>
              
              <?php }else{ ?>
              
              	<li class="parent"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>"><i class=" "></i><span>Jobs</span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting'); ?>">Jobs</a> </li>
                  
                  <?php if($Client->super_client_id==0){  ?>
                  	<li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/addjob'); ?>">Add Jobs</a> </li>
                  <?php } ?>
                  
                  <?php if($Client->super_client_id==0){  ?>
                  	<li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/joblisting1',array('tab'=>'pending')); ?>">Pending Approval</a> </li>
                  <?php } ?>
                </ul>
              </li>
              
              <?php } }else{
				//Menu before the approval of the account
				  
				if($Client->type == 'Super Client' || $Client->member_type == 'MSP Coordinator' || $Client->member_type == 'Administrator'){
				   ?>
                <li class="parent"><a href="#"><i class=""></i><span>Jobs</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Submission</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Scheduled Interview</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Work Order</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Expenses</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Invoice</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>App Settings</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>My Profile</span></a></li>
              
               <?php 
			  //Procurement Manager menu restriction
			  }else if($Client->member_type == 'Procurement Manager'){ ?>
              
              	<li class="parent"><a href="#"><i class=""></i><span>Jobs</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Work Order</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a></li>
              
               <?php 
			  //HR Manager menu restriction
			  }else if($Client->member_type == 'HR Manager'){ ?>
              
              <li class="parent"><a href="#"><i class=""></i><span>Jobs</span></a></li>
              
              <?php 
			  //Finance Manager menu restriction
			  }else if($Client->member_type == 'Finance Manager'){ ?>
              
              	<li class="parent"><a href="#"><i class=""></i><span>Jobs</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Work Order</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Expenses</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Invoice</span></a></li>
              
              <?php 
			  //Time Sheet Manager menu restriction
			  }else if($Client->member_type == 'Time Sheet Manager'){ ?>
              
              	<li class="parent"><a href="#"><i class=""></i><span>Jobs</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Work Order</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a></li>
              
              
              <?php 
			  //Time Sheet Manager menu restriction
			  }else if($Client->member_type == 'Department Manager'){ ?>
              
              <li class="parent"><a href="#"><i class=""></i><span>Jobs</span></a></li>              
              
              <?php }else{ ?>
              
              
              	<li class="parent"><a href="#"><i class=""></i><span>Jobs</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Submission</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Scheduled Interview</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Work Order</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Time Sheets</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Expenses</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>Invoice</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>App Settings</span></a></li>
                <li class="parent"><a href="#"><i class=""></i><span>My Profile</span></a></li>
              
              
              <?php } } ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="be-content">
    <?php echo $content; ?>
  </div>
  <nav class="be-right-sidebar">
    <div class="sb-content">
      <div class="tab-navigation">
        <ul role="tablist" class="nav nav-tabs nav-justified">
          <li role="presentation" class="active"><a href="#tab1" aria-controls="chat" role="tab" data-toggle="tab">Chat</a></li>
          <li role="presentation"><a href="#tab2" aria-controls="todo" role="tab" data-toggle="tab">Todo</a></li>
          <li role="presentation"><a href="#tab3" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
        </ul>
      </div>
      <div class="tab-panel">
        <div class="tab-content">
          <div id="tab1" role="tabpanel" class="tab-pane tab-chat active">
            <div class="chat-contacts">
              <div class="chat-sections">
                <div class="be-scroller">
                  <div class="content">
                    <h2>Recent</h2>
                    <div class="contact-list contact-list-recent">
                      <div class="user"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar1.png">
                        <div class="user-data"><span class="status away"></span><span class="name">Claire Sassu</span><span class="message">Can you share the...</span></div>
                        </a></div>
                      <div class="user"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar2.png">
                        <div class="user-data"><span class="status"></span><span class="name">Maggie jackson</span><span class="message">I confirmed the info.</span></div>
                        </a></div>
                      <div class="user"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar3.png">
                        <div class="user-data"><span class="status offline"></span><span class="name">Joel King </span><span class="message">Ready for the meeti...</span></div>
                        </a></div>
                    </div>
                    <h2>Contacts</h2>
                    <div class="contact-list">
                      <div class="user"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar4.png">
                        <div class="user-data2"><span class="status"></span><span class="name">Mike Bolthort</span></div>
                        </a></div>
                      <div class="user"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar5.png">
                        <div class="user-data2"><span class="status"></span><span class="name">Maggie jackson</span></div>
                        </a></div>
                      <div class="user"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar6.png">
                        <div class="user-data2"><span class="status offline"></span><span class="name">Jhon Voltemar</span></div>
                        </a></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="bottom-input">
                <input type="text" placeholder="Search..." name="q">
                <span class="mdi mdi-search"></span> </div>
            </div>
            <div class="chat-window">
              <div class="title">
                <div class="user"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/img/avatar2.png">
                  <h2>Maggie jackson</h2>
                  <span>Active 1h ago</span> </div>
                <span class="icon return mdi mdi-chevron-left"></span> </div>
              <div class="chat-messages">
                <div class="be-scroller">
                  <div class="content">
                    <ul>
                      <li class="friend">
                        <div class="msg">Hello</div>
                      </li>
                      <li class="self">
                        <div class="msg">Hi, how are you?</div>
                      </li>
                      <li class="friend">
                        <div class="msg">Good, I'll need support with my pc</div>
                      </li>
                      <li class="self">
                        <div class="msg">Sure, just tell me what is going on with your computer?</div>
                      </li>
                      <li class="friend">
                        <div class="msg">I don't know it just turns off suddenly</div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="chat-input">
                <div class="input-wrapper"><span class="photo mdi mdi-camera"></span>
                  <input type="text" placeholder="Message..." name="q" autocomplete="off">
                  <span class="send-msg mdi mdi-mail-send"></span> </div>
              </div>
            </div>
          </div>
          <div id="tab2" role="tabpanel" class="tab-pane tab-todo">
            <div class="todo-container">
              <div class="todo-wrapper">
                <div class="be-scroller">
                  <div class="todo-content"><span class="category-title">Today</span>
                    <ul class="todo-list">
                      <li>
                        <div class="be-checkbox be-checkbox-sm"><span class="delete mdi mdi-delete"></span>
                          <input id="todo1" type="checkbox" checked="">
                          <label for="todo1">Initialize the project</label>
                        </div>
                      </li>
                      <li>
                        <div class="be-checkbox be-checkbox-sm"><span class="delete mdi mdi-delete"></span>
                          <input id="todo2" type="checkbox">
                          <label for="todo2">Create the main structure</label>
                        </div>
                      </li>
                      <li>
                        <div class="be-checkbox be-checkbox-sm"><span class="delete mdi mdi-delete"></span>
                          <input id="todo3" type="checkbox">
                          <label for="todo3">Updates changes to GitHub</label>
                        </div>
                      </li>
                    </ul>
                    <span class="category-title">Tomorrow</span>
                    <ul class="todo-list">
                      <li>
                        <div class="be-checkbox be-checkbox-sm"><span class="delete mdi mdi-delete"></span>
                          <input id="todo4" type="checkbox">
                          <label for="todo4">Initialize the project</label>
                        </div>
                      </li>
                      <li>
                        <div class="be-checkbox be-checkbox-sm"><span class="delete mdi mdi-delete"></span>
                          <input id="todo5" type="checkbox">
                          <label for="todo5">Create the main structure</label>
                        </div>
                      </li>
                      <li>
                        <div class="be-checkbox be-checkbox-sm"><span class="delete mdi mdi-delete"></span>
                          <input id="todo6" type="checkbox">
                          <label for="todo6">Updates changes to GitHub</label>
                        </div>
                      </li>
                      <li>
                        <div class="be-checkbox be-checkbox-sm"><span class="delete mdi mdi-delete"></span>
                          <input id="todo7" type="checkbox">
                          <label for="todo7" title="This task is too long to be displayed in a normal space!">This task is too long to be displayed in a normal space!</label>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="bottom-input">
                <input type="text" placeholder="Create new task..." name="q">
                <span class="mdi mdi-plus"></span> </div>
            </div>
          </div>
          <div id="tab3" role="tabpanel" class="tab-pane tab-settings">
            <div class="settings-wrapper">
              <div class="be-scroller"><span class="category-title">General</span>
                <ul class="settings-list">
                  <li>
                    <div class="switch-button switch-button-sm">
                      <input type="checkbox" checked="" name="st1" id="st1">
                      <span>
                      <label for="st1"></label>
                      </span> </div>
                    <span class="name">Available</span> </li>
                  <li>
                    <div class="switch-button switch-button-sm">
                      <input type="checkbox" checked="" name="st2" id="st2">
                      <span>
                      <label for="st2"></label>
                      </span> </div>
                    <span class="name">Enable notifications</span> </li>
                  <li>
                    <div class="switch-button switch-button-sm">
                      <input type="checkbox" checked="" name="st3" id="st3">
                      <span>
                      <label for="st3"></label>
                      </span> </div>
                    <span class="name">Login with Facebook</span> </li>
                </ul>
                <span class="category-title">Notifications</span>
                <ul class="settings-list">
                  <li>
                    <div class="switch-button switch-button-sm">
                      <input type="checkbox" name="st4" id="st4">
                      <span>
                      <label for="st4"></label>
                      </span> </div>
                    <span class="name">Email notifications</span> </li>
                  <li>
                    <div class="switch-button switch-button-sm">
                      <input type="checkbox" checked="" name="st5" id="st5">
                      <span>
                      <label for="st5"></label>
                      </span> </div>
                    <span class="name">Project updates</span> </li>
                  <li>
                    <div class="switch-button switch-button-sm">
                      <input type="checkbox" checked="" name="st6" id="st6">
                      <span>
                      <label for="st6"></label>
                      </span> </div>
                    <span class="name">New comments</span> </li>
                  <li>
                    <div class="switch-button switch-button-sm">
                      <input type="checkbox" name="st7" id="st7">
                      <span>
                      <label for="st7"></label>
                      </span> </div>
                    <span class="name">Chat messages</span> </li>
                </ul>
                <span class="category-title">Workflow</span>
                <ul class="settings-list">
                  <li>
                    <div class="switch-button switch-button-sm">
                      <input type="checkbox" name="st8" id="st8">
                      <span>
                      <label for="st8"></label>
                      </span> </div>
                    <span class="name">Deploy on commit</span> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/jquery/jquery.min.js" type="text/javascript"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>

 
<?php /*?><script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/js/main.js" type="text/javascript"></script><?php */?>

<script src="http://demobetademo.online/clientportal/client/assets/js/main.js" type="text/javascript"></script>

 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script> 


<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>



<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap-tokenfield/dist/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap-tokenfield/dist/bootstrap-tokenfield.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap3-wysihtml5-master/wysihtml5-0.3.0.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap3-wysihtml5-master/bootstrap3-wysihtml5.js"></script>


<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/jquery-ui-1.12.0/jquery-ui.js"></script>



<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/morrisjs/morris.css"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/raphael/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/morrisjs/morris.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/js/app-charts-morris.js" type="text/javascript"></script>









<?php	 
	$skill = Skills::model()->findAll();
	$skills = array();
	//$skills1 = array();
	foreach($skill as $skill){
		$skills[] = "'$skill->name'";
		//$skills1[] = "'$skill->name'";
		}
 ?>
 <?php
 $loginUserId = Yii::app()->user->id;
 $firstdate = date('Y-m-d');
 $seconddate = date('Y-m-d',strtotime("-1 days"));
 $thireddate = date('Y-m-d',strtotime("-2 days"));
 $fourthdate = date('Y-m-d',strtotime("-3 days"));
 $fifthdate = date('Y-m-d',strtotime("-4 days"));
 $sixthdate = date('Y-m-d',strtotime("-5 days"));
 $sevendate = date('Y-m-d',strtotime("-6 days"));
 $firstsubbmission = VendorJobSubmission::model()->countByAttributes(array('client_id'=>$loginUserId,'date_created'=>$firstdate));
 $secondsubbmission = VendorJobSubmission::model()->countByAttributes(array('client_id'=>$loginUserId,'date_created'=>$seconddate));
 $thiredsubbmission = VendorJobSubmission::model()->countByAttributes(array('client_id'=>$loginUserId,'date_created'=>$thireddate));
 $fourthsubbmission = VendorJobSubmission::model()->countByAttributes(array('client_id'=>$loginUserId,'date_created'=>$fourthdate));
 $fifthsubbmission = VendorJobSubmission::model()->countByAttributes(array('client_id'=>$loginUserId,'date_created'=>$fifthdate));
 $sixthsubbmission = VendorJobSubmission::model()->countByAttributes(array('client_id'=>$loginUserId,'date_created'=>$sixthdate));
 $sevensubbmission = VendorJobSubmission::model()->countByAttributes(array('client_id'=>$loginUserId,'date_created'=>$sevendate));
 ?>
<script>
$('#job_skills,#job_skills1,#job_skills2').tokenfield({
    autocomplete: {
	  source: [<?php echo implode(",", $skills); ?>],
    
      //delay: 100
    },
    showAutocompleteOnFocus: false,
	delimiter: [',','-', '_']
  });
  </script>




  <?php /** ?>
  
<script>
 $('#job_skills1').tokenfield({
    autocomplete: {
	  source: [<?php echo implode(",", $skills); ?>],
     // delay: 100
    },
    showAutocompleteOnFocus: true,
	delimiter: [',','-', '_']
  });
  </script>
  
<script>
  $('#job_skills2').tokenfield({
    autocomplete: {
	  source: [<?php echo implode(",", $skills); ?>],
      //delay: 100
    },
    showAutocompleteOnFocus: true,
	delimiter: [',','-', '_']
  });
  
</script>

<?php **/ ?>


<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/canvasjs/jquery.canvasjs.min.js"></script>
     
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/jquery-flot/jquery.flot.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/jquery-flot/jquery.flot.resize.js" type="text/javascript"></script>


<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/lib/jquery-flot/plugins/curvedLines.js" type="text/javascript"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/js/app-charts.js" type="text/javascript"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme-assets/assets/plugins/jquery.flot.time.js" type="text/javascript"></script>



<script type="text/javascript">  
  //rating code
  $(function() {
    /// target element
    //var el = document.getElementById('#el');

    var el = document.getElementsByClassName("rating-star"); 
    console.log(el)

    $(el).each(function(index, el) {
      // statements
      // current rating, or initial rating
      var currentRating = Math.floor((Math.random() * 5) + 1);

      // max rating, i.e. number of s$tars you want
      var maxRating= 5;

      // callback to run after setting the rating
      var callback = function(rating) { alert(rating); };

      // rating instance

      var myRating = rating(el, currentRating, maxRating, callback);
    });
    
    
  });

</script>
<script type="text/javascript">
  jQuery('#log-date-range').daterangepicker({
    toggleActive: true,
    orientation: "top auto"
  });
</script>

<script>
  var c = 0; max_count = 60; logout = true;
  startTimer();

  function startTimer(){
    var session = parseInt(<?php echo Yii::app()->getSession()->getTimeout(); ?>);
    session = session-60;
    setTimeout(function(){
      logout = true;
      c = 0;
      max_count = 60;
      $('#timer').html(max_count);
      $('#logout_popup').modal('show');
      startCount();

    }, 1000*session);
  }

  function resetTimer(){
    logout = false;
    $('#logout_popup').modal('hide');
    startTimer();
  }

  function timedCount() {
    c = c + 1;
    remaining_time = max_count - c;
    if( remaining_time == 0 && logout ){
      $('#logout_popup').modal('hide');
      location.href = '<?php echo Yii::app()->createAbsoluteUrl('/Client/default/login'); ?>';

    }else{
      $('#timer').html(remaining_time);
      t = setTimeout(function(){timedCount()}, 1000);
    }
  }

  function startCount() {
    timedCount();
  }
</script>


<div class="modal fade" id="logout_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div style="width:100%;height:100%;margin: 0px; padding:0px">
          <div style="width:25%;margin: 0px; padding:0px;float:left;">
            <i class="fa fa-warning" style="font-size: 140px;color:#da4f49"></i>
          </div>
          <div style="width:70%;margin: 0px; padding:0px;float:right;padding-top: 10px;padding-left: 3%;">
            <h4>Your session is about to expire!</h4>
            <p style="font-size: 15px;">You will be logged out in <span id="timer" style="display: inline;font-size: 30px;font-style: bold">10</span> seconds.</p>
            <p style="font-size: 15px;">Do you want to stay signed in?</p>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div style="margin-left: 30%;margin-bottom: 20px;margin-top: 20px;">
        <?php
        $curpage = Yii::app()->getController()->getAction()->controller->id;
        $curpage .= '/'.Yii::app()->getController()->getAction()->controller->action->id;
        ?>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/'.$curpage);?>" class="btn btn-primary" aria-hidden="true">Yes, Keep me signed in</a>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('/Client/default/logout');?>" class="btn btn-danger" aria-hidden="true">No, Sign me out</a>
      </div>
    </div>
  </div>
</div>

</body>
</html>