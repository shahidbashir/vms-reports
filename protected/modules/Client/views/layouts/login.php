<!doctype html>
              <html lang="en">
              <head>
                  <meta charset="utf-8"/>
                  <meta http-equiv="X-UA-Compatible" content="IE=edge">
                  <meta name="description" content=""/>
                  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1"/>
                  <meta name="msapplication-tap-highlight" content="no">

                  <meta name="mobile-web-app-capable" content="yes">
                  <meta name="application-name" content="Milestone">

                  <meta name="apple-mobile-web-app-capable" content="yes">
                  <meta name="apple-mobile-web-app-status-bar-style" content="black">
                  <meta name="apple-mobile-web-app-title" content="Milestone">

                  <meta name="theme-color" content="#4C7FF0">

                  <title>VMS</title>

                  <!-- page stylesheets -->
                  <!-- end page stylesheets -->

                  <!-- build:css({.tmp,app}) styles/app.min.css -->
                  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap/dist/css/bootstrap.css"/>
                  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/font-awesome/css/font-awesome.css"/>
                  <link href="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/css/style.css" rel="stylesheet">
                  <!-- endbuild -->
              </head>
              <body styel="background: #FBFBFB;">


              <div class="login-form">
                  <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php echo $content; ?>
                      </div>
                  </div> <!-- col -->
          </div> <!-- row -->
              </body>
            <!-- build:js({.tmp,app}) scripts/app.min.js -->
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/jquery/dist/jquery.js"></script>

            <script src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/template-assets/vendor/bootstrap/dist/js/bootstrap.js"></script>

            <!-- endbuild -->



</html>