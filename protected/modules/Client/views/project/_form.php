<?php $this->renderPartial('/settinggeneral/appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
	<div class="cleafix " style="padding: 30px 20px; ">
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
			<h4 class="m-b-10">Project</h4>
			<p class="m-b-40"></p>

			<?php if(Yii::app()->user->hasFlash('success')):?>
				<?php echo Yii::app()->user->getFlash('success'); ?>
			<?php endif; ?>

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'project-form',
				// Please note: When you enable ajax validation, make sure the corresponding
				// controller action is handling ajax validation correctly.
				// There is a call to performAjaxValidation() commented in generated controller code.
				// See class documentation of CActiveForm for details on this.
				'enableAjaxValidation'=>false,
			)); ?>
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="">Project Name</label>
							<?php echo $form->textField($model,'project_name',array('class'=>'form-control')); ?>
							<?php echo $form->error($model,'project_name'); ?>
						</div>
					</div>
					<!-- col -->
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="">Select Cost Center</label>
							<?php
							$list = CHtml::listData(CostCenter::model()->findAllByAttributes(array('client_id'=>Yii::app()->user->id)),'id', 'cost_code');
							echo $form->dropDownList($model, 'cost_center', $list, array('class'=>'form-control','empty' => '','required'=>'required')); ?>
						</div>
					</div>
					<!-- col -->
				</div>
				<!-- row -->
				<br>
				<button type="submit" class="btn btn-success">Add</button>
				<a href="" class="btn btn-default">Cancel</a>
			<?php $this->endWidget(); ?>



		</div> <!-- col -->
		
	</div> <!-- row -->
	<div class="clearfix p-20">

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<h4 class="m-b-30 table-heading">List of Project</h4>
			<?php if($modelProject){ ?>
			<table class="table m-b-40 without-border">
				<thead class="thead-default">
				<tr>
					<th>Project Name:</th>
					<th>Cost Center Cost</th>
					<th class="text-center">Action</th>
				</tr>
				</thead>
				<tbody>
				<?php
				foreach($modelProject as $value){
				$costCenter = CostCenter::model()->findByPk($value['cost_center']);
				?>
				<tr>
					<td>
						<?php echo $value['project_name']; ?>
					</td>

					<td>
						$ <?php echo $costCenter->cost_code; ?>
					</td>
					<td style="text-align: center">
						<a href="<?php echo Yii::app()->createAbsoluteUrl('Client/project/delete',array('id'=>$value['id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
				<?php } ?>
				</tbody>
			</table>
			<?php } ?>
		</div> <!-- col -->
	</div> <!-- row -->
	<div class="seprater-bottom-100"></div>
</div>
