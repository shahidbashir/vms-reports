<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<h2>Authorized User(s)</h2>
<?php if(!empty($users)) : ?>
    <ul>
        <?php foreach($users as $user) : ?>
            <li><?=str_replace('.txt', '', $user);?></li>
        <?php endforeach; ?>
    </ul>
    <?=CHtml::link('Add Event to all Authorized users', $this->createAbsoluteUrl('offlineAccess/add')); ?>
    <br/><br/>
<?php endif; ?>
<?=CHtml::link('Click here to Authorize Access', oAuthService::getLoginUrl($this->createAbsoluteUrl('offlineAccess/authorize'))); ?>