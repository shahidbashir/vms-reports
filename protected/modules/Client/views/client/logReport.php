<?php $this->renderPartial('/settinggeneral/appsettingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px; ">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h4 class="m-b-10">Log Report</h4>
            <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>

            <form id="client-form" action="" method="post">
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="">Select Range</label>
                            <input type="text" name="daterange" id="input" class="form-control drp" value="" required="required">
                        </div>
                    </div>

                </div>
                <!-- row -->
                <br>
                <button type="submit" name="Logreport" class="btn btn-success">Submit</button>
            </form>

        </div> <!-- col -->
        
    </div> <!-- row -->
    <div class="clearfix p-20">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-b-30 table-heading">List of Job Alerts</h4>
            <table class="table m-b-40 without-border">
                <thead class="thead-default">
                <tr>
                    <th>S.No</th>
                    <th>Time  In</th>
                    <th>Time Out</th>
                    <th>Name of User</th>
                    <th>User Permission</th>
                    <th>IP Address</th>
                    <th>Country</th>
                </tr>
                </thead>
                <tbody>
                <?php if($LogReport) {
                $i = 0;
                foreach ($LogReport as $data) {
                $i++;
                ?>
                <tr>
                    <th><?php echo $i; ?></th>
                    <td><?php echo $data->time_in; ?></td>
                    <td><?php echo $data->time_out; ?></td>
                    <td><?php echo $data->first_name . ' ' . $data->last_name; ?></td>
                    <td><?php
                        if ($data->user_permission == '') {
                            echo 'Super Client';
                        } else {
                            echo $data->user_permission;
                        } ?></td>
                    <td><?php echo $data->ip_address; ?></td>
                    <td><?php echo $data->country; ?></td>


                </tr>
                <?php }
                }else{ ?>
                    <tr><td colspan="7" style="text-align: center;"><h2>No Result found</h2></td></tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div> <!-- col -->
    </div> <!-- row -->

    <div class="seprater-bottom-100"></div>
</div>