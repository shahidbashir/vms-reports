<div class="db-content-wraper">

			<div class="db-sidebar">
					  <ul class="list-unstyled">
					  	
					  	<li class="dropdown">
					  		<a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-1"></i></a>
					  	</li>
					  	<li class="dropdown">
					  		<a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-2"></i></a>
					  		
					  	</li>
					  
					  	<li class="dropdown">
					  		<a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-3"></i></a>
					  		
					  	</li>
					  
					  	<li class="dropdown">
					  		<a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-4"></i></a>
					  		<ul class="dropdown-menu">
					  			<li><a href="#">Today Interviews</a></li>
					  			<li><a href="#">Schedule a Interviews</a></li>
					  			<li><a href="#">Pending Interviews</a></li>
					  		</ul>
					  	</li>
					  
					  	<li class="dropdown">
					  		<a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-5"></i></a>
					  		<ul class="dropdown-menu">
					  			<li><a href="#">TimeSheets</a></li>
					  			<li><a href="#">Add Timesheet</a></li>
					  		</ul>
					  	</li>
					  
					  	<li class="dropdown">
					  		<a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-6"></i></a>
					  		<ul class="dropdown-menu">
					  			<li><a href="#">All Offers & Contracts</a></li>
					  			<li><a href="#">Create Offers & Contracts</a></li>
					  		</ul>
					  	</li>
					  
					  	<li class="dropdown">
					  		<a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-7"></i></a>
					  		<ul class="dropdown-menu">
					  			<li><a href="#">Time Sheet</a></li>
					  			<li><a href="#">Invoice & Payments</a></li>
					  			<li><a href="#">Renewal & Release</a></li>
					  		</ul>
					  	</li>
					  
					  </ul>
					  
					  <a  href="" class="setting"><i class=""></i></a>
					  
			</div> <!-- db-sidebar -->

			<div class="db-content-inner">
				
				<div class="db-content">

					<div class="db-g-header">
						<h4></h4>
						<a href="job-listing.html">Back to Job Listing</a>
					</div>



					<div class="jobs">
						

						<div class="job">
							<div class="job-title">
								<h4><a href="">CL-35485 Senior UX Designer - NYC</a></h4>
								<p>Location: NY, Dallas.</p>
							</div>
							<div class="job-meta">
								<div class="top row no-margin">
									<div class="top-inner">
										<div class="col-xs-7 no-padding">
												<div class="label label-green">A</div>
												<span>23 Submission</span>
												<div class="salary">
													$45.00 - $55.00
												</div>
										</div>

										<div class="col-xs-5 no-padding">
											<ul class="list-inline pull-right">
												<li><a href="">Edit</a></li>
												<li><a href="">Delete</a></li>
											</ul>
										</div>
									</div>
									

								</div>
								<div class="bottom clearfix">
									<div class="job-post">Account Manager: Blake William</div>
									<a href="" class="pull-right">2 Openigns</a>
								</div>
							</div>
						</div> <!-- job -->

					</div> <!-- jobs -->

				
					<div class="submision-wraper">

						<div class="jb-list-wp__nav-wraper">
							
							<nav class="navbar navbar-default" role="navigation">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse navbar-ex1-collapse">
									<ul class="nav navbar-nav">
										<li class="active">
												<a href="job-listing-workspace.html">Overview</a>
											</li><li>
												<a href="job-listing-workspace-submission.html">Submission</a>
											</li><li>
												<a href="job-workspace-intreview.html">Interviews</a>
											</li><li>
												<a href="job-listing-workspace-pipeline.html">Recruitment Pipeline </a>
											</li><li>
												<a href="client-workspace-jobdetails.html">Job Details </a>
											</li><li>
												<a href="job-listing-workspace-rates.html">Rates</a>
											</li><li>
												<a href="job-listing-workspace-notescomment.html">Notes & Comments</a>
											</li><li>
												<a href="#">Offer & Contracts</a>
											</li>
											<li>
												<a href="#"><i class="icon-2"></i></i></i>+</a>
											</li>
									</ul>
									
								</div><!-- /.navbar-collapse -->
							</nav>
							
						</div> <!-- jb-list-wp__nav-wraper -->
							
						 <div class="j-w-teammenber_header">
						 	<a href="" data-toggle="modal" data-target="#invite-teammeber">Invite Team Members</a>
						 </div>

							<div class="j-w-teammenber_wraper">
								
								<div class="tag">
									<h4>Client Teams</h4>
								</div>

								<div class="j-w-teammenber_cards">
									<div class="row">
										
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<div class="j-w-teammenber_card">
												<div class="media">
													<a class="pull-left" href="#">
														<img class="media-object" src="assets/images/avatar-3.png" alt="Image">
													</a>
													<div class="media-body">
														<h4 class="media-heading">Jony Sacks</h4>
														<p>5874-85698</p>
														<p><span class="label label-info">Active</span></p>
														<p>example@email.com</p>
													</div>
												</div>
											</div>
										</div> <!-- col -->

										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<div class="j-w-teammenber_card">
												<div class="media">
													<a class="pull-left" href="#">
														<img class="media-object" src="assets/images/avatar-3.png" alt="Image">
													</a>
													<div class="media-body">
														<h4 class="media-heading">Jony Sacks</h4>
														<p>5874-85698</p>
														<p><span class="label label-info">Active</span></p>
														<p>example@email.com</p>
													</div>
												</div>
											</div>
										</div> <!-- col -->


									</div> <!-- row -->

								</div> <!-- j-w-teammenber_cards -->
								
								<div class="tag">
										<h4>Account Managers</h4>
								</div>
				

								<div class="j-w-teammenber_cards">
									<div class="row">
										
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<div class="j-w-teammenber_card">
												<div class="media">
													<a class="pull-left" href="#">
														<img class="media-object" src="assets/images/avatar-3.png" alt="Image">
													</a>
													<div class="media-body">
														<h4 class="media-heading">Jony Sacks</h4>
														<p>5874-85698</p>
														<p><span class="label label-info">Active</span></p>
														<p>example@email.com</p>
													</div>
												</div>
											</div>
										</div> <!-- col -->

										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<div class="j-w-teammenber_card">
												<div class="media">
													<a class="pull-left" href="#">
														<img class="media-object" src="assets/images/avatar-3.png" alt="Image">
													</a>
													<div class="media-body">
														<h4 class="media-heading">Jony Sacks</h4>
														<p>5874-85698</p>
														<p><span class="label label-info">Active</span></p>
														<p>example@email.com</p>
													</div>
												</div>
											</div>
										</div> <!-- col -->


									</div> <!-- row -->

								</div> <!-- j-w-teammenber_cards -->


							</div> <!-- j-w-teammenber_wraper -->

						
					</div> <!-- submision-wraper -->

					
					
				</div> <!-- db-content -->

			</div> <!-- db-content-inner -->
			
	</div> <!-- db-content-wraper -->