<div class="page-head">

          <h2 class="page-head-title">Step 2 - Invite Team Members</h2>

          <ol class="breadcrumb page-head-nav">

            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>">Dashbord</a></li>

            

            <li class="active">Step 2 - Invite Team Members</li>

          </ol>

        </div>

        <div class="main-content">

        
        <?php if(Yii::app()->user->hasFlash('success')):?>

            <?php echo Yii::app()->user->getFlash('success'); ?>

        <?php endif; ?>
         

            <!--Condensed Table-->

           

           



              <!--Hover table-->

            

           <div class="row">

            <div class="col-md-12">

              <div class="panel panel-border-color panel-border-color-primary">

                

                <div class="panel-body">

                    

                  <div class="white-box">

                     

                     <div class="row m-0">

                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">

                           <div class="business-setup">

                              <div class="form-wraper">

                                 <br>    

                  <?php $form=$this->beginWidget('CActiveForm', array(

						'id'=>'client-form',

						'enableAjaxValidation'=>false,

					)); ?>

                    <div class="row">

                      <div class="col-xs-12 col-sm-6">

                        <label for="" class="">Your Full Name</label>

                        <div class="two-flieds">

                          <div class="form-group">

                          	<?php echo $form->textField($model,'first_name',array('class'=>'form-control','placeholder'=>'First Name')); ?>

							<?php //echo $form->error($model,'first_name'); ?>

                          </div>

                          <div class="form-group">

                          	<?php echo $form->textField($model,'last_name',array('class'=>'form-control','placeholder'=>'Last Name')); ?>

                            <?php //echo $form->error($model,'last_name'); ?>

                          </div>

                        </div>

                      </div>

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="">Email Address</label>

                          <?php echo $form->textField($model,'email',array('class'=>'form-control','required'=>true)); ?>

						  <?php //echo $form->error($model,'email'); ?>

                        </div>

                      </div>

                      <!-- col-12 --> 

                      

                    </div>

                    <!-- row -->

                    

                    <div class="row">

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="">Department</label>

                          <div class="single">

                          	<?php 

                            $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=4','order'=>'id DESC')),'title', 'title');

                            echo $form->dropDownList($model, 'department',$list, array('class'=>'ui fluid search dropdown selection form-control','empty' => '')); ?>

                          </div>

                        </div>

                      </div>

                      <!-- col-12 -->

                      

                      <div class="col-xs-12 col-sm-6">

                        <div class="form-group">

                          <label for="" class="">Member Type </label>

                          <div class="single">

                          	<?php 

                             $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=3')),'title', 'title');

                             echo $form->dropDownList($model, 'member_type',$list, array('class'=>'ui fluid search dropdown selection form-control','empty' => '')); ?>

                          </div>

                        </div>

                      </div>

                      <!-- col-12 --> 

                      

                    </div>

                    <!-- row -->

                    

                    <div class="button-group">

                    	<button type="submit" class="btn btn-success">SAVE &amp; CONTINUE</button>

                        <?php echo CHtml::link('SKIP &amp; CONTINUE',array('client/skipMembers'),array('class'=>'btn btn-default'));?>

                    </div>

                    <?php $this->endWidget(); ?>


                                 

                           </div>

                           <!-- business-setup -->

                        </div>

                  



                </div>

              </div>

            </div>

          

         

          </div>

            </div>







             <!--Hover table-->





                 <!--Hover table-->

               
<?php if($allTeam) { ?>
              <div class="row">

               <div class="col-md-12">

                 <div class="panel panel-default panel-table">

                   

                   <div class="panel-body">

                     

                      <div class="table-default">

                         

                              <h4 class="" style="margin:20px 20px;">Team Members</h4>

                         

                               <div class="table-responsive">

                                     <table class="table table-condensed table-striped">

                    

                   

                        <tbody>

                        	<?php foreach( $allTeam as $value ) { ?>

                            <tr>

                              <td><p><?php echo $value->first_name.' '.$value->last_name;?></p>

                                <span class="cell-detail-description"><?php echo $value->department;?></span></td>

                              <td><p><?php echo $value->email;?></p>

                                <span class="cell-detail-description"><?php echo $value->member_type;?></span></td>

                              <td style="text-align: right; padding-right: 30px;"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/deleteteammember',array('id'=>$value->id))?>" data-toggle="popover" data-placement="bottom" data-content="Delete" class="delete"><i class="fa fa-trash"></i>Delete</td>

                            </tr>

                         <?php } ?>

                         

                        </tbody>

                      </table>

                   
                         

                                </div>

                      </div>

                   



                   </div>

                 </div>

               </div>

             

             

             </div>

               </div>







                	<?php } ?>

              </div>

              <!-- business-setup --> 

            </div>

          

  </div>

  <!-- container --> 

  <br>

  <br>

  <br>

  <?php //$this->renderPartial('/default/footer'); ?>

</div>

</div>











