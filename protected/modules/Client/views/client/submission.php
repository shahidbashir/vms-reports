<?php $this->pageTitle =  'Submission';
$categoryID = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$jobModel->cat_id));
//Administrative/Clerical Jobs category id 71
//client id 41
$clientRateData = ClientRate::model()->findByAttributes(array('client_id'=>$jobModel->user_id));
/*echo '<pre>';	print_r($clientRateData);	exit;*/
$over_time = 0;	$half_time = 0; $double_time = 0;
if($clientRateData){
    $over_time = $clientRateData->over_time;
    $double_time = $clientRateData->double_time;
}
 $mark_up = $vendorSubmission->makrup;
 
//picking up bill rate value for the No markup job category.
$settingData = Setting::model()->findByAttributes(array('category_id'=>9,'title'=>$jobModel->cat_id));
$noMarkupCat = NoMarkupCate::model()->findByAttributes(array('category_id'=>$settingData->id));

if(!empty($noMarkupCat->bill_rate)){
	$billrate = $noMarkupCat->bill_rate;
	}else{
		 $billrate = $vendorSubmission->candidate_pay_rate + $vendorSubmission->candidate_pay_rate * ($mark_up)/100;
		 }
 
// $billrate = $vendorSubmission->candidate_pay_rate + $vendorSubmission->candidate_pay_rate * ($mark_up)/100;
 $billrate_overtime = $billrate + $billrate*$over_time/100;
 $billrate_doubletime = $billrate + $billrate*$double_time/100;
?>
<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php
      $date = date('h:i:s a m/d/Y', strtotime($vendorSubmission->date_created));
      $time = explode(' ',$date);
      ?>
      <h4 class="m-b-10"><?php echo $jobModel->title.'('.$jobModel->id.')';?> <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/admin'); ?>" class="btn btn-sm btn-default pull-right"> Back to Submission</a> </h4>
      <p class="m-b-30">Submmission was done on <?php echo date('F d,Y',strtotime($vendorSubmission->date_created));?> at <?php echo $time[0].' '.$time[1]; ?></p>
      <?php if(Yii::app()->user->hasFlash('success')):
                echo Yii::app()->user->getFlash('success');
            endif; ?>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <?php 
		  //if candidate is hired then don't show any of the below button
		  if(in_array($vendorSubmission->resume_status,array(8,9))){
			  //No button will be here
		  }else{
		  ?>
          <?php 
			  if(!in_array($vendorSubmission->resume_status,array(6,7))){ ?>
          <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/directStep2',array('id'=>$jobModel->id,'sub_id' =>$_GET['submission-id'])); ?>" class="btn btn-sm btn-success"> Schedule a Interview</a>
          <?php }if($vendorSubmission->resume_status != '6' ){ ?>
          <a data-toggle="modal" href='#modal-id' class="btn btn-sm btn-default-2"> Reject Candidate</a>
		  <?php }
		  
		  	if(in_array($vendorSubmission->resume_status,array(7,4,5))){  
			 ?>
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/createoffer',array('submission-id'=>$vendorSubmission->id)); ?>" class="btn btn-sm btn-success"> Create Offer</a>
          <?php }
          if($vendorSubmission->resume_status=='9'){ ?>
            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/offer/createworkOrder',array('submission-id'=>$vendorSubmission->id)); ?>" class="btn btn-sm btn-success"> Create Work Order</a>
          <?php }  
		  
		  }
			
          ?>
        </div>
        <?php
		$status= UtilityManager::resumeStatus();
		switch ($status[$vendorSubmission->resume_status]) {
			case "Submitted":
				$color = 'label-hold';
				break;
			case "MSP Review":
				$color = 'label-filled';
				break;
			case "MSP Shortlisted":
				$color = 'label-new-request';
				break;
			case "Client Review":
				$color = 'label-filled';
				break;
			case "Interview Process":
				$color = 'label-open';
				break;
			case "Rejected":
				$color = 'label-rejected';
				break;
			case "Offer":
				$color = 'label-pending-aproval';
				break;
			case "Approved":
				$color = 'label-new-request';
				break;
			case "Work Order Release":
				$color = 'label-re-open';
				break;
			default:
				$color = 'label-re-open';
		}
?>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">  </div>
      </div>
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
          <div class="simplify-tabs blue-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <li class="nav-item"> <a class="nav-link active"  href="#submission" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Submission</a> </li>
                <li class="nav-item"> <a class="nav-link"  href="#rates" role="tab" data-toggle="tab" data-placement="bottom" title="Overview"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Rates</a> </li>
              </ul>
              <div class="tab-content   p-r-0 p-l-0">
                <div class="tab-pane active" id="submission"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                      <table class="table tab-table m-b-20">
                        <tbody>
                        <tr>
                          <td>Status</td>
                          <td><span class="tag <?php echo $color; ?>"><?php echo $status[$vendorSubmission->resume_status]; ?></span></td>
                        </tr>
                          <tr>
                            <td>Name of the Candidate:</td>
                            <td><?php echo $candidateModel->first_name.' '.$candidateModel->last_name.' '.$candidateModel->middle_name;?></td>
                          </tr>
                          <tr>
                            <td>Current Location:</td>
                            <td><?php echo $candidateModel->current_location;?></td>
                          </tr>
                          <tr>
                            <th>Start Date :</th>
                            <th><?php echo date('F d,Y',strtotime($vendorSubmission->estimate_start_date));?></th>
                          </tr>
                          <tr>
                            <td>Job Title:</td>
                            <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobModel->id,array('type'=>'info')); ?>"><?php echo $jobModel->title;?></a></td>
                          </tr>
                          <tr>
                            <td>Worker Pay Type:</td>
                            <td><?php echo $candidateModel->worker_pay_type;?></td>
                          </tr>
                          <tr>
                            <td>Working under a W2 directly:</td>
                            <td><?php echo $candidateModel->W2_directly;?></td>
                          </tr>
                          <tr>
                            <td>US Citizen or US Permanent Resident:</td>
                            <td><?php echo $candidateModel->US_permanent_resident;?></td>
                          </tr>
                          <tr>
                            <td>Resume:</td>
                            <td><?php if($candidateModel->resume){?>
                              <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/downloadResume',array('id'=>$candidateModel->id)); ?>" class="blue">Download <i class="fa fa-download"></i></a>
                              <?php  }else {
											echo "NULL";
										}?></td>
                          </tr>
                          <tr>
                            <td>Willing to relocate or not:</td>
                            <td><?php echo $candidateModel->willing_relocate;?></td>
                          </tr>
                          <tr>
                            <td>Vendor Name:</td>
                            <td><?php echo $vendor->organization.'('.$vendor->first_name.' '.$vendor->last_name.')';?></td>
                          </tr>
                          <tr>
                            <td>Vendor Markup:</td>
                            <td><?php echo $mark_up; ?> %</td>
                          </tr>
                          <tr>
                            <td>Submitted to other job posting:</td>
                            <td><?php echo $candidateModel->submitted_other_posting;?></td>
                          </tr>
                          <tr>
                            <td>Former Contractor or affiliates for Client:</td>
                            <td><?php echo $candidateModel->former_contractor;?></td>
                          </tr>
                          <tr>
                            <td>Reason for leaving:</td>
                            <td><?php echo $candidateModel->reason_last_leaving;?></td>
                          </tr>
                          <tr>
                            <td>Submission Date:</td>
                            <td><?php echo date('F d,Y',strtotime($vendorSubmission->date_created));?> at <?php echo $time[0].' '.$time[1];?></td>
                          </tr>
                        </tbody>
                      </table>
                      <?php /*if($vendorSubmission->resume_status != '8'){
                        //if candidate is hired then remove button and you can't update status after that.
                        ?>
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'vendor-form',
                            'enableAjaxValidation'=>false,
                        )); ?>
                        <div class="two-fields">
                          <div class="form-group" style="width:70%; padding-left: 10px;">
                            <label for="">Update Status</label>
                            <?php
                            $list= UtilityManager::resumeStatusClientSide();
                            echo $form->dropDownList($vendorSubmission,'resume_status', $list,
                                array('empty' => 'Change Status','class'=>'form-control','style'=>'height: 32px;  padding: 2px 12px; font-size: 13px;','required'=>'required','options' => array($vendorSubmission->resume_status=>array('selected'=>true)))); ?>
                            <?php echo $form->error($vendorSubmission,'resume_status'); ?> </div>
                          <div class="form-group" style="width:30%;  padding-left: 10px;  padding-right: 10px; ">

                            <p>
                              <button type="submit" class="btn btn-block btn-sm btn-warning">Update</button>
                            </p>

                          </div>
                        </div>
                        <?php $this->endWidget(); ?>
                      <?php }*/ ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane " id="rates"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table class="table tab-table">
                        <tbody>
                          <tr>
                            <th> Pay Rate ( For Candidate ) </th>
                            <th> </th>
                          </tr>
                          <tr>
                            <td>Pay Rate</td>
                            <td>$<?php echo $vendorSubmission->candidate_pay_rate;?></td>
                          </tr>
                          <tr>
                            <td>Over Time</td>
                            <td>$<?php echo $vendorSubmission->over_time_rate;?></td>
                          </tr>
                          <tr>
                            <td>Double Time</td>
                            <td>$<?php echo $vendorSubmission->double_time_rate;?></td>
                          </tr>
                          <tr>
                            <th> Bill Rate ( For Client ) </th>
                          </tr>
                          <?php if(!empty($noMarkupCat)){ ?>
                          <tr>
                            <td>Vendor Bill Rate</td>
                            <td><?php echo '$'.$vendorSubmission->vendor_bill_rate; ?></td>
                          </tr>
                          <?php } ?>
                          <tr>
                            <td>Bill Rate</td>
                            <td>$<?php echo $billrate;?></td>
                          </tr>
                          <tr>
                            <td>Over Time</td>
                            <td>$<?php echo $billrate_overtime; ?></td>
                          </tr>
                          <tr>
                            <td>Double Time</td>
                            <td>$<?php echo $billrate_doubletime; ?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
          </div>
          <!-- simplify-tabs --> 
          
        </div>
        <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
          <div class="simplify-tabs m-t-20">
            <div role="tabpanel"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs no-hover" role="tablist">
                <li class="nav-item"> <a class="nav-link "  href="#resume" role="tab" data-toggle="tab" data-placement="bottom" title="Resume"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i> Resume</a> </li>
                <li class="nav-item">
                  <a class="nav-link "  href="#comments" role="tab" data-toggle="tab" data-placement="bottom" title="Right To Represent">
                    <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i>
                    Right To Represent</a>
                </li>
                <li class="nav-item"> <a class="nav-link active"  href="#skills" role="tab" data-toggle="tab" data-placement="bottom" title="Skills"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Skills</a> </li>
                <?php if($vendorSubmission->resume_status == '6'){ ?>
                <li class="nav-item"> <a class="nav-link"  href="#rejected" role="tab" data-toggle="tab" data-placement="bottom" title="Rejected"> <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/submission@512px.svg"></i> Rejected</a> </li>
                <?php } ?>
                <li class="nav-item">
                  <a class="nav-link "  href="#subcomments" role="tab" data-toggle="tab" data-placement="bottom" title="Comments">
                    <i class=" nav-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/discussion@512px.svg"></i>
                    Comments</a>
                </li>
              </ul>
              <div class="tab-content ">
                <div class="tab-pane" id="rejected"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="alert-box m-t-10">
                        <p class="bold m-b-10">Reason for Rejection</p>
                        <?php $rejectreason = Setting::model()->findByPk($vendorSubmission->reason_for_rejection);
                                                if($rejectreason){
                                                ?>
                        <p class="m-b-10"><?php echo $rejectreason->title;?>.</p>
                        <?php } ?>
                        <p class="bold m-b-10">Notes:</p>
                        <p class="m-b-10"><?php echo $vendorSubmission->notes;?>.</p>
                        <?php $client = Client::model()->findByPk($vendorSubmission->rejected_by);
                                                if($client){
                                                ?>
                        <p><strong>Posted By :</strong> <?php echo $client->first_name.' '.$client->last_name; ?> </p>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane active m--31" id="skills"  role="tabpanel">
                  <table class="table sub-table  tab-table gray-table  submission-skill-table m-b-0">
                    <thead>
                      <tr>
                        <th>Primary Skill</th>
                        <th>Year of Experience</th>
                        <th style="text-align: center;">Rating</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php  $primarySkill = unserialize($candidateModel->primary_skill);
                                        if($primarySkill)
                                        foreach($primarySkill['experience'] as $key=>$value) {
                                        ?>
                      <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $primarySkill['experience'][$key]; ?></td>
                        <td style="text-align: right;"><?php  $rating = explode('(',$primarySkill['rating'][$key]);
                                                     $prating = trim($rating[0]);
                                                    if($prating=='Intermediates'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          / Intermediates
                          <?php }
                                                    if($prating=='Advanced'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> </p>
                          /Advanced
                          <?php }
                                                    if($prating=='Beginners'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star-half-full filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          / Beginners
                          <?php }
                                                    ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                    <thead>
                      <tr>
                        <th>Secondary Skill</th>
                        <th>Year of Experience</th>
                        <th style="text-align: center;">Rating</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                                        $secondarySkill = unserialize($candidateModel->secondary_skill);
                                        if($secondarySkill)
                                        foreach($secondarySkill['experience'] as $key=>$value) { ?>
                      <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $secondarySkill['experience'][$key]; ?></td>
                        <td style="text-align: right;"><?php  $rating = explode('(',$secondarySkill['rating'][$key]);
                                                     $srating = trim($rating[0]);
                                                    if($srating=='Intermediates'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          /Intermediates
                          <?php }
                                                    if($srating=='Advanced'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> </p>
                          /Advanced
                          <?php }
                                                    if($srating=='Beginners'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star-half-full filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          /Beginners
                          <?php }
                                                    ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                    <thead>
                      <tr>
                        <th>Other skills</th>
                        <th>Year of Experience</th>
                        <th style="text-align: center;">Rating</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $otherSkill = unserialize($candidateModel->other_skill);
                                        if($otherSkill)
                                        foreach($otherSkill['experience'] as $key=>$value) {
                                        ?>
                      <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $otherSkill['experience'][$key]; ?></td>
                        <td style="text-align: right;"><?php  $otherSkill['rating'][$key]; ?>
                          <?php  $rating = explode('(',$otherSkill['rating'][$key]);
                                                     $orating = trim($rating[0]);
                                                    if($orating=='Intermediates'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          /Intermediates
                          <?php }
                                                    if($orating=='Advanced'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> <i class="fa fa-star filled"></i> </p>
                          /Advanced
                          <?php }
                                                    if($orating=='Beginners'){ ?>
                          <p class="stars"> <i class="fa fa-star filled"></i> <i class="fa fa-star-half-full filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> <i class="fa fa-star-o filled"></i> </p>
                          /Beginners
                          <?php }
                                                    ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane" id="resume"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <?php
                      if(!empty($candidateModel->resume)){
                        $path = '/candidate_resumes/'.$candidateModel->resume; ?>
                        <iframe src = "<?php echo Yii::app()->getBaseUrl(true);?>/ViewerJS/#..<?php echo $path;?>" width='724' height='1024' allowfullscreen webkitallowfullscreen></iframe>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="comments"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                      <?php
                      if(!empty($candidateModel->right_to_represent)){
                        $path = '/right_to_represent/'.$candidateModel->right_to_represent; ?>
                        <iframe src = "<?php echo Yii::app()->getBaseUrl(true);?>/ViewerJS/#..<?php echo $path;?>" width='724' height='1024' allowfullscreen webkitallowfullscreen></iframe>
                      <?php  } ?>

                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="subcomments"  role="tabpanel">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="alert-box m-t-10">
                        <?php if($vendorSubmission->notes){ ?>
                        <p class="bold m-b-10"><?php echo $vendorSubmission->notes; ?></p>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabpanel --> 
            
          </div>
          <!-- simplify-tabs --> 
        </div>
      </div>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="seprater-bottom-100"></div>
</div>
<div class="modal fade" id="modal-id">
  <div class="modal-dialog">
    <form action="<?php echo Yii::app()->createAbsoluteUrl('Client/client/rejectSub',array('submission-id'=>$vendorSubmission->id)); ?>" method="POST" role="form">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Reject Candidate</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="">Reason for Rejection</label>
            <select name="rejection_type" id="input" class="form-control" required="required">
              <option value="">Select Type</option>
              <?php $list = Setting::model()->findAllByAttributes(array('category_id'=>54));
                        if($list){
                            foreach($list as $data){
                                ?>
              <option value="<?php echo  $data['id']; ?>"><?php echo $data['title']; ?></option>
              <?php } } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="">Note</label>
            <textarea name="notes" id="input" class="form-control" rows="3" required="required"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#notesMessage').hide();
        $("textarea[name=notes]").bind('keyup keypress blur change',function(){
            var submissionID = $(this).attr('id');
            var notes =  $(this).val();
            $.ajax({
                    type: "POST",
                    // dataType:"json",
                    url: "<?php echo Yii::app()->createUrl('candidates/saveNote'); ?>",
                    data: { submissionID: submissionID,notes:notes},
                    beforeSend: function(){
                        $('#notesMessage').show();
                    },
                })
                .done(function(results) {
                    $('#notesMessage').show();
                    $('#notesMessage').html('Saved...');
                });
        });});
</script>