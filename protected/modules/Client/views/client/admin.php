<?php error_reporting(0);
$this->pageTitle =  'Submissions';?>

<div class="col-lg-12 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
  <div class="cleafix " style="padding: 30px 20px 10px; ">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h4 class="m-b-10 page-title">List of Submissions <!--<a href="" class="pull-right"><i class="fa fa-question"></i></a> <a href="" class="pull-right"><i class="fa fa-info"></i></a>--> </h4>
      <p class="m-b-20">List of candidates who have applied for jobs.</p>
      <div class="search-box">
        <div class="two-fields">
          <div class="form-group">
            <label for=""></label>
            <input type="text" class="form-control" id="sdata" placeholder="Search Submissions By Id , Name , Job, Date , Pay rate, Location">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button type="button" class="btn btn-primary btn-block">Search</button>
          </div>
        </div>
        <!-- two-flieds --> 
      </div>
      <div class="row m-b-20">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <form action="" method="post">
            <div class="two-fields-70-30">
              <div class="form-group form-control-2">
                <label for="" style="text-align: left;">Sort by Job Name</label>
                <?php $jobData = Job::model()->findAllByAttributes(array('user_id'=>UtilityManager::superClient(Yii::app()->user->id))); ?>
                <select name="s" id="input" class="form-control  select2" required="required">
                  <option value="">Select</option>
                  <?php foreach ($jobData as $data){ ?>
                  <option value="<?php echo $data->id; ?>"<?php if(isset($_POST['s']) && $_POST['s']==$data->id){ ?> selected="selected" <?php } ?>><?php echo $data->title.'('.$data->id.')';?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="" >&nbsp; </label>
                <p>
                  <button type="submit" class="btn btn-success">Show</button>
                </p>
              </div>
            </div>
          </form>
        </div>
        <!--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <p class="bold m-t-40"> <a href="" class="pull-right"> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/listing@512px.svg"></i> </a> <a href="" class="pull-right" style="margin-right: 10px; "> <i class="icon list-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/menu@512px.svg"></i> </a> </p>
        </div>-->
      </div>
      <table class="table m-b-40 without-border">
        <thead class="thead-default">
          <tr>
            <th  style="width: 10px;">Status</th>
            <th> ID </th>
            <th>Name</th>
            <th> Job Name ( Job ID ) </th>
            <th>Start Date</th>
            <th>Pay Rate</th>
            <th>Bill Rate</th>
            <th style="width: 157px;">Location</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody id="subdata">
          <?php if($model){
                 foreach($model as $value) {

                 //$submissionModel = VendorJobSubmission::model()->findByAttributes(array('candidate_Id'=>$value->id,'client_id'=>Yii::app()->user->id,'resume_status'=>array('3','4','5','7')),array('order' =>'id DESC' , ));
                     $Candidates = Candidates::model()->findByPk($value['candidate_Id']);
                 	 $jobModel = Job::model()->findByPk($value['job_id']);
					 $billrate = $value['candidate_pay_rate'] + $value['candidate_pay_rate'] * ($value['vendorMarkup'])/100;
                     $status = UtilityManager::resumeStatus();
                     switch ($status[$value['resume_status']]) {

                         case "Submitted":
                             $color = 'label-hold';
                             break;

                         case "MSP Review":
                             $color = 'label-filled';
                             break;

                         case "MSP Shortlisted":
                             $color = 'label-new-request';
                             break;

                         case "Client Review":
                             $color = 'label-filled';
                             break;

                         case "Interview Process":
                             $color = 'label-open';
                             break;

                         case "Rejected":
                             $color = 'label-rejected';
                             break;

                         case "Offer":
                             $color = 'label-pending-aproval';
                             break;

                         case "Approved":
                             $color = 'label-new-request';
                             break;

                         case "Work Order Release":
                             $color = 'label-re-open';
                             break;

                         default:
                             $color = 'label-new-request';

                     }
                 ?>
          <tr>
            <td><span class="tag <?php echo $color; ?>"><?php echo $status[$value['resume_status']]; ?></span></td>
            <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$value['sub_id'])); ?>"><?php echo $value['sub_id'];?></a></td>
            <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$value['sub_id'])); ?>"><?php echo $value['first_name'].' '.$value['last_name'];?></a></td>
            <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/job/overview/id/'.$jobModel->id,array('type'=>'info')); ?>"><?php echo $jobModel->title.'('.$jobModel->id.')';?></a></td>
            <td><?php echo  date('F d,Y',strtotime($value['estimate_start_date']));?></td>
            <td>$<?php echo $value['candidate_pay_rate'];?></td>
            <td>
                $<?php echo number_format($billrate, 2);?>
            </td>
            <td><?php echo $Candidates->current_location;?></td>
            <td style="text-align: center"><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/submission',array('submission-id'=>$value['sub_id'])); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="View"><i class="icon svg-icon"><img src="<?php echo Yii::app()->request->baseUrl; ?>/new-theme/assets/images/svg/view@512px-grey.svg"></i></a></td>
          </tr>
          <?php } }else{ ?>
          <tr>
            <td colspan="9">Currently there is no submission for following job</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <!-- col --> 
    
  </div>
  <!-- row -->
  <div class="clearfix" style="padding: 10px 20px 10px; ">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <?php
             $this->widget('CLinkPager', array(
                 'pages' => $pages,
                 'header' => '',
                 'nextPageLabel' => 'Next',
                 'prevPageLabel' => 'Prev',
                 'selectedPageCssClass' => 'active',
                 'hiddenPageCssClass' => 'disabled',
                 'htmlOptions' => array(
                     'class' => 'pagination m-t-0',
                 )
             ))
             ?>
    </div>
    
    <!--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
             <p class="text-right">
                 Showing 10 to 20 of 50 entries
             </p>
         </div>--> 
  </div>
  <div class="clearfix" style="padding: 10px 20px 30px; ">
    <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h5 class="m-b-10">Quick Note</h5>
      <p class="m-b-10"> <span class="tag label-re-open">Completed</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-open">Open</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-hold">Submitted</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-rejected">MSP Review</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-new-request">Shortlisted</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-filled">Review</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-open">Interview</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-pending-aproval">Offer</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-re-open">WorkOrder Release</span> Lorem ipsum dolor sit amet. </p>
      <p class="m-b-10"> <span class="tag label-new-request">Approved</span> Lorem ipsum dolor sit amet. </p>
    </div>-->
  </div>
  <div class="seprater-bottom-100"></div>
</div>
<script>
    $(document).ready(function(){
        /*$("input").keydown(function(){
         $("input").css("background-color", "yellow");
         });*/
        $('#sdata').keyup(function(){
            var subValue=$(this).val();
            $.ajax({
                'url':'<?php echo $this->createUrl('costCenter/submissionSearch') ?>',
                type: "POST",
                data: { subValue: subValue},
                'success':function(html){
                    //alert(html);
                    $("#subdata").html(html)
                }
            });
            //$("input").css("background-color", "pink");
        });
    });
</script>