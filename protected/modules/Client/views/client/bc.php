<div class="db-content">

	<div class="db-content__header">
		<h3>Step 3 - Add Locations</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>


	<div class="business-profile">

		<div class="form-wraper">

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'location-form',
				// Please note: When you enable ajax validation, make sure the corresponding
				// controller action is handling ajax validation correctly.
				// There is a call to performAjaxValidation() commented in generated controller code.
				// See class documentation of CActiveForm for details on this.
				'enableAjaxValidation'=>false,
			)); ?>


				<div class="row">

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Location Name</label>
							<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
							<?php echo $form->error($model,'name'); ?>
						</div>
					</div> <!-- col-12 -->

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="" class="">Address 1</label>
							<?php echo $form->textField($model,'address1',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
							<input class="field" id="route" />
							<?php echo $form->error($model,'address1'); ?>
						</div>
					</div> <!-- col-12 -->

				</div> <!-- row -->

				<div class="row">

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Address 2</label>
							<?php echo $form->textField($model,'address2',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
						</div>
					</div> <!-- col-12 -->

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="" class="">Select Country</label>
							<div class="single">
								<select name="Locatio[country]" id="Location_country" class="ui fluid search dropdown">
									<option value=""></option>
									<option value="angular">UK</option>
									<option value="css">USA</option>
									<option value="design">Canada</option>
								</select>
							</div>
						</div>
					</div> <!-- col-12 -->

				</div> <!-- row -->

				<div class="row">

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">City</label>
							<?php echo $form->textField($model,'city',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
						</div>
					</div> <!-- col-12 -->

					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="" class="">State</label>
							<div class="single">
								<select name="Location[state]" class="ui fluid search dropdown">
									<option value=""></option>
									<option value="angular">UK</option>
									<option value="css">USA</option>
									<option value="design">Canada</option>
								</select>
							</div>
						</div>
					</div> <!-- col-12 -->

				</div> <!-- row -->


				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Zip Code</label>
							<?php echo $form->textField($model,'zip_code',array('size'=>30,'maxlength'=>30,'class'=>'form-control')); ?>
						</div>
					</div> <!-- col-12 -->
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label for="">Location Account Manger</label>
							<div class="single">
								<select name="Location[location_account_mananger]" id="Location_location_account_mananger" class="ui fluid search dropdown">
									<option value=""></option>
									<option value="angular">UK</option>
									<option value="css">USA</option>
									<option value="design">Canada</option>
								</select>
							</div>
						</div>
					</div> <!-- col-12 -->



				</div> <!-- row -->





				<div class="button-group">
					<button type="submit" class="btn-round btn-blue">ADD LOCATION</button>
					 <?php echo CHtml::link('SKIP & CONTINUE',array('default/index'),array('class'=>'btn-round btn-blue'));?>
				</div>


			<?php $this->endWidget(); ?>
		</div> <!-- form-wraper -->

		<div class="team-members">
			<div class="team-members_header">
				<h4>LOCATIONS</h4>
			</div>

			<div class="table-responsive">
				<table class="table">
					<tbody>

					 <?php 
                    if( $allLocation )
                    foreach( $allLocation as $value ) { ?>
                    <tr>
                        <td>
                            <p><?php echo $value->name;?></p>
                            <span><?php echo $value->location_account_mananger;?></span>
                        </td>
                        <td>
                            <p><?php echo $value->address1.' '. $value->address2.' '.$value->city.' '. $value->state.' '. $value->country.' '. $value->zip_code;?></p>
                           
                        </td>
                        <td>
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/deletelocation',array('id'=>$value->id))?>"><img src="theme-assets/images/icons/del.png"></a>
                        </td>
                    </tr>

                  <?php } ?>


					</tbody>
				</table>
			</div>
		</div>

	</div> <!-- business-profile -->

</div> <!-- db-content -->

 <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        Location_address1: 'short_name',
        route: 'long_name',
        Location_city: 'long_name',
        Location_location_account_mananger: 'short_name',
        Location_country: 'long_name',
        Location_zip_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeg8MgxBJnzgTHYvbwMtHr7rUyhxsUbUA&libraries=places&callback=initAutocomplete"
        async defer></script>
