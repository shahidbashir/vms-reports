<div class="page-head">

          <h2 class="page-head-title">Step 3 - Add Locations</h2>

          <ol class="breadcrumb page-head-nav">

            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/index'); ?>">Dashbord</a></li>

            

            <li class="active">Step 3 - Add Locations</li>

          </ol>

        </div>

        <div class="main-content">
           <?php if(Yii::app()->user->hasFlash('success')):?>

        <?php echo Yii::app()->user->getFlash('success'); ?>

        <?php endif; ?>

           <div class="row">

            <div class="col-md-12">

              <div class="panel panel-border-color panel-border-color-primary">
                <div class="panel-body">
                    <div class="white-box">
                       <div class="row m-0">
                          <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">

                             <div class="business-setup">

                                <div class="form-wraper">

                                   <br>

                  <?php $form=$this->beginWidget('CActiveForm', array(

						'id'=>'location-form',

						'enableAjaxValidation'=>false,

					)); ?>

                  <div class="row">

                    <div class="col-xs-12 col-sm-6">

                      <div class="form-group">

                        <label for="">Location Name</label>

                        <?php echo $form->textField($model,'name',array('maxlength'=>100,'class'=>'form-control')); ?>

                        <?php //echo $form->error($model,'name'); ?>

                      </div>

                    </div>

                    <!-- col-12 -->

                    

                    <div class="col-xs-12 col-sm-6">

                      <div class="form-group">

                        <label for="" class="">Address 1</label>

                        <input id="autocomplete" placeholder=""

             onFocus="geolocate()" type="text" class="form-control" name="Location[address1]">

                        </input>

                        <input id="route" type="hidden">

                        </input>

                      </div>

                    </div>

                    <!-- col-12 --> 

                    

                  </div>

                  <!-- row -->

                  

                  <div class="row">

                    <div class="col-xs-12 col-sm-6">

                      <div class="form-group">

                        <label for="">Address 2</label>

                        <?php echo $form->textField($model,'address2',array('id'=>'route2','maxlength'=>100,'class'=>'form-control')); ?> </div>

                    </div>

                    <!-- col-12 -->

                    

                    <div class="col-xs-12 col-sm-6">

                      <div class="form-group">

                        <label for="" class="">Select Country</label>

                        <div class="single"> <?php echo $form->textField($model,'country',array('id'=>'country','maxlength'=>100,'class'=>'form-control')); ?> </div>

                      </div>

                    </div>

                    <!-- col-12 --> 

                    

                  </div>

                  <!-- row -->

                  

                  <div class="row">

                    <div class="col-xs-12 col-sm-6">

                      <div class="form-group">

                        <label for="">City</label>

                        <?php echo $form->textField($model,'city',array('id'=>'locality','maxlength'=>30,'class'=>'form-control')); ?> </div>

                    </div>

                    <!-- col-12 -->

                    

                    <div class="col-xs-12 col-sm-6">

                      <div class="form-group">

                        <label for="" class="">State</label>

                        <div class="single"> <?php echo $form->textField($model,'state',array('id'=>'administrative_area_level_1','maxlength'=>100,'class'=>'form-control')); ?> </div>

                      </div>

                    </div>

                    <!-- col-12 --> 

                    

                  </div>

                  <!-- row -->

                  <input type="hidden" id="postal_code" name="zip_code" />

                  <div class="row">

                    <div class="col-xs-12 col-sm-6">

                      <div class="form-group">

                        <label for="">Location Account Manger</label>

                        <div class="single">

                          <?php 

							 $list = Client::model()->findAll(array('condition'=>'super_client_id='.Yii::app()->user->id));

							 $array = array();

							 foreach($list as $listValue){

							 	$array[$listValue->first_name.' '.$listValue->last_name] = $listValue->first_name.' '.$listValue->last_name .'( '.$listValue->email.')';

							 }



                             echo $form->dropDownList($model, 'location_account_mananger',$array, array('class'=>'ui fluid search dropdown form-control','empty' => '')); ?>

                        </div>

                      </div>

                    </div>

                    <!-- col-12 --> 

                    

                  </div>

                  <!-- row -->

                  

                  <div class="button-group">

                    <button type="submit" class="btn btn-success">ADD LOCATION</button>

                    <?php echo CHtml::link('SKIP &amp; CONTINUE',array('client/skipLocation'),array('class'=>'btn btn-default'));?> </div>

                  <?php $this->endWidget(); ?>


    <br>

    <br>

    

    

</div>

<!-- business-setup -->

</div>



</div>

</div>

</div>





</div>

</div>







<!--Hover table-->





<!--Hover table-->

  <?php if($allLocation) { ?>

<div class="row">

<div class="col-md-12">

<div class="panel panel-default panel-table">



<div class="panel-body">



<div class="row">

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

<div class="table-default">

                              

                                    <h4 class="" style="margin:20px 20px;">Locations</h4>

                              

                              <div class="table-responsive">



                               <table class="table table-condensed table-striped">


                        <tbody>

                          <?php foreach( $allLocation as $value ) { ?>

                          <tr>

                            <td><p><?php echo $value->name;?></p>

                              <span class="cell-detail-description"><?php echo $value->location_account_mananger;?></span></td>

                            <td><p><?php echo $value->address1; ?></p></td>

                            <td><a href="<?php echo Yii::app()->createAbsoluteUrl('Client/client/deletelocation',array('id'=>$value->id))?>" data-toggle="popover" data-placement="bottom" data-content="Delete" class="delete"><i class="fa fa-trash"></i> Delete</a></td>

                          </tr>

                          <?php } ?>

                        </tbody>

                      </table>

                              </div>





                       

         

                        

                        <br>

                     </div>

</div> <!-- col -->

</div> <!-- row -->



</div>

</div>

</div>





</div>

</div>







<!--Hover table-->


                  <?php } ?>

                </div>

                <!-- business-setup --> 

              </div>

             
            </div>

          </div>

        </div>

      </div>

    </div>

    <!-- container --> 

    <br>

    <br>

    <br>

    <?php //$this->renderPartial('/default/footer'); ?>

  </div>

</div>

<script>

      // This example displays an address form, using the autocomplete feature

      // of the Google Places API to help users fill in the information.



      // This example requires the Places library. Include the libraries=places

      // parameter when you first load the API. For example:

      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



      var placeSearch, autocomplete;

      var componentForm = {

        //street_number: 'short_name',

        route: 'long_name',

        locality: 'long_name',

        administrative_area_level_1: 'short_name',

        country: 'long_name',

        postal_code: 'short_name'

      };



      function initAutocomplete() {

        // Create the autocomplete object, restricting the search to geographical

        // location types.

        autocomplete = new google.maps.places.Autocomplete(

            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),

            {types: ['geocode']});



        // When the user selects an address from the dropdown, populate the address

        // fields in the form.

        autocomplete.addListener('place_changed', fillInAddress);

      }



      function fillInAddress() {

        // Get the place details from the autocomplete object.

        var place = autocomplete.getPlace();



        for (var component in componentForm) {

          document.getElementById(component).value = '';

          document.getElementById(component).disabled = false;

        }



        // Get each component of the address from the place details

        // and fill the corresponding field on the form.

        for (var i = 0; i < place.address_components.length; i++) {

          var addressType = place.address_components[i].types[0];

          console.log(addressType);

          if (componentForm[addressType]) {

            var val = place.address_components[i][componentForm[addressType]];

            document.getElementById(addressType).value = val;



          }

        }

      }



      // Bias the autocomplete object to the user's geographical location,

      // as supplied by the browser's 'navigator.geolocation' object.

      function geolocate() {

        if (navigator.geolocation) {

          navigator.geolocation.getCurrentPosition(function(position) {

            var geolocation = {

              lat: position.coords.latitude,

              lng: position.coords.longitude

            };

            var circle = new google.maps.Circle({

              center: geolocation,

              radius: position.coords.accuracy

            });

            autocomplete.setBounds(circle.getBounds());

          });

        }

      }



 $('#autocomplete').live('change',function(){

 	$('#route').val($(this).value);

 });

    </script> 

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw9D1ilDTssilN8RzWxTTP5vCwaUDB4RA&libraries=places&callback=initAutocomplete"

        async defer></script> 

