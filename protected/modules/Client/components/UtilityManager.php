<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UtilityManager
{
	/*function __construct() {
		   ob_start();
	   }*/
	public static function day(){
		return array(
			'Sunday'=>'Sunday',
			'Monday'=>'Monday',
			'Tuesday'=>'Tuesday',
			'Wednesday'=>'Wednesday',
			'Thursday'=>'Thursday',
			'Friday'=>'Friday',
			'Saturday'=>'Saturday',
		);
	}
	public static function reportGroup() {
		return array(
			'1' => 'Activity Report',
			'2' => 'Scorecard Report',
		);
	}
	public static function accessGroup() {
		return array(
			'1' => 'Self',
			'2' => 'Human Resource Department',
			'3'=>'All'
		);
	}
	public static function invoiceStatus() {
		return array(
			//'0' => 'Draft',
			'0' => 'Pending',
			'1' => 'Pending',
			'2' => 'Approved',
		);
	}
	public static function timeSheetStatus() {
        return array(
   			'0' => 'Pending',
            '1' => 'Approved',
            '2' => 'Rejected',
			'3' => 'Draft',
        );
    }
	public static function getWorkingDays($startDate, $endDate,$hoursperday)
	{
		$begin = strtotime($startDate);
		$end   = strtotime($endDate);
		if ($begin > $end) {
			//echo "startdate is in the future! <br />";

			return 0;
		} else {
			$no_days  = 0;
			$weekends = 0;
			while ($begin <= $end) {
				$no_days++; // no of days in the given interval
				$what_day = date("N", $begin);
				if ($what_day > 5) { // 6 and 7 are weekend days
					$weekends++;
				};
				$begin += 86400; // +1 day
			};
			$working_days = $no_days - $weekends;

			return $working_days*$hoursperday;
		}
	}
	public static function lastday($month, $year) {
		if (empty($month)) {
			$month = date('m');
		}
		if (empty($year)) {
			$year = date('Y');
		}
		$result = strtotime("{$year}-{$month}-01");
		$result = strtotime('-1 second', strtotime('+1 month', $result));
		return date('Y-m-d', $result);
	}
	public static function firstDay($month, $year)
	{
		if (empty($month)) {
			$month = date('m');
		}
		if (empty($year)) {
			$year = date('Y');
		}
		$result = strtotime("{$year}-{$month}-01");
		return date('Y-m-d', $result);
	}
	public static function getStartAndEndDate($week, $year)
	{
		$time = strtotime("1 January $year", time());
		$day = date('w', $time);
		$time += ((7*$week)+1-$day)*24*3600;
		$return[0] = date('Y-n-j', $time);
		$time += 6*24*3600;
		$return[1] = date('Y-n-j', $time);
		return $return;
	}
	
	//digital document types in setting of client portal
	public static function digitalDocTypes(){
		return array(
			'1'=>'For Offer',
			'2'=>'For Onboarding'
		);
	}
	
	/*public static function jobStatus() {
	 return array(
				'1' => 'Review', 
				'2' => 'Open',
				'3' => 'Approved',
				'4' => 'Filled',
				'5' => 'Cancelled',
				'6' => 'Re-open',
				'7' => 'Hidden',
				'8' => 'Closed',
				'9' => 'Archived',
				'10' => 'Hold',
				'11' => 'Open for Vendors'
				);
		  
	 }*/
	 
	public static function jobStatus() {
	 return array(				
				'1' => 'Pending Approval',
				'2' => 'Draft',
				'3' => 'Open',
				'4' => 'Filled',
				'5' => 'Rejected',
				'6' => 'Re-open',
				'10' => 'Hold',
				'11' => 'New Request',
				'12' => 'Closed'
				);
		  
	 }
	 
	public static function jobPostingStatus() {
        return array(
            '2' => 'Review',
			'1' => 'Draft',
            '3' => 'Approved',
            '4' => 'Open',
			'5' => 'Hold',
			'6' => 'Filled',
			'7' => 'Closed',
            '8' => 'Rejected',
			'9' => 'Re-open',
			'10' => 'Cancelled',
            '11' => 'Archived',
        );
    }
	
	
	
    public static function resumeStatus() {
        return array(
            '1' => 'Submitted',
            '2' => 'MSP Review',
            '3' => 'MSP Shortlisted',
            '4' => 'Client Review',
            '5' => 'Interview Process',
            '6' => 'Rejected',
            '7' => 'Offer',
			//'8' => 'Approved',
			'8' => 'Hired',
			'9' => 'Work Order Release',
        );
    }
	
	//these status will be used on client side
	public static function resumeStatusClientSide() {
        return array(
            '4' => 'Client Review',
            '5' => 'Interview Process',
			'6' => 'Rejected',
            '7' => 'Offer',
			'9' => 'Work Order Release',
			//'8' => 'Approved',
			'8' => 'Hired',
			
        );
    }
	
	public static function interviewStatus() {
        return array(
            '1' => 'Waiting for Approval',
            '2' => 'Approved',
            '3' => 'Cancelled',
            '4' => 'Reschedule',
			'5' => 'Interview Completed',
        );
    }
	
	public static function offerStatus() {
        return array(
            '1' => 'Approved',
            '2' => 'Rejected',
            //'3' => 'Change Request',
			'4' => 'Waiting for Approval',
			'5' => 'OnHold',
			'6' => 'Cancelled'
        );
    }
	
	public static function workorderStatus() {
        return array(
			'0' => 'Pending',
            '1' => 'Approved',
            '2' => 'Rejected',
			'3' => 'Closed',
			//'4' => 'Renewed',
			//'3' => 'On Contract',
        );
    }
	
	public static function expressJobStatus() {
        return array(
			'0' => 'Review',
            '1' => 'Open',
            '2' => 'Published',
        );
    }
	
	public static function terminationStatus() {
        return array(
			'0' => '',
            '1' => 'Pending',
            '2' => 'Approved',
			'3' => 'Cancelled',
        );
    }
	
	//field name "ext_status" in contract table
	public static function extensionStatus() {
        return array(
			'0' => '',
            '1' => 'Pending',
            '2' => 'Approved',
			'3' => 'Rejected',
        );
    }
	
	//field name "ext_vendor_approval" in contract table
	public static function vendorExtensionStatus() {
        return array(
			'0' => '',
            '1' => 'Pending',
            '2' => 'Approved',
			'3' => 'Rejected',
        );
    }
	
	//field name "bill_req_apprej_by" in vms_contract_extension_req table
	public static function vendorBillRequestApproval() {
        return array(
			'0' => '',
            '1' => 'Pending',
            '2' => 'Approved',
			'3' => 'Rejected',
        );
    }
	
	//field name "bill_request_status" in vms_contract_extension_req table
	public static function billChangeRequestByVendor() {
        return array(
			'0' => '',
            '1' => 'Pending',
            '2' => 'Approved',
			'3' => 'Rejected',
        );
    }

	public static function supervisoremailOnboard($id){

		//echo $password;echo $email;exit;
		$contractID = $id;
		$ContractInfo = Contract::model()->findByPk($contractID);
		$offer = Offer::model()->findByPk($ContractInfo->offer_id);
		$candidate = Candidates::model()->findByPk($ContractInfo->candidate_id);
		$location = Location::model()->findByPk($offer->approver_manager_location);
		$department = Setting::model()->findByPk($ContractInfo->candidate_dept);

		$htmlbody = '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px; "Proxima Nova","Helvetica Neue",Arial,sans-serif;padding:2% 7%">
            <div>

            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hi '.$ContractInfo->supervisor_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                    <span style="color:#333;font-size:13px;line-height:1.4">Contract Info</span><br>
                    Name of Candidate: '.$candidate->first_name.' '.$candidate->last_name.'<br />
      				Start Date: '.$ContractInfo->start_date.'<br />
      				End Date: '.$ContractInfo->end_date.'<br />
      				Location: '.$location->name.'<br />
      				Department: '.$department->title.'<br />
      				</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
   Regard <br> Webmaster - SimplifyVMS
   </p>
  </div>
                </div>
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';


		$message = Yii::app()->sendgrid->createEmail();
		//shortcut to $message=new YiiSendGridMail($viewsPath); 
		$message->setHtml($htmlbody)
			->setSubject('Contract Alert')
			->addTo($ContractInfo->supervisor_email)
			->setFrom('account@simplifyvms.net');

		Yii::app()->sendgrid->send($message);
	}
	
	
	public static function SuperClient($mainclient){
		$client = Client::model()->findByPk($mainclient);
		  if($client->member_type!=NULL){
		   $client = Client::model()->findByPk($client->super_client_id);
		   return $mainclient = $client->id;
		  }else{
			   return $mainclient = $mainclient;
			   }
		}
		
	public static function GetSubClients($mainclient){
		 $ids[$mainclient] = $mainclient;
		 $allClients = Client::model()->findAllByAttributes(array(
				'super_client_id' => $mainclient, 
			  ));
		  
		  foreach($allClients as $clientOb){
		   $ids[$clientOb->id] = $clientOb->id;
		  }
		return implode(',' , $ids);
		}

	// for converting word files into PDF
	public static function convertFile($path , $pdfname , $name){
		$fullPath = $path.$name;
		$endpoint = 'http://smcpdf.online/api.php?'.http_build_query([
				'key' => 'mTDYtBXwH2X7b6mJ9n8GZy45SEI9RRtd'
			]);

		$c = curl_init($endpoint);
		// file to convert
		$file = new CURLFile(
			$fullPath      // path to file
		);

		// send a file
		curl_setopt($c, CURLOPT_POST, true);
		curl_setopt($c, CURLOPT_POSTFIELDS, ['file' => $file]);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

		$pdfOrError = curl_exec($c);
		// Checks if there is any error during the process
		$statusCode = curl_getinfo($c, CURLINFO_HTTP_CODE);
		$pdfFileName = preg_replace("/\.[^.]+$/", "", basename($name));

		if (200 === $statusCode) {  // Successfully converted
			// Saves the PDF somewhere.
			file_put_contents($path.$pdfFileName.'.pdf', $pdfOrError);
			$pdfFileName = $pdfFileName.'.pdf';
		} else {
			// Outputs the error. The $pdf variable should have the details of what happened.
			echo 'An error occurred while processing: ' . $pdfOrError;
		}

		// close the session
		curl_close($c);
		return 	$pdfFileName;
	}
	//timesheet status
	//0 pending
	//1 approved
	//2 rejected
	
	//roles in project
	/*
	1) MSP Coordinator
	2) Account Approver
	3) Procurement Manager
	4) HR Manager
	5) Finance Manager
	6) Time Sheet Manager
	7) Department Manager
	8) Hiring Manager
	9) Administrator
	*/
	
	/*contract termination status list
	0=>'',
	1=>'Pending',
	2=>'Approved',
	3=>'cancelled',*/
	
	
    public static function emailByTemplate($id,$sendToType,$emailType='client-creation') {
        if($sendToType == 'Client') { 
		$info = Client::model()->findByPk($id);
		$emailTemp = EmailTemplate::model()->findByAttributes(array('type'=>$emailType));
 
		$customerEmail = $info->email;
          $urlID = UrlEnDeCode::getIdDecoded($id); 
          $search  = array(0,1, 2, 3, 4, 5,6,7,8,9);
          $replace = array('s', 'l', 'm', 'k', 'z','n','o','q','t','d');
          $urlID = str_replace($search, $replace, $id);
          if($emailType == 'client-forgot-password') {
            	$link = CHtml::link(Yii::app()->createAbsoluteUrl('Client/default/resetpassword',array('client_id'=>$urlID)),Yii::app()->createAbsoluteUrl('client/resetpassword',array('client_id'=>$urlID)));
            }else {
              $link = CHtml::link(Yii::app()->createAbsoluteUrl('client/accountactiavtion',array('client_id'=>$urlID)),Yii::app()->createAbsoluteUrl('client/accountactiavtion',array('client_id'=>$urlID)));
            }
       
           
          $content = str_replace("{{name}}", $info->first_name.' '.$info->last_name, $emailTemp->content);
          $content = str_replace("{{url of the activation}}", $link, $content);
          $cc = $emailTemp->cc_email_ID;
          $bcc = $emailTemp->bcc_email_ID;
          $fromName =  $emailTemp->from_email_ID;
	     }
        
          //$applicationUrl = $info->application_url;
           //echo "<pre>";var_dump($id.$sendToType.$emailType); var_dump($content);print_r($info);exit;
            $message = Yii::app()->sendgrid->createEmail();
      //shortcut to $message=new YiiSendGridMail($viewsPath);
            $message->setHtml($content)
                ->setSubject($emailTemp->subject_line)
                ->addTo($customerEmail)
                ->addTo($cc)
                ->addBcc($bcc)
                ->setFromName($fromName)
                ->setFrom($emailTemp->from_email);
            Yii::app()->sendgrid->send($message);
   }
    
    public static function jobNotification($email,$jobTitle) {
        $staffEmail = $email;
        $htmlbody='<div align="center">
  <table border="0" cellspacing="0" cellpadding="0" width="650" style="background:white;border:solid #e0e0e0 1.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt">
    <tbody>
      <tr>
        <td valign="top"><h1>Hi,<u></u><u></u></h1>
        <p>Thanks for trying US VMS!</p></br>
          <p>Job Title : '.$jobTitle.'</p></br>
          <p>';
          if(!empty($_POST['Interview']['notes'])){
            $htmlbody.=$_POST['Interview']['notes'];
          }else{
            $htmlbody.=' ';
          }
          $htmlbody.='</br>
          <p>Webmaster</p>
          </td>
      </tr>
      <tr>
        </td>
      </tr>
    </tbody>
  </table>
</div>
  ';
        $message = Yii::app()->sendgrid->createEmail();
        //shortcut to $message=new YiiSendGridMail($viewsPath);
        $message->setHtml($htmlbody)
            ->setSubject($jobTitle)
            ->addTo($staffEmail)
            ->setFrom('alert@simplifyvms.net');
        Yii::app()->sendgrid->send($message);
    }
	
	public static function directJobApprovalNotify($clientData,$model,$locationsV,$workFlow) {
        $htmlbody =  '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px,Arial,sans-serif;padding:2% 7%">
            <div>

            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$clientData->first_name.' '.$clientData->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">This job is created and automatically approved by our system.</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                        <span style="color:#333;font-size:13px;line-height:1.4"><strong>Job ID </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->id.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Name </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->title.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Description </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->description.'</span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Pay Rate </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->pay_rate.' </span><br /><br />


						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Location </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$locationsV.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Hour for the Job </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->hours_per_week.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Shift Time. </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->shift.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Project Reference Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->pre_reference_code.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Bill Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->billing_code.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Job Request Code </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$model->request_dept.' </span><br /><br />

						<span style="color:#333;font-size:13px;line-height:1.4"><strong>Work Flow Approval : </strong></span><br />
						<span style="color:#333;font-size:13px;line-height:1.4">'.$workFlow->flow_name.' </span><br /><br />

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
                        Thanks,<br> Webmaster - Simplifyvms
                    </p>
                </div>
            </div>


            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';
        $message = Yii::app()->sendgrid->createEmail();
        $message->setHtml($htmlbody)
            ->setSubject($model->title)
            ->addTo($clientData->email)
            ->setFrom('alert@simplifyvms.net');
        Yii::app()->sendgrid->send($message);
    }
	
	public static function offerNotification($email,$jobTitle,$message) {
        $staffEmail = $email;
		$htmlbody =  '
			<div style="font-size:13px;line-height:1.4;margin:0;padding:0">
			  <div style="background:#f7f7f7;font:13px,Arial,sans-serif;padding:2% 7%">
				<div> </div>
				<div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
				  <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%"> '.$message.' </div>
				</div>
				<div class="adL"> </div>
			  </div>
			  <div class="adL"> </div>
			</div>
			';
        $message = Yii::app()->sendgrid->createEmail();
        //shortcut to $message=new YiiSendGridMail($viewsPath);
        $message->setHtml($htmlbody)
            ->setSubject($jobTitle)
            ->addTo($staffEmail)
            ->setFrom('alert@simplifyvms.net');
        Yii::app()->sendgrid->send($message);
    }
	
	
	
	// Email to vendor and msp
	public static function interviewNotification($email,$submisionID,$jobID,$interviewID){
		
		$submissonData = VendorJobSubmission::model()->findByPk($submisionID);
		$candidateData = Candidates::model()->findByPk($submissonData->candidate_Id);
		$job = Job::model()->findByPk($jobID);
		$vendor = Vendor::model()->findByPk($submissonData->vendor_id);
		$interViewModel = Interview::model()->findByPk($interviewID);
		
		//Subject Line : Interview ID requested for candidate Johnson, Michelle Lee for Opening #107044, Customer Service Representative
		$jobTitle = $interviewID.' requested for candidate '.$candidateData->first_name.' '.$candidateData->last_name.' for Opening #'.$jobID.' '.$job->title;
		
		
		$loc = unserialize($job->location);
		$locationsV = '';
		foreach($loc as $val){
			$location = Location::model()->findByPk($val);
			$locationsV .= $location->name.', ';
			}
			
		$alt1Time = '';
	   if($interViewModel->interview_alt1_start_date_time){
		   $alt1Time = '<span style="color:#333;font-size:13px;line-height:1.4">Recommended Date: <strong>'.$interViewModel->interview_alt1_start_date.' </strong> </span><br />
		  <br />
		  
		  <span style="color:#333;font-size:13px;line-height:1.4">Time Duration: <strong>'.$interViewModel->interview_alt1_start_date_time.' to '.$interViewModel->interview_alt1_end_date_time.' </strong> </span><br />
		  <br />';
		   }
		   
		$alt2Time = '';
	   if($interViewModel->interview_alt2_start_date_time){
		   $alt2Time = '<span style="color:#333;font-size:13px;line-height:1.4">Recommended Date: <strong>'.$interViewModel->interview_alt2_start_date.' </strong> </span><br />
		  <br />
		  
		  <span style="color:#333;font-size:13px;line-height:1.4">Time Duration: <strong>'.$interViewModel->interview_alt2_start_date_time.' to '.$interViewModel->interview_alt2_end_date_time.' </strong> </span><br />
		  <br />';
		   }
		   
		$alt3Time = '';
	   if($interViewModel->interview_alt3_start_date_time){
		   $alt3Time = '<span style="color:#333;font-size:13px;line-height:1.4">Recommended Date: <strong>'.$interViewModel->interview_alt3_start_date.' </strong> </span><br />
		  <br />
		  
		  <span style="color:#333;font-size:13px;line-height:1.4">Time Duration: <strong>'.$interViewModel->interview_alt3_start_date_time.' to '.$interViewModel->interview_alt3_end_date_time.' </strong> </span><br />
		  <br />';
		   }

		$htmlbody  = '<body>
			<div style="font-size:13px;line-height:1.4;margin:0;padding:0">
		
				<div style="background:#f7f7f7;font:13px "Proxima Nova","Helvetica Neue",Arial,sans-serif;padding:2% 7%">
					<div>
						
					</div>
		
		
					<div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
						<div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
							<h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Congratulations,</h1>
		
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">Your candidate has been selected for an interview.</p>
		
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Candidate : '.$candidateData->first_name.' '.$candidateData->last_name.'
								 </p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Job ID #: '.$job->id.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Job Name: '.$job->title.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Job Location: : '.$locationsV.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Vendor Organization Name: '.$vendor->organization.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Interview ID #: '.$interViewModel->id.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Interview Type : '.$interViewModel->interview_type.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Recommended Date :'.$interViewModel->interview_start_date.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Time Duration : '.$interViewModel->interview_start_date_time.' to '.$interViewModel->interview_end_date_time.'
							</p></br>
		
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								<span style="color:#333;font-size:13px;line-height:1.4">To Approve the Interview</span><br>
								<a href="'.Yii::app()->createAbsoluteUrl('Client/job/directApproveTime',
					array('submissonId'=>$submisionID,'id'=>$job->id,'interviewId'=>$interviewID)).'" style="color:#069;font-size:13px;line-height:1.4;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://fwebinc.freshsales.io/confirmations/xbootIN_zLafNA1D3eYAoA&amp;source=gmail&amp;ust=1468345965323000&amp;usg=AFQjCNHpRcdBxBCT16Bscb76mCXx0b9dDw">Click here</a>
							</p>
							<br>
							<p>Other suggest date are available. </p>
							'.$alt1Time.$alt2Time.$alt3Time.'
							<p><strong>To confirm please login into SimplifyVMS</strong> and approve it.</p>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
								Thanks,<br> Webmaster - Simplifyvms
							</p>
						</div>
					</div>
		
					
					<div class="adL">
		
					</div>
				</div>
				<div class="adL">
				</div>
			</div>
		</body>';
	  
	  
	  //echo '<pre>'; print_r($htmlbody); exit;
	$message = Yii::app()->sendgrid->createEmail();
	//shortcut to $message=new YiiSendGridMail($viewsPath);
	$message->setHtml($htmlbody)
		->setSubject($jobTitle)
		->addTo($email)
		->setFrom('alert@simplifyvms.net');
	Yii::app()->sendgrid->send($message);
	  
	  
	}
	
	//email for guests of interview
	public static function interviewGuestNotification($email,$submisionID,$jobID,$interviewID){
		
		$submissonData = VendorJobSubmission::model()->findByPk($submisionID);
		$candidateData = Candidates::model()->findByPk($submissonData->candidate_Id);
		$job = Job::model()->findByPk($jobID);
		$vendor = Vendor::model()->findByPk($submissonData->vendor_id);
		$interViewModel = Interview::model()->findByPk($interviewID);
		
		//Subject Line : Interview ID requested for candidate Johnson, Michelle Lee for Opening #107044, Customer Service Representative
		$jobTitle = '<strong>Ask the Join the Interview </strong>'. $interviewID.' requested for candidate '.$candidateData->first_name.' '.$candidateData->last_name.' for Opening #'.$jobID.' '.$job->title;
		
		
		$loc = unserialize($job->location);
		$locationsV = '';
		foreach($loc as $val){
			$location = Location::model()->findByPk($val);
			$locationsV .= $location->name.', ';
			}
			
		$alt1Time = '';
	   if($interViewModel->interview_alt1_start_date_time){
		   $alt1Time = '<span style="color:#333;font-size:13px;line-height:1.4">Recommended Date: <strong>'.$interViewModel->interview_alt1_start_date.' </strong> </span><br />
		  <br />
		  
		  <span style="color:#333;font-size:13px;line-height:1.4">Time Duration: <strong>'.$interViewModel->interview_alt1_start_date_time.' to '.$interViewModel->interview_alt1_end_date_time.' </strong> </span><br />
		  <br />';
		   }
		   
		$alt2Time = '';
	   if($interViewModel->interview_alt2_start_date_time){
		   $alt2Time = '<span style="color:#333;font-size:13px;line-height:1.4">Recommended Date: <strong>'.$interViewModel->interview_alt2_start_date.' </strong> </span><br />
		  <br />
		  
		  <span style="color:#333;font-size:13px;line-height:1.4">Time Duration: <strong>'.$interViewModel->interview_alt2_start_date_time.' to '.$interViewModel->interview_alt2_end_date_time.' </strong> </span><br />
		  <br />';
		   }
		   
		$alt3Time = '';
	   if($interViewModel->interview_alt3_start_date_time){
		   $alt3Time = '<span style="color:#333;font-size:13px;line-height:1.4">Recommended Date: <strong>'.$interViewModel->interview_alt3_start_date.' </strong> </span><br />
		  <br />
		  
		  <span style="color:#333;font-size:13px;line-height:1.4">Time Duration: <strong>'.$interViewModel->interview_alt3_start_date_time.' to '.$interViewModel->interview_alt3_end_date_time.' </strong> </span><br />
		  <br />';
		   }

		$htmlbody  ='<body>
			<div style="font-size:13px;line-height:1.4;margin:0;padding:0">
		
				<div style="background:#f7f7f7;font:13px; "Proxima Nova","Helvetica Neue",Arial,sans-serif;padding:2% 7%">
					<div>
						
					</div>
		
		
					<div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
						<div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
							<h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Congratulations,</h1>
		
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">Your candidate has been selected for an interview.</p>
		
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Candidate : '.$candidateData->first_name.' '.$candidateData->last_name.'
								 </p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Job ID #: '.$job->id.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Job Name: '.$job->title.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Job Location:'.$locationsV.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Vendor Organization Name: '.$vendor->organization.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Interview ID #: '.$interViewModel->id.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Interview Type : '.$interViewModel->interview_type.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Recommended Date : '.$interViewModel->interview_start_date.'
							</p></br>
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								Time Duration : '.$interViewModel->interview_start_date_time.' to '.$interViewModel->interview_end_date_time.'
							</p></br>
		
							<p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
								<span style="color:#333;font-size:13px;line-height:1.4">You are asked to make yourself avaiable for the interview call.</span></p>
		
							<p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
								Thanks,<br>  Webmaster - Simplifyvms
							</p>
						</div>
					</div>
		
					
					<div class="adL">
		
					</div>
				</div>
				<div class="adL">
				</div>
			</div>
		</body>';
	  
	  
	  
	$message = Yii::app()->sendgrid->createEmail();
	//shortcut to $message=new YiiSendGridMail($viewsPath);
	$message->setHtml($htmlbody)
		->setSubject($jobTitle)
		->addTo($email)
		->setFrom('alert@simplifyvms.net');
	Yii::app()->sendgrid->send($message);
	  
	  
	}
	
	public static function invoiceConsolidate() {
		return array(
			'0' => 'Draft',
			'1' => 'Pending',
			'2' => 'Payment Done',
		);
	}
	
	//email sent on workorder creation
	public static function emailWorkorder($email,$firstname,$lastname,$offerID){
		$Offermodel = Offer::model()->findByPk($offerID);
		$jobmodel = Job::model()->findByPk($Offermodel->job_id);
		$Vendor = Vendor::model()->findByPk($Offermodel->vendor_id);
		$Candidate = Candidates::model()->findByPk($Offermodel->candidate_id);
		$htmlbody = '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">
        <div style="background:#f7f7f7;font:13px; "Proxima Nova","Helvetica Neue",Arial,sans-serif;padding:2% 7%">
            <div>
            </div>
            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$firstname.' '.$lastname.',</h1>
					  Job ID : '.$jobmodel->id.'<br/>
					  JobName : '.$jobmodel->title.'<br/>
					  Candiate Name : '.$Candidate->first_name.' '.$Candidate->last_name.'<br/>
					  Vendor Name : '.$Vendor->organization.'<br/>
					  Start Date : '.$Offermodel->job_start_date.'<br/>
					  End Date : '.$Offermodel->job_end_date.'<br/>
					  Pay Rate : '.$Offermodel->offer_pay_rate.'<br/>
					  Bill Rate : '.$Offermodel->offer_bill_rate.'<br/>
					  Hour : '.$Offermodel->hours_per_week.'<br/>
					  Shift Time. : '.$Offermodel->shift.'<br/>
					  Over Time : '.$Offermodel->over_time.'<br/>
					  Double Time : '.$Offermodel->double_time.'<br/>
				  </div>
                </div>
            <div class="adL">
            </div>
        </div>
        <div class="adL">
        </div>
    </div>';
		$message = Yii::app()->sendgrid->createEmail();
		//shortcut to $message=new YiiSendGridMail($viewsPath);
		$message->setHtml($htmlbody)
			->setSubject($Offermodel->id.'/'.$jobmodel->title.'- Created Work Order')
			->addTo($email)
			->setFrom('account@simplifyvms.net');
		Yii::app()->sendgrid->send($message);
	}
	
	public static function extensionWFmembersTable($id,$flowName,$flowMembers){ ?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="form-group">
                    <label for="">Contract Work flow approval.</label>
                    <select name="ContractExtensionReq[contract_work_flow]" class="form-control" readonly="readonly">
                        <option value='<?php echo $id; ?>'><?php echo $flowName; ?></option>
                    </select>
                     <input type="hidden" name="contract_work_flow" id="contract_work_flow" value="<?php echo $id; ?>" />
                </div>

                <p class="m-t-10 m-b-10">Following are the Member for Approval</p>

                <table class="table table-striped table-hover table-white">
                    <thead>
                    <tr>
                        <th>Approval</th>
                        <th>Name</th>
                        <th>Department</th>
                        <th>Work Flow Order</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($flowMembers as $val ){
                        $clientdata = Client::model()->findByPk($val->client_id); ?>
                        <tr>
                            <td><?php echo $flowName; ?></td>
                            <td><?php echo $clientdata->first_name . ' ' . $clientdata->last_name; ?></td>
                            <td><?php echo $val->department_id; ?></td>
                            <td><?php echo $val->order_approve; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
		<?php }
		
    public static function extensionWFAcceptReject($id,$flowName,$flowMembers , $superClient, $extension_id,$contract_id){ ?>
 
                <div class="form-group">
                    <label for="">Contract Work flow approval.</label>
                    <br /><?php echo $flowName; ?>
                </div>

                <p class="m-t-10 m-b-10">Following are the Member for Approval</p>

                <table class="table table-striped table-hover table-white">
                    <thead>
                    <tr>
                        <th>Approval</th>
                        <th>Name</th>
                        <th>Department</th>
                        <th>Work Flow Order</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($flowMembers as $val ){
                        $clientdata = Client::model()->findByPk($val->client_id); 
						 $query = "select * from vms_contract_extension_approval where client_id=".$val->client_id." AND workflow_id = ".$id." AND extension_id = ".$extension_id;
		 				 $approvalStatus = Yii::app()->db->createCommand( $query )->query()->readAll(); 
  					?>
                        <tr>
                            <td><?php echo $flowName; ?></td>
                            <td><?php echo $clientdata->first_name . ' ' . $clientdata->last_name; ?></td>
                            <td><?php echo $val->department_id; ?></td>
                            <td><?php echo $val->order_approve; ?></td>
                            <td>
                             <?php if($superClient == Yii::app()->user->id) { 
										if($approvalStatus[0]['status'] == 1 || $approvalStatus[0]['status'] == 0)
											echo 'Pending Approval	';
										elseif($approvalStatus[0]['status'] == 2)
											echo 'Approved';
										elseif($approvalStatus[0]['status'] == 3)
											echo 'Rejected';
							 		}
							 	   else{
									   if($approvalStatus[0]['status'] == 1 && $approvalStatus[0]['client_id'] == Yii::app()->user->id) {?>
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/approveExtWorkflow', array('id' =>$approvalStatus[0]['id']  , 'workflow_id' => $id, 'client_id' =>$approvalStatus[0]['client_id'], 'order' => $val->order_approve, 'extension_id' =>$extension_id , 'contract_id'=>$contract_id)); ?>" class="btn btn-success small" style="padding:8px;">Accept</a> <a href="#modal-id"  style="padding:8px;" data-toggle="modal" class="btn btn-danger small">Reject</a> 
								<div class="modal fade" id="modal-id">
                                  <div class="modal-dialog">
                                    <form action="<?php echo Yii::app()->createAbsoluteUrl('Client/contract/approveExtWorkflow'); ?>" method="post">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Reject Extension</h4>
                                          </div>
                                          <div class="modal-body">
                                    
                                    
                                            <div class="form-group">
                                              <label for="">Reason for Bill Request Rejection</label>
                                              <select name="reason" id="reason" class="form-control" required="required">
                                                 <?php
                                                $reason = Setting::model()->findAll(array('condition'=>'category_id=63'));
                                                foreach($reason as $reason){ ?>
                                                  <option value="<?php echo $reason->id; ?>"><?php echo $reason->title; ?></option>
                                                  <?php  } 	?>
                                              </select>
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                              <label for="">Note for Rejection</label>
                                             <textarea name="note" id="note" class="form-control" rows="3" required="required"></textarea>
                                            </div>
                                            
                                          </div>
                                          <div class="modal-footer">
                                            <input type="hidden" value="<?php  echo $id ;?>" name="workflow_id" />
                                             <input type="hidden" value="<?php  echo $contract_id;?>" name="contract_id" />
                                            <input type="hidden" value="<?php  echo $approvalStatus[0]['id'] ;?>" name="id" />
                                            <input type="hidden" value="<?php  echo $approvalStatus[0]['client_id'] ;?>" name="client_id" />
                                            <input type="hidden" value="<?php  echo $val->order_approve ;?>" name="order" />
                                            <input type="hidden" value="<?php  echo $extension_id ;?>" name="extension_id" />
                                            <button type="submit" name="rejectbill" class="btn btn-success">Save</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>
                                    </form>
                                  </div>
                                </div>
				  <?php }
                            elseif($approvalStatus[0]['status'] == 0 ) {echo 'Pending Approval';}
                            elseif($approvalStatus[0]['status'] == 2 ){
                                  echo 'Approved';
                              }
                              elseif($approvalStatus[0]['status'] == 3 ){
                                  echo 'Rejected';
                              }else{ echo 'Pending Approval';}
                            ?></td>
                        </tr>
                    <?php } 
						}?>
                    </tbody>
                </table>
 		<?php }	  //extra space removed
}
?>