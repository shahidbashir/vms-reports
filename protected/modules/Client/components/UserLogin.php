<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserLogin extends CUserIdentity
{
	private $_id;
    public function authenticate()
    {
        $record=Client::model()->findByAttributes(array('email'=>$this->username));
		$password = md5($this->password);
		//echo $this->username;
		$candidate = Candidates::model()->findByAttributes(array('emp_official_email'=>$this->username));
		//print_r($candidate);
		if(!empty($candidate)) {
			$submission = VendorJobSubmission ::model()->findByAttributes(array('candidate_Id' => $candidate->id));
			//print_r($submission);
			$this->setState('submission_id', $submission->id);
			//exir;
		}else{
			$this->setState('submission_id', 0);
		} 
		
		if(isset($_POST['byAdminAsADoctor']) && $_POST['byAdminAsADoctor']=='byAdminAsADoctor') {
			 $password = $this->password;
			 $this->setState('roleAsAdmin','admin'); 
			 }
			 
        if($record===null && $record->profile_status !=1)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($record->password!==$password)
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else  if($record->profile_status_by == 'Admin')
        { 
            Yii::app()->session->add('error_status', 'Your account is disapproved, Please contact Administrator!');
           // Yii::app()->addError('username','Your account is disapproved, Please contact Administrator!');
        }     
        else
        {
            $this->_id = $record->id;
            $this->setState('title', $record->email);
			$this->setState('isAdmin', true);
            $this->setState('profile_approve', $record->profile_approve);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}