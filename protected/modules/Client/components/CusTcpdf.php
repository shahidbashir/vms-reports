<?php
//============================================================+
// File name   : example_003.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 003 for TCPDF class
//               Custom Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
include( Yii::getPathOfAlias('ext.tcpdf.tcpdf').'.php' );


// Extend the TCPDF class to create custom Header and Footer
class CusTcpdf extends TCPDF {

	public $pdfHeaderText='';
	public $pdfFooterText='';
	  
    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES.'logo_example.jpg';
        //$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
          $this->SetFont('helvetica', '', 15);
        // Title
       // $this->Cell(0, 15, '<< '.$this->pdfHeaderText.' >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->writeHTMLCell(0, 0, '', '', $this->pdfHeaderText, 0, 0, false, "L", true);
        //$this->writeHTML($this->pdfHeaderText,$tc=array(0,0,0), $lc=array(0,0,0));
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
		$this->writeHTMLCell(0, 0, '', '', $this->pdfFooterText."<br />Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, 0, false, "L", true);
        //$this->Cell(0, 10, "laskdfklsdjflksdjfsdf<br />Page ".$this->getAliasNumPage()."/".$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

