<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserAppLogin extends CUserIdentity
{
	private $_id;
    public function authenticate()
    {
        $record=Client::model()->findByAttributes(array('email'=>$_POST['email']));
		$password = md5($_POST['password']);
        
		if(isset($_POST['byAdminAsADoctor']) && $_POST['byAdminAsADoctor']=='byAdminAsADoctor') {
			 $password = $this->password;
			 $this->setState('roleAsAdmin','admin'); 
			 }
			 
        if($record===null && $record->profile_status !=1)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($record->password!==$password)
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id = $record->id;
            $this->setState('title', $record->email);
			$this->setState('isAdmin', true);
            $this->setState('profile_approve', $record->profile_approve);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}