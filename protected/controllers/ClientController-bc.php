<?php

class ClientController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $defaultAction = 'create';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionConfirmation()
	{
		$this->render('confirmation',array(
			'model'=>$this->loadModel(Yii::app()->session['client_id']),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Client;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model->scenario='signup1';

		if(isset($_POST['Client']))
		{
			$model->attributes=$_POST['Client'];
			$model->date_created=date('Y-m-d');
			$model->date_updated=date('Y-m-d');
			if($model->save()) {
				$this->clientEmail($model->id);
				Yii::app()->session['client_id'] = $model->id;
				$this->redirect(array('confirmation'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionResendactivation() {

		if( Yii::app()->session['client_id'] ) {
			$this->clientEmail(Yii::app()->session['client_id']);
			$this->redirect(array('confirmation'));
	     }else {
	     	$this->redirect(array('create'));
	     }

	}

   public function clientEmail($id) {

		 $info = Client::model()->findByPk($id);
          $customerEmail = $info->email;
          //$applicationUrl = $info->application_url;

          $htmlbody='<div align="center">
        <table border="0" cellspacing="0" cellpadding="0" width="650" style="background:white;border:solid #e0e0e0 1.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt">
          <tbody>
            <tr>
              <br valign="top"><h1>Hi,<u></u><u></u></h1>
              <p>Thanks for trying US VMS!</p></br>
                <p>To get started, please confirm your account by clicking the following link:: '.CHtml::link('Click Here',Yii::app()->createAbsoluteUrl('client/accountactiavtion',array('id'=>$id))).' <br />
                <p>Please do not reply to this email. If you are receiving this email in error, you can safely ignore it.</p></br>
                <p>The US Tec Solution team</p></br>
                <p></p>
                </td>
            </tr>
          </tbody>
        </table>
      </div> ';



            $message = Yii::app()->sendgrid->createEmail();
      //shortcut to $message=new YiiSendGridMail($viewsPath);
            $message->setHtml($htmlbody)
                ->setSubject('US Tech HelpDesk - Account Verification')
                ->addTo($customerEmail)
                ->setFrom('helpdesk@hypovps.in');
            Yii::app()->sendgrid->send($message);
   }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionAccountactiavtion($id)
	{
		$model=$this->loadModel($id);

		error_reporting(0);
        if( $model->profile_status==1){
        	$this->redirect(array('/Client/default/login'));
        }
		$model->profile_status = 1;
		if( $model->save(false)) {
			$model->scenario='create_password';
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);

			if(!empty($_POST['Client']['password']) && $_POST['Client']['password']==$_POST['Client_password_confirm'])
			{
				$model->password=md5($_POST['Client']['password']);
				if($model->validate() && $model->save())
					$this->redirect(array('Client/default'));
			}else if(isset($_POST['Client'])){
				Yii::app()->user->setFlash('success', "Password did not match!");
			}

			$this->render('accountactiavtion',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	

	public function loadModel($id)
	{
		$model=Client::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Client $model the model to be validated
	 */
	
}
