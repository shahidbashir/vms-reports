<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionCronejobReport(){
		//echo Yii::app()->basepath . '/../reports/excelreport.xls'; exit;
		$today = date('Y-m-d');
		$monthbackDate = date('Y-m-d', strtotime('-30 days', strtotime($today)));
		$weekbackDate = date('Y-m-d', strtotime('-7 days', strtotime($today)));
		$dayname = date('D', strtotime($today));
		//$model = CronejobReport::model()->findAll();
		$Criteria = new CDbCriteria();
		$Criteria->condition = "start_date < '".$today."' AND end_date > '".$today."'";
		$model = CronejobReport::model()->findAll($Criteria);
		foreach ($model as $value){
			if($value->run_frequency=='Monthly'){
				if($value->cronejob_date==$monthbackDate) {
					if($dayname==substr($value->run_day, 0, 3)) {
						$ReportGenerate = ReportGenerate::model()->findByPk($value->report_id);
						UtilityManager::CronejobReportEmail($ReportGenerate, $value->reciver);
						$value->cronejob_date = date('Y-m-d');
						$value->save(false);
					}
				}

			}
			if ($value->run_frequency=='Weekly'){
				if($value->cronejob_date==$weekbackDate) {
					if($dayname==substr($value->run_day, 0, 3)) {
						$ReportGenerate = ReportGenerate::model()->findByPk($value->report_id);
						UtilityManager::CronejobReportEmail($ReportGenerate, $value->reciver);
						$value->cronejob_date = date('Y-m-d');
						$value->save(false);
					}
				}

			}
			if ($value->cronejob_date=='0000-00-00') {
				$ReportGenerate = ReportGenerate::model()->findByPk($value->report_id);
				UtilityManager::CronejobReportEmail($ReportGenerate,$value->reciver);
				$value->cronejob_date = date('Y-m-d');
				$value->save(false);
			}
		}


	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionTestHelloApi(){
		return 'Hello API Event Received';
		}
	
	public function actionIndex()
	{
		$timestamp = date("H:i", time());
		//echo $timestamp;

		$modelClient=Client::model()->findall();
		foreach ($modelClient as $value){
			if($value->super_client_id ==0 ||$value->member_type=='Administrator'){
				$email = $value->email.'</br>';

		 $todayDate = date('Y-m-d');
		/** today three queries for job below**/
		$NumberofNewJobs = Job::model()->countByAttributes(array('jobStatus'=>11,'date_created'=>$todayDate));
		$PendingApproval = Job::model()->countByAttributes(array('jobStatus'=>1,'date_created'=>$todayDate));
		$NewOpen = Job::model()->countByAttributes(array('jobStatus'=>3,'date_created'=>$todayDate));

		/** today approved interview query below**/
		$count_queryforinterview = "select count(*) FROM vms_interview w where w.status = 2
		and ((w.interview_start_date ='".$todayDate."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$todayDate."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$todayDate."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$todayDate."' and w.alt3_date_status =1))";
		$todayinterview = Yii::app()->db->createCommand($count_queryforinterview)->queryScalar();

		/** today rejected interview query below**/
		$count_queryforrejected = "select count(*) FROM vms_interview w where w.status = 3
		and ((w.interview_start_date ='".$todayDate."' and w.start_date_status =1) or (w.interview_alt1_start_date ='".$todayDate."' and w.alt1_date_status =1 ) or (w.interview_alt2_start_date ='".$todayDate."' and w.alt2_date_status =1) or (w.interview_alt3_start_date ='".$todayDate."' and w.alt3_date_status =1))";
		$todayrejectedinterview = Yii::app()->db->createCommand($count_queryforrejected)->queryScalar();

		/** today release offer**/
		$criteria = new CDbCriteria;
		$criteria->condition = "status=4";
		$criteria->compare('DATE_FORMAT(date_created,"%Y-%m-%d")',$todayDate,true);
		$models = Offer::model()->findAll($criteria);
		$todatReleasoffer = count($models);
		/** today accepted offer**/
		$Criteria = new CDbCriteria;
		$Criteria->condition = "status=1";
		$Criteria->compare('DATE_FORMAT(date_modified,"%Y-%m-%d")',$todayDate,true);
		$models = Offer::model()->findAll($Criteria);
		$todatacceptedoffer = count($models);

		/** today relaes workorder**/
		$Criteria = new CDbCriteria;
		$Criteria->condition = "workorder_status=0";
		$Criteria->compare('DATE_FORMAT(date_created,"%Y-%m-%d")',$todayDate,true);
		$models = Workorder::model()->findAll($Criteria);
		$todatreleaseworkorder = count($models);

		/** today accepted workorder**/
		$Criteria = new CDbCriteria;
		$Criteria->condition = "workorder_status=1";
		$Criteria->compare('DATE_FORMAT(accept_date,"%Y-%m-%d")',$todayDate,true);
		$models = Workorder::model()->findAll($Criteria);
		$todayacceptedworkorder = count($models);


		/** today submission**/
		$Criteria = new CDbCriteria;
		$Criteria->compare('DATE_FORMAT(date_created,"%Y-%m-%d")',$todayDate,true);
		$models = VendorJobSubmission::model()->findAll($Criteria);
		$todaysubmission = count($models);

		/** today submission client review**/
		$Criteria = new CDbCriteria;
		$Criteria->condition = "resume_status=4";
		$Criteria->compare('DATE_FORMAT(date_created,"%Y-%m-%d")',$todayDate,true);
		$models = VendorJobSubmission::model()->findAll($Criteria);
		$todayclientreview = count($models);
		$htmlbody =  '<body>
    <div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px \'Proxima Nova\',\'Helvetica Neue\',Arial,sans-serif;padding:2% 7%">
            <div>
                
            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hello '.$value->first_name.' '.$value->last_name.',</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">Today Report : '.$todayDate.'</p>

                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: center; padding-bottom: 60px; padding-top:30px; ">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$NumberofNewJobs.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">New Jobs</span>
                                
                            </td>

                            <td style="text-align: center; padding-bottom: 60px; ">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$PendingApproval.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">Pending Approval </span>
                                
                            </td>

                            <td style="text-align: center; padding-bottom: 60px; ">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$NewOpen.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">New Open Job Released</span>
                                
                            </td>
                        </tr>
                         <tr>
                            <td style="text-align: center; padding-bottom: 60px; ">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$todayinterview.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">Today Interview </span>
                                
                            </td>

                            <td style="text-align: center; padding-bottom: 60px;">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$todayrejectedinterview.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">Today Cancelled Interview</span>
                                
                            </td>

                            <td style="text-align: center; padding-bottom: 60px;">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$todatReleasoffer.' </span>
                                <span style="display: block; font-size: 16px; color: #666666;">Offer Released</span>
                                
                            </td>
                        </tr>


                        <tr>
                            <td style="text-align: center; padding-bottom: 60px; ">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$todatacceptedoffer.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">Offer Accepted</span>
                                
                            </td>

                            <td style="text-align: center; padding-bottom: 60px;">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$todatreleaseworkorder.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">Work Order Released</span>
                                
                            </td>

                            <td style="text-align: center; padding-bottom: 60px;">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$todayacceptedworkorder.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">Work Order Accepted</span>
                                
                            </td>
                        </tr>

                         <tr>
                            <td style="text-align: center; padding-bottom: 60px; ">
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$todaysubmission.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">Today Submission </span>
                                
                            </td>
                            <td>
                                
                            </td>

                            <td style="text-align: center; padding-bottom: 60px;" >
                                <span style="display: block; font-size: 40px; color: #FFAE3C; margin-bottom: 6px; ">'.$todayclientreview.'</span>
                                <span style="display: block; font-size: 16px; color: #666666;">Submission On Client Review</span>
                                
                            </td>
                        </tr>
                    </table>
                   

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
                        Thanks,<br>Simplify VMS
                    </p>
                </div>
            </div>

            
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>
</body>';
		$message = Yii::app()->sendgrid->createEmail();
		$message->setHtml($htmlbody)
			->setSubject('Today Summery')
			->addTo($email)
			->setFrom('alert@simplifyvms.net');
		Yii::app()->sendgrid->send($message);
			}
		}
		exit;
		//$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$this->redirect(Yii::app()->user->returnUrl);
				unset($this->_errors[$attribute]);
			}
				
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}