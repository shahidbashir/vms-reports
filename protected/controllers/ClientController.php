<?php
class ClientController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	//public $defaultAction = 'create';
	public $defaultAction = 'redirectLogin';
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionRedirectLogin(){
		$this->redirect(array('Client/default/login'));
		}
	public function actionConfirmation()
	{
		$this->render('confirmation',array(
			'model'=>$this->loadModel(Yii::app()->session['client_id']),
		));
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Client;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model->scenario='signup1';
		if(isset($_POST['Client']))
		{
			$model->attributes=$_POST['Client'];
			$model->date_created=date('Y-m-d');
			$model->date_updated=date('Y-m-d');
			
			if($model->save()) {
				UtilityManager::emailByTemplate($model->id,'Client','client-creation');
				Yii::app()->session['client_id'] = $model->id;
				$this->redirect(array('confirmation'));
			}
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}
	public function actionResendactivation() {
		if( Yii::app()->session['client_id'] ) {
			UtilityManager::emailByTemplate(Yii::app()->session['client_id'],'Client','client-creation');
			//$this->clientEmail(Yii::app()->session['client_id']);
			$this->redirect(array('confirmation'));
	     }else {
	     	$this->redirect(array('create'));
	     }
	}
	
	
	
	//ali's action of forgot password       and now this function is moved to the default controler
	public function actionForgotPassword() {
        $model=new Client;
		$model->scenario='create_password';
			if(!empty($_POST['Client']['email']))
			{
				$client=Client::model()->findByAttributes(array('email'=>$_POST['Client']['email']));
				if($client) {
					UtilityManager::emailByTemplate($client->id,'Client','client-forgot-password');
					Yii::app()->user->setFlash('success', "Check your email for details");
				}else {
					Yii::app()->user->setFlash('success', "Email did not find!");
				}
			}else if(isset($_POST['Client'])){
				Yii::app()->user->setFlash('success', "Email did not find!");
			}
			$this->render('forgotpassword',array(
				'model'=>$model,
			));
	}
	public function actionResetpassword()
	{
		$this->layout = 'login';
        if( isset($_GET['client_id'])) {
        	//$id =  UrlEnDeCode::getIdDecoded($_GET['client_id']);
        	$search = array('s', 'l', 'm', 'k', 'z','n','o','q','t','d');
		    $replace  = array(0,1, 2, 3, 4, 5,6,7,8,9);
		
		    $id = str_replace($search, $replace, $_GET['client_id']);
        }
        $model=$this->loadModel($id);
       
			
		$model->scenario='create_password';
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
			if(!empty($_POST['Client']['password']) && $_POST['Client']['password']==$_POST['Client_password_confirm'])
			{
				$model->password=md5($_POST['Client']['password']);
				if($model->validate() && $model->save()) {
					$this->redirect(array('Client/default'));
				}
			}else if(isset($_POST['Client'])){
				Yii::app()->user->setFlash('success', "Password did not match!");
			}
			$this->render('accountactiavtion',array(
				'model'=>$model,
			));
		
	}
   public function clientEmail($id) {
		$info = Client::model()->findByPk($id);
		$emailTemp = EmailTemplate::model()->findByPk(1);
        $customerEmail = $info->email;
        $link = CHtml::link($this->createAbsoluteUrl('client/accountactiavtion',array('client_id'=>$id)),$this->createAbsoluteUrl('client/accountactiavtion',array('client_id'=>$id)));
       
        $content = str_replace("{{name}}", $info->first_name.' '.$info->last_name, $emailTemp->content);
        $content = str_replace("{{url of the activation}}", $link, $content);
          //$applicationUrl = $info->application_url;
       
            $message = Yii::app()->sendgrid->createEmail();
      //shortcut to $message=new YiiSendGridMail($viewsPath);
            $message->setHtml($content)
                ->setSubject($emailTemp->subject_line)
                ->addTo($customerEmail)
                ->setFrom($emailTemp->from_email);
            Yii::app()->sendgrid->send($message);
   }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionAccountactiavtion()
	{
		$this->layout = 'login';
        if( isset($_GET['client_id'])) {
        	$search = array('s', 'l', 'm', 'k', 'z','n','o','q','t','d');
		    $replace  = array(0,1, 2, 3, 4, 5,6,7,8,9);
		
		    $id = str_replace($search, $replace, $_GET['client_id']);
        }
        $model=$this->loadModel($id);
		/*echo 'i am out';
		exit;*/
         
		//error_reporting(0);
        if(!empty($_POST['Client']['password'])/* && $model->profile_status==1*/){
        	//$this->redirect(array('/Client/default/login'));
        }
		$model->profile_status = 1;
		if( $model->save(false)) {
		//setting this condition that team member never get activation email.
		//if($model->super_client_id == 0)
		if($model->super_client_id == 0){
            if(!isset($_POST['Client'])) {
				UtilityManager::emailByTemplate($model->id,'Client','client-activation');
				
			}
		}
			$model->scenario='create_password';
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
			if(!empty($_POST['Client']['password']) && $_POST['Client']['password']==$_POST['Client_password_confirm'])
			{
				$model->password=md5($_POST['Client']['password']);
				if($model->validate() && $model->save()) {
					$this->redirect(array('Client/default'));
				}
			}else if(isset($_POST['Client'])){
				Yii::app()->user->setFlash('success', "Password did not match!");
			}
			$this->render('accountactiavtion',array(
				'model'=>$model,
			));
		}
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function loadModel($id)
	{
		$model=Client::model()->findByPk($id);
		if($model===null)
			$this->redirect(array('Client/default'));
		return $model;
	}
	/**
	 * Performs the AJAX validation.
	 * @param Client $model the model to be validated
	 */
	
}
