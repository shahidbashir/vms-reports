<?php
class UtilityManager {

    public static function CronejobReportEmail($ReportGenerate,$email){

        if($ReportGenerate['formate'] == 'Excel'){
           /* header( "Content-Type: application/vnd.ms-excel" );
            header( "Content-disposition: attachment; filename=Report.xls" );*/
            // for example:
            $data =  'S.No'
                . "\t"  . 'Job Id'
                . "\t"  . 'Candidate Id'
                . "\t"  . 'Candidate Name'
                . "\t"  . 'Canddiate Email'
                . "\t"  . 'Start Date'
                . "\t"  . 'End Date'
                . "\t"  . 'Bill Rate'
                . "\t"  . 'Pay Rate'
                . "\t"  . 'Hiring Mgr Name'
                . "\t"  . 'Hiring Mg Department'
                . "\t"  . 'Supplier Name'
                . "\t"  . 'Location'
                . "\t"  . 'Termination Date'
                . "\t"  . 'Status'
                . "\t"  . 'Job Title'
                . "\t"  . 'Job Category'
                . "\n";

            $whereClause = '';
            if(isset($ReportGenerate->job_category) && $ReportGenerate->job_category !==''){
                $whereClause .= " WHERE job_category = '". $ReportGenerate->job_category. "'";
            }

            if(isset($ReportGenerate->location) && $ReportGenerate->location !==''){
                //$location = Location::model()->findByPk($ReportGenerate->location);
                $whereClause .= " and location = '". $ReportGenerate->location. "'";
            }
            if(isset($ReportGenerate->department) && $ReportGenerate->department !==''){
                $whereClause .= " and hiring_mgr_department = '". $ReportGenerate->department."'";
            }
            if(isset($ReportGenerate->supplier) && $ReportGenerate->supplier !==''){
                $whereClause .= " and supplier_name = '". $ReportGenerate->supplier."'";
            }

            $sql = 'SELECT * FROM rpt_headcount_current '.$whereClause.'';
            $headcountData = Yii::app()->db->createCommand($sql)->query()->readAll();
            $i = 0;
            foreach ($headcountData as $value) {
                $i++;
               $data .=  $i
                    . "\t" . $value['job_id']
                    . "\t" . $value['candidate_id']
                    . "\t" . $value['candidate_name']
                    . "\t" . $value['candidate_email']
                    . "\t" . $value['start_date']
                    . "\t" . $value['end_date']
                    . "\t" . $value['bill_rate']
                    . "\t" . $value['pay_rate']
                    . "\t" . $value['hiring_mgr_name']
                    . "\t" . $value['hiring_mgr_department']
                    . "\t" . $value['supplier_name']
                    . "\t" . $value['location']
                    . "\t" . $value['termination_date']
                    . "\t" . $value['Status']
                    . "\t" . $value['job_title']
                    . "\t" . $value['job_category']
                    . "\n";
            }
            $basePath = Yii::app()->basePath;
            file_put_contents($basePath.'/../reports/excelreport.xls', $data);

            $htmlbody = '<div style="font-size:13px;line-height:1.4;margin:0;padding:0">

        <div style="background:#f7f7f7;font:13px; "Proxima Nova","Helvetica Neue",Arial,sans-serif;padding:2% 7%">
            <div>

            </div>


            <div style="background:#fff;border-top-color:#ffa800;border-top-style:solid;border-top-width:4px;margin:25px auto">
                <div style="border-color:#e5e5e5;border-style:none solid solid;border-width:2px;padding:7%">
                    <h1 style="color:#333;font-size:17px;line-height:1.4;margin:0 0 20px">Hi,</h1>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:20px 0">
                    <span style="color:#333;font-size:13px;line-height:1.4">Please find the Attachment</span><br>
                   
      				</p>

                    <p style="color:#333;font-size:13px;line-height:1.4;margin:40px 0 20px">
   Regard <br> Webmaster - SimplifyVMS
   </p>
  </div>
                </div>
            <div class="adL">

            </div>
        </div>
        <div class="adL">
        </div>
    </div>';
            $path = Yii::app()->basePath . '/../reports/excelreport.xls';
            $directoryPath =dirname(__FILE__).'/../../vendor/autoload.php';
            require_once($directoryPath);
            $mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.sendgrid.net';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'hypovps.in';                 // SMTP username
            $mail->Password = 'yahoo2015';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            $mail->setFrom('account@simplifyvms.net', 'Report');
            $mail->addAddress($email);     // Add a recipient

            $mail->addAttachment($path);         // Add attachments
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = 'Reports Alert';
            $mail->Body    = $htmlbody;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();

        }
    }
	
   public static function emailByTemplate($id,$sendToType,$emailType='client-creation') {
        if($sendToType == 'Client') { 
    			$info = Client::model()->findByPk($id);
          $emailTemp = EmailTemplate::model()->findByAttributes(array('type'=>$emailType));
         
    			$customerEmail = $info->email;
          $urlID = UrlEnDeCode::getIdDecoded($id); 
          $search  = array(0,1, 2, 3, 4, 5,6,7,8,9);
          $replace = array('s', 'l', 'm', 'k', 'z','n','o','q','t','d');
          $urlID = str_replace($search, $replace, $id);
          if($emailType == 'client-forgot-password') {
            	$link = CHtml::link(Yii::app()->createAbsoluteUrl('client/resetpassword',array('client_id'=>$urlID)),Yii::app()->createAbsoluteUrl('client/resetpassword',array('client_id'=>$urlID)));
            }else {
              $link = CHtml::link(Yii::app()->createAbsoluteUrl('client/accountactiavtion',array('client_id'=>$urlID)),Yii::app()->createAbsoluteUrl('client/accountactiavtion',array('client_id'=>$urlID)));
            }
       
           
          $content = str_replace("{{name}}", $info->first_name.' '.$info->last_name, $emailTemp->content);
          $content = str_replace("{{url of the activation}}", $link, $content);
          $cc = $emailTemp->cc_email_ID;
          $bcc = $emailTemp->bcc_email_ID;
          $fromName =  $emailTemp->from_email_ID;
	     }
        
          //$applicationUrl = $info->application_url;
           //echo "<pre>";var_dump($id.$sendToType.$emailType); var_dump($content);print_r($info);exit;
            $message = Yii::app()->sendgrid->createEmail();
      //shortcut to $message=new YiiSendGridMail($viewsPath);
            $message->setHtml($content)
                ->setSubject($emailTemp->subject_line)
                ->addTo($customerEmail)
                ->addTo($cc)
                ->addBcc($bcc)
                ->setFromName($fromName)
                ->setFrom($emailTemp->from_email);
            Yii::app()->sendgrid->send($message);
   }
 	//field name "bill_req_apprej_by" in vms_contract_extension_req table
	 public static function vendorBillRequestApproval() {
			return array(
	   			'0' => '',
				'1' => 'Pending',
				'2' => 'Approved',
	   			'3' => 'Rejected',
			);  
	 }
	 
	 //field name "bill_request_status" in vms_contract_extension_req table
 public static function billChangeRequestByVendor() {
        return array(
   			'0' => '',
            '1' => 'Pending',
            '2' => 'Approved',
   			'3' => 'Rejected',
        );
    }
}