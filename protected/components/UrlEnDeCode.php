<?php

  class UrlEnDeCode { 
   
   public static function getIdEncoded($number) {
	  /* 
	     0==s
	     1==l;
		 2==m;
		 3==k;
		 4==z;
		 5==n;
		 6==o;
		 7==q;
		 8==t;
		 9==d
		 
	  */
	  
		$search  = array(0,1, 2, 3, 4, 5,6,7,8,9);
		$replace = array('s', 'l', 'm', 'k', 'z','n','o','q','t','d');
		
		return str_replace($search, $replace, $number);
	}
	
	public static function getIdDecoded($number) {
	
	 /* 
	     0==s
	     1==l;
		 2==m;
		 3==k;
		 4==z;
		 5==n;
		 6==o;
		 7==q;
		 8==t;
		 9==d
		 
	  */

		$search = array('s', 'l', 'm', 'k', 'z','n','o','q','t','d');
		$replace  = array('0','1', '2', '3', '4', '5','6','7','8','9');
		
		return str_replace($search, $replace, $number);
	} 
  
  }