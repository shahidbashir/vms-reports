<?php
/* @var $this OfferController */
/* @var $data Offer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('submissio_id')); ?>:</b>
	<?php echo CHtml::encode($data->submissio_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by_id')); ?>:</b>
	<?php echo CHtml::encode($data->created_by_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by_type')); ?>:</b>
	<?php echo CHtml::encode($data->created_by_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_by_id')); ?>:</b>
	<?php echo CHtml::encode($data->modified_by_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_by_type')); ?>:</b>
	<?php echo CHtml::encode($data->modified_by_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('rates_regular')); ?>:</b>
	<?php echo CHtml::encode($data->rates_regular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rates_extra')); ?>:</b>
	<?php echo CHtml::encode($data->rates_extra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rates_status')); ?>:</b>
	<?php echo CHtml::encode($data->rates_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('internal_notes')); ?>:</b>
	<?php echo CHtml::encode($data->internal_notes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('for_admin_notes')); ?>:</b>
	<?php echo CHtml::encode($data->for_admin_notes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('issued_offer_date')); ?>:</b>
	<?php echo CHtml::encode($data->issued_offer_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valid_till_date')); ?>:</b>
	<?php echo CHtml::encode($data->valid_till_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
	<?php echo CHtml::encode($data->date_created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_modified')); ?>:</b>
	<?php echo CHtml::encode($data->date_modified); ?>
	<br />

	*/ ?>

</div>