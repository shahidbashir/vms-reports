<?php
/* @var $this OfferController */
/* @var $model Offer */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'submissio_id'); ?>
		<?php echo $form->textField($model,'submissio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by_id'); ?>
		<?php echo $form->textField($model,'created_by_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by_type'); ?>
		<?php echo $form->textField($model,'created_by_type',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified_by_id'); ?>
		<?php echo $form->textField($model,'modified_by_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified_by_type'); ?>
		<?php echo $form->textField($model,'modified_by_type',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rates_regular'); ?>
		<?php echo $form->textField($model,'rates_regular',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rates_extra'); ?>
		<?php echo $form->textField($model,'rates_extra',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rates_status'); ?>
		<?php echo $form->textField($model,'rates_status',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'internal_notes'); ?>
		<?php echo $form->textArea($model,'internal_notes',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'for_admin_notes'); ?>
		<?php echo $form->textField($model,'for_admin_notes'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'issued_offer_date'); ?>
		<?php echo $form->textField($model,'issued_offer_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valid_till_date'); ?>
		<?php echo $form->textField($model,'valid_till_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_modified'); ?>
		<?php echo $form->textField($model,'date_modified'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->