<?php
/* @var $this OfferController */
/* @var $model Offer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'offer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'submissio_id'); ?>
		<?php echo $form->textField($model,'submissio_id'); ?>
		<?php echo $form->error($model,'submissio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by_id'); ?>
		<?php echo $form->textField($model,'created_by_id'); ?>
		<?php echo $form->error($model,'created_by_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by_type'); ?>
		<?php echo $form->textField($model,'created_by_type',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'created_by_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified_by_id'); ?>
		<?php echo $form->textField($model,'modified_by_id'); ?>
		<?php echo $form->error($model,'modified_by_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified_by_type'); ?>
		<?php echo $form->textField($model,'modified_by_type',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'modified_by_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rates_regular'); ?>
		<?php echo $form->textField($model,'rates_regular',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'rates_regular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rates_extra'); ?>
		<?php echo $form->textField($model,'rates_extra',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'rates_extra'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rates_status'); ?>
		<?php echo $form->textField($model,'rates_status',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'rates_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'internal_notes'); ?>
		<?php echo $form->textArea($model,'internal_notes',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'internal_notes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'for_admin_notes'); ?>
		<?php echo $form->textField($model,'for_admin_notes'); ?>
		<?php echo $form->error($model,'for_admin_notes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'issued_offer_date'); ?>
		<?php echo $form->textField($model,'issued_offer_date'); ?>
		<?php echo $form->error($model,'issued_offer_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valid_till_date'); ?>
		<?php echo $form->textField($model,'valid_till_date'); ?>
		<?php echo $form->error($model,'valid_till_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created'); ?>
		<?php echo $form->error($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_modified'); ?>
		<?php echo $form->textField($model,'date_modified'); ?>
		<?php echo $form->error($model,'date_modified'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->