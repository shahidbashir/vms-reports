<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
<title></title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      
      <?php echo $content; ?>
      
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/js/custom.js"></script>
<!--Style Switcher -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/theme-assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
