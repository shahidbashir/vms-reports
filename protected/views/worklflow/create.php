<?php
/* @var $this WorklflowController */
/* @var $model Worklflow */

$this->breadcrumbs=array(
	'Worklflows'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Worklflow', 'url'=>array('index')),
	array('label'=>'Manage Worklflow', 'url'=>array('admin')),
);
?>

<h1>Create Worklflow</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>