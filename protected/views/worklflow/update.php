<?php
/* @var $this WorklflowController */
/* @var $model Worklflow */

$this->breadcrumbs=array(
	'Worklflows'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Worklflow', 'url'=>array('index')),
	array('label'=>'Create Worklflow', 'url'=>array('create')),
	array('label'=>'View Worklflow', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Worklflow', 'url'=>array('admin')),
);
?>

<h1>Update Worklflow <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>