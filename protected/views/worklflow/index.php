<?php
/* @var $this WorklflowController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Worklflows',
);

$this->menu=array(
	array('label'=>'Create Worklflow', 'url'=>array('create')),
	array('label'=>'Manage Worklflow', 'url'=>array('admin')),
);
?>

<h1>Worklflows</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
