<?php
/* @var $this WorklflowController */
/* @var $data Worklflow */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flow_name')); ?>:</b>
	<?php echo CHtml::encode($data->flow_name); ?>
	<br />


</div>