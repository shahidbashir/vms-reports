<?php
/* @var $this WorklflowController */
/* @var $model Worklflow */

$this->breadcrumbs=array(
	'Worklflows'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Worklflow', 'url'=>array('index')),
	array('label'=>'Create Worklflow', 'url'=>array('create')),
	array('label'=>'Update Worklflow', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Worklflow', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Worklflow', 'url'=>array('admin')),
);
?>

<h1>View Worklflow #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'flow_name',
	),
)); ?>
