<?php
/* @var $this WorklflowMemberController */
/* @var $data WorklflowMember */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workflow_id')); ?>:</b>
	<?php echo CHtml::encode($data->workflow_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('member_id')); ?>:</b>
	<?php echo CHtml::encode($data->member_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_status')); ?>:</b>
	<?php echo CHtml::encode($data->job_status); ?>
	<br />


</div>