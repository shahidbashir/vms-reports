<?php
/* @var $this WorklflowMemberController */
/* @var $model WorklflowMember */

$this->breadcrumbs=array(
	'Worklflow Members'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List WorklflowMember', 'url'=>array('index')),
	array('label'=>'Manage WorklflowMember', 'url'=>array('admin')),
);
?>

<h1>Create WorklflowMember</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>