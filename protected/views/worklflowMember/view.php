<?php
/* @var $this WorklflowMemberController */
/* @var $model WorklflowMember */

$this->breadcrumbs=array(
	'Worklflow Members'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List WorklflowMember', 'url'=>array('index')),
	array('label'=>'Create WorklflowMember', 'url'=>array('create')),
	array('label'=>'Update WorklflowMember', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete WorklflowMember', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WorklflowMember', 'url'=>array('admin')),
);
?>

<h1>View WorklflowMember #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'workflow_id',
		'member_id',
		'job_status',
	),
)); ?>
