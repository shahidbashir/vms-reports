<?php
/* @var $this WorklflowMemberController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Worklflow Members',
);

$this->menu=array(
	array('label'=>'Create WorklflowMember', 'url'=>array('create')),
	array('label'=>'Manage WorklflowMember', 'url'=>array('admin')),
);
?>

<h1>Worklflow Members</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
