<?php
/* @var $this WorklflowMemberController */
/* @var $model WorklflowMember */

$this->breadcrumbs=array(
	'Worklflow Members'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List WorklflowMember', 'url'=>array('index')),
	array('label'=>'Create WorklflowMember', 'url'=>array('create')),
	array('label'=>'View WorklflowMember', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage WorklflowMember', 'url'=>array('admin')),
);
?>

<h1>Update WorklflowMember <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>