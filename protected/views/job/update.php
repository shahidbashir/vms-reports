<?php
/* @var $this JobController */
/* @var $model Job */

$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Job', 'url'=>array('index')),
	array('label'=>'Create Job', 'url'=>array('create')),
	array('label'=>'View Job', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Job', 'url'=>array('admin')),
);
?>

<?php /*?><h1>Update Job <?php echo $model->id; ?></h1><?php */?>
<div class="db-content-inner">
  <div class="db-content">
    <div class="db-g-header">
      <h4>Update "<?php echo $model->title; ?>" Job Description.</h4>
      <a href="<?php echo Yii::app()->createAbsoluteUrl('job/jobListing'); ?>">Back to Job Listing</a> </div>
		<?php $this->renderPartial('_form', array('model'=>$model)); ?>

	</div>
  <!-- db-content --> 
</div>