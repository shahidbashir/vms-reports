<div class="db-content-inner">
  <div class="db-content">
    <div class="job-listing-header">
      <p>Currently we have 24 Active Jobs</p>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search…">
        <span class="input-group-btn">
        <button type="button" class="btn btn-default">Go</button>
        </span> <i class="glyphicon glyphicon-search"></i> </div>
    </div>
    <!-- job-listing-header -->
    
    <div class="job-listing">
      <div class="job-listing-top">
        <p class="closing">Closing Soon: 10th May 2015</p>
        <ul class="list-unstyle navbar-right">
          <li class="dropdown btn btn-default"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sort Job By <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#">Closing Soon</a></li>
              <li><a href="#">New Jobs</a></li>
              <li><a href="#">Closed Jobs</a></li>
              <li><a href="#">Pending Job Approval</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- job-listing-top -->
      
      <div class="jobs">
        <div class="job">
          <div class="job-title">
            <h4><a href="">CL-35485 Senior UX Designer - NYC</a></h4>
            <p>Location: NY, Dallas.</p>
          </div>
          <div class="job-meta">
            <div class="top row no-margin">
              <div class="top-inner">
                <div class="col-xs-7 no-padding">
                  <div class="label label-green">A</div>
                  <span>23 Submission</span>
                  <div class="salary"> $45.00 - $55.00 </div>
                </div>
                <div class="col-xs-5 no-padding">
                  <ul class="list-inline pull-right">
                    <li><a href="">Edit</a></li>
                    <li><a href="">Delete</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="bottom clearfix">
              <div class="job-post">Account Manager: Blake William</div>
              <a href="" class="pull-right">2 Openigns</a> </div>
          </div>
        </div>
        <!-- job -->
        
        <div class="job">
          <div class="job-title">
            <h4><a href="">CL-35485 Senior UX Designer - NYC</a></h4>
            <p>Location: NY, Dallas.</p>
          </div>
          <div class="job-meta">
            <div class="top row no-margin">
              <div class="top-inner">
                <div class="col-xs-7 no-padding">
                  <div class="label label-default">W</div>
                  <span>23 Submission</span>
                  <div class="salary"> $45.00 - $55.00 </div>
                </div>
                <div class="col-xs-5 no-padding">
                  <ul class="list-inline pull-right">
                    <li><a href="">Edit</a></li>
                    <li><a href="">Delete</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="bottom clearfix">
              <div class="job-post">Account Manager: Blake William</div>
              <a href="" class="pull-right">2 Openigns</a> </div>
          </div>
        </div>
        <!-- job -->
        
        <div class="job">
          <div class="job-title">
            <h4><a href="">CL-35485 Senior UX Designer - NYC</a></h4>
            <p>Location: NY, Dallas.</p>
          </div>
          <div class="job-meta">
            <div class="top row no-margin">
              <div class="top-inner">
                <div class="col-xs-7 no-padding">
                  <div class="label label-green">A</div>
                  <span>23 Submission</span>
                  <div class="salary"> $45.00 - $55.00 </div>
                </div>
                <div class="col-xs-5 no-padding">
                  <ul class="list-inline pull-right">
                    <li><a href="">Edit</a></li>
                    <li><a href="">Delete</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="bottom clearfix">
              <div class="job-post">Account Manager: Blake William</div>
              <a href="" class="pull-right">2 Openigns</a> </div>
          </div>
        </div>
        <!-- job -->
        
        <div class="job">
          <div class="job-title">
            <h4><a href="">CL-35485 Senior UX Designer - NYC</a></h4>
            <p>Location: NY, Dallas.</p>
          </div>
          <div class="job-meta">
            <div class="top row no-margin">
              <div class="top-inner">
                <div class="col-xs-7 no-padding">
                  <div class="label label-default">W</div>
                  <span>23 Submission</span>
                  <div class="salary"> $45.00 - $55.00 </div>
                </div>
                <div class="col-xs-5 no-padding">
                  <ul class="list-inline pull-right">
                    <li><a href="">Edit</a></li>
                    <li><a href="">Delete</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="bottom clearfix">
              <div class="job-post">Account Manager: Blake William</div>
              <a href="" class="pull-right">2 Openigns</a> </div>
          </div>
        </div>
        <!-- job --> 
        
      </div>
      <!-- jobs -->
      
      <h4 class="bold">New Job Posting</h4>
      <div class="jobs">
        <div class="job">
          <div class="job-title">
            <h4><a href="">CL-35485 Senior UX Designer - NYC</a></h4>
            <p>Location: NY, Dallas.</p>
          </div>
          <div class="job-meta">
            <div class="top row no-margin">
              <div class="top-inner">
                <div class="col-xs-7 no-padding">
                  <div class="label label-green">A</div>
                  <span>23 Submission</span>
                  <div class="salary"> $45.00 - $55.00 </div>
                </div>
                <div class="col-xs-5 no-padding">
                  <ul class="list-inline pull-right">
                    <li><a href="">Edit</a></li>
                    <li><a href="">Delete</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="bottom clearfix">
              <div class="job-post">Account Manager: Blake William</div>
              <a href="" class="pull-right">2 Openigns</a> </div>
          </div>
        </div>
        <!-- job -->
        
        <div class="job">
          <div class="job-title">
            <h4><a href="">CL-35485 Senior UX Designer - NYC</a></h4>
            <p>Location: NY, Dallas.</p>
          </div>
          <div class="job-meta">
            <div class="top row no-margin">
              <div class="top-inner">
                <div class="col-xs-7 no-padding">
                  <div class="label label-default">W</div>
                  <span>23 Submission</span>
                  <div class="salary"> $45.00 - $55.00 </div>
                </div>
                <div class="col-xs-5 no-padding">
                  <ul class="list-inline pull-right">
                    <li><a href="">Edit</a></li>
                    <li><a href="">Delete</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="bottom clearfix">
              <div class="job-post">Account Manager: Blake William</div>
              <a href="" class="pull-right">2 Openigns</a> </div>
          </div>
        </div>
        <!-- job --> 
        
      </div>
      <div class="job-pagination">
        <ul class="pagination pull-left">
          <li><a href="#"><i class="fa fa-long-arrow-left"></i> Older</a></li>
        </ul>
        <ul class="pagination pull-right">
          <li><a href="#">Newer <i class="fa fa-long-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
    <!-- job-listing --> 
    
  </div>
  <!-- db-content --> 
  
</div>
<!-- db-content-inner -->