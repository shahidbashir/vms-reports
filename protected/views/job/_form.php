<div class="add-job">
      <?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'job-form',
				'enableAjaxValidation'=>false,
			)); ?>
            
            <?php echo $form->errorSummary($model); ?>
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="">Job Title</label>
              <?php echo $form->textField($model,'title',array('class'=>'form-control')); ?>
              <!--<input type="text" class="form-control" id="" placeholder="">-->
            </div>
          </div>
          <!-- col-12 -->
          
          <div class="col-xs-12 col-sm-6">
            <label for="" class="">Category</label>
            <div class="single">
            	 <?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=9')),'title', 'title');
                   echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
              <?php /*?><select class="ui fluid search dropdown" name="Job[cat_id]">
                <option value=""></option>
                <option value="Php Developer">Php Developer</option>
                <option value="Asp Developer">Asp Developer</option>
                <option value="Web Designer">Web Designer</option>
              </select><?php */?>
            </div>
          </div>
          <!-- col-12 --> 
          
        </div>
        <!-- row -->
        
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            <div class="form-group">
              <label for="">Skills</label>
              <?php //echo $form->textArea($model,'skills',array('rows'=>6, 'cols'=>50)); ?>
              <input type="text" class="form-control" id="job_skills" placeholder="" name="Job[skills]">
              <!--<input type="text" class="form-control" id="" placeholder="">-->
            </div>
          </div>
          <!-- col-12 --> 
          
        </div>
        <!-- row -->
        
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="">Location</label>
              <div class="single">
                <select class="ui fluid search multiple  dropdown" name="Job[location][]">
                  <option value=""></option>
                  <option value="US">US</option>
                  <option value="UK">UK</option>
                  <option value="Canada">Canada</option>
                </select>
              </div>
            </div>
          </div>
          <!-- col-12 -->
          
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="" class="">Number of Openings</label>
              <input type="number" class="form-control" name="Job[num_openings]"  />
              <?php //echo $form->textField($model,'num_openings',array('class'=>'form-control')); ?>
            </div>
          </div>
          <!-- col-12 --> 
          
        </div>
        <!-- row -->
        
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="">Experiences</label>
              <div class="single">
              	<?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=10')),'title', 'title');
                   echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                <?php /*?><select class="ui fluid search dropdown" name="Job[experience]">
                  <option value=""></option>
                  <option value="1">1 Year</option>
                  <option value="2">2 Year</option>
                  <option value="3">3 Year</option>
                  <option value="4">4 Year</option>
                  <option value="3">3 Year</option>
                </select><?php */?>
              </div>
            </div>
          </div>
          <!-- col-12 -->
          
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="" class="">Job Type</label>
              <div class="single">
              	<?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=11')),'title', 'title');
                   echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                <?php /*?><select class="ui fluid search dropdown" name="Job[type]">
                  <option value=""></option>
                  <option value="Designer">Designer</option>
                  <option value="Developer">Developer</option>
                  <option value="Graphic Design">Graphic Design</option>
                </select><?php */?>
              </div>
            </div>
          </div>
          <!-- col-12 --> 
          
        </div>
        <!-- row -->
        
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="editor-wrap"> 
              <!-- wysihtml5 -->
              <label for="" class="">Job Description</label>
              <textarea class="textarea form-control" placeholder="Enter text ..." style="width: 100%; min-height: 150px"  name="Job[description]"></textarea>
            </div>
          </div>
          <!-- col-12 --> 
        </div>
        <!-- row -->
        
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            <div class="hirring-pipline">
              <p>Hiring Pipeline</p>
              <label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox1" value="1"  name="Job[resume_submission]">
                Resume Submission </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox2" value="1"  name="Job[assignment_process]">
                Assignemnet Process </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox3" value="1"  name="Job[phone_interview]">
                Phone Interview </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox3" value="1"  name="Job[onsite_interview]">
                Onsite Interview </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox3" value="1"  name="Job[offer]">
                Offer </label>
            </div>
          </div>
          <!-- col --> 
        </div>
        <!-- row -->
        
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <label for="" class="">Rates</label>
            <div class="form-group two-inputs">
              <input type="number" class="form-control" step=".1" id="" placeholder="Rates"   name="Job[rate]">
              <div class="single">
              <?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=12')),'title', 'title');
                   echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                <?php /*?><select class="ui fluid search dropdown" name="Job[currency]">
                  <option value="">Select Currency</option>
                  <option value="USD">USD</option>
                  <option value="PKR">PKR</option>
                  <option value="INR">INR</option>
                </select><?php */?>
              </div>
            </div>
          </div>
          <!-- col-12 -->
          
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label for="">&nbsp;</label>
              <div class="single">
              	<?php 
                   $list = CHtml::listData(Setting::model()->findAll(array('condition'=>'category_id=13')),'title', 'title');
                   echo $form->dropDownList($model, 'cat_id', $list , array('class'=>'ui fluid search dropdown','empty' => '')); ?>
                <?php /*?><select class="ui fluid search dropdown" name="Job[payment_type]">
                  <option value="">Select Payment Type</option>
                  <option value="Paypal">Paypal</option>
                  <option value="Stripe">Stripe</option>
                  <option value="Authorize.net">Authorize.net</option>
                </select>
<?php */?>              
			</div>
            </div>
          </div>
          <!-- col-12 --> 
          
        </div>
        <!-- row -->
        
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            <div class="form-group">
              <label for="">Invite your Team member to Collaborate</label>
              <div class="single">
                <select class="ui fluid search multiple  dropdown" name="Job[invite_team_member][]">
                  <option value=""></option>
                  <option value="first friend">first friend</option>
                  <option value="secod friend">second friend</option>
                  <option value="third friend">third friend</option>
                </select>
              </div>
            </div>
          </div>
          <!-- col-12 --> 
        </div>
        <!-- row -->
        
        <div class="text-center">
          <button type="submit" class="btn-round btn-blue">SAVE & CONTINUE</button>
        </div>
        <?php $this->endWidget(); ?>
    </div>
