<?php

/* @var $this JobController */

/* @var $model Job */

/* @var $form CActiveForm */

?>

<div class="form">
  <?php $form=$this->beginWidget('CActiveForm', array(

	'id'=>'job-form',

	'enableAjaxValidation'=>false,

)); ?>
  <p class="note">Fields with <span class="required">*</span> are required.</p>
  <?php echo $form->errorSummary($model); ?>
  <div class="row"> <?php echo $form->labelEx($model,'cat_id'); ?> 
  <?php echo $form->textField($model,'cat_id'); ?> 
  <?php echo $form->error($model,'cat_id'); ?> 
  </div>
  <div class="row"> <?php echo $form->labelEx($model,'cat_id'); ?> <?php echo $form->textField($model,'cat_id'); ?> <?php echo $form->error($model,'cat_id'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'reference_id'); ?> <?php echo $form->textField($model,'reference_id'); ?> <?php echo $form->error($model,'reference_id'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'user_id'); ?> <?php echo $form->textField($model,'user_id',array('size'=>55,'maxlength'=>55)); ?> <?php echo $form->error($model,'user_id'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'skills'); ?> <?php echo $form->textArea($model,'skills',array('rows'=>6, 'cols'=>50)); ?> <?php echo $form->error($model,'skills'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'location'); ?> <?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>255)); ?> <?php echo $form->error($model,'location'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'num_openings'); ?> <?php echo $form->textField($model,'num_openings'); ?> <?php echo $form->error($model,'num_openings'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'experience'); ?> <?php echo $form->textField($model,'experience',array('size'=>60,'maxlength'=>111)); ?> <?php echo $form->error($model,'experience'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'type'); ?> <?php echo $form->textField($model,'type'); ?> <?php echo $form->error($model,'type'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'description'); ?> <?php echo $form->textField($model,'description'); ?> <?php echo $form->error($model,'description'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'resume_submission'); ?> <?php echo $form->textField($model,'resume_submission'); ?> <?php echo $form->error($model,'resume_submission'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'assignment_process'); ?> <?php echo $form->textField($model,'assignment_process'); ?> <?php echo $form->error($model,'assignment_process'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'phone_interview'); ?> <?php echo $form->textField($model,'phone_interview'); ?> <?php echo $form->error($model,'phone_interview'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'onsite_interview'); ?> <?php echo $form->textField($model,'onsite_interview'); ?> <?php echo $form->error($model,'onsite_interview'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'offer'); ?> <?php echo $form->textField($model,'offer'); ?> <?php echo $form->error($model,'offer'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'rate'); ?> <?php echo $form->textField($model,'rate'); ?> <?php echo $form->error($model,'rate'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'currency'); ?> <?php echo $form->textField($model,'currency',array('size'=>60,'maxlength'=>111)); ?> <?php echo $form->error($model,'currency'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'payment_type'); ?> <?php echo $form->textField($model,'payment_type',array('size'=>60,'maxlength'=>111)); ?> <?php echo $form->error($model,'payment_type'); ?> </div>
  <div class="row"> <?php echo $form->labelEx($model,'invite_team_member'); ?> <?php echo $form->textArea($model,'invite_team_member',array('rows'=>6, 'cols'=>50)); ?> <?php echo $form->error($model,'invite_team_member'); ?> </div>
  <div class="row buttons"> <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?> </div>
  <?php $this->endWidget(); ?>
</div>
<!-- form -->