<?php
/* @var $this JobController */
/* @var $model Job */

$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Job', 'url'=>array('index')),
	array('label'=>'Manage Job', 'url'=>array('admin')),
);
?>

<!--<h1>Create Job</h1>-->
<div class="db-content-inner">
  <div class="db-content">
    <div class="db-g-header">
      <h4>Create New Job Description.</h4>
      <a href="<?php echo Yii::app()->createAbsoluteUrl('job/jobListing'); ?>">Back to Job Listing</a> </div>
		<?php $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
  <!-- db-content --> 
</div>