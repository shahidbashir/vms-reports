	
<?php /*?>  <?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'client-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('class'=>'form-horizontal form-material'),
            //'enctype' => 'multipart/form-data'),
		)); ?>
   <?php if($form->errorSummary($model)) { ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong> There is error submitting form. 
        </div>
  <?php } ?>
  <h3 class="box-title m-b-20">Create my account</h3>
  <div class="form-group ">
    <div class="col-xs-12">
    	<?php echo $form->textField($model,'first_name',array('class'=>'form-control','placeholder'=>"First Name")); ?>
		<?php //echo $form->error($model,'first_name'); ?>
    </div>
  </div>
  <div class="form-group ">
    <div class="col-xs-12">
    	<?php echo $form->textField($model,'last_name',array('class'=>'form-control','placeholder'=>"Last Name")); ?>
		<?php //echo $form->error($model,'last_name'); ?>
    </div>
  </div>
  <div class="form-group ">
    <div class="col-xs-12">
    	<?php echo $form->textField($model,'organization',array('class'=>'form-control','placeholder'=>"Organization")); ?>
		<?php //echo $form->error($model,'organization'); ?>
    </div>
  </div>
  <div class="form-group ">
    <div class="col-xs-12">
    	<?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>"Email")); ?>
		<?php //echo $form->error($model,'email'); ?>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-12">
      <div class="checkbox checkbox-primary p-t-0">
        <input id="checkbox-signup" type="checkbox">
        <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
      </div>
    </div>
  </div>
  <div class="form-group text-center m-t-20">
    <div class="col-xs-12">
      <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Create my account</button>
    </div>
  </div>
  <div class="form-group m-b-0">
    <div class="col-sm-12 text-center">
      <p>Already have an account? <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/login')?>" class="text-primary m-l-5"><b>Sign In</b></a></p>
    </div>
  </div>
<?php $this->endWidget(); ?><?php */?>



<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'client-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('class'=>''),
		)); ?>
  <?php if($form->errorSummary($model)) { ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error!</strong> There is error submitting form. 
        </div>
  <?php } ?>
  
  <span class="splash-title xs-pb-20">Sign Up</span>
  <div class="form-group">
    <?php echo $form->textField($model,'first_name',array('class'=>'form-control','placeholder'=>"First Name")); ?>
	<?php //echo $form->error($model,'first_name'); ?>
  </div>
  <div class="form-group">
    <?php echo $form->textField($model,'last_name',array('class'=>'form-control','placeholder'=>"Last Name")); ?>
	<?php //echo $form->error($model,'last_name'); ?>
  </div>
  <div class="form-group">
    <?php echo $form->textField($model,'organization',array('class'=>'form-control','placeholder'=>"Organization")); ?>
	<?php //echo $form->error($model,'organization'); ?>
    
  </div>
  <div class="form-group">
    <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>"Email Address")); ?>
	<?php //echo $form->error($model,'email'); ?>
  </div>
  <div class="form-group xs-pt-10">
    <button type="submit" class="btn btn-block btn-primary btn-xl">Sign Up</button>
  </div>
<div class="form-group  social-signup">
  <p>Already have an account? <a href="<?php echo Yii::app()->createAbsoluteUrl('Client/default/login'); ?>" class="text-primary m-l-5"><b>Sign In</b></a></p>
</div>
  <div class="form-group xs-pt-10">
    <div class="be-checkbox">
      <input type="checkbox" id="remember">
      <label for="remember">By creating an account, you agree the <a href="#">terms and conditions</a>.</label>
    </div>
  </div>
<?php $this->endWidget(); ?>