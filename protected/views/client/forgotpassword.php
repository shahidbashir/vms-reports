 <?php if(Yii::app()->user->hasFlash('success')):?>
 <div class="message activated">
       
    
      
    <div class="info">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>

  </div><?php endif; ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	    
                                   <div class="form-group">
                                          <label for="">Email</label>
                                        <?php echo $form->textField($model,'email',array('class'=>'form-control')); ?>
    <?php echo $form->error($model,'email'); ?>
                                   </div>

                                   

                                   
                                   <button type="submit" class="btn-round btn-blue btn-signin">Continue</button>

<?php $this->endWidget(); ?>

