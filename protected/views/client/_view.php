<?php
/* @var $this ClientController */
/* @var $data Client */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('super_client_id')); ?>:</b>
	<?php echo CHtml::encode($data->super_client_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
	<?php echo CHtml::encode($data->first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
	<?php echo CHtml::encode($data->last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organization')); ?>:</b>
	<?php echo CHtml::encode($data->organization); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_name')); ?>:</b>
	<?php echo CHtml::encode($data->business_name); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('website_url')); ?>:</b>
	<?php echo CHtml::encode($data->website_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_type')); ?>:</b>
	<?php echo CHtml::encode($data->business_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_staff')); ?>:</b>
	<?php echo CHtml::encode($data->total_staff); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('revenue_range')); ?>:</b>
	<?php echo CHtml::encode($data->revenue_range); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('describe_business')); ?>:</b>
	<?php echo CHtml::encode($data->describe_business); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('linkedin_profile')); ?>:</b>
	<?php echo CHtml::encode($data->linkedin_profile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('twitter_profile')); ?>:</b>
	<?php echo CHtml::encode($data->twitter_profile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_status')); ?>:</b>
	<?php echo CHtml::encode($data->profile_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_approve')); ?>:</b>
	<?php echo CHtml::encode($data->profile_approve); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
	<?php echo CHtml::encode($data->date_created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_updated')); ?>:</b>
	<?php echo CHtml::encode($data->date_updated); ?>
	<br />

	*/ ?>

</div>