<?php /*?><h2>Thank you for registering.</h2>
<div class="message">
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam optio?</p>
  <span class="email">"<?php echo $model->email;?>"</span> </div>
<a href="<?php echo Yii::app()->createAbsoluteUrl('client/resendactivation')?>" class="activation">Resend Activation Code Again.</a><?php */?>


<form class="form-horizontal form-material" id="loginform" action="index.html">
  <div class="text-center m-t-10 m-b-30" > <i class="fa fa-check animated  bounceIn" style="font-size: 60px; color: #00c292"></i> </div>
  <p class="text-center">Please check your email <br>
    <strong> ( <?php echo $model->email;?> )</strong> <br>
    and follow the instruction to complete the registration process.</p>
  <div class="form-group text-center m-t-20">
    <div class="col-xs-12">
      <!--<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Resend Activation Again</button>-->
      <a href="<?php echo Yii::app()->createAbsoluteUrl('client/resendactivation')?>" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light">Resend Activation Again</a>
    </div>
  </div>
</form>
