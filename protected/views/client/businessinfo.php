<?php error_reporting(0); ?>

<?php $this->renderPartial('_header',array('data'=>$data));?>
<div class="row" style="top: -25px;position: relative;">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
     <div class="panel panel-default panel-table">
       
       <div class="panel-body" style="padding: 14px;">

       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-striped dataTable no-footer">
          <tbody>
            <tr>
              <td>Business Name</td>
              <td><?php echo $data->business_name;?></td>
            </tr>

             <tr>
              <td>Email Address</td>
              <td><?php echo $data->email;?></td>
            </tr>



            <tr>
              <td>Website URL</td>
              <td><?php echo $data->website_url;?></td>
            </tr>



            <tr>
              <td>Number of Staff Members</td>
              <td><?php echo $data->total_staff;?></td>
            </tr>



            <tr>
              <td>Business Category</td>
              <td><?php echo $data->business_type;?></td>
            </tr>



            <tr>
              <td> Revenue Range</td>
              <td><?php echo $data->revenue_range;?></td>
            </tr>



            <tr>
              <td>Describe Your Business</td>
              <td><?php echo $data->describe_business;?></td>
            </tr>



            <tr>
              <td>Linked In Profile</td>
              <td><?php echo $data->linkedin_profile;?></td>
            </tr>

            <tr>
              <td>Twitter Profile</td>
              <td><?php echo $data->twitter_profile;?></td>
            </tr>

            <tr>
              <td>Contact Person Name</td>
              <td><?php echo $data->first_name.' '. $data->last_name;?></td>
            </tr>

            <tr>
              <td>Phone Number</td>
              <td><?php echo $data->phone;?></td>
            </tr>

            <tr>
              <td>Mobile Number</td>
              <td><?php echo $data->mobile;?></td>
            </tr>

            <tr>
              <td>Approved by</td>
              <td><?php $approvedBy = Admin::model()->findByPk($data->profile_approved_by);
               if(!empty($approvedBy)){ echo $approvedBy->email; }
               else { echo 'NULL';}?></td>
            </tr>
            <tr>
              <td>Date of Approval</td>
              <td><?php echo $data->profile_approved_date !="0000-00-00 00:00:00"?date('m/d/Y',strtotime($data->profile_approved_date)):"NULL"; ?></td>
            </tr>
            <tr>
              <td>Date of Registration</td>
              <td><?php echo date('m/d/Y',strtotime($data->date_created));?></td>
            </tr>
          </tbody>
        </table>
     
       	<p class="" style="margin:20px 0;">List of Team Members</p>

<table class="table table-striped dataTable no-footer">

	
	<tbody>
	
	


	<?php  $clientModel = Client::model()->findAll(array('condition'=>'super_client_id ='.$data->id));

    foreach($clientModel as $value) { ?>  

		<tr>

			<td class="avatar">
    <div class="inline-block">
      <div class="avatar-small-text">
        <span><?php echo  $value->first_name[0].' '.$value->last_name[0];?></span>
      </div>
    </div>
    <div class="inline-block">
      <strong><?php echo  $value->first_name.' '.$value->last_name;?></strong>
      <br>
      <span class="cell-detail-description"><?php echo  $value->department?></span>
    </div>
  </td>

   <td>
      <strong><?php echo  $value->email?></strong>
      <br>
      <span class="cell-detail-description"><?php echo  $value->member_type?></span>
  </td>
  
  <td style="text-align: center">
      <a href="#" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" ti-trash"></i></a>
  </td>
  

</tr>

	<?php } ?>

	</tbody>

</table>


<hr>

<p class="" style="margin:20px 0;">Client Location </p>



<table class="table table-striped dataTable no-footer">

	<thead>

		<tr>



			<th>Location Name</th>

			<th>Address</th>

			<th>Location Manager</th>

			<th style="text-align: center">&nbsp;</th>



		</tr>

	</thead>

	<tbody>

		 <?php  $modelLocation = Location::model()->findAll(array('condition'=>'reference_id ='.$data->id));

    foreach($modelLocation as $value) { ?>    

		<tr>

			<td><?php echo $value->name; ?></td>

			<td><?php echo $value->address1.' '.$value->city.' '.$value->state.' '.$value->zip_code; echo ', '.$value->country; ?></td>

			<td><?php echo $value->location_account_mananger;?></td>

			<td style="text-align: right">

				<a data-original-title="Edit" class=" tooltips" data-toggle="tooltip" data-placement="top" href="#"><i class="fa fa-pencil"></i></a>

				<a data-original-title="Delete" class=" tooltips" data-toggle="tooltip" data-placement="top" href="#"><i class=" ti-trash"></i></a>

			</td>

		</tr>

	<?php } ?>



	</tbody>

</table>

<hr />

<p class="" style="margin:20px 0;">Work Flow</p>


<table class="table table-striped dataTable no-footer">

	<tbody>



	 <?php  $worklflowModel = Worklflow::model()->findAll(array(
      'condition'=>'client_id ='.$data->id));

    foreach($worklflowModel as $value) {



    $worklflowMemberCount = WorklflowMember::model()->countByAttributes(

      array('workflow_id'=>$value->id));



      $worklflowMemberModel = WorklflowMember::model()->findAll(

      array('condition'=>'workflow_id='.$value->id));

     ?> 
<tr>

			<td><?php echo $value->flow_name;?></td>

			<td><?php echo $worklflowMemberCount;?> Members</td>
			<td style="text-align: right">

			<a data-toggle="modal" data-target="#workflow<?php echo $value->id;?>"  data-original-title="View" class=" tooltips" data-placement="top"><i class="fa fa-eye"></i></a>



			   <div class="modal fade" id="workflow<?php echo $value->id;?>">

<div class="modal-dialog">

<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

<h4 class="modal-title">Work Flow Listing</h4>

<br>

</div>

<div class="modal-body">

<table class="table">

<tbody>

<?php foreach($worklflowMemberModel as $memberValue) {



  $clientModel = Client::model()->findByPk($memberValue->client_id);

  if($clientModel){?>

   

<tr>

<td><?php echo $clientModel->id;?></td>

<td>

<h4><?php echo $clientModel->first_name.' '.$clientModel->last_name;?> </h4>

<p><?php echo  $clientModel->email;?> </p>

</td>

<td>

<p><?php echo  $clientModel->department;?> </p>

</td>

</tr>

<?php } } ?>

</tbody>

</table>

</div>



<div class="seprater-bottom-100"></div>

</div></div></div>

				

			</td>

		</tr>

		

<?php } ?>

	</tbody>

</table>

<hr />
<p class="" style="margin:20px 0;">Job Request</p>
 <table class="table table-striped dataTable no-footer">
	<tbody>
	 <?php  $requestModel = JobrequestDepartment::model()->findAll(array('condition'=>'client_id ='.$data->id));

    foreach($requestModel as $value) { ?>

		

		<tr>

			<td><a href=""><?php echo $value->job_request;?></a></td>

			<td><?php echo $value->department;?></td>

			

			<td style="text-align: right">

				<a data-original-title="Delete" class=" tooltips" data-toggle="tooltip" data-placement="top" href="#"><i class=" ti-trash"></i></a>	

			</td>

		</tr>

	<?php } ?>



	</tbody>

</table>

<hr />

<p class="" style="margin:20px 0;">Billing Code</p>


<table class="table table-striped dataTable no-footer">

	<tbody>



	<?php  $codeModel = BillingcodeDepartment::model()->findAll(array('condition'=>'client_id ='.$data->id));
    foreach($codeModel as $value) { ?>

		

		<tr>

			<td><a href=""><?php echo $value->billingcode;?></a></td>

			<td><?php echo $value->department;?></td>

			

			<td style="text-align: right">

				<a data-original-title="Delete" class=" tooltips" data-toggle="tooltip" data-placement="top" href="#"><i class=" ti-trash"></i></a>	

			</td>

		</tr>

		

	<?php } ?>

	</tbody>

</table>
 </div>
</div></div></div></div>

<?php $this->renderPartial('_footer');?>