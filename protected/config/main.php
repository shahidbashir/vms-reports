<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',
     'defaultController' => 'client', 
	// preloading 'log' component
	'preload'=>array('log'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.vendor.Microsoft.*',
	),
	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'malik',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'Client','Admin'
		
	),
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		 'sendgrid' => array(  
            'class' => 'ext.yii-sendgrid.YiiSendGrid', //path to YiiSendGrid class  
            'username'=>'hypovps.in', //replace with your actual username
   			'password'=>'yahoo2015',
            //alias to the layouts path. Optional  
            //'viewPath' => 'application.views.mail',  
            //wheter to log the emails sent. Optional  
            //'enableLog' => YII_DEBUG, 
            //if enabled, it won't actually send the emails, only log. Optional  
            //'dryRun' => false, 
            //ignore verification of SSL certificate  
            //'disableSslVerification'=>true,
        ),
		'session'=>array(

			'class'=>'CDbHttpSession',
			'timeout'=>1200,
		), 
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		 // this is used in contact page
		 'adminEmail'=>'webmaster@example.com',
		 'msp_secret_path'=>__DIR__.'/'.'msp_secret.json',
		 'client_secret_path'=>__DIR__.'/'.'client_secret.json',
		 'vendor_secret_path'=>__DIR__.'/'.'vendor_secret.json',
	),
);
