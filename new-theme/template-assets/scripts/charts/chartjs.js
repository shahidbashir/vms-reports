/**
 * ChartJS chart page
 */

 jQuery(document).ready(function($) {

     'use strict';

       //Global defaults
      Chart.defaults.global.responsive = true;
      Chart.defaults.global.defaultFontFamily = $.constants.font;
      Chart.defaults.global.defaultFontSize = 12;
      //Title
      Chart.defaults.global.title.fontFamily = $.constants.font;
      Chart.defaults.global.title.fontStyle = 'normal';
      //Tooltip
      Chart.defaults.global.tooltips.FontFamily = $.constants.font;
      Chart.defaults.global.tooltips.FontSize = 12;
      Chart.defaults.global.elements.arc.borderWidth = 1;
      Chart.defaults.global.elements.line.borderWidth = 1;

        
	  //these charts are moved to the jobviewchart file in job views and dashboard screen
});

